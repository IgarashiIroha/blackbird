# Ling Qi

![Ling Qi](https://cdn.myanimelist.net/images/anime/9/81045l.jpg)

* Japanese:  灵契

## Information

 - Type: ONA
 - Episodes: 20
 - Status: Finished Airing
 - Aired: Jun 21, 2016 to Nov 1, 2016
 - Producers: Tencent Animation & Comics 
 - Licensors: None found, add some 
 - Studios: Haoliners Animation League 
 - Source: Web manga
 - Genres: Action, Comedy, Supernatural, Magic, Shounen Ai
 - Duration: 14 min. per ep.
 - Rating: PG-13 - Teens 13 or older


## Links

- [[Refrain] Ling Qi - 01 VOSTFR [1080p]](https://uptobox.com/nqgpur65a4ix)
- [[Refrain] Ling Qi - 01 VOSTFR [720p]](https://uptobox.com/07zzgbz8klco)
- [[Refrain] Ling Qi - 02 VOSTFR [1080p]](https://uptobox.com/0axigc01pg9c)
- [[Refrain] Ling Qi - 02 VOSTFR [720p]](https://uptobox.com/ayape27xjvw3)
- [[Refrain] Ling Qi - 03 VOSTFR [1080p]](https://uptobox.com/q25uzi9z8wzm)
- [[Refrain] Ling Qi - 03 VOSTFR [720p]](https://uptobox.com/ixpnr6tlcqrx)
- [[Refrain] Ling Qi - 04 VOSTFR [1080p]](https://uptobox.com/p1o53pzg6975)
- [[Refrain] Ling Qi - 04 VOSTFR [720p]](https://uptobox.com/o8zcvfhexwbe)
- [[Refrain] Ling Qi - 05 VOSTFR [1080p]](https://uptobox.com/f3dwb5rhfqoh)
- [[Refrain] Ling Qi - 05 VOSTFR [720p]](https://uptobox.com/65jq8iil6m8s)
- [[Refrain] Ling Qi - 06 VOSTFR [1080p]](https://uptobox.com/z4h7k7gv3gt0)
- [[Refrain] Ling Qi - 06 VOSTFR [720p]](https://uptobox.com/w6xnlgnk837i)
- [[Refrain] Ling Qi - 07 VOSTFR [1080p]](https://uptobox.com/klln2shlk7ia)
- [[Refrain] Ling Qi - 07 VOSTFR [720p]](https://uptobox.com/3gl312a75ya6)
- [[Refrain] Ling Qi - 08 VOSTFR [1080p]](https://uptobox.com/wyyd9h4j1zl8)
- [[Refrain] Ling Qi - 08 VOSTFR [720p]](https://uptobox.com/pxco10te1rn8)
- [[Refrain] Ling Qi - 09 VOSTFR [1080p]](https://uptobox.com/yun9iphs3qdq)
- [[Refrain] Ling Qi - 10 VOSTFR [1080p]](https://uptobox.com/160ptvh4rzn3)
- [[Refrain] Ling Qi - 10 VOSTFR [720p]](https://uptobox.com/jby4dije52yz)
- [[Refrain] Ling Qi 2 - 01 VOSTFR [1080p]](https://uptobox.com/arweggzps79m)
- [[Refrain] Ling Qi 2 - 01 VOSTFR [720p]](https://uptobox.com/3vxabvpm2mt5)
- [[Refrain] Ling Qi 2 - 02 VOSTFR [1080p]](https://uptobox.com/16nrxl813rdq)
- [[Refrain] Ling Qi 2 - 02 VOSTFR [720p]](https://uptobox.com/6kzlnjjqadq5)
- [[Refrain] Ling Qi 2 - 03 VOSTFR [1080p]](https://uptobox.com/6n4asdoq5pwy)
- [[Refrain] Ling Qi 2 - 03 VOSTFR [720p]](https://uptobox.com/d2gnxgs70616)
- [[Refrain] Ling Qi 2 - 04 VOSTFR [1080p]](https://uptobox.com/ih198azqulzz)
- [[Refrain] Ling Qi 2 - 04 VOSTFR [720p]](https://uptobox.com/4a057xv3be1c)
- [[Refrain] Ling Qi 2 - 05 VOSTFR [1080p]](https://uptobox.com/hgivngum3d4r)
- [[Refrain] Ling Qi 2 - 05 VOSTFR [720p]](https://uptobox.com/77ineagxhnmh)
- [[Refrain] Ling Qi 2 - 06 VOSTFR [1080p]](https://uptobox.com/kgvbwk9jtey7)
- [[Refrain] Ling Qi 2 - 06 VOSTFR [720p]](https://uptobox.com/nfvu8yav6wxo)
- [[Refrain] Ling Qi 2 - 07 VOSTFR [1080p]](https://uptobox.com/rb6iv6oxmkdg)
- [[Refrain] Ling Qi 2 - 07 VOSTFR [720p]](https://uptobox.com/xp9tlaps0ibg)
- [[Refrain] Ling Qi 2 - 08 VOSTFR [1080p]](https://uptobox.com/brb7nifavj3d)
- [[Refrain] Ling Qi 2 - 08 VOSTFR [720p]](https://uptobox.com/toey3fa5fwh4)
- [[Refrain] Ling Qi 2 - 09 VOSTFR [1080p]](https://uptobox.com/9uxprhcm2nd1)
- [[Refrain] Ling Qi 2 - 09 VOSTFR [720p]](https://uptobox.com/8x0bmxmekenz)
- [[Refrain] Ling Qi 2 - 10 VOSTFR [1080p]](https://uptobox.com/c72atiayoz51)
- [[Refrain] Ling Qi 2 - 10 VOSTFR [720p]](https://uptobox.com/qtd5kh22klkw)
- [[Refrain] Ling Qi 2 - 11 VOSTFR [1080p]](https://uptobox.com/5hddw5p0rksd)
- [[Refrain] Ling Qi 2 - 11 VOSTFR [720p]](https://uptobox.com/icwcc3vp1a25)
- [[Refrain] Ling Qi 2 - 12 VOSTFR [1080p]](https://uptobox.com/tprk3zf3c4xe)
- [[Refrain] Ling Qi 2 - 12 VOSTFR [720p]](https://uptobox.com/467eexlh5kr8)
