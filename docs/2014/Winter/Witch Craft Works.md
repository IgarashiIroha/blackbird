# Witch Craft Works

![Witch Craft Works](https://cdn.myanimelist.net/images/anime/12/55693l.jpg)

* Japanese:  ウィッチクラフトワークス

## Information

 - Type: TV
 - Episodes: 12
 - Status: Finished Airing
 - Aired: Jan 5, 2014 to Mar 23, 2014
 - Premiered: Winter 2014
 - Broadcast: Unknown
 - Producers: Bandai Visual, Lantis, Kodansha, Movic, I Will 
 - Licensors: None found, add some 
 - Studios: J.C.Staff 
 - Source: Manga
 - Genres: Action, Fantasy, Magic, Seinen, Supernatural
 - Duration: 23 min. per ep.
 - Rating: PG-13 - Teens 13 or older


## Links

- [[Refrain] Witch Craft Works - 01 VF VOSTFR [1080p]](https://uptobox.com/4uz587n1rpaq)
- [[Refrain] Witch Craft Works - 01 VF VOSTFR [720p]](https://uptobox.com/j0lez0sihebk)
- [[Refrain] Witch Craft Works - 02 VF VOSTFR [1080p]](https://uptobox.com/n984b5h7z42m)
- [[Refrain] Witch Craft Works - 02 VF VOSTFR [720p]](https://uptobox.com/5cy1locrqsf0)
- [[Refrain] Witch Craft Works - 03 VF VOSTFR [1080p]](https://uptobox.com/26euvf0ely7b)
- [[Refrain] Witch Craft Works - 03 VF VOSTFR [720p]](https://uptobox.com/61p43sz5fsgd)
- [[Refrain] Witch Craft Works - 04 VF VOSTFR [1080p]](https://uptobox.com/vvjgcc1lxzs0)
- [[Refrain] Witch Craft Works - 04 VF VOSTFR [720p]](https://uptobox.com/e6qr2mbbhnre)
- [[Refrain] Witch Craft Works - 05 VF VOSTFR [1080p]](https://uptobox.com/m5jflxelkbyn)
- [[Refrain] Witch Craft Works - 05 VF VOSTFR [720p]](https://uptobox.com/moy6fs6buown)
- [[Refrain] Witch Craft Works - 06 VF VOSTFR [1080p]](https://uptobox.com/ay9pae7ozn3k)
- [[Refrain] Witch Craft Works - 06 VF VOSTFR [720p]](https://uptobox.com/qu4m6iaj9959)
- [[Refrain] Witch Craft Works - 07 VF VOSTFR [1080p]](https://uptobox.com/sm9meow1o7c8)
- [[Refrain] Witch Craft Works - 07 VF VOSTFR [720p]](https://uptobox.com/jn4fqrvfe0cc)
- [[Refrain] Witch Craft Works - 08 VF VOSTFR [1080p]](https://uptobox.com/5kx6qro6je1r)
- [[Refrain] Witch Craft Works - 08 VF VOSTFR [720p]](https://uptobox.com/s7kch2jvyx4g)
- [[Refrain] Witch Craft Works - 09 VF VOSTFR [1080p]](https://uptobox.com/iqgenxnl9cgh)
- [[Refrain] Witch Craft Works - 09 VF VOSTFR [720p]](https://uptobox.com/awj59df2c0pz)
- [[Refrain] Witch Craft Works - 10 VF VOSTFR [1080p]](https://uptobox.com/oje53f8jgw6x)
- [[Refrain] Witch Craft Works - 10 VF VOSTFR [720p]](https://uptobox.com/kzs3wo9cza8n)
- [[Refrain] Witch Craft Works - 11 VF VOSTFR [1080p]](https://uptobox.com/ior3hymqszsb)
- [[Refrain] Witch Craft Works - 11 VF VOSTFR [720p]](https://uptobox.com/0ocnitpeqyyq)
- [[Refrain] Witch Craft Works - 12 VF VOSTFR [1080p]](https://uptobox.com/nq2d9ua089c4)
- [[Refrain] Witch Craft Works - 12 VF VOSTFR [720p]](https://uptobox.com/mc2k6d45qqw5)
