# Rurouni Kenshin: Meiji Kenkaku Romantan - Seisou-hen

![Rurouni Kenshin: Meiji Kenkaku Romantan - Seisou-hen](https://cdn.myanimelist.net/images/anime/1971/90877l.jpg)

* Japanese:  るろうに剣心 -明治剣客浪漫譚- 星霜編

## Information

 - Type: OVA
 - Episodes: 2
 - Status: Finished Airing
 - Aired: Dec 3, 2001
 - Producers: SME Visual Works 
 - Licensors: ADV Films, Aniplex of America 
 - Studios: Studio Deen 
 - Source: Manga
 - Genres: Romance, Samurai, Historical, Drama, Shounen
 - Duration: 42 min. per ep.
 - Rating: R - 17+ (violence & profanity)


## Links

- [[Refrain] Rurouni Kenshin - Meiji Kenkaku Romantan - Seisou-hen - 01 VF VOSTFR [1080p]](https://uptobox.com/e9jye9lmn2iy)
- [[Refrain] Rurouni Kenshin - Meiji Kenkaku Romantan - Seisou-hen - 01 VF VOSTFR [720p]](https://uptobox.com/tghibqp26bm2)
- [[Refrain] Rurouni Kenshin - Meiji Kenkaku Romantan - Seisou-hen - 02 VF VOSTFR [1080p]](https://uptobox.com/bpb9u2y6lm7p)
- [[Refrain] Rurouni Kenshin - Meiji Kenkaku Romantan - Seisou-hen - 02 VF VOSTFR [720p]](https://uptobox.com/dde5qxb7w8yi)
