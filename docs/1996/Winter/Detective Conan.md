# Detective Conan

![Detective Conan](https://cdn.myanimelist.net/images/anime/7/75199l.jpg)

* Japanese:  名探偵コナン

## Information

 - Type: TV
 - Episodes: Unknown
 - Status: Currently Airing
 - Aired: Jan 8, 1996 to ?
 - Premiered: Winter 1996
 - Broadcast: Saturdays at 18:00 (JST)
 - Producers: Yomiuri Telecasting, Animax, YTV, TMS-Kyokuchi, Shogakukan 
 - Licensors: Funimation, Crunchyroll 
 - Studios: TMS Entertainment 
 - Source: Manga
 - Genres: Adventure, Comedy, Mystery, Police, Shounen
 - Duration: 25 min.
 - Rating: PG-13 - Teens 13 or older


## Links

- [[Refrain] Detective Conan - 913 VOSTFR {ADN} [1080p]](https://uptobox.com/1ut0o4cddm9i)
- [[Refrain] Detective Conan - 913 VOSTFR {ADN} [720p]](https://uptobox.com/fobhwr9mhr12)
- [[Refrain] Detective Conan - 914 VOSTFR {ADN} [1080p]](https://uptobox.com/yjoqhhpt5im6)
- [[Refrain] Detective Conan - 914 VOSTFR {ADN} [720p]](https://uptobox.com/qzoy93ig1gli)
- [[Refrain] Detective Conan - 915 VOSTFR {ADN} [1080p]](https://uptobox.com/8e5igbamdad7)
- [[Refrain] Detective Conan - 915 VOSTFR {ADN} [720p]](https://uptobox.com/apdprttbq4eo)
- [[Refrain] Detective Conan - 916 VOSTFR {ADN} [1080p]](https://uptobox.com/t48acscb7d9f)
- [[Refrain] Detective Conan - 916 VOSTFR {ADN} [720p]](https://uptobox.com/km9v2xmzvnna)
- [[Refrain] Detective Conan - 917 VOSTFR {ADN} [1080p]](https://uptobox.com/ek012h9jbtb0)
- [[Refrain] Detective Conan - 917 VOSTFR {ADN} [720p]](https://uptobox.com/8f3h18ty8xoa)
- [[Refrain] Detective Conan - 918 VOSTFR {ADN} [1080p]](https://uptobox.com/i4uk5vgumifp)
- [[Refrain] Detective Conan - 918 VOSTFR {ADN} [720p]](https://uptobox.com/iq5zhxlx3per)
- [[Refrain] Detective Conan - 919 VOSTFR {ADN} [1080p]](https://uptobox.com/9xpxt5e744k1)
- [[Refrain] Detective Conan - 919 VOSTFR {ADN} [720p]](https://uptobox.com/m7exatepptfm)
- [[Refrain] Detective Conan - 920 VOSTFR {ADN} [1080p]](https://uptobox.com/r6km4r6rj4r7)
- [[Refrain] Detective Conan - 920 VOSTFR {ADN} [720p]](https://uptobox.com/abmkdd3gupbc)
- [[Refrain] Detective Conan - 921 VOSTFR {ADN} [1080p]](https://uptobox.com/jyrmp1t78qgq)
- [[Refrain] Detective Conan - 921 VOSTFR {ADN} [720p]](https://uptobox.com/ldrpndz5asyx)
- [[Refrain] Detective Conan - 922 VOSTFR {ADN} [1080p]](https://uptobox.com/rmcjcpg8vl4u)
- [[Refrain] Detective Conan - 922 VOSTFR {ADN} [720p]](https://uptobox.com/dtpf7i3zxcdk)
- [[Refrain] Detective Conan - 923 VOSTFR {ADN} [720p]](https://uptobox.com/whoos7g0hb0k)
- [[Refrain] Detective Conan - 924 VOSTFR {ADN} [1080p]](https://uptobox.com/2dno1wzqv03p)
- [[Refrain] Detective Conan - 924 VOSTFR {ADN} [720p]](https://uptobox.com/mag4xbufq9j5)
- [[Refrain] Detective Conan - 925 VOSTFR {ADN} [1080p]](https://uptobox.com/poynka6eji77)
- [[Refrain] Detective Conan - 925 VOSTFR {ADN} [720p]](https://uptobox.com/i52vpdbrktlo)
- [[Refrain] Detective Conan - 926 VOSTFR {ADN} [1080p]](https://uptobox.com/oz11sd2d3pbz)
- [[Refrain] Detective Conan - 926 VOSTFR {ADN} [720p]](https://uptobox.com/5xh4qi5nihit)
- [[Refrain] Detective Conan - 927 VOSTFR {ADN} [1080p]](https://uptobox.com/nrrd6itwfhtg)
- [[Refrain] Detective Conan - 927 VOSTFR {ADN} [720p]](https://uptobox.com/hyxrz37jfnpr)
- [[Refrain] Detective Conan - 928 VOSTFR {ADN} [1080p]](https://uptobox.com/im1zbe9vqd6a)
- [[Refrain] Detective Conan - 928 VOSTFR {ADN} [720p]](https://uptobox.com/n10try98k5r0)
- [[Refrain] Detective Conan - 929 VOSTFR {ADN} [1080p]](https://uptobox.com/vriwawcnoi9l)
- [[Refrain] Detective Conan - 929 VOSTFR {ADN} [720p]](https://uptobox.com/213c23otwdo4)
- [[Refrain] Detective Conan - 930 VOSTFR {ADN} [1080p]](https://uptobox.com/48lgb2lfqfir)
- [[Refrain] Detective Conan - 930 VOSTFR {ADN} [720p]](https://uptobox.com/hcendmvif56t)
- [[Refrain] Detective Conan - 931 VOSTFR {ADN} [1080p]](https://uptobox.com/e39mmptryjxe)
- [[Refrain] Detective Conan - 931 VOSTFR {ADN} [720p]](https://uptobox.com/hstopcmhylyw)
- [[Refrain] Detective Conan - 932 VOSTFR {ADN} [1080p]](https://uptobox.com/aok0s1mpnsx4)
- [[Refrain] Detective Conan - 932 VOSTFR {ADN} [720p]](https://uptobox.com/hho9jjadw9yi)
- [[Refrain] Detective Conan - 933 VOSTFR {ADN} [1080p]](https://uptobox.com/v1b7w8qlgg5a)
- [[Refrain] Detective Conan - 933 VOSTFR {ADN} [720p]](https://uptobox.com/p3lari3om3lt)
- [[Refrain] Detective Conan - 934 VOSTFR {ADN} [1080p]](https://uptobox.com/3jkxv27o9yu8)
- [[Refrain] Detective Conan - 934 VOSTFR {ADN} [720p]](https://uptobox.com/ze22v8v6st1u)
- [[Refrain] Detective Conan - 935 VOSTFR {ADN} [1080p]](https://uptobox.com/v552149cxrbe)
- [[Refrain] Detective Conan - 935 VOSTFR {ADN} [720p]](https://uptobox.com/8cwysal33tzo)
- [[Refrain] Detective Conan - 936 VOSTFR {ADN} [1080p]](https://uptobox.com/c9kfxy60zlpj)
- [[Refrain] Detective Conan - 936 VOSTFR {ADN} [720p]](https://uptobox.com/dcy2ohlpatpl)
- [[Refrain] Detective Conan - 937 VOSTFR {ADN} [1080p]](https://uptobox.com/qmayxxp6myb4)
- [[Refrain] Detective Conan - 937 VOSTFR {ADN} [720p]](https://uptobox.com/0mbhjz9r34ap)
- [[Refrain] Detective Conan - 938 VOSTFR {ADN} [1080p]](https://uptobox.com/ajr1ov7kay5j)
- [[Refrain] Detective Conan - 938 VOSTFR {ADN} [720p]](https://uptobox.com/2mw7f1awwsjj)
- [[Refrain] Detective Conan - 939 VOSTFR {ADN} [1080p]](https://uptobox.com/wtx0ycic1m2p)
- [[Refrain] Detective Conan - 939 VOSTFR {ADN} [720p]](https://uptobox.com/zfzfa0o2pbsa)
- [[Refrain] Detective Conan - 940 VOSTFR {ADN} [1080p]](https://uptobox.com/tw95noq9q7pf)
- [[Refrain] Detective Conan - 940 VOSTFR {ADN} [720p]](https://uptobox.com/nudwtoymadqe)
- [[Refrain] Detective Conan - 941 VOSTFR {ADN} [1080p]](https://uptobox.com/0qdj4tm2sbw8)
- [[Refrain] Detective Conan - 941 VOSTFR {ADN} [720p]](https://uptobox.com/cxm1cgk7gceg)
- [[Refrain] Detective Conan - 942 VOSTFR {ADN} [1080p]](https://uptobox.com/dn0q1hgnd29r)
- [[Refrain] Detective Conan - 942 VOSTFR {ADN} [720p]](https://uptobox.com/54jwpmjsw5nf)
- [[Refrain] Detective Conan - 943 VOSTFR {ADN} [1080p]](https://uptobox.com/hfs8jmh8w6tp)
- [[Refrain] Detective Conan - 943 VOSTFR {ADN} [720p]](https://uptobox.com/ad8lble5kul8)
