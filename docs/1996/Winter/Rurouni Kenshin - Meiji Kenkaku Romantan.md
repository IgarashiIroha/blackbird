# Rurouni Kenshin: Meiji Kenkaku Romantan

![Rurouni Kenshin: Meiji Kenkaku Romantan](https://cdn.myanimelist.net/images/anime/7/6803l.jpg)

* Japanese:  るろうに剣心 -明治剣客浪漫譚-

## Information

 - Type: TV
 - Episodes: 94
 - Status: Finished Airing
 - Aired: Jan 10, 1996 to Sep 8, 1998
 - Premiered: Winter 1996
 - Broadcast: Wednesdays at 19:30 (JST)
 - Producers: Aniplex, SME Visual Works, Fuji TV 
 - Licensors: Media Blasters 
 - Studios: Studio Gallop, Studio Deen 
 - Source: Manga
 - Genres: Action, Adventure, Comedy, Historical, Romance, Samurai, Shounen
 - Duration: 25 min. per ep.
 - Rating: PG-13 - Teens 13 or older


## Links

- [[Refrain] Rurouni Kenshin - Meiji Kenkaku Romantan - 01 VF VOSTFR [540p]](https://uptobox.com/nckrz6686y0a)
- [[Refrain] Rurouni Kenshin - Meiji Kenkaku Romantan - 02 VF VOSTFR [540p]](https://uptobox.com/f7mtgz1jknwf)
- [[Refrain] Rurouni Kenshin - Meiji Kenkaku Romantan - 03 VF VOSTFR [540p]](https://uptobox.com/25eabbu5kj78)
- [[Refrain] Rurouni Kenshin - Meiji Kenkaku Romantan - 04 VF VOSTFR [540p]](https://uptobox.com/u234efzijyut)
- [[Refrain] Rurouni Kenshin - Meiji Kenkaku Romantan - 05 VF VOSTFR [540p]](https://uptobox.com/qlehyxv5ub5w)
- [[Refrain] Rurouni Kenshin - Meiji Kenkaku Romantan - 06 VF VOSTFR [540p]](https://uptobox.com/i6sqfqwtbmgv)
- [[Refrain] Rurouni Kenshin - Meiji Kenkaku Romantan - 07 VF VOSTFR [540p]](https://uptobox.com/eukgjuetoup0)
- [[Refrain] Rurouni Kenshin - Meiji Kenkaku Romantan - 08 VF VOSTFR [540p]](https://uptobox.com/q0wolq8iw5uj)
- [[Refrain] Rurouni Kenshin - Meiji Kenkaku Romantan - 09 VF VOSTFR [540p]](https://uptobox.com/d9vzr6eps0bp)
- [[Refrain] Rurouni Kenshin - Meiji Kenkaku Romantan - 10 VF VOSTFR [540p]](https://uptobox.com/uz1bl8l7a5db)
- [[Refrain] Rurouni Kenshin - Meiji Kenkaku Romantan - 11 VF VOSTFR [540p]](https://uptobox.com/j8vbjyjyc91x)
- [[Refrain] Rurouni Kenshin - Meiji Kenkaku Romantan - 12 VF VOSTFR [540p]](https://uptobox.com/4yqa9ivdcpmh)
- [[Refrain] Rurouni Kenshin - Meiji Kenkaku Romantan - 13 VF VOSTFR [540p]](https://uptobox.com/nqeye1jois46)
- [[Refrain] Rurouni Kenshin - Meiji Kenkaku Romantan - 14 VF VOSTFR [540p]](https://uptobox.com/hgs6e2pjq4rn)
- [[Refrain] Rurouni Kenshin - Meiji Kenkaku Romantan - 15 VF VOSTFR [540p]](https://uptobox.com/pz1hrdypipfq)
- [[Refrain] Rurouni Kenshin - Meiji Kenkaku Romantan - 16 VF VOSTFR [540p]](https://uptobox.com/aao53nefavs3)
- [[Refrain] Rurouni Kenshin - Meiji Kenkaku Romantan - 17 VF VOSTFR [540p]](https://uptobox.com/5d5aj561wheh)
- [[Refrain] Rurouni Kenshin - Meiji Kenkaku Romantan - 18 VF VOSTFR [540p]](https://uptobox.com/9b4129wb8yif)
- [[Refrain] Rurouni Kenshin - Meiji Kenkaku Romantan - 19 VF VOSTFR [540p]](https://uptobox.com/c8v9gm8sgwl2)
- [[Refrain] Rurouni Kenshin - Meiji Kenkaku Romantan - 20 VF VOSTFR [540p]](https://uptobox.com/m49gbb3yir1v)
- [[Refrain] Rurouni Kenshin - Meiji Kenkaku Romantan - 21 VF VOSTFR [540p]](https://uptobox.com/ju2zxh17xfoi)
- [[Refrain] Rurouni Kenshin - Meiji Kenkaku Romantan - 22 VF VOSTFR [540p]](https://uptobox.com/vk7ifeep1u39)
- [[Refrain] Rurouni Kenshin - Meiji Kenkaku Romantan - 23 VF VOSTFR [540p]](https://uptobox.com/np12yox4zn5z)
- [[Refrain] Rurouni Kenshin - Meiji Kenkaku Romantan - 24 VF VOSTFR [540p]](https://uptobox.com/bixfnmbk381p)
- [[Refrain] Rurouni Kenshin - Meiji Kenkaku Romantan - 25 VF VOSTFR [540p]](https://uptobox.com/ktg9xagzm7gu)
- [[Refrain] Rurouni Kenshin - Meiji Kenkaku Romantan - 26 VF VOSTFR [540p]](https://uptobox.com/67y22z6f5up5)
- [[Refrain] Rurouni Kenshin - Meiji Kenkaku Romantan - 27 VF VOSTFR [540p]](https://uptobox.com/u1gvp1xhs6dz)
- [[Refrain] Rurouni Kenshin - Meiji Kenkaku Romantan - 28 VF VOSTFR [540p]](https://uptobox.com/rn9a6s40r69e)
- [[Refrain] Rurouni Kenshin - Meiji Kenkaku Romantan - 29 VF VOSTFR [540p]](https://uptobox.com/7y13ld8sbiwm)
- [[Refrain] Rurouni Kenshin - Meiji Kenkaku Romantan - 30 VF VOSTFR [540p]](https://uptobox.com/bjhx8uhiyjfv)
- [[Refrain] Rurouni Kenshin - Meiji Kenkaku Romantan - 31 VF VOSTFR [540p]](https://uptobox.com/gsca1o35v84o)
- [[Refrain] Rurouni Kenshin - Meiji Kenkaku Romantan - 32 VF VOSTFR [540p]](https://uptobox.com/gm86j7ptnsbr)
- [[Refrain] Rurouni Kenshin - Meiji Kenkaku Romantan - 33 VF VOSTFR [540p]](https://uptobox.com/s857rp3un92s)
- [[Refrain] Rurouni Kenshin - Meiji Kenkaku Romantan - 34 VF VOSTFR [540p]](https://uptobox.com/rzihahivlkq2)
- [[Refrain] Rurouni Kenshin - Meiji Kenkaku Romantan - 34 VF VOSTFR [540p]](https://uptobox.com/mp6edubz4bhg)
- [[Refrain] Rurouni Kenshin - Meiji Kenkaku Romantan - 35 VF VOSTFR [540p]](https://uptobox.com/03fbjemv583m)
- [[Refrain] Rurouni Kenshin - Meiji Kenkaku Romantan - 36 VF VOSTFR [540p]](https://uptobox.com/owanijvgeb0g)
- [[Refrain] Rurouni Kenshin - Meiji Kenkaku Romantan - 37 VF VOSTFR [540p]](https://uptobox.com/vk9ko04mitlk)
- [[Refrain] Rurouni Kenshin - Meiji Kenkaku Romantan - 38 VF VOSTFR [540p]](https://uptobox.com/g6j6rwfqajv9)
- [[Refrain] Rurouni Kenshin - Meiji Kenkaku Romantan - 39 VF VOSTFR [540p]](https://uptobox.com/tmegv9679kb4)
- [[Refrain] Rurouni Kenshin - Meiji Kenkaku Romantan - 40 VF VOSTFR [540p]](https://uptobox.com/g3w8y1lwew2w)
- [[Refrain] Rurouni Kenshin - Meiji Kenkaku Romantan - 41 VF VOSTFR [540p]](https://uptobox.com/jg83q3l7iypo)
- [[Refrain] Rurouni Kenshin - Meiji Kenkaku Romantan - 42 VF VOSTFR [540p]](https://uptobox.com/oz6rkg5d2g5j)
- [[Refrain] Rurouni Kenshin - Meiji Kenkaku Romantan - 43 VF VOSTFR [540p]](https://uptobox.com/mlxtk81uus5r)
- [[Refrain] Rurouni Kenshin - Meiji Kenkaku Romantan - 44 VF VOSTFR [540p]](https://uptobox.com/pzymsso5ozsd)
- [[Refrain] Rurouni Kenshin - Meiji Kenkaku Romantan - 45 VF VOSTFR [540p]](https://uptobox.com/7inkqdoapr2y)
- [[Refrain] Rurouni Kenshin - Meiji Kenkaku Romantan - 46 VF VOSTFR [540p]](https://uptobox.com/r8gvbps357g7)
- [[Refrain] Rurouni Kenshin - Meiji Kenkaku Romantan - 47 VF VOSTFR [540p]](https://uptobox.com/9dkh1bjn2bcf)
- [[Refrain] Rurouni Kenshin - Meiji Kenkaku Romantan - 48 VF VOSTFR [540p]](https://uptobox.com/jfwcinr0as0p)
- [[Refrain] Rurouni Kenshin - Meiji Kenkaku Romantan - 49 VF VOSTFR [540p]](https://uptobox.com/pfmjqdxo2ruz)
- [[Refrain] Rurouni Kenshin - Meiji Kenkaku Romantan - 50 VF VOSTFR [540p]](https://uptobox.com/nta3dpnatsf7)
- [[Refrain] Rurouni Kenshin - Meiji Kenkaku Romantan - 51 VF VOSTFR [540p]](https://uptobox.com/g6kz6wah8u2q)
- [[Refrain] Rurouni Kenshin - Meiji Kenkaku Romantan - 52 VF VOSTFR [540p]](https://uptobox.com/0afwr4z79e0j)
- [[Refrain] Rurouni Kenshin - Meiji Kenkaku Romantan - 53 VF VOSTFR [540p]](https://uptobox.com/htxsvt1g6efl)
- [[Refrain] Rurouni Kenshin - Meiji Kenkaku Romantan - 54 VF VOSTFR [540p]](https://uptobox.com/s2xqajglh75p)
- [[Refrain] Rurouni Kenshin - Meiji Kenkaku Romantan - 55 VF VOSTFR [540p]](https://uptobox.com/vn2aaqlubw3x)
- [[Refrain] Rurouni Kenshin - Meiji Kenkaku Romantan - 56 VF VOSTFR [540p]](https://uptobox.com/p7xopaw2jajw)
- [[Refrain] Rurouni Kenshin - Meiji Kenkaku Romantan - 57 VF VOSTFR [540p]](https://uptobox.com/kxskfjimejhq)
- [[Refrain] Rurouni Kenshin - Meiji Kenkaku Romantan - 58 VF VOSTFR [540p]](https://uptobox.com/qdx232iu6tbw)
- [[Refrain] Rurouni Kenshin - Meiji Kenkaku Romantan - 59 VF VOSTFR [540p]](https://uptobox.com/fkq85pqvtczt)
- [[Refrain] Rurouni Kenshin - Meiji Kenkaku Romantan - 60 VF VOSTFR [540p]](https://uptobox.com/o3m39mta5pe2)
- [[Refrain] Rurouni Kenshin - Meiji Kenkaku Romantan - 61 VF VOSTFR [540p]](https://uptobox.com/gq38qbmhtnk1)
- [[Refrain] Rurouni Kenshin - Meiji Kenkaku Romantan - 62 VF VOSTFR [540p]](https://uptobox.com/plh1ad6kikxa)
- [[Refrain] Rurouni Kenshin - Meiji Kenkaku Romantan - 63 VF VOSTFR [540p]](https://uptobox.com/oq8otxnjhhph)
- [[Refrain] Rurouni Kenshin - Meiji Kenkaku Romantan - 64 VF VOSTFR [540p]](https://uptobox.com/1mw6wq31i6nd)
- [[Refrain] Rurouni Kenshin - Meiji Kenkaku Romantan - 65 VF VOSTFR [540p]](https://uptobox.com/2tfvsj6t5fyf)
- [[Refrain] Rurouni Kenshin - Meiji Kenkaku Romantan - 66 VF VOSTFR [540p]](https://uptobox.com/wyeqmuybe02q)
- [[Refrain] Rurouni Kenshin - Meiji Kenkaku Romantan - 67 VF VOSTFR [540p]](https://uptobox.com/e6lurzawjptf)
- [[Refrain] Rurouni Kenshin - Meiji Kenkaku Romantan - 68 VF VOSTFR [540p]](https://uptobox.com/i97rzavlla65)
