# Mobile Suit Gundam: The Origin

![Mobile Suit Gundam: The Origin](https://cdn.myanimelist.net/images/anime/4/72702l.jpg)

* Japanese:  機動戦士ガンダムTHE ORIGIN

## Information

 - Type: OVA
 - Episodes: 6
 - Status: Finished Airing
 - Aired: Apr 24, 2015 to May 5, 2018
 - Producers: None found, add some 
 - Licensors: NYAV Post 
 - Studios: Sunrise 
 - Source: Manga
 - Genres: Action, Military, Sci-Fi, Space, Mecha, Shounen
 - Duration: 1 hr. 11 min. per ep.
 - Rating: R+ - Mild Nudity


## Links

- [[Refrain] Mobile Suit Gundam - The Origin - Advent of the Red Comet - 01 VOSTFR [1080p]](https://uptobox.com/jtv7yfzjm3u1)
- [[Refrain] Mobile Suit Gundam - The Origin - Advent of the Red Comet - 01 VOSTFR [720p]](https://uptobox.com/kc5olpr7asnn)
- [[Refrain] Mobile Suit Gundam - The Origin - Advent of the Red Comet - 02 VOSTFR [1080p]](https://uptobox.com/2txfuus4v2ga)
- [[Refrain] Mobile Suit Gundam - The Origin - Advent of the Red Comet - 02 VOSTFR [720p]](https://uptobox.com/gusdap36pp58)
- [[Refrain] Mobile Suit Gundam - The Origin - Advent of the Red Comet - 03 VOSTFR [1080p]](https://uptobox.com/gt7p95f5t4tr)
- [[Refrain] Mobile Suit Gundam - The Origin - Advent of the Red Comet - 03 VOSTFR [720p]](https://uptobox.com/pyn20nu9j11q)
- [[Refrain] Mobile Suit Gundam - The Origin - Advent of the Red Comet - 04 VOSTFR [1080p]](https://uptobox.com/ecjj88ai5enf)
- [[Refrain] Mobile Suit Gundam - The Origin - Advent of the Red Comet - 04 VOSTFR [720p]](https://uptobox.com/u50f97uudtbe)
- [[Refrain] Mobile Suit Gundam - The Origin - Advent of the Red Comet - 05 VOSTFR [1080p]](https://uptobox.com/wg0izersgojb)
- [[Refrain] Mobile Suit Gundam - The Origin - Advent of the Red Comet - 05 VOSTFR [720p]](https://uptobox.com/jvua9mck4chb)
- [[Refrain] Mobile Suit Gundam - The Origin - Advent of the Red Comet - 08 VOSTFR [1080p]](https://uptobox.com/t1v1sm5fsfjv)
- [[Refrain] Mobile Suit Gundam - The Origin - Advent of the Red Comet - 08 VOSTFR [720p]](https://uptobox.com/vybkv350e94e)
