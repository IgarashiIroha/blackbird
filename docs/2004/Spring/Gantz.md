# Gantz

![Gantz](https://cdn.myanimelist.net/images/anime/13/5998l.jpg)

* Japanese:  ガンツ

## Information

 - Type: TV
 - Episodes: 13
 - Status: Finished Airing
 - Aired: Apr 13, 2004 to Jun 22, 2004
 - Premiered: Spring 2004
 - Broadcast: Unknown
 - Producers: Fuji TV 
 - Licensors: ADV Films, Funimation 
 - Studios: Gonzo 
 - Source: Manga
 - Genres: Action, Sci-Fi, Horror, Psychological, Supernatural, Drama, Ecchi
 - Duration: 22 min. per ep.
 - Rating: R+ - Mild Nudity


## Links

- [[Refrain] Gantz - 01 VF VOSTFR [480p]](https://uptobox.com/y8l775p4cu1z)
- [[Refrain] Gantz - 02 VF VOSTFR [480p]](https://uptobox.com/ksvym4hp2d12)
- [[Refrain] Gantz - 03 VF VOSTFR [480p]](https://uptobox.com/xqdogexmrvug)
- [[Refrain] Gantz - 04 VF VOSTFR [480p]](https://uptobox.com/akf8udb9nte4)
- [[Refrain] Gantz - 05 VF VOSTFR [480p]](https://uptobox.com/as9jfkhrozlu)
- [[Refrain] Gantz - 06 VF VOSTFR [480p]](https://uptobox.com/4hu6zndbjjnw)
- [[Refrain] Gantz - 07 VF VOSTFR [480p]](https://uptobox.com/r9as50rx9lh0)
- [[Refrain] Gantz - 08 VF VOSTFR [480p]](https://uptobox.com/pni1gvj7jxbn)
- [[Refrain] Gantz - 09 VF VOSTFR [480p]](https://uptobox.com/4orr5ar907cg)
- [[Refrain] Gantz - 10 VF VOSTFR [480p]](https://uptobox.com/vco3wgz0g7h4)
- [[Refrain] Gantz - 11 VF VOSTFR [480p]](https://uptobox.com/d2qeatweyrls)
- [[Refrain] Gantz - 12 VF VOSTFR [480p]](https://uptobox.com/c6q0wcre47i5)
- [[Refrain] Gantz - 13 VF VOSTFR [480p]](https://uptobox.com/w8lky2pqxe34)
- [[Refrain] Gantz - 14 VF VOSTFR [480p]](https://uptobox.com/ef13k0y7pepq)
- [[Refrain] Gantz - 15 VF VOSTFR [480p]](https://uptobox.com/czy8j4ra5qkf)
- [[Refrain] Gantz - 16 VF VOSTFR [480p]](https://uptobox.com/a4xtulzs7hj9)
- [[Refrain] Gantz - 17 VF VOSTFR [480p]](https://uptobox.com/oyyh4hguotkz)
- [[Refrain] Gantz - 18 VF VOSTFR [480p]](https://uptobox.com/m88zw0ba4n5z)
- [[Refrain] Gantz - 19 VF VOSTFR [480p]](https://uptobox.com/jpu3qa78zuqt)
- [[Refrain] Gantz - 20 VF VOSTFR [480p]](https://uptobox.com/a652ohflmimj)
- [[Refrain] Gantz - 21 VF VOSTFR [480p]](https://uptobox.com/eoll9mn0nefh)
- [[Refrain] Gantz - 22 VF VOSTFR [480p]](https://uptobox.com/qvls44m1qw3t)
- [[Refrain] Gantz - 23 VF VOSTFR [480p]](https://uptobox.com/ll1rjt1t9pur)
- [[Refrain] Gantz - 24 VF VOSTFR [480p]](https://uptobox.com/vttoyl5ml6nd)
- [[Refrain] Gantz - 25 VF VOSTFR [480p]](https://uptobox.com/muov40lt1ph6)
- [[Refrain] Gantz - 26 VF VOSTFR [480p]](https://uptobox.com/0imf8wptarkr)
