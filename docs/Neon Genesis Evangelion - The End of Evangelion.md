# Neon Genesis Evangelion: The End of Evangelion

![Neon Genesis Evangelion: The End of Evangelion](https://cdn.myanimelist.net/images/anime/12/39305l.jpg)

* Japanese:  新世紀エヴァンゲリオン劇場版 THE END OF EVANGELION

## Information

 - Type: Movie
 - Episodes: 1
 - Status: Finished Airing
 - Aired: Jul 19, 1997
 - Producers: TV Tokyo, Toei Animation, Kadokawa Shoten, Movic, Sega, TV Tokyo Music, Audio Tanaka 
 - Licensors: Manga Entertainment 
 - Studios: Gainax, Production I.G 
 - Source: Original
 - Genres: Sci-Fi, Dementia, Psychological, Drama, Mecha
 - Duration: 1 hr. 27 min.
 - Rating: R+ - Mild Nudity


## Links

