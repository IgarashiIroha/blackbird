# Angel Beats!

![Angel Beats!](https://cdn.myanimelist.net/images/anime/10/22061l.jpg)

* Japanese:  Angel Beats!（エンジェルビーツ）

## Information

 - Type: TV
 - Episodes: 13
 - Status: Finished Airing
 - Aired: Apr 3, 2010 to Jun 26, 2010
 - Premiered: Spring 2010
 - Broadcast: Unknown
 - Producers: Aniplex, Dentsu, Mainichi Broadcasting System, CBC, Movic, Visual Art&#039;s, ASCII Media Works 
 - Licensors: Sentai Filmworks 
 - Studios: P.A. Works 
 - Source: Original
 - Genres: Action, Comedy, Drama, School, Supernatural
 - Duration: 24 min. per ep.
 - Rating: PG-13 - Teens 13 or older


## Links

- [[Refrain] Angel Beats! - 01 VOSTFR [720p]](https://uptobox.com/6ah2fmkavmg6)
- [[Refrain] Angel Beats! - 02 VOSTFR [720p]](https://uptobox.com/7j7breyis969)
- [[Refrain] Angel Beats! - 03 VOSTFR [720p]](https://uptobox.com/1w5f6j30yumw)
- [[Refrain] Angel Beats! - 04 VOSTFR [720p]](https://uptobox.com/qwfgna63d80u)
- [[Refrain] Angel Beats! - 05 VOSTFR [720p]](https://uptobox.com/0ktoe61pzf39)
- [[Refrain] Angel Beats! - 06 VOSTFR [720p]](https://uptobox.com/i4adg9h4ob8f)
- [[Refrain] Angel Beats! - 07 VOSTFR [720p]](https://uptobox.com/lk45amy69g5w)
- [[Refrain] Angel Beats! - 08 VOSTFR [720p]](https://uptobox.com/87lwfpt0nhxm)
- [[Refrain] Angel Beats! - 09 VOSTFR [720p]](https://uptobox.com/vftdmfcv8df5)
- [[Refrain] Angel Beats! - 10 VOSTFR [720p]](https://uptobox.com/bk1zz6njd3ie)
- [[Refrain] Angel Beats! - 11 VOSTFR [720p]](https://uptobox.com/e8q1c7avexwz)
- [[Refrain] Angel Beats! - 12 VOSTFR [720p]](https://uptobox.com/6dj66em9hwb2)
- [[Refrain] Angel Beats! - 13 VOSTFR [720p]](https://uptobox.com/vdbxher808cq)
- [[Refrain] Angel Beats! - 14 VOSTFR [720p]](https://uptobox.com/un8y1qht9c1u)
