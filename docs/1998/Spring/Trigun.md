# Trigun

![Trigun](https://cdn.myanimelist.net/images/anime/7/20310l.jpg)

* Japanese:  トライガン

## Information

 - Type: TV
 - Episodes: 26
 - Status: Finished Airing
 - Aired: Apr 1, 1998 to Sep 30, 1998
 - Premiered: Spring 1998
 - Broadcast: Thursdays at 01:15 (JST)
 - Producers: Victor Entertainment 
 - Licensors: Funimation, Geneon Entertainment USA 
 - Studios: Madhouse 
 - Source: Manga
 - Genres: Action, Sci-Fi, Adventure, Comedy, Drama, Shounen
 - Duration: 24 min. per ep.
 - Rating: PG-13 - Teens 13 or older


## Links

- [[Refrain] Trigun - 01 VF VOSTFR [540p]](https://uptobox.com/lq47brrtbl5i)
- [[Refrain] Trigun - 02 VF VOSTFR [540p]](https://uptobox.com/zq4ugnoxey22)
- [[Refrain] Trigun - 03 VF VOSTFR [540p]](https://uptobox.com/xqsolgthsvxi)
- [[Refrain] Trigun - 04 VF VOSTFR [540p]](https://uptobox.com/9g96b45xp3og)
- [[Refrain] Trigun - 05 VF VOSTFR [540p]](https://uptobox.com/u9n72q0p2cw9)
- [[Refrain] Trigun - 06 VF VOSTFR [540p]](https://uptobox.com/enyq4ybjraas)
- [[Refrain] Trigun - 07 VF VOSTFR [540p]](https://uptobox.com/1kycc3v7pa09)
- [[Refrain] Trigun - 08 VF VOSTFR [540p]](https://uptobox.com/66wxjsvp88kp)
- [[Refrain] Trigun - 09 VF VOSTFR [540p]](https://uptobox.com/29p3sakmxqv7)
- [[Refrain] Trigun - 10 VF VOSTFR [540p]](https://uptobox.com/8v55cznjia6u)
- [[Refrain] Trigun - 11 VF VOSTFR [540p]](https://uptobox.com/muxqdg5zmv0m)
- [[Refrain] Trigun - 12 VF VOSTFR [540p]](https://uptobox.com/r2e71c902wv9)
- [[Refrain] Trigun - 13 VF VOSTFR [540p]](https://uptobox.com/gvujyc3qf2do)
- [[Refrain] Trigun - 14 VF VOSTFR [540p]](https://uptobox.com/njqz754eg2q7)
- [[Refrain] Trigun - 15 VF VOSTFR [540p]](https://uptobox.com/vqo8g1gcxufp)
- [[Refrain] Trigun - 16 VF VOSTFR [540p]](https://uptobox.com/9hnzsxr5eqip)
- [[Refrain] Trigun - 17 VF VOSTFR [540p]](https://uptobox.com/9s6s2o1s1ke1)
- [[Refrain] Trigun - 18 VF VOSTFR [540p]](https://uptobox.com/732iml0womlp)
- [[Refrain] Trigun - 19 VF VOSTFR [540p]](https://uptobox.com/dnpo0oiudoix)
- [[Refrain] Trigun - 20 VF VOSTFR [540p]](https://uptobox.com/myku9lmjm7ld)
- [[Refrain] Trigun - 21 VF VOSTFR [540p]](https://uptobox.com/x9854xas03lt)
- [[Refrain] Trigun - 22 VF VOSTFR [540p]](https://uptobox.com/6toudn8htr96)
- [[Refrain] Trigun - 23 VF VOSTFR [540p]](https://uptobox.com/moi12i9qt3q6)
- [[Refrain] Trigun - 24 VF VOSTFR [540p]](https://uptobox.com/kuhmqlb1il9m)
- [[Refrain] Trigun - 25 VF VOSTFR [540p]](https://uptobox.com/bpre90lyd0jk)
- [[Refrain] Trigun - 26 VF VOSTFR [540p]](https://uptobox.com/761n5y512a8e)
