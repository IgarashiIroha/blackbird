# Zero kara Hajimeru Mahou no Sho

![Zero kara Hajimeru Mahou no Sho](https://cdn.myanimelist.net/images/anime/5/85224l.jpg)

* Japanese:  ゼロから始める魔法の書

## Information

 - Type: TV
 - Episodes: 12
 - Status: Finished Airing
 - Aired: Apr 10, 2017 to Jun 26, 2017
 - Premiered: Spring 2017
 - Broadcast: Mondays at 23:30 (JST)
 - Producers: Lantis, Half H.P Studio, Happinet Pictures, Infinite 
 - Licensors: Sentai Filmworks 
 - Studios: White Fox 
 - Source: Light novel
 - Genres: Action, Fantasy
 - Duration: 24 min. per ep.
 - Rating: PG-13 - Teens 13 or older


## Links

