# Oushitsu Kyoushi Heine

![Oushitsu Kyoushi Heine](https://cdn.myanimelist.net/images/anime/11/85517l.jpg)

* Japanese:  王室教師ハイネ

## Information

 - Type: TV
 - Episodes: 12
 - Status: Finished Airing
 - Aired: Apr 5, 2017 to Jun 21, 2017
 - Premiered: Spring 2017
 - Broadcast: Wednesdays at 02:05 (JST)
 - Producers: TV Tokyo, Square Enix, Sotsu, TV Osaka, AT-X, DAX Production, TV Tokyo Music, Avex Pictures, BS Japan, Dai Nippon Printing, Contents Seed, A-Sketch 
 - Licensors: Funimation 
 - Studios: Bridge 
 - Source: Manga
 - Genres: Slice of Life, Comedy, Historical, Shounen
 - Duration: 23 min. per ep.
 - Rating: PG-13 - Teens 13 or older


## Links

- [[Refrain] Oushitsu Kyoushi Heine Movie - 00 VOSTFR [720p]](https://uptobox.com/gzqxfqglnob8)
- [[Refrain] Oushitsu Kyoushi Heine Movie - 00 VOSTFR [720p]](https://uptobox.com/5t4kb7pqhg3w)
