# Room Mate

![Room Mate](https://cdn.myanimelist.net/images/anime/10/85196l.jpg)

* Japanese:  Room Mate

## Information

 - Type: TV
 - Episodes: 12
 - Status: Finished Airing
 - Aired: Apr 12, 2017 to Jun 28, 2017
 - Premiered: Spring 2017
 - Broadcast: Wednesdays at 22:40 (JST)
 - Producers: Smiral Animation 
 - Licensors: None found, add some 
 - Studios: Typhoon Graphics 
 - Source: Original
 - Genres: Harem, Slice of Life
 - Duration: 4 min. per ep.
 - Rating: PG-13 - Teens 13 or older


## Links

