# Granblue Fantasy The Animation

![Granblue Fantasy The Animation](https://cdn.myanimelist.net/images/anime/2/81630l.jpg)

* Japanese:  GRANBLUE FANTASY The Animation

## Information

 - Type: TV
 - Episodes: 13
 - Status: Finished Airing
 - Aired: Apr 2, 2017 to Jun 25, 2017
 - Premiered: Spring 2017
 - Broadcast: Sundays at 00:00 (JST)
 - Producers: Aniplex, Tohokushinsha Film Corporation, Cygames 
 - Licensors: Aniplex of America 
 - Studios: A-1 Pictures 
 - Source: Game
 - Genres: Adventure, Fantasy
 - Duration: 23 min. per ep.
 - Rating: PG-13 - Teens 13 or older


## Links

