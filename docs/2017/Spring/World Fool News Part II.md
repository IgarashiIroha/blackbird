# World Fool News (TV) Part II

![World Fool News (TV) Part II](https://cdn.myanimelist.net/images/anime/4/85654l.jpg)

* Japanese:  ワールドフールニュース PartⅡ

## Information

 - Type: TV
 - Episodes: 12
 - Status: Finished Airing
 - Aired: Apr 2, 2017 to Jun 18, 2017
 - Premiered: Spring 2017
 - Broadcast: Sundays at 18:45 (JST)
 - Producers: CoMix Wave Films 
 - Licensors: None found, add some 
 - Studios: None found, add some 
 - Source: Other
 - Genres: Slice of Life, Comedy
 - Duration: 9 min. per ep.
 - Rating: G - All Ages


## Links

