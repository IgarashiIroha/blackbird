# Beyblade Burst God

![Beyblade Burst God](https://cdn.myanimelist.net/images/anime/12/84704l.jpg)

* Japanese:  ベイブレードバースト 神〈ゴッド〉

## Information

 - Type: TV
 - Episodes: 51
 - Status: Finished Airing
 - Aired: Apr 3, 2017 to Mar 26, 2018
 - Premiered: Spring 2017
 - Broadcast: Mondays at 17:55 (JST)
 - Producers: TV Tokyo, d-rights 
 - Licensors: None found, add some 
 - Studios: OLM 
 - Source: Original
 - Genres: Action, Sci-Fi, Adventure, Sports, Shounen
 - Duration: 24 min. per ep.
 - Rating: PG - Children


## Links

