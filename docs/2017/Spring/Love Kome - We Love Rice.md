# Love Kome: We Love Rice

![Love Kome: We Love Rice](https://cdn.myanimelist.net/images/anime/5/84377l.jpg)

* Japanese:  ラブ米 -WE LOVE RICE-

## Information

 - Type: TV
 - Episodes: 12
 - Status: Finished Airing
 - Aired: Apr 5, 2017 to Jun 21, 2017
 - Premiered: Spring 2017
 - Broadcast: Wednesdays at 22:50 (JST)
 - Producers: Just Production, Kanetsu Co., LTD. 
 - Licensors: None found, add some 
 - Studios: Encourage Films 
 - Source: Original
 - Genres: Slice of Life
 - Duration: 4 min. per ep.
 - Rating: PG-13 - Teens 13 or older


## Links

