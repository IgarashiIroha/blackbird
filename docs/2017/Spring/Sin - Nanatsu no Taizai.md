# Sin: Nanatsu no Taizai

![Sin: Nanatsu no Taizai](https://cdn.myanimelist.net/images/anime/7/86621l.jpg)

* Japanese:  sin 七つの大罪

## Information

 - Type: TV
 - Episodes: 12
 - Status: Finished Airing
 - Aired: Apr 15, 2017 to Jul 29, 2017
 - Premiered: Spring 2017
 - Broadcast: Saturdays at 00:00 (JST)
 - Producers: Genco, Lantis, AT-X, Hobby Japan, NBCUniversal Entertainment Japan, Kanon Sound, Orchid Seed 
 - Licensors: Funimation 
 - Studios: Artland, TNK 
 - Source: Other
 - Genres: Ecchi, Fantasy
 - Duration: 23 min. per ep.
 - Rating: R+ - Mild Nudity


## Links

