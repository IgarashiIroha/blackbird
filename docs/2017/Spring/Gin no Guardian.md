# Gin no Guardian

![Gin no Guardian](https://cdn.myanimelist.net/images/anime/8/86525l.jpg)

* Japanese:  銀の墓守り〈ガーディアン〉

## Information

 - Type: TV
 - Episodes: 12
 - Status: Finished Airing
 - Aired: Apr 1, 2017 to Jun 17, 2017
 - Premiered: Spring 2017
 - Broadcast: Saturdays at 21:00 (JST)
 - Producers: Emon 
 - Licensors: Funimation 
 - Studios: Haoliners Animation League 
 - Source: Web manga
 - Genres: Adventure, Fantasy
 - Duration: 13 min. per ep.
 - Rating: PG-13 - Teens 13 or older


## Links

