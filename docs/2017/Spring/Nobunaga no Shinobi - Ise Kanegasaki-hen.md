# Nobunaga no Shinobi: Ise Kanegasaki-hen

![Nobunaga no Shinobi: Ise Kanegasaki-hen](https://cdn.myanimelist.net/images/anime/3/84890l.jpg)

* Japanese:  信長の忍び~伊勢・金ヶ崎篇~

## Information

 - Type: TV
 - Episodes: 26
 - Status: Finished Airing
 - Aired: Apr 8, 2017 to Sep 30, 2017
 - Premiered: Spring 2017
 - Broadcast: Saturdays at 01:35 (JST)
 - Producers: None found, add some 
 - Licensors: None found, add some 
 - Studios: TMS Entertainment 
 - Source: 4-koma manga
 - Genres: Comedy, Historical
 - Duration: 3 min. per ep.
 - Rating: PG - Children


## Links

