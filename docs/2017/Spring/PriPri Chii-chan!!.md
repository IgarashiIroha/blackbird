# PriPri Chii-chan!!

![PriPri Chii-chan!!](https://cdn.myanimelist.net/images/anime/10/83963l.jpg)

* Japanese:  プリプリちぃちゃん!!

## Information

 - Type: TV
 - Episodes: 36
 - Status: Finished Airing
 - Aired: Apr 15, 2017 to Dec 16, 2017
 - Premiered: Spring 2017
 - Broadcast: Saturdays at 06:30 (JST)
 - Producers: None found, add some 
 - Licensors: None found, add some 
 - Studios: OLM 
 - Source: Manga
 - Genres: Sci-Fi, Comedy, Shoujo
 - Duration: 12 min. per ep.
 - Rating: G - All Ages


## Links

