# Busou Shoujo Machiavellianism

![Busou Shoujo Machiavellianism](https://cdn.myanimelist.net/images/anime/3/83995l.jpg)

* Japanese:  武装少女マキャヴェリズム

## Information

 - Type: TV
 - Episodes: 12
 - Status: Finished Airing
 - Aired: Apr 5, 2017 to Jun 21, 2017
 - Premiered: Spring 2017
 - Broadcast: Wednesdays at 23:00 (JST)
 - Producers: Media Factory, AT-X, Nippon Columbia, KlockWorx, Glovision, Kadokawa Media House, Toshiba Digital Frontiers, Tsukuru no Mori 
 - Licensors: Sentai Filmworks 
 - Studios: Silver Link., Connect 
 - Source: Manga
 - Genres: Action, Comedy, School, Shounen
 - Duration: 23 min. per ep.
 - Rating: PG-13 - Teens 13 or older


## Links

