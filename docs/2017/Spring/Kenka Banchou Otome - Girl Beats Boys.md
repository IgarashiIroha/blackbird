# Kenka Banchou Otome: Girl Beats Boys

![Kenka Banchou Otome: Girl Beats Boys](https://cdn.myanimelist.net/images/anime/9/85022l.jpg)

* Japanese:  喧嘩番長 乙女 -GIRL BEATS BOYS-

## Information

 - Type: TV
 - Episodes: 12
 - Status: Finished Airing
 - Aired: Apr 12, 2017 to Jun 28, 2017
 - Premiered: Spring 2017
 - Broadcast: Wednesdays at 22:30 (JST)
 - Producers: Grooove, DMM.com 
 - Licensors: Funimation 
 - Studios: Project No.9, A-Real 
 - Source: Visual novel
 - Genres: Action, Martial Arts, School, Shoujo
 - Duration: 8 min. per ep.
 - Rating: PG-13 - Teens 13 or older


## Links

