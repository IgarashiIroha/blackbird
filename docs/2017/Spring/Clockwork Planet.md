# Clockwork Planet

![Clockwork Planet](https://cdn.myanimelist.net/images/anime/12/85227l.jpg)

* Japanese:  クロックワーク・プラネット

## Information

 - Type: TV
 - Episodes: 12
 - Status: Finished Airing
 - Aired: Apr 7, 2017 to Jun 23, 2017
 - Premiered: Spring 2017
 - Broadcast: Fridays at 01:58 (JST)
 - Producers: TBS, Kodansha, NBCUniversal Entertainment Japan, NichiNare, GYAO! 
 - Licensors: Funimation 
 - Studios: Xebec 
 - Source: Light novel
 - Genres: Fantasy, Sci-Fi
 - Duration: 24 min. per ep.
 - Rating: PG-13 - Teens 13 or older


## Links

