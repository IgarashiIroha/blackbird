# ID-0

![ID-0](https://cdn.myanimelist.net/images/anime/12/84421l.jpg)

* Japanese:  ID-0

## Information

 - Type: TV
 - Episodes: 12
 - Status: Finished Airing
 - Aired: Apr 9, 2017 to Jun 25, 2017
 - Premiered: Spring 2017
 - Broadcast: Sundays at 23:00 (JST)
 - Producers: Bandai Visual, Lantis, Ultra Super Pictures, World Cosplay Summit 
 - Licensors: None found, add some 
 - Studios: SANZIGEN 
 - Source: Original
 - Genres: Sci-Fi, Space, Mecha
 - Duration: 24 min. per ep.
 - Rating: PG-13 - Teens 13 or older


## Links

