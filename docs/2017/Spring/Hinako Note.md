# Hinako Note

![Hinako Note](https://cdn.myanimelist.net/images/anime/2/84858l.jpg)

* Japanese:  ひなこのーと

## Information

 - Type: TV
 - Episodes: 12
 - Status: Finished Airing
 - Aired: Apr 7, 2017 to Jun 23, 2017
 - Premiered: Spring 2017
 - Broadcast: Fridays at 21:00 (JST)
 - Producers: Media Factory, AT-X, Enterbrain, Sony Music Communications 
 - Licensors: None found, add some 
 - Studios: Passione 
 - Source: 4-koma manga
 - Genres: Slice of Life, Comedy
 - Duration: 23 min. per ep.
 - Rating: PG-13 - Teens 13 or older


## Links

