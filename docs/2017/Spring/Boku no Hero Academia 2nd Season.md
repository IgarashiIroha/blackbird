# Boku no Hero Academia 2nd Season

![Boku no Hero Academia 2nd Season](https://cdn.myanimelist.net/images/anime/12/85221l.jpg)

* Japanese:  僕のヒーローアカデミア 2ndシーズン

## Information

 - Type: TV
 - Episodes: 25
 - Status: Finished Airing
 - Aired: Apr 1, 2017 to Sep 30, 2017
 - Premiered: Spring 2017
 - Broadcast: Saturdays at 17:30 (JST)
 - Producers: Dentsu, Yomiuri Telecasting, Movic, Sony Music Entertainment, TOHO animation, Shueisha 
 - Licensors: Funimation 
 - Studios: Bones 
 - Source: Manga
 - Genres: Action, Comedy, Super Power, School, Shounen
 - Duration: 23 min. per ep.
 - Rating: PG-13 - Teens 13 or older


## Links

