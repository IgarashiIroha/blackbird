# Ani ni Tsukeru Kusuri wa Nai!

![Ani ni Tsukeru Kusuri wa Nai!](https://cdn.myanimelist.net/images/anime/4/85295l.jpg)

* Japanese:  兄に付ける薬はない!

## Information

 - Type: TV
 - Episodes: 12
 - Status: Finished Airing
 - Aired: Apr 7, 2017 to Jun 23, 2017
 - Premiered: Spring 2017
 - Broadcast: Fridays at 21:55 (JST)
 - Producers: bilibili, Tencent Penguin Pictures 
 - Licensors: None found, add some 
 - Studios: Fanworks, Imagineer 
 - Source: Web manga
 - Genres: Slice of Life, Comedy, School
 - Duration: 3 min. per ep.
 - Rating: PG - Children


## Links

