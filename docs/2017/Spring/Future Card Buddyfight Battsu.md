# Future Card Buddyfight Battsu

![Future Card Buddyfight Battsu](https://cdn.myanimelist.net/images/anime/6/84792l.jpg)

* Japanese:  フューチャーカード バディファイトX

## Information

 - Type: TV
 - Episodes: 52
 - Status: Finished Airing
 - Aired: Apr 1, 2017 to Mar 31, 2018
 - Premiered: Spring 2017
 - Broadcast: Saturdays at 08:00 (JST)
 - Producers: Dentsu, TV Aichi 
 - Licensors: None found, add some 
 - Studios: Xebec, OLM 
 - Source: Card game
 - Genres: Game
 - Duration: 23 min. per ep.
 - Rating: PG - Children


## Links

