# Starmyu 2nd Season

![Starmyu 2nd Season](https://cdn.myanimelist.net/images/anime/13/84655l.jpg)

* Japanese:  高校星歌劇［スタミュ］

## Information

 - Type: TV
 - Episodes: 12
 - Status: Finished Airing
 - Aired: Apr 4, 2017 to Jun 20, 2017
 - Premiered: Spring 2017
 - Broadcast: Tuesdays at 00:00 (JST)
 - Producers: NBCUniversal Entertainment Japan 
 - Licensors: None found, add some 
 - Studios: C-Station 
 - Source: Original
 - Genres: Music, Slice of Life, School
 - Duration: 24 min. per ep.
 - Rating: PG-13 - Teens 13 or older


## Links

