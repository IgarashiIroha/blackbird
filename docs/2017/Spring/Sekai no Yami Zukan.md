# Sekai no Yami Zukan

![Sekai no Yami Zukan](https://cdn.myanimelist.net/images/anime/5/84611l.jpg)

* Japanese:  世界の闇図鑑

## Information

 - Type: TV
 - Episodes: 13
 - Status: Finished Airing
 - Aired: Apr 3, 2017 to Jun 26, 2017
 - Premiered: Spring 2017
 - Broadcast: Mondays at 03:35 (JST)
 - Producers: TV Tokyo 
 - Licensors: None found, add some 
 - Studios: ILCA 
 - Source: Original
 - Genres: Horror, Supernatural
 - Duration: 4 min. per ep.
 - Rating: PG - Children


## Links

