# Shounen Ashibe: Go! Go! Goma-chan 2

![Shounen Ashibe: Go! Go! Goma-chan 2](https://cdn.myanimelist.net/images/anime/6/89580l.jpg)

* Japanese:  少年アシベ GO！GO！ゴマちゃん

## Information

 - Type: TV
 - Episodes: 32
 - Status: Finished Airing
 - Aired: Apr 4, 2017 to Feb 13, 2018
 - Premiered: Spring 2017
 - Broadcast: Unknown
 - Producers: None found, add some 
 - Licensors: None found, add some 
 - Studios: Bridge, Husio Studio 
 - Source: 4-koma manga
 - Genres: Slice of Life, Comedy, Seinen
 - Duration: 9 min. per ep.
 - Rating: G - All Ages


## Links

