# Sakurada Reset

![Sakurada Reset](https://cdn.myanimelist.net/images/anime/5/84470l.jpg)

* Japanese:  サクラダリセット

## Information

 - Type: TV
 - Episodes: 24
 - Status: Finished Airing
 - Aired: Apr 5, 2017 to Sep 13, 2017
 - Premiered: Spring 2017
 - Broadcast: Wednesdays at 23:30 (JST)
 - Producers: Media Factory, flying DOG, Asmik Ace, Showgate, Teichiku Entertainment, Hakuhodo DY Music & Pictures, Kansai Telecasting, BS11, Sony PCL, Radio Osaka, A-Sketch, Parco, e-notion 
 - Licensors: Sentai Filmworks 
 - Studios: David Production 
 - Source: Light novel
 - Genres: Mystery, Super Power, Supernatural, School
 - Duration: 25 min. per ep.
 - Rating: PG-13 - Teens 13 or older


## Links

