# 100% Pascal-sensei (TV)

![100% Pascal-sensei (TV)](https://cdn.myanimelist.net/images/anime/10/86858l.jpg)

* Japanese:  100%パスカル先生 (TV)

## Information

 - Type: TV
 - Episodes: 36
 - Status: Finished Airing
 - Aired: Apr 15, 2017 to Dec 16, 2017
 - Premiered: Spring 2017
 - Broadcast: Saturdays at 06:30 (JST)
 - Producers: Dentsu, Shogakukan Productions, Asatsu DK, Mainichi Broadcasting System, Nippon Columbia, Shogakukan, Sumitomo 
 - Licensors: None found, add some 
 - Studios: OLM 
 - Source: Manga
 - Genres: Comedy
 - Duration: 12 min. per ep.
 - Rating: PG - Children


## Links

