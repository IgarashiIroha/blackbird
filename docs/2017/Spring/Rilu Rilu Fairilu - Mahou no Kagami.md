# Rilu Rilu Fairilu: Mahou no Kagami

![Rilu Rilu Fairilu: Mahou no Kagami](https://cdn.myanimelist.net/images/anime/13/84497l.jpg)

* Japanese:  リルリルフェアリル ~魔法の鏡~

## Information

 - Type: TV
 - Episodes: 51
 - Status: Finished Airing
 - Aired: Apr 7, 2017 to Mar 30, 2018
 - Premiered: Spring 2017
 - Broadcast: Fridays at 17:55 (JST)
 - Producers: None found, add some 
 - Licensors: None found, add some 
 - Studios: Studio Deen 
 - Source: Original
 - Genres: Slice of Life, Magic, Fantasy
 - Duration: 23 min. per ep.
 - Rating: G - All Ages


## Links

