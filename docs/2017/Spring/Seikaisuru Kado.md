# Seikaisuru Kado

![Seikaisuru Kado](https://cdn.myanimelist.net/images/anime/3/85255l.jpg)

* Japanese:  正解するカド

## Information

 - Type: TV
 - Episodes: 12
 - Status: Finished Airing
 - Aired: Apr 7, 2017 to Jun 30, 2017
 - Premiered: Spring 2017
 - Broadcast: Fridays at 22:30 (JST)
 - Producers: Kinoshita Group Holdings 
 - Licensors: Funimation 
 - Studios: Toei Animation 
 - Source: Original
 - Genres: Sci-Fi
 - Duration: 23 min. per ep.
 - Rating: PG-13 - Teens 13 or older


## Links

