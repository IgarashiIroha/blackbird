# Berserk 2nd Season

![Berserk 2nd Season](https://cdn.myanimelist.net/images/anime/12/85296l.jpg)

* Japanese:  ベルセルク

## Information

 - Type: TV
 - Episodes: 12
 - Status: Finished Airing
 - Aired: Apr 7, 2017 to Jun 23, 2017
 - Premiered: Spring 2017
 - Broadcast: Fridays at 22:30 (JST)
 - Producers: WOWOW, Mainichi Broadcasting System, Hakusensha, Ultra Super Pictures, LIDENFILMS, NBCUniversal Entertainment Japan, Lucent Pictures Entertainment, Docomo Anime Store, Koei Tecmo Games 
 - Licensors: Funimation, Crunchyroll 
 - Studios: Millepensee, GEMBA 
 - Source: Manga
 - Genres: Action, Adventure, Demons, Drama, Fantasy, Horror, Magic, Military, Romance, Supernatural
 - Duration: 24 min. per ep.
 - Rating: R - 17+ (violence & profanity)


## Links

