# Shingeki no Bahamut: Virgin Soul

![Shingeki no Bahamut: Virgin Soul](https://cdn.myanimelist.net/images/anime/13/85564l.jpg)

* Japanese:  神撃のバハムート VIRGIN SOUL

## Information

 - Type: TV
 - Episodes: 24
 - Status: Finished Airing
 - Aired: Apr 8, 2017 to Sep 30, 2017
 - Premiered: Spring 2017
 - Broadcast: Saturdays at 01:55 (JST)
 - Producers: Cygames 
 - Licensors: None found, add some 
 - Studios: MAPPA 
 - Source: Game
 - Genres: Action, Adventure, Demons, Supernatural, Magic, Fantasy
 - Duration: 23 min. per ep.
 - Rating: R - 17+ (violence & profanity)


## Links

