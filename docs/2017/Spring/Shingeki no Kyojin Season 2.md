# Shingeki no Kyojin Season 2

![Shingeki no Kyojin Season 2](https://cdn.myanimelist.net/images/anime/4/84177l.jpg)

* Japanese:  進撃の巨人 Season2

## Information

 - Type: TV
 - Episodes: 12
 - Status: Finished Airing
 - Aired: Apr 1, 2017 to Jun 17, 2017
 - Premiered: Spring 2017
 - Broadcast: Saturdays at 22:00 (JST)
 - Producers: Production I.G, Dentsu, Mainichi Broadcasting System, Pony Canyon, Kodansha, Techno Sound, Pony Canyon Enterprise 
 - Licensors: Funimation 
 - Studios: Wit Studio 
 - Source: Manga
 - Genres: Action, Drama, Fantasy, Military, Mystery, Shounen, Super Power
 - Duration: 24 min. per ep.
 - Rating: R - 17+ (violence & profanity)


## Links

