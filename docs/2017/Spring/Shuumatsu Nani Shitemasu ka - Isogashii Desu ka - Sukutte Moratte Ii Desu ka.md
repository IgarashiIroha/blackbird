# Shuumatsu Nani Shitemasu ka? Isogashii Desu ka? Sukutte Moratte Ii Desu ka?

![Shuumatsu Nani Shitemasu ka? Isogashii Desu ka? Sukutte Moratte Ii Desu ka?](https://cdn.myanimelist.net/images/anime/4/85260l.jpg)

* Japanese:  終末なにしてますか？忙しいですか？救ってもらっていいですか？

## Information

 - Type: TV
 - Episodes: 12
 - Status: Finished Airing
 - Aired: Apr 11, 2017 to Jun 27, 2017
 - Premiered: Spring 2017
 - Broadcast: Tuesdays at 23:30 (JST)
 - Producers: Lantis, Kadokawa Shoten, AT-X, DAX Production, KlockWorx, Kadokawa Media House, Production Ace 
 - Licensors: Funimation 
 - Studios: Satelight, C2C 
 - Source: Light novel
 - Genres: Sci-Fi, Drama, Romance, Fantasy
 - Duration: 24 min. per ep.
 - Rating: PG-13 - Teens 13 or older


## Links

