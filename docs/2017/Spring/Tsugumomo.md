# Tsugumomo

![Tsugumomo](https://cdn.myanimelist.net/images/anime/13/84461l.jpg)

* Japanese:  つぐもも

## Information

 - Type: TV
 - Episodes: 12
 - Status: Finished Airing
 - Aired: Apr 2, 2017 to Jun 18, 2017
 - Premiered: Spring 2017
 - Broadcast: Sundays at 23:30 (JST)
 - Producers: Animax, Toei Video, Pony Canyon, Studio NOIX, Hakuhodo DY Music & Pictures, Futabasha, Hakuhodo, Kakao Japan, My Theater D.D. 
 - Licensors: Funimation 
 - Studios: Zero-G 
 - Source: Manga
 - Genres: Action, Comedy, Supernatural, Ecchi, School, Seinen
 - Duration: 24 min. per ep.
 - Rating: R+ - Mild Nudity


## Links

