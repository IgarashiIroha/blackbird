# Twin Angel Break

![Twin Angel Break](https://cdn.myanimelist.net/images/anime/11/86505l.jpg)

* Japanese:  ツインエンジェルBREAK

## Information

 - Type: TV
 - Episodes: 12
 - Status: Finished Airing
 - Aired: Apr 7, 2017 to Jun 23, 2017
 - Premiered: Spring 2017
 - Broadcast: Fridays at 22:00 (JST)
 - Producers: Media Factory, Sammy 
 - Licensors: None found, add some 
 - Studios: J.C.Staff 
 - Source: Game
 - Genres: Drama, Magic
 - Duration: 23 min. per ep.
 - Rating: PG-13 - Teens 13 or older


## Links

