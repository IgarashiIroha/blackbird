# Kabuki-bu!

![Kabuki-bu!](https://cdn.myanimelist.net/images/anime/4/85291l.jpg)

* Japanese:  カブキブ！

## Information

 - Type: TV
 - Episodes: 12
 - Status: Finished Airing
 - Aired: Apr 7, 2017 to Jun 23, 2017
 - Premiered: Spring 2017
 - Broadcast: Fridays at 02:28 (JST)
 - Producers: Shochiku, Kadokawa Shoten, Pony Canyon, TBS 
 - Licensors: Sentai Filmworks 
 - Studios: Studio Deen 
 - Source: Novel
 - Genres: School
 - Duration: 24 min. per ep.
 - Rating: PG-13 - Teens 13 or older


## Links

