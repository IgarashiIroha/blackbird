# Atom: The Beginning

![Atom: The Beginning](https://cdn.myanimelist.net/images/anime/6/86607l.jpg)

* Japanese:  アトム ザ・ビギニング

## Information

 - Type: TV
 - Episodes: 12
 - Status: Finished Airing
 - Aired: Apr 15, 2017 to Jul 8, 2017
 - Premiered: Spring 2017
 - Broadcast: Saturdays at 23:00 (JST)
 - Producers: NHK, Tezuka Productions, Studio Mausu, NBCUniversal Entertainment Japan, Hakuhodo DY Music & Pictures, Fields 
 - Licensors: Sentai Filmworks 
 - Studios: Production I.G, OLM, Signal.MD 
 - Source: Manga
 - Genres: Action, Sci-Fi, Mecha, Seinen
 - Duration: 25 min. per ep.
 - Rating: PG-13 - Teens 13 or older


## Links

