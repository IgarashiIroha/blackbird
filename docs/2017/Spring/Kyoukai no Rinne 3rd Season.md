# Kyoukai no Rinne 3rd Season

![Kyoukai no Rinne 3rd Season](https://cdn.myanimelist.net/images/anime/13/85289l.jpg)

* Japanese:  境界のRINNE 第3シリーズ

## Information

 - Type: TV
 - Episodes: 25
 - Status: Finished Airing
 - Aired: Apr 8, 2017 to Sep 23, 2017
 - Premiered: Spring 2017
 - Broadcast: Saturdays at 17:35 (JST)
 - Producers: Shogakukan Productions, NHK, Shogakukan 
 - Licensors: Sentai Filmworks 
 - Studios: Brain&#039;s Base 
 - Source: Manga
 - Genres: Comedy, Romance, School, Shounen, Supernatural
 - Duration: 24 min. per ep.
 - Rating: PG-13 - Teens 13 or older


## Links

