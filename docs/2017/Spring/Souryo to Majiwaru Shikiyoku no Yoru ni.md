# Souryo to Majiwaru Shikiyoku no Yoru ni...

![Souryo to Majiwaru Shikiyoku no Yoru ni...](https://cdn.myanimelist.net/images/anime/9/86606l.jpg)

* Japanese:  僧侶と交わる色欲の夜に…

## Information

 - Type: TV
 - Episodes: 12
 - Status: Finished Airing
 - Aired: Apr 3, 2017 to Jun 19, 2017
 - Premiered: Spring 2017
 - Broadcast: Mondays at 01:00 (JST)
 - Producers: Kujou-kun no Bonnou wo Mimamoru-kai 
 - Licensors: None found, add some 
 - Studios: Seven 
 - Source: Manga
 - Genres: Drama, Romance, Ecchi
 - Duration: 3 min. per ep.
 - Rating: R+ - Mild Nudity


## Links

