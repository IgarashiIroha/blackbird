# Warau Salesman New

![Warau Salesman New](https://cdn.myanimelist.net/images/anime/3/85132l.jpg)

* Japanese:  笑ゥせぇるすまんNEW

## Information

 - Type: TV
 - Episodes: 12
 - Status: Finished Airing
 - Aired: Apr 3, 2017 to Jun 19, 2017
 - Premiered: Spring 2017
 - Broadcast: Mondays at 23:00 (JST)
 - Producers: TMS Entertainment, Nippon Columbia, Shogakukan Music & Digital Entertainment 
 - Licensors: None found, add some 
 - Studios: Shin-Ei Animation 
 - Source: Manga
 - Genres: Comedy, Drama, Seinen
 - Duration: 23 min. per ep.
 - Rating: PG-13 - Teens 13 or older


## Links

