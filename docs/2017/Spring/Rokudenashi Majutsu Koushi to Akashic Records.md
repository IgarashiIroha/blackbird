# Rokudenashi Majutsu Koushi to Akashic Records

![Rokudenashi Majutsu Koushi to Akashic Records](https://cdn.myanimelist.net/images/anime/8/85593l.jpg)

* Japanese:  ロクでなし魔術講師と禁忌教典

## Information

 - Type: TV
 - Episodes: 12
 - Status: Finished Airing
 - Aired: Apr 4, 2017 to Jun 20, 2017
 - Premiered: Spring 2017
 - Broadcast: Tuesdays at 20:30 (JST)
 - Producers: Media Factory, AT-X, Kadokawa Pictures Japan, Ultra Super Pictures, Tohokushinsha Film Corporation, Sony Music Communications, Fujimi Shobo, Kanetsu Co., LTD. 
 - Licensors: Funimation 
 - Studios: LIDENFILMS 
 - Source: Light novel
 - Genres: Action, Magic, Fantasy, School
 - Duration: 24 min. per ep.
 - Rating: R - 17+ (violence & profanity)


## Links

