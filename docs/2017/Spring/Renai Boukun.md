# Renai Boukun

![Renai Boukun](https://cdn.myanimelist.net/images/anime/9/84266l.jpg)

* Japanese:  恋愛暴君

## Information

 - Type: TV
 - Episodes: 12
 - Status: Finished Airing
 - Aired: Apr 7, 2017 to Jun 23, 2017
 - Premiered: Spring 2017
 - Broadcast: Fridays at 02:35 (JST)
 - Producers: TV Tokyo, Half H.P Studio, DIVE II Entertainment, Avex Pictures 
 - Licensors: Funimation 
 - Studios: EMT² 
 - Source: Web manga
 - Genres: Harem, Comedy, Supernatural, Romance, Ecchi, School
 - Duration: 23 min. per ep.
 - Rating: PG-13 - Teens 13 or older


## Links

