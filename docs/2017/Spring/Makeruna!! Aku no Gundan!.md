# Makeruna!! Aku no Gundan!

![Makeruna!! Aku no Gundan!](https://cdn.myanimelist.net/images/anime/8/84731l.jpg)

* Japanese:  まけるな!! あくのぐんだん！

## Information

 - Type: TV
 - Episodes: 12
 - Status: Finished Airing
 - Aired: Apr 5, 2017 to Jun 21, 2017
 - Premiered: Spring 2017
 - Broadcast: Wednesdays at 22:45 (JST)
 - Producers: Bushiroad 
 - Licensors: None found, add some 
 - Studios: Tatsunoko Production 
 - Source: Manga
 - Genres: Comedy, Shounen
 - Duration: 5 min. per ep.
 - Rating: G - All Ages


## Links

