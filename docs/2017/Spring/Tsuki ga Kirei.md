# Tsuki ga Kirei

![Tsuki ga Kirei](https://cdn.myanimelist.net/images/anime/2/85592l.jpg)

* Japanese:  月がきれい

## Information

 - Type: TV
 - Episodes: 12
 - Status: Finished Airing
 - Aired: Apr 7, 2017 to Jun 30, 2017
 - Premiered: Spring 2017
 - Broadcast: Fridays at 00:00 (JST)
 - Producers: Sotsu, Mainichi Broadcasting System, flying DOG 
 - Licensors: Funimation 
 - Studios: feel. 
 - Source: Original
 - Genres: Romance, School
 - Duration: 24 min. per ep.
 - Rating: PG-13 - Teens 13 or older


## Links

