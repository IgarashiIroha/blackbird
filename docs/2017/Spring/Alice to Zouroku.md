# Alice to Zouroku

![Alice to Zouroku](https://cdn.myanimelist.net/images/anime/2/84598l.jpg)

* Japanese:  アリスと蔵六

## Information

 - Type: TV
 - Episodes: 12
 - Status: Finished Airing
 - Aired: Apr 2, 2017 to Jun 25, 2017
 - Premiered: Spring 2017
 - Broadcast: Sundays at 22:30 (JST)
 - Producers: Bandai Visual, Lantis, Tokuma Shoten, Tokyo MX, Hakuhodo DY Music & Pictures, Sony PCL 
 - Licensors: Funimation 
 - Studios: J.C.Staff 
 - Source: Manga
 - Genres: Adventure, Mystery, Seinen
 - Duration: 23 min. per ep.
 - Rating: PG-13 - Teens 13 or older


## Links

