# Idol Time PriPara

![Idol Time PriPara](https://cdn.myanimelist.net/images/anime/3/86056l.jpg)

* Japanese:  アイドルタイムプリパラ

## Information

 - Type: TV
 - Episodes: 51
 - Status: Finished Airing
 - Aired: Apr 4, 2017 to Mar 27, 2018
 - Premiered: Spring 2017
 - Broadcast: Tuesdays at 17:55 (JST)
 - Producers: TV Tokyo, Nihon Ad Systems, Avex Pictures, Takara Tomy A.R.T.S, Shogakukan 
 - Licensors: None found, add some 
 - Studios: Tatsunoko Production, Dongwoo A&E 
 - Source: Game
 - Genres: Music, School, Shoujo
 - Duration: 24 min. per ep.
 - Rating: PG - Children


## Links

