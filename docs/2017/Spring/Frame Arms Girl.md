# Frame Arms Girl

![Frame Arms Girl](https://cdn.myanimelist.net/images/anime/5/86625l.jpg)

* Japanese:  フレームアームズ・ガール

## Information

 - Type: TV
 - Episodes: 12
 - Status: Finished Airing
 - Aired: Apr 4, 2017 to Jun 20, 2017
 - Premiered: Spring 2017
 - Broadcast: Tuesdays at 01:05 (JST)
 - Producers: Frontier Works, Pony Canyon, Nippon Columbia, Bandai Namco Games, Kotobukiya 
 - Licensors: Sentai Filmworks 
 - Studios: Zexcs, Studio A-CAT 
 - Source: Other
 - Genres: Action, Sci-Fi, Slice of Life, Mecha
 - Duration: 23 min. per ep.
 - Rating: PG-13 - Teens 13 or older


## Links

