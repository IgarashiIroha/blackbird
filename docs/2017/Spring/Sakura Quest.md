# Sakura Quest

![Sakura Quest](https://cdn.myanimelist.net/images/anime/6/85222l.jpg)

* Japanese:  サクラクエスト

## Information

 - Type: TV
 - Episodes: 25
 - Status: Finished Airing
 - Aired: Apr 6, 2017 to Sep 21, 2017
 - Premiered: Spring 2017
 - Broadcast: Thursdays at 00:00 (JST)
 - Producers: AT-X, Houbunsha, TOHO animation, BS11, ABC Animation, Sony PCL 
 - Licensors: Funimation 
 - Studios: P.A. Works 
 - Source: Original
 - Genres: Slice of Life, Comedy
 - Duration: 23 min. per ep.
 - Rating: PG-13 - Teens 13 or older


## Links

