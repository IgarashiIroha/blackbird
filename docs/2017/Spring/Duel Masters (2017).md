# Duel Masters (2017)

![Duel Masters (2017)](https://cdn.myanimelist.net/images/anime/12/86604l.jpg)

* Japanese:  デュエル・マスターズ (2017)

## Information

 - Type: TV
 - Episodes: 51
 - Status: Finished Airing
 - Aired: Apr 2, 2017 to Mar 25, 2018
 - Premiered: Spring 2017
 - Broadcast: Sundays at 08:30 (JST)
 - Producers: None found, add some 
 - Licensors: None found, add some 
 - Studios: Ascension 
 - Source: Original
 - Genres: Action, Adventure, Comedy, Shounen
 - Duration: 24 min. per ep.
 - Rating: PG-13 - Teens 13 or older


## Links

