# Yu☆Gi☆Oh! VRAINS

![Yu☆Gi☆Oh! VRAINS](https://cdn.myanimelist.net/images/anime/13/84901l.jpg)

* Japanese:  遊戯王VRAINS

## Information

 - Type: TV
 - Episodes: Unknown
 - Status: Currently Airing
 - Aired: May 10, 2017 to ?
 - Premiered: Spring 2017
 - Broadcast: Wednesdays at 18:25 (JST)
 - Producers: TV Tokyo, Nihon Ad Systems 
 - Licensors: None found, add some 
 - Studios: Studio Gallop 
 - Source: Manga
 - Genres: Action, Fantasy, Game, Sci-Fi, Shounen
 - Duration: 24 min.
 - Rating: PG-13 - Teens 13 or older


## Links

