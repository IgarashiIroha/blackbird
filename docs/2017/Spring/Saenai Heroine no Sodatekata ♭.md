# Saenai Heroine no Sodatekata ♭

![Saenai Heroine no Sodatekata ♭](https://cdn.myanimelist.net/images/anime/2/84797l.jpg)

* Japanese:  冴えない彼女〈ヒロイン〉の育てかた♭

## Information

 - Type: TV
 - Episodes: 11
 - Status: Finished Airing
 - Aired: Apr 14, 2017 to Jun 23, 2017
 - Premiered: Spring 2017
 - Broadcast: Fridays at 00:55 (JST)
 - Producers: Aniplex, Dentsu, Fuji TV, Cospa, Good Smile Company, Kansai Telecasting, Fujimi Shobo 
 - Licensors: None found, add some 
 - Studios: A-1 Pictures 
 - Source: Light novel
 - Genres: Comedy, Ecchi, Harem, Romance, School
 - Duration: 22 min. per ep.
 - Rating: PG-13 - Teens 13 or older


## Links

