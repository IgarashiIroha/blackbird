# Dungeon ni Deai wo Motomeru no wa Machigatteiru Darou ka Gaiden: Sword Oratoria

![Dungeon ni Deai wo Motomeru no wa Machigatteiru Darou ka Gaiden: Sword Oratoria](https://cdn.myanimelist.net/images/anime/7/85467l.jpg)

* Japanese:  ダンジョンに出会いを求めるのは間違っているだろうか外伝 ソード・オラトリア

## Information

 - Type: TV
 - Episodes: 12
 - Status: Finished Airing
 - Aired: Apr 15, 2017 to Jul 1, 2017
 - Premiered: Spring 2017
 - Broadcast: Saturdays at 00:30 (JST)
 - Producers: Genco, Movic, Warner Bros. Japan, KlockWorx, SoftBank Creative, MAGES., Hakuhodo DY Music & Pictures, Furyu 
 - Licensors: Sentai Filmworks 
 - Studios: J.C.Staff 
 - Source: Light novel
 - Genres: Action, Adventure, Fantasy
 - Duration: 25 min. per ep.
 - Rating: PG-13 - Teens 13 or older


## Links

