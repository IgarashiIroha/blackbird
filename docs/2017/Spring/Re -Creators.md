# Re:Creators

![Re:Creators](https://cdn.myanimelist.net/images/anime/11/85469l.jpg)

* Japanese:  Re:CREATORS 〈レクリエイターズ〉

## Information

 - Type: TV
 - Episodes: 22
 - Status: Finished Airing
 - Aired: Apr 8, 2017 to Sep 16, 2017
 - Premiered: Spring 2017
 - Broadcast: Saturdays at 23:30 (JST)
 - Producers: Aniplex, Magic Capsule, Shogakukan, ABC Animation 
 - Licensors: None found, add some 
 - Studios: TROYCA 
 - Source: Original
 - Genres: Action, Sci-Fi, Fantasy
 - Duration: 24 min. per ep.
 - Rating: R - 17+ (violence & profanity)


## Links

