# Cinderella Girls Gekijou

![Cinderella Girls Gekijou](https://cdn.myanimelist.net/images/anime/10/89786l.jpg)

* Japanese:  シンデレラガールズ劇場

## Information

 - Type: TV
 - Episodes: 13
 - Status: Finished Airing
 - Aired: Apr 4, 2017 to Jun 27, 2017
 - Premiered: Spring 2017
 - Broadcast: Tuesdays at 21:55 (JST)
 - Producers: Frontier Works, Half H.P Studio, Nippon Columbia, Bandai Namco Entertainment, Anime Consortium Japan 
 - Licensors: Crunchyroll 
 - Studios: Gathering, Lesprit 
 - Source: 4-koma manga
 - Genres: Comedy, Slice of Life
 - Duration: 3 min. per ep.
 - Rating: PG-13 - Teens 13 or older


## Links

