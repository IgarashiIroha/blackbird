# Fukumenkei Noise

![Fukumenkei Noise](https://cdn.myanimelist.net/images/anime/11/85350l.jpg)

* Japanese:  覆面系ノイズ

## Information

 - Type: TV
 - Episodes: 12
 - Status: Finished Airing
 - Aired: Apr 11, 2017 to Jun 27, 2017
 - Premiered: Spring 2017
 - Broadcast: Tuesdays at 23:00 (JST)
 - Producers: Shogakukan Productions, Nihon Ad Systems, Hakusensha, Warner Bros. Japan, KlockWorx, BS Fuji, Kansai Telecasting 
 - Licensors: Sentai Filmworks 
 - Studios: Brain&#039;s Base 
 - Source: Manga
 - Genres: Music, Romance, Shoujo
 - Duration: 23 min. per ep.
 - Rating: PG-13 - Teens 13 or older


## Links

