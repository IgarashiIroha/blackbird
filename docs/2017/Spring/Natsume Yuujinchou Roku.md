# Natsume Yuujinchou Roku

![Natsume Yuujinchou Roku](https://cdn.myanimelist.net/images/anime/6/84416l.jpg)

* Japanese:  夏目友人帳 陸

## Information

 - Type: TV
 - Episodes: 11
 - Status: Finished Airing
 - Aired: Apr 12, 2017 to Jun 21, 2017
 - Premiered: Spring 2017
 - Broadcast: Wednesdays at 01:35 (JST)
 - Producers: TV Tokyo, Aniplex, Nihon Ad Systems, Hakusensha 
 - Licensors: None found, add some 
 - Studios: Shuka 
 - Source: Manga
 - Genres: Drama, Fantasy, Shoujo, Slice of Life, Supernatural
 - Duration: 23 min. per ep.
 - Rating: PG-13 - Teens 13 or older


## Links

