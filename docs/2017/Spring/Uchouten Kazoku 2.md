# Uchouten Kazoku 2

![Uchouten Kazoku 2](https://cdn.myanimelist.net/images/anime/8/85354l.jpg)

* Japanese:  有頂天家族2

## Information

 - Type: TV
 - Episodes: 12
 - Status: Finished Airing
 - Aired: Apr 9, 2017 to Jun 25, 2017
 - Premiered: Spring 2017
 - Broadcast: Sundays at 22:00 (JST)
 - Producers: Lantis, KBS, Good Smile Company, Docomo Anime Store, Yomiuri TV Enterprise, DMM.com, Cygames 
 - Licensors: None found, add some 
 - Studios: P.A. Works 
 - Source: Novel
 - Genres: Slice of Life, Comedy, Demons, Drama, Fantasy
 - Duration: 23 min. per ep.
 - Rating: PG-13 - Teens 13 or older


## Links

