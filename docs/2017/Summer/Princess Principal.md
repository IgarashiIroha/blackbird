# Princess Principal

![Princess Principal](https://cdn.myanimelist.net/images/anime/7/86768l.jpg)

* Japanese:  プリンセス・プリンシパル

## Information

 - Type: TV
 - Episodes: 12
 - Status: Finished Airing
 - Aired: Jul 9, 2017 to Sep 24, 2017
 - Premiered: Summer 2017
 - Broadcast: Sundays at 23:00 (JST)
 - Producers: Bandai Visual, Lantis, Movic, Tokyo MX, Q-Tec, Hakuhodo DY Music & Pictures, Medicos Entertainment 
 - Licensors: Sentai Filmworks 
 - Studios: Actas, Studio 3Hz 
 - Source: Original
 - Genres: Action, Mystery, Historical
 - Duration: 25 min. per ep.
 - Rating: R - 17+ (violence & profanity)


## Links

