# Shoukoku no Altair

![Shoukoku no Altair](https://cdn.myanimelist.net/images/anime/3/86751l.jpg)

* Japanese:  将国のアルタイル

## Information

 - Type: TV
 - Episodes: 24
 - Status: Finished Airing
 - Aired: Jul 8, 2017 to Dec 23, 2017
 - Premiered: Summer 2017
 - Broadcast: Saturdays at 02:25 (JST)
 - Producers: Aniplex, Mainichi Broadcasting System, Kodansha, Lucent Pictures Entertainment, Hakuhodo DY Music & Pictures, Lawson, GYAO!, Kanetsu Co., LTD. 
 - Licensors: None found, add some 
 - Studios: MAPPA 
 - Source: Manga
 - Genres: Adventure, Historical, Drama, Fantasy, Shounen
 - Duration: 24 min. per ep.
 - Rating: PG-13 - Teens 13 or older


## Links

