# Clione no Akari

![Clione no Akari](https://cdn.myanimelist.net/images/anime/3/84706l.jpg)

* Japanese:  クリオネの灯り

## Information

 - Type: TV
 - Episodes: 12
 - Status: Finished Airing
 - Aired: Jul 12, 2017 to Sep 27, 2017
 - Premiered: Summer 2017
 - Broadcast: Wednesdays at 20:20 (JST)
 - Producers: Kyotoma, Kanetsu Co., LTD. 
 - Licensors: None found, add some 
 - Studios: drop 
 - Source: Novel
 - Genres: Drama, School
 - Duration: 9 min. per ep.
 - Rating: PG - Children


## Links

