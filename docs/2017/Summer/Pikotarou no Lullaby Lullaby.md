# Pikotarou no Lullaby Lullaby (TV)

![Pikotarou no Lullaby Lullaby (TV)](https://cdn.myanimelist.net/images/anime/13/86266l.jpg)

* Japanese:  ピコ太郎のララバイラーラバイ

## Information

 - Type: TV
 - Episodes: 9
 - Status: Finished Airing
 - Aired: Aug 2, 2017 to Sep 27, 2017
 - Premiered: Summer 2017
 - Broadcast: Wednesdays at 22:48 (JST)
 - Producers: TV Tokyo Music, Avex Pictures 
 - Licensors: None found, add some 
 - Studios: DLE 
 - Source: Original
 - Genres: Comedy
 - Duration: 3 min. per ep.
 - Rating: G - All Ages


## Links

