# Vatican Kiseki Chousakan

![Vatican Kiseki Chousakan](https://cdn.myanimelist.net/images/anime/4/86746l.jpg)

* Japanese:  バチカン奇跡調査官

## Information

 - Type: TV
 - Episodes: 12
 - Status: Finished Airing
 - Aired: Jul 7, 2017 to Sep 22, 2017
 - Premiered: Summer 2017
 - Broadcast: Fridays at 22:30 (JST)
 - Producers: WOWOW, Lantis, Media Factory, Kadokawa Shoten, Movic, KBS, KlockWorx, Kadokawa Media House, Kadokawa Daiei Studio 
 - Licensors: Sentai Filmworks 
 - Studios: J.C.Staff 
 - Source: Novel
 - Genres: Mystery, Supernatural, Drama
 - Duration: 24 min. per ep.
 - Rating: R - 17+ (violence & profanity)


## Links

