# Action Heroine Cheer Fruits

![Action Heroine Cheer Fruits](https://cdn.myanimelist.net/images/anime/12/86741l.jpg)

* Japanese:  アクションヒロイン チアフルーツ

## Information

 - Type: TV
 - Episodes: 12
 - Status: Finished Airing
 - Aired: Jul 7, 2017 to Sep 29, 2017
 - Premiered: Summer 2017
 - Broadcast: Fridays at 02:28 (JST)
 - Producers: TBS, Nippon Columbia, KlockWorx, Hobby Japan, Glovision, RAY, Nichion, GYAO!, Production Ace 
 - Licensors: Sentai Filmworks 
 - Studios: Diomedea 
 - Source: Original
 - Genres: Comedy, School, Slice of Life
 - Duration: 24 min. per ep.
 - Rating: PG-13 - Teens 13 or older


## Links

