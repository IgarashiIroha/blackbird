# Konbini Kareshi

![Konbini Kareshi](https://cdn.myanimelist.net/images/anime/4/86243l.jpg)

* Japanese:  コンビニカレシ

## Information

 - Type: TV
 - Episodes: 12
 - Status: Finished Airing
 - Aired: Jul 7, 2017 to Sep 29, 2017
 - Premiered: Summer 2017
 - Broadcast: Fridays at 01:58 (JST)
 - Producers: VAP, Dentsu, TBS, chara-ani.com, Pierrot Plus, Being, Gzbrain, Cocoro Free, Tosho Printing 
 - Licensors: Funimation 
 - Studios: Studio Pierrot 
 - Source: Original
 - Genres: Slice of Life, Romance
 - Duration: 24 min. per ep.
 - Rating: PG-13 - Teens 13 or older


## Links

