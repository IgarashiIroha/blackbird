# Nana Maru San Batsu

![Nana Maru San Batsu](https://cdn.myanimelist.net/images/anime/9/86731l.jpg)

* Japanese:  ナナマル サンバツ

## Information

 - Type: TV
 - Episodes: 12
 - Status: Finished Airing
 - Aired: Jul 5, 2017 to Sep 20, 2017
 - Premiered: Summer 2017
 - Broadcast: Wednesdays at 01:59 (JST)
 - Producers: Toei Video, Enterbrain, Nippon Television Network 
 - Licensors: None found, add some 
 - Studios: TMS Entertainment 
 - Source: Manga
 - Genres: Game, School, Seinen
 - Duration: 23 min. per ep.
 - Rating: PG-13 - Teens 13 or older


## Links

