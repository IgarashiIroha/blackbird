# Katsugeki/Touken Ranbu

![Katsugeki/Touken Ranbu](https://cdn.myanimelist.net/images/anime/12/87984l.jpg)

* Japanese:  活撃／刀剣乱舞

## Information

 - Type: TV
 - Episodes: 13
 - Status: Finished Airing
 - Aired: Jul 2, 2017 to Sep 24, 2017
 - Premiered: Summer 2017
 - Broadcast: Sundays at 00:30 (JST)
 - Producers: Aniplex, Nitroplus, Good Smile Company, DMM.com 
 - Licensors: Aniplex of America 
 - Studios: ufotable 
 - Source: Card game
 - Genres: Action, Fantasy, Historical, Samurai
 - Duration: 23 min. per ep.
 - Rating: PG-13 - Teens 13 or older


## Links

