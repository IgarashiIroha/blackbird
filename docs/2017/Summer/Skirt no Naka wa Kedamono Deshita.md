# Skirt no Naka wa Kedamono Deshita.

![Skirt no Naka wa Kedamono Deshita.](https://cdn.myanimelist.net/images/anime/4/86647l.jpg)

* Japanese:  スカートの中はケダモノでした。

## Information

 - Type: TV
 - Episodes: 12
 - Status: Finished Airing
 - Aired: Jul 3, 2017 to Sep 18, 2017
 - Premiered: Summer 2017
 - Broadcast: Mondays at 01:00 (JST)
 - Producers: None found, add some 
 - Licensors: None found, add some 
 - Studios: Magic Bus 
 - Source: Manga
 - Genres: Drama, Romance, Ecchi
 - Duration: 5 min. per ep.
 - Rating: R+ - Mild Nudity


## Links

