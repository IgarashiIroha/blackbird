# Aho Girl

![Aho Girl](https://cdn.myanimelist.net/images/anime/7/86665l.jpg)

* Japanese:  アホガール

## Information

 - Type: TV
 - Episodes: 12
 - Status: Finished Airing
 - Aired: Jul 4, 2017 to Sep 19, 2017
 - Premiered: Summer 2017
 - Broadcast: Tuesdays at 23:00 (JST)
 - Producers: Nihon Ad Systems, Kodansha, Movic, KlockWorx, Sony Music Entertainment, King Records, BS11, Animatic, Marui Group 
 - Licensors: None found, add some 
 - Studios: Diomedea 
 - Source: 4-koma manga
 - Genres: Comedy, Romance, School, Shounen
 - Duration: 12 min. per ep.
 - Rating: PG-13 - Teens 13 or older


## Links

