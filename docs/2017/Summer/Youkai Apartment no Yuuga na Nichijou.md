# Youkai Apartment no Yuuga na Nichijou

![Youkai Apartment no Yuuga na Nichijou](https://cdn.myanimelist.net/images/anime/5/86678l.jpg)

* Japanese:  妖怪アパートの幽雅な日常

## Information

 - Type: TV
 - Episodes: 26
 - Status: Finished Airing
 - Aired: Jul 3, 2017 to Dec 25, 2017
 - Premiered: Summer 2017
 - Broadcast: Mondays at 23:00 (JST)
 - Producers: TMS Entertainment, Kodansha, Asmik Ace, East Japan Marketing & Communications, Sumitomo 
 - Licensors: None found, add some 
 - Studios: Shin-Ei Animation 
 - Source: Novel
 - Genres: Slice of Life, Mystery, Supernatural
 - Duration: 23 min. per ep.
 - Rating: PG-13 - Teens 13 or older


## Links

