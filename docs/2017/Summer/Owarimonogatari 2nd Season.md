# Owarimonogatari 2nd Season

![Owarimonogatari 2nd Season](https://cdn.myanimelist.net/images/anime/6/87322l.jpg)

* Japanese:  終物語

## Information

 - Type: TV
 - Episodes: 7
 - Status: Finished Airing
 - Aired: Aug 12, 2017 to Aug 13, 2017
 - Premiered: Summer 2017
 - Broadcast: Not scheduled once per week
 - Producers: Aniplex, Kodansha 
 - Licensors: Aniplex of America 
 - Studios: Shaft 
 - Source: Light novel
 - Genres: Comedy, Mystery, Supernatural, Vampire
 - Duration: 22 min. per ep.
 - Rating: R - 17+ (violence & profanity)


## Links

