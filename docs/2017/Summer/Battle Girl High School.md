# Battle Girl High School

![Battle Girl High School](https://cdn.myanimelist.net/images/anime/9/87837l.jpg)

* Japanese:  バトルガール ハイスクール

## Information

 - Type: TV
 - Episodes: 12
 - Status: Finished Airing
 - Aired: Jul 3, 2017 to Sep 18, 2017
 - Premiered: Summer 2017
 - Broadcast: Mondays at 00:30 (JST)
 - Producers: flying DOG, Hakuhodo DY Music & Pictures, COLOPL 
 - Licensors: Sentai Filmworks 
 - Studios: Silver Link. 
 - Source: Game
 - Genres: Action, Sci-Fi, School
 - Duration: 23 min. per ep.
 - Rating: PG-13 - Teens 13 or older


## Links

