# Tsurezure Children

![Tsurezure Children](https://cdn.myanimelist.net/images/anime/12/86676l.jpg)

* Japanese:  徒然チルドレン

## Information

 - Type: TV
 - Episodes: 12
 - Status: Finished Airing
 - Aired: Jul 4, 2017 to Sep 19, 2017
 - Premiered: Summer 2017
 - Broadcast: Tuesdays at 23:15 (JST)
 - Producers: Nihon Ad Systems, Kodansha, Movic, KlockWorx, Sony Music Entertainment, King Records, BS11, Animatic, Marui Group 
 - Licensors: Funimation 
 - Studios: Studio Gokumi 
 - Source: 4-koma manga
 - Genres: Comedy, Romance, School, Shounen
 - Duration: 12 min. per ep.
 - Rating: PG-13 - Teens 13 or older


## Links

