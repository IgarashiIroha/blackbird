# Senki Zesshou Symphogear AXZ

![Senki Zesshou Symphogear AXZ](https://cdn.myanimelist.net/images/anime/3/86584l.jpg)

* Japanese:  戦姫絶唱シンフォギアAXZ〈アクシズ〉

## Information

 - Type: TV
 - Episodes: 13
 - Status: Finished Airing
 - Aired: Jul 2, 2017 to Oct 1, 2017
 - Premiered: Summer 2017
 - Broadcast: Sundays at 01:00 (JST)
 - Producers: Memory-Tech, Bushiroad, MAGES., King Records, Kinyosha 
 - Licensors: None found, add some 
 - Studios: Satelight 
 - Source: Original
 - Genres: Action, Music, Sci-Fi
 - Duration: 24 min. per ep.
 - Rating: PG-13 - Teens 13 or older


## Links

