# Hina Logi: From Luck & Logic

![Hina Logi: From Luck & Logic](https://cdn.myanimelist.net/images/anime/11/86576l.jpg)

* Japanese:  ひなろじ～from Luck & Logic～

## Information

 - Type: TV
 - Episodes: 12
 - Status: Finished Airing
 - Aired: Jul 1, 2017 to Sep 23, 2017
 - Premiered: Summer 2017
 - Broadcast: Saturdays at Unknown
 - Producers: Bandai Visual, Lantis, Nitroplus, Bushiroad, Yuhodo 
 - Licensors: Funimation 
 - Studios: Doga Kobo 
 - Source: Card game
 - Genres: Action, Comedy, Fantasy, School
 - Duration: 23 min. per ep.
 - Rating: PG-13 - Teens 13 or older


## Links

