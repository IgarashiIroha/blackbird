# Yami Shibai 5

![Yami Shibai 5](https://cdn.myanimelist.net/images/anime/11/86158l.jpg)

* Japanese:  闇芝居 5期

## Information

 - Type: TV
 - Episodes: 13
 - Status: Finished Airing
 - Aired: Jul 3, 2017 to Oct 2, 2017
 - Premiered: Summer 2017
 - Broadcast: Mondays at 03:05 (JST)
 - Producers: TV Tokyo 
 - Licensors: None found, add some 
 - Studios: ILCA 
 - Source: Original
 - Genres: Dementia, Horror, Supernatural
 - Duration: 4 min. per ep.
 - Rating: PG-13 - Teens 13 or older


## Links

