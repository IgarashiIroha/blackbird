# Musekinin Galaxy☆Tylor

![Musekinin Galaxy☆Tylor](https://cdn.myanimelist.net/images/anime/13/86832l.jpg)

* Japanese:  無責任ギャラクシー☆タイラー

## Information

 - Type: TV
 - Episodes: 12
 - Status: Finished Airing
 - Aired: Jul 11, 2017 to Sep 26, 2017
 - Premiered: Summer 2017
 - Broadcast: Tuesdays at 21:55 (JST)
 - Producers: DAX Production, Dream Creation 
 - Licensors: None found, add some 
 - Studios: Seven 
 - Source: Light novel
 - Genres: Military, Sci-Fi, Space, Comedy, Parody
 - Duration: 3 min. per ep.
 - Rating: PG-13 - Teens 13 or older


## Links

