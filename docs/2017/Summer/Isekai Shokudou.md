# Isekai Shokudou

![Isekai Shokudou](https://cdn.myanimelist.net/images/anime/3/86666l.jpg)

* Japanese:  異世界食堂

## Information

 - Type: TV
 - Episodes: 12
 - Status: Finished Airing
 - Aired: Jul 4, 2017 to Sep 19, 2017
 - Premiered: Summer 2017
 - Broadcast: Tuesdays at 01:35 (JST)
 - Producers: TV Tokyo, Sotsu, flying DOG, Nikkatsu, Avex Pictures, Dai Nippon Printing, Shufunotomo 
 - Licensors: Funimation 
 - Studios: Silver Link. 
 - Source: Light novel
 - Genres: Comedy, Fantasy, Mystery
 - Duration: 23 min. per ep.
 - Rating: PG-13 - Teens 13 or older


## Links

