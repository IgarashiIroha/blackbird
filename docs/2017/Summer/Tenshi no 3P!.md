# Tenshi no 3P!

![Tenshi no 3P!](https://cdn.myanimelist.net/images/anime/8/86831l.jpg)

* Japanese:  天使の3P！〈スリーピース〉

## Information

 - Type: TV
 - Episodes: 12
 - Status: Finished Airing
 - Aired: Jul 10, 2017 to Sep 25, 2017
 - Premiered: Summer 2017
 - Broadcast: Mondays at 20:00 (JST)
 - Producers: Sotsu, Lantis, AT-X, Barnum Studio, KlockWorx, GAGA, ASCII Media Works, Studio Mausu, Showgate, Happinet Pictures, Hakuhodo DY Music & Pictures 
 - Licensors: None found, add some 
 - Studios: Project No.9 
 - Source: Light novel
 - Genres: Music, School, Slice of Life
 - Duration: 23 min. per ep.
 - Rating: PG-13 - Teens 13 or older


## Links

