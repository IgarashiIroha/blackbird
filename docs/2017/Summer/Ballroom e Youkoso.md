# Ballroom e Youkoso

![Ballroom e Youkoso](https://cdn.myanimelist.net/images/anime/5/86739l.jpg)

* Japanese:  ボールルームへようこそ

## Information

 - Type: TV
 - Episodes: 24
 - Status: Finished Airing
 - Aired: Jul 9, 2017 to Dec 17, 2017
 - Premiered: Summer 2017
 - Broadcast: Sundays at 02:08 (JST)
 - Producers: Dentsu, Mainichi Broadcasting System, Pony Canyon, Kodansha, Movic, Bandai Namco Games, Toy&#039;s Factory, Animatic 
 - Licensors: Ponycan USA 
 - Studios: Production I.G 
 - Source: Manga
 - Genres: Comedy, Drama, Romance, School, Shounen, Sports
 - Duration: 24 min. per ep.
 - Rating: PG-13 - Teens 13 or older


## Links

