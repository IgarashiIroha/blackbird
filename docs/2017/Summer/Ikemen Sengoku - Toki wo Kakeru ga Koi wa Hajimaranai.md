# Ikemen Sengoku: Toki wo Kakeru ga Koi wa Hajimaranai

![Ikemen Sengoku: Toki wo Kakeru ga Koi wa Hajimaranai](https://cdn.myanimelist.net/images/anime/7/86054l.jpg)

* Japanese:  イケメン戦国◆時をかけるが恋ははじまらない

## Information

 - Type: TV
 - Episodes: 12
 - Status: Finished Airing
 - Aired: Jul 12, 2017 to Sep 27, 2017
 - Premiered: Summer 2017
 - Broadcast: Wednesdays at 22:10 (JST)
 - Producers: None found, add some 
 - Licensors: None found, add some 
 - Studios: TMS Entertainment, Jinnis Animation Studios 
 - Source: Game
 - Genres: Historical, Romance
 - Duration: 3 min. per ep.
 - Rating: PG-13 - Teens 13 or older


## Links

