# Koi to Uso

![Koi to Uso](https://cdn.myanimelist.net/images/anime/5/86663l.jpg)

* Japanese:  恋と嘘

## Information

 - Type: TV
 - Episodes: 12
 - Status: Finished Airing
 - Aired: Jul 4, 2017 to Sep 19, 2017
 - Premiered: Summer 2017
 - Broadcast: Tuesdays at 00:00 (JST)
 - Producers: Frontier Works, Pony Canyon, Kodansha, Ultra Super Pictures, Memory-Tech, Amuse, Hakuhodo DY Music & Pictures, Animatic, DeNA, A-Sketch 
 - Licensors: Sentai Filmworks 
 - Studios: LIDENFILMS 
 - Source: Manga
 - Genres: Drama, Romance, School
 - Duration: 24 min. per ep.
 - Rating: PG-13 - Teens 13 or older


## Links

