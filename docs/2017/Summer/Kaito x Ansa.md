# Kaito x Ansa

![Kaito x Ansa](https://cdn.myanimelist.net/images/anime/5/85063l.jpg)

* Japanese:  カイトアンサ

## Information

 - Type: TV
 - Episodes: 12
 - Status: Finished Airing
 - Aired: Jul 12, 2017 to Sep 27, 2017
 - Premiered: Summer 2017
 - Broadcast: Wednesdays at 20:10 (JST)
 - Producers: Pony Canyon 
 - Licensors: None found, add some 
 - Studios: Tengu Kobo 
 - Source: Original
 - Genres: Game
 - Duration: 7 min. per ep.
 - Rating: PG-13 - Teens 13 or older


## Links

