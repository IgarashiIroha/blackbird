# The Reflection

![The Reflection](https://cdn.myanimelist.net/images/anime/4/83018l.jpg)

* Japanese:  ザ・リフレクション

## Information

 - Type: TV
 - Episodes: 12
 - Status: Finished Airing
 - Aired: Jul 22, 2017 to Oct 7, 2017
 - Premiered: Summer 2017
 - Broadcast: Saturdays at 23:00 (JST)
 - Producers: Bandai Visual, NHK, 81 Produce, Haoliners Animation League, China Animation Characters, BIGFACE, NHN PlayArt, Ima Group, Crunchyroll SC Anime Fund 
 - Licensors: Funimation 
 - Studios: Studio Deen 
 - Source: Original
 - Genres: Action, Super Power
 - Duration: 25 min. per ep.
 - Rating: PG-13 - Teens 13 or older


## Links

