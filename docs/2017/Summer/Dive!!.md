# Dive!!

![Dive!!](https://cdn.myanimelist.net/images/anime/11/86744l.jpg)

* Japanese:  DIVE!!

## Information

 - Type: TV
 - Episodes: 12
 - Status: Finished Airing
 - Aired: Jul 7, 2017 to Sep 22, 2017
 - Premiered: Summer 2017
 - Broadcast: Fridays at 00:55 (JST)
 - Producers: Aniplex, Dentsu, Fuji TV, DMM pictures 
 - Licensors: Sentai Filmworks 
 - Studios: Zero-G 
 - Source: Novel
 - Genres: Comedy, Sports
 - Duration: 22 min. per ep.
 - Rating: PG-13 - Teens 13 or older


## Links

