# Teekyuu 9

![Teekyuu 9](https://cdn.myanimelist.net/images/anime/5/86833l.jpg)

* Japanese:  てーきゅう 9期

## Information

 - Type: TV
 - Episodes: 12
 - Status: Finished Airing
 - Aired: Jul 12, 2017 to Sep 27, 2017
 - Premiered: Summer 2017
 - Broadcast: Wednesdays at 22:40 (JST)
 - Producers: DAX Production, Smiral Animation 
 - Licensors: None found, add some 
 - Studios: Millepensee 
 - Source: Manga
 - Genres: Comedy, Sports
 - Duration: 2 min. per ep.
 - Rating: PG-13 - Teens 13 or older


## Links

