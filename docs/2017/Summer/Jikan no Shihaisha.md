# Jikan no Shihaisha

![Jikan no Shihaisha](https://cdn.myanimelist.net/images/anime/3/86753l.jpg)

* Japanese:  時間の支配者

## Information

 - Type: TV
 - Episodes: 13
 - Status: Finished Airing
 - Aired: Jul 8, 2017 to Sep 30, 2017
 - Premiered: Summer 2017
 - Broadcast: Saturdays at 00:30 (JST)
 - Producers: Lantis, NBCUniversal Entertainment Japan 
 - Licensors: Funimation 
 - Studios: Project No.9 
 - Source: Manga
 - Genres: Supernatural, Shounen
 - Duration: 24 min. per ep.
 - Rating: PG-13 - Teens 13 or older


## Links

