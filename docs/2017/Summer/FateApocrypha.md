# Fate/Apocrypha

![Fate/Apocrypha](https://cdn.myanimelist.net/images/anime/9/86573l.jpg)

* Japanese:  Fate/Apocrypha

## Information

 - Type: TV
 - Episodes: 25
 - Status: Finished Airing
 - Aired: Jul 2, 2017 to Dec 31, 2017
 - Premiered: Summer 2017
 - Broadcast: Sundays at 00:00 (JST)
 - Producers: Aniplex, Half H.P Studio, Notes, Tokyo MX, BS11 
 - Licensors: Aniplex of America 
 - Studios: A-1 Pictures 
 - Source: Light novel
 - Genres: Action, Supernatural, Drama, Magic, Fantasy
 - Duration: 23 min. per ep.
 - Rating: R - 17+ (violence & profanity)


## Links

