# Netsuzou TRap

![Netsuzou TRap](https://cdn.myanimelist.net/images/anime/10/86667l.jpg)

* Japanese:  捏造トラップ―NTR―

## Information

 - Type: TV
 - Episodes: 12
 - Status: Finished Airing
 - Aired: Jul 5, 2017 to Sep 20, 2017
 - Premiered: Summer 2017
 - Broadcast: Wednesdays at 20:00 (JST)
 - Producers: DMM.com, Ichijinsha 
 - Licensors: None found, add some 
 - Studios: Creators in Pack 
 - Source: Manga
 - Genres: Drama, Romance, Shoujo Ai
 - Duration: 9 min. per ep.
 - Rating: R+ - Mild Nudity


## Links

