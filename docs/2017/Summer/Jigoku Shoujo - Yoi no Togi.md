# Jigoku Shoujo: Yoi no Togi

![Jigoku Shoujo: Yoi no Togi](https://cdn.myanimelist.net/images/anime/1110/92117l.jpg)

* Japanese:  地獄少女 宵伽

## Information

 - Type: TV
 - Episodes: 6
 - Status: Finished Airing
 - Aired: Jul 15, 2017 to Aug 19, 2017
 - Premiered: Summer 2017
 - Broadcast: Saturdays at 00:00 (JST)
 - Producers: Aniplex, Fujishouji 
 - Licensors: Aniplex of America 
 - Studios: Studio Deen 
 - Source: Original
 - Genres: Horror, Mystery, Psychological, Supernatural
 - Duration: 23 min. per ep.
 - Rating: R - 17+ (violence & profanity)


## Links

