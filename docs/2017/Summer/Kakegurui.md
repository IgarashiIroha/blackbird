# Kakegurui

![Kakegurui](https://cdn.myanimelist.net/images/anime/3/86578l.jpg)

* Japanese:  賭ケグルイ

## Information

 - Type: TV
 - Episodes: 12
 - Status: Finished Airing
 - Aired: Jul 1, 2017 to Sep 23, 2017
 - Premiered: Summer 2017
 - Broadcast: Saturdays at 22:00 (JST)
 - Producers: Dentsu, Square Enix, Movic, KlockWorx, DIVE II Entertainment, 81 Produce, Furyu 
 - Licensors: None found, add some 
 - Studios: MAPPA 
 - Source: Manga
 - Genres: Game, Mystery, Psychological, Drama, School, Shounen
 - Duration: 24 min. per ep.
 - Rating: R - 17+ (violence & profanity)


## Links

