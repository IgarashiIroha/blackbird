# Gamers!

![Gamers!](https://cdn.myanimelist.net/images/anime/4/86828l.jpg)

* Japanese:  ゲーマーズ！

## Information

 - Type: TV
 - Episodes: 12
 - Status: Finished Airing
 - Aired: Jul 13, 2017 to Sep 28, 2017
 - Premiered: Summer 2017
 - Broadcast: Thursdays at 21:00 (JST)
 - Producers: Sony Pictures Entertainment, Frontier Works, Kadokawa Shoten, AT-X, NBCUniversal Entertainment Japan, Tokyo MX, i0+, Chugai Mining, BS11 
 - Licensors: Funimation 
 - Studios: Pine Jam 
 - Source: Light novel
 - Genres: Comedy, Romance, School
 - Duration: 24 min. per ep.
 - Rating: PG-13 - Teens 13 or older


## Links

