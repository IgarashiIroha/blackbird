# Keppeki Danshi! Aoyama-kun

![Keppeki Danshi! Aoyama-kun](https://cdn.myanimelist.net/images/anime/9/86644l.jpg)

* Japanese:  潔癖男子！青山くん

## Information

 - Type: TV
 - Episodes: 12
 - Status: Finished Airing
 - Aired: Jul 3, 2017 to Sep 18, 2017
 - Premiered: Summer 2017
 - Broadcast: Sundays at 00:00 (JST)
 - Producers: Sotsu, TMS Entertainment, Pony Canyon, KlockWorx, Tokyo MX, East Japan Marketing & Communications 
 - Licensors: Ponycan USA 
 - Studios: Studio Hibari 
 - Source: Manga
 - Genres: Comedy, Seinen, Sports
 - Duration: 23 min. per ep.
 - Rating: PG-13 - Teens 13 or older


## Links

