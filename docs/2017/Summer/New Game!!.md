# New Game!!

![New Game!!](https://cdn.myanimelist.net/images/anime/4/86790l.jpg)

* Japanese:  NEW GAME!!

## Information

 - Type: TV
 - Episodes: 12
 - Status: Finished Airing
 - Aired: Jul 11, 2017 to Sep 26, 2017
 - Premiered: Summer 2017
 - Broadcast: Tuesdays at 21:30 (JST)
 - Producers: Frontier Works, Media Factory, Movic, AT-X, Sony Music Communications, Houbunsha, MAGES., Lawson 
 - Licensors: Funimation 
 - Studios: Doga Kobo 
 - Source: 4-koma manga
 - Genres: Game, Slice of Life, Comedy
 - Duration: 24 min. per ep.
 - Rating: PG-13 - Teens 13 or older


## Links

