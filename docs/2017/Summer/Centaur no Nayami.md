# Centaur no Nayami

![Centaur no Nayami](https://cdn.myanimelist.net/images/anime/2/86767l.jpg)

* Japanese:  セントールの悩み

## Information

 - Type: TV
 - Episodes: 12
 - Status: Finished Airing
 - Aired: Jul 9, 2017 to Sep 24, 2017
 - Premiered: Summer 2017
 - Broadcast: Sundays at 22:00 (JST)
 - Producers: Tokuma Shoten, MAGES., Yomiuri TV Enterprise, Kanon Sound, Emon, RAY, NichiNare, Top Marshal, Crunchyroll SC Anime Fund 
 - Licensors: Funimation 
 - Studios: Haoliners Animation League 
 - Source: Manga
 - Genres: Slice of Life, Comedy, Fantasy, School, Seinen
 - Duration: 23 min. per ep.
 - Rating: R+ - Mild Nudity


## Links

