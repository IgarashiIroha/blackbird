# Mahoujin Guruguru (2017)

![Mahoujin Guruguru (2017)](https://cdn.myanimelist.net/images/anime/9/86835l.jpg)

* Japanese:  魔法陣グルグル

## Information

 - Type: TV
 - Episodes: 24
 - Status: Finished Airing
 - Aired: Jul 11, 2017 to Dec 20, 2017
 - Premiered: Summer 2017
 - Broadcast: Wednesdays at 01:35 (JST)
 - Producers: TV Tokyo, Square Enix, Lantis, Kadokawa Shoten, AT-X, Magic Capsule, Sony Music Communications 
 - Licensors: None found, add some 
 - Studios: Production I.G 
 - Source: Manga
 - Genres: Adventure, Fantasy, Magic, Comedy, Shounen
 - Duration: 24 min. per ep.
 - Rating: PG-13 - Teens 13 or older


## Links

