# Made in Abyss

![Made in Abyss](https://cdn.myanimelist.net/images/anime/1922/91900l.jpg)

* Japanese:  メイドインアビス

## Information

 - Type: TV
 - Episodes: 13
 - Status: Finished Airing
 - Aired: Jul 7, 2017 to Sep 29, 2017
 - Premiered: Summer 2017
 - Broadcast: Fridays at 21:30 (JST)
 - Producers: Media Factory, AT-X, Takeshobo, Sony Music Communications, Kadokawa Media House, Cygames, Kanetsu Co., LTD. 
 - Licensors: Sentai Filmworks 
 - Studios: Kinema Citrus 
 - Source: Web manga
 - Genres: Sci-Fi, Adventure, Mystery, Drama, Fantasy
 - Duration: 25 min. per ep.
 - Rating: R - 17+ (violence & profanity)


## Links

