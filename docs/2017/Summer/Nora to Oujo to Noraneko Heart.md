# Nora to Oujo to Noraneko Heart

![Nora to Oujo to Noraneko Heart](https://cdn.myanimelist.net/images/anime/3/85868l.jpg)

* Japanese:  ノラと皇女と野良猫ハート

## Information

 - Type: TV
 - Episodes: 11
 - Status: Finished Airing
 - Aired: Jul 12, 2017 to Sep 27, 2017
 - Premiered: Summer 2017
 - Broadcast: Wednesdays at 22:45 (JST)
 - Producers: Magic Capsule 
 - Licensors: None found, add some 
 - Studios: W-Toon Studio, DMM.futureworks 
 - Source: Visual novel
 - Genres: Comedy, Ecchi
 - Duration: 3 min. per ep.
 - Rating: R+ - Mild Nudity


## Links

