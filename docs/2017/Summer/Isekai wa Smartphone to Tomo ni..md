# Isekai wa Smartphone to Tomo ni.

![Isekai wa Smartphone to Tomo ni.](https://cdn.myanimelist.net/images/anime/7/86794l.jpg)

* Japanese:  異世界はスマートフォンとともに。

## Information

 - Type: TV
 - Episodes: 12
 - Status: Finished Airing
 - Aired: Jul 11, 2017 to Sep 26, 2017
 - Premiered: Summer 2017
 - Broadcast: Tuesdays at 20:30 (JST)
 - Producers: Sotsu, MediaNet, Pony Canyon, AT-X, Asmik Ace, Hobby Japan, 81 Produce, Tokyo MX, Exit Tunes 
 - Licensors: Funimation 
 - Studios: Production Reed 
 - Source: Light novel
 - Genres: Adventure, Harem, Comedy, Magic, Romance, Fantasy
 - Duration: 24 min. per ep.
 - Rating: PG-13 - Teens 13 or older


## Links

- [[Refrain] Isekai wa Smartphone to Tomo ni. - 03 VF [720p]](https://uptobox.com/w3au1wia8cry)
- [[Refrain] Isekai wa Smartphone to Tomo ni. - 03 VF [1080p]](https://uptobox.com/uo2pspantkda)
- [[Refrain] Isekai wa Smartphone to Tomo ni. - 04 VF [720p]](https://uptobox.com/f85mfm0djo3s)
- [[Refrain] Isekai wa Smartphone to Tomo ni. - 04 VF [1080p]](https://uptobox.com/4ftw85nzjzce)
- [[Refrain] Isekai wa Smartphone to Tomo ni. - 05 VF [720p]](https://uptobox.com/43zkwnqh16y0)
- [[Refrain] Isekai wa Smartphone to Tomo ni. - 05 VF [1080p]](https://uptobox.com/y53d7gbqvxnm)
- [[Refrain] Isekai wa Smartphone to Tomo ni. - 06 VF [720p]](https://uptobox.com/ecttyd1yeh8h)
- [[Refrain] Isekai wa Smartphone to Tomo ni. - 06 VF [1080p]](https://uptobox.com/u48ly4rurvsy)
- [[Refrain] Isekai wa Smartphone to Tomo ni. - 07 VF [720p]](https://uptobox.com/gas2mlw0coh3)
- [[Refrain] Isekai wa Smartphone to Tomo ni. - 07 VF [1080p]](https://uptobox.com/bo96g3fa7diq)
- [[Refrain] Isekai wa Smartphone to Tomo ni. - 08 VF [720p]](https://uptobox.com/5d7am25depk9)
- [[Refrain] Isekai wa Smartphone to Tomo ni. - 08 VF [1080p]](https://uptobox.com/xja8liywi0mi)
- [[Refrain] Isekai wa Smartphone to Tomo ni. - 09 VF [720p]](https://uptobox.com/d0q12rvhks3g)
- [[Refrain] Isekai wa Smartphone to Tomo ni. - 09 VF [1080p]](https://uptobox.com/u7n4eyja1mc2)
- [[Refrain] Isekai wa Smartphone to Tomo ni. - 10 VF [720p]](https://uptobox.com/4pf156yr9zvr)
- [[Refrain] Isekai wa Smartphone to Tomo ni. - 10 VF [1080p]](https://uptobox.com/ni5jbe3wufqx)
- [[Refrain] Isekai wa Smartphone to Tomo ni. - 11 VF [720p]](https://uptobox.com/uk7elipxvpck)
- [[Refrain] Isekai wa Smartphone to Tomo ni. - 11 VF [1080p]](https://uptobox.com/ff1vtp79hebc)
- [[Refrain] Isekai wa Smartphone to Tomo ni. - 12 VF [720p]](https://uptobox.com/0fnm1y1copnf)
- [[Refrain] Isekai wa Smartphone to Tomo ni. - 12 VF [1080p]](https://uptobox.com/zuc82pd6jlrf)
