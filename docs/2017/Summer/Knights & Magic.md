# Knight&#039;s & Magic

![Knight&#039;s & Magic](https://cdn.myanimelist.net/images/anime/1472/93813l.jpg)

* Japanese:  ナイツ&マジック

## Information

 - Type: TV
 - Episodes: 13
 - Status: Finished Airing
 - Aired: Jul 2, 2017 to Sep 24, 2017
 - Premiered: Summer 2017
 - Broadcast: Sundays at 21:00 (JST)
 - Producers: Bandai Visual, Genco, Lantis, AT-X, Magic Capsule, Q-Tec, Hakuhodo DY Music & Pictures 
 - Licensors: Funimation 
 - Studios: 8bit 
 - Source: Light novel
 - Genres: Action, Fantasy, Mecha, School
 - Duration: 24 min. per ep.
 - Rating: PG-13 - Teens 13 or older


## Links

