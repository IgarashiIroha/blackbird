# Saiyuuki Reload Blast

![Saiyuuki Reload Blast](https://cdn.myanimelist.net/images/anime/11/86675l.jpg)

* Japanese:  最遊記RELOAD BLAST

## Information

 - Type: TV
 - Episodes: 12
 - Status: Finished Airing
 - Aired: Jul 5, 2017 to Sep 20, 2017
 - Premiered: Summer 2017
 - Broadcast: Wednesdays at 22:30 (JST)
 - Producers: Studio Pierrot, Dentsu, Frontier Works, Lantis, Media Factory, AT-X, Ichijinsha 
 - Licensors: Funimation 
 - Studios: Platinum Vision 
 - Source: Manga
 - Genres: Action, Adventure, Comedy, Supernatural, Drama, Josei
 - Duration: 23 min. per ep.
 - Rating: PG-13 - Teens 13 or older


## Links

