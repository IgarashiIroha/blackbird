# Hajimete no Gal

![Hajimete no Gal](https://cdn.myanimelist.net/images/anime/9/86826l.jpg)

* Japanese:  はじめてのギャル

## Information

 - Type: TV
 - Episodes: 10
 - Status: Finished Airing
 - Aired: Jul 12, 2017 to Sep 13, 2017
 - Premiered: Summer 2017
 - Broadcast: Wednesdays at 23:00 (JST)
 - Producers: Kadokawa Shoten, AT-X, KlockWorx, Toranoana, MAGES., Glovision, Kadokawa Media House, Sun TV, Kanetsu Co., LTD. 
 - Licensors: Funimation 
 - Studios: NAZ 
 - Source: Manga
 - Genres: Comedy, Romance, Ecchi, School, Shounen
 - Duration: 23 min. per ep.
 - Rating: R+ - Mild Nudity


## Links

