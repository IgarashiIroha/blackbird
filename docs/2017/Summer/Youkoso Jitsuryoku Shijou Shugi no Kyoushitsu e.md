# Youkoso Jitsuryoku Shijou Shugi no Kyoushitsu e

![Youkoso Jitsuryoku Shijou Shugi no Kyoushitsu e](https://cdn.myanimelist.net/images/anime/5/86830l.jpg)

* Japanese:  ようこそ実力至上主義の教室へ

## Information

 - Type: TV
 - Episodes: 12
 - Status: Finished Airing
 - Aired: Jul 12, 2017 to Sep 27, 2017
 - Premiered: Summer 2017
 - Broadcast: Wednesdays at 23:30 (JST)
 - Producers: Studio Hibari, Lantis, Media Factory, AT-X, Sony Music Communications, Toranoana, Kadokawa Media House, AKABEiSOFT2 
 - Licensors: Funimation 
 - Studios: Lerche 
 - Source: Light novel
 - Genres: Drama, Psychological, School, Slice of Life
 - Duration: 24 min. per ep.
 - Rating: PG-13 - Teens 13 or older


## Links

