# Hitorijime My Hero

![Hitorijime My Hero](https://cdn.myanimelist.net/images/anime/12/86825l.jpg)

* Japanese:  ひとりじめマイヒーロー

## Information

 - Type: TV
 - Episodes: 12
 - Status: Finished Airing
 - Aired: Jul 8, 2017 to Sep 23, 2017
 - Premiered: Summer 2017
 - Broadcast: Sundays at 01:30 (JST)
 - Producers: Sotsu, AT-X, Tokyo MX, Avex Pictures, Q-Tec, Docomo Anime Store, Marine Entertainment, Kanon Sound, Contents Seed, Ichijinsha, BS NTV 
 - Licensors: Sentai Filmworks 
 - Studios: Encourage Films 
 - Source: Manga
 - Genres: Drama, Romance, School, Shounen Ai, Slice of Life
 - Duration: 24 min. per ep.
 - Rating: PG-13 - Teens 13 or older


## Links

