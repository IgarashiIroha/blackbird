# Tales of Zestiria the Cross 2nd Season

![Tales of Zestiria the Cross 2nd Season](https://cdn.myanimelist.net/images/anime/2/82039l.jpg)

* Japanese:  テイルズ オブ ゼスティリア ザ クロス 第2期

## Information

 - Type: TV
 - Episodes: 13
 - Status: Finished Airing
 - Aired: Jan 8, 2017 to Apr 29, 2017
 - Premiered: Winter 2017
 - Broadcast: Sundays at 23:00 (JST)
 - Producers: Bandai Visual, Lantis, Bandai Namco Entertainment, Anime Consortium Japan 
 - Licensors: Funimation 
 - Studios: ufotable 
 - Source: Game
 - Genres: Action, Adventure, Magic, Fantasy
 - Duration: 23 min. per ep.
 - Rating: PG-13 - Teens 13 or older


## Links

- [[Refrain] Tales of Zestiria the Cross 2nd Season - 01 VF VOSTFR [1080p]](https://uptobox.com/e2g09wqousbw)
- [[Refrain] Tales of Zestiria the Cross 2nd Season - 01 VF VOSTFR [720p]](https://uptobox.com/oxduesrbv13u)
- [[Refrain] Tales of Zestiria the Cross 2nd Season - 02 VF VOSTFR [1080p]](https://uptobox.com/fpa4wv9odwbd)
- [[Refrain] Tales of Zestiria the Cross 2nd Season - 02 VF VOSTFR [720p]](https://uptobox.com/zhrem69v3alq)
- [[Refrain] Tales of Zestiria the Cross 2nd Season - 03 VF VOSTFR [1080p]](https://uptobox.com/msjequlg3176)
- [[Refrain] Tales of Zestiria the Cross 2nd Season - 03 VF VOSTFR [720p]](https://uptobox.com/krwu5ms1f6z6)
- [[Refrain] Tales of Zestiria the Cross 2nd Season - 04 VF VOSTFR [1080p]](https://uptobox.com/kmj4bbcf7kkx)
- [[Refrain] Tales of Zestiria the Cross 2nd Season - 04 VF VOSTFR [720p]](https://uptobox.com/ltcff8v4d60c)
- [[Refrain] Tales of Zestiria the Cross 2nd Season - 05 VF VOSTFR [1080p]](https://uptobox.com/sqyp9qzjbu6r)
- [[Refrain] Tales of Zestiria the Cross 2nd Season - 05 VF VOSTFR [720p]](https://uptobox.com/rmsffhdb9dmm)
- [[Refrain] Tales of Zestiria the Cross 2nd Season - 06 VF VOSTFR [1080p]](https://uptobox.com/icjgbupciri8)
- [[Refrain] Tales of Zestiria the Cross 2nd Season - 06 VF VOSTFR [720p]](https://uptobox.com/hvoz1e946qdp)
- [[Refrain] Tales of Zestiria the Cross 2nd Season - 07 VF VOSTFR [1080p]](https://uptobox.com/1fyunr55mf3l)
- [[Refrain] Tales of Zestiria the Cross 2nd Season - 07 VF VOSTFR [720p]](https://uptobox.com/67cd2ubrvsx7)
- [[Refrain] Tales of Zestiria the Cross 2nd Season - 08 VF VOSTFR [1080p]](https://uptobox.com/rtf31q8shfos)
- [[Refrain] Tales of Zestiria the Cross 2nd Season - 08 VF VOSTFR [720p]](https://uptobox.com/nssvacsl1sy2)
- [[Refrain] Tales of Zestiria the Cross 2nd Season - 09 VF VOSTFR [1080p]](https://uptobox.com/za2xtpywcwrc)
- [[Refrain] Tales of Zestiria the Cross 2nd Season - 09 VF VOSTFR [720p]](https://uptobox.com/rr2w47wveb2l)
- [[Refrain] Tales of Zestiria the Cross 2nd Season - 10 VF VOSTFR [1080p]](https://uptobox.com/hv9haizc1kvj)
- [[Refrain] Tales of Zestiria the Cross 2nd Season - 10 VF VOSTFR [720p]](https://uptobox.com/d095rkyd64q4)
- [[Refrain] Tales of Zestiria the Cross 2nd Season - 11 VF VOSTFR [1080p]](https://uptobox.com/5uj5p1xxgo0s)
- [[Refrain] Tales of Zestiria the Cross 2nd Season - 11 VF VOSTFR [720p]](https://uptobox.com/ljxbahobe3k9)
- [[Refrain] Tales of Zestiria the Cross 2nd Season - 12 VF VOSTFR [1080p]](https://uptobox.com/v2wova4cmb3o)
- [[Refrain] Tales of Zestiria the Cross 2nd Season - 12 VF VOSTFR [720p]](https://uptobox.com/13bo5lh3uwx4)
- [[Refrain] Tales of Zestiria the Cross 2nd Season - 13 VF VOSTFR [1080p]](https://uptobox.com/qjpvzqoyjlyt)
