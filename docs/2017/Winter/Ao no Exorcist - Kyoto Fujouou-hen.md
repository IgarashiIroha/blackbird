# Ao no Exorcist: Kyoto Fujouou-hen

![Ao no Exorcist: Kyoto Fujouou-hen](https://cdn.myanimelist.net/images/anime/5/85201l.jpg)

* Japanese:  青の祓魔師 京都不浄王篇

## Information

 - Type: TV
 - Episodes: 12
 - Status: Finished Airing
 - Aired: Jan 7, 2017 to Mar 25, 2017
 - Premiered: Winter 2017
 - Broadcast: Saturdays at 01:55 (JST)
 - Producers: Aniplex, Dentsu, Mainichi Broadcasting System, Movic, Shueisha 
 - Licensors: Aniplex of America 
 - Studios: A-1 Pictures 
 - Source: Manga
 - Genres: Action, Demons, Supernatural, Fantasy, Shounen
 - Duration: 24 min. per ep.
 - Rating: PG-13 - Teens 13 or older


## Links

- [[Refrain] Ao no Exorcist - Kyoto Fujouou-hen - 01 VF VOSTFR [1080p]](https://uptobox.com/lqv644446ekr)
- [[Refrain] Ao no Exorcist - Kyoto Fujouou-hen - 01 VF VOSTFR [720p]](https://uptobox.com/3hl8bk2hcxu0)
- [[Refrain] Ao no Exorcist - Kyoto Fujouou-hen - 02 VF VOSTFR [1080p]](https://uptobox.com/35s6e12zdkml)
- [[Refrain] Ao no Exorcist - Kyoto Fujouou-hen - 02 VF VOSTFR [720p]](https://uptobox.com/zhsr09b8zzss)
- [[Refrain] Ao no Exorcist - Kyoto Fujouou-hen - 03 VF VOSTFR [1080p]](https://uptobox.com/a6tvfhubbodp)
- [[Refrain] Ao no Exorcist - Kyoto Fujouou-hen - 03 VF VOSTFR [720p]](https://uptobox.com/vp45u94sc23c)
- [[Refrain] Ao no Exorcist - Kyoto Fujouou-hen - 04 VF VOSTFR [1080p]](https://uptobox.com/bmyvv9obuq74)
- [[Refrain] Ao no Exorcist - Kyoto Fujouou-hen - 04 VF VOSTFR [720p]](https://uptobox.com/602dasbanpaj)
- [[Refrain] Ao no Exorcist - Kyoto Fujouou-hen - 05 VF VOSTFR [1080p]](https://uptobox.com/utxf7jyz39gs)
- [[Refrain] Ao no Exorcist - Kyoto Fujouou-hen - 05 VF VOSTFR [720p]](https://uptobox.com/pg7t6fqfhc37)
- [[Refrain] Ao no Exorcist - Kyoto Fujouou-hen - 06 VF VOSTFR [1080p]](https://uptobox.com/g4va0y59qfzw)
- [[Refrain] Ao no Exorcist - Kyoto Fujouou-hen - 06 VF VOSTFR [720p]](https://uptobox.com/g9g0fj20pz33)
- [[Refrain] Ao no Exorcist - Kyoto Fujouou-hen - 07 VF VOSTFR [1080p]](https://uptobox.com/pme1pwgeue8g)
- [[Refrain] Ao no Exorcist - Kyoto Fujouou-hen - 07 VF VOSTFR [720p]](https://uptobox.com/n5b8wdgrgntz)
- [[Refrain] Ao no Exorcist - Kyoto Fujouou-hen - 08 VF VOSTFR [1080p]](https://uptobox.com/bw43hiy3itfp)
- [[Refrain] Ao no Exorcist - Kyoto Fujouou-hen - 08 VF VOSTFR [720p]](https://uptobox.com/k4x3dh3psqmd)
- [[Refrain] Ao no Exorcist - Kyoto Fujouou-hen - 09 VF VOSTFR [1080p]](https://uptobox.com/i6bol7kx1wph)
- [[Refrain] Ao no Exorcist - Kyoto Fujouou-hen - 09 VF VOSTFR [720p]](https://uptobox.com/yhiwycdol39h)
- [[Refrain] Ao no Exorcist - Kyoto Fujouou-hen - 10 VF VOSTFR [1080p]](https://uptobox.com/v992ir2c5gue)
- [[Refrain] Ao no Exorcist - Kyoto Fujouou-hen - 10 VF VOSTFR [720p]](https://uptobox.com/nx7rjdv6d7jd)
- [[Refrain] Ao no Exorcist - Kyoto Fujouou-hen - 11 VF VOSTFR [1080p]](https://uptobox.com/mkxb2gijq15r)
- [[Refrain] Ao no Exorcist - Kyoto Fujouou-hen - 11 VF VOSTFR [720p]](https://uptobox.com/z62q9oelu3ck)
- [[Refrain] Ao no Exorcist - Kyoto Fujouou-hen - 12 VF VOSTFR [1080p]](https://uptobox.com/o8aas0xmfkfc)
- [[Refrain] Ao no Exorcist - Kyoto Fujouou-hen - 12 VF VOSTFR [720p]](https://uptobox.com/5qkw5sl5c27t)
