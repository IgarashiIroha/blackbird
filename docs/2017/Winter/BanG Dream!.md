# BanG Dream!

![BanG Dream!](https://cdn.myanimelist.net/images/anime/4/82414l.jpg)

* Japanese:  BanG Dream!（バンドリ！）

## Information

 - Type: TV
 - Episodes: 13
 - Status: Finished Airing
 - Aired: Jan 21, 2017 to Apr 22, 2017
 - Premiered: Winter 2017
 - Broadcast: Saturdays at 22:30 (JST)
 - Producers: OLM, HoriPro, Bushiroad, Tokyo MX, Good Smile Company, Overlap, Bushiroad Music 
 - Licensors: Sentai Filmworks 
 - Studios: Xebec, Issen 
 - Source: Manga
 - Genres: Music
 - Duration: 24 min. per ep.
 - Rating: PG-13 - Teens 13 or older


## Links

