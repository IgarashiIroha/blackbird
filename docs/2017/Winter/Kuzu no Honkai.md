# Kuzu no Honkai

![Kuzu no Honkai](https://cdn.myanimelist.net/images/anime/5/83937l.jpg)

* Japanese:  クズの本懐

## Information

 - Type: TV
 - Episodes: 12
 - Status: Finished Airing
 - Aired: Jan 13, 2017 to Mar 31, 2017
 - Premiered: Winter 2017
 - Broadcast: Fridays at 00:55 (JST)
 - Producers: Aniplex, Dentsu, Square Enix, Studio Hibari, Fuji TV, Kyoraku Industrial Holdings, Kansai Telecasting 
 - Licensors: Sentai Filmworks 
 - Studios: Lerche 
 - Source: Manga
 - Genres: Drama, Romance, Ecchi, School, Seinen
 - Duration: 22 min. per ep.
 - Rating: R+ - Mild Nudity


## Links

