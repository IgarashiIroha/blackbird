# Reikenzan: Eichi e no Shikaku

![Reikenzan: Eichi e no Shikaku](https://cdn.myanimelist.net/images/anime/11/83499l.jpg)

* Japanese:  霊剣山 叡智への資格

## Information

 - Type: TV
 - Episodes: 12
 - Status: Finished Airing
 - Aired: Jan 8, 2017 to Mar 26, 2017
 - Premiered: Winter 2017
 - Broadcast: Sundays at 00:00 (JST)
 - Producers: Haoliners Animation League, Tencent Animation & Comics 
 - Licensors: None found, add some 
 - Studios: Studio Deen 
 - Source: Light novel
 - Genres: Comedy, Magic, Fantasy
 - Duration: 24 min. per ep.
 - Rating: PG-13 - Teens 13 or older


## Links

