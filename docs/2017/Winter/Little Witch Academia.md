# Little Witch Academia (TV)

![Little Witch Academia (TV)](https://cdn.myanimelist.net/images/anime/13/83934l.jpg)

* Japanese:  リトルウィッチアカデミア

## Information

 - Type: TV
 - Episodes: 25
 - Status: Finished Airing
 - Aired: Jan 9, 2017 to Jun 26, 2017
 - Premiered: Winter 2017
 - Broadcast: Mondays at 00:00 (JST)
 - Producers: Ultra Super Pictures, TOHO animation, Good Smile Company 
 - Licensors: None found, add some 
 - Studios: Trigger 
 - Source: Original
 - Genres: Adventure, Comedy, Fantasy, Magic, School
 - Duration: 24 min. per ep.
 - Rating: PG - Children


## Links

