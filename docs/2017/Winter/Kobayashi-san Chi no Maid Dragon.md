# Kobayashi-san Chi no Maid Dragon

![Kobayashi-san Chi no Maid Dragon](https://cdn.myanimelist.net/images/anime/5/85434l.jpg)

* Japanese:  小林さんちのメイドラゴン

## Information

 - Type: TV
 - Episodes: 13
 - Status: Finished Airing
 - Aired: Jan 12, 2017 to Apr 6, 2017
 - Premiered: Winter 2017
 - Broadcast: Thursdays at 00:00 (JST)
 - Producers: Lantis, Pony Canyon, ABC Animation 
 - Licensors: Funimation 
 - Studios: Kyoto Animation 
 - Source: Manga
 - Genres: Slice of Life, Comedy, Fantasy
 - Duration: 24 min. per ep.
 - Rating: PG-13 - Teens 13 or older


## Links

