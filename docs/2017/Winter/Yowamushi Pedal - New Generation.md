# Yowamushi Pedal: New Generation

![Yowamushi Pedal: New Generation](https://cdn.myanimelist.net/images/anime/4/83782l.jpg)

* Japanese:  弱虫ペダル NEW GENERATION

## Information

 - Type: TV
 - Episodes: 25
 - Status: Finished Airing
 - Aired: Jan 10, 2017 to Jun 27, 2017
 - Premiered: Winter 2017
 - Broadcast: Tuesdays at 01:35 (JST)
 - Producers: TV Tokyo, Shogakukan Productions, TOHO animation, Akita Shoten 
 - Licensors: None found, add some 
 - Studios: TMS Entertainment 
 - Source: Manga
 - Genres: Comedy, Sports, Drama, Shounen
 - Duration: 23 min. per ep.
 - Rating: PG-13 - Teens 13 or older


## Links

