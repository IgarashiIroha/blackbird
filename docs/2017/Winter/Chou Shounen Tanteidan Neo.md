# Chou Shounen Tanteidan Neo

![Chou Shounen Tanteidan Neo](https://cdn.myanimelist.net/images/anime/4/83182l.jpg)

* Japanese:  超・少年探偵団NEO

## Information

 - Type: TV
 - Episodes: 13
 - Status: Finished Airing
 - Aired: Jan 2, 2017 to Mar 27, 2017
 - Premiered: Winter 2017
 - Broadcast: Mondays at 21:55 (JST)
 - Producers: Tokyo MX 
 - Licensors: None found, add some 
 - Studios: DLE 
 - Source: Novel
 - Genres: Mystery
 - Duration: 4 min. per ep.
 - Rating: PG-13 - Teens 13 or older


## Links

