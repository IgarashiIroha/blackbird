# Idol Jihen

![Idol Jihen](https://cdn.myanimelist.net/images/anime/7/83624l.jpg)

* Japanese:  アイドル事変

## Information

 - Type: TV
 - Episodes: 12
 - Status: Finished Airing
 - Aired: Jan 8, 2017 to Mar 26, 2017
 - Premiered: Winter 2017
 - Broadcast: Sundays at 23:30 (JST)
 - Producers: BS Fuji, MAGES., Docomo Anime Store, bilibili, Crunchyroll SC Anime Fund 
 - Licensors: None found, add some 
 - Studios: MAPPA, Studio VOLN 
 - Source: Original
 - Genres: Music
 - Duration: 23 min. per ep.
 - Rating: PG-13 - Teens 13 or older


## Links

