# Fuuka

![Fuuka](https://cdn.myanimelist.net/images/anime/8/83735l.jpg)

* Japanese:  風夏

## Information

 - Type: TV
 - Episodes: 12
 - Status: Finished Airing
 - Aired: Jan 6, 2017 to Mar 24, 2017
 - Premiered: Winter 2017
 - Broadcast: Fridays at 22:00 (JST)
 - Producers: Studio Tulip, Magic Capsule, Warner Bros. Japan, flying DOG 
 - Licensors: Funimation 
 - Studios: Diomedea 
 - Source: Manga
 - Genres: Drama, Ecchi, Music, Romance, School, Shounen
 - Duration: 24 min. per ep.
 - Rating: R+ - Mild Nudity


## Links

