# Rewrite 2nd Season

![Rewrite 2nd Season](https://cdn.myanimelist.net/images/anime/13/83930l.jpg)

* Japanese:  Rewrite 2ndシーズン

## Information

 - Type: TV
 - Episodes: 11
 - Status: Finished Airing
 - Aired: Jan 14, 2017 to Mar 25, 2017
 - Premiered: Winter 2017
 - Broadcast: Saturdays at 23:30 (JST)
 - Producers: Aniplex, Mainichi Broadcasting System, Movic, Visual Art&#039;s, Magic Capsule, ASCII Media Works, Bushiroad, Tokyo MX, BS11, Lawson 
 - Licensors: None found, add some 
 - Studios: 8bit 
 - Source: Visual novel
 - Genres: Action, Military, Comedy, Supernatural, Romance, Fantasy
 - Duration: 24 min. per ep.
 - Rating: PG-13 - Teens 13 or older


## Links

