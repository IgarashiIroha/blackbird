# Hand Shakers

![Hand Shakers](https://cdn.myanimelist.net/images/anime/7/83395l.jpg)

* Japanese:  ハンドシェイカー

## Information

 - Type: TV
 - Episodes: 12
 - Status: Finished Airing
 - Aired: Jan 11, 2017 to Mar 29, 2017
 - Premiered: Winter 2017
 - Broadcast: Wednesdays at 00:30 (JST)
 - Producers: Frontier Works, Media Factory, Movic, Sony Music Communications, Medicos Entertainment, Kadokawa Media House 
 - Licensors: Funimation 
 - Studios: GoHands 
 - Source: Original
 - Genres: Action
 - Duration: 23 min. per ep.
 - Rating: PG-13 - Teens 13 or older


## Links

