# Piace: Watashi no Italian

![Piace: Watashi no Italian](https://cdn.myanimelist.net/images/anime/8/83098l.jpg)

* Japanese:  ピアシェ～私のイタリアン～

## Information

 - Type: TV
 - Episodes: 12
 - Status: Finished Airing
 - Aired: Jan 11, 2017 to Mar 29, 2017
 - Premiered: Winter 2017
 - Broadcast: Wednesdays at 22:30 (JST)
 - Producers: Pony Canyon, Studio NOIX, comico 
 - Licensors: None found, add some 
 - Studios: Zero-G 
 - Source: Web manga
 - Genres: Slice of Life, Comedy
 - Duration: 4 min. per ep.
 - Rating: PG-13 - Teens 13 or older


## Links

