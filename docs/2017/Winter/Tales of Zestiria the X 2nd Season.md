# Tales of Zestiria the X 2nd Season

![Tales of Zestiria the X 2nd Season](https://cdn.myanimelist.net/images/anime/2/82039l.jpg)

* Japanese:  テイルズ オブ ゼスティリア ザ クロス 第2期

## Information

 - Type: TV
 - Episodes: 13
 - Status: Finished Airing
 - Aired: Jan 8, 2017 to Apr 29, 2017
 - Premiered: Winter 2017
 - Broadcast: Sundays at 23:00 (JST)
 - Producers: Bandai Visual, Lantis, Bandai Namco Entertainment, Anime Consortium Japan 
 - Licensors: Funimation 
 - Studios: ufotable 
 - Source: Game
 - Genres: Action, Adventure, Magic, Fantasy
 - Duration: 23 min. per ep.
 - Rating: PG-13 - Teens 13 or older


## Links

