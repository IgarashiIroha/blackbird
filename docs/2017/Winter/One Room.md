# One Room

![One Room](https://cdn.myanimelist.net/images/anime/9/83777l.jpg)

* Japanese:  One Room

## Information

 - Type: TV
 - Episodes: 12
 - Status: Finished Airing
 - Aired: Jan 11, 2017 to Mar 29, 2017
 - Premiered: Winter 2017
 - Broadcast: Wednesdays at 22:40 (JST)
 - Producers: Smiral Animation 
 - Licensors: None found, add some 
 - Studios: Typhoon Graphics 
 - Source: Original
 - Genres: Slice of Life
 - Duration: 4 min. per ep.
 - Rating: PG-13 - Teens 13 or older


## Links

