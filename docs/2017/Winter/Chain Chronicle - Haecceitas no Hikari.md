# Chain Chronicle: Haecceitas no Hikari

![Chain Chronicle: Haecceitas no Hikari](https://cdn.myanimelist.net/images/anime/10/83740l.jpg)

* Japanese:  チェインクロニクル～ヘクセイタスの閃〈ひかり〉～

## Information

 - Type: TV
 - Episodes: 12
 - Status: Finished Airing
 - Aired: Jan 8, 2017 to Mar 26, 2017
 - Premiered: Winter 2017
 - Broadcast: Sundays at 02:29 (JST)
 - Producers: Bandai Visual, Sotsu, Sega, flying DOG, Slowcurve, Showgate, Sound Team Don Juan, Hakuhodo DY Music & Pictures, Sammy, Pazzy Entertainment 
 - Licensors: Funimation 
 - Studios: Telecom Animation Film, Graphinica 
 - Source: Game
 - Genres: Action, Adventure, Fantasy, Magic
 - Duration: 24 min. per ep.
 - Rating: PG-13 - Teens 13 or older


## Links

