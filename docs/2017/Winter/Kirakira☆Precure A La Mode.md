# Kirakira☆Precure A La Mode

![Kirakira☆Precure A La Mode](https://cdn.myanimelist.net/images/anime/12/88612l.jpg)

* Japanese:  キラキラ☆プリキュアアラモード

## Information

 - Type: TV
 - Episodes: 49
 - Status: Finished Airing
 - Aired: Feb 5, 2017 to Jan 28, 2018
 - Premiered: Winter 2017
 - Broadcast: Sundays at 08:30 (JST)
 - Producers: Asatsu DK, Asahi Broadcasting, ABC Animation 
 - Licensors: None found, add some 
 - Studios: Toei Animation 
 - Source: Original
 - Genres: Action, Fantasy, Magic, Shoujo, Slice of Life
 - Duration: 24 min. per ep.
 - Rating: G - All Ages


## Links

