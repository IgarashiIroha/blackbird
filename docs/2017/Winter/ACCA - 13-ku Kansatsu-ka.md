# ACCA: 13-ku Kansatsu-ka

![ACCA: 13-ku Kansatsu-ka](https://cdn.myanimelist.net/images/anime/3/83776l.jpg)

* Japanese:  ACCA 13区監察課

## Information

 - Type: TV
 - Episodes: 12
 - Status: Finished Airing
 - Aired: Jan 10, 2017 to Mar 28, 2017
 - Premiered: Winter 2017
 - Broadcast: Tuesdays at 23:00 (JST)
 - Producers: Bandai Visual, Square Enix, Lantis, Nihon Ad Systems, Banpresto, Medicos Entertainment, BS11, Sony PCL, Contents Seed 
 - Licensors: Funimation 
 - Studios: Madhouse 
 - Source: Manga
 - Genres: Mystery, Police, Drama, Fantasy, Seinen
 - Duration: 24 min. per ep.
 - Rating: PG-13 - Teens 13 or older


## Links

