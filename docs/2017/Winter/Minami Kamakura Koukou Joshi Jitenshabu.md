# Minami Kamakura Koukou Joshi Jitenshabu

![Minami Kamakura Koukou Joshi Jitenshabu](https://cdn.myanimelist.net/images/anime/10/83345l.jpg)

* Japanese:  南鎌倉高校女子自転車部

## Information

 - Type: TV
 - Episodes: 12
 - Status: Finished Airing
 - Aired: Jan 7, 2017 to Mar 25, 2017
 - Premiered: Winter 2017
 - Broadcast: Saturdays at 00:00 (JST)
 - Producers: Genco, AT-X, Exit Tunes, Duckbill Entertainment, Shanghai Tiantan Culture & Media, NADA Holdings, My Theater D.D., Crunchyroll SC Anime Fund 
 - Licensors: None found, add some 
 - Studios: J.C.Staff, A.C.G.T. 
 - Source: Manga
 - Genres: Sports, School, Shounen
 - Duration: 23 min. per ep.
 - Rating: PG-13 - Teens 13 or older


## Links

