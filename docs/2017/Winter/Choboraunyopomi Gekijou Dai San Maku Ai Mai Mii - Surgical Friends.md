# Choboraunyopomi Gekijou Dai San Maku Ai Mai Mii: Surgical Friends

![Choboraunyopomi Gekijou Dai San Maku Ai Mai Mii: Surgical Friends](https://cdn.myanimelist.net/images/anime/11/83846l.jpg)

* Japanese:  ちょぼらうにょぽみ劇場第三幕『あいまいみー～Surgical Friends～』

## Information

 - Type: TV
 - Episodes: 12
 - Status: Finished Airing
 - Aired: Jan 3, 2017 to Mar 21, 2017
 - Premiered: Winter 2017
 - Broadcast: Tuesdays at 23:30 (JST)
 - Producers: Dream Creation, On-Lead 
 - Licensors: None found, add some 
 - Studios: Seven 
 - Source: 4-koma manga
 - Genres: Slice of Life, Comedy
 - Duration: 3 min. per ep.
 - Rating: PG-13 - Teens 13 or older


## Links

