# Nanbaka 2

![Nanbaka 2](https://cdn.myanimelist.net/images/anime/9/83263l.jpg)

* Japanese:  ナンバカ 2期

## Information

 - Type: ONA
 - Episodes: 12
 - Status: Finished Airing
 - Aired: Jan 4, 2017 to Mar 22, 2017
 - Producers: comico 
 - Licensors: Funimation 
 - Studios: Satelight 
 - Source: Web manga
 - Genres: Action, Comedy, Drama
 - Duration: 23 min. per ep.
 - Rating: PG-13 - Teens 13 or older


## Links

- [[Refrain] Nanbaka 2 - 26 VOSTFR [720p]](https://uptobox.com/5xo59sddoy8r)
- [[Refrain] Nanbaka 2 - 26 VOSTFR [1080p]](https://uptobox.com/ys4iugxjujrk)
- [[Refrain] Nanbaka 2 - 25 VOSTFR [720p]](https://uptobox.com/huaeuahc19ig)
- [[Refrain] Nanbaka 2 - 25 VOSTFR [1080p]](https://uptobox.com/lsl4ndujo9ep)
- [[Refrain] Nanbaka 2 - 24 VOSTFR [720p]](https://uptobox.com/nds8g3arep4d)
- [[Refrain] Nanbaka 2 - 24 VOSTFR [1080p]](https://uptobox.com/uisdqmq74qy7)
- [[Refrain] Nanbaka 2 - 23 VOSTFR [720p]](https://uptobox.com/u032qqotdd0m)
- [[Refrain] Nanbaka 2 - 23 VOSTFR [1080p]](https://uptobox.com/ejt22qeraf89)
- [[Refrain] Nanbaka 2 - 22 VOSTFR [720p]](https://uptobox.com/9q7z6pf7k4dc)
- [[Refrain] Nanbaka 2 - 22 VOSTFR [1080p]](https://uptobox.com/m396q01zx8k4)
- [[Refrain] Nanbaka 2 - 21 VOSTFR [720p]](https://uptobox.com/20zjcktrjqaa)
- [[Refrain] Nanbaka 2 - 21 VOSTFR [1080p]](https://uptobox.com/lk4bc96aq0a6)
- [[Refrain] Nanbaka 2 - 20 VOSTFR [720p]](https://uptobox.com/vgziiadtkfq9)
- [[Refrain] Nanbaka 2 - 20 VOSTFR [1080p]](https://uptobox.com/hgxdbn5xjgq3)
- [[Refrain] Nanbaka 2 - 19 VOSTFR [720p]](https://uptobox.com/gtawhjdklxyu)
- [[Refrain] Nanbaka 2 - 19 VOSTFR [1080p]](https://uptobox.com/bik1a9xqht9l)
- [[Refrain] Nanbaka 2 - 18 VOSTFR [720p]](https://uptobox.com/ausa6iiu7fg0)
- [[Refrain] Nanbaka 2 - 18 VOSTFR [1080p]](https://uptobox.com/79h41gzdiz70)
- [[Refrain] Nanbaka 2 - 17 VOSTFR [720p]](https://uptobox.com/ffn6a9w8qohg)
- [[Refrain] Nanbaka 2 - 17 VOSTFR [1080p]](https://uptobox.com/3gda1uutowa6)
- [[Refrain] Nanbaka 2 - 16 VOSTFR [720p]](https://uptobox.com/yvef8i2uijcq)
- [[Refrain] Nanbaka 2 - 16 VOSTFR [1080p]](https://uptobox.com/511r7fqg0hxp)
- [[Refrain] Nanbaka 2 - 15 VOSTFR [720p]](https://uptobox.com/7kq2a6gcwwn2)
- [[Refrain] Nanbaka 2 - 15 VOSTFR [1080p]](https://uptobox.com/aheo25w8ki9h)
- [[Refrain] Nanbaka 2 - 14 VOSTFR [720p]](https://uptobox.com/m2vl7jfyw1ii)
- [[Refrain] Nanbaka 2 - 14 VOSTFR [1080p]](https://uptobox.com/rdixw970xw0u)
