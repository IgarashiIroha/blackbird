# Onihei

![Onihei](https://cdn.myanimelist.net/images/anime/2/83758l.jpg)

* Japanese:  鬼平

## Information

 - Type: TV
 - Episodes: 13
 - Status: Finished Airing
 - Aired: Jan 10, 2017 to Apr 4, 2017
 - Premiered: Winter 2017
 - Broadcast: Tuesdays at 02:05 (JST)
 - Producers: TV Tokyo, TMS Entertainment, Bungeishunjuu, Jidaigeki Channel 
 - Licensors: None found, add some 
 - Studios: Studio M2 
 - Source: Novel
 - Genres: Historical, Seinen
 - Duration: 23 min. per ep.
 - Rating: R - 17+ (violence & profanity)


## Links

