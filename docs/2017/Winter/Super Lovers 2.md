# Super Lovers 2

![Super Lovers 2](https://cdn.myanimelist.net/images/anime/9/83807l.jpg)

* Japanese:  SUPER LOVERS（スーパーラヴァーズ）  2

## Information

 - Type: TV
 - Episodes: 10
 - Status: Finished Airing
 - Aired: Jan 12, 2017 to Mar 16, 2017
 - Premiered: Winter 2017
 - Broadcast: Thursdays at 23:30 (JST)
 - Producers: Kadokawa Shoten, Movic, AT-X, DAX Production, Nippon Columbia, KlockWorx, Sony Music Communications, Tokyo MX, Lawson HMV Entertainment, TV Saitama 
 - Licensors: None found, add some 
 - Studios: Studio Deen 
 - Source: Manga
 - Genres: Slice of Life, Comedy, Drama, Romance, Shounen Ai
 - Duration: 24 min. per ep.
 - Rating: PG-13 - Teens 13 or older


## Links

