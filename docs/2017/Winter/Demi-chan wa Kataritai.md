# Demi-chan wa Kataritai

![Demi-chan wa Kataritai](https://cdn.myanimelist.net/images/anime/8/83417l.jpg)

* Japanese:  亜人ちゃんは語りたい

## Information

 - Type: TV
 - Episodes: 12
 - Status: Finished Airing
 - Aired: Jan 8, 2017 to Mar 26, 2017
 - Premiered: Winter 2017
 - Broadcast: Sundays at 00:00 (JST)
 - Producers: Aniplex, Kodansha, Kanetsu Co., LTD. 
 - Licensors: Funimation 
 - Studios: A-1 Pictures 
 - Source: Manga
 - Genres: Comedy, Vampire, Fantasy, School, Seinen
 - Duration: 23 min. per ep.
 - Rating: PG-13 - Teens 13 or older


## Links

