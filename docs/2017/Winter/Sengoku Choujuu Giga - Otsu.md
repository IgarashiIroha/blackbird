# Sengoku Choujuu Giga: Otsu

![Sengoku Choujuu Giga: Otsu](https://cdn.myanimelist.net/images/anime/7/83737l.jpg)

* Japanese:  戦国鳥獣戯画~乙~

## Information

 - Type: TV
 - Episodes: 13
 - Status: Finished Airing
 - Aired: Jan 15, 2017 to Apr 9, 2017
 - Premiered: Winter 2017
 - Broadcast: Sundays at 01:57 (JST)
 - Producers: King Records 
 - Licensors: None found, add some 
 - Studios: ILCA 
 - Source: Original
 - Genres: Historical, Demons, Supernatural, Samurai
 - Duration: 3 min. per ep.
 - Rating: PG-13 - Teens 13 or older


## Links

