# Marginal#4: Kiss kara Tsukuru Big Bang

![Marginal#4: Kiss kara Tsukuru Big Bang](https://cdn.myanimelist.net/images/anime/10/83799l.jpg)

* Japanese:  MARGINAL#4 KISSから創造るBig Bang

## Information

 - Type: TV
 - Episodes: 12
 - Status: Finished Airing
 - Aired: Jan 12, 2017 to Mar 30, 2017
 - Premiered: Winter 2017
 - Broadcast: Thursdays at 23:30 (JST)
 - Producers: Sotsu, Movic, Warner Bros. Japan, KlockWorx 
 - Licensors: None found, add some 
 - Studios: J.C.Staff 
 - Source: Music
 - Genres: Music, Comedy, School
 - Duration: 23 min. per ep.
 - Rating: PG-13 - Teens 13 or older


## Links

