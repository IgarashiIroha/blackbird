# Youjo Senki

![Youjo Senki](https://cdn.myanimelist.net/images/anime/5/82890l.jpg)

* Japanese:  幼女戦記

## Information

 - Type: TV
 - Episodes: 12
 - Status: Finished Airing
 - Aired: Jan 6, 2017 to Mar 31, 2017
 - Premiered: Winter 2017
 - Broadcast: Fridays at 22:00 (JST)
 - Producers: Media Factory, AT-X, Enterbrain, Sony Music Communications 
 - Licensors: Funimation 
 - Studios: Nut 
 - Source: Light novel
 - Genres: Magic, Military
 - Duration: 24 min. per ep.
 - Rating: R - 17+ (violence & profanity)


## Links

