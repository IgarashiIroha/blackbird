# ChäoS;Child

![ChäoS;Child](https://cdn.myanimelist.net/images/anime/1310/90137l.jpg)

* Japanese:  CHAOS;CHILD

## Information

 - Type: TV
 - Episodes: 12
 - Status: Finished Airing
 - Aired: Jan 11, 2017 to Mar 29, 2017
 - Premiered: Winter 2017
 - Broadcast: Wednesdays at 23:30 (JST)
 - Producers: Kadokawa Shoten, AT-X, MAGES., Glovision, 5pb. 
 - Licensors: Funimation 
 - Studios: Silver Link. 
 - Source: Visual novel
 - Genres: Sci-Fi, Mystery, Psychological, Supernatural
 - Duration: 23 min. per ep.
 - Rating: R - 17+ (violence & profanity)


## Links

