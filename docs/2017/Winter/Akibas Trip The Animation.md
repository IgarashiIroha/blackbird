# Akiba&#039;s Trip The Animation

![Akiba&#039;s Trip The Animation](https://cdn.myanimelist.net/images/anime/9/83185l.jpg)

* Japanese:  AKIBA&#039;S TRIP THE ANIMATION

## Information

 - Type: TV
 - Episodes: 13
 - Status: Finished Airing
 - Aired: Jan 4, 2017 to Mar 29, 2017
 - Premiered: Winter 2017
 - Broadcast: Wednesdays at 22:30 (JST)
 - Producers: AT-X, DLE, Studio Mausu, BS Fuji, Tokyo MX, Evil Line Records, Docomo Anime Store, bilibili, DMM.com, Sotsu Music Publishing, Exa International 
 - Licensors: Funimation 
 - Studios: Gonzo 
 - Source: Game
 - Genres: Action, Comedy, Supernatural, Ecchi
 - Duration: 23 min. per ep.
 - Rating: PG-13 - Teens 13 or older


## Links

