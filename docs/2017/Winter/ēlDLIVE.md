# ēlDLIVE

![ēlDLIVE](https://cdn.myanimelist.net/images/anime/8/82348l.jpg)

* Japanese:  エルドライブ 【ēlDLIVE】

## Information

 - Type: TV
 - Episodes: 12
 - Status: Finished Airing
 - Aired: Jan 8, 2017 to Mar 26, 2017
 - Premiered: Winter 2017
 - Broadcast: Sundays at 22:00 (JST)
 - Producers: Yomiuri Telecasting, d-rights, Half H.P Studio, Shueisha, Tokuma Japan Communications, Tencent Japan 
 - Licensors: Funimation 
 - Studios: Studio Pierrot 
 - Source: Manga
 - Genres: Action, Sci-Fi, Space, Police, Shounen
 - Duration: 24 min. per ep.
 - Rating: PG-13 - Teens 13 or older


## Links

