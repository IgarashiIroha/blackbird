# Urara Meirochou

![Urara Meirochou](https://cdn.myanimelist.net/images/anime/6/84119l.jpg)

* Japanese:  うらら迷路帖

## Information

 - Type: TV
 - Episodes: 12
 - Status: Finished Airing
 - Aired: Jan 6, 2017 to Mar 24, 2017
 - Premiered: Winter 2017
 - Broadcast: Fridays at 01:58 (JST)
 - Producers: TBS, Magic Capsule, Houbunsha, NBCUniversal Entertainment Japan, Nichion 
 - Licensors: Sentai Filmworks 
 - Studios: J.C.Staff 
 - Source: 4-koma manga
 - Genres: Comedy, Fantasy, Seinen, Slice of Life
 - Duration: 24 min. per ep.
 - Rating: PG-13 - Teens 13 or older


## Links

