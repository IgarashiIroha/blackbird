# Schoolgirl Strikers: Animation Channel

![Schoolgirl Strikers: Animation Channel](https://cdn.myanimelist.net/images/anime/4/82560l.jpg)

* Japanese:  スクールガールストライカーズ Animation Channel

## Information

 - Type: TV
 - Episodes: 13
 - Status: Finished Airing
 - Aired: Jan 7, 2017 to Apr 1, 2017
 - Premiered: Winter 2017
 - Broadcast: Saturdays at 00:30 (JST)
 - Producers: Square Enix, Movic, Warner Bros. Japan, KlockWorx, BS11, Egg Firm 
 - Licensors: None found, add some 
 - Studios: J.C.Staff 
 - Source: Game
 - Genres: Action, Sci-Fi, School
 - Duration: 24 min. per ep.
 - Rating: PG-13 - Teens 13 or older


## Links

