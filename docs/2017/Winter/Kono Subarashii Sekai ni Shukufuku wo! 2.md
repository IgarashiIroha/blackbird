# Kono Subarashii Sekai ni Shukufuku wo! 2

![Kono Subarashii Sekai ni Shukufuku wo! 2](https://cdn.myanimelist.net/images/anime/2/83188l.jpg)

* Japanese:  この素晴らしい世界に祝福を！ 2

## Information

 - Type: TV
 - Episodes: 10
 - Status: Finished Airing
 - Aired: Jan 12, 2017 to Mar 16, 2017
 - Premiered: Winter 2017
 - Broadcast: Thursdays at 01:05 (JST)
 - Producers: Kadokawa Shoten, Nippon Columbia, KlockWorx, Sony Music Communications, Toranoana, 81 Produce, Kadokawa Media House 
 - Licensors: None found, add some 
 - Studios: Studio Deen 
 - Source: Light novel
 - Genres: Adventure, Comedy, Parody, Supernatural, Magic, Fantasy
 - Duration: 23 min. per ep.
 - Rating: PG-13 - Teens 13 or older


## Links

