# Gintama.

![Gintama.](https://cdn.myanimelist.net/images/anime/3/83528l.jpg)

* Japanese:  銀魂。

## Information

 - Type: TV
 - Episodes: 12
 - Status: Finished Airing
 - Aired: Jan 9, 2017 to Mar 27, 2017
 - Premiered: Winter 2017
 - Broadcast: Mondays at 01:35 (JST)
 - Producers: TV Tokyo, Aniplex, Dentsu, Shueisha 
 - Licensors: None found, add some 
 - Studios: Bandai Namco Pictures 
 - Source: Manga
 - Genres: Action, Comedy, Historical, Parody, Samurai, Sci-Fi, Shounen
 - Duration: 24 min. per ep.
 - Rating: PG-13 - Teens 13 or older


## Links

