# Chiruran: Nibun no Ichi

![Chiruran: Nibun no Ichi](https://cdn.myanimelist.net/images/anime/6/82948l.jpg)

* Japanese:  ちるらん にぶんの壱

## Information

 - Type: TV
 - Episodes: 12
 - Status: Finished Airing
 - Aired: Jan 10, 2017 to Mar 28, 2017
 - Premiered: Winter 2017
 - Broadcast: Tuesdays at 01:00 (JST)
 - Producers: DAX Production, KBS, Coamix, Warner Music Japan, Smiral Animation 
 - Licensors: None found, add some 
 - Studios: LandQ studios 
 - Source: Manga
 - Genres: Action, Comedy, Historical
 - Duration: 2 min. per ep.
 - Rating: G - All Ages


## Links

