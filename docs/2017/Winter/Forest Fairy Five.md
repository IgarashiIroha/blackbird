# Forest Fairy Five

![Forest Fairy Five](https://cdn.myanimelist.net/images/anime/9/83713l.jpg)

* Japanese:  FOREST FAIRY FIVE

## Information

 - Type: TV
 - Episodes: 13
 - Status: Finished Airing
 - Aired: Jan 6, 2017 to Mar 31, 2017
 - Premiered: Winter 2017
 - Broadcast: Fridays at 20:00 (JST)
 - Producers: Showgate, Tokyo MX 
 - Licensors: None found, add some 
 - Studios: None found, add some 
 - Source: Picture book
 - Genres: Music, Comedy, Fantasy
 - Duration: 10 min. per ep.
 - Rating: PG - Children


## Links

