# Masamune-kun no Revenge

![Masamune-kun no Revenge](https://cdn.myanimelist.net/images/anime/12/83709l.jpg)

* Japanese:  政宗くんのリベンジ

## Information

 - Type: TV
 - Episodes: 12
 - Status: Finished Airing
 - Aired: Jan 5, 2017 to Mar 23, 2017
 - Premiered: Winter 2017
 - Broadcast: Thursdays at 22:30 (JST)
 - Producers: Sotsu, Lantis, AT-X, KlockWorx, BS Fuji, Tokyo MX, Ichijinsha, Furyu, Crunchyroll SC Anime Fund 
 - Licensors: Funimation 
 - Studios: Silver Link. 
 - Source: Manga
 - Genres: Harem, Comedy, Romance, School, Shounen
 - Duration: 24 min. per ep.
 - Rating: PG-13 - Teens 13 or older


## Links

