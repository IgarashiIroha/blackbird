# Nyanko Days

![Nyanko Days](https://cdn.myanimelist.net/images/anime/10/83933l.jpg)

* Japanese:  にゃんこデイズ

## Information

 - Type: TV
 - Episodes: 12
 - Status: Finished Airing
 - Aired: Jan 8, 2017 to Mar 26, 2017
 - Premiered: Winter 2017
 - Broadcast: Sundays at 22:25 (JST)
 - Producers: Quatre Stella 
 - Licensors: None found, add some 
 - Studios: EMT² 
 - Source: 4-koma manga
 - Genres: Slice of Life, Comedy
 - Duration: 2 min. per ep.
 - Rating: PG-13 - Teens 13 or older


## Links

