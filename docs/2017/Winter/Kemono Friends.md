# Kemono Friends

![Kemono Friends](https://cdn.myanimelist.net/images/anime/2/83518l.jpg)

* Japanese:  けものフレンズ

## Information

 - Type: TV
 - Episodes: 12
 - Status: Finished Airing
 - Aired: Jan 11, 2017 to Mar 29, 2017
 - Premiered: Winter 2017
 - Broadcast: Wednesdays at 01:35 (JST)
 - Producers: TV Tokyo, Victor Entertainment, AT-X, KlockWorx, flying DOG, Bushiroad, Age Global Networks, famima.com, Docomo Anime Store, Just Production 
 - Licensors: Crunchyroll 
 - Studios: Yaoyorozu 
 - Source: Game
 - Genres: Adventure, Comedy, Fantasy
 - Duration: 24 min. per ep.
 - Rating: PG - Children


## Links

