# Gabriel DropOut

![Gabriel DropOut](https://cdn.myanimelist.net/images/anime/9/82590l.jpg)

* Japanese:  ガヴリールドロップアウト

## Information

 - Type: TV
 - Episodes: 12
 - Status: Finished Airing
 - Aired: Jan 9, 2017 to Mar 27, 2017
 - Premiered: Winter 2017
 - Broadcast: Mondays at 22:30 (JST)
 - Producers: Media Factory, AT-X, Sony Music Communications, Kadokawa Media House, NTT Plala 
 - Licensors: None found, add some 
 - Studios: Doga Kobo 
 - Source: Manga
 - Genres: Comedy, Demons, Supernatural, School, Shounen
 - Duration: 23 min. per ep.
 - Rating: PG-13 - Teens 13 or older


## Links

