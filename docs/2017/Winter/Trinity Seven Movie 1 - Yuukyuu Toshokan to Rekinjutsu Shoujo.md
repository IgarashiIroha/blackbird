# Trinity Seven Movie 1: Eternity Library to Alchemic Girl

![Trinity Seven Movie 1: Eternity Library to Alchemic Girl](https://cdn.myanimelist.net/images/anime/6/83172l.jpg)

* Japanese:  劇場版 トリニティセブン －悠久図書館〈エターニティライブラリー〉と錬金術少女〈アルケミックガール〉－

## Information

 - Type: Movie
 - Episodes: 1
 - Status: Finished Airing
 - Aired: Feb 25, 2017
 - Producers: Avex Pictures 
 - Licensors: None found, add some 
 - Studios: Seven Arcs Pictures 
 - Source: Manga
 - Genres: Action, Comedy, Ecchi, Fantasy, Harem, Magic, Romance, School, Shounen
 - Duration: 55 min.
 - Rating: PG-13 - Teens 13 or older


## Links

- [[Refrain] Trinity Seven Movie 1 - Yuukyuu Toshokan to Rekinjutsu Shoujo - 00 VOSTFR [720p]](https://uptobox.com/r534at2mp5jy)
- [[Refrain] Trinity Seven Movie 1 - Yuukyuu Toshokan to Rekinjutsu Shoujo - 00 VOSTFR [1080p]](https://uptobox.com/w36tf3ddispa)
