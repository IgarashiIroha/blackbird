# Seiren

![Seiren](https://cdn.myanimelist.net/images/anime/1295/95681l.jpg)

* Japanese:  セイレン

## Information

 - Type: TV
 - Episodes: 12
 - Status: Finished Airing
 - Aired: Jan 6, 2017 to Mar 24, 2017
 - Premiered: Winter 2017
 - Broadcast: Fridays at 02:28 (JST)
 - Producers: Pony Canyon, TBS, DAX Production, Pony Canyon Enterprise, Stardust Promotion, GYAO! 
 - Licensors: None found, add some 
 - Studios: Studio Gokumi, AXsiZ 
 - Source: Original
 - Genres: Romance, School
 - Duration: 24 min. per ep.
 - Rating: PG-13 - Teens 13 or older


## Links

