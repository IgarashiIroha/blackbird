# Shouwa Genroku Rakugo Shinjuu: Sukeroku Futatabi-hen

![Shouwa Genroku Rakugo Shinjuu: Sukeroku Futatabi-hen](https://cdn.myanimelist.net/images/anime/10/82947l.jpg)

* Japanese:  昭和元禄落語心中～助六再び篇～

## Information

 - Type: TV
 - Episodes: 12
 - Status: Finished Airing
 - Aired: Jan 7, 2017 to Mar 25, 2017
 - Premiered: Winter 2017
 - Broadcast: Saturdays at 02:25 (JST)
 - Producers: Mainichi Broadcasting System, Kodansha, DAX Production, King Records 
 - Licensors: None found, add some 
 - Studios: Studio Deen 
 - Source: Manga
 - Genres: Drama, Historical, Josei
 - Duration: 24 min. per ep.
 - Rating: PG-13 - Teens 13 or older


## Links

