# Yami Shibai 4

![Yami Shibai 4](https://cdn.myanimelist.net/images/anime/12/83113l.jpg)

* Japanese:  闇芝居 4期

## Information

 - Type: TV
 - Episodes: 13
 - Status: Finished Airing
 - Aired: Jan 16, 2017 to Mar 27, 2017
 - Premiered: Winter 2017
 - Broadcast: Mondays at 02:05 (JST)
 - Producers: TV Tokyo 
 - Licensors: None found, add some 
 - Studios: ILCA 
 - Source: Original
 - Genres: Dementia, Horror, Supernatural
 - Duration: 4 min. per ep.
 - Rating: PG-13 - Teens 13 or older


## Links

