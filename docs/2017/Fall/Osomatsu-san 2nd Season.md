# Osomatsu-san 2nd Season

![Osomatsu-san 2nd Season](https://cdn.myanimelist.net/images/anime/13/88328l.jpg)

* Japanese:  おそ松さん 第2期

## Information

 - Type: TV
 - Episodes: 25
 - Status: Finished Airing
 - Aired: Oct 3, 2017 to Mar 27, 2018
 - Premiered: Fall 2017
 - Broadcast: Tuesdays at 01:35 (JST)
 - Producers: TV Tokyo, Dentsu, Rakuonsha, AT-X, Avex Pictures, Daiichi Shokai 
 - Licensors: Viz Media 
 - Studios: Studio Pierrot 
 - Source: Manga
 - Genres: Comedy, Parody
 - Duration: 23 min. per ep.
 - Rating: R - 17+ (violence & profanity)


## Links

