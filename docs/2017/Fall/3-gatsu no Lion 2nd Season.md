# 3-gatsu no Lion 2nd Season

![3-gatsu no Lion 2nd Season](https://cdn.myanimelist.net/images/anime/3/88469l.jpg)

* Japanese:  3月のライオン 第2シリーズ

## Information

 - Type: TV
 - Episodes: 22
 - Status: Finished Airing
 - Aired: Oct 14, 2017 to Mar 31, 2018
 - Premiered: Fall 2017
 - Broadcast: Saturdays at 23:00 (JST)
 - Producers: Aniplex, Dentsu, NHK, Hakusensha, Asmik Ace, Toy&#039;s Factory 
 - Licensors: Aniplex of America 
 - Studios: Shaft 
 - Source: Manga
 - Genres: Drama, Game, Seinen, Slice of Life
 - Duration: 25 min. per ep.
 - Rating: PG-13 - Teens 13 or older


## Links

- [[Refrain] 3-gatsu no Lion 2nd Season - 01 VOSTFR [1080p]](https://uptobox.com/sckr09fna7jq)
- [[Refrain] 3-gatsu no Lion 2nd Season - 01 VOSTFR [720p]](https://uptobox.com/t5m1bajyrou3)
- [[Refrain] 3-gatsu no Lion 2nd Season - 02 VOSTFR [1080p]](https://uptobox.com/0j7mx36g92xf)
- [[Refrain] 3-gatsu no Lion 2nd Season - 02 VOSTFR [720p]](https://uptobox.com/z4kfuvw27jyt)
- [[Refrain] 3-gatsu no Lion 2nd Season - 03 VOSTFR [1080p]](https://uptobox.com/pufitw4w26da)
- [[Refrain] 3-gatsu no Lion 2nd Season - 03 VOSTFR [720p]](https://uptobox.com/ya01stf5lw2z)
- [[Refrain] 3-gatsu no Lion 2nd Season - 04 VOSTFR [1080p]](https://uptobox.com/xm9qdefaw4if)
- [[Refrain] 3-gatsu no Lion 2nd Season - 04 VOSTFR [720p]](https://uptobox.com/9epz8z9in2vd)
- [[Refrain] 3-gatsu no Lion 2nd Season - 05 VOSTFR [1080p]](https://uptobox.com/66l2hyvd9s0e)
- [[Refrain] 3-gatsu no Lion 2nd Season - 05 VOSTFR [720p]](https://uptobox.com/c87oefp0vu6o)
- [[Refrain] 3-gatsu no Lion 2nd Season - 06 VOSTFR [1080p]](https://uptobox.com/qx7v410q4ixd)
- [[Refrain] 3-gatsu no Lion 2nd Season - 06 VOSTFR [720p]](https://uptobox.com/npc5r1y93bqq)
- [[Refrain] 3-gatsu no Lion 2nd Season - 07 VOSTFR [1080p]](https://uptobox.com/hs5m47a2f7ml)
- [[Refrain] 3-gatsu no Lion 2nd Season - 07 VOSTFR [720p]](https://uptobox.com/3v3idzy5e5hy)
- [[Refrain] 3-gatsu no Lion 2nd Season - 08 VOSTFR [1080p]](https://uptobox.com/gk82z34hcrbq)
- [[Refrain] 3-gatsu no Lion 2nd Season - 08 VOSTFR [720p]](https://uptobox.com/p2ujsuagdv23)
- [[Refrain] 3-gatsu no Lion 2nd Season - 09 VOSTFR [1080p]](https://uptobox.com/5oghc5gcd4w7)
- [[Refrain] 3-gatsu no Lion 2nd Season - 09 VOSTFR [720p]](https://uptobox.com/p28tn2qiqlc4)
- [[Refrain] 3-gatsu no Lion 2nd Season - 10 VOSTFR [1080p]](https://uptobox.com/t2wkrhh60cwa)
- [[Refrain] 3-gatsu no Lion 2nd Season - 10 VOSTFR [720p]](https://uptobox.com/2rlorff937aj)
- [[Refrain] 3-gatsu no Lion 2nd Season - 11 VOSTFR [1080p]](https://uptobox.com/ae5eu24ntqr7)
- [[Refrain] 3-gatsu no Lion 2nd Season - 11 VOSTFR [720p]](https://uptobox.com/sd832p2mud6g)
- [[Refrain] 3-gatsu no Lion 2nd Season - 12 VOSTFR [1080p]](https://uptobox.com/gxaqygm6tm56)
- [[Refrain] 3-gatsu no Lion 2nd Season - 12 VOSTFR [720p]](https://uptobox.com/aqrg01wnr4wk)
- [[Refrain] 3-gatsu no Lion 2nd Season - 13 VOSTFR [1080p]](https://uptobox.com/3eilwoe2e96f)
- [[Refrain] 3-gatsu no Lion 2nd Season - 13 VOSTFR [720p]](https://uptobox.com/uvqiul0tvyyr)
- [[Refrain] 3-gatsu no Lion 2nd Season - 14 VOSTFR [1080p]](https://uptobox.com/i3kdazzc9rzd)
- [[Refrain] 3-gatsu no Lion 2nd Season - 14 VOSTFR [720p]](https://uptobox.com/o2dk30hdh186)
- [[Refrain] 3-gatsu no Lion 2nd Season - 15 VOSTFR [1080p]](https://uptobox.com/pkl44g5ygynd)
- [[Refrain] 3-gatsu no Lion 2nd Season - 15 VOSTFR [720p]](https://uptobox.com/zbluq9zy8avs)
- [[Refrain] 3-gatsu no Lion 2nd Season - 16 VOSTFR [1080p]](https://uptobox.com/v3humsiz2klq)
- [[Refrain] 3-gatsu no Lion 2nd Season - 16 VOSTFR [720p]](https://uptobox.com/8p10wxuarlmi)
- [[Refrain] 3-gatsu no Lion 2nd Season - 17 VOSTFR [1080p]](https://uptobox.com/x7ic71sl28xy)
- [[Refrain] 3-gatsu no Lion 2nd Season - 17 VOSTFR [720p]](https://uptobox.com/j0iolz4w336h)
- [[Refrain] 3-gatsu no Lion 2nd Season - 18 VOSTFR [1080p]](https://uptobox.com/tjlf2i1fu1vk)
- [[Refrain] 3-gatsu no Lion 2nd Season - 18 VOSTFR [720p]](https://uptobox.com/gcio9x1qk195)
- [[Refrain] 3-gatsu no Lion 2nd Season - 19 VOSTFR [1080p]](https://uptobox.com/uafupfrcf32m)
- [[Refrain] 3-gatsu no Lion 2nd Season - 19 VOSTFR [720p]](https://uptobox.com/spxj1wzgd1et)
- [[Refrain] 3-gatsu no Lion 2nd Season - 20 VOSTFR [1080p]](https://uptobox.com/fg0micehoh7s)
- [[Refrain] 3-gatsu no Lion 2nd Season - 20 VOSTFR [720p]](https://uptobox.com/0j94cikjc1nd)
- [[Refrain] 3-gatsu no Lion 2nd Season - 21 VOSTFR [1080p]](https://uptobox.com/ieervulm77zv)
- [[Refrain] 3-gatsu no Lion 2nd Season - 21 VOSTFR [720p]](https://uptobox.com/4t66apjiq9mp)
- [[Refrain] 3-gatsu no Lion 2nd Season - 22 VOSTFR [1080p]](https://uptobox.com/aphrmtwrvkaw)
- [[Refrain] 3-gatsu no Lion 2nd Season - 22 VOSTFR [720p]](https://uptobox.com/snbaazp91ywh)
