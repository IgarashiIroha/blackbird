# Tsukipro The Animation

![Tsukipro The Animation](https://cdn.myanimelist.net/images/anime/4/88060l.jpg)

* Japanese:  TSUKIPRO THE ANIMATION

## Information

 - Type: TV
 - Episodes: 13
 - Status: Finished Airing
 - Aired: Oct 4, 2017 to Dec 27, 2017
 - Premiered: Fall 2017
 - Broadcast: Wednesdays at 22:30 (JST)
 - Producers: Studio Pierrot, Movic, Bandai Namco Entertainment, Docomo Anime Store, AZ Creative 
 - Licensors: None found, add some 
 - Studios: PRA 
 - Source: Music
 - Genres: Music
 - Duration: 23 min. per ep.
 - Rating: PG-13 - Teens 13 or older


## Links

