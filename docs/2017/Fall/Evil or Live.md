# Evil or Live

![Evil or Live](https://cdn.myanimelist.net/images/anime/1333/90138l.jpg)

* Japanese:  EVIL OR LIVE

## Information

 - Type: TV
 - Episodes: 12
 - Status: Finished Airing
 - Aired: Oct 11, 2017 to Jan 2, 2018
 - Premiered: Fall 2017
 - Broadcast: Wednesdays at 01:41 (JST)
 - Producers: Emon 
 - Licensors: None found, add some 
 - Studios: Haoliners Animation League 
 - Source: Web manga
 - Genres: Psychological, School
 - Duration: 23 min. per ep.
 - Rating: R - 17+ (violence & profanity)


## Links

