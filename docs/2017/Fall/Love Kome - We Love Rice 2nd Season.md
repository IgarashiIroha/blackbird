# Love Kome: We Love Rice 2nd Season

![Love Kome: We Love Rice 2nd Season](https://cdn.myanimelist.net/images/anime/4/88115l.jpg)

* Japanese:  ラブ米 -WE LOVE RICE-

## Information

 - Type: TV
 - Episodes: 12
 - Status: Finished Airing
 - Aired: Oct 6, 2017 to Dec 23, 2017
 - Premiered: Fall 2017
 - Broadcast: Fridays at 02:59 (JST)
 - Producers: Just Production 
 - Licensors: None found, add some 
 - Studios: Encourage Films 
 - Source: Original
 - Genres: Slice of Life
 - Duration: 5 min. per ep.
 - Rating: PG-13 - Teens 13 or older


## Links

