# Hitori no Shita: The Outcast 2nd Season

![Hitori no Shita: The Outcast 2nd Season](https://cdn.myanimelist.net/images/anime/4/88698l.jpg)

* Japanese:  一人之下 THE OUTCAST 2ndシーズン

## Information

 - Type: ONA
 - Episodes: 24
 - Status: Finished Airing
 - Aired: Oct 27, 2017 to May 18, 2018
 - Producers: Tencent Animation & Comics 
 - Licensors: None found, add some 
 - Studios: Haoliners Animation League 
 - Source: Manga
 - Genres: Action, Super Power, Supernatural
 - Duration: 24 min. per ep.
 - Rating: R - 17+ (violence & profanity)


## Links

