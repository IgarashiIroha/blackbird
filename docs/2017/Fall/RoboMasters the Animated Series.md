# RoboMasters the Animated Series

![RoboMasters the Animated Series](https://cdn.myanimelist.net/images/anime/11/86297l.jpg)

* Japanese:  ROBOMASTERS THE ANIMATED SERIES

## Information

 - Type: TV
 - Episodes: 6
 - Status: Finished Airing
 - Aired: Oct 13, 2017 to Nov 17, 2017
 - Premiered: Fall 2017
 - Broadcast: Fridays at 22:30 (JST)
 - Producers: DJI 
 - Licensors: None found, add some 
 - Studios: Gonzo, DandeLion Animation Studio 
 - Source: Original
 - Genres: Action, Mecha, School
 - Duration: 24 min. per ep.
 - Rating: PG-13 - Teens 13 or older


## Links

