# ClassicaLoid 2nd Season

![ClassicaLoid 2nd Season](https://cdn.myanimelist.net/images/anime/2/88678l.jpg)

* Japanese:  クラシカロイド 第2シリーズ

## Information

 - Type: TV
 - Episodes: 25
 - Status: Finished Airing
 - Aired: Oct 7, 2017 to Mar 24, 2018
 - Premiered: Fall 2017
 - Broadcast: Saturdays at 17:35 (JST)
 - Producers: NHK, Bandai Namco Pictures 
 - Licensors: Sentai Filmworks 
 - Studios: Sunrise 
 - Source: Original
 - Genres: Music, Comedy
 - Duration: 24 min. per ep.
 - Rating: PG-13 - Teens 13 or older


## Links

