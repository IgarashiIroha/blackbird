# Cardfight!! Vanguard G: Z

![Cardfight!! Vanguard G: Z](https://cdn.myanimelist.net/images/anime/13/88063l.jpg)

* Japanese:  CARDFIGHT!! ヴァンガードG Z

## Information

 - Type: TV
 - Episodes: 24
 - Status: Finished Airing
 - Aired: Oct 8, 2017 to Apr 1, 2018
 - Premiered: Fall 2017
 - Broadcast: Sundays at 10:00 (JST)
 - Producers: None found, add some 
 - Licensors: None found, add some 
 - Studios: OLM 
 - Source: Original
 - Genres: Action, Game, Shounen
 - Duration: 24 min. per ep.
 - Rating: PG-13 - Teens 13 or older


## Links

