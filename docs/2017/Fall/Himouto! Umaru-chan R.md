# Himouto! Umaru-chan R

![Himouto! Umaru-chan R](https://cdn.myanimelist.net/images/anime/10/89671l.jpg)

* Japanese:  干物妹！うまるちゃんR

## Information

 - Type: TV
 - Episodes: 12
 - Status: Finished Airing
 - Aired: Oct 8, 2017 to Dec 24, 2017
 - Premiered: Fall 2017
 - Broadcast: Sundays at 23:30 (JST)
 - Producers: AT-X, Flex Comix, TOHO animation, Good Smile Company, Shueisha, bilibili, BS11, Jinnan Studio, RAY 
 - Licensors: Sentai Filmworks 
 - Studios: Doga Kobo 
 - Source: Manga
 - Genres: Comedy, School, Seinen, Slice of Life
 - Duration: 23 min. per ep.
 - Rating: PG-13 - Teens 13 or older


## Links

