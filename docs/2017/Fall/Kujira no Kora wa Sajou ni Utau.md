# Kujira no Kora wa Sajou ni Utau

![Kujira no Kora wa Sajou ni Utau](https://cdn.myanimelist.net/images/anime/4/86661l.jpg)

* Japanese:  クジラの子らは砂上に歌う

## Information

 - Type: TV
 - Episodes: 12
 - Status: Finished Airing
 - Aired: Oct 8, 2017 to Dec 24, 2017
 - Premiered: Fall 2017
 - Broadcast: Sundays at 23:00 (JST)
 - Producers: Bandai Visual, Lantis, Magic Capsule, Hakuhodo DY Music & Pictures, Akita Shoten, Sony PCL, Contents Seed 
 - Licensors: None found, add some 
 - Studios: J.C.Staff 
 - Source: Manga
 - Genres: Drama, Fantasy, Mystery, Sci-Fi, Shoujo
 - Duration: 23 min. per ep.
 - Rating: PG-13 - Teens 13 or older


## Links

