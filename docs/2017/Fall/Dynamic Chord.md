# Dynamic Chord

![Dynamic Chord](https://cdn.myanimelist.net/images/anime/1750/95683l.jpg)

* Japanese:  DYNAMIC CHORD

## Information

 - Type: TV
 - Episodes: 12
 - Status: Finished Airing
 - Aired: Oct 6, 2017 to Dec 22, 2017
 - Premiered: Fall 2017
 - Broadcast: Fridays at 01:58 (JST)
 - Producers: TBS, Nippon Columbia, Zack Promotion, Overlap, Nichion, Asgard 
 - Licensors: Sentai Filmworks 
 - Studios: Studio Pierrot, Pierrot Plus 
 - Source: Visual novel
 - Genres: Music
 - Duration: 24 min. per ep.
 - Rating: PG-13 - Teens 13 or older


## Links

