# Infini-T Force

![Infini-T Force](https://cdn.myanimelist.net/images/anime/6/88430l.jpg)

* Japanese:  Infini-T Force

## Information

 - Type: TV
 - Episodes: 12
 - Status: Finished Airing
 - Aired: Oct 3, 2017 to Dec 19, 2017
 - Premiered: Fall 2017
 - Broadcast: Wednesdays at 01:59 (JST)
 - Producers: Yomiuri Telecasting, Shochiku, HoriPro, Pony Canyon, Digital Frontier, Bushiroad, Nippon Television Network, Children&#039;s Playground Entertainment, Nippon Television Music, Hulu, Kadokawa, Sapporo Television Broadcasting, Miyagi Television Broadcasting, Shizuoka Daiichi Television, Chukyo TV Broadcasting, Hiroshima Television, Fukuoka Broadcasting System 
 - Licensors: Viz Media 
 - Studios: Tatsunoko Production 
 - Source: Original
 - Genres: Action, Sci-Fi, Super Power
 - Duration: 22 min. per ep.
 - Rating: PG-13 - Teens 13 or older


## Links

