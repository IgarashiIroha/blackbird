# Inuyashiki

![Inuyashiki](https://cdn.myanimelist.net/images/anime/7/88471l.jpg)

* Japanese:  いぬやしき

## Information

 - Type: TV
 - Episodes: 11
 - Status: Finished Airing
 - Aired: Oct 13, 2017 to Dec 22, 2017
 - Premiered: Fall 2017
 - Broadcast: Fridays at 00:55 (JST)
 - Producers: Aniplex, Dentsu, Kodansha, Fuji TV, DMM pictures 
 - Licensors: None found, add some 
 - Studios: MAPPA 
 - Source: Manga
 - Genres: Action, Drama, Psychological, Sci-Fi, Seinen
 - Duration: 22 min. per ep.
 - Rating: R - 17+ (violence & profanity)


## Links

