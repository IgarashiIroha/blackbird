# Sengoku Night Blood

![Sengoku Night Blood](https://cdn.myanimelist.net/images/anime/4/88329l.jpg)

* Japanese:  戦刻ナイトブラッド

## Information

 - Type: TV
 - Episodes: 12
 - Status: Finished Airing
 - Aired: Oct 3, 2017 to Dec 26, 2017
 - Premiered: Fall 2017
 - Broadcast: Tuesdays at 23:00 (JST)
 - Producers: TV Asahi, Marvelous, Pony Canyon, JY Animation, Live Viewing Japan 
 - Licensors: None found, add some 
 - Studios: Typhoon Graphics 
 - Source: Game
 - Genres: Historical, Romance, Fantasy
 - Duration: 23 min. per ep.
 - Rating: PG-13 - Teens 13 or older


## Links

