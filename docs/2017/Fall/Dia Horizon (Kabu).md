# Dia Horizon (Kabu)

![Dia Horizon (Kabu)](https://cdn.myanimelist.net/images/anime/6/87933l.jpg)

* Japanese:  ディアホライゾン（被）

## Information

 - Type: TV
 - Episodes: 12
 - Status: Finished Airing
 - Aired: Oct 3, 2017 to Dec 19, 2017
 - Premiered: Fall 2017
 - Broadcast: Tuesdays at 01:00 (JST)
 - Producers: Square Enix 
 - Licensors: None found, add some 
 - Studios: Fanworks 
 - Source: Original
 - Genres: Action, Adventure, Magic, Fantasy
 - Duration: 3 min. per ep.
 - Rating: PG-13 - Teens 13 or older


## Links

