# Shokugeki no Souma: San no Sara

![Shokugeki no Souma: San no Sara](https://cdn.myanimelist.net/images/anime/3/88434l.jpg)

* Japanese:  食戟のソーマ 餐ノ皿

## Information

 - Type: TV
 - Episodes: 12
 - Status: Finished Airing
 - Aired: Oct 4, 2017 to Dec 20, 2017
 - Premiered: Fall 2017
 - Broadcast: Wednesdays at 00:30 (JST)
 - Producers: Lantis, Warner Bros. Japan, KlockWorx, Shueisha 
 - Licensors: None found, add some 
 - Studios: J.C.Staff 
 - Source: Manga
 - Genres: Ecchi, School, Shounen
 - Duration: 24 min. per ep.
 - Rating: PG-13 - Teens 13 or older


## Links

