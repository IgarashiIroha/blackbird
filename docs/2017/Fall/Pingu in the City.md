# Pingu in the City

![Pingu in the City](https://cdn.myanimelist.net/images/anime/1140/95174l.jpg)

* Japanese:  ピングー in ザ・シティ

## Information

 - Type: TV
 - Episodes: 26
 - Status: Finished Airing
 - Aired: Oct 7, 2017 to Mar 31, 2018
 - Premiered: Fall 2017
 - Broadcast: Saturdays at 09:20 (JST)
 - Producers: NHK, Polygon Pictures, NHK Enterprises, Mattel Creations 
 - Licensors: None found, add some 
 - Studios: DandeLion Animation Studio 
 - Source: Other
 - Genres: Comedy, Kids, Slice of Life
 - Duration: 7 min. per ep.
 - Rating: G - All Ages


## Links

