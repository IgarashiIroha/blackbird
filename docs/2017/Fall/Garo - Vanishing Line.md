# Garo: Vanishing Line

![Garo: Vanishing Line](https://cdn.myanimelist.net/images/anime/10/88236l.jpg)

* Japanese:  牙狼〈GARO〉-VANISHING LINE-

## Information

 - Type: TV
 - Episodes: 24
 - Status: Finished Airing
 - Aired: Oct 7, 2017 to Mar 30, 2018
 - Premiered: Fall 2017
 - Broadcast: Saturdays at 01:23 (JST)
 - Producers: Tohokushinsha Film Corporation 
 - Licensors: Funimation 
 - Studios: MAPPA 
 - Source: Original
 - Genres: Action, Demons, Fantasy, Supernatural
 - Duration: 24 min. per ep.
 - Rating: R - 17+ (violence & profanity)


## Links

