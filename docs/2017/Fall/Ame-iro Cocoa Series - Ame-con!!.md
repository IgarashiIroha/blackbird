# Ame-iro Cocoa Series: Ame-con!!

![Ame-iro Cocoa Series: Ame-con!!](https://cdn.myanimelist.net/images/anime/3/88225l.jpg)

* Japanese:  雨色ココアシリーズ あめこん!!

## Information

 - Type: TV
 - Episodes: 12
 - Status: Finished Airing
 - Aired: Oct 4, 2017 to Dec 20, 2017
 - Premiered: Fall 2017
 - Broadcast: Wednesdays at 22:25 (JST)
 - Producers: None found, add some 
 - Licensors: None found, add some 
 - Studios: EMT² 
 - Source: Digital manga
 - Genres: Slice of Life, Comedy
 - Duration: 2 min. per ep.
 - Rating: PG-13 - Teens 13 or older


## Links

