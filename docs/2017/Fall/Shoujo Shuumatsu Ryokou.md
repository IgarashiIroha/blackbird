# Shoujo Shuumatsu Ryokou

![Shoujo Shuumatsu Ryokou](https://cdn.myanimelist.net/images/anime/12/88321l.jpg)

* Japanese:  少女終末旅行

## Information

 - Type: TV
 - Episodes: 12
 - Status: Finished Airing
 - Aired: Oct 6, 2017 to Dec 22, 2017
 - Premiered: Fall 2017
 - Broadcast: Fridays at 21:30 (JST)
 - Producers: AT-X, Shinchosha, Kadokawa Media House, Kadokawa 
 - Licensors: Sentai Filmworks 
 - Studios: White Fox 
 - Source: Web manga
 - Genres: Adventure, Mystery, Sci-Fi, Slice of Life
 - Duration: 23 min. per ep.
 - Rating: PG-13 - Teens 13 or older


## Links

