# Houseki no Kuni

![Houseki no Kuni](https://cdn.myanimelist.net/images/anime/3/88293l.jpg)

* Japanese:  宝石の国

## Information

 - Type: TV
 - Episodes: 12
 - Status: Finished Airing
 - Aired: Oct 7, 2017 to Dec 23, 2017
 - Premiered: Fall 2017
 - Broadcast: Saturdays at 21:30 (JST)
 - Producers: Mainichi Broadcasting System, Kodansha, Movic, AT-X, TOHO animation, Tokyo MX, Q-Tec, Kyoraku Industrial Holdings, BS11 
 - Licensors: Sentai Filmworks 
 - Studios: Orange 
 - Source: Manga
 - Genres: Action, Drama, Fantasy, Mystery, Seinen
 - Duration: 24 min. per ep.
 - Rating: PG-13 - Teens 13 or older


## Links

