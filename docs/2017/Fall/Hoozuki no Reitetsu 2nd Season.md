# Hoozuki no Reitetsu 2nd Season

![Hoozuki no Reitetsu 2nd Season](https://cdn.myanimelist.net/images/anime/8/88280l.jpg)

* Japanese:  鬼灯の冷徹 第弐期

## Information

 - Type: TV
 - Episodes: 13
 - Status: Finished Airing
 - Aired: Oct 8, 2017 to Dec 31, 2017
 - Premiered: Fall 2017
 - Broadcast: Sundays at 01:00 (JST)
 - Producers: Kodansha, King Records 
 - Licensors: Sentai Filmworks 
 - Studios: Studio Deen 
 - Source: Manga
 - Genres: Comedy, Demons, Supernatural, Fantasy, Seinen
 - Duration: 23 min. per ep.
 - Rating: PG-13 - Teens 13 or older


## Links

