# Ousama Game The Animation

![Ousama Game The Animation](https://cdn.myanimelist.net/images/anime/3/88190l.jpg)

* Japanese:  王様ゲーム The Animation

## Information

 - Type: TV
 - Episodes: 12
 - Status: Finished Airing
 - Aired: Oct 5, 2017 to Dec 21, 2017
 - Premiered: Fall 2017
 - Broadcast: Thursdays at 23:30 (JST)
 - Producers: AT-X, Warner Music Japan, Amuse, Hakuhodo DY Music & Pictures, Futabasha, Kakao Japan, Spacey Music Entertainment, A-Craft, Tohan Corporation 
 - Licensors: Funimation 
 - Studios: Seven 
 - Source: Novel
 - Genres: Dementia, Supernatural, School, Drama, Horror, Mystery
 - Duration: 23 min. per ep.
 - Rating: R - 17+ (violence & profanity)


## Links

