# Yuuki Yuuna wa Yuusha de Aru: Washio Sumi no Shou

![Yuuki Yuuna wa Yuusha de Aru: Washio Sumi no Shou](https://cdn.myanimelist.net/images/anime/7/83012l.jpg)

* Japanese:  結城友奈は勇者である -鷲尾須美の章-

## Information

 - Type: TV
 - Episodes: 6
 - Status: Finished Airing
 - Aired: Oct 7, 2017 to Nov 11, 2017
 - Premiered: Fall 2017
 - Broadcast: Fridays at 01:55 (JST)
 - Producers: Dentsu, Mainichi Broadcasting System, Pony Canyon, Movic, KlockWorx, Kadokawa 
 - Licensors: Ponycan USA 
 - Studios: Studio Gokumi 
 - Source: Light novel
 - Genres: Slice of Life, Drama, Magic, Fantasy
 - Duration: 24 min. per ep.
 - Rating: PG-13 - Teens 13 or older


## Links

