# Urahara

![Urahara](https://cdn.myanimelist.net/images/anime/12/88432l.jpg)

* Japanese:  URAHARA

## Information

 - Type: TV
 - Episodes: 12
 - Status: Finished Airing
 - Aired: Oct 4, 2017 to Dec 20, 2017
 - Premiered: Fall 2017
 - Broadcast: Wednesdays at 20:00 (JST)
 - Producers: VAP, bilibili, Crunchyroll SC Anime Fund 
 - Licensors: Funimation 
 - Studios: Shirogumi, EMT² 
 - Source: Web manga
 - Genres: Comedy, Fantasy, Psychological, Sci-Fi
 - Duration: 23 min. per ep.
 - Rating: PG-13 - Teens 13 or older


## Links

