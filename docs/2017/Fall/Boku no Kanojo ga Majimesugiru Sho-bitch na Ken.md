# Boku no Kanojo ga Majimesugiru Sho-bitch na Ken

![Boku no Kanojo ga Majimesugiru Sho-bitch na Ken](https://cdn.myanimelist.net/images/anime/12/87623l.jpg)

* Japanese:  僕の彼女がマジメ過ぎるしょびっちな件

## Information

 - Type: TV
 - Episodes: 10
 - Status: Finished Airing
 - Aired: Oct 12, 2017 to Dec 14, 2017
 - Premiered: Fall 2017
 - Broadcast: Thursdays at 00:30 (JST)
 - Producers: AT-X, Nippon Columbia, KlockWorx, Sony Music Communications, Toranoana, Glovision, Sammy, Kadokawa Media House, Production Ace, Kadokawa 
 - Licensors: Sentai Filmworks 
 - Studios: Diomedea, Studio Blanc 
 - Source: Web manga
 - Genres: Harem, Comedy, Romance, Ecchi, School
 - Duration: 23 min. per ep.
 - Rating: PG-13 - Teens 13 or older


## Links

