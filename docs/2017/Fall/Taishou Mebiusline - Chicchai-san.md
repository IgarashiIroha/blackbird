# Taishou Mebiusline: Chicchai-san

![Taishou Mebiusline: Chicchai-san](https://cdn.myanimelist.net/images/anime/12/88029l.jpg)

* Japanese:  大正メビウスライン ちっちゃいさん

## Information

 - Type: TV
 - Episodes: 12
 - Status: Finished Airing
 - Aired: Oct 6, 2017 to Dec 22, 2017
 - Premiered: Fall 2017
 - Broadcast: Fridays at 21:54 (JST)
 - Producers: None found, add some 
 - Licensors: None found, add some 
 - Studios: Studio A-CAT 
 - Source: Visual novel
 - Genres: Historical, Supernatural
 - Duration: 3 min. per ep.
 - Rating: PG-13 - Teens 13 or older


## Links

