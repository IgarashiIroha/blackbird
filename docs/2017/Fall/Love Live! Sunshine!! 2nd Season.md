# Love Live! Sunshine!! 2nd Season

![Love Live! Sunshine!! 2nd Season](https://cdn.myanimelist.net/images/anime/13/88322l.jpg)

* Japanese:  ラブライブ！サンシャイン!! 第2期

## Information

 - Type: TV
 - Episodes: 13
 - Status: Finished Airing
 - Aired: Oct 7, 2017 to Dec 30, 2017
 - Premiered: Fall 2017
 - Broadcast: Saturdays at 22:30 (JST)
 - Producers: Bandai Visual, Lantis, ASCII Media Works, Bushiroad 
 - Licensors: Funimation 
 - Studios: Sunrise 
 - Source: Original
 - Genres: Music, School, Slice of Life
 - Duration: 23 min. per ep.
 - Rating: PG-13 - Teens 13 or older


## Links

