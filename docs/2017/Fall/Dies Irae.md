# Dies Irae

![Dies Irae](https://cdn.myanimelist.net/images/anime/2/89682l.jpg)

* Japanese:  Dies irae

## Information

 - Type: TV
 - Episodes: 11
 - Status: Finished Airing
 - Aired: Oct 14, 2017 to Dec 23, 2017
 - Premiered: Fall 2017
 - Broadcast: Saturdays at 01:05 (JST)
 - Producers: Genco, DMM pictures, Greenwood, My Theater D.D., Brave Hearts, Crunchyroll SC Anime Fund 
 - Licensors: Funimation 
 - Studios: A.C.G.T. 
 - Source: Visual novel
 - Genres: Action, Military, Super Power, Magic
 - Duration: 23 min. per ep.
 - Rating: R - 17+ (violence & profanity)


## Links

