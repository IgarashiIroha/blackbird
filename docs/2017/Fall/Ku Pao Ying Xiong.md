# Ku Pao Ying Xiong

![Ku Pao Ying Xiong](https://cdn.myanimelist.net/images/anime/3/87942l.jpg)

* Japanese:  酷跑英雄

## Information

 - Type: TV
 - Episodes: 26
 - Status: Finished Airing
 - Aired: Sep 2, 2017 to Feb 17, 2018
 - Premiered: Fall 2017
 - Broadcast: Saturdays at 10:00 (JST)
 - Producers: Tencent Penguin Pictures 
 - Licensors: None found, add some 
 - Studios: G.CMay Animation & Film 
 - Source: Game
 - Genres: Action, Adventure, Fantasy, Game, Magic
 - Duration: 14 min. per ep.
 - Rating: None


## Links

