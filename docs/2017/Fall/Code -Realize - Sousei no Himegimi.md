# Code:Realize - Sousei no Himegimi

![Code:Realize - Sousei no Himegimi](https://cdn.myanimelist.net/images/anime/6/88465l.jpg)

* Japanese:  Code:Realize ～創世の姫君～

## Information

 - Type: TV
 - Episodes: 12
 - Status: Finished Airing
 - Aired: Oct 7, 2017 to Dec 23, 2017
 - Premiered: Fall 2017
 - Broadcast: Saturdays at 20:00 (JST)
 - Producers: Frontier Works, Lantis, AT-X, DAX Production, Asmik Ace, Sony Music Communications, Hakuhodo DY Music & Pictures, Chugai Mining, Kadokawa 
 - Licensors: Funimation 
 - Studios: M.S.C 
 - Source: Visual novel
 - Genres: Military, Historical, Romance, Fantasy, Josei
 - Duration: 23 min. per ep.
 - Rating: PG-13 - Teens 13 or older


## Links

