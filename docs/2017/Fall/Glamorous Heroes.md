# Glamorous Heroes

![Glamorous Heroes](https://cdn.myanimelist.net/images/anime/2/87661l.jpg)

* Japanese:  グラマラスヒーローズ

## Information

 - Type: TV
 - Episodes: 10
 - Status: Finished Airing
 - Aired: Oct 7, 2017 to Dec 9, 2017
 - Premiered: Fall 2017
 - Broadcast: Saturdays at 02:10 (JST)
 - Producers: Shanghai Jump Network Technology 
 - Licensors: None found, add some 
 - Studios: drop 
 - Source: Game
 - Genres: Action, Game
 - Duration: 5 min. per ep.
 - Rating: PG-13 - Teens 13 or older


## Links

