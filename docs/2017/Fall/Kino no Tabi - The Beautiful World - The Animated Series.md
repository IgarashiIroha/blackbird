# Kino no Tabi: The Beautiful World - The Animated Series

![Kino no Tabi: The Beautiful World - The Animated Series](https://cdn.myanimelist.net/images/anime/13/87235l.jpg)

* Japanese:  キノの旅 -the Beautiful World- the Animated Series

## Information

 - Type: TV
 - Episodes: 12
 - Status: Finished Airing
 - Aired: Oct 6, 2017 to Dec 22, 2017
 - Premiered: Fall 2017
 - Broadcast: Fridays at 22:00 (JST)
 - Producers: Movic, Magic Capsule, ASCII Media Works, NBCUniversal Entertainment Japan, Egg Firm, Crunchyroll SC Anime Fund 
 - Licensors: Funimation 
 - Studios: Lerche 
 - Source: Light novel
 - Genres: Action, Adventure, Slice of Life
 - Duration: 23 min. per ep.
 - Rating: R - 17+ (violence & profanity)


## Links

