# Omiai Aite wa Oshiego, Tsuyoki na, Mondaiji.

![Omiai Aite wa Oshiego, Tsuyoki na, Mondaiji.](https://cdn.myanimelist.net/images/anime/9/88326l.jpg)

* Japanese:  お見合い相手は教え子、強気な、問題児。

## Information

 - Type: TV
 - Episodes: 12
 - Status: Finished Airing
 - Aired: Oct 2, 2017 to Dec 18, 2017
 - Premiered: Fall 2017
 - Broadcast: Mondays at 01:00 (JST)
 - Producers: Studio Mausu 
 - Licensors: None found, add some 
 - Studios: Seven 
 - Source: Digital manga
 - Genres: Romance, Ecchi, School
 - Duration: 4 min. per ep.
 - Rating: R+ - Mild Nudity


## Links

