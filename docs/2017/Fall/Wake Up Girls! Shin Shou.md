# Wake Up, Girls! Shin Shou

![Wake Up, Girls! Shin Shou](https://cdn.myanimelist.net/images/anime/10/87732l.jpg)

* Japanese:  Wake Up, Girls！新章

## Information

 - Type: TV
 - Episodes: 12
 - Status: Finished Airing
 - Aired: Oct 10, 2017 to Jan 8, 2018
 - Premiered: Fall 2017
 - Broadcast: Tuesdays at 02:05 (JST)
 - Producers: TV Tokyo, Sotsu, Tatsunoko Production, AT-X, Toho, KlockWorx, DIVE II Entertainment, 81 Produce, Good Smile Company, Avex Pictures, KLab, Rakuten, Epicross 
 - Licensors: None found, add some 
 - Studios: Millepensee 
 - Source: Original
 - Genres: Music, Drama
 - Duration: 23 min. per ep.
 - Rating: PG-13 - Teens 13 or older


## Links

