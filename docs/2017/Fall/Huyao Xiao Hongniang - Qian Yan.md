# Huyao Xiao Hongniang: Qian Yan

![Huyao Xiao Hongniang: Qian Yan](https://cdn.myanimelist.net/images/anime/5/88318l.jpg)

* Japanese:  狐妖小红娘 千颜

## Information

 - Type: ONA
 - Episodes: 15
 - Status: Finished Airing
 - Aired: Feb 24, 2017 to Jun 2, 2017
 - Producers: Tencent Animation & Comics 
 - Licensors: None found, add some 
 - Studios: Haoliners Animation League 
 - Source: Manga
 - Genres: Comedy, Historical, Supernatural, Romance
 - Duration: 15 min. per ep.
 - Rating: PG-13 - Teens 13 or older


## Links

