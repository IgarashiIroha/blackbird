# Blend S

![Blend S](https://cdn.myanimelist.net/images/anime/6/88286l.jpg)

* Japanese:  ブレンド・S

## Information

 - Type: TV
 - Episodes: 12
 - Status: Finished Airing
 - Aired: Oct 8, 2017 to Dec 24, 2017
 - Premiered: Fall 2017
 - Broadcast: Sundays at 00:30 (JST)
 - Producers: Aniplex, Houbunsha, Kansai Telecasting, Lawson, Contents Seed, Kinoshita Group Holdings, Drecom 
 - Licensors: Aniplex of America 
 - Studios: A-1 Pictures 
 - Source: 4-koma manga
 - Genres: Slice of Life, Comedy
 - Duration: 23 min. per ep.
 - Rating: PG-13 - Teens 13 or older


## Links

