# Mahoutsukai no Yome

![Mahoutsukai no Yome](https://cdn.myanimelist.net/images/anime/3/88476l.jpg)

* Japanese:  魔法使いの嫁

## Information

 - Type: TV
 - Episodes: 24
 - Status: Finished Airing
 - Aired: Oct 8, 2017 to Mar 25, 2018
 - Premiered: Fall 2017
 - Broadcast: Sundays at 01:30 (JST)
 - Producers: Production I.G, Shochiku, flying DOG, Mag Garden 
 - Licensors: Funimation 
 - Studios: Wit Studio 
 - Source: Manga
 - Genres: Slice of Life, Fantasy, Magic, Shounen
 - Duration: 24 min. per ep.
 - Rating: PG-13 - Teens 13 or older


## Links

