# Cinderella Girls Gekijou 2nd Season

![Cinderella Girls Gekijou 2nd Season](https://cdn.myanimelist.net/images/anime/6/89788l.jpg)

* Japanese:  シンデレラガールズ劇場 第2期

## Information

 - Type: TV
 - Episodes: 13
 - Status: Finished Airing
 - Aired: Oct 3, 2017 to Dec 26, 2017
 - Premiered: Fall 2017
 - Broadcast: Tuesdays at 21:55 (JST)
 - Producers: Frontier Works, Half H.P Studio, Nippon Columbia, Bandai Namco Entertainment, Anime Consortium Japan 
 - Licensors: Crunchyroll 
 - Studios: Gathering, Lesprit 
 - Source: 4-koma manga
 - Genres: Slice of Life, Comedy
 - Duration: 3 min. per ep.
 - Rating: PG-13 - Teens 13 or older


## Links

