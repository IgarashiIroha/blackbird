# Yuuki Yuuna wa Yuusha de Aru: Yuusha no Shou

![Yuuki Yuuna wa Yuusha de Aru: Yuusha no Shou](https://cdn.myanimelist.net/images/anime/8/83013l.jpg)

* Japanese:  結城友奈は勇者である -勇者の章-

## Information

 - Type: TV
 - Episodes: 6
 - Status: Finished Airing
 - Aired: Nov 25, 2017 to Jan 6, 2018
 - Premiered: Fall 2017
 - Broadcast: Fridays at 01:55 (JST)
 - Producers: Dentsu, Mainichi Broadcasting System, Pony Canyon, Movic, KlockWorx, Kadokawa 
 - Licensors: Ponycan USA 
 - Studios: Studio Gokumi 
 - Source: Original
 - Genres: Slice of Life, Drama, Magic, Fantasy
 - Duration: 24 min. per ep.
 - Rating: PG-13 - Teens 13 or older


## Links

