# Animegataris

![Animegataris](https://cdn.myanimelist.net/images/anime/12/89686l.jpg)

* Japanese:  アニメガタリズ

## Information

 - Type: TV
 - Episodes: 12
 - Status: Finished Airing
 - Aired: Oct 8, 2017 to Dec 24, 2017
 - Premiered: Fall 2017
 - Broadcast: Sundays at 22:00 (JST)
 - Producers: W-Toon Studio, Yomiuri TV Enterprise, DMM.futureworks, JY Animation 
 - Licensors: Funimation 
 - Studios: WAO World 
 - Source: Original
 - Genres: Comedy, Parody, School
 - Duration: 23 min. per ep.
 - Rating: PG-13 - Teens 13 or older


## Links

