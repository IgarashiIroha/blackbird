# Kekkai Sensen & Beyond

![Kekkai Sensen & Beyond](https://cdn.myanimelist.net/images/anime/3/88282l.jpg)

* Japanese:  血界戦線 & BEYOND

## Information

 - Type: TV
 - Episodes: 12
 - Status: Finished Airing
 - Aired: Oct 8, 2017 to Dec 24, 2017
 - Premiered: Fall 2017
 - Broadcast: Sundays at 03:08 (JST)
 - Producers: Nihon Ad Systems, Mainichi Broadcasting System, TOHO animation, Shueisha 
 - Licensors: Funimation 
 - Studios: Bones 
 - Source: Manga
 - Genres: Action, Comedy, Fantasy, Shounen, Super Power, Supernatural, Vampire
 - Duration: 24 min. per ep.
 - Rating: R - 17+ (violence & profanity)


## Links

