# Just Because!

![Just Because!](https://cdn.myanimelist.net/images/anime/10/88234l.jpg)

* Japanese:  Just Because!

## Information

 - Type: TV
 - Episodes: 12
 - Status: Finished Airing
 - Aired: Oct 5, 2017 to Dec 28, 2017
 - Premiered: Fall 2017
 - Broadcast: Thursdays at 21:00 (JST)
 - Producers: Sotsu, AT-X, BS Fuji, NBCUniversal Entertainment Japan, Tokyo MX, Moonbell, INCS toenter 
 - Licensors: Sentai Filmworks 
 - Studios: Pine Jam 
 - Source: Original
 - Genres: Slice of Life, Drama, Romance, School
 - Duration: 23 min. per ep.
 - Rating: PG-13 - Teens 13 or older


## Links

