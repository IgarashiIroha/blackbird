# Osake wa Fuufu ni Natte kara

![Osake wa Fuufu ni Natte kara](https://cdn.myanimelist.net/images/anime/11/88433l.jpg)

* Japanese:  お酒は夫婦になってから

## Information

 - Type: TV
 - Episodes: 13
 - Status: Finished Airing
 - Aired: Oct 4, 2017 to Dec 27, 2017
 - Premiered: Fall 2017
 - Broadcast: Wednesdays at 01:00 (JST)
 - Producers: VAP, Yomiko Advertising, Tokyo MX, GYAO! 
 - Licensors: None found, add some 
 - Studios: Creators in Pack 
 - Source: Web manga
 - Genres: Comedy, Romance, Slice of Life
 - Duration: 3 min. per ep.
 - Rating: PG-13 - Teens 13 or older


## Links

