# Konohana Kitan

![Konohana Kitan](https://cdn.myanimelist.net/images/anime/3/88431l.jpg)

* Japanese:  このはな綺譚

## Information

 - Type: TV
 - Episodes: 12
 - Status: Finished Airing
 - Aired: Oct 4, 2017 to Dec 20, 2017
 - Premiered: Fall 2017
 - Broadcast: Wednesdays at 20:00 (JST)
 - Producers: Genco, Lantis, AT-X, GAGA, Memory-Tech, Studio Mausu, DeNA, Kadokawa, Melonbooks 
 - Licensors: Funimation 
 - Studios: Lerche 
 - Source: Manga
 - Genres: Slice of Life, Fantasy, Seinen, Shoujo Ai
 - Duration: 23 min. per ep.
 - Rating: PG-13 - Teens 13 or older


## Links

