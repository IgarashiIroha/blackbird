# UQ Holder!: Mahou Sensei Negima! 2

![UQ Holder!: Mahou Sensei Negima! 2](https://cdn.myanimelist.net/images/anime/6/88354l.jpg)

* Japanese:  UQ HOLDER! ～魔法先生ネギま！2～

## Information

 - Type: TV
 - Episodes: 12
 - Status: Finished Airing
 - Aired: Oct 3, 2017 to Dec 19, 2017
 - Premiered: Fall 2017
 - Broadcast: Tuesdays at 00:00 (JST)
 - Producers: Kodansha, Lawson HMV Entertainment, Egg Firm, GYAO! 
 - Licensors: Sentai Filmworks 
 - Studios: J.C.Staff 
 - Source: Manga
 - Genres: Action, Sci-Fi, Magic, Fantasy, Shounen
 - Duration: 23 min. per ep.
 - Rating: PG-13 - Teens 13 or older


## Links

