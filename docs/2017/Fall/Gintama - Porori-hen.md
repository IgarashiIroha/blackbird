# Gintama.: Porori-hen

![Gintama.: Porori-hen](https://cdn.myanimelist.net/images/anime/11/88325l.jpg)

* Japanese:  銀魂。ポロリ編

## Information

 - Type: TV
 - Episodes: 13
 - Status: Finished Airing
 - Aired: Oct 2, 2017 to Dec 25, 2017
 - Premiered: Fall 2017
 - Broadcast: Mondays at 01:35 (JST)
 - Producers: TV Tokyo, Aniplex, Dentsu, Shueisha 
 - Licensors: None found, add some 
 - Studios: Bandai Namco Pictures 
 - Source: Manga
 - Genres: Action, Sci-Fi, Comedy, Historical, Parody, Samurai, Shounen
 - Duration: 23 min. per ep.
 - Rating: PG-13 - Teens 13 or older


## Links

