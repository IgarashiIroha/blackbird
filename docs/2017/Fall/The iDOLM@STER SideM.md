# The iDOLM@STER SideM

![The iDOLM@STER SideM](https://cdn.myanimelist.net/images/anime/3/88467l.jpg)

* Japanese:  アイドルマスター SideM

## Information

 - Type: TV
 - Episodes: 13
 - Status: Finished Airing
 - Aired: Oct 7, 2017 to Dec 30, 2017
 - Premiered: Fall 2017
 - Broadcast: Saturdays at 23:30 (JST)
 - Producers: Aniplex, Lantis, Bandai Namco Live Creative, Bandai Namco Entertainment, ABC Animation 
 - Licensors: None found, add some 
 - Studios: A-1 Pictures 
 - Source: Game
 - Genres: Music
 - Duration: 24 min. per ep.
 - Rating: PG-13 - Teens 13 or older


## Links

