# Imouto sae Ireba Ii.

![Imouto sae Ireba Ii.](https://cdn.myanimelist.net/images/anime/10/88472l.jpg)

* Japanese:  妹さえいればいい。

## Information

 - Type: TV
 - Episodes: 12
 - Status: Finished Airing
 - Aired: Oct 8, 2017 to Dec 24, 2017
 - Premiered: Fall 2017
 - Broadcast: Sundays at 22:30 (JST)
 - Producers: Bandai Visual, Lantis, Shochiku, Bushiroad, Hakuhodo DY Music & Pictures, BS11, Shogakukan 
 - Licensors: Funimation 
 - Studios: Silver Link. 
 - Source: Light novel
 - Genres: Comedy, Romance
 - Duration: 23 min. per ep.
 - Rating: R+ - Mild Nudity


## Links

