# Itsudatte Bokura no Koi wa 10 cm Datta.

![Itsudatte Bokura no Koi wa 10 cm Datta.](https://cdn.myanimelist.net/images/anime/2/89554l.jpg)

* Japanese:  いつだって僕らの恋は10センチだった。

## Information

 - Type: TV
 - Episodes: 6
 - Status: Finished Airing
 - Aired: Nov 25, 2017 to Dec 30, 2017
 - Premiered: Fall 2017
 - Broadcast: Saturdays at 00:00 (JST)
 - Producers: Aniplex, Movic, Magic Capsule, Lawson, Music Ray&#039;n, Kadokawa, INCS toenter, Yoshimoto Creative Agency 
 - Licensors: Aniplex of America 
 - Studios: Lay-duce 
 - Source: Music
 - Genres: Comedy, Drama, Romance, School
 - Duration: 23 min. per ep.
 - Rating: PG-13 - Teens 13 or older


## Links

