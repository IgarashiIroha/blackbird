# Two Car

![Two Car](https://cdn.myanimelist.net/images/anime/3/88291l.jpg)

* Japanese:  つうかあ

## Information

 - Type: TV
 - Episodes: 12
 - Status: Finished Airing
 - Aired: Oct 8, 2017 to Dec 24, 2017
 - Premiered: Fall 2017
 - Broadcast: Sundays at 00:00 (JST)
 - Producers: Lantis, AT-X, KlockWorx, Sony Music Communications, BS Fuji, 81 Produce, Tokyo MX, Hakuhodo DY Music & Pictures, Furyu, Kadokawa, D-techno 
 - Licensors: None found, add some 
 - Studios: Silver Link. 
 - Source: Original
 - Genres: Sports
 - Duration: 23 min. per ep.
 - Rating: PG-13 - Teens 13 or older


## Links

