# Juuni Taisen

![Juuni Taisen](https://cdn.myanimelist.net/images/anime/5/87684l.jpg)

* Japanese:  十二大戦

## Information

 - Type: TV
 - Episodes: 12
 - Status: Finished Airing
 - Aired: Oct 3, 2017 to Dec 19, 2017
 - Premiered: Fall 2017
 - Broadcast: Tuesdays at 23:30 (JST)
 - Producers: Sotsu, Mainichi Broadcasting System, AT-X, Studio Mausu, Tokyo MX, Avex Pictures, Shueisha, BS11, GYAO!, A-Sketch 
 - Licensors: Funimation 
 - Studios: Graphinica 
 - Source: Novel
 - Genres: Action
 - Duration: 24 min. per ep.
 - Rating: R - 17+ (violence & profanity)


## Links

