# Rurouni Kenshin: Meiji Kenkaku Romantan - Tsuioku-hen

![Rurouni Kenshin: Meiji Kenkaku Romantan - Tsuioku-hen](https://cdn.myanimelist.net/images/anime/1899/94562l.jpg)

* Japanese:  るろうに剣心―明治剣客浪漫譚―追憶編

## Information

 - Type: OVA
 - Episodes: 4
 - Status: Finished Airing
 - Aired: Feb 20, 1999 to Sep 22, 1999
 - Producers: Aniplex 
 - Licensors: ADV Films, Aniplex of America 
 - Studios: Studio Deen 
 - Source: Manga
 - Genres: Action, Historical, Drama, Romance, Martial Arts, Samurai, Shounen
 - Duration: 30 min. per ep.
 - Rating: R - 17+ (violence & profanity)


## Links

- [[Refrain] Rurouni Kenshin - Meiji Kenkaku Romantan - Tsuioku-hen - 01 VF VOSTFR [1080p]](https://uptobox.com/xbwe9pw2m3ol)
- [[Refrain] Rurouni Kenshin - Meiji Kenkaku Romantan - Tsuioku-hen - 01 VF VOSTFR [720p]](https://uptobox.com/xmvmzn5s0qjx)
- [[Refrain] Rurouni Kenshin - Meiji Kenkaku Romantan - Tsuioku-hen - 02 VF VOSTFR [1080p]](https://uptobox.com/t4xn3ekk201f)
- [[Refrain] Rurouni Kenshin - Meiji Kenkaku Romantan - Tsuioku-hen - 02 VF VOSTFR [720p]](https://uptobox.com/2pscukjqhl7b)
- [[Refrain] Rurouni Kenshin - Meiji Kenkaku Romantan - Tsuioku-hen - 03 VF VOSTFR [1080p]](https://uptobox.com/ul69x2jmlkht)
- [[Refrain] Rurouni Kenshin - Meiji Kenkaku Romantan - Tsuioku-hen - 03 VF VOSTFR [720p]](https://uptobox.com/u9pk7sp5u3fo)
- [[Refrain] Rurouni Kenshin - Meiji Kenkaku Romantan - Tsuioku-hen - 04 VF VOSTFR [1080p]](https://uptobox.com/z7ug11d5mlh6)
- [[Refrain] Rurouni Kenshin - Meiji Kenkaku Romantan - Tsuioku-hen - 04 VF VOSTFR [720p]](https://uptobox.com/j8awnud5fp3c)
