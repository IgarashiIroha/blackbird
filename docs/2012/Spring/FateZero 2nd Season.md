# Fate/Zero 2nd Season

![Fate/Zero 2nd Season](https://cdn.myanimelist.net/images/anime/8/41125l.jpg)

* Japanese:  フェイト/ゼロ 2ndシーズン

## Information

 - Type: TV
 - Episodes: 12
 - Status: Finished Airing
 - Aired: Apr 8, 2012 to Jun 24, 2012
 - Premiered: Spring 2012
 - Broadcast: Sundays at 00:00 (JST)
 - Producers: Aniplex, Nitroplus, Seikaisha, Notes 
 - Licensors: Aniplex of America 
 - Studios: ufotable 
 - Source: Light novel
 - Genres: Action, Supernatural, Magic, Fantasy
 - Duration: 24 min. per ep.
 - Rating: R - 17+ (violence & profanity)


## Links

- [[Refrain] FateZero 2nd Season - 01 VOSTFR [720p]](https://uptobox.com/07etnc2gx50u)
- [[Refrain] FateZero 2nd Season - 02 VOSTFR [720p]](https://uptobox.com/gcokt7j56gjs)
- [[Refrain] FateZero 2nd Season - 03 VOSTFR [720p]](https://uptobox.com/eto07okd913z)
- [[Refrain] FateZero 2nd Season - 04 VOSTFR [720p]](https://uptobox.com/e94bxgahm9dr)
- [[Refrain] FateZero 2nd Season - 05 VOSTFR [720p]](https://uptobox.com/mhzibq8k1o1m)
- [[Refrain] FateZero 2nd Season - 06 VOSTFR [720p]](https://uptobox.com/f5b1tj1muzj7)
- [[Refrain] FateZero 2nd Season - 07 VOSTFR [720p]](https://uptobox.com/sxpvocnjqqc7)
- [[Refrain] FateZero 2nd Season - 08 VOSTFR [720p]](https://uptobox.com/4sy1a6km66rj)
- [[Refrain] FateZero 2nd Season - 09 VOSTFR [720p]](https://uptobox.com/mo3b4yeub4zl)
- [[Refrain] FateZero 2nd Season - 10 VOSTFR [720p]](https://uptobox.com/qui8vwetr8yw)
- [[Refrain] FateZero 2nd Season - 11 VOSTFR [720p]](https://uptobox.com/c7gpkw1g8kg1)
