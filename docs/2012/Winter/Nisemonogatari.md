# Nisemonogatari

![Nisemonogatari](https://cdn.myanimelist.net/images/anime/10/35619l.jpg)

* Japanese:  偽物語

## Information

 - Type: TV
 - Episodes: 11
 - Status: Finished Airing
 - Aired: Jan 8, 2012 to Mar 18, 2012
 - Premiered: Winter 2012
 - Broadcast: Sundays at 00:00 (JST)
 - Producers: Aniplex, Kodansha, Rakuonsha 
 - Licensors: Aniplex of America 
 - Studios: Shaft 
 - Source: Light novel
 - Genres: Mystery, Comedy, Supernatural, Ecchi
 - Duration: 24 min. per ep.
 - Rating: R - 17+ (violence & profanity)


## Links

- [[Refrain] Nisemonogatari - 01 VF VOSTFR [1080p]](https://uptobox.com/7r96hkujvywc)
- [[Refrain] Nisemonogatari - 01 VF VOSTFR [720p]](https://uptobox.com/kikek54kgnxs)
- [[Refrain] Nisemonogatari - 02 VF VOSTFR [1080p]](https://uptobox.com/w4dy4d8c4z8x)
- [[Refrain] Nisemonogatari - 02 VF VOSTFR [720p]](https://uptobox.com/341q7vz9qc8a)
- [[Refrain] Nisemonogatari - 03 VF VOSTFR [1080p]](https://uptobox.com/nex4m6finspy)
- [[Refrain] Nisemonogatari - 03 VF VOSTFR [720p]](https://uptobox.com/ik7f5o4xou4c)
- [[Refrain] Nisemonogatari - 04 VF VOSTFR [1080p]](https://uptobox.com/huoeotllh26z)
- [[Refrain] Nisemonogatari - 04 VF VOSTFR [720p]](https://uptobox.com/qh0oy2ifh2y8)
- [[Refrain] Nisemonogatari - 05 VF VOSTFR [1080p]](https://uptobox.com/le5pszo1y5oz)
- [[Refrain] Nisemonogatari - 05 VF VOSTFR [720p]](https://uptobox.com/m7nm2m8rqk5w)
- [[Refrain] Nisemonogatari - 06 VF VOSTFR [1080p]](https://uptobox.com/vboq9dntvl80)
- [[Refrain] Nisemonogatari - 06 VF VOSTFR [720p]](https://uptobox.com/v58i0iz8qj1q)
- [[Refrain] Nisemonogatari - 07 VF VOSTFR [1080p]](https://uptobox.com/cslpc7rmuwzy)
- [[Refrain] Nisemonogatari - 07 VF VOSTFR [720p]](https://uptobox.com/sxey2gp33zv3)
- [[Refrain] Nisemonogatari - 08 VF VOSTFR [1080p]](https://uptobox.com/r56wavf0miiu)
- [[Refrain] Nisemonogatari - 08 VF VOSTFR [720p]](https://uptobox.com/nxsu0cofribt)
- [[Refrain] Nisemonogatari - 09 VF VOSTFR [1080p]](https://uptobox.com/82o79tva1ml6)
- [[Refrain] Nisemonogatari - 09 VF VOSTFR [720p]](https://uptobox.com/ak935rtm3gjd)
- [[Refrain] Nisemonogatari - 10 VF VOSTFR [1080p]](https://uptobox.com/o21jwl4a6cmo)
- [[Refrain] Nisemonogatari - 10 VF VOSTFR [720p]](https://uptobox.com/7z7mcwwz2faq)
- [[Refrain] Nisemonogatari - 11 VF VOSTFR [1080p]](https://uptobox.com/7ni7c34rm2w2)
- [[Refrain] Nisemonogatari - 11 VF VOSTFR [720p]](https://uptobox.com/txbnn3kovmzs)
