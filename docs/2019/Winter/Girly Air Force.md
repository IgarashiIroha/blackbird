# Girly Air Force

![Girly Air Force](https://cdn.myanimelist.net/images/anime/1872/93941l.jpg)

* Japanese: ガーリー・エアフォース

## Information

- Type: TV
- Episodes: Unknown
- Status: Currently Airing
- Aired: Jan 10, 2019 to ?
- Premiered: Winter 2019
- Broadcast: Thursdays at 23:00 (JST)
- Producers: None found, add some
- Licensors: None found, add some
- Studios: Satelight
- Source: Light novel
- Genres: Action, Sci-Fi
- Duration: Unknown
- Rating: None

## Links
- [[Refrain] Girly Air Force - 01 VOSTFR [720p]](https://uptobox.com/kk7thokt9wo1)
- [[Refrain] Girly Air Force - 01 VOSTFR [1080p]](https://uptobox.com/t4l3f3jkea8k)
- [[Refrain] Girly Air Force - 02 VOSTFR [720p]](https://uptobox.com/e03yfcnlsdvb)
- [[Refrain] Girly Air Force - 02 VOSTFR [1080p]](https://uptobox.com/g31eltg2bx22)
- [[Refrain] Girly Air Force - 03 VOSTFR [720p]](https://uptobox.com/j8plhafzb83k)
- [[Refrain] Girly Air Force - 03 VOSTFR [1080p]](https://uptobox.com/nn5xpywaqlim)
- [[Refrain] Girly Air Force - 04 VOSTFR [720p]](https://uptobox.com/c7jc8nukq8jd)
- [[Refrain] Girly Air Force - 04 VOSTFR [1080p]](https://uptobox.com/pvuvigubgz7j)
- [[Refrain] Girly Air Force - 05 VOSTFR [720p]](https://uptobox.com/11b47w4s2jap)
- [[Refrain] Girly Air Force - 05 VOSTFR [1080p]](https://uptobox.com/6kgtapvh77ex)
- [[Refrain] Girly Air Force - 06 VOSTFR [720p]](https://uptobox.com/34cpptjd7avc)
- [[Refrain] Girly Air Force - 06 VOSTFR [1080p]](https://uptobox.com/q58hj62wclow)
- [[Refrain] Girly Air Force - 07 VOSTFR [720p]](https://uptobox.com/n50zlnxlm2iq)
- [[Refrain] Girly Air Force - 07 VOSTFR [1080p]](https://uptobox.com/0ae2k2ugncut)
- [[Refrain] Girly Air Force - 08 VOSTFR [720p]](https://uptobox.com/ona87hpid59g)
- [[Refrain] Girly Air Force - 08 VOSTFR [1080p]](https://uptobox.com/rm1de9wr20em)
- [[Refrain] Girly Air Force - 09 VOSTFR [720p]](https://uptobox.com/mpkbsnjtujgd)
- [[Refrain] Girly Air Force - 09 VOSTFR [1080p]](https://uptobox.com/tjqpjbfu1b13)
- [[Refrain] Girly Air Force - 10 VOSTFR [720p]](https://uptobox.com/4fqxgezc2jvp)
- [[Refrain] Girly Air Force - 10 VOSTFR [1080p]](https://uptobox.com/zsntkymria9i)
- [[Refrain] Girly Air Force - 11 VOSTFR [720p]](https://uptobox.com/hyuywohc29dm)
- [[Refrain] Girly Air Force - 11 VOSTFR [1080p]](https://uptobox.com/q54yicwnpb2x)
- [[Refrain] Girly Air Force - 12 VOSTFR [720p]](https://uptobox.com/d2b6ti38p1ze)
- [[Refrain] Girly Air Force - 12 VOSTFR [1080p]](https://uptobox.com/1ltbviikqyde)
