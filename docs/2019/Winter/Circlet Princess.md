# Circlet Princess

![Circlet Princess](https://cdn.myanimelist.net/images/anime/1760/97712l.jpg)

* Japanese:  CIRCLET PRINCESS（サークレット・プリンセス）

## Information

 - Type: TV
 - Episodes: 12
 - Status: Currently Airing
 - Aired: Jan 8, 2019 to ?
 - Premiered: Winter 2019
 - Broadcast: Tuesdays at 23:00 (JST)
 - Producers: Lantis 
 - Licensors: None found, add some 
 - Studios: Silver Link. 
 - Source: Game
 - Genres: Action, Sci-Fi, Sports, School
 - Duration: Unknown
 - Rating: None


## Links

