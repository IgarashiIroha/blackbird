# Kaguya-sama wa Kokurasetai: Tensai-tachi no Renai Zunousen

![Kaguya-sama wa Kokurasetai: Tensai-tachi no Renai Zunousen](https://cdn.myanimelist.net/images/anime/1291/97023l.jpg)

* Japanese:  かぐや様は告らせたい～天才たちの恋愛頭脳戦～

## Information

 - Type: TV
 - Episodes: Unknown
 - Status: Currently Airing
 - Aired: Jan 12, 2019 to ?
 - Premiered: Winter 2019
 - Broadcast: Saturdays at 23:30 (JST)
 - Producers: Aniplex, Magic Capsule, Shueisha 
 - Licensors: Aniplex of America 
 - Studios: A-1 Pictures 
 - Source: Manga
 - Genres: Comedy, Psychological, Romance, School, Seinen
 - Duration: Unknown
 - Rating: PG-13 - Teens 13 or older


## Links

- [[Refrain] Kaguya-sama wa Kokurasetai - Tensai-tachi no Renai Zunousen - 01 VOSTFR [1080p]](https://uptobox.com/o0ohtch96ama)
- [[Refrain] Kaguya-sama wa Kokurasetai - Tensai-tachi no Renai Zunousen - 01 VOSTFR [720p]](https://uptobox.com/21m3gigj62d3)
- [[Refrain] Kaguya-sama wa Kokurasetai - Tensai-tachi no Renai Zunousen - 02 VOSTFR [1080p]](https://uptobox.com/rya4yp8cbwax)
- [[Refrain] Kaguya-sama wa Kokurasetai - Tensai-tachi no Renai Zunousen - 02 VOSTFR [720p]](https://uptobox.com/fs3hj01ghmin)
- [[Refrain] Kaguya-sama wa Kokurasetai - Tensai-tachi no Renai Zunousen - 03 VOSTFR [1080p]](https://uptobox.com/o015ybvdecgx)
- [[Refrain] Kaguya-sama wa Kokurasetai - Tensai-tachi no Renai Zunousen - 03 VOSTFR [720p]](https://uptobox.com/5osjnrh4nwrt)
- [[Refrain] Kaguya-sama wa Kokurasetai - Tensai-tachi no Renai Zunousen - 04 VOSTFR [1080p]](https://uptobox.com/tjgexz89qaar)
- [[Refrain] Kaguya-sama wa Kokurasetai - Tensai-tachi no Renai Zunousen - 04 VOSTFR [720p]](https://uptobox.com/fg011r8lh7fj)
- [[Refrain] Kaguya-sama wa Kokurasetai - Tensai-tachi no Renai Zunousen - 05 VOSTFR [1080p]](https://uptobox.com/elrdh1rh957a)
- [[Refrain] Kaguya-sama wa Kokurasetai - Tensai-tachi no Renai Zunousen - 05 VOSTFR [720p]](https://uptobox.com/tea5yk3entca)
- [[Refrain] Kaguya-sama wa Kokurasetai - Tensai-tachi no Renai Zunousen - 06 VOSTFR [1080p]](https://uptobox.com/bhm85p9ou2tt)
- [[Refrain] Kaguya-sama wa Kokurasetai - Tensai-tachi no Renai Zunousen - 06 VOSTFR [720p]](https://uptobox.com/1053v4hche0w)
- [[Refrain] Kaguya-sama wa Kokurasetai - Tensai-tachi no Renai Zunousen - 07 VOSTFR [1080p]](https://uptobox.com/nx5xrzqpczjz)
- [[Refrain] Kaguya-sama wa Kokurasetai - Tensai-tachi no Renai Zunousen - 07 VOSTFR [720p]](https://uptobox.com/v6uxw3f483ca)
- [[Refrain] Kaguya-sama wa Kokurasetai - Tensai-tachi no Renai Zunousen - 08 VOSTFR [1080p]](https://uptobox.com/kx9hrglxhpqh)
- [[Refrain] Kaguya-sama wa Kokurasetai - Tensai-tachi no Renai Zunousen - 08 VOSTFR [720p]](https://uptobox.com/sd79erwm8s6e)
- [[Refrain] Kaguya-sama wa Kokurasetai - Tensai-tachi no Renai Zunousen - 09 VOSTFR [1080p]](https://uptobox.com/1ct7iij870s3)
- [[Refrain] Kaguya-sama wa Kokurasetai - Tensai-tachi no Renai Zunousen - 09 VOSTFR [720p]](https://uptobox.com/uml5p6myfuzs)
- [[Refrain] Kaguya-sama wa Kokurasetai - Tensai-tachi no Renai Zunousen - 10 VOSTFR [1080p]](https://uptobox.com/7zebhgc1ixng)
- [[Refrain] Kaguya-sama wa Kokurasetai - Tensai-tachi no Renai Zunousen - 10 VOSTFR [720p]](https://uptobox.com/9xjnwlc68mn6)
- [[Refrain] Kaguya-sama wa Kokurasetai - Tensai-tachi no Renai Zunousen - 11 VOSTFR [1080p]](https://uptobox.com/of3fprei6ct1)
- [[Refrain] Kaguya-sama wa Kokurasetai - Tensai-tachi no Renai Zunousen - 11 VOSTFR [720p]](https://uptobox.com/48xeo0x5671u)
- [[Refrain] Kaguya-sama wa Kokurasetai - Tensai-tachi no Renai Zunousen - 12 VOSTFR [1080p]](https://uptobox.com/t3dv5nbgz6bx)
- [[Refrain] Kaguya-sama wa Kokurasetai - Tensai-tachi no Renai Zunousen - 12 VOSTFR [720p]](https://uptobox.com/z2a1ungxt73b)
