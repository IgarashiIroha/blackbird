# Gotoubun no Hanayome

![Gotoubun no Hanayome](https://cdn.myanimelist.net/images/anime/1819/97947l.jpg)

* Japanese:  ベルゼブブ嬢のお気に召すまま。

## Information

 - Type: TV
 - Episodes: 12
 - Status: Finished Airing
 - Aired: Oct 11, 2018 to Dec 27, 2018
 - Premiered: Fall 2018
 - Broadcast: Thursdays at 02:50 (JST)
 - Producers: Aniplex, Square Enix, Movic, Studio Mausu, Tokyo MX, Q-Tec, ABC Animation, CA-Cygames Anime Fund
 - Licensors: None found, add some
 - Studios: LIDENFILMS
 - Source: 4-koma manga
 - Genres: Comedy, Demons, Romance, Fantasy, Shounen
 - Duration: 24 min. per ep.
 - Rating: PG-13 - Teens 13 or older


## Links

- [[Refrain] Gotoubun no Hanayome - 01 VOSTFR [720p]](https://uptobox.com/bft0dm46sv88)
- [[Refrain] Gotoubun no Hanayome - 01 VOSTFR [1080p]](https://uptobox.com/vr63o3tftevt)
- [[Refrain] Gotoubun no Hanayome - 02 VOSTFR [720p]](https://uptobox.com/s2tgk7eucssw)
- [[Refrain] Gotoubun no Hanayome - 02 VOSTFR [1080p]](https://uptobox.com/it9pw520pzo0)
- [[Refrain] Gotoubun no Hanayome - 03 VOSTFR [720p]](https://uptobox.com/hoigvtt1gmrd)
- [[Refrain] Gotoubun no Hanayome - 03 VOSTFR [1080p]](https://uptobox.com/qbw97qrg5m2m)
- [[Refrain] Gotoubun no Hanayome - 04 VOSTFR [720p]](https://uptobox.com/8mwcj7pdsphr)
- [[Refrain] Gotoubun no Hanayome - 04 VOSTFR [1080p]](https://uptobox.com/cepvrlrz6u4z)
- [[Refrain] Gotoubun no Hanayome - 05 VOSTFR [720p]](https://uptobox.com/v9vlgirlxyw7)
- [[Refrain] Gotoubun no Hanayome - 05 VOSTFR [1080p]](https://uptobox.com/kgv0qz6l9rb3)
- [[Refrain] Gotoubun no Hanayome - 06 VOSTFR [720p]](https://uptobox.com/8rm4ckxnli5f)
- [[Refrain] Gotoubun no Hanayome - 06 VOSTFR [1080p]](https://uptobox.com/pglz7n11cs4q)
- [[Refrain] Gotoubun no Hanayome - 07 VOSTFR [720p]](https://uptobox.com/910m2etoaib7)
- [[Refrain] Gotoubun no Hanayome - 07 VOSTFR [1080p]](https://uptobox.com/i4h8l0y81add)
- [[Refrain] Gotoubun no Hanayome - 08 VOSTFR [720p]](https://uptobox.com/hw93rky9q2vu)
- [[Refrain] Gotoubun no Hanayome - 08 VOSTFR [1080p]](https://uptobox.com/cv0lq4n9yros)
- [[Refrain] Gotoubun no Hanayome - 09 VOSTFR [720p]](https://uptobox.com/yjkpwa28f5ir)
- [[Refrain] Gotoubun no Hanayome - 09 VOSTFR [1080p]](https://uptobox.com/rnqbiiab6qac)
- [[Refrain] Gotoubun no Hanayome - 10 VOSTFR [720p]](https://uptobox.com/lbg7ue36m4q0)
- [[Refrain] Gotoubun no Hanayome - 10 VOSTFR [1080p]](https://uptobox.com/145k9rtvnesf)
- [[Refrain] Gotoubun no Hanayome - 11 VOSTFR [720p]](https://uptobox.com/oqb5hwboowyz)
- [[Refrain] Gotoubun no Hanayome - 11 VOSTFR [1080p]](https://uptobox.com/ajedsf4mt0cm)
- [[Refrain] Gotoubun no Hanayome - 12 VOSTFR [720p]](https://uptobox.com/fyhpyknpjcn4)
- [[Refrain] Gotoubun no Hanayome - 12 VOSTFR [1080p]](https://uptobox.com/2dtfo56307mt)
