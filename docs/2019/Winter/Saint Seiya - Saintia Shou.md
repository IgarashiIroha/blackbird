# Saint Seiya - Saintia Shou

![Saint Seiya - Saintia Shou](https://cdn.myanimelist.net/images/anime/1022/93948l.jpg)

* Japanese: 聖闘士星矢 セインティア翔

## Information

- Type: ONA
- Episodes: Unknown
- Status: Currently Airing
- Aired: Dec 10, 2018 to ?
- Producers: Toei Animation
- Licensors: None found, add some
- Studios: Gonzo
- Source: Manga
- Genres: Action, Adventure, Seinen
- Duration: 25 min.
- Rating: PG-13 - Teens 13 or older

## Links

- [[Refrain] Saint Seiya - Saintia Shou - 01 VOSTFR [720p]](https://uptobox.com/a7noqplc8hk8)
- [[Refrain] Saint Seiya - Saintia Shou - 01 VOSTFR [1080p]](https://uptobox.com/jo4rojau41bm)
- [[Refrain] Saint Seiya - Saintia Shou - 01 VOSTFR {ADN} [720p]](https://uptobox.com/sy19p4u0hnlq)
- [[Refrain] Saint Seiya - Saintia Shou - 01 VOSTFR {ADN} [1080p]](https://uptobox.com/hkiwsk3id87b)
- [[Refrain] Saint Seiya - Saintia Shou - 02 VOSTFR [720p]](https://uptobox.com/ty1gy4m2ywvg)
- [[Refrain] Saint Seiya - Saintia Shou - 02 VOSTFR [1080p]](https://uptobox.com/g9vr183iqwta)
- [[Refrain] Saint Seiya - Saintia Shou - 02 VOSTFR {ADN} [720p]](https://uptobox.com/ru7g1lgcvzcw)
- [[Refrain] Saint Seiya - Saintia Shou - 02 VOSTFR {ADN} [1080p]](https://uptobox.com/c0y90qdqc6yt)
- [[Refrain] Saint Seiya - Saintia Shou - 03 VOSTFR [720p]](https://uptobox.com/m8g16ywhrwfd)
- [[Refrain] Saint Seiya - Saintia Shou - 03 VOSTFR [1080p]](https://uptobox.com/kbj2xwors2yx)
- [[Refrain] Saint Seiya - Saintia Shou - 03 VOSTFR {ADN} [720p]](https://uptobox.com/dg7gu28xhpza)
- [[Refrain] Saint Seiya - Saintia Shou - 03 VOSTFR {ADN} [1080p]](https://uptobox.com/x9ugqkm7z140)
- [[Refrain] Saint Seiya - Saintia Shou - 04 VOSTFR [720p]](https://uptobox.com/m560t723ttrh)
- [[Refrain] Saint Seiya - Saintia Shou - 04 VOSTFR [1080p]](https://uptobox.com/0bewt1wp3jem)
- [[Refrain] Saint Seiya - Saintia Shou - 04 VOSTFR {ADN} [720p]](https://uptobox.com/vzckkh8lt31g)
- [[Refrain] Saint Seiya - Saintia Shou - 04 VOSTFR {ADN} [1080p]](https://uptobox.com/03f8434553j5)
- [[Refrain] Saint Seiya - Saintia Shou - 05 VOSTFR [720p]](https://uptobox.com/ya3j9adqoh0h)
- [[Refrain] Saint Seiya - Saintia Shou - 05 VOSTFR [1080p]](https://uptobox.com/rahf78krewa7)
- [[Refrain] Saint Seiya - Saintia Shou - 05 VOSTFR {ADN} [720p]](https://uptobox.com/h69tmyqrteoa)
- [[Refrain] Saint Seiya - Saintia Shou - 05 VOSTFR {ADN} [1080p]](https://uptobox.com/td5a76k4tlzx)
- [[Refrain] Saint Seiya - Saintia Shou - 06 VOSTFR [720p]](https://uptobox.com/iets3cwefyvi)
- [[Refrain] Saint Seiya - Saintia Shou - 06 VOSTFR [1080p]](https://uptobox.com/3b63t7rx5cl5)
- [[Refrain] Saint Seiya - Saintia Shou - 06 VOSTFR {ADN} [720p]](https://uptobox.com/9hvotewtrbft)
- [[Refrain] Saint Seiya - Saintia Shou - 06 VOSTFR {ADN} [1080p]](https://uptobox.com/5mby2rmg9b3j)
- [[Refrain] Saint Seiya - Saintia Shou - 07 VOSTFR [720p]](https://uptobox.com/oez9y8nypnu8)
- [[Refrain] Saint Seiya - Saintia Shou - 07 VOSTFR [1080p]](https://uptobox.com/qp8fg495vm3j)
- [[Refrain] Saint Seiya - Saintia Shou - 07 VOSTFR {ADN} [720p]](https://uptobox.com/3lk0ya5hfrju)
- [[Refrain] Saint Seiya - Saintia Shou - 07 VOSTFR {ADN} [1080p]](https://uptobox.com/mosxhwu0f91p)
- [[Refrain] Saint Seiya - Saintia Shou - 08 VOSTFR [720p]](https://uptobox.com/qeztf8tlv4il)
- [[Refrain] Saint Seiya - Saintia Shou - 08 VOSTFR [1080p]](https://uptobox.com/8gffzbpjqy9b)
- [[Refrain] Saint Seiya - Saintia Shou - 08 VOSTFR {ADN} [720p]](https://uptobox.com/u0ouqkdy8dzy)
- [[Refrain] Saint Seiya - Saintia Shou - 08 VOSTFR {ADN} [1080p]](https://uptobox.com/gc8kvrlg12gi)
- [[Refrain] Saint Seiya - Saintia Shou - 09 VOSTFR [720p]](https://uptobox.com/tb5zahl8mfg7)
- [[Refrain] Saint Seiya - Saintia Shou - 09 VOSTFR [1080p]](https://uptobox.com/vlqww28wwto7)
- [[Refrain] Saint Seiya - Saintia Shou - 09 VOSTFR {ADN} [720p]](https://uptobox.com/ddrvhajkf6pq)
- [[Refrain] Saint Seiya - Saintia Shou - 09 VOSTFR {ADN} [1080p]](https://uptobox.com/0vleeehw491j)
- [[Refrain] Saint Seiya - Saintia Shou - 10 VOSTFR [720p]](https://uptobox.com/yrq2c4q0iez3)
- [[Refrain] Saint Seiya - Saintia Shou - 10 VOSTFR [1080p]](https://uptobox.com/wiwcd39ci05y)
- [[Refrain] Saint Seiya - Saintia Shou - 10 VOSTFR {ADN} [720p]](https://uptobox.com/ewyg3s4mj3qa)
- [[Refrain] Saint Seiya - Saintia Shou - 10 VOSTFR {ADN} [1080p]](https://uptobox.com/18kjix0fpr87)
