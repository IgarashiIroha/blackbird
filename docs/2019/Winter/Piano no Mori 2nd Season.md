# Piano no Mori (TV) 2nd Season

![Piano no Mori (TV) 2nd Season](https://cdn.myanimelist.net/images/anime/1670/93176l.jpg)

* Japanese:  ピアノの森 第2シリーズ

## Information

 - Type: TV
 - Episodes: 12
 - Status: Not yet aired
 - Aired: Jan 28, 2019 to ?
 - Premiered: Winter 2019
 - Broadcast: Mondays at 00:10 (JST)
 - Producers: NHK 
 - Licensors: None found, add some 
 - Studios: Gaina 
 - Source: Manga
 - Genres: Comedy, Drama, Music, School, Seinen
 - Duration: Unknown
 - Rating: None


## Links

