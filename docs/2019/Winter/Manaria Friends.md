# Manaria Friends

![Manaria Friends](https://cdn.myanimelist.net/images/anime/1580/95199l.jpg)

* Japanese:  マナリアフレンズ

## Information

 - Type: TV
 - Episodes: 10
 - Status: Currently Airing
 - Aired: Dec 16, 2018 to ?
 - Premiered: Winter 2019
 - Broadcast: Mondays at 00:30 (JST)
 - Producers: Cygames, Happinet 
 - Licensors: Sentai Filmworks 
 - Studios: CygamesPictures 
 - Source: Game
 - Genres: Action, Adventure, Demons, Fantasy, Magic, Supernatural
 - Duration: 15 min. per ep.
 - Rating: None


## Links

- [[Refrain] Manaria Friends - 01 VOSTFR [720p]](https://uptobox.com/ccvj52ywwyvr)
- [[Refrain] Manaria Friends - 01 VOSTFR [1080p]](https://uptobox.com/w46cqt5dsc80)
- [[Refrain] Manaria Friends - 02 VOSTFR [720p]](https://uptobox.com/bfe5ur0yn2yx)
- [[Refrain] Manaria Friends - 02 VOSTFR [1080p]](https://uptobox.com/1qjvzk0mlj2h)
- [[Refrain] Manaria Friends - 03 VOSTFR [720p]](https://uptobox.com/vgh2azz6gpzo)
- [[Refrain] Manaria Friends - 03 VOSTFR [1080p]](https://uptobox.com/xsw8nyhucpjx)
- [[Refrain] Manaria Friends - 04 VOSTFR [720p]](https://uptobox.com/9ynattgj63e7)
- [[Refrain] Manaria Friends - 04 VOSTFR [1080p]](https://uptobox.com/gmplpuxvjd4j)
- [[Refrain] Manaria Friends - 05 VOSTFR [720p]](https://uptobox.com/d1lr8dkgilhx)
- [[Refrain] Manaria Friends - 05 VOSTFR [1080p]](https://uptobox.com/w7tac5fbrtw9)
- [[Refrain] Manaria Friends - 06 VOSTFR [720p]](https://uptobox.com/nx9n48bf1t9p)
- [[Refrain] Manaria Friends - 06 VOSTFR [1080p]](https://uptobox.com/og7d5869t23j)
- [[Refrain] Manaria Friends - 07 VOSTFR [720p]](https://uptobox.com/vfha0hhuh9he)
- [[Refrain] Manaria Friends - 07 VOSTFR [1080p]](https://uptobox.com/3lc5jyr644li)
- [[Refrain] Manaria Friends - 08 VOSTFR [720p]](https://uptobox.com/mu11fa35jnvz)
- [[Refrain] Manaria Friends - 08 VOSTFR [1080p]](https://uptobox.com/2zxww54i2rrc)
- [[Refrain] Manaria Friends - 09 VOSTFR [720p]](https://uptobox.com/d4wbmc66ple0)
- [[Refrain] Manaria Friends - 09 VOSTFR [1080p]](https://uptobox.com/0dytt5ix7jxq)
- [[Refrain] Manaria Friends - 10 VOSTFR [720p]](https://uptobox.com/x3n7fhi3qgce)
- [[Refrain] Manaria Friends - 10 VOSTFR [1080p]](https://uptobox.com/gezluivfmmn0)
