# Egao no Daika

![Egao no Daika](https://cdn.myanimelist.net/images/anime/1935/96963l.jpg)


* Synonyms: <small>The Cost of Smiles, The Price of Smiles</small>
* Japanese: エガオノダイカ

## Information

- Type: TV
- Episodes: Unknown
- Status: Currently Airing
- Aired: Jan 4, 2019 to ?
- Premiered: Winter 2019
- Broadcast: Fridays at 21:30 (JST)
- Producers: None found, add some
- Licensors: None found, add some
- Studios: Tatsunoko Production
- Source: Original
- Genres: Military, Slice of Life, Drama, Fantasy
- Duration: Unknown
- Rating: None

## Links

- [[Refrain] Egao no Daika - 01 VOSTFR [720p]](https://uptobox.com/8o20pla89n7l)
- [[Refrain] Egao no Daika - 01 VOSTFR [1080p]](https://uptobox.com/c5pcjxsu71fc)
- [[Refrain] Egao no Daika - 02 VOSTFR [720p]](https://uptobox.com/mm5c2opgd8mp)
- [[Refrain] Egao no Daika - 02 VOSTFR [1080p]](https://uptobox.com/a88nrt9m0u3d)
- [[Refrain] Egao no Daika - 03 VOSTFR [720p]](https://uptobox.com/lilzjm58ttuz)
- [[Refrain] Egao no Daika - 03 VOSTFR [1080p]](https://uptobox.com/pawijeftgzce)
- [[Refrain] Egao no Daika - 04 VOSTFR [720p]](https://uptobox.com/l4l9ejo7xu7h)
- [[Refrain] Egao no Daika - 04 VOSTFR [1080p]](https://uptobox.com/drjzdzvp4oje)
- [[Refrain] Egao no Daika - 05 VOSTFR [720p]](https://uptobox.com/gxrtzgzc9uwn)
- [[Refrain] Egao no Daika - 05 VOSTFR [1080p]](https://uptobox.com/c943va7bu4t7)
- [[Refrain] Egao no Daika - 06 VOSTFR [720p]](https://uptobox.com/evr31xqdl7ur)
- [[Refrain] Egao no Daika - 06 VOSTFR [1080p]](https://uptobox.com/t71gfkz4bd3e)
- [[Refrain] Egao no Daika - 07 VOSTFR [720p]](https://uptobox.com/hr9d83efdo3v)
- [[Refrain] Egao no Daika - 07 VOSTFR [1080p]](https://uptobox.com/rr55pfbhc4vs)
- [[Refrain] Egao no Daika - 08 VOSTFR [720p]](https://uptobox.com/hsxbpxj37c9z)
- [[Refrain] Egao no Daika - 08 VOSTFR [1080p]](https://uptobox.com/f4dlbmsb0a9u)
- [[Refrain] Egao no Daika - 09 VOSTFR [720p]](https://uptobox.com/lx0y1f09ggpg)
- [[Refrain] Egao no Daika - 09 VOSTFR [1080p]](https://uptobox.com/p571jfwn592h)
- [[Refrain] Egao no Daika - 10 VOSTFR [720p]](https://uptobox.com/30y3d2hxxjla)
- [[Refrain] Egao no Daika - 10 VOSTFR [1080p]](https://uptobox.com/nitf0ubuzyje)
- [[Refrain] Egao no Daika - 11 VOSTFR [720p]](https://uptobox.com/i24eyla69c1g)
- [[Refrain] Egao no Daika - 11 VOSTFR [1080p]](https://uptobox.com/5gwiu4p9kt7i)
- [[Refrain] Egao no Daika - 12 VOSTFR [720p]](https://uptobox.com/afiwdc40duj4)
- [[Refrain] Egao no Daika - 12 VOSTFR [1080p]](https://uptobox.com/e63zpnb43jy1)
