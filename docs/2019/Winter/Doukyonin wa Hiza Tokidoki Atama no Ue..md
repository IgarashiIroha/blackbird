# Doukyonin wa Hiza Tokidoki Atama no Ue.


![Doukyonin wa Hiza Tokidoki Atama no Ue.](https://cdn.myanimelist.net/images/anime/1634/97368l.jpg)

* Japanese: 同居人はひざ、時々、頭のうえ。

## Information

- Type: TV
- Episodes: 12
- Status: Currently Airing
- Aired: Jan 9, 2019 to ?
- Premiered: Winter 2019
- Broadcast: Wednesdays at 23:30 (JST)
- Producers: None found, add some
- Licensors: None found, add some
- Studios: Zero-G
- Source: Web manga
- Genres: Slice of Life, Comedy
- Duration: Unknown
- Rating: PG-13 - Teens 13 or older


## Links

- [[Refrain] Doukyonin wa Hiza Tokidoki Atama no Ue. - 01 VOSTFR [720p]](https://uptobox.com/9a1kavrukqx9)
- [[Refrain] Doukyonin wa Hiza Tokidoki Atama no Ue. - 01 VOSTFR [1080p]](https://uptobox.com/l7nj5jkpxgqt)
- [[Refrain] Doukyonin wa Hiza Tokidoki Atama no Ue. - 02 VOSTFR [720p]](https://uptobox.com/gg9me5rxfa6j)
- [[Refrain] Doukyonin wa Hiza Tokidoki Atama no Ue. - 02 VOSTFR [1080p]](https://uptobox.com/ufu15tndgv4p)
- [[Refrain] Doukyonin wa Hiza Tokidoki Atama no Ue. - 03 VOSTFR [720p]](https://uptobox.com/5i583og7juc9)
- [[Refrain] Doukyonin wa Hiza Tokidoki Atama no Ue. - 03 VOSTFR [1080p]](https://uptobox.com/bfiq4bt662ul)
- [[Refrain] Doukyonin wa Hiza Tokidoki Atama no Ue. - 04 VOSTFR [720p]](https://uptobox.com/9zu3o40v3mxu)
- [[Refrain] Doukyonin wa Hiza Tokidoki Atama no Ue. - 04 VOSTFR [1080p]](https://uptobox.com/puzcboi14gxp)
- [[Refrain] Doukyonin wa Hiza Tokidoki Atama no Ue. - 05 VOSTFR [720p]](https://uptobox.com/mf3zdrpry2dv)
- [[Refrain] Doukyonin wa Hiza Tokidoki Atama no Ue. - 05 VOSTFR [1080p]](https://uptobox.com/c50hhggkp3o6)
- [[Refrain] Doukyonin wa Hiza Tokidoki Atama no Ue. - 06 VOSTFR [720p]](https://uptobox.com/e30f4bmtkyvh)
- [[Refrain] Doukyonin wa Hiza Tokidoki Atama no Ue. - 06 VOSTFR [1080p]](https://uptobox.com/k36cutyz3mf2)
- [[Refrain] Doukyonin wa Hiza Tokidoki Atama no Ue. - 07 VOSTFR [720p]](https://uptobox.com/gts3ke70vk23)
- [[Refrain] Doukyonin wa Hiza Tokidoki Atama no Ue. - 07 VOSTFR [1080p]](https://uptobox.com/2e15558hap1p)
- [[Refrain] Doukyonin wa Hiza Tokidoki Atama no Ue. - 08 VOSTFR [720p]](https://uptobox.com/kk9eg558g0du)
- [[Refrain] Doukyonin wa Hiza Tokidoki Atama no Ue. - 08 VOSTFR [1080p]](https://uptobox.com/90x9dutj4fz4)
- [[Refrain] Doukyonin wa Hiza Tokidoki Atama no Ue. - 09 VOSTFR [720p]](https://uptobox.com/a46vhr5rok4a)
- [[Refrain] Doukyonin wa Hiza Tokidoki Atama no Ue. - 09 VOSTFR [1080p]](https://uptobox.com/rr2eh88jn5qx)
- [[Refrain] Doukyonin wa Hiza Tokidoki Atama no Ue. - 10 VOSTFR [720p]](https://uptobox.com/oaef9a4gi4wg)
- [[Refrain] Doukyonin wa Hiza Tokidoki Atama no Ue. - 10 VOSTFR [1080p]](https://uptobox.com/sqkdym4bwse1)
- [[Refrain] Doukyonin wa Hiza Tokidoki Atama no Ue. - 11 VOSTFR [720p]](https://uptobox.com/s9hwzykbfu67)
- [[Refrain] Doukyonin wa Hiza Tokidoki Atama no Ue. - 11 VOSTFR [1080p]](https://uptobox.com/b0hz9jnizofx)
- [[Refrain] Doukyonin wa Hiza Tokidoki Atama no Ue. - 12 VOSTFR [720p]](https://uptobox.com/sa6gjx0dj7xj)
- [[Refrain] Doukyonin wa Hiza Tokidoki Atama no Ue. - 12 VOSTFR [1080p]](https://uptobox.com/hx1xrldax047)
