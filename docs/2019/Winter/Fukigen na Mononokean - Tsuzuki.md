# Fukigen na Mononokean - Tsuzuki

![Fukigen na Mononokean - Tsuzuki](https://cdn.myanimelist.net/images/anime/1960/95662l.jpg)


* Synonyms: <small>Fukigen na Mononokean 2nd Season</small>
* Japanese: 不機嫌なモノノケ庵 續

## Information

- Type: TV
- Episodes: 13
- Status: Currently Airing
- Aired: Jan 5, 2019 to ?
- Premiered: Winter 2019
- Broadcast: Saturdays at 21:00 (JST)
- Producers: VAP, Square Enix, AT-X, Nippon Columbia, Age Global Networks, Yomiuri TV Enterprise, Kanon Sound, RAY
- Licensors: None found, add some
- Studios: Pierrot Plus
- Source: Web manga
- Genres: Comedy, Demons, Supernatural
- Duration: 23 min. per ep.
- Rating: None

## Links

- [[Refrain] Fukigen na Mononokean - Tsuzuki - 01 VOSTFR [720p]](https://uptobox.com/oxm6hofh1t4b)
- [[Refrain] Fukigen na Mononokean - Tsuzuki - 01 VOSTFR [1080p]](https://uptobox.com/fks1qtif9lf4)
- [[Refrain] Fukigen na Mononokean - Tsuzuki - 02 VOSTFR [720p]](https://uptobox.com/ccofqye08ome)
- [[Refrain] Fukigen na Mononokean - Tsuzuki - 02 VOSTFR [1080p]](https://uptobox.com/i4vnpubqd2mq)
- [[Refrain] Fukigen na Mononokean - Tsuzuki - 03 VOSTFR [720p]](https://uptobox.com/8m0y34h3z459)
- [[Refrain] Fukigen na Mononokean - Tsuzuki - 03 VOSTFR [1080p]](https://uptobox.com/6m32xn6mxb5j)
- [[Refrain] Fukigen na Mononokean - Tsuzuki - 04 VOSTFR [720p]](https://uptobox.com/3rz4gw2459uf)
- [[Refrain] Fukigen na Mononokean - Tsuzuki - 04 VOSTFR [1080p]](https://uptobox.com/uoik2lwjktr5)
- [[Refrain] Fukigen na Mononokean - Tsuzuki - 05 VOSTFR [720p]](https://uptobox.com/2mlliepx45h9)
- [[Refrain] Fukigen na Mononokean - Tsuzuki - 05 VOSTFR [1080p]](https://uptobox.com/94oszj1aihck)
- [[Refrain] Fukigen na Mononokean - Tsuzuki - 06 VOSTFR [720p]](https://uptobox.com/9jxoi1cy54or)
- [[Refrain] Fukigen na Mononokean - Tsuzuki - 06 VOSTFR [1080p]](https://uptobox.com/9orpe7gmsk61)
- [[Refrain] Fukigen na Mononokean - Tsuzuki - 07 VOSTFR [720p]](https://uptobox.com/2dulos1erfqt)
- [[Refrain] Fukigen na Mononokean - Tsuzuki - 07 VOSTFR [1080p]](https://uptobox.com/aenu64a7irse)
- [[Refrain] Fukigen na Mononokean - Tsuzuki - 08 VOSTFR [720p]](https://uptobox.com/nbvckbz1c4w9)
- [[Refrain] Fukigen na Mononokean - Tsuzuki - 08 VOSTFR [1080p]](https://uptobox.com/fsz2gdjd5nuw)
- [[Refrain] Fukigen na Mononokean - Tsuzuki - 09 VOSTFR [720p]](https://uptobox.com/tgay2qfyvf2b)
- [[Refrain] Fukigen na Mononokean - Tsuzuki - 09 VOSTFR [1080p]](https://uptobox.com/5f4n61ab58mu)
- [[Refrain] Fukigen na Mononokean - Tsuzuki - 10 VOSTFR [720p]](https://uptobox.com/2pgpcxzlxmyp)
- [[Refrain] Fukigen na Mononokean - Tsuzuki - 10 VOSTFR [1080p]](https://uptobox.com/zh0x6a9snc1m)
- [[Refrain] Fukigen na Mononokean - Tsuzuki - 11 VOSTFR [720p]](https://uptobox.com/4z72nq302pxj)
- [[Refrain] Fukigen na Mononokean - Tsuzuki - 11 VOSTFR [1080p]](https://uptobox.com/4z8zrv6wi53b)
- [[Refrain] Fukigen na Mononokean - Tsuzuki - 12 VOSTFR [720p]](https://uptobox.com/zy450reptb5y)
- [[Refrain] Fukigen na Mononokean - Tsuzuki - 12 VOSTFR [1080p]](https://uptobox.com/pbjy2782bgn6)
- [[Refrain] Fukigen na Mononokean - Tsuzuki - 13 VOSTFR [720p]](https://uptobox.com/gze1utrkqeje)
- [[Refrain] Fukigen na Mononokean - Tsuzuki - 13 VOSTFR [1080p]](https://uptobox.com/0inrrgquhvh1)
