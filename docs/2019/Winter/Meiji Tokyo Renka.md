# Meiji Tokyo Renka

![Meiji Tokyo Renka](https://cdn.myanimelist.net/images/anime/1205/96175l.jpg)

* Japanese:  明治東亰恋伽

## Information

 - Type: TV
 - Episodes: 12
 - Status: Currently Airing
 - Aired: Jan 9, 2019 to ?
 - Premiered: Winter 2019
 - Broadcast: Wednesdays at 23:30 (JST)
 - Producers: None found, add some 
 - Licensors: None found, add some 
 - Studios: TMS Entertainment 
 - Source: Game
 - Genres: Historical, Romance, Fantasy, Shoujo
 - Duration: Unknown
 - Rating: None


## Links

