# Mahou Shoujo Tokushusen Asuka

![Mahou Shoujo Tokushusen Asuka](https://cdn.myanimelist.net/images/anime/1993/96614l.jpg)

* Japanese:  魔法少女特殊戦あすか

## Information

 - Type: TV
 - Episodes: Unknown
 - Status: Currently Airing
 - Aired: Jan 12, 2019 to ?
 - Premiered: Winter 2019
 - Broadcast: Saturdays at 02:25 (JST)
 - Producers: None found, add some 
 - Licensors: None found, add some 
 - Studios: LIDENFILMS 
 - Source: Manga
 - Genres: Drama, Magic, Seinen
 - Duration: Unknown
 - Rating: R - 17+ (violence & profanity)


## Links

- [[Refrain] Mahou Shoujo Tokushusen Asuka - 01 VOSTFR [1080p]](https://uptobox.com/b6zzjpsjn9o7)
- [[Refrain] Mahou Shoujo Tokushusen Asuka - 01 VOSTFR [720p]](https://uptobox.com/89d4rdplc0e2)
- [[Refrain] Mahou Shoujo Tokushusen Asuka - 02 VOSTFR [1080p]](https://uptobox.com/d61lbh87sp0d)
- [[Refrain] Mahou Shoujo Tokushusen Asuka - 02 VOSTFR [720p]](https://uptobox.com/wd658z0fpone)
- [[Refrain] Mahou Shoujo Tokushusen Asuka - 03 VOSTFR [1080p]](https://uptobox.com/uycujxekcfia)
- [[Refrain] Mahou Shoujo Tokushusen Asuka - 03 VOSTFR [720p]](https://uptobox.com/duvdecufhnzc)
- [[Refrain] Mahou Shoujo Tokushusen Asuka - 04 VOSTFR [1080p]](https://uptobox.com/2sy9ykmw987r)
- [[Refrain] Mahou Shoujo Tokushusen Asuka - 04 VOSTFR [720p]](https://uptobox.com/1v2fhj9ysh59)
- [[Refrain] Mahou Shoujo Tokushusen Asuka - 05 VOSTFR [1080p]](https://uptobox.com/gx06r7niv337)
- [[Refrain] Mahou Shoujo Tokushusen Asuka - 05 VOSTFR [720p]](https://uptobox.com/ly1n5pmwksra)
- [[Refrain] Mahou Shoujo Tokushusen Asuka - 06 VOSTFR [1080p]](https://uptobox.com/hvzcrwj4gg3g)
- [[Refrain] Mahou Shoujo Tokushusen Asuka - 06 VOSTFR [720p]](https://uptobox.com/64ac5nlhvbew)
- [[Refrain] Mahou Shoujo Tokushusen Asuka - 07 VOSTFR [1080p]](https://uptobox.com/xazd3vkacov5)
- [[Refrain] Mahou Shoujo Tokushusen Asuka - 07 VOSTFR [720p]](https://uptobox.com/94um9d2b257n)
- [[Refrain] Mahou Shoujo Tokushusen Asuka - 08 VOSTFR [1080p]](https://uptobox.com/pjn2ousugbgf)
- [[Refrain] Mahou Shoujo Tokushusen Asuka - 08 VOSTFR [720p]](https://uptobox.com/clzlde13mpb7)
- [[Refrain] Mahou Shoujo Tokushusen Asuka - 09 VOSTFR [1080p]](https://uptobox.com/ir521782pyr4)
- [[Refrain] Mahou Shoujo Tokushusen Asuka - 09 VOSTFR [720p]](https://uptobox.com/cz3iyotf9vup)
- [[Refrain] Mahou Shoujo Tokushusen Asuka - 10 VOSTFR [1080p]](https://uptobox.com/nmapubhv4b02)
- [[Refrain] Mahou Shoujo Tokushusen Asuka - 10 VOSTFR [720p]](https://uptobox.com/238u8zuguroq)
- [[Refrain] Mahou Shoujo Tokushusen Asuka - 11 VOSTFR [1080p]](https://uptobox.com/renez8hy1skh)
- [[Refrain] Mahou Shoujo Tokushusen Asuka - 11 VOSTFR [720p]](https://uptobox.com/j1ondft0bclk)
- [[Refrain] Mahou Shoujo Tokushusen Asuka - 12 VOSTFR [1080p]](https://uptobox.com/3rdoj3emo6jp)
- [[Refrain] Mahou Shoujo Tokushusen Asuka - 12 VOSTFR [720p]](https://uptobox.com/3vikwwoy6shi)
