# 3D Kanojo: Real Girl 2nd Season

![3D Kanojo: Real Girl 2nd Season](https://cdn.myanimelist.net/images/anime/1941/97219l.jpg)

* Japanese:  3D彼女 リアルガール第2シーズン

## Information

 - Type: TV
 - Episodes: 12
 - Status: Currently Airing
 - Aired: Jan 9, 2019 to ?
 - Premiered: Winter 2019
 - Broadcast: Wednesdays at 01:59 (JST)
 - Producers: None found, add some 
 - Licensors: Sentai Filmworks 
 - Studios: Hoods Entertainment 
 - Source: Manga
 - Genres: Romance, School, Shoujo
 - Duration: 20 min. per ep.
 - Rating: None


## Links

