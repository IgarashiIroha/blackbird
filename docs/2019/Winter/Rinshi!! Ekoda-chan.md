# Rinshi!! Ekoda-chan

![Rinshi!! Ekoda-chan](https://cdn.myanimelist.net/images/anime/1953/95142l.jpg)

* Synonyms: <small>Near Death!! Ekoda-chan</small>
* Japanese: 臨死!!江古田ちゃん

## Information

- Type: TV
- Episodes: 12
- Status: Currently Airing
- Aired: Jan 9, 2019 to ?
- Premiered: Winter 2019
- Broadcast: Wednesdays at 01:00 (JST)
- Producers: None found, add some
- Licensors: None found, add some
- Studios: None found, add some
- Source: 4-koma manga
- Genres: Slice of Life, Comedy, Seinen
- Duration: 4 min. per ep.
- Rating: PG-13 - Teens 13 or older

## Links

- [[Refrain] Rinshi!! Ekoda-chan - 01 VOSTFR [720p]](https://uptobox.com/d99mlvwh011q)
- [[Refrain] Rinshi!! Ekoda-chan - 01 VOSTFR [1080p]](https://uptobox.com/pb7g6lpms92x)
- [[Refrain] Rinshi!! Ekoda-chan - 02 VOSTFR [720p]](https://uptobox.com/apx70lvm2cv1)
- [[Refrain] Rinshi!! Ekoda-chan - 02 VOSTFR [1080p]](https://uptobox.com/24byrm7i7kae)
- [[Refrain] Rinshi!! Ekoda-chan - 03 VOSTFR [720p]](https://uptobox.com/xk2c2emy357o)
- [[Refrain] Rinshi!! Ekoda-chan - 03 VOSTFR [1080p]](https://uptobox.com/hm0zzd5wuucx)
- [[Refrain] Rinshi!! Ekoda-chan - 04 VOSTFR [720p]](https://uptobox.com/168xmaswp0ra)
- [[Refrain] Rinshi!! Ekoda-chan - 04 VOSTFR [1080p]](https://uptobox.com/ytcqvpp7w8zu)
- [[Refrain] Rinshi!! Ekoda-chan - 05 VOSTFR [720p]](https://uptobox.com/aw5cnzhdyzpd)
- [[Refrain] Rinshi!! Ekoda-chan - 05 VOSTFR [1080p]](https://uptobox.com/z1ceq4fpmkw6)
- [[Refrain] Rinshi!! Ekoda-chan - 06 VOSTFR [720p]](https://uptobox.com/khpodx0dduxi)
- [[Refrain] Rinshi!! Ekoda-chan - 06 VOSTFR [1080p]](https://uptobox.com/x01q9fndrhd7)
- [[Refrain] Rinshi!! Ekoda-chan - 07 VOSTFR [720p]](https://uptobox.com/sg5am4623dnt)
- [[Refrain] Rinshi!! Ekoda-chan - 07 VOSTFR [1080p]](https://uptobox.com/zbaseh44fb44)
- [[Refrain] Rinshi!! Ekoda-chan - 08 VOSTFR [720p]](https://uptobox.com/a545lavdkwlc)
- [[Refrain] Rinshi!! Ekoda-chan - 08 VOSTFR [1080p]](https://uptobox.com/aclkemp28d74)
- [[Refrain] Rinshi!! Ekoda-chan - 09 VOSTFR [720p]](https://uptobox.com/yu2qw1gw11n0)
- [[Refrain] Rinshi!! Ekoda-chan - 09 VOSTFR [1080p]](https://uptobox.com/sk1qs6yb323q)
- [[Refrain] Rinshi!! Ekoda-chan - 10 VOSTFR [720p]](https://uptobox.com/4fdonvuurp0o)
- [[Refrain] Rinshi!! Ekoda-chan - 10 VOSTFR [1080p]](https://uptobox.com/5y9d5gpeunlw)
- [[Refrain] Rinshi!! Ekoda-chan - 11 VOSTFR [720p]](https://uptobox.com/qmjlt3zf7ken)
- [[Refrain] Rinshi!! Ekoda-chan - 11 VOSTFR [1080p]](https://uptobox.com/j7q1lmwqxf82)
