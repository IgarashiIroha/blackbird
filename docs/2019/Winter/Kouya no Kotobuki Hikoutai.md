# Kouya no Kotobuki Hikoutai

![Kouya no Kotobuki Hikoutai](https://cdn.myanimelist.net/images/anime/1252/96179l.jpg)

* Japanese:  荒野のコトブキ飛行隊

## Information

 - Type: TV
 - Episodes: Unknown
 - Status: Currently Airing
 - Aired: Jan 13, 2019 to ?
 - Premiered: Winter 2019
 - Broadcast: Sundays at 22:30 (JST)
 - Producers: Digital Frontier, Bandai Namco Arts
 - Licensors: None found, add some
 - Studios: WAO World, GEMBA
 - Source: Original
 - Genres: Action, Military, Adventure
 - Duration: Unknown
 - Rating: None


## Links

- [[Refrain] Kouya no Kotobuki Hikoutai - 01 VOSTFR [1080p]](https://uptobox.com/wtc2mq2qk7ha)
- [[Refrain] Kouya no Kotobuki Hikoutai - 01 VOSTFR [720p]](https://uptobox.com/wf02198e6th0)
- [[Refrain] Kouya no Kotobuki Hikoutai - 02 VOSTFR [1080p]](https://uptobox.com/ss2ees3ey6yr)
- [[Refrain] Kouya no Kotobuki Hikoutai - 02 VOSTFR [720p]](https://uptobox.com/9wmwup3uokc6)
- [[Refrain] Kouya no Kotobuki Hikoutai - 03 VOSTFR [1080p]](https://uptobox.com/uvqa2qx4e2hz)
- [[Refrain] Kouya no Kotobuki Hikoutai - 03 VOSTFR [720p]](https://uptobox.com/uaraiq1axs7p)
- [[Refrain] Kouya no Kotobuki Hikoutai - 04 VOSTFR [1080p]](https://uptobox.com/0ift0caumdca)
- [[Refrain] Kouya no Kotobuki Hikoutai - 04 VOSTFR [720p]](https://uptobox.com/j9mchahohf7s)
- [[Refrain] Kouya no Kotobuki Hikoutai - 05 VOSTFR [1080p]](https://uptobox.com/1get5534sw6v)
- [[Refrain] Kouya no Kotobuki Hikoutai - 05 VOSTFR [720p]](https://uptobox.com/hbsudrq9vwma)
- [[Refrain] Kouya no Kotobuki Hikoutai - 06 VOSTFR [1080p]](https://uptobox.com/fnic3vz4iks2)
- [[Refrain] Kouya no Kotobuki Hikoutai - 06 VOSTFR [720p]](https://uptobox.com/e4hpk93neika)
- [[Refrain] Kouya no Kotobuki Hikoutai - 07 VOSTFR [1080p]](https://uptobox.com/y2fqlw5r9fnp)
- [[Refrain] Kouya no Kotobuki Hikoutai - 07 VOSTFR [720p]](https://uptobox.com/d14spzorxim3)
- [[Refrain] Kouya no Kotobuki Hikoutai - 08 VOSTFR [1080p]](https://uptobox.com/mm6aihp1c9y7)
- [[Refrain] Kouya no Kotobuki Hikoutai - 08 VOSTFR [720p]](https://uptobox.com/9ywdphnzwegl)
- [[Refrain] Kouya no Kotobuki Hikoutai - 09 VOSTFR [1080p]](https://uptobox.com/6v8ml8twnk64)
- [[Refrain] Kouya no Kotobuki Hikoutai - 09 VOSTFR [720p]](https://uptobox.com/175qm2vikbzf)
- [[Refrain] Kouya no Kotobuki Hikoutai - 10 VOSTFR [1080p]](https://uptobox.com/c12lvobvqcpw)
- [[Refrain] Kouya no Kotobuki Hikoutai - 10 VOSTFR [720p]](https://uptobox.com/ncimnhnwep9o)
- [[Refrain] Kouya no Kotobuki Hikoutai - 11 VOSTFR [1080p]](https://uptobox.com/m14bo0ekrguo)
- [[Refrain] Kouya no Kotobuki Hikoutai - 11 VOSTFR [720p]](https://uptobox.com/tx6x2fv3sywy)
- [[Refrain] Kouya no Kotobuki Hikoutai - 12 VOSTFR [1080p]](https://uptobox.com/t0fjsk6kh0iw)
- [[Refrain] Kouya no Kotobuki Hikoutai - 12 VOSTFR [720p]](https://uptobox.com/v4txsot7poju)
