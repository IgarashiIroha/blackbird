# Fight League: Gear Gadget Generators

![Fight League: Gear Gadget Generators](https://myanimelist.cdn-dena.com/images/anime/1351/96571l.jpg)

* Japanese:  ファイトリーグ ギア・ガジェット・ジェネレーターズ

## Information

 - Type: ONA
 - Episodes: Unknown
 - Status: Currently Airing
 - Aired: Feb 14, 2019 to ?
 - Producers: Bandai Entertainment, Magic Capsule
 - Licensors: None found, add some
 - Studios: Sunrise, Bandai Namco Pictures, XFLAG
 - Source: Game
 - Genres: Action, Sci-Fi, Mecha
 - Duration: 15 min.
 - Rating: PG-13 - Teens 13 or older


## Links

- [[Refrain] Fight League - Gear Gadget Generators - 01 VOSTFR [720p]](https://uptobox.com/mvj3s39wz43z)
- [[Refrain] Fight League - Gear Gadget Generators - 01 VOSTFR [1080p]](https://uptobox.com/uo778csb6nte)
- [[Refrain] Fight League - Gear Gadget Generators - 03 VOSTFR [720p]](https://uptobox.com/73rp9cui53sg)
- [[Refrain] Fight League - Gear Gadget Generators - 03 VOSTFR [1080p]](https://uptobox.com/7no4cvsf3959)
- [[Refrain] Fight League - Gear Gadget Generators - 04 VOSTFR [720p]](https://uptobox.com/lu6c0fpqmg6f)
- [[Refrain] Fight League - Gear Gadget Generators - 04 VOSTFR [1080p]](https://uptobox.com/pz944fiilwtn)
- [[Refrain] Fight League - Gear Gadget Generators - 05 VOSTFR [720p]](https://uptobox.com/cm4hxanj1g4w)
- [[Refrain] Fight League - Gear Gadget Generators - 05 VOSTFR [1080p]](https://uptobox.com/svqtafxv0gem)
- [[Refrain] Fight League - Gear Gadget Generators - 06 VOSTFR [720p]](https://uptobox.com/yyv8eb5l32r4)
- [[Refrain] Fight League - Gear Gadget Generators - 06 VOSTFR [1080p]](https://uptobox.com/y6kj7dt1s6cr)
- [[Refrain] Fight League - Gear Gadget Generators - 07 VOSTFR [720p]](https://uptobox.com/loz6qn4auml5)
- [[Refrain] Fight League - Gear Gadget Generators - 07 VOSTFR [1080p]](https://uptobox.com/bchj0mdly287)
- [[Refrain] Fight League - Gear Gadget Generators - 02 VOSTFR [720p]](https://uptobox.com/077yuvp6hxyc)
- [[Refrain] Fight League - Gear Gadget Generators - 02 VOSTFR [1080p]](https://uptobox.com/cz84ikqrvkpw)
- [[Refrain] Fight League - Gear Gadget Generators - 08 VOSTFR [720p]](https://uptobox.com/aotc4og38mda)
- [[Refrain] Fight League - Gear Gadget Generators - 08 VOSTFR [1080p]](https://uptobox.com/0pcdrroqinq2)
- [[Refrain] Fight League - Gear Gadget Generators - 09 VOSTFR [720p]](https://uptobox.com/bz3zjgzt3sku)
- [[Refrain] Fight League - Gear Gadget Generators - 09 VOSTFR [1080p]](https://uptobox.com/w5kax4lwtqm1)
- [[Refrain] Fight League - Gear Gadget Generators - 10 VOSTFR [720p]](https://uptobox.com/p6g20p0smzko)
- [[Refrain] Fight League - Gear Gadget Generators - 10 VOSTFR [1080p]](https://uptobox.com/dclcykvrxt7w)
- [[Refrain] Fight League - Gear Gadget Generators - 11 VOSTFR [720p]](https://uptobox.com/ncebtnmvoim4)
- [[Refrain] Fight League - Gear Gadget Generators - 11 VOSTFR [1080p]](https://uptobox.com/4e90jeizhd6x)
- [[Refrain] Fight League - Gear Gadget Generators - 12 VOSTFR [720p]](https://uptobox.com/nsdy3w4j82ca)
- [[Refrain] Fight League - Gear Gadget Generators - 12 VOSTFR [1080p]](https://uptobox.com/qgadm5h6bj1m)
- [[Refrain] Fight League - Gear Gadget Generators - 13 VOSTFR [720p]](https://uptobox.com/sf9rhi7avy7y)
- [[Refrain] Fight League - Gear Gadget Generators - 13 VOSTFR [1080p]](https://uptobox.com/ve5i5j7a8gmt)
- [[Refrain] Fight League - Gear Gadget Generators - 14 VOSTFR [720p]](https://uptobox.com/woir064rkgjl)
- [[Refrain] Fight League - Gear Gadget Generators - 14 VOSTFR [1080p]](https://uptobox.com/785rnzryhw6j)
- [[Refrain] Fight League - Gear Gadget Generators - 15 VOSTFR [720p]](https://uptobox.com/jefj0g9kswxs)
- [[Refrain] Fight League - Gear Gadget Generators - 15 VOSTFR [1080p]](https://uptobox.com/hgdsacd5qlx0)
- [[Refrain] Fight League - Gear Gadget Generators - 16 VOSTFR [720p]](https://uptobox.com/85eaexqr4rii)
- [[Refrain] Fight League - Gear Gadget Generators - 16 VOSTFR [1080p]](https://uptobox.com/pmbf6t7w053v)
- [[Refrain] Fight League - Gear Gadget Generators - 17 VOSTFR [720p]](https://uptobox.com/luxuwwqt1jvj)
- [[Refrain] Fight League - Gear Gadget Generators - 17 VOSTFR [1080p]](https://uptobox.com/vor3ho4ond29)
