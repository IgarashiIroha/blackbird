# Revisions

![Revisions](https://cdn.myanimelist.net/images/anime/1062/97007l.jpg)

* Japanese:  revisions リヴィジョンズ

## Information

 - Type: ONA
 - Episodes: 12
 - Status: Finished Airing
 - Aired: Jan 10, 2019
 - Producers: Fuji TV 
 - Licensors: None found, add some 
 - Studios: Shirogumi 
 - Source: Original
 - Genres: Action, Sci-Fi, Mecha
 - Duration: Unknown
 - Rating: None


## Links

- [[Refrain] Revisions - 01 VF VOSTFR [1080p]](https://uptobox.com/sork4ensroxs)
- [[Refrain] Revisions - 01 VF VOSTFR [720p]](https://uptobox.com/5p2gri3k0ttz)
- [[Refrain] Revisions - 02 VF VOSTFR [1080p]](https://uptobox.com/vl6w2at7h09p)
- [[Refrain] Revisions - 02 VF VOSTFR [720p]](https://uptobox.com/c13j22f6expu)
- [[Refrain] Revisions - 03 VF VOSTFR [1080p]](https://uptobox.com/wssoywtkht72)
- [[Refrain] Revisions - 03 VF VOSTFR [720p]](https://uptobox.com/zsbvj17fu2ta)
- [[Refrain] Revisions - 04 VF VOSTFR [1080p]](https://uptobox.com/ruskhv5x39rp)
- [[Refrain] Revisions - 04 VF VOSTFR [720p]](https://uptobox.com/ibrg5b32nb8q)
- [[Refrain] Revisions - 05 VF VOSTFR [1080p]](https://uptobox.com/9177vcnw106m)
- [[Refrain] Revisions - 05 VF VOSTFR [720p]](https://uptobox.com/vbglyu266gwu)
- [[Refrain] Revisions - 06 VF VOSTFR [1080p]](https://uptobox.com/fovxn7f268io)
- [[Refrain] Revisions - 06 VF VOSTFR [720p]](https://uptobox.com/dv5ei5457ktb)
- [[Refrain] Revisions - 07 VF VOSTFR [1080p]](https://uptobox.com/g4abyrzema8m)
- [[Refrain] Revisions - 07 VF VOSTFR [720p]](https://uptobox.com/orslg8ek330f)
- [[Refrain] Revisions - 08 VF VOSTFR [1080p]](https://uptobox.com/5yfpy9416yrk)
- [[Refrain] Revisions - 08 VF VOSTFR [720p]](https://uptobox.com/c2j92n37np09)
- [[Refrain] Revisions - 09 VF VOSTFR [1080p]](https://uptobox.com/azgel8edfmfb)
- [[Refrain] Revisions - 09 VF VOSTFR [720p]](https://uptobox.com/o67vaaoipvbz)
- [[Refrain] Revisions - 10 VF VOSTFR [1080p]](https://uptobox.com/ay7jcyiq5nv4)
- [[Refrain] Revisions - 11 VF VOSTFR [1080p]](https://uptobox.com/0dwwwbcpictw)
- [[Refrain] Revisions - 11 VF VOSTFR [720p]](https://uptobox.com/37jzea5dpdu9)
- [[Refrain] Revisions - 12 VF VOSTFR [1080p]](https://uptobox.com/qizp65licsk7)
- [[Refrain] Revisions - 12 VF VOSTFR [720p]](https://uptobox.com/q8w5kfkxvurq)
