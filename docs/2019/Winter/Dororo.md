# Dororo

![Dororo](https://cdn.myanimelist.net/images/anime/1933/97061l.jpg)

* Japanese:  どろろ

## Information

 - Type: TV
 - Episodes: Unknown
 - Status: Currently Airing
 - Aired: Jan 7, 2019 to ?
 - Premiered: Winter 2019
 - Broadcast: Mondays at 22:30 (JST)
 - Producers: Twin Engine 
 - Licensors: None found, add some 
 - Studios: Tezuka Productions, MAPPA 
 - Source: Manga
 - Genres: Action, Adventure, Demons, Historical, Shounen, Supernatural
 - Duration: Unknown
 - Rating: R - 17+ (violence & profanity)


## Links

- [[Refrain] Dororo - 01 VOSTFR [720p]](https://uptobox.com/hemmh1luc2ml)
- [[Refrain] Dororo - 01 VOSTFR [1080p]](https://uptobox.com/edpibboa1l5j)
- [[Refrain] Dororo - 02 VOSTFR [1080p]](https://uptobox.com/oedkkgtnqwhd)
- [[Refrain] Dororo - 02 VOSTFR [720p]](https://uptobox.com/lgxp1rv85n26)
- [[Refrain] Dororo - 03 VOSTFR [1080p]](https://uptobox.com/fdqjub6d6g4e)
- [[Refrain] Dororo - 03 VOSTFR [720p]](https://uptobox.com/6519or7iu9tx)
- [[Refrain] Dororo - 04 VOSTFR [1080p]](https://uptobox.com/tqxazoy6btof)
- [[Refrain] Dororo - 04 VOSTFR [720p]](https://uptobox.com/xedgqw4q2a2t)
