# Bermuda Triangle: Colorful Pastrale

![Bermuda Triangle: Colorful Pastrale](https://cdn.myanimelist.net/images/anime/1449/93947l.jpg)

* Japanese:  バミューダトライアングル ～カラフル・パストラーレ～

## Information

 - Type: TV
 - Episodes: Unknown
 - Status: Currently Airing
 - Aired: Jan 12, 2019 to ?
 - Premiered: Winter 2019
 - Broadcast: Saturdays at 22:00 (JST)
 - Producers: Bushiroad 
 - Licensors: Sentai Filmworks 
 - Studios: Seven Arcs Pictures 
 - Source: Original
 - Genres: Music, Fantasy
 - Duration: Unknown
 - Rating: PG-13 - Teens 13 or older


## Links

