# BanG Dream! 2nd Season

![BanG Dream! 2nd Season](https://cdn.myanimelist.net/images/anime/1106/98131l.jpg)

* Japanese:  BanG Dream!（バンドリ！）第2期

## Information

 - Type: TV
 - Episodes: Unknown
 - Status: Currently Airing
 - Aired: Jan 3, 2019 to ?
 - Premiered: Winter 2019
 - Broadcast: Thursdays at 23:00 (JST)
 - Producers: Bushiroad
 - Licensors: Sentai Filmworks
 - Studios: SANZIGEN
 - Source: Manga
 - Genres: Music
 - Duration: 24 min.
 - Rating: PG-13 - Teens 13 or older


## Links
- [[Refrain] Bang Dream! 2nd Season - 01 VOSTFR [1080p]](https://uptobox.com/00zryanh5k7v)
- [[Refrain] Bang Dream! 2nd Season - 01 VOSTFR [720p]](https://uptobox.com/y61zfalic3us)
- [[Refrain] Bang Dream! 2nd Season - 02 VOSTFR [1080p]](https://uptobox.com/ltavfb8tm5dv)
- [[Refrain] Bang Dream! 2nd Season - 02 VOSTFR [720p]](https://uptobox.com/jrx8dh0ccjej)
- [[Refrain] Bang Dream! 2nd Season - 03 VOSTFR [1080p]](https://uptobox.com/0uxk14vxa019)
- [[Refrain] Bang Dream! 2nd Season - 03 VOSTFR [720p]](https://uptobox.com/b215isqqus11)
- [[Refrain] Bang Dream! 2nd Season - 04 VOSTFR [1080p]](https://uptobox.com/r17gw6ci2me1)
- [[Refrain] Bang Dream! 2nd Season - 04 VOSTFR [720p]](https://uptobox.com/fg4vhxznil4b)
- [[Refrain] Bang Dream! 2nd Season - 05 VOSTFR [1080p]](https://uptobox.com/oogqq6a5uuwg)
- [[Refrain] Bang Dream! 2nd Season - 05 VOSTFR [720p]](https://uptobox.com/vbfl9r1gojmd)
- [[Refrain] Bang Dream! 2nd Season - 06 VOSTFR [1080p]](https://uptobox.com/y5j1jcscx0is)
- [[Refrain] Bang Dream! 2nd Season - 06 VOSTFR [720p]](https://uptobox.com/tuvmcw9089b7)
- [[Refrain] Bang Dream! 2nd Season - 07 VOSTFR [1080p]](https://uptobox.com/ru0ehhlwg0es)
- [[Refrain] Bang Dream! 2nd Season - 07 VOSTFR [720p]](https://uptobox.com/k161ifc0nlor)
- [[Refrain] Bang Dream! 2nd Season - 08 VOSTFR [1080p]](https://uptobox.com/rp0ve9lvzkvi)
- [[Refrain] Bang Dream! 2nd Season - 08 VOSTFR [720p]](https://uptobox.com/6ntewa1yq19j)
- [[Refrain] Bang Dream! 2nd Season - 09 VOSTFR [1080p]](https://uptobox.com/dxmrpy3r2i2b)
- [[Refrain] Bang Dream! 2nd Season - 09 VOSTFR [720p]](https://uptobox.com/el2yzmhs3o5j)
- [[Refrain] Bang Dream! 2nd Season - 10 VOSTFR [1080p]](https://uptobox.com/phr785567lb7)
- [[Refrain] Bang Dream! 2nd Season - 10 VOSTFR [720p]](https://uptobox.com/rrbylapj1he9)
- [[Refrain] Bang Dream! 2nd Season - 11 VOSTFR [1080p]](https://uptobox.com/iw1esxhumwlk)
- [[Refrain] Bang Dream! 2nd Season - 11 VOSTFR [720p]](https://uptobox.com/pkv2mbzfndva)
- [[Refrain] Bang Dream! 2nd Season - 12 VOSTFR [1080p]](https://uptobox.com/z074aezwn9gw)
- [[Refrain] Bang Dream! 2nd Season - 12 VOSTFR [720p]](https://uptobox.com/zwxshcu0gqdi)
- [[Refrain] Bang Dream! 2nd Season - 13 VOSTFR [1080p]](https://uptobox.com/a38eq9p5mr3t)
- [[Refrain] Bang Dream! 2nd Season - 13 VOSTFR [720p]](https://uptobox.com/jtwmdxhxknpy)
