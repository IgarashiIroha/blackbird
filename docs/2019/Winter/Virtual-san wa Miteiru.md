# Virtual-san wa Miteiru

![Virtual-san wa Miteiru](https://cdn.myanimelist.net/images/anime/1512/97579l.jpg)

* Japanese:  バーチャルさんはみている

## Information

 - Type: TV
 - Episodes: 12
 - Status: Currently Airing
 - Aired: Jan 10, 2019 to ?
 - Premiered: Winter 2019
 - Broadcast: Thursdays at 00:00 (JST)
 - Producers: Dwango
 - Licensors: None found, add some
 - Studios: Lide
 - Source: Original
 - Genres: Music, Comedy
 - Duration: Unknown
 - Rating: None


## Links

- [[Refrain] Virtual-san wa Miteiru - 01 VOSTFR [720p]](https://uptobox.com/0qjmd6muyvyc)
- [[Refrain] Virtual-san wa Miteiru - 01 VOSTFR [1080p]](https://uptobox.com/7gh46eur1b1c)
- [[Refrain] Virtual-san wa Miteiru - 02 VOSTFR [720p]](https://uptobox.com/apxtxtpzpxjg)
- [[Refrain] Virtual-san wa Miteiru - 02 VOSTFR [1080p]](https://uptobox.com/3puc53pltcst)
- [[Refrain] Virtual-san wa Miteiru - 07 VOSTFR [720p]](https://uptobox.com/q7n58dh2wdq1)
- [[Refrain] Virtual-san wa Miteiru - 07 VOSTFR [1080p]](https://uptobox.com/9h3ln0838c5d)
- [[Refrain] Virtual-san wa Miteiru - 08 VOSTFR [720p]](https://uptobox.com/mux9vv5d02qz)
- [[Refrain] Virtual-san wa Miteiru - 08 VOSTFR [1080p]](https://uptobox.com/8u1al7c0s6x8)
- [[Refrain] Virtual-san wa Miteiru - 09 VOSTFR [720p]](https://uptobox.com/bfpi6khrxbyc)
- [[Refrain] Virtual-san wa Miteiru - 09 VOSTFR [1080p]](https://uptobox.com/iyspqo379ill)
- [[Refrain] Virtual-san wa Miteiru - 10 VOSTFR [720p]](https://uptobox.com/01z23k7sd6zs)
- [[Refrain] Virtual-san wa Miteiru - 10 VOSTFR [1080p]](https://uptobox.com/8bdrun6ik649)
- [[Refrain] Virtual-san wa Miteiru - 11 VOSTFR [720p]](https://uptobox.com/5othj9n944p1)
- [[Refrain] Virtual-san wa Miteiru - 11 VOSTFR [1080p]](https://uptobox.com/abc9yl71zlvp)
- [[Refrain] Virtual-san wa Miteiru - 12 VOSTFR [720p]](https://uptobox.com/7un4fehmdkma)
- [[Refrain] Virtual-san wa Miteiru - 12 VOSTFR [1080p]](https://uptobox.com/xzshn8zn15lm)
- [[Refrain] Virtual-san wa Miteiru - 06 VOSTFR [720p]](https://uptobox.com/vdp6xvg26bq4)
- [[Refrain] Virtual-san wa Miteiru - 06 VOSTFR [1080p]](https://uptobox.com/xn441ei3etbz)
- [[Refrain] Virtual-san wa Miteiru - 05 VOSTFR [720p]](https://uptobox.com/xrpvapp6czci)
- [[Refrain] Virtual-san wa Miteiru - 05 VOSTFR [1080p]](https://uptobox.com/lk87gipz2pmk)
- [[Refrain] Virtual-san wa Miteiru - 04 VOSTFR [720p]](https://uptobox.com/c161ap1ebd04)
- [[Refrain] Virtual-san wa Miteiru - 04 VOSTFR [1080p]](https://uptobox.com/jgtmqpmzorfs)
- [[Refrain] Virtual-san wa Miteiru - 03 VOSTFR [720p]](https://uptobox.com/bjyl21rxetz6)
- [[Refrain] Virtual-san wa Miteiru - 03 VOSTFR [1080p]](https://uptobox.com/sy63imai3fe8)
