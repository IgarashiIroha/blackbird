# Grimms Notes The Animation

![Grimms Notes The Animation](https://cdn.myanimelist.net/images/anime/1708/96962l.jpg)

* Japanese:  グリムノーツ

## Information

 - Type: TV
 - Episodes: Unknown
 - Status: Currently Airing
 - Aired: Jan 11, 2019 to ?
 - Premiered: Winter 2019
 - Broadcast: Fridays at 01:58 (JST)
 - Producers: None found, add some
 - Licensors: None found, add some
 - Studios: Brain&#039;s Base
 - Source: Game
 - Genres: Action, Adventure, Magic, Fantasy
 - Duration: Unknown
 - Rating: None


## Links

- [[Refrain] Grimms Notes The Animation - 01 VOSTFR {ADN} [720p]](https://uptobox.com/kkhkkqd8ukzx)
- [[Refrain] Grimms Notes The Animation - 01 VOSTFR {ADN} [1080p]](https://uptobox.com/i2r1hccids2v)
- [[Refrain] Grimms Notes The Animation - 02 VOSTFR {ADN} [720p]](https://uptobox.com/rygau3btl9ol)
- [[Refrain] Grimms Notes The Animation - 02 VOSTFR {ADN} [1080p]](https://uptobox.com/z7a0g9ffuw39)
- [[Refrain] Grimms Notes The Animation - 03 VOSTFR {ADN} [720p]](https://uptobox.com/6b4p87mudm5v)
- [[Refrain] Grimms Notes The Animation - 03 VOSTFR {ADN} [1080p]](https://uptobox.com/5jy3enciokhc)
- [[Refrain] Grimms Notes The Animation - 04 VOSTFR {ADN} [720p]](https://uptobox.com/vm2n4d83vzsj)
- [[Refrain] Grimms Notes The Animation - 04 VOSTFR {ADN} [1080p]](https://uptobox.com/vyhe1wx2fr34)
- [[Refrain] Grimms Notes The Animation - 05 VOSTFR {ADN} [720p]](https://uptobox.com/se4df9c53xtm)
- [[Refrain] Grimms Notes The Animation - 05 VOSTFR {ADN} [1080p]](https://uptobox.com/j1ojppzrme4n)
- [[Refrain] Grimms Notes The Animation - 06 VOSTFR {ADN} [720p]](https://uptobox.com/zfu4zesfbpmy)
- [[Refrain] Grimms Notes The Animation - 06 VOSTFR {ADN} [1080p]](https://uptobox.com/8e752zbae7as)
- [[Refrain] Grimms Notes The Animation - 07 VOSTFR {ADN} [720p]](https://uptobox.com/c0k20lzbcmcq)
- [[Refrain] Grimms Notes The Animation - 07 VOSTFR {ADN} [1080p]](https://uptobox.com/nljv7gqzevrg)
- [[Refrain] Grimms Notes The Animation - 08 VOSTFR {ADN} [720p]](https://uptobox.com/uskmyi1iu0dh)
- [[Refrain] Grimms Notes The Animation - 08 VOSTFR {ADN} [1080p]](https://uptobox.com/ap21hjcplmkr)
- [[Refrain] Grimms Notes The Animation - 09 VOSTFR {ADN} [720p]](https://uptobox.com/31sn2nbdvpwy)
- [[Refrain] Grimms Notes The Animation - 09 VOSTFR {ADN} [1080p]](https://uptobox.com/v05ek7ejf7pp)
- [[Refrain] Grimms Notes The Animation - 10 VOSTFR {ADN} [720p]](https://uptobox.com/y30s0npb4a19)
- [[Refrain] Grimms Notes The Animation - 10 VOSTFR {ADN} [1080p]](https://uptobox.com/ouywrbgnbwjg)
- [[Refrain] Grimms Notes The Animation - 11 VOSTFR {ADN} [720p]](https://uptobox.com/fklph689ru3g)
- [[Refrain] Grimms Notes The Animation - 11 VOSTFR {ADN} [1080p]](https://uptobox.com/w326whcan26s)
- [[Refrain] Grimms Notes The Animation - 12 VOSTFR {ADN} [720p]](https://uptobox.com/c749kmsbqykq)
- [[Refrain] Grimms Notes The Animation - 12 VOSTFR {ADN} [1080p]](https://uptobox.com/ibryxr0na3h8)
