# Pastel Memories

![Pastel Memories](https://cdn.myanimelist.net/images/anime/1563/95294l.jpg)


* Japanese: ぱすてるメモリーズ

## Information

- Type: TV
- Episodes: 12
- Status: Currently Airing
- Aired: Jan 8, 2019 to ?
- Premiered: Winter 2019
- Broadcast: Tuesdays at 00:30 (JST)
- Producers: MAGES., Furyu
- Licensors: Sentai Filmworks
- Studios: Project No.9
- Source: Game
- Genres: Action, Sci-Fi, Adventure
- Duration: Unknown
- Rating: PG-13 - Teens 13 or older

## Links

- [[Refrain] Pastel Memories - 01 VOSTFR {ADN} [720p]](https://uptobox.com/ajaxn3r8dsaa)
- [[Refrain] Pastel Memories - 01 VOSTFR {ADN} [1080p]](https://uptobox.com/7oxh5f9nfyo1)
- [[Refrain] Pastel Memories - 02 VOSTFR {ADN} [720p]](https://uptobox.com/rbohh710w6s5)
- [[Refrain] Pastel Memories - 02 VOSTFR {ADN} [1080p]](https://uptobox.com/ix81cwf04a3z)
- [[Refrain] Pastel Memories - 03 VOSTFR {ADN} [720p]](https://uptobox.com/5qpmxng5yiby)
- [[Refrain] Pastel Memories - 03 VOSTFR {ADN} [1080p]](https://uptobox.com/b7ra6eb5hi2f)
- [[Refrain] Pastel Memories - 04 VOSTFR {ADN} [720p]](https://uptobox.com/udji95sv43bj)
- [[Refrain] Pastel Memories - 04 VOSTFR {ADN} [1080p]](https://uptobox.com/cqdkbbg2ld7p)
- [[Refrain] Pastel Memories - 05 VOSTFR {ADN} [720p]](https://uptobox.com/9gjrefqlp6p3)
- [[Refrain] Pastel Memories - 05 VOSTFR {ADN} [1080p]](https://uptobox.com/6ozbouzrho49)
- [[Refrain] Pastel Memories - 06 VOSTFR {ADN} [720p]](https://uptobox.com/l4eqknek7yhd)
- [[Refrain] Pastel Memories - 06 VOSTFR {ADN} [1080p]](https://uptobox.com/96o6ag7vv9p2)
- [[Refrain] Pastel Memories - 07 VOSTFR {ADN} [720p]](https://uptobox.com/0jg1simh35fr)
- [[Refrain] Pastel Memories - 07 VOSTFR {ADN} [1080p]](https://uptobox.com/rvle0ruvush2)
- [[Refrain] Pastel Memories - 08 VOSTFR {ADN} [720p]](https://uptobox.com/afkq0p8phtvk)
- [[Refrain] Pastel Memories - 08 VOSTFR {ADN} [1080p]](https://uptobox.com/nrhvlolj7zmr)
- [[Refrain] Pastel Memories - 09 VOSTFR {ADN} [720p]](https://uptobox.com/yyu200xpimr0)
- [[Refrain] Pastel Memories - 09 VOSTFR {ADN} [1080p]](https://uptobox.com/fz90gmomzme4)
- [[Refrain] Pastel Memories - 10 VOSTFR {ADN} [720p]](https://uptobox.com/k1th7w52mhbm)
- [[Refrain] Pastel Memories - 10 VOSTFR {ADN} [1080p]](https://uptobox.com/cdjfjyl3gj5t)
- [[Refrain] Pastel Memories - 11 VOSTFR {ADN} [720p]](https://uptobox.com/uxlaec1tvrsy)
- [[Refrain] Pastel Memories - 11 VOSTFR {ADN} [1080p]](https://uptobox.com/6kbbw8szv5u9)
- [[Refrain] Pastel Memories - 12 VOSTFR {ADN} [720p]](https://uptobox.com/6kzkalk2trx9)
- [[Refrain] Pastel Memories - 12 VOSTFR {ADN} [1080p]](https://uptobox.com/7lkumnwgh5ve)
