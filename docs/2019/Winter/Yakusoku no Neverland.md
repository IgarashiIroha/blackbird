# Yakusoku no Neverland

![Yakusoku no Neverland](https://cdn.myanimelist.net/images/anime/1125/96929l.jpg)

* Japanese:  約束のネバーランド

## Information

 - Type: TV
 - Episodes: 12
 - Status: Currently Airing
 - Aired: Jan 11, 2019 to 2019
 - Premiered: Winter 2019
 - Broadcast: Fridays at 00:55 (JST)
 - Producers: Aniplex, Dentsu, A-1 Pictures, Fuji TV, Shueisha, CA-Cygames Anime Fund 
 - Licensors: Aniplex of America 
 - Studios: CloverWorks 
 - Source: Manga
 - Genres: Sci-Fi, Mystery, Horror, Shounen
 - Duration: Unknown
 - Rating: None


## Links

- [[Refrain] Yakusoku no Neverland - 01 VOSTFR [1080p]](https://uptobox.com/0mgzw18lg8nf)
- [[Refrain] Yakusoku no Neverland - 01 VOSTFR [720p]](https://uptobox.com/rjl3atpci67h)
- [[Refrain] Yakusoku no Neverland - 02 VOSTFR [1080p]](https://uptobox.com/mch7e9dvqf3k)
- [[Refrain] Yakusoku no Neverland - 02 VOSTFR [720p]](https://uptobox.com/57io79mkr0p0)
- [[Refrain] Yakusoku no Neverland - 03 VOSTFR [1080p]](https://uptobox.com/1xu7m3muveyo)
- [[Refrain] Yakusoku no Neverland - 03 VOSTFR [720p]](https://uptobox.com/rl4vl5dhlg1z)
- [[Refrain] Yakusoku no Neverland - 04 VOSTFR [1080p]](https://uptobox.com/dbr8797s8lp6)
- [[Refrain] Yakusoku no Neverland - 04 VOSTFR [720p]](https://uptobox.com/xui215br6evk)
- [[Refrain] Yakusoku no Neverland - 05 VOSTFR [1080p]](https://uptobox.com/nbszohfwtcdn)
- [[Refrain] Yakusoku no Neverland - 05 VOSTFR [720p]](https://uptobox.com/cfhjna6h8fdn)
- [[Refrain] Yakusoku no Neverland - 06 VOSTFR [1080p]](https://uptobox.com/ri3p6sapgm5a)
- [[Refrain] Yakusoku no Neverland - 06 VOSTFR [720p]](https://uptobox.com/0j4knufjbxe0)
- [[Refrain] Yakusoku no Neverland - 07 VOSTFR [1080p]](https://uptobox.com/8mh77vsyy2gi)
- [[Refrain] Yakusoku no Neverland - 07 VOSTFR [720p]](https://uptobox.com/civn954bfzv6)
- [[Refrain] Yakusoku no Neverland - 08 VOSTFR [1080p]](https://uptobox.com/pqptvnrd92h0)
- [[Refrain] Yakusoku no Neverland - 08 VOSTFR [720p]](https://uptobox.com/jgpr6qtric48)
- [[Refrain] Yakusoku no Neverland - 09 VOSTFR [1080p]](https://uptobox.com/yvhludvfqmp2)
- [[Refrain] Yakusoku no Neverland - 09 VOSTFR [720p]](https://uptobox.com/g47zye3sg549)
- [[Refrain] Yakusoku no Neverland - 10 VOSTFR [1080p]](https://uptobox.com/p6d9vk5rk4m0)
- [[Refrain] Yakusoku no Neverland - 10 VOSTFR [720p]](https://uptobox.com/380s4z09gon9)
- [[Refrain] Yakusoku no Neverland - 11 VOSTFR [1080p]](https://uptobox.com/u7ajy1hijehu)
- [[Refrain] Yakusoku no Neverland - 11 VOSTFR [720p]](https://uptobox.com/24wwplwifl28)
- [[Refrain] Yakusoku no Neverland - 12 VOSTFR [1080p]](https://uptobox.com/fcjkqztv55kf)
- [[Refrain] Yakusoku no Neverland - 12 VOSTFR [720p]](https://uptobox.com/nmyg4jvohdlj)
