# Kakegurui××

![Kakegurui××](https://cdn.myanimelist.net/images/anime/1985/97651l.jpg)

* Japanese:  賭ケグルイ××

## Information

 - Type: TV
 - Episodes: Unknown
 - Status: Currently Airing
 - Aired: Jan 9, 2019 to ?
 - Premiered: Winter 2019
 - Broadcast: Wednesdays at 02:30 (JST)
 - Producers: None found, add some 
 - Licensors: None found, add some 
 - Studios: MAPPA 
 - Source: Manga
 - Genres: Drama, Game, Mystery, Psychological, School, Shounen
 - Duration: Unknown
 - Rating: None


## Links

