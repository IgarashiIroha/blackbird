# Mini Toji

![Mini Toji](https://cdn.myanimelist.net/images/anime/1781/98227l.jpg)

* Japanese:  みにとじ

## Information

 - Type: TV
 - Episodes: Unknown
 - Status: Currently Airing
 - Aired: Jan 13, 2019 to ?
 - Premiered: Winter 2019
 - Broadcast: Sundays at 01:55 (JST)
 - Producers: None found, add some 
 - Licensors: None found, add some 
 - Studios: Project No.9 
 - Source: Original
 - Genres: Action, Comedy, Fantasy
 - Duration: 5 min.
 - Rating: None


## Links

- [[Refrain] Mini Toji - 01 VOSTFR [720p]](https://uptobox.com/yqykootzf47z)
- [[Refrain] Mini Toji - 01 VOSTFR [1080p]](https://uptobox.com/z1pdm1tuu1ou)
- [[Refrain] Mini Toji - 02 VOSTFR [720p]](https://uptobox.com/omv8fanaq8vy)
- [[Refrain] Mini Toji - 02 VOSTFR [1080p]](https://uptobox.com/mxiyr7tzf93c)
- [[Refrain] Mini Toji - 03 VOSTFR [720p]](https://uptobox.com/os575lv4mmax)
- [[Refrain] Mini Toji - 03 VOSTFR [1080p]](https://uptobox.com/t87gev4xiq65)
- [[Refrain] Mini Toji - 04 VOSTFR [720p]](https://uptobox.com/cbucr8jyb18f)
- [[Refrain] Mini Toji - 04 VOSTFR [1080p]](https://uptobox.com/wxyp23220xlm)
- [[Refrain] Mini Toji - 05 VOSTFR [720p]](https://uptobox.com/9msk74wpo0w2)
- [[Refrain] Mini Toji - 05 VOSTFR [1080p]](https://uptobox.com/0slj7ibi2tl5)
- [[Refrain] Mini Toji - 06 VOSTFR [720p]](https://uptobox.com/kva2t3vtle1a)
- [[Refrain] Mini Toji - 06 VOSTFR [1080p]](https://uptobox.com/qsbc0l5vp3n3)
- [[Refrain] Mini Toji - 09 VOSTFR [720p]](https://uptobox.com/3op95hf545ga)
- [[Refrain] Mini Toji - 09 VOSTFR [1080p]](https://uptobox.com/bazob4o4msrq)
- [[Refrain] Mini Toji - 10 VOSTFR [720p]](https://uptobox.com/jkglmwhjews9)
- [[Refrain] Mini Toji - 10 VOSTFR [1080p]](https://uptobox.com/tx6mtvypo1dr)
