# Endro~!

![Endro~!](https://cdn.myanimelist.net/images/anime/1475/96180l.jpg)

* Japanese:  えんどろ～！

## Information

 - Type: TV
 - Episodes: Unknown
 - Status: Currently Airing
 - Aired: Jan 13, 2019 to ?
 - Premiered: Winter 2019
 - Broadcast: Sundays at 01:00 (JST)
 - Producers: Egg Firm
 - Licensors: None found, add some
 - Studios: Studio Gokumi
 - Source: Original
 - Genres: Slice of Life, Magic, Fantasy
 - Duration: Unknown
 - Rating: None


## Links

- [[Refrain] Endro~! - 01 VOSTFR [1080p]](https://uptobox.com/82xcok0y5y56)
- [[Refrain] Endro~! - 01 VOSTFR [720p]](https://uptobox.com/bhlfqssud0f5)
- [[Refrain] Endro~! - 02 VOSTFR [1080p]](https://uptobox.com/u4sf6euvdtdh)
- [[Refrain] Endro~! - 02 VOSTFR [720p]](https://uptobox.com/qbd82betqaig)
- [[Refrain] Endro~! - 03 VOSTFR [1080p]](https://uptobox.com/tej76fw4vm8y)
- [[Refrain] Endro~! - 03 VOSTFR [720p]](https://uptobox.com/w9hfun6weipa)
- [[Refrain] Endro~! - 04 VOSTFR [1080p]](https://uptobox.com/z7pwvzba9qea)
- [[Refrain] Endro~! - 04 VOSTFR [720p]](https://uptobox.com/23lh9kmvcktc)
- [[Refrain] Endro~! - 05 VOSTFR [1080p]](https://uptobox.com/l09rxn9noc6v)
- [[Refrain] Endro~! - 05 VOSTFR [720p]](https://uptobox.com/04vgofgop9ee)
- [[Refrain] Endro~! - 06 VOSTFR [1080p]](https://uptobox.com/njsbbuucdill)
- [[Refrain] Endro~! - 06 VOSTFR [720p]](https://uptobox.com/kzrtn35nbow2)
- [[Refrain] Endro~! - 07 VOSTFR [1080p]](https://uptobox.com/ms8j6et1onyn)
- [[Refrain] Endro~! - 07 VOSTFR [720p]](https://uptobox.com/cydp0rzltndy)
- [[Refrain] Endro~! - 08 VOSTFR [1080p]](https://uptobox.com/lr9tjn4xr00e)
- [[Refrain] Endro~! - 08 VOSTFR [720p]](https://uptobox.com/dph53wuq64jb)
- [[Refrain] Endro~! - 09 VOSTFR [1080p]](https://uptobox.com/k96qt3yo3fso)
- [[Refrain] Endro~! - 09 VOSTFR [720p]](https://uptobox.com/9jb7q8zn3roy)
- [[Refrain] Endro~! - 10 VOSTFR [1080p]](https://uptobox.com/frlwifn6n8an)
- [[Refrain] Endro~! - 10 VOSTFR [720p]](https://uptobox.com/2jn55avkqzhy)
- [[Refrain] Endro~! - 11 VOSTFR [1080p]](https://uptobox.com/s13enpgr5as7)
- [[Refrain] Endro~! - 11 VOSTFR [720p]](https://uptobox.com/1rcfj7jy16ha)
- [[Refrain] Endro~! - 12 VOSTFR [1080p]](https://uptobox.com/0xl4vp5n2a2p)
- [[Refrain] Endro~! - 12 VOSTFR [720p]](https://uptobox.com/jia8xdmudwk8)
