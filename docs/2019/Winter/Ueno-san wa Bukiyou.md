# Ueno-san wa Bukiyou

![Ueno-san wa Bukiyou](https://cdn.myanimelist.net/images/anime/1942/93388l.jpg)

* Japanese:  上野さんは不器用

## Information

 - Type: TV
 - Episodes: Unknown
 - Status: Currently Airing
 - Aired: Jan 7, 2019 to ?
 - Premiered: Winter 2019
 - Broadcast: Mondays at 00:30 (JST)
 - Producers: None found, add some
 - Licensors: Sentai Filmworks
 - Studios: Lesprit
 - Source: Manga
 - Genres: Comedy, Romance, Seinen
 - Duration: 15 min.
 - Rating: None


## Links

- [[Refrain] Ueno-san wa Bukiyou - 01 VOSTFR [720p]](https://uptobox.com/puybu9qtvspt)
- [[Refrain] Ueno-san wa Bukiyou - 01 VOSTFR [1080p]](https://uptobox.com/zfmlnmgsyaif)
- [[Refrain] Ueno-san wa Bukiyou - 02 VOSTFR [720p]](https://uptobox.com/8iu6spmjl4l5)
- [[Refrain] Ueno-san wa Bukiyou - 02 VOSTFR [1080p]](https://uptobox.com/yxpapazzlq2w)
- [[Refrain] Ueno-san wa Bukiyou - 03 VOSTFR [720p]](https://uptobox.com/kl25bzph45ix)
- [[Refrain] Ueno-san wa Bukiyou - 03 VOSTFR [1080p]](https://uptobox.com/3xay5a0s7e6a)
- [[Refrain] Ueno-san wa Bukiyou - 04 VOSTFR [720p]](https://uptobox.com/bxxdackde8ao)
- [[Refrain] Ueno-san wa Bukiyou - 04 VOSTFR [1080p]](https://uptobox.com/egsg7wt16q7l)
- [[Refrain] Ueno-san wa Bukiyou - 05 VOSTFR [720p]](https://uptobox.com/377yajmnzgjf)
- [[Refrain] Ueno-san wa Bukiyou - 05 VOSTFR [1080p]](https://uptobox.com/tpv6lvmi74le)
- [[Refrain] Ueno-san wa Bukiyou - 06 VOSTFR [720p]](https://uptobox.com/l65jkl7xn9o7)
- [[Refrain] Ueno-san wa Bukiyou - 06 VOSTFR [1080p]](https://uptobox.com/12uwsy2obeey)
- [[Refrain] Ueno-san wa Bukiyou - 07 VOSTFR [720p]](https://uptobox.com/cyld53663xxf)
- [[Refrain] Ueno-san wa Bukiyou - 07 VOSTFR [1080p]](https://uptobox.com/khyrdwwj4cme)
- [[Refrain] Ueno-san wa Bukiyou - 08 VOSTFR [720p]](https://uptobox.com/4686gojgdanm)
- [[Refrain] Ueno-san wa Bukiyou - 08 VOSTFR [1080p]](https://uptobox.com/v648wxtldlff)
- [[Refrain] Ueno-san wa Bukiyou - 09 VOSTFR [720p]](https://uptobox.com/ad7ukh0i70dq)
- [[Refrain] Ueno-san wa Bukiyou - 09 VOSTFR [1080p]](https://uptobox.com/1l6cxgb8hhcp)
- [[Refrain] Ueno-san wa Bukiyou - 10 VOSTFR [720p]](https://uptobox.com/gjeburju0dxo)
- [[Refrain] Ueno-san wa Bukiyou - 10 VOSTFR [1080p]](https://uptobox.com/b46xaoxtoy1u)
- [[Refrain] Ueno-san wa Bukiyou - 11 VOSTFR [720p]](https://uptobox.com/sd8vhho3vl1y)
- [[Refrain] Ueno-san wa Bukiyou - 11 VOSTFR [1080p]](https://uptobox.com/ugcmrhv3fykt)
- [[Refrain] Ueno-san wa Bukiyou - 12 VOSTFR [720p]](https://uptobox.com/jxssrre90rk9)
- [[Refrain] Ueno-san wa Bukiyou - 12 VOSTFR [1080p]](https://uptobox.com/tw7rkalhpl8b)
