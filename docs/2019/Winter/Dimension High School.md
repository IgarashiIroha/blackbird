# Dimension High School

![Dimension High School](https://cdn.myanimelist.net/images/anime/1399/97349l.jpg)

* Japanese:  Dimensionハイスクール

## Information

 - Type: TV
 - Episodes: Unknown
 - Status: Currently Airing
 - Aired: Jan 10, 2019 to ?
 - Premiered: Winter 2019
 - Broadcast: Thursdays at 22:00 (JST)
 - Producers: Polygon Magic 
 - Licensors: None found, add some 
 - Studios: None found, add some 
 - Source: Original
 - Genres: School
 - Duration: 10 min.
 - Rating: None


## Links

