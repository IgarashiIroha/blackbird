# Ame-iro Cocoa - Side G


![Ame-iro Cocoa - Side G](https://cdn.myanimelist.net/images/anime/1463/97361l.jpg)


* Synonyms: <small>Ame-iro Cocoa 5th Season, Ameiro Cocoa, Rainy Cocoa, Ame-iro Cocoa 5</small>
* Japanese: 雨色ココア side G

## Information

- Type: TV
- Episodes: Unknown
- Status: Currently Airing
- Aired: Jan 9, 2019 to ?
- Premiered: Winter 2019
- Broadcast: Wednesdays at 01:35 (JST)
- Producers: None found, add some
- Licensors: None found, add some
- Studios: EMT²
- Source: Digital manga
- Genres: Slice of Life, Comedy
- Duration: 2 min.
- Rating: PG-13 - Teens 13 or older

## Links

- [[Refrain] Ame-iro Cocoa - Side G - 01 VOSTFR [720p]](https://uptobox.com/aknpcvldk6u5)
- [[Refrain] Ame-iro Cocoa - Side G - 01 VOSTFR [1080p]](https://uptobox.com/b0n1a8gzprw9)
- [[Refrain] Ame-iro Cocoa - Side G - 02 VOSTFR [720p]](https://uptobox.com/l5r5g6ems0gd)
- [[Refrain] Ame-iro Cocoa - Side G - 02 VOSTFR [1080p]](https://uptobox.com/bty240tfyez1)
- [[Refrain] Ame-iro Cocoa - Side G - 03 VOSTFR [720p]](https://uptobox.com/q4wtlwez51w5)
- [[Refrain] Ame-iro Cocoa - Side G - 03 VOSTFR [1080p]](https://uptobox.com/ej953jc1096t)
- [[Refrain] Ame-iro Cocoa - Side G - 04 VOSTFR [720p]](https://uptobox.com/xqry8wcj4rao)
- [[Refrain] Ame-iro Cocoa - Side G - 04 VOSTFR [1080p]](https://uptobox.com/pp8efulpfu9t)
- [[Refrain] Ame-iro Cocoa - Side G - 05 VOSTFR [720p]](https://uptobox.com/n8bbkc2up1o7)
- [[Refrain] Ame-iro Cocoa - Side G - 05 VOSTFR [1080p]](https://uptobox.com/9az22e8ozxlh)
- [[Refrain] Ame-iro Cocoa - Side G - 06 VOSTFR [720p]](https://uptobox.com/v0komcmcdcbn)
- [[Refrain] Ame-iro Cocoa - Side G - 06 VOSTFR [1080p]](https://uptobox.com/zr259dk9alyj)
- [[Refrain] Ame-iro Cocoa - Side G - 07 VOSTFR [720p]](https://uptobox.com/p85uekph0r3v)
- [[Refrain] Ame-iro Cocoa - Side G - 07 VOSTFR [1080p]](https://uptobox.com/1nz58umwb13g)
- [[Refrain] Ame-iro Cocoa - Side G - 08 VOSTFR [720p]](https://uptobox.com/wog9b08i19ef)
- [[Refrain] Ame-iro Cocoa - Side G - 08 VOSTFR [1080p]](https://uptobox.com/r3fz86uizywj)
- [[Refrain] Ame-iro Cocoa - Side G - 09 VOSTFR [720p]](https://uptobox.com/ty9357donmmj)
- [[Refrain] Ame-iro Cocoa - Side G - 09 VOSTFR [1080p]](https://uptobox.com/vcqyyfngpaui)
- [[Refrain] Ame-iro Cocoa - Side G - 10 VOSTFR [720p]](https://uptobox.com/yxbzagg6yxrc)
- [[Refrain] Ame-iro Cocoa - Side G - 10 VOSTFR [1080p]](https://uptobox.com/chv9qzeyypeh)
- [[Refrain] Ame-iro Cocoa - Side G - 11 VOSTFR [720p]](https://uptobox.com/py4o8u52kicq)
- [[Refrain] Ame-iro Cocoa - Side G - 11 VOSTFR [1080p]](https://uptobox.com/07tc6vt4kkau)
- [[Refrain] Ame-iro Cocoa - Side G - 12 VOSTFR [720p]](https://uptobox.com/tio7hgku0odh)
- [[Refrain] Ame-iro Cocoa - Side G - 12 VOSTFR [1080p]](https://uptobox.com/g2z1b0klt5oc)
