# Kemurikusa (TV)

![Kemurikusa (TV)](https://cdn.myanimelist.net/images/anime/1741/94887l.jpg)

* Japanese:  ケムリクサ

## Information

 - Type: TV
 - Episodes: Unknown
 - Status: Currently Airing
 - Aired: Jan 9, 2019 to ?
 - Premiered: Winter 2019
 - Broadcast: Wednesdays at 22:30 (JST)
 - Producers: flying DOG, Tokyo MX 
 - Licensors: None found, add some 
 - Studios: Yaoyorozu 
 - Source: Original
 - Genres: Fantasy, Sci-Fi
 - Duration: Unknown
 - Rating: None


## Links

- [[Refrain] Kemurikusa (TV) - 01 VOSTFR [720p]](https://uptobox.com/g4u3m0nig4wz)
- [[Refrain] Kemurikusa (TV) - 01 VOSTFR [1080p]](https://uptobox.com/4con2mvrgkht)
- [[Refrain] Kemurikusa (TV) - 02 VOSTFR [1080p]](https://uptobox.com/6z0lq0nb9tb4)
- [[Refrain] Kemurikusa (TV) - 02 VOSTFR [720p]](https://uptobox.com/yu8jylx40ohs)
- [[Refrain] Kemurikusa (TV) - 03 VOSTFR [1080p]](https://uptobox.com/v4ux73s11paw)
- [[Refrain] Kemurikusa (TV) - 03 VOSTFR [720p]](https://uptobox.com/96a4g5g0avlf)
- [[Refrain] Kemurikusa (TV) - 04 VOSTFR [1080p]](https://uptobox.com/0w9xh99plrjq)
- [[Refrain] Kemurikusa (TV) - 04 VOSTFR [720p]](https://uptobox.com/gv5drxg3eyt6)
