# Date A Live Ⅲ

![Date A Live Ⅲ](https://cdn.myanimelist.net/images/anime/1295/97615l.jpg)

* Japanese:  デート・ア・ライブⅢ

## Information

 - Type: TV
 - Episodes: 12
 - Status: Currently Airing
 - Aired: Jan 7, 2019 to ?
 - Premiered: Winter 2019
 - Broadcast: Fridays at 21:30 (JST)
 - Producers: None found, add some 
 - Licensors: None found, add some 
 - Studios: J.C.Staff 
 - Source: Light novel
 - Genres: Sci-Fi, Harem, Comedy, Romance, Mecha, School
 - Duration: 24 min. per ep.
 - Rating: R - 17+ (violence & profanity)


## Links

- [[Refrain] Date A Live III - 01 VOSTFR [1080p]](https://uptobox.com/b19pq59mgqm2)
- [[Refrain] Date A Live III - 01 VOSTFR [720p]](https://uptobox.com/pqm3htix9d4y)
- [[Refrain] Date A Live III - 02 VOSTFR [1080p]](https://uptobox.com/t2n3m8cxss16)
- [[Refrain] Date A Live III - 02 VOSTFR [720p]](https://uptobox.com/c771pk31cj0e)
- [[Refrain] Date A Live III - 03 VOSTFR [1080p]](https://uptobox.com/2vltdndop4za)
- [[Refrain] Date A Live III - 03 VOSTFR [720p]](https://uptobox.com/ugdwoef91rop)
- [[Refrain] Date A Live III - 04 VOSTFR [1080p]](https://uptobox.com/m9ymwnium6lz)
- [[Refrain] Date A Live III - 04 VOSTFR [720p]](https://uptobox.com/6b5fw9asbifr)
- [[Refrain] Date A Live III - 05 VOSTFR [1080p]](https://uptobox.com/whr0a7oz809f)
- [[Refrain] Date A Live III - 05 VOSTFR [720p]](https://uptobox.com/f4c6kj11k3ya)
- [[Refrain] Date A Live III - 06 VOSTFR [1080p]](https://uptobox.com/zm3qp6y2ds1c)
- [[Refrain] Date A Live III - 06 VOSTFR [720p]](https://uptobox.com/vdprtn2hahg2)
- [[Refrain] Date A Live III - 07 VOSTFR [1080p]](https://uptobox.com/mq9lnzqyxhfz)
- [[Refrain] Date A Live III - 07 VOSTFR [720p]](https://uptobox.com/02kiuqn6gc1f)
- [[Refrain] Date A Live III - 08 VOSTFR [1080p]](https://uptobox.com/e34wa13bsy4d)
- [[Refrain] Date A Live III - 08 VOSTFR [720p]](https://uptobox.com/bxc9q8otczii)
- [[Refrain] Date A Live III - 09 VOSTFR [1080p]](https://uptobox.com/6ksbq2kr2mqw)
- [[Refrain] Date A Live III - 09 VOSTFR [720p]](https://uptobox.com/m3qem6cmxosl)
- [[Refrain] Date A Live III - 10 VOSTFR [1080p]](https://uptobox.com/hzf8sr5wttq3)
- [[Refrain] Date A Live III - 10 VOSTFR [720p]](https://uptobox.com/op3jv9tbzgk3)
- [[Refrain] Date A Live III - 11 VOSTFR [1080p]](https://uptobox.com/3qyhn7dzzaym)
- [[Refrain] Date A Live III - 11 VOSTFR [720p]](https://uptobox.com/4cqm1ck49jzc)
- [[Refrain] Date A Live III - 12 VOSTFR [1080p]](https://uptobox.com/iw84qopnjilq)
- [[Refrain] Date A Live III - 12 VOSTFR [720p]](https://uptobox.com/js77s1z4l355)
