# W'z

![W'z](https://cdn.myanimelist.net/images/anime/1838/96177l.jpg)

* Japanese:  ウィズ

## Information

 - Type: TV
 - Episodes: 13
 - Status: Currently Airing
 - Aired: Jan 6, 2019 to ?
 - Premiered: Winter 2019
 - Broadcast: Sundays at 01:30 (JST)
 - Producers: Frontier Works
 - Licensors: Sentai Filmworks
 - Studios: GoHands
 - Source: Original
 - Genres: Action, Music
 - Duration: Unknown
 - Rating: None


## Links

- [[Refrain] W'z - 01 VOSTFR [1080p]](https://uptobox.com/mka4nfqa36r0)
- [[Refrain] W'z - 01 VOSTFR [720p]](https://uptobox.com/t4opfj8nhgrc)
- [[Refrain] W'z - 02 VOSTFR [1080p]](https://uptobox.com/vucx1wsu42p8)
- [[Refrain] W'z - 02 VOSTFR [720p]](https://uptobox.com/v205m0h5gy1g)
- [[Refrain] W'z - 03 VOSTFR [1080p]](https://uptobox.com/3heshtoh9lzt)
- [[Refrain] W'z - 03 VOSTFR [720p]](https://uptobox.com/l3nv349emwaj)
- [[Refrain] W'z - 04 VOSTFR [1080p]](https://uptobox.com/n29jof0qz29t)
- [[Refrain] W'z - 04 VOSTFR [720p]](https://uptobox.com/9nb91x3iuis8)
- [[Refrain] W'z - 05 VOSTFR [1080p]](https://uptobox.com/ahvxgrwjs3dj)
- [[Refrain] W'z - 05 VOSTFR [720p]](https://uptobox.com/fx2tsquv3lwg)
- [[Refrain] W'z - 06 VOSTFR [1080p]](https://uptobox.com/ibapq963gufb)
- [[Refrain] W'z - 06 VOSTFR [720p]](https://uptobox.com/qwkch0zztyz1)
- [[Refrain] W'z - 07 VOSTFR [1080p]](https://uptobox.com/mx421ykgyn3r)
- [[Refrain] W'z - 07 VOSTFR [720p]](https://uptobox.com/l5ans7edqqkw)
- [[Refrain] W'z - 08 VOSTFR [1080p]](https://uptobox.com/o0y4pbditmtb)
- [[Refrain] W'z - 08 VOSTFR [720p]](https://uptobox.com/zblsqb8gsv1m)
- [[Refrain] W'z - 09 VOSTFR [1080p]](https://uptobox.com/wubr7jltn7ov)
- [[Refrain] W'z - 09 VOSTFR [720p]](https://uptobox.com/qlmqqgl8c3s5)
- [[Refrain] W'z - 10 VOSTFR [1080p]](https://uptobox.com/of5cl1al5y7e)
- [[Refrain] W'z - 10 VOSTFR [720p]](https://uptobox.com/g23vbv8agmyo)
- [[Refrain] W'z - 11 VOSTFR [1080p]](https://uptobox.com/jdkdfkzyhoyz)
- [[Refrain] W'z - 11 VOSTFR [720p]](https://uptobox.com/8u7g4tp3ji47)
- [[Refrain] W'z - 12 VOSTFR [1080p]](https://uptobox.com/j0gbkn4ae6rw)
- [[Refrain] W'z - 12 VOSTFR [720p]](https://uptobox.com/onetwvt2x8v3)
- [[Refrain] W'z - 13 VOSTFR [1080p]](https://uptobox.com/8bl79kdpbg9i)
- [[Refrain] W'z - 13 VOSTFR [720p]](https://uptobox.com/n4hhl731prn6)
