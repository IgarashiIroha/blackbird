# Lord El-Melloi II Sei no Jikenbo: Rail Zeppelin Grace Note


![Lord El-Melloi II Sei no Jikenbo: Rail Zeppelin Grace Note](https://cdn.myanimelist.net/images/anime/1403/98092l.jpg)

* Japanese: ロード・エルメロイⅡ世の事件簿 -魔眼蒐集列車 Grace note-

## Information

- Type: TV
- Episodes: Unknown
- Status: Not yet aired
- Aired: Jul, 2019 to ?
- Premiered: Summer 2019
- Broadcast: Unknown
- Producers: None found, add some
- Licensors: None found, add some
- Studios: TROYCA
- Source: Light novel
- Genres: Fantasy, Mystery, Supernatural
- Duration: Unknown
- Rating: None


## Links

- [[Refrain] Lord El-Melloi II-sei - Rail Zeppelin Grace Note - 00 VOSTFR [720p]](https://uptobox.com/tla60k03oyet)
- [[Refrain] Lord El-Melloi II-sei - Rail Zeppelin Grace Note - 00 VOSTFR [1080p]](https://uptobox.com/1ex0dsom44dz)
