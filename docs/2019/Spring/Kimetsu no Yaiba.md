# Kimetsu no Yaiba

![Kimetsu no Yaiba](https://cdn.myanimelist.net/images/anime/1286/99889l.jpg)

* Japanese:  鬼滅の刃

## Information

 - Type: TV
 - Episodes: Unknown
 - Status: Currently Airing
 - Aired: Mar 29, 2019 to ?
 - Premiered: Spring 2019
 - Broadcast: Saturdays at 23:30 (JST)
 - Producers: Aniplex, Shueisha 
 - Licensors: None found, add some 
 - Studios: ufotable 
 - Source: Manga
 - Genres: Action, Demons, Historical, Shounen, Supernatural
 - Duration: Unknown
 - Rating: None


## Links

