# Strike Witches: 501 Butai Hasshin Shimasu!

![Strike Witches: 501 Butai Hasshin Shimasu!](https://cdn.myanimelist.net/images/anime/1791/99459l.jpg)

* Japanese:  ストライクウィッチーズ 501部隊発進しますっ！

## Information

 - Type: TV
 - Episodes: Unknown
 - Status: Not yet aired
 - Aired: Apr 10, 2019 to ?
 - Premiered: Spring 2019
 - Broadcast: Wednesdays at 00:45 (JST)
 - Producers: None found, add some 
 - Licensors: None found, add some 
 - Studios: None found, add some 
 - Source: Manga
 - Genres: Comedy, Military, Sci-Fi, Shounen
 - Duration: Unknown
 - Rating: PG-13 - Teens 13 or older


## Links

