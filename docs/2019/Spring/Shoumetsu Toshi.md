# Shoumetsu Toshi

![Shoumetsu Toshi](https://cdn.myanimelist.net/images/anime/1022/98071l.jpg)

* Japanese:  消滅都市

## Information

 - Type: TV
 - Episodes: 12
 - Status: Not yet aired
 - Aired: Apr 7, 2019 to ?
 - Premiered: Spring 2019
 - Broadcast: Sundays at 23:30 (JST)
 - Producers: Pony Canyon 
 - Licensors: None found, add some 
 - Studios: Madhouse 
 - Source: Game
 - Genres: Action, Adventure, Drama, Fantasy, Mystery
 - Duration: Unknown
 - Rating: None


## Links

