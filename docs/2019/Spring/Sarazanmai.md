# Sarazanmai

![Sarazanmai](https://cdn.myanimelist.net/images/anime/1970/99302l.jpg)

* Japanese:  さらざんまい

## Information

 - Type: TV
 - Episodes: 11
 - Status: Not yet aired
 - Aired: Apr 12, 2019 to ?
 - Premiered: Spring 2019
 - Broadcast: Fridays at 00:55 (JST)
 - Producers: None found, add some 
 - Licensors: None found, add some 
 - Studios: MAPPA, Lapin Track 
 - Source: Original
 - Genres: Action, Fantasy, Supernatural
 - Duration: Unknown
 - Rating: PG-13 - Teens 13 or older


## Links

