# King of Prism: Shiny Seven Stars

![King of Prism: Shiny Seven Stars](https://cdn.myanimelist.net/images/anime/1649/100502l.jpg)

* Japanese:  KING OF PRISM -Shiny Seven Stars-

## Information

 - Type: TV
 - Episodes: 12
 - Status: Currently Airing
 - Aired: Apr 16, 2019 to ?
 - Premiered: ?
 - Broadcast: Tuesdays at 01:35 (JST)
 - Producers: None found, add some 
 - Licensors: None found, add some 
 - Studios: None found, add some 
 - Source: Original
 - Genres: Music, Shounen, Sports
 - Duration: 20 min. per ep.
 - Rating: None


## Links

- [[Refrain] King of Prism - Shiny Seven Stars - 01 VOSTFR [720p]](https://uptobox.com/grd9lr4ytptt)
- [[Refrain] King of Prism - Shiny Seven Stars - 01 VOSTFR [1080p]](https://uptobox.com/hyf23k1epx10)
- [[Refrain] King of Prism - Shiny Seven Stars - 02 VOSTFR [720p]](https://uptobox.com/bqsmeuotjstp)
- [[Refrain] King of Prism - Shiny Seven Stars - 02 VOSTFR [1080p]](https://uptobox.com/mgbvwn78zm6v)
- [[Refrain] King of Prism - Shiny Seven Stars - 03 VOSTFR [720p]](https://uptobox.com/zl8wekrncsws)
- [[Refrain] King of Prism - Shiny Seven Stars - 03 VOSTFR [1080p]](https://uptobox.com/dk9lf1bld64q)
- [[Refrain] King of Prism - Shiny Seven Stars - 04 VOSTFR [720p]](https://uptobox.com/9506g43a5z1c)
- [[Refrain] King of Prism - Shiny Seven Stars - 04 VOSTFR [1080p]](https://uptobox.com/3857ljc4m77f)
- [[Refrain] King of Prism - Shiny Seven Stars - 05 VOSTFR [720p]](https://uptobox.com/swyyqoehd530)
- [[Refrain] King of Prism - Shiny Seven Stars - 05 VOSTFR [1080p]](https://uptobox.com/qg0rf6ktc3zc)
- [[Refrain] King of Prism - Shiny Seven Stars - 06 VOSTFR [720p]](https://uptobox.com/4vequsovyunf)
- [[Refrain] King of Prism - Shiny Seven Stars - 06 VOSTFR [1080p]](https://uptobox.com/bu5doea5bmdc)
- [[Refrain] King of Prism - Shiny Seven Stars - 07 VOSTFR [720p]](https://uptobox.com/w13uusb3hmeh)
- [[Refrain] King of Prism - Shiny Seven Stars - 07 VOSTFR [1080p]](https://uptobox.com/m4ymiii8zu4p)
- [[Refrain] King of Prism - Shiny Seven Stars - 08 VOSTFR [720p]](https://uptobox.com/2si8j6at0710)
- [[Refrain] King of Prism - Shiny Seven Stars - 08 VOSTFR [1080p]](https://uptobox.com/ftonhiri80fc)
