# Bungou Stray Dogs 3rd Season

![Bungou Stray Dogs 3rd Season](https://cdn.myanimelist.net/images/anime/1829/98115l.jpg)

* Japanese:  文豪ストレイドッグス 第3期

## Information

 - Type: TV
 - Episodes: Unknown
 - Status: Currently Airing
 - Aired: Apr 12, 2019 to ?
 - Premiered: Spring 2019
 - Broadcast: Fridays at 22:30 (JST)
 - Producers: None found, add some 
 - Licensors: None found, add some 
 - Studios: Bones 
 - Source: Manga
 - Genres: Action, Mystery, Super Power, Supernatural, Seinen
 - Duration: 23 min.
 - Rating: R - 17+ (violence & profanity)


## Links

