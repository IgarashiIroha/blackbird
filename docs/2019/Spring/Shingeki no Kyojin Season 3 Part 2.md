# Shingeki no Kyojin Season 3 Part 2

![Shingeki no Kyojin Season 3 Part 2](https://cdn.myanimelist.net/images/anime/1731/100088l.jpg)

* Japanese:  進撃の巨人 Season3 Part.2

## Information

 - Type: TV
 - Episodes: 10
 - Status: Not yet aired
 - Aired: Apr 29, 2019 to ?
 - Premiered: Spring 2019
 - Broadcast: Mondays at 00:10 (JST)
 - Producers: Production I.G, Dentsu, Mainichi Broadcasting System, Pony Canyon, Kodansha, Techno Sound, Pony Canyon Enterprise 
 - Licensors: Funimation 
 - Studios: Wit Studio 
 - Source: Manga
 - Genres: Action, Military, Mystery, Super Power, Drama, Fantasy, Shounen
 - Duration: Unknown
 - Rating: R - 17+ (violence & profanity)


## Links

