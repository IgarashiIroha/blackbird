# Araiya-san!: Ore to Aitsu ga Onnayu de!?

![Araiya-san!: Ore to Aitsu ga Onnayu de!?](https://cdn.myanimelist.net/images/anime/1756/99295l.jpg)

* Japanese:  洗い屋さん! ~俺とアイツが女湯で!?~

## Information

 - Type: TV
 - Episodes: Unknown
 - Status: Not yet aired
 - Aired: Apr 8, 2019 to ?
 - Premiered: Spring 2019
 - Broadcast: Mondays at 01:00 (JST)
 - Producers: None found, add some 
 - Licensors: None found, add some 
 - Studios: Magic Bus 
 - Source: Manga
 - Genres: Comedy, Ecchi, Romance
 - Duration: 6 min.
 - Rating: R+ - Mild Nudity


## Links

