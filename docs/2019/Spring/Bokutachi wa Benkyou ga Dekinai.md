# Bokutachi wa Benkyou ga Dekinai

![Bokutachi wa Benkyou ga Dekinai](https://cdn.myanimelist.net/images/anime/1256/97725l.jpg)

* Japanese:  ぼくたちは勉強ができない

## Information

 - Type: TV
 - Episodes: Unknown
 - Status: Not yet aired
 - Aired: Apr 7, 2019 to ?
 - Premiered: Spring 2019
 - Broadcast: Sundays at 00:30 (JST)
 - Producers: Aniplex, Magic Capsule, Shueisha 
 - Licensors: None found, add some 
 - Studios: Silver, Arvo Animation 
 - Source: Manga
 - Genres: Comedy, Harem, Romance, School, Shounen
 - Duration: Unknown
 - Rating: None


## Links

