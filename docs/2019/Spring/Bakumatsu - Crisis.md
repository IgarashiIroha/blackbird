# Bakumatsu: Crisis

![Bakumatsu: Crisis](https://cdn.myanimelist.net/images/anime/1585/99180l.jpg)

* Japanese:  BAKUMATSU ~恋愛幕末カレシ 外伝~ クライシス

## Information

 - Type: TV
 - Episodes: Unknown
 - Status: Not yet aired
 - Aired: Apr 5, 2019 to ?
 - Premiered: Spring 2019
 - Broadcast: Fridays at 01:58 (JST)
 - Producers: None found, add some 
 - Licensors: None found, add some 
 - Studios: Studio Deen 
 - Source: Game
 - Genres: Action, Historical, Samurai
 - Duration: Unknown
 - Rating: None


## Links

