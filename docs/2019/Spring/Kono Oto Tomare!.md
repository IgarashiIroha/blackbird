# Kono Oto Tomare!

![Kono Oto Tomare!](https://cdn.myanimelist.net/images/anime/1464/99881l.jpg)

* Japanese:  この音とまれ！

## Information

 - Type: TV
 - Episodes: Unknown
 - Status: Not yet aired
 - Aired: Apr 7, 2019 to ?
 - Premiered: Spring 2019
 - Broadcast: Sundays at 01:00 (JST)
 - Producers: Shueisha 
 - Licensors: None found, add some 
 - Studios: Platinum Vision 
 - Source: Manga
 - Genres: Music, Drama, School, Shounen
 - Duration: Unknown
 - Rating: None


## Links

- [[Refrain] Kono Oto Tomare! - 01 VOSTFR {ADN} [720p]](https://uptobox.com/hdn3lz7lmrpw)
- [[Refrain] Kono Oto Tomare! - 01 VOSTFR {ADN} [1080p]](https://uptobox.com/klqka2y18jyw)
- [[Refrain] Kono Oto Tomare! - 02 VOSTFR {ADN} [720p]](https://uptobox.com/r00srklp5h7l)
- [[Refrain] Kono Oto Tomare! - 02 VOSTFR {ADN} [1080p]](https://uptobox.com/1l26bjqjtu3v)
- [[Refrain] Kono Oto Tomare! - 03 VOSTFR {ADN} [720p]](https://uptobox.com/eak9uutmrn8o)
- [[Refrain] Kono Oto Tomare! - 03 VOSTFR {ADN} [1080p]](https://uptobox.com/y00zk8cmvoj5)
- [[Refrain] Kono Oto Tomare! - 04 VOSTFR {ADN} [720p]](https://uptobox.com/vp15wf6o9yoq)
- [[Refrain] Kono Oto Tomare! - 04 VOSTFR {ADN} [1080p]](https://uptobox.com/5cl6tfcke71k)
- [[Refrain] Kono Oto Tomare! - 05 VOSTFR {ADN} [720p]](https://uptobox.com/yy7lt4d0h81e)
- [[Refrain] Kono Oto Tomare! - 05 VOSTFR {ADN} [1080p]](https://uptobox.com/mgvoitse8vm9)
- [[Refrain] Kono Oto Tomare! - 06 VOSTFR {ADN} [720p]](https://uptobox.com/2u97ovl5xz14)
- [[Refrain] Kono Oto Tomare! - 06 VOSTFR {ADN} [1080p]](https://uptobox.com/2m66efo7uobt)
- [[Refrain] Kono Oto Tomare! - 07 VOSTFR {ADN} [720p]](https://uptobox.com/c1m2h03ke7qg)
- [[Refrain] Kono Oto Tomare! - 07 VOSTFR {ADN} [1080p]](https://uptobox.com/jrkrxoa9mvqm)
- [[Refrain] Kono Oto Tomare! - 08 VOSTFR {ADN} [720p]](https://uptobox.com/gjouf2aga4zq)
- [[Refrain] Kono Oto Tomare! - 08 VOSTFR {ADN} [1080p]](https://uptobox.com/g0n6mleqmtca)
- [[Refrain] Kono Oto Tomare! - 09 VOSTFR {ADN} [720p]](https://uptobox.com/p5u6vp7ee1os)
- [[Refrain] Kono Oto Tomare! - 09 VOSTFR {ADN} [1080p]](https://uptobox.com/c0ardh5cb85m)
- [[Refrain] Kono Oto Tomare! - 10 VOSTFR {ADN} [720p]](https://uptobox.com/21ymcvgwyy3u)
- [[Refrain] Kono Oto Tomare! - 10 VOSTFR {ADN} [1080p]](https://uptobox.com/lmv1fqals4of)
