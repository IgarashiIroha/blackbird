# Chou Kadou Girl ⅙: Amazing Stranger

![Chou Kadou Girl ⅙: Amazing Stranger](https://cdn.myanimelist.net/images/anime/1492/99084l.jpg)

* Japanese:  超可動ガール1/6 AMAZING STRANGER

## Information

 - Type: TV
 - Episodes: Unknown
 - Status: Not yet aired
 - Aired: Apr 6, 2019 to ?
 - Premiered: Spring 2019
 - Broadcast: Saturdays at 23:00 (JST)
 - Producers: None found, add some 
 - Licensors: None found, add some 
 - Studios: None found, add some 
 - Source: Manga
 - Genres: Comedy, Seinen
 - Duration: Unknown
 - Rating: None


## Links

- [[Refrain] Chou Kadou Girl - Amazing Stranger - 01 VOSTFR [720p]](https://uptobox.com/yvp0sco9csit)
- [[Refrain] Chou Kadou Girl - Amazing Stranger - 01 VOSTFR [1080p]](https://uptobox.com/x45xt2rjd3ps)
- [[Refrain] Chou Kadou Girl - Amazing Stranger - 02 VOSTFR [720p]](https://uptobox.com/i64jcb1vg4ux)
- [[Refrain] Chou Kadou Girl - Amazing Stranger - 02 VOSTFR [1080p]](https://uptobox.com/39mlo190e3mw)
- [[Refrain] Chou Kadou Girl - Amazing Stranger - 03 VOSTFR [720p]](https://uptobox.com/984kp5kfjfm9)
- [[Refrain] Chou Kadou Girl - Amazing Stranger - 03 VOSTFR [1080p]](https://uptobox.com/zxrf0bgep78z)
- [[Refrain] Chou Kadou Girl - Amazing Stranger - 04 VOSTFR [720p]](https://uptobox.com/g7cojcsfpj9n)
- [[Refrain] Chou Kadou Girl - Amazing Stranger - 04 VOSTFR [1080p]](https://uptobox.com/tiropz8tq6f0)
- [[Refrain] Chou Kadou Girl - Amazing Stranger - 05 VOSTFR [720p]](https://uptobox.com/oj1odh54j2o0)
- [[Refrain] Chou Kadou Girl - Amazing Stranger - 05 VOSTFR [1080p]](https://uptobox.com/yn2ljs3x54lm)
- [[Refrain] Chou Kadou Girl - Amazing Stranger - 07 VOSTFR [720p]](https://uptobox.com/cwdfd5v1riz2)
- [[Refrain] Chou Kadou Girl - Amazing Stranger - 07 VOSTFR [1080p]](https://uptobox.com/wm6npqac52wy)
- [[Refrain] Chou Kadou Girl - Amazing Stranger - 08 VOSTFR [720p]](https://uptobox.com/hgus27dcc14k)
- [[Refrain] Chou Kadou Girl - Amazing Stranger - 08 VOSTFR [1080p]](https://uptobox.com/eiymh74z8hhe)
- [[Refrain] Chou Kadou Girl - Amazing Stranger - 09 VOSTFR [720p]](https://uptobox.com/2j6uz7uomo1w)
- [[Refrain] Chou Kadou Girl - Amazing Stranger - 09 VOSTFR [1080p]](https://uptobox.com/bfirqmmziwcj)
- [[Refrain] Chou Kadou Girl - Amazing Stranger - 10 VOSTFR [720p]](https://uptobox.com/jola5dmw69ra)
- [[Refrain] Chou Kadou Girl - Amazing Stranger - 10 VOSTFR [1080p]](https://uptobox.com/un854i7w4gng)
