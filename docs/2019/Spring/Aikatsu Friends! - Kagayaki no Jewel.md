# Aikatsu Friends!: Kagayaki no Jewel

![Aikatsu Friends!: Kagayaki no Jewel](https://cdn.myanimelist.net/images/anime/1026/98476l.jpg)

* Japanese:  アイカツフレンズ！~かがやきのジュエル~

## Information

 - Type: TV
 - Episodes: Unknown
 - Status: Not yet aired
 - Aired: Apr 4, 2019 to ?
 - Premiered: Spring 2019
 - Broadcast: Thursdays at 18:25 (JST)
 - Producers: None found, add some 
 - Licensors: None found, add some 
 - Studios: None found, add some 
 - Source: Game
 - Genres: Music, Slice of Life, School, Shoujo
 - Duration: 24 min.
 - Rating: PG - Children


## Links

