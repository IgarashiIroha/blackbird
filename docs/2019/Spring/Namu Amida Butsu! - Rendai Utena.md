# Namu Amida Butsu!: Rendai Utena

![Namu Amida Butsu!: Rendai Utena](https://cdn.myanimelist.net/images/anime/1598/99004l.jpg)

* Japanese:  なむあみだ仏っ! -蓮台 UTENA-

## Information

 - Type: TV
 - Episodes: 12
 - Status: Not yet aired
 - Aired: Apr 8, 2019 to ?
 - Premiered: Spring 2019
 - Broadcast: Mondays at 22:30 (JST)
 - Producers: DMM pictures 
 - Licensors: None found, add some 
 - Studios: Asahi Production 
 - Source: Game
 - Genres: Fantasy
 - Duration: Unknown
 - Rating: None


## Links

