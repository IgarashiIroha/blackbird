# RobiHachi

![RobiHachi](https://cdn.myanimelist.net/images/anime/1315/99433l.jpg)

* Japanese:  ロビハチ

## Information

 - Type: TV
 - Episodes: 12
 - Status: Not yet aired
 - Aired: Apr 8, 2019 to ?
 - Premiered: Spring 2019
 - Broadcast: Mondays at 23:30 (JST)
 - Producers: None found, add some 
 - Licensors: None found, add some 
 - Studios: Studio Comet 
 - Source: Original
 - Genres: Sci-Fi, Space
 - Duration: Unknown
 - Rating: None


## Links

