# Kono Yo no Hate de Koi wo Utau Shoujo YU-NO

![Kono Yo no Hate de Koi wo Utau Shoujo YU-NO](https://cdn.myanimelist.net/images/anime/1976/99003l.jpg)

* Japanese:  この世の果てで恋を唄う少女YU-NO

## Information

 - Type: TV
 - Episodes: 26
 - Status: Currently Airing
 - Aired: Apr 2, 2019 to ?
 - Premiered: Spring 2019
 - Broadcast: Tuesdays at 23:00 (JST)
 - Producers: Genco 
 - Licensors: None found, add some 
 - Studios: feel. 
 - Source: Visual novel
 - Genres: Drama, Sci-Fi
 - Duration: Unknown
 - Rating: PG-13 - Teens 13 or older


## Links

