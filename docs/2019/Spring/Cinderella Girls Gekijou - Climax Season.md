# Cinderella Girls Gekijou: Climax Season

![Cinderella Girls Gekijou: Climax Season](https://cdn.myanimelist.net/images/anime/1315/99901l.jpg)

* Japanese:  シンデレラガールズ劇場 CLIMAX SEASON

## Information

 - Type: TV
 - Episodes: Unknown
 - Status: Currently Airing
 - Aired: Apr 2, 2019 to ?
 - Premiered: Spring 2019
 - Broadcast: Tuesdays at 21:54 (JST)
 - Producers: None found, add some 
 - Licensors: None found, add some 
 - Studios: Gathering, Lesprit 
 - Source: 4-koma manga
 - Genres: Slice of Life, Comedy
 - Duration: 3 min.
 - Rating: PG-13 - Teens 13 or older


## Links

