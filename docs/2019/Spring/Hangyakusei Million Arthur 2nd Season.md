# Hangyakusei Million Arthur 2nd Season

![Hangyakusei Million Arthur 2nd Season](https://cdn.myanimelist.net/images/anime/1251/99666l.jpg)

* Japanese:  叛逆性ミリオンアーサー 第2シーズン

## Information

 - Type: TV
 - Episodes: 11
 - Status: Not yet aired
 - Aired: Apr 4, 2019 to ?
 - Premiered: Spring 2019
 - Broadcast: Thursdays at 22:00 (JST)
 - Producers: Square Enix, Sotsu, Genco, Lantis, Magic Capsule, Fields, bilibili, Bandai Namco Arts, Happinet 
 - Licensors: None found, add some 
 - Studios: J.C.Staff 
 - Source: Game
 - Genres: Action, Adventure, Fantasy, Magic
 - Duration: Unknown
 - Rating: PG-13 - Teens 13 or older


## Links

