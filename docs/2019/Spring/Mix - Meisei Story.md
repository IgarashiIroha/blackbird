# Mix: Meisei Story

![Mix: Meisei Story](https://cdn.myanimelist.net/images/anime/1722/99022l.jpg)

* Japanese:  MIX（ミックス）MEISEI STORY

## Information

 - Type: TV
 - Episodes: Unknown
 - Status: Not yet aired
 - Aired: Apr 6, 2019 to ?
 - Premiered: Spring 2019
 - Broadcast: Saturdays at 17:30 (JST)
 - Producers: YTV, Shueisha 
 - Licensors: None found, add some 
 - Studios: OLM 
 - Source: Manga
 - Genres: Sports, Drama, Romance, School, Shounen
 - Duration: Unknown
 - Rating: None


## Links

- [[Refrain] Mix - Meisei Story - 01 VOSTFR {ADN} [720p]](https://uptobox.com/60s907sx7pph)
- [[Refrain] Mix - Meisei Story - 01 VOSTFR {ADN} [1080p]](https://uptobox.com/8avwu9bxf0o7)
- [[Refrain] Mix - Meisei Story - 02 VOSTFR {ADN} [720p]](https://uptobox.com/spf6lzc4fdr2)
- [[Refrain] Mix - Meisei Story - 02 VOSTFR {ADN} [1080p]](https://uptobox.com/1yfqfwsdi177)
- [[Refrain] Mix - Meisei Story - 03 VOSTFR {ADN} [720p]](https://uptobox.com/ic54ix4d52oi)
- [[Refrain] Mix - Meisei Story - 03 VOSTFR {ADN} [1080p]](https://uptobox.com/mxdfjuosy5nh)
- [[Refrain] Mix - Meisei Story - 04 VOSTFR {ADN} [720p]](https://uptobox.com/csmfcrj5sblu)
- [[Refrain] Mix - Meisei Story - 04 VOSTFR {ADN} [1080p]](https://uptobox.com/n69yrzfj6gh1)
- [[Refrain] Mix - Meisei Story - 05 VOSTFR {ADN} [720p]](https://uptobox.com/g3hkciqfiakc)
- [[Refrain] Mix - Meisei Story - 05 VOSTFR {ADN} [1080p]](https://uptobox.com/mh6hlh16b04z)
- [[Refrain] Mix - Meisei Story - 06 VOSTFR {ADN} [720p]](https://uptobox.com/wqfnsi0ll11o)
- [[Refrain] Mix - Meisei Story - 06 VOSTFR {ADN} [1080p]](https://uptobox.com/0zhr3iz61uqs)
- [[Refrain] Mix - Meisei Story - 07 VOSTFR {ADN} [720p]](https://uptobox.com/5kbr74txursp)
- [[Refrain] Mix - Meisei Story - 07 VOSTFR {ADN} [1080p]](https://uptobox.com/5amr6zgsid4n)
- [[Refrain] Mix - Meisei Story - 08 VOSTFR {ADN} [720p]](https://uptobox.com/tmrz3nh5jp4s)
- [[Refrain] Mix - Meisei Story - 08 VOSTFR {ADN} [1080p]](https://uptobox.com/ahpmgq56e90b)
- [[Refrain] Mix - Meisei Story - 09 VOSTFR {ADN} [720p]](https://uptobox.com/sfktmi5yxb2c)
- [[Refrain] Mix - Meisei Story - 09 VOSTFR {ADN} [1080p]](https://uptobox.com/el6clat4o1j6)
- [[Refrain] Mix - Meisei Story - 10 VOSTFR {ADN} [720p]](https://uptobox.com/trzaguk74wtk)
- [[Refrain] Mix - Meisei Story - 10 VOSTFR {ADN} [1080p]](https://uptobox.com/4zqylhet54hs)
