# Sewayaki Kitsune no Senko-san

![Sewayaki Kitsune no Senko-san](https://cdn.myanimelist.net/images/anime/1814/99677l.jpg)

* Japanese:  世話やきキツネの仙狐さん

## Information

 - Type: TV
 - Episodes: 12
 - Status: Not yet aired
 - Aired: Apr 10, 2019 to Jul, 2019
 - Premiered: Spring 2019
 - Broadcast: Wednesdays at 22:30 (JST)
 - Producers: None found, add some 
 - Licensors: None found, add some 
 - Studios: Doga Kobo 
 - Source: Web manga
 - Genres: Comedy, Supernatural, Romance
 - Duration: Unknown
 - Rating: None


## Links

