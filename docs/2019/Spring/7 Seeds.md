# 7 Seeds

![7 Seeds](https://cdn.myanimelist.net/images/anime/1305/96703l.jpg)

* Japanese:  セブンシーズ

## Information

 - Type: ONA
 - Episodes: Unknown
 - Status: Not yet aired
 - Aired: Jun, 2019 to ?
 - Producers: None found, add some 
 - Licensors: None found, add some 
 - Studios: Gonzo 
 - Source: Manga
 - Genres: Action, Sci-Fi, Adventure, Mystery, Horror, Psychological, Drama, Romance, Shoujo
 - Duration: Unknown
 - Rating: None


## Links

