# Isekai Quartet

![Isekai Quartet](https://cdn.myanimelist.net/images/anime/1965/99667l.jpg)

* Japanese:  異世界かるてっと

## Information

 - Type: TV
 - Episodes: Unknown
 - Status: Not yet aired
 - Aired: Apr 10, 2019 to ?
 - Premiered: Spring 2019
 - Broadcast: Wednesdays at 00:30 (JST)
 - Producers: Kadokawa 
 - Licensors: None found, add some 
 - Studios: Studio PuYUKAI 
 - Source: Original
 - Genres: Comedy, Parody, Fantasy
 - Duration: Unknown
 - Rating: None


## Links

