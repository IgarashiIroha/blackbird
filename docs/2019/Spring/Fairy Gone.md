# Fairy Gone

![Fairy Gone](https://cdn.myanimelist.net/images/anime/1805/99784l.jpg)

* Japanese:  Fairy gone フェアリーゴーン

## Information

 - Type: TV
 - Episodes: Unknown
 - Status: Not yet aired
 - Aired: Apr 6, 2019 to ?
 - Premiered: Spring 2019
 - Broadcast: Mondays at 00:00 (JST)
 - Producers: None found, add some 
 - Licensors: None found, add some 
 - Studios: P.A. Works 
 - Source: Original
 - Genres: Action, Demons, Fantasy, Magic, Supernatural
 - Duration: Unknown
 - Rating: None


## Links

