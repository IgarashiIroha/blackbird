# Yatogame-chan Kansatsu Nikki

![Yatogame-chan Kansatsu Nikki](https://cdn.myanimelist.net/images/anime/1142/99002l.jpg)

* Japanese:  八十亀ちゃんかんさつにっき

## Information

 - Type: TV
 - Episodes: Unknown
 - Status: Not yet aired
 - Aired: Apr 4, 2019 to ?
 - Premiered: Spring 2019
 - Broadcast: Thursdays at 19:53 (JST)
 - Producers: None found, add some 
 - Licensors: None found, add some 
 - Studios: Creators in Pack, Saetta 
 - Source: 4-koma manga
 - Genres: Slice of Life, Comedy, School, Shounen
 - Duration: Unknown
 - Rating: None


## Links

- [[Refrain] Yatogame-chan Kansatsu Nikki - 01 VOSTFR [720p]](https://uptobox.com/9wyda9lz122n)
- [[Refrain] Yatogame-chan Kansatsu Nikki - 01 VOSTFR [1080p]](https://uptobox.com/7mbzdsa1j59k)
- [[Refrain] Yatogame-chan Kansatsu Nikki - 02 VOSTFR [720p]](https://uptobox.com/5yg7ch5wf5kl)
- [[Refrain] Yatogame-chan Kansatsu Nikki - 02 VOSTFR [720p]](https://uptobox.com/ia6ew5ppcrg3)
- [[Refrain] Yatogame-chan Kansatsu Nikki - 02 VOSTFR [1080p]](https://uptobox.com/sgb2yu52a1r8)
- [[Refrain] Yatogame-chan Kansatsu Nikki - 03 VOSTFR [720p]](https://uptobox.com/0fofx9wmvhn7)
- [[Refrain] Yatogame-chan Kansatsu Nikki - 03 VOSTFR [1080p]](https://uptobox.com/s8e80od94v6b)
- [[Refrain] Yatogame-chan Kansatsu Nikki - 04 VOSTFR [720p]](https://uptobox.com/q2az70ks88c2)
- [[Refrain] Yatogame-chan Kansatsu Nikki - 04 VOSTFR [1080p]](https://uptobox.com/w9bn4ynevx03)
- [[Refrain] Yatogame-chan Kansatsu Nikki - 05 VOSTFR [720p]](https://uptobox.com/0454iu7ifdg0)
- [[Refrain] Yatogame-chan Kansatsu Nikki - 05 VOSTFR [1080p]](https://uptobox.com/8ow7f8dwuvpk)
- [[Refrain] Yatogame-chan Kansatsu Nikki - 06 VOSTFR [720p]](https://uptobox.com/f7yls49lim7c)
- [[Refrain] Yatogame-chan Kansatsu Nikki - 06 VOSTFR [1080p]](https://uptobox.com/8wgs9d2o1vwj)
- [[Refrain] Yatogame-chan Kansatsu Nikki - 07 VOSTFR [720p]](https://uptobox.com/uxjjwagf9b0a)
- [[Refrain] Yatogame-chan Kansatsu Nikki - 07 VOSTFR [1080p]](https://uptobox.com/rr7a42nhuj50)
- [[Refrain] Yatogame-chan Kansatsu Nikki - 08 VOSTFR [720p]](https://uptobox.com/6v0e5nfko9tc)
- [[Refrain] Yatogame-chan Kansatsu Nikki - 08 VOSTFR [1080p]](https://uptobox.com/bvc4tqb3p5y4)
- [[Refrain] Yatogame-chan Kansatsu Nikki - 09 VOSTFR [720p]](https://uptobox.com/fqtpi2adxgbb)
- [[Refrain] Yatogame-chan Kansatsu Nikki - 09 VOSTFR [1080p]](https://uptobox.com/vutfq6mhbdhd)
