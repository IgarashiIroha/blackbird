# Gunjou no Magmel

![Gunjou no Magmel](https://cdn.myanimelist.net/images/anime/1063/98597l.jpg)

* Japanese:  群青のマグメル

## Information

 - Type: TV
 - Episodes: Unknown
 - Status: Not yet aired
 - Aired: Apr 7, 2019 to ?
 - Premiered: Spring 2019
 - Broadcast: Sundays at 22:00 (JST)
 - Producers: Shueisha 
 - Licensors: None found, add some 
 - Studios: Studio Pierrot 
 - Source: Manga
 - Genres: Action, Adventure, Super Power, Fantasy, Shounen
 - Duration: Unknown
 - Rating: None


## Links

