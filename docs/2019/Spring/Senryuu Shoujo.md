# Senryuu Shoujo

![Senryuu Shoujo](https://cdn.myanimelist.net/images/anime/1751/98216l.jpg)

* Japanese:  川柳少女

## Information

 - Type: TV
 - Episodes: Unknown
 - Status: Not yet aired
 - Aired: Apr 6, 2019 to ?
 - Premiered: Spring 2019
 - Broadcast: Saturdays at 01:55 (JST)
 - Producers: None found, add some 
 - Licensors: None found, add some 
 - Studios: Connect 
 - Source: 4-koma manga
 - Genres: Slice of Life, Comedy, School, Shounen
 - Duration: Unknown
 - Rating: None


## Links

