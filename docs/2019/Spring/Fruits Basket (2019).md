# Fruits Basket (2019)

![Fruits Basket (2019)](https://cdn.myanimelist.net/images/anime/1447/99827l.jpg)

* Japanese:  フルーツバスケット

## Information

 - Type: TV
 - Episodes: Unknown
 - Status: Currently Airing
 - Aired: Mar 26, 2019 to ?
 - Premiered: Spring 2019
 - Broadcast: Saturdays at 01:23 (JST)
 - Producers: None found, add some 
 - Licensors: Funimation 
 - Studios: TMS Entertainment 
 - Source: Manga
 - Genres: Comedy, Drama, Fantasy, Romance, Shoujo, Slice of Life
 - Duration: Unknown
 - Rating: PG-13 - Teens 13 or older


## Links

- [[Refrain] Fruits Basket (2019) - 00 VOSTFR {ADN} [720p]](https://uptobox.com/lk636bnrr8vw)
- [[Refrain] Fruits Basket (2019) - 01 VOSTFR {ADN} [720p]](https://uptobox.com/2bnh5hgnwu5f)
- [[Refrain] Fruits Basket (2019) - 01 VOSTFR {ADN} [1080p]](https://uptobox.com/us9knbyicw0w)
- [[Refrain] Fruits Basket (2019) - 01 VOSTFR {ADN} [720p]](https://uptobox.com/bg2s9hz09b5r)
- [[Refrain] Fruits Basket (2019) - 01 VOSTFR {ADN} [1080p]](https://uptobox.com/w2oywcji19u3)
- [[Refrain] Fruits Basket (2019) - 02 VOSTFR {ADN} [720p]](https://uptobox.com/0w0pd206020w)
- [[Refrain] Fruits Basket (2019) - 02 VOSTFR {ADN} [1080p]](https://uptobox.com/nmgcis66hoq8)
- [[Refrain] Fruits Basket (2019) - 03 VOSTFR {ADN} [720p]](https://uptobox.com/466zlthmk62y)
- [[Refrain] Fruits Basket (2019) - 03 VOSTFR {ADN} [1080p]](https://uptobox.com/uu5d42eje91y)
- [[Refrain] Fruits Basket (2019) - 04 VOSTFR {ADN} [720p]](https://uptobox.com/8ic9os5lb0ji)
- [[Refrain] Fruits Basket (2019) - 04 VOSTFR {ADN} [1080p]](https://uptobox.com/2inrlfemwaz6)
- [[Refrain] Fruits Basket (2019) - 28.5 VOSTFR {ADN} [720p]](https://uptobox.com/m3revc9t3kn2)
- [[Refrain] Fruits Basket (2019) - 05 VOSTFR {ADN} [720p]](https://uptobox.com/sk9e9mxkrgvn)
- [[Refrain] Fruits Basket (2019) - 05 VOSTFR {ADN} [1080p]](https://uptobox.com/9aypxen79gn7)
- [[Refrain] Fruits Basket (2019) - 06 VOSTFR {ADN} [720p]](https://uptobox.com/hv94es34izw4)
- [[Refrain] Fruits Basket (2019) - 06 VOSTFR {ADN} [1080p]](https://uptobox.com/kh158s8bf2ai)
- [[Refrain] Fruits Basket (2019) - 07 VOSTFR {ADN} [720p]](https://uptobox.com/8uzd8qg8ftio)
- [[Refrain] Fruits Basket (2019) - 07 VOSTFR {ADN} [1080p]](https://uptobox.com/66qywxdhibm2)
- [[Refrain] Fruits Basket (2019) - 08 VOSTFR {ADN} [720p]](https://uptobox.com/tjkmz4af7st0)
- [[Refrain] Fruits Basket (2019) - 08 VOSTFR {ADN} [1080p]](https://uptobox.com/66dh3sqzzzmi)
- [[Refrain] Fruits Basket (2019) - 09 VOSTFR {ADN} [720p]](https://uptobox.com/6t1ndvw1u3od)
- [[Refrain] Fruits Basket (2019) - 09 VOSTFR {ADN} [1080p]](https://uptobox.com/jra462mbcbbv)
- [[Refrain] Fruits Basket (2019) - 10 VOSTFR {ADN} [720p]](https://uptobox.com/jgf7fkj07xj6)
- [[Refrain] Fruits Basket (2019) - 10 VOSTFR {ADN} [1080p]](https://uptobox.com/pj3cxc9e7py3)
