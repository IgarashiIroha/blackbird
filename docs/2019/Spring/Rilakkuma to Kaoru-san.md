# Rilakkuma to Kaoru-san

![Rilakkuma to Kaoru-san](https://cdn.myanimelist.net/images/anime/1582/100702l.jpg)

* Japanese:  リラックマとカオルさん

## Information

 - Type: ONA
 - Episodes: 13
 - Status: Finished Airing
 - Aired: Apr 19, 2019
 - Producers: None found, add some 
 - Licensors: None found, add some 
 - Studios: dwarf 
 - Source: Other
 - Genres: Kids
 - Duration: 11 min. per ep.
 - Rating: PG - Children


## Links

- [[Refrain] Rilakkuma to Kaoru-san - 01 VF VOSTFR [1080p]](https://uptobox.com/p3xgulj3nqdg)
- [[Refrain] Rilakkuma to Kaoru-san - 01 VF VOSTFR [720p]](https://uptobox.com/yjq9j6qo1dcv)
- [[Refrain] Rilakkuma to Kaoru-san - 02 VF VOSTFR [1080p]](https://uptobox.com/vxtsimlc80vg)
- [[Refrain] Rilakkuma to Kaoru-san - 02 VF VOSTFR [720p]](https://uptobox.com/7ci5q3xoq91t)
- [[Refrain] Rilakkuma to Kaoru-san - 03 VF VOSTFR [1080p]](https://uptobox.com/o4iphx7qs7ax)
- [[Refrain] Rilakkuma to Kaoru-san - 03 VF VOSTFR [720p]](https://uptobox.com/poxvishfd2o1)
- [[Refrain] Rilakkuma to Kaoru-san - 04 VF VOSTFR [1080p]](https://uptobox.com/zq09mtdzwrpx)
- [[Refrain] Rilakkuma to Kaoru-san - 04 VF VOSTFR [720p]](https://uptobox.com/eloq45w7t8mz)
- [[Refrain] Rilakkuma to Kaoru-san - 05 VF VOSTFR [1080p]](https://uptobox.com/ln0fyxdw6nio)
- [[Refrain] Rilakkuma to Kaoru-san - 05 VF VOSTFR [720p]](https://uptobox.com/tfenbcv7tq86)
- [[Refrain] Rilakkuma to Kaoru-san - 06 VF VOSTFR [1080p]](https://uptobox.com/kzdtqohpzt5w)
- [[Refrain] Rilakkuma to Kaoru-san - 06 VF VOSTFR [720p]](https://uptobox.com/nttv1lyu6s00)
- [[Refrain] Rilakkuma to Kaoru-san - 07 VF VOSTFR [1080p]](https://uptobox.com/xpnhwdo4ddzn)
- [[Refrain] Rilakkuma to Kaoru-san - 07 VF VOSTFR [720p]](https://uptobox.com/ggy70gs7toyf)
- [[Refrain] Rilakkuma to Kaoru-san - 08 VF VOSTFR [1080p]](https://uptobox.com/lq5jta49a4lp)
- [[Refrain] Rilakkuma to Kaoru-san - 08 VF VOSTFR [720p]](https://uptobox.com/x3ct0mpav3ju)
- [[Refrain] Rilakkuma to Kaoru-san - 09 VF VOSTFR [1080p]](https://uptobox.com/a4s3zecav1bp)
- [[Refrain] Rilakkuma to Kaoru-san - 09 VF VOSTFR [720p]](https://uptobox.com/kxnpn51oryoq)
- [[Refrain] Rilakkuma to Kaoru-san - 10 VF VOSTFR [1080p]](https://uptobox.com/rmvzxe4ynype)
- [[Refrain] Rilakkuma to Kaoru-san - 10 VF VOSTFR [720p]](https://uptobox.com/od3sd4w34548)
- [[Refrain] Rilakkuma to Kaoru-san - 11 VF VOSTFR [1080p]](https://uptobox.com/opg29va6k7kk)
- [[Refrain] Rilakkuma to Kaoru-san - 11 VF VOSTFR [720p]](https://uptobox.com/abniyye9h7rx)
- [[Refrain] Rilakkuma to Kaoru-san - 12 VF VOSTFR [1080p]](https://uptobox.com/1vm9hwvys50t)
- [[Refrain] Rilakkuma to Kaoru-san - 12 VF VOSTFR [720p]](https://uptobox.com/d0w4fx3abc4v)
- [[Refrain] Rilakkuma to Kaoru-san - 13 VF VOSTFR [1080p]](https://uptobox.com/576imkwsfvg4)
- [[Refrain] Rilakkuma to Kaoru-san - 13 VF VOSTFR [720p]](https://uptobox.com/caym7c26gev8)
