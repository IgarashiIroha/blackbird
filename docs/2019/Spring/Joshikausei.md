# Joshikausei

![Joshikausei](https://cdn.myanimelist.net/images/anime/1660/98376l.jpg)

* Japanese:  女子かう生

## Information

 - Type: TV
 - Episodes: Unknown
 - Status: Not yet aired
 - Aired: Apr 6, 2019 to ?
 - Premiered: Spring 2019
 - Broadcast: Saturdays at 23:15 (JST)
 - Producers: None found, add some 
 - Licensors: None found, add some 
 - Studios: Seven 
 - Source: Web manga
 - Genres: Slice of Life, Comedy, School
 - Duration: Unknown
 - Rating: PG-13 - Teens 13 or older


## Links

- [[Refrain] Joshikausei - 01 VOSTFR [720p]](https://uptobox.com/r88uo3et3dt8)
- [[Refrain] Joshikausei - 01 VOSTFR [1080p]](https://uptobox.com/6al6wpf3qme9)
- [[Refrain] Joshikausei - 02 VOSTFR [720p]](https://uptobox.com/wf5msx6g9e1h)
- [[Refrain] Joshikausei - 02 VOSTFR [1080p]](https://uptobox.com/o9pa6ivyshev)
- [[Refrain] Joshikausei - 03 VOSTFR [720p]](https://uptobox.com/6x0j6kq8l2rv)
- [[Refrain] Joshikausei - 03 VOSTFR [1080p]](https://uptobox.com/y7yh17fbe4na)
- [[Refrain] Joshikausei - 04 VOSTFR [720p]]()
- [[Refrain] Joshikausei - 04 VOSTFR [1080p]]()
- [[Refrain] Joshikausei - 05 VOSTFR [720p]](https://uptobox.com/raw4cmrvbrmw)
- [[Refrain] Joshikausei - 05 VOSTFR [1080p]](https://uptobox.com/s3os77413z19)
- [[Refrain] Joshikausei - 07 VOSTFR [720p]](https://uptobox.com/8iqdc499128w)
- [[Refrain] Joshikausei - 07 VOSTFR [1080p]](https://uptobox.com/6hdw4pavrhoi)
- [[Refrain] Joshikausei - 08 VOSTFR [720p]](https://uptobox.com/j9qkeb0zsxdq)
- [[Refrain] Joshikausei - 08 VOSTFR [1080p]](https://uptobox.com/2khrqfq7r5xc)
- [[Refrain] Joshikausei - 09 VOSTFR [720p]](https://uptobox.com/ux9ib7f51rqb)
- [[Refrain] Joshikausei - 09 VOSTFR [1080p]](https://uptobox.com/1e9ug7mm7zl9)
