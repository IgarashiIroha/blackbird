# Carole & Tuesday

![Carole & Tuesday](https://cdn.myanimelist.net/images/anime/1611/96157l.jpg)

* Japanese:  キャロル&チューズデイ

## Information

 - Type: TV
 - Episodes: 24
 - Status: Not yet aired
 - Aired: Apr 11, 2019 to ?
 - Premiered: Spring 2019
 - Broadcast: Thursdays at 00:55 (JST)
 - Producers: Fuji TV, flying DOG 
 - Licensors: None found, add some 
 - Studios: Bones 
 - Source: Original
 - Genres: Sci-Fi, Music, Drama, Romance
 - Duration: Unknown
 - Rating: None


## Links

