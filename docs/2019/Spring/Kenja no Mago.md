# Kenja no Mago

![Kenja no Mago](https://cdn.myanimelist.net/images/anime/1745/99413l.jpg)

* Japanese:  賢者の孫

## Information

 - Type: TV
 - Episodes: Unknown
 - Status: Not yet aired
 - Aired: Apr 10, 2019 to ?
 - Premiered: Spring 2019
 - Broadcast: Wednesdays at 23:30 (JST)
 - Producers: None found, add some 
 - Licensors: None found, add some 
 - Studios: Silver Link. 
 - Source: Light novel
 - Genres: Action, Comedy, Magic, Fantasy
 - Duration: Unknown
 - Rating: PG-13 - Teens 13 or older


## Links

