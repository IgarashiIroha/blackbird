# Nobunaga-sensei no Osanazuma

![Nobunaga-sensei no Osanazuma](https://cdn.myanimelist.net/images/anime/1524/97286l.jpg)

* Japanese:  ノブナガ先生の幼な妻

## Information

 - Type: TV
 - Episodes: Unknown
 - Status: Not yet aired
 - Aired: Apr 6, 2019 to ?
 - Premiered: Spring 2019
 - Broadcast: Saturdays at 23:20 (JST)
 - Producers: None found, add some
 - Licensors: None found, add some
 - Studios: Seven
 - Source: Manga
 - Genres: Comedy, Romance, School
 - Duration: Unknown
 - Rating: PG-13 - Teens 13 or older


## Links

- [[Refrain] Nobunaga-sensei no Osanazuma - 01 NC VOSTFR [720p]](https://uptobox.com/pl39j1wboxka)
- [[Refrain] Nobunaga-sensei no Osanazuma - 01 NC VOSTFR [1080p]](https://uptobox.com/nmarcpdmvp1d)
- [[Refrain] Nobunaga-sensei no Osanazuma - 01 VOSTFR [720p]](https://uptobox.com/qm69bgqtl7v3)
- [[Refrain] Nobunaga-sensei no Osanazuma - 01 VOSTFR [1080p]](https://uptobox.com/cqdm13j6r4qb)
- [[Refrain] Nobunaga-sensei no Osanazuma - 02 NC VOSTFR [720p]](https://uptobox.com/0l6mj5e3syu0)
- [[Refrain] Nobunaga-sensei no Osanazuma - 02 NC VOSTFR [1080p]](https://uptobox.com/ycp393o3xr27)
- [[Refrain] Nobunaga-sensei no Osanazuma - 02 VOSTFR [720p]](https://uptobox.com/691g9gzojgdb)
- [[Refrain] Nobunaga-sensei no Osanazuma - 02 VOSTFR [1080p]](https://uptobox.com/b56dfnvekp18)
- [[Refrain] Nobunaga-sensei no Osanazuma - 03 VOSTFR [720p]](https://uptobox.com/mrpgatwws9op)
- [[Refrain] Nobunaga-sensei no Osanazuma - 03 VOSTFR [1080p]](https://uptobox.com/xndfp3excw79)
- [[Refrain] Nobunaga-sensei no Osanazuma - 03 VOSTFR [720p]](https://uptobox.com/s48kg2b14tc0)
- [[Refrain] Nobunaga-sensei no Osanazuma - 03 VOSTFR [1080p]](https://uptobox.com/5vuy0vbw0zup)
- [[Refrain] Nobunaga-sensei no Osanazuma - 04 VOSTFR [720p]]()
- [[Refrain] Nobunaga-sensei no Osanazuma - 04 VOSTFR [1080p]]()
- [[Refrain] Nobunaga-sensei no Osanazuma - 04 VOSTFR [720p]](https://uptobox.com/mwjx9wv4c5gu)
- [[Refrain] Nobunaga-sensei no Osanazuma - 04 VOSTFR [1080p]](https://uptobox.com/swnyjzouw8oy)
- [[Refrain] Nobunaga-sensei no Osanazuma - 05 VOSTFR [720p]](https://uptobox.com/hfmbskkus0b9)
- [[Refrain] Nobunaga-sensei no Osanazuma - 05 VOSTFR [1080p]](https://uptobox.com/1me3lsnpm1xi)
- [[Refrain] Nobunaga-sensei no Osanazuma - 05 VOSTFR [720p]](https://uptobox.com/cksfooq6jtdq)
- [[Refrain] Nobunaga-sensei no Osanazuma - 05 VOSTFR [1080p]](https://uptobox.com/pslhkyfyihac)
- [[Refrain] Nobunaga-sensei no Osanazuma - 06 VOSTFR [720p]](https://uptobox.com/k7oc74kae9x2)
- [[Refrain] Nobunaga-sensei no Osanazuma - 06 VOSTFR [1080p]](https://uptobox.com/mrwuu0zd5m84)
- [[Refrain] Nobunaga-sensei no Osanazuma - 07 VOSTFR [720p]](https://uptobox.com/lgze311nwpdm)
- [[Refrain] Nobunaga-sensei no Osanazuma - 07 VOSTFR [1080p]](https://uptobox.com/q5skdovp1kqh)
- [[Refrain] Nobunaga-sensei no Osanazuma - 07 VOSTFR [720p]](https://uptobox.com/k8s4x6j0pd2c)
- [[Refrain] Nobunaga-sensei no Osanazuma - 07 VOSTFR [1080p]](https://uptobox.com/irsybv0a3m7x)
- [[Refrain] Nobunaga-sensei no Osanazuma - 08 VOSTFR [720p]](https://uptobox.com/4zs3253ilift)
- [[Refrain] Nobunaga-sensei no Osanazuma - 08 VOSTFR [1080p]](https://uptobox.com/e5qiwuif0lzx)
- [[Refrain] Nobunaga-sensei no Osanazuma - 08 VOSTFR [720p]](https://uptobox.com/5wj9ymarwcrw)
- [[Refrain] Nobunaga-sensei no Osanazuma - 08 VOSTFR [1080p]](https://uptobox.com/qlnyw46orw4z)
- [[Refrain] Nobunaga-sensei no Osanazuma - 09 VOSTFR [720p]](https://uptobox.com/n1w9a7jvgfy4)
- [[Refrain] Nobunaga-sensei no Osanazuma - 09 VOSTFR [1080p]](https://uptobox.com/zi2oqlmzo43h)
- [[Refrain] Nobunaga-sensei no Osanazuma - 09 VOSTFR [720p]](https://uptobox.com/m14b5bdg8gmb)
- [[Refrain] Nobunaga-sensei no Osanazuma - 09 VOSTFR [1080p]](https://uptobox.com/fy8iamnvxu16)
