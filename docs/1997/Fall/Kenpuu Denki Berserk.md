# Kenpuu Denki Berserk

![Kenpuu Denki Berserk](https://cdn.myanimelist.net/images/anime/12/18520l.jpg)

* Japanese:  剣風伝奇ベルセルク

## Information

 - Type: TV
 - Episodes: 25
 - Status: Finished Airing
 - Aired: Oct 8, 1997 to Apr 1, 1998
 - Premiered: Fall 1997
 - Broadcast: Wednesdays at 01:45 (JST)
 - Producers: VAP, Hakusensha, Nippon Television Network 
 - Licensors: Media Blasters, NYAV Post 
 - Studios: OLM 
 - Source: Manga
 - Genres: Action, Adventure, Demons, Drama, Fantasy, Horror, Military, Romance, Seinen, Supernatural
 - Duration: 25 min. per ep.
 - Rating: R+ - Mild Nudity


## Links

- [[Refrain] Kenpuu Denki Berserk - 01 VF VOSTFR [1080p]](https://uptobox.com/f3z7wlvcxifd)
- [[Refrain] Kenpuu Denki Berserk - 01 VF VOSTFR [720p]](https://uptobox.com/qkvg4h3wve4f)
- [[Refrain] Kenpuu Denki Berserk - 02 VF VOSTFR [1080p]](https://uptobox.com/l3ltiza24spd)
- [[Refrain] Kenpuu Denki Berserk - 03 VF VOSTFR [1080p]](https://uptobox.com/o8zq49eta022)
- [[Refrain] Kenpuu Denki Berserk - 03 VF VOSTFR [720p]](https://uptobox.com/4j6iupypi2v7)
- [[Refrain] Kenpuu Denki Berserk - 04 VF VOSTFR [1080p]](https://uptobox.com/8nnfailr3r61)
- [[Refrain] Kenpuu Denki Berserk - 04 VF VOSTFR [720p]](https://uptobox.com/xxobfhwt7wsp)
- [[Refrain] Kenpuu Denki Berserk - 05 VF VOSTFR [1080p]](https://uptobox.com/gt8tzv8ugwqf)
- [[Refrain] Kenpuu Denki Berserk - 05 VF VOSTFR [720p]](https://uptobox.com/wqfb58whw2uk)
- [[Refrain] Kenpuu Denki Berserk - 06 VF VOSTFR [1080p]](https://uptobox.com/ok3gb5gycmx1)
- [[Refrain] Kenpuu Denki Berserk - 06 VF VOSTFR [720p]](https://uptobox.com/erdt74ikinhj)
- [[Refrain] Kenpuu Denki Berserk - 07 VF VOSTFR [1080p]](https://uptobox.com/nys150bhlb0z)
- [[Refrain] Kenpuu Denki Berserk - 07 VF VOSTFR [720p]](https://uptobox.com/nsgkiq7rcqct)
- [[Refrain] Kenpuu Denki Berserk - 08 VF VOSTFR [1080p]](https://uptobox.com/kxu0fkumg0z6)
- [[Refrain] Kenpuu Denki Berserk - 08 VF VOSTFR [720p]](https://uptobox.com/9uocx3ve607p)
- [[Refrain] Kenpuu Denki Berserk - 09 VF VOSTFR [1080p]](https://uptobox.com/pxrjrak6l7hp)
- [[Refrain] Kenpuu Denki Berserk - 09 VF VOSTFR [720p]](https://uptobox.com/5i5hhtz3chm2)
- [[Refrain] Kenpuu Denki Berserk - 10 VF VOSTFR [1080p]](https://uptobox.com/hcy7h6ei1p1q)
- [[Refrain] Kenpuu Denki Berserk - 10 VF VOSTFR [720p]](https://uptobox.com/1f3awq4u6l8i)
- [[Refrain] Kenpuu Denki Berserk - 11 VF VOSTFR [1080p]](https://uptobox.com/mxhtwnt4apv1)
- [[Refrain] Kenpuu Denki Berserk - 11 VF VOSTFR [720p]](https://uptobox.com/4o35673gjbnp)
- [[Refrain] Kenpuu Denki Berserk - 12 VF VOSTFR [1080p]](https://uptobox.com/dvd2nmpi3tg0)
- [[Refrain] Kenpuu Denki Berserk - 12 VF VOSTFR [720p]](https://uptobox.com/s4tsf3jj7kq5)
- [[Refrain] Kenpuu Denki Berserk - 13 VF VOSTFR [1080p]](https://uptobox.com/vm6gr8hzqwlz)
- [[Refrain] Kenpuu Denki Berserk - 13 VF VOSTFR [720p]](https://uptobox.com/yil41tp2cbtj)
- [[Refrain] Kenpuu Denki Berserk - 14 VF VOSTFR [1080p]](https://uptobox.com/6v4v7dip7sz0)
- [[Refrain] Kenpuu Denki Berserk - 14 VF VOSTFR [720p]](https://uptobox.com/i130mttdh130)
- [[Refrain] Kenpuu Denki Berserk - 15 VF VOSTFR [1080p]](https://uptobox.com/0jz6ob1pyoxx)
- [[Refrain] Kenpuu Denki Berserk - 15 VF VOSTFR [720p]](https://uptobox.com/sjwq6c2fc6yy)
- [[Refrain] Kenpuu Denki Berserk - 16 VF VOSTFR [1080p]](https://uptobox.com/gfafpks84bp2)
- [[Refrain] Kenpuu Denki Berserk - 16 VF VOSTFR [720p]](https://uptobox.com/a7s77tezw1md)
- [[Refrain] Kenpuu Denki Berserk - 17 VF VOSTFR [1080p]](https://uptobox.com/odjosjdn5j86)
- [[Refrain] Kenpuu Denki Berserk - 17 VF VOSTFR [720p]](https://uptobox.com/qvxa4uyeetr7)
- [[Refrain] Kenpuu Denki Berserk - 18 VF VOSTFR [1080p]](https://uptobox.com/j8pbyhr8ccft)
- [[Refrain] Kenpuu Denki Berserk - 18 VF VOSTFR [720p]](https://uptobox.com/etyd90dlqh2b)
- [[Refrain] Kenpuu Denki Berserk - 19 VF VOSTFR [1080p]](https://uptobox.com/va7r2odk4tm3)
- [[Refrain] Kenpuu Denki Berserk - 19 VF VOSTFR [720p]](https://uptobox.com/pphug0d8xk5z)
- [[Refrain] Kenpuu Denki Berserk - 20 VF VOSTFR [1080p]](https://uptobox.com/hnxj38fvpjch)
- [[Refrain] Kenpuu Denki Berserk - 20 VF VOSTFR [720p]](https://uptobox.com/0sa0hdbhtwf8)
- [[Refrain] Kenpuu Denki Berserk - 21 VF VOSTFR [1080p]](https://uptobox.com/u10t2pl9f1mx)
- [[Refrain] Kenpuu Denki Berserk - 21 VF VOSTFR [720p]](https://uptobox.com/1cg9nh715vxc)
- [[Refrain] Kenpuu Denki Berserk - 22 VF VOSTFR [1080p]](https://uptobox.com/yyazbale5z6k)
- [[Refrain] Kenpuu Denki Berserk - 22 VF VOSTFR [720p]](https://uptobox.com/ynif8zjdnv0k)
- [[Refrain] Kenpuu Denki Berserk - 23 VF VOSTFR [1080p]](https://uptobox.com/5pt8fu4qimz4)
- [[Refrain] Kenpuu Denki Berserk - 23 VF VOSTFR [720p]](https://uptobox.com/3bd5zfbrmcoq)
- [[Refrain] Kenpuu Denki Berserk - 24 VF VOSTFR [1080p]](https://uptobox.com/5a22n3ych7gh)
- [[Refrain] Kenpuu Denki Berserk - 24 VF VOSTFR [720p]](https://uptobox.com/laeseckv9l91)
- [[Refrain] Kenpuu Denki Berserk - 25 VF VOSTFR [1080p]](https://uptobox.com/4fpk2xzbb3lj)
- [[Refrain] Kenpuu Denki Berserk - 25 VF VOSTFR [720p]](https://uptobox.com/aogzpffx5gf3)
