# Neon Genesis Evangelion

![Neon Genesis Evangelion](https://cdn.myanimelist.net/images/anime/12/21418l.jpg)

* Japanese:  新世紀エヴァンゲリオン

## Information

 - Type: TV
 - Episodes: 26
 - Status: Finished Airing
 - Aired: Oct 4, 1995 to Mar 27, 1996
 - Premiered: Fall 1995
 - Broadcast: Wednesdays at 18:30 (JST)
 - Producers: TV Tokyo, Kadokawa Shoten, Nihon Ad Systems, Audio Tanaka 
 - Licensors: ADV Films 
 - Studios: Gainax, Tatsunoko Production 
 - Source: Original
 - Genres: Action, Dementia, Drama, Mecha, Psychological, Sci-Fi
 - Duration: 24 min. per ep.
 - Rating: PG-13 - Teens 13 or older


## Links

