# Pokemon XY&Z

![Pokemon XY&Z](https://cdn.myanimelist.net/images/anime/1599/94412l.jpg)

* Japanese:  ポケットモンスターXY&Z

## Information

 - Type: TV
 - Episodes: 47
 - Status: Finished Airing
 - Aired: Oct 29, 2015 to Oct 27, 2016
 - Premiered: Fall 2015
 - Broadcast: Thursdays at 18:55 (JST)
 - Producers: None found, add some 
 - Licensors: The Pokemon Company International 
 - Studios: OLM 
 - Source: Game
 - Genres: Action, Adventure, Comedy, Kids, Fantasy
 - Duration: 23 min. per ep.
 - Rating: PG - Children


## Links

- [[Refrain] Pokemon XY]](https://uptobox.com/cizmguhl0klq)
- [[Refrain] Pokemon XY]](https://uptobox.com/9tea0p2uv2ka)
