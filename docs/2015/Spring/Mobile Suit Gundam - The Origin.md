# Mobile Suit Gundam: The Origin

![Mobile Suit Gundam: The Origin](https://cdn.myanimelist.net/images/anime/4/72702l.jpg)

* Japanese:  機動戦士ガンダムTHE ORIGIN

## Information

 - Type: OVA
 - Episodes: 6
 - Status: Finished Airing
 - Aired: Apr 24, 2015 to May 5, 2018
 - Producers: None found, add some 
 - Licensors: NYAV Post 
 - Studios: Sunrise 
 - Source: Manga
 - Genres: Action, Military, Sci-Fi, Space, Mecha, Shounen
 - Duration: 1 hr. 11 min. per ep.
 - Rating: R+ - Mild Nudity


## Links

