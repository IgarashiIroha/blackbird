# Rurouni Kenshin: Meiji Kenkaku Romantan - Seisou-hen

![Rurouni Kenshin: Meiji Kenkaku Romantan - Seisou-hen](https://cdn.myanimelist.net/images/anime/1971/90877l.jpg)

* Japanese:  るろうに剣心 -明治剣客浪漫譚- 星霜編

## Information

 - Type: OVA
 - Episodes: 2
 - Status: Finished Airing
 - Aired: Dec 3, 2001
 - Producers: SME Visual Works 
 - Licensors: ADV Films, Aniplex of America 
 - Studios: Studio Deen 
 - Source: Manga
 - Genres: Romance, Samurai, Historical, Drama, Shounen
 - Duration: 42 min. per ep.
 - Rating: R - 17+ (violence & profanity)


## Links

