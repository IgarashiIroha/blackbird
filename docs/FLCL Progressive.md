# FLCL Progressive

![FLCL Progressive](https://cdn.myanimelist.net/images/anime/1499/93700l.jpg)

* Japanese:  フリクリ プログレ

## Information

 - Type: Movie
 - Episodes: 1
 - Status: Finished Airing
 - Aired: Sep 28, 2018
 - Producers: Toho Visual Entertainment 
 - Licensors: NYAV Post 
 - Studios: Production I.G, Signal.MD, Production GoodBook 
 - Source: Original
 - Genres: Action, Comedy, Dementia, Mecha, Parody, Sci-Fi
 - Duration: 2 hr. 16 min.
 - Rating: PG-13 - Teens 13 or older


## Links

- [[Refrain] FLCL Progressive - 01 VOSTFR [1080p]](https://uptobox.com/idfua95rsfx5)
- [[Refrain] FLCL Progressive - 01 VOSTFR [720p]](https://uptobox.com/ca035g2qhdws)
- [[Refrain] FLCL Progressive - 02 VOSTFR [1080p]](https://uptobox.com/b1qr0amta64o)
- [[Refrain] FLCL Progressive - 02 VOSTFR [720p]](https://uptobox.com/yzf626xcqw7v)
- [[Refrain] FLCL Progressive - 03 VOSTFR [1080p]](https://uptobox.com/56kxu1nj4j6y)
- [[Refrain] FLCL Progressive - 03 VOSTFR [720p]](https://uptobox.com/brb41534346g)
- [[Refrain] FLCL Progressive - 04 VOSTFR [1080p]](https://uptobox.com/prpelck0ujyb)
- [[Refrain] FLCL Progressive - 04 VOSTFR [720p]](https://uptobox.com/uuwygoat019c)
- [[Refrain] FLCL Progressive - 05 VOSTFR [1080p]](https://uptobox.com/bwuoyw4sizej)
- [[Refrain] FLCL Progressive - 05 VOSTFR [720p]](https://uptobox.com/v9hqbefr92aj)
- [[Refrain] FLCL Progressive - 06 VOSTFR [1080p]](https://uptobox.com/mupunxr8du87)
- [[Refrain] FLCL Progressive - 06 VOSTFR [720p]](https://uptobox.com/3zq1b3bqbksz)
