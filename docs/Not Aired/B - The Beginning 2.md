# B: The Beginning 2

![B: The Beginning 2](https://cdn.myanimelist.net/images/anime/1594/93194l.jpg)

* Japanese:  B: The Beginning 2

## Information

 - Type: ONA
 - Episodes: 12
 - Status: Not yet aired
 - Aired: Not available
 - Producers: None found, add some 
 - Licensors: None found, add some 
 - Studios: Production I.G 
 - Source: Original
 - Genres: Action, Mystery, Police, Psychological, Supernatural, Thriller
 - Duration: 23 min. per ep.
 - Rating: R+ - Mild Nudity


## Links

