# Neon Genesis Evangelion: Death & Rebirth

![Neon Genesis Evangelion: Death & Rebirth](https://cdn.myanimelist.net/images/anime/13/75963l.jpg)

* Japanese:  新世紀エヴァンゲリオン劇場版 シト新生

## Information

 - Type: Movie
 - Episodes: 1
 - Status: Finished Airing
 - Aired: Mar 15, 1997
 - Producers: Movic, Sega, TV Tokyo Music, Audio Tanaka 
 - Licensors: Manga Entertainment 
 - Studios: Gainax, Production I.G 
 - Source: Original
 - Genres: Drama, Mecha, Psychological, Sci-Fi
 - Duration: 1 hr. 41 min.
 - Rating: R - 17+ (violence & profanity)


## Links

