# Meitantei Holmes

![Meitantei Holmes](https://cdn.myanimelist.net/images/anime/4/75723l.jpg)

* Japanese:  名探偵ホームズ

## Information

 - Type: TV
 - Episodes: 26
 - Status: Finished Airing
 - Aired: Nov 6, 1984 to May 21, 1985
 - Premiered: Fall 1984
 - Broadcast: Unknown
 - Producers: None found, add some 
 - Licensors: Discotek Media, Geneon Entertainment USA 
 - Studios: Studio Gallop, TMS Entertainment 
 - Source: Novel
 - Genres: Action, Adventure, Mystery, Comedy, Police
 - Duration: 25 min. per ep.
 - Rating: G - All Ages


## Links



