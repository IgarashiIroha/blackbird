# Planet With

![Planet With](https://cdn.myanimelist.net/images/anime/1985/95103l.jpg)

* Japanese:  プラネット・ウィズ

## Information

 - Type: TV
 - Episodes: 12
 - Status: Finished Airing
 - Aired: Jul 8, 2018 to Sep 23, 2018
 - Premiered: Summer 2018
 - Broadcast: Sundays at 22:30 (JST)
 - Producers: None found, add some
 - Licensors: None found, add some
 - Studios: J.C.Staff
 - Source: Original
 - Genres: Action, Mecha, Sci-Fi
 - Duration: 23 min. per ep.
 - Rating: PG-13 - Teens 13 or older


## Links

- [[Refrain] Planet With - 01 SP VOSTFR [720p]](https://uptobox.com/h58hg37jc857)
- [[Refrain] Planet With - 01 SP VOSTFR [1080p]](https://uptobox.com/80v81ymcdwyn)
- [[Refrain] Planet With - 02 VOSTFR [720p]](https://uptobox.com/vxyaeo0txq1g)
- [[Refrain] Planet With - 02 VOSTFR [1080p]](https://uptobox.com/7q3aqslrym6s)
- [[Refrain] Planet With - 03 VOSTFR [720p]](https://uptobox.com/9qe1rdytiuit)
- [[Refrain] Planet With - 03 VOSTFR [1080p]](https://uptobox.com/ekqx0kieg415)
- [[Refrain] Planet With - 04 VOSTFR [720p]](https://uptobox.com/qpvovodclwiw)
- [[Refrain] Planet With - 04 VOSTFR [1080p]](https://uptobox.com/85rs7qgrkr2p)
- [[Refrain] Planet With - 05 VOSTFR [720p]](https://uptobox.com/085229wq4byi)
- [[Refrain] Planet With - 05 VOSTFR [1080p]](https://uptobox.com/80kqsxo39gcp)
- [[Refrain] Planet With - 06 VOSTFR [720p]](https://uptobox.com/zumbfwauvfe5)
- [[Refrain] Planet With - 06 VOSTFR [1080p]](https://uptobox.com/b6i9rtthe4ac)
- [[Refrain] Planet With - 07 VOSTFR [720p]](https://uptobox.com/km0o99cjeybh)
- [[Refrain] Planet With - 07 VOSTFR [1080p]](https://uptobox.com/zd5iwwm83jix)
- [[Refrain] Planet With - 08 VOSTFR [720p]](https://uptobox.com/mk98m95pokm1)
- [[Refrain] Planet With - 08 VOSTFR [1080p]](https://uptobox.com/yx4i1vnbhuk0)
- [[Refrain] Planet With - 09 VOSTFR [720p]](https://uptobox.com/tvf361at3eaf)
- [[Refrain] Planet With - 09 VOSTFR [1080p]](https://uptobox.com/mwp0c73yztnu)
- [[Refrain] Planet With - 10 VOSTFR [720p]](https://uptobox.com/50y7gkb8a80r)
- [[Refrain] Planet With - 10 VOSTFR [1080p]](https://uptobox.com/3s0pvpdp4dga)
- [[Refrain] Planet With - 11 VOSTFR [720p]](https://uptobox.com/6nyfpl9ljsov)
- [[Refrain] Planet With - 11 VOSTFR [1080p]](https://uptobox.com/jzk158fs9d4n)
- [[Refrain] Planet With - 12 VOSTFR [720p]](https://uptobox.com/9ar5uhwtbvds)
- [[Refrain] Planet With - 12 VOSTFR [1080p]](https://uptobox.com/kbsh2iasx0by)
