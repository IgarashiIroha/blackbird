# Sunohara-sou no Kanrinin-san

![Sunohara-sou no Kanrinin-san](https://cdn.myanimelist.net/images/anime/1368/95074l.jpg)

* Japanese:  すのはら荘の管理人さん

## Information

 - Type: TV
 - Episodes: 12
 - Status: Finished Airing
 - Aired: Jul 5, 2018 to Sep 20, 2018
 - Premiered: Summer 2018
 - Broadcast: Thursdays at 21:00 (JST)
 - Producers: flying DOG, Ichijinsha 
 - Licensors: None found, add some 
 - Studios: Silver Link. 
 - Source: 4-koma manga
 - Genres: Comedy, Slice of Life
 - Duration: 23 min. per ep.
 - Rating: PG-13 - Teens 13 or older


## Links

