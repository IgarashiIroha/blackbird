# Satsuriku no Tenshi

![Satsuriku no Tenshi](https://cdn.myanimelist.net/images/anime/1862/95624l.jpg)

* Japanese:  殺戮の天使

## Information

 - Type: TV
 - Episodes: 12
 - Status: Finished Airing
 - Aired: Jul 6, 2018 to Sep 21, 2018
 - Premiered: Summer 2018
 - Broadcast: Fridays at 20:30 (JST)
 - Producers: Lantis, Kadokawa
 - Licensors: Funimation
 - Studios: J.C.Staff
 - Source: Game
 - Genres: Adventure, Horror, Psychological, Thriller
 - Duration: 23 min. per ep.
 - Rating: R - 17+ (violence & profanity)


## Links

- [[Refrain] Satsuriku no Tenshi - 01 VOSTFR [720p]](https://uptobox.com/6gfz2ce14gir)
- [[Refrain] Satsuriku no Tenshi - 01 VOSTFR [1080p]](https://uptobox.com/4i59ns6bunl3)
- [[Refrain] Satsuriku no Tenshi - 02 VOSTFR [720p]](https://uptobox.com/1oaq7usy7y9x)
- [[Refrain] Satsuriku no Tenshi - 02 VOSTFR [1080p]](https://uptobox.com/4pgeozdnxbtx)
- [[Refrain] Satsuriku no Tenshi - 03 VOSTFR [720p]](https://uptobox.com/1o3bslzka321)
- [[Refrain] Satsuriku no Tenshi - 03 VOSTFR [1080p]](https://uptobox.com/cjjwdneyklx9)
- [[Refrain] Satsuriku no Tenshi - 04 VOSTFR [720p]](https://uptobox.com/qscydj83gjx3)
- [[Refrain] Satsuriku no Tenshi - 04 VOSTFR [1080p]](https://uptobox.com/oxilq3m0t7jv)
- [[Refrain] Satsuriku no Tenshi - 05 VOSTFR [720p]](https://uptobox.com/dkg4sm3i2opj)
- [[Refrain] Satsuriku no Tenshi - 05 VOSTFR [1080p]](https://uptobox.com/lorrjbak31vw)
- [[Refrain] Satsuriku no Tenshi - 06 VOSTFR [720p]](https://uptobox.com/68i8s12zshnd)
- [[Refrain] Satsuriku no Tenshi - 06 VOSTFR [1080p]](https://uptobox.com/qiclt0vxklt0)
- [[Refrain] Satsuriku no Tenshi - 07 VOSTFR [720p]](https://uptobox.com/4mhghsq2o653)
- [[Refrain] Satsuriku no Tenshi - 07 VOSTFR [1080p]](https://uptobox.com/mlgnfjwsv7yq)
- [[Refrain] Satsuriku no Tenshi - 08 VOSTFR [720p]](https://uptobox.com/9v8syzbaetuj)
- [[Refrain] Satsuriku no Tenshi - 08 VOSTFR [1080p]](https://uptobox.com/atx9td8mpz6b)
- [[Refrain] Satsuriku no Tenshi - 09 VOSTFR [720p]](https://uptobox.com/z19zctcmbv27)
- [[Refrain] Satsuriku no Tenshi - 09 VOSTFR [1080p]](https://uptobox.com/ktpqdt9r1w4r)
- [[Refrain] Satsuriku no Tenshi - 10 VOSTFR [720p]](https://uptobox.com/9a553vz7npse)
- [[Refrain] Satsuriku no Tenshi - 10 VOSTFR [1080p]](https://uptobox.com/mteza9m9ihxu)
- [[Refrain] Satsuriku no Tenshi - 11 VOSTFR [720p]](https://uptobox.com/lf7rjj67795c)
- [[Refrain] Satsuriku no Tenshi - 11 VOSTFR [1080p]](https://uptobox.com/585tnmzx2dek)
- [[Refrain] Satsuriku no Tenshi - 12 VOSTFR [720p]](https://uptobox.com/rsregvhuan1h)
- [[Refrain] Satsuriku no Tenshi - 12 VOSTFR [1080p]](https://uptobox.com/yaucb6tuoss8)
- [[Refrain] Satsuriku no Tenshi - 13 VOSTFR [720p]](https://uptobox.com/c3a7aii9szwp)
- [[Refrain] Satsuriku no Tenshi - 13 VOSTFR [1080p]](https://uptobox.com/6ncng5gpd2xo)
- [[Refrain] Satsuriku no Tenshi - 14 VOSTFR [720p]](https://uptobox.com/0kjasp4pg89v)
- [[Refrain] Satsuriku no Tenshi - 14 VOSTFR [1080p]](https://uptobox.com/dgyw5l6t06qt)
- [[Refrain] Satsuriku no Tenshi - 15 VOSTFR [720p]](https://uptobox.com/dr724dkhpf87)
- [[Refrain] Satsuriku no Tenshi - 15 VOSTFR [1080p]](https://uptobox.com/yuebh7w0ejoj)
- [[Refrain] Satsuriku no Tenshi - 16 VOSTFR [720p]](https://uptobox.com/5plsllbvw6k2)
- [[Refrain] Satsuriku no Tenshi - 16 VOSTFR [1080p]](https://uptobox.com/x7fyorw9kcog)
- [[Refrain] Shichisei no Subaru - 01 VOSTFR [720p]](https://uptobox.com/kjgbm6y6733y)
- [[Refrain] Shichisei no Subaru - 01 VOSTFR [1080p]](https://uptobox.com/38iomva2vhb0)
- [[Refrain] Shichisei no Subaru - 02 VOSTFR [720p]](https://uptobox.com/sjf1s1f3nz17)
- [[Refrain] Shichisei no Subaru - 02 VOSTFR [1080p]](https://uptobox.com/10ujhj1fg664)
- [[Refrain] Shichisei no Subaru - 03 VOSTFR [720p]](https://uptobox.com/9rlnwcmmjict)
- [[Refrain] Shichisei no Subaru - 03 VOSTFR [1080p]](https://uptobox.com/0hn9oq3stjyh)
- [[Refrain] Shichisei no Subaru - 04 VOSTFR [720p]](https://uptobox.com/05szcz14pz2m)
- [[Refrain] Shichisei no Subaru - 04 VOSTFR [1080p]](https://uptobox.com/scr9mww38req)
- [[Refrain] Shichisei no Subaru - 05 VOSTFR [720p]](https://uptobox.com/etz7t536zrf5)
- [[Refrain] Shichisei no Subaru - 05 VOSTFR [1080p]](https://uptobox.com/pd4xpjjdp769)
- [[Refrain] Shichisei no Subaru - 06 VOSTFR [720p]](https://uptobox.com/22fwk04td792)
- [[Refrain] Shichisei no Subaru - 06 VOSTFR [1080p]](https://uptobox.com/v4z6povoum04)
- [[Refrain] Shichisei no Subaru - 07 VOSTFR [720p]](https://uptobox.com/ucdc3h4qiw49)
- [[Refrain] Shichisei no Subaru - 07 VOSTFR [1080p]](https://uptobox.com/950cxyffbgwk)
- [[Refrain] Shichisei no Subaru - 08 VOSTFR [720p]](https://uptobox.com/ozsecuix4tc9)
- [[Refrain] Shichisei no Subaru - 08 VOSTFR [1080p]](https://uptobox.com/yi6vesclq7pl)
- [[Refrain] Shichisei no Subaru - 09 VOSTFR [720p]](https://uptobox.com/sysrvvn0iyfe)
- [[Refrain] Shichisei no Subaru - 09 VOSTFR [1080p]](https://uptobox.com/1fp1mye09i4j)
- [[Refrain] Shichisei no Subaru - 10 VOSTFR [720p]](https://uptobox.com/kp6famd9gdyo)
- [[Refrain] Shichisei no Subaru - 10 VOSTFR [1080p]](https://uptobox.com/y6syj7jayx7w)
- [[Refrain] Shichisei no Subaru - 11 VOSTFR [720p]](https://uptobox.com/mh8hfle9rk79)
- [[Refrain] Shichisei no Subaru - 11 VOSTFR [1080p]](https://uptobox.com/jzldg1l1fn4g)
- [[Refrain] Shichisei no Subaru - 12 VOSTFR [720p]](https://uptobox.com/xkiefq8h6klu)
- [[Refrain] Shichisei no Subaru - 12 VOSTFR [1080p]](https://uptobox.com/czkhuoekwc98)
