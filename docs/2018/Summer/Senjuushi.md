# Senjuushi

![Senjuushi](https://cdn.myanimelist.net/images/anime/1357/95039l.jpg)

* Japanese:  千銃士

## Information

 - Type: TV
 - Episodes: 12
 - Status: Finished Airing
 - Aired: Jul 3, 2018 to Sep 25, 2018
 - Premiered: Summer 2018
 - Broadcast: Tuesdays at 23:00 (JST)
 - Producers: None found, add some 
 - Licensors: Sentai Filmworks 
 - Studios: TMS Entertainment 
 - Source: Game
 - Genres: Action, Military
 - Duration: 24 min. per ep.
 - Rating: PG-13 - Teens 13 or older


## Links

