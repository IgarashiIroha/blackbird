# Kyoto Teramachi Sanjou no Holmes

![Kyoto Teramachi Sanjou no Holmes](https://cdn.myanimelist.net/images/anime/1304/92930l.jpg)

* Japanese:  京都寺町三条のホームズ

## Information

 - Type: TV
 - Episodes: 12
 - Status: Finished Airing
 - Aired: Jul 10, 2018 to Sep 25, 2018
 - Premiered: Summer 2018
 - Broadcast: Tuesdays at 02:10 (JST)
 - Producers: None found, add some
 - Licensors: None found, add some
 - Studios: Seven
 - Source: Novel
 - Genres: Mystery
 - Duration: 23 min. per ep.
 - Rating: PG-13 - Teens 13 or older


## Links

- [[Refrain] Kyoto Teramachi Sanjou no Holmes - 01 VOSTFR [720p]](https://uptobox.com/b0gtuqp8159v)
- [[Refrain] Kyoto Teramachi Sanjou no Holmes - 01 VOSTFR [1080p]](https://uptobox.com/hw98izzkn2zu)
- [[Refrain] Kyoto Teramachi Sanjou no Holmes - 02 VOSTFR [720p]](https://uptobox.com/ocdl9mzfj0pb)
- [[Refrain] Kyoto Teramachi Sanjou no Holmes - 02 VOSTFR [1080p]](https://uptobox.com/6i6d6ey1kuzw)
- [[Refrain] Kyoto Teramachi Sanjou no Holmes - 03 VOSTFR [720p]](https://uptobox.com/2hcjoasqkfw7)
- [[Refrain] Kyoto Teramachi Sanjou no Holmes - 03 VOSTFR [1080p]](https://uptobox.com/hz9owxejw5al)
- [[Refrain] Kyoto Teramachi Sanjou no Holmes - 04 VOSTFR [720p]](https://uptobox.com/pse41zbr4bud)
- [[Refrain] Kyoto Teramachi Sanjou no Holmes - 04 VOSTFR [1080p]](https://uptobox.com/7g3ddhoy0lr1)
- [[Refrain] Kyoto Teramachi Sanjou no Holmes - 05 VOSTFR [720p]](https://uptobox.com/lolgqr91nl43)
- [[Refrain] Kyoto Teramachi Sanjou no Holmes - 05 VOSTFR [1080p]](https://uptobox.com/7p7tmymht2zg)
- [[Refrain] Kyoto Teramachi Sanjou no Holmes - 06 VOSTFR [720p]](https://uptobox.com/qptt4i9iamll)
- [[Refrain] Kyoto Teramachi Sanjou no Holmes - 06 VOSTFR [1080p]](https://uptobox.com/clgzmimnm75o)
- [[Refrain] Kyoto Teramachi Sanjou no Holmes - 07 VOSTFR [720p]](https://uptobox.com/ne3isiqcvn8o)
- [[Refrain] Kyoto Teramachi Sanjou no Holmes - 07 VOSTFR [1080p]](https://uptobox.com/l9klvn7cy3yd)
- [[Refrain] Kyoto Teramachi Sanjou no Holmes - 08 VOSTFR [720p]](https://uptobox.com/f6m7m8d7phup)
- [[Refrain] Kyoto Teramachi Sanjou no Holmes - 08 VOSTFR [1080p]](https://uptobox.com/n1ryuddijkos)
- [[Refrain] Kyoto Teramachi Sanjou no Holmes - 09 VOSTFR [720p]](https://uptobox.com/feqx7xmqvug0)
- [[Refrain] Kyoto Teramachi Sanjou no Holmes - 09 VOSTFR [1080p]](https://uptobox.com/x0us0osnmpsi)
- [[Refrain] Kyoto Teramachi Sanjou no Holmes - 10 VOSTFR [720p]](https://uptobox.com/6t0wmudq1a7x)
- [[Refrain] Kyoto Teramachi Sanjou no Holmes - 10 VOSTFR [1080p]](https://uptobox.com/lovu7wzval73)
- [[Refrain] Kyoto Teramachi Sanjou no Holmes - 11 VOSTFR [720p]](https://uptobox.com/7kd8t9lyraml)
- [[Refrain] Kyoto Teramachi Sanjou no Holmes - 11 VOSTFR [1080p]](https://uptobox.com/pykamtbqiwg8)
- [[Refrain] Kyoto Teramachi Sanjou no Holmes - 12 VOSTFR [720p]](https://uptobox.com/eung7119dk52)
- [[Refrain] Kyoto Teramachi Sanjou no Holmes - 12 VOSTFR [1080p]](https://uptobox.com/hyrp0quh7e22)
