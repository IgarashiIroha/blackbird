# Shichisei no Subaru

![Shichisei no Subaru](https://cdn.myanimelist.net/images/anime/1956/94914l.jpg)

* Japanese:  七星のスバル

## Information

 - Type: TV
 - Episodes: 12
 - Status: Finished Airing
 - Aired: Jul 6, 2018 to Sep 21, 2018
 - Premiered: Summer 2018
 - Broadcast: Fridays at 01:58 (JST)
 - Producers: TBS
 - Licensors: None found, add some
 - Studios: Lerche
 - Source: Light novel
 - Genres: Action, Fantasy, Game, Sci-Fi
 - Duration: 24 min. per ep.
 - Rating: PG-13 - Teens 13 or older


## Links

- [[Refrain] Shichisei no Subaru - 01 VOSTFR [720p]](https://uptobox.com/kjgbm6y6733y)
- [[Refrain] Shichisei no Subaru - 01 VOSTFR [1080p]](https://uptobox.com/38iomva2vhb0)
- [[Refrain] Shichisei no Subaru - 02 VOSTFR [720p]](https://uptobox.com/sjf1s1f3nz17)
- [[Refrain] Shichisei no Subaru - 02 VOSTFR [1080p]](https://uptobox.com/10ujhj1fg664)
- [[Refrain] Shichisei no Subaru - 03 VOSTFR [720p]](https://uptobox.com/9rlnwcmmjict)
- [[Refrain] Shichisei no Subaru - 03 VOSTFR [1080p]](https://uptobox.com/0hn9oq3stjyh)
- [[Refrain] Shichisei no Subaru - 04 VOSTFR [720p]](https://uptobox.com/05szcz14pz2m)
- [[Refrain] Shichisei no Subaru - 04 VOSTFR [1080p]](https://uptobox.com/scr9mww38req)
- [[Refrain] Shichisei no Subaru - 05 VOSTFR [720p]](https://uptobox.com/etz7t536zrf5)
- [[Refrain] Shichisei no Subaru - 05 VOSTFR [1080p]](https://uptobox.com/pd4xpjjdp769)
- [[Refrain] Shichisei no Subaru - 06 VOSTFR [720p]](https://uptobox.com/22fwk04td792)
- [[Refrain] Shichisei no Subaru - 06 VOSTFR [1080p]](https://uptobox.com/v4z6povoum04)
- [[Refrain] Shichisei no Subaru - 07 VOSTFR [720p]](https://uptobox.com/ucdc3h4qiw49)
- [[Refrain] Shichisei no Subaru - 07 VOSTFR [1080p]](https://uptobox.com/950cxyffbgwk)
- [[Refrain] Shichisei no Subaru - 08 VOSTFR [720p]](https://uptobox.com/ozsecuix4tc9)
- [[Refrain] Shichisei no Subaru - 08 VOSTFR [1080p]](https://uptobox.com/yi6vesclq7pl)
- [[Refrain] Shichisei no Subaru - 09 VOSTFR [720p]](https://uptobox.com/sysrvvn0iyfe)
- [[Refrain] Shichisei no Subaru - 09 VOSTFR [1080p]](https://uptobox.com/1fp1mye09i4j)
- [[Refrain] Shichisei no Subaru - 10 VOSTFR [720p]](https://uptobox.com/kp6famd9gdyo)
- [[Refrain] Shichisei no Subaru - 10 VOSTFR [1080p]](https://uptobox.com/y6syj7jayx7w)
- [[Refrain] Shichisei no Subaru - 11 VOSTFR [720p]](https://uptobox.com/mh8hfle9rk79)
- [[Refrain] Shichisei no Subaru - 11 VOSTFR [1080p]](https://uptobox.com/jzldg1l1fn4g)
- [[Refrain] Shichisei no Subaru - 12 VOSTFR [720p]](https://uptobox.com/xkiefq8h6klu)
- [[Refrain] Shichisei no Subaru - 12 VOSTFR [1080p]](https://uptobox.com/czkhuoekwc98)
