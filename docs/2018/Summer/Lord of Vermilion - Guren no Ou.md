# Lord of Vermilion: Guren no Ou

![Lord of Vermilion: Guren no Ou](https://cdn.myanimelist.net/images/anime/1557/95053l.jpg)

* Japanese:  ロード オブ ヴァーミリオン　紅蓮の王

## Information

 - Type: TV
 - Episodes: 12
 - Status: Finished Airing
 - Aired: Jul 14, 2018 to Sep 29, 2018
 - Premiered: Summer 2018
 - Broadcast: Saturdays at 01:10 (JST)
 - Producers: None found, add some
 - Licensors: Funimation
 - Studios: Asread
 - Source: Game
 - Genres: Action, Fantasy
 - Duration: 23 min. per ep.
 - Rating: None


## Links

- [[Refrain] Lord of Vermilion - Guren no Ou - 01 VOSTFR {ADN} [720p]](https://uptobox.com/o8hkl63496e5)
- [[Refrain] Lord of Vermilion - Guren no Ou - 01 VOSTFR {ADN} [1080p]](https://uptobox.com/6gxpxc1bvcqe)
- [[Refrain] Lord of Vermilion - Guren no Ou - 01 VOSTFR {CR} [720p]](https://uptobox.com/sn9e31c49sos)
- [[Refrain] Lord of Vermilion - Guren no Ou - 01 VOSTFR {CR} [1080p]](https://uptobox.com/kgisxwhto64y)
- [[Refrain] Lord of Vermilion - Guren no Ou - 02 VOSTFR {ADN} [720p]](https://uptobox.com/t67jvepl9k3i)
- [[Refrain] Lord of Vermilion - Guren no Ou - 02 VOSTFR {ADN} [1080p]](https://uptobox.com/bzso6bx6cbec)
- [[Refrain] Lord of Vermilion - Guren no Ou - 02 VOSTFR {CR} [720p]](https://uptobox.com/zehxa3bs21bs)
- [[Refrain] Lord of Vermilion - Guren no Ou - 02 VOSTFR {CR} [1080p]](https://uptobox.com/h1jl6ofdub6p)
- [[Refrain] Lord of Vermilion - Guren no Ou - 03 VOSTFR {ADN} [720p]](https://uptobox.com/2d6s0dnicobf)
- [[Refrain] Lord of Vermilion - Guren no Ou - 03 VOSTFR {ADN} [1080p]](https://uptobox.com/6cu04ebt6t17)
- [[Refrain] Lord of Vermilion - Guren no Ou - 03 VOSTFR {CR} [720p]](https://uptobox.com/07lptibymas7)
- [[Refrain] Lord of Vermilion - Guren no Ou - 03 VOSTFR {CR} [1080p]](https://uptobox.com/k9g7lpmep33g)
- [[Refrain] Lord of Vermilion - Guren no Ou - 04 VOSTFR {ADN} [720p]](https://uptobox.com/ja6g5ixfk7wi)
- [[Refrain] Lord of Vermilion - Guren no Ou - 04 VOSTFR {ADN} [1080p]](https://uptobox.com/i2osozyiic6i)
- [[Refrain] Lord of Vermilion - Guren no Ou - 04 VOSTFR {CR} [720p]](https://uptobox.com/6jnkf3qguz97)
- [[Refrain] Lord of Vermilion - Guren no Ou - 04 VOSTFR {CR} [1080p]](https://uptobox.com/env2e3d67wx1)
- [[Refrain] Lord of Vermilion - Guren no Ou - 05 VOSTFR {ADN} [720p]](https://uptobox.com/ua37ukf7jnil)
- [[Refrain] Lord of Vermilion - Guren no Ou - 05 VOSTFR {ADN} [1080p]](https://uptobox.com/xsn90gc9s70j)
- [[Refrain] Lord of Vermilion - Guren no Ou - 05 VOSTFR {CR} [720p]](https://uptobox.com/v1rpguxq955w)
- [[Refrain] Lord of Vermilion - Guren no Ou - 05 VOSTFR {CR} [1080p]](https://uptobox.com/0rkha8in9dqk)
- [[Refrain] Lord of Vermilion - Guren no Ou - 06 VOSTFR {ADN} [720p]](https://uptobox.com/cafc647mydju)
- [[Refrain] Lord of Vermilion - Guren no Ou - 06 VOSTFR {ADN} [1080p]](https://uptobox.com/qc0t6sb2gqi7)
- [[Refrain] Lord of Vermilion - Guren no Ou - 06 VOSTFR {CR} [720p]](https://uptobox.com/5d7csqxpq0kj)
- [[Refrain] Lord of Vermilion - Guren no Ou - 06 VOSTFR {CR} [1080p]](https://uptobox.com/hjur26sh1mm7)
- [[Refrain] Lord of Vermilion - Guren no Ou - 07 VOSTFR {ADN} [720p]](https://uptobox.com/mx4klvdfi2dc)
- [[Refrain] Lord of Vermilion - Guren no Ou - 07 VOSTFR {ADN} [1080p]](https://uptobox.com/23k528n3rrza)
- [[Refrain] Lord of Vermilion - Guren no Ou - 07 VOSTFR {CR} [720p]](https://uptobox.com/ttt85evehzei)
- [[Refrain] Lord of Vermilion - Guren no Ou - 07 VOSTFR {CR} [1080p]](https://uptobox.com/mz1e4h4eorve)
- [[Refrain] Lord of Vermilion - Guren no Ou - 08 VOSTFR {ADN} [720p]](https://uptobox.com/mk5kwd1nwq4u)
- [[Refrain] Lord of Vermilion - Guren no Ou - 08 VOSTFR {ADN} [1080p]](https://uptobox.com/nf032wb9zqtd)
- [[Refrain] Lord of Vermilion - Guren no Ou - 08 VOSTFR {CR} [720p]](https://uptobox.com/6lvjw54ug78c)
- [[Refrain] Lord of Vermilion - Guren no Ou - 08 VOSTFR {CR} [1080p]](https://uptobox.com/26vyc72e1bie)
- [[Refrain] Lord of Vermilion - Guren no Ou - 09 VOSTFR {ADN} [720p]](https://uptobox.com/pziyq8difq5f)
- [[Refrain] Lord of Vermilion - Guren no Ou - 09 VOSTFR {ADN} [1080p]](https://uptobox.com/i73um3d5neni)
- [[Refrain] Lord of Vermilion - Guren no Ou - 09 VOSTFR {CR} [720p]](https://uptobox.com/7mbqi4vxi26l)
- [[Refrain] Lord of Vermilion - Guren no Ou - 09 VOSTFR {CR} [1080p]](https://uptobox.com/io1ytef3fdwg)
- [[Refrain] Lord of Vermilion - Guren no Ou - 10 VOSTFR {ADN} [720p]](https://uptobox.com/u6urluyou0px)
- [[Refrain] Lord of Vermilion - Guren no Ou - 10 VOSTFR {ADN} [1080p]](https://uptobox.com/gmjie7jz2n0n)
- [[Refrain] Lord of Vermilion - Guren no Ou - 10 VOSTFR {CR} [720p]](https://uptobox.com/ouwdze1w7ur4)
- [[Refrain] Lord of Vermilion - Guren no Ou - 10 VOSTFR {CR} [1080p]](https://uptobox.com/10dlxkkt02p0)
- [[Refrain] Lord of Vermilion - Guren no Ou - 11 VOSTFR {ADN} [720p]](https://uptobox.com/v1u416jebnfj)
- [[Refrain] Lord of Vermilion - Guren no Ou - 11 VOSTFR {ADN} [1080p]](https://uptobox.com/yz8qnqsnv0oi)
- [[Refrain] Lord of Vermilion - Guren no Ou - 11 VOSTFR {CR} [720p]](https://uptobox.com/z2l6gxhx5uyv)
- [[Refrain] Lord of Vermilion - Guren no Ou - 11 VOSTFR {CR} [1080p]](https://uptobox.com/o94hfgw22dzs)
- [[Refrain] Lord of Vermilion - Guren no Ou - 12 VOSTFR {ADN} [720p]](https://uptobox.com/f7clh29mnkjf)
- [[Refrain] Lord of Vermilion - Guren no Ou - 12 VOSTFR {ADN} [1080p]](https://uptobox.com/30h3nk8230du)
- [[Refrain] Lord of Vermilion - Guren no Ou - 12 VOSTFR {CR} [720p]](https://uptobox.com/muh31gww440t)
- [[Refrain] Lord of Vermilion - Guren no Ou - 12 VOSTFR {CR} [1080p]](https://uptobox.com/gvmb3hwxewe5)
