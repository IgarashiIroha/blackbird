# Future Card Shin Buddyfight

![Future Card Shin Buddyfight](https://cdn.myanimelist.net/images/anime/1367/95055l.jpg)

* Japanese:  フューチャーカード 神バディファイト

## Information

 - Type: TV
 - Episodes: Unknown
 - Status: Currently Airing
 - Aired: Jun 2, 2018 to ?
 - Premiered: Summer 2018
 - Broadcast: Saturdays at 08:00 (JST)
 - Producers: None found, add some 
 - Licensors: None found, add some 
 - Studios: Xebec, OLM 
 - Source: Card game
 - Genres: Game
 - Duration: 24 min.
 - Rating: PG - Children


## Links

