# Dies Irae: To the Ring Reincarnation

![Dies Irae: To the Ring Reincarnation](https://cdn.myanimelist.net/images/anime/1244/93395l.jpg)

* Japanese:  Dies irae: To the ring reincarnation

## Information

 - Type: ONA
 - Episodes: 6
 - Status: Finished Airing
 - Aired: Jul 1, 2018
 - Producers: Genco, DMM pictures, Greenwood, My Theater D.D., Brave Hearts, Crunchyroll SC Anime Fund
 - Licensors: Funimation
 - Studios: A.C.G.T.
 - Source: Visual novel
 - Genres: Action, Super Power, Magic
 - Duration: 28 min. per ep.
 - Rating: R - 17+ (violence & profanity)


## Links

- [[Refrain] Dies Irae (ONA) - 12 VOSTFR [720p]](https://uptobox.com/gvj83cn2wv8l)
- [[Refrain] Dies Irae (ONA) - 12 VOSTFR [1080p]](https://uptobox.com/obw6gzu5sunr)
- [[Refrain] Dies Irae (ONA) - 13 VOSTFR [720p]](https://uptobox.com/yf9bmw3144cz)
- [[Refrain] Dies Irae (ONA) - 13 VOSTFR [1080p]](https://uptobox.com/kj9k59pdamwx)
- [[Refrain] Dies Irae (ONA) - 14 VOSTFR [720p]](https://uptobox.com/aiwqci0xvx73)
- [[Refrain] Dies Irae (ONA) - 14 VOSTFR [1080p]](https://uptobox.com/bx37xuwph2qm)
- [[Refrain] Dies Irae (ONA) - 15 VOSTFR [720p]](https://uptobox.com/5ip7wqq2l4lg)
- [[Refrain] Dies Irae (ONA) - 15 VOSTFR [1080p]](https://uptobox.com/12ux33m32pg9)
- [[Refrain] Dies Irae (ONA) - 16 VOSTFR [720p]](https://uptobox.com/yinlf0wkcs3s)
- [[Refrain] Dies Irae (ONA) - 16 VOSTFR [1080p]](https://uptobox.com/mzgkchsyz2wk)
- [[Refrain] Dies Irae (ONA) - 17 VOSTFR [720p]](https://uptobox.com/flw3sa3ljgeu)
- [[Refrain] Dies Irae (ONA) - 17 VOSTFR [1080p]](https://uptobox.com/yparj0zzynd0)
