# Sword Gai: The Animation Part II

![Sword Gai: The Animation Part II](https://cdn.myanimelist.net/images/anime/1805/91647l.jpg)

* Japanese:  ソードガイ The Animation Part II

## Information

 - Type: ONA
 - Episodes: 12
 - Status: Finished Airing
 - Aired: Jul 30, 2018
 - Producers: None found, add some
 - Licensors: None found, add some
 - Studios: Production I.G, DLE
 - Source: Manga
 - Genres: Action, Supernatural, Seinen
 - Duration: 22 min. per ep.
 - Rating: R - 17+ (violence & profanity)


## Links

- [[Refrain] Sword Gai The Animation Part II - 01 VF VOSTFR [720p]](https://uptobox.com/mrs42am9lcq9)
- [[Refrain] Sword Gai The Animation Part II - 01 VF VOSTFR [1080p]](https://uptobox.com/qm85l7jilsik)
- [[Refrain] Sword Gai The Animation Part II - 02 VF VOSTFR [720p]](https://uptobox.com/jyxw3rfyu0u5)
- [[Refrain] Sword Gai The Animation Part II - 02 VF VOSTFR [1080p]](https://uptobox.com/46iilno1lm5p)
- [[Refrain] Sword Gai The Animation Part II - 03 VF VOSTFR [720p]](https://uptobox.com/yh31jafwjace)
- [[Refrain] Sword Gai The Animation Part II - 03 VF VOSTFR [1080p]](https://uptobox.com/31ix8yi390zm)
- [[Refrain] Sword Gai The Animation Part II - 04 VF VOSTFR [720p]](https://uptobox.com/6q8itwtky6wz)
- [[Refrain] Sword Gai The Animation Part II - 04 VF VOSTFR [1080p]](https://uptobox.com/f77789nbgw05)
- [[Refrain] Sword Gai The Animation Part II - 05 VF VOSTFR [720p]](https://uptobox.com/lybt0cs4uzsp)
- [[Refrain] Sword Gai The Animation Part II - 05 VF VOSTFR [1080p]](https://uptobox.com/5lq7ns69r1kd)
- [[Refrain] Sword Gai The Animation Part II - 06 VF VOSTFR [720p]](https://uptobox.com/aykpo5r3gi2m)
- [[Refrain] Sword Gai The Animation Part II - 06 VF VOSTFR [1080p]](https://uptobox.com/kzn8slt5rv6y)
- [[Refrain] Sword Gai The Animation Part II - 07 VF VOSTFR [720p]](https://uptobox.com/y3fqaff2ngi9)
- [[Refrain] Sword Gai The Animation Part II - 07 VF VOSTFR [1080p]](https://uptobox.com/3kj3u5el4d1a)
- [[Refrain] Sword Gai The Animation Part II - 08 VF VOSTFR [720p]](https://uptobox.com/u0gqiueu4mig)
- [[Refrain] Sword Gai The Animation Part II - 08 VF VOSTFR [1080p]](https://uptobox.com/pf9phg0vp2tf)
- [[Refrain] Sword Gai The Animation Part II - 09 VF VOSTFR [720p]](https://uptobox.com/eo390lbycd42)
- [[Refrain] Sword Gai The Animation Part II - 09 VF VOSTFR [1080p]](https://uptobox.com/v6tidaeha0ha)
- [[Refrain] Sword Gai The Animation Part II - 10 VF VOSTFR [720p]](https://uptobox.com/sio0xa3ii9x4)
- [[Refrain] Sword Gai The Animation Part II - 10 VF VOSTFR [1080p]](https://uptobox.com/ftnl8famd3oq)
- [[Refrain] Sword Gai The Animation Part II - 11 VF VOSTFR [720p]](https://uptobox.com/6rpu16iklpho)
- [[Refrain] Sword Gai The Animation Part II - 11 VF VOSTFR [1080p]](https://uptobox.com/k0ilhnw6c99m)
- [[Refrain] Sword Gai The Animation Part II - 12 VF VOSTFR [720p]](https://uptobox.com/lxtju9ghqs16)
- [[Refrain] Sword Gai The Animation Part II - 12 VF VOSTFR [1080p]](https://uptobox.com/aceah50wd5rr)
