# Hataraku Onii-san! no 2!

![Hataraku Onii-san! no 2!](https://cdn.myanimelist.net/images/anime/1250/95044l.jpg)

* Japanese:  働くお兄さん！の2！

## Information

 - Type: TV
 - Episodes: 11
 - Status: Finished Airing
 - Aired: Jul 6, 2018 to Sep 21, 2018
 - Premiered: Summer 2018
 - Broadcast: Fridays at 21:54 (JST)
 - Producers: Lantis, Movic, Half H.P Studio, BS Fuji, Tokyo MX, Good Smile Company, Bandai Namco Arts 
 - Licensors: None found, add some 
 - Studios: Fanworks, Tomovies 
 - Source: Original
 - Genres: Comedy, Slice of Life
 - Duration: 4 min. per ep.
 - Rating: G - All Ages


## Links

