# Back Street Girls: Gokudolls

![Back Street Girls: Gokudolls](https://cdn.myanimelist.net/images/anime/1484/93140l.jpg)

* Japanese:  Back Street Girls -ゴクドルズ

## Information

 - Type: TV
 - Episodes: 10
 - Status: Finished Airing
 - Aired: Jul 4, 2018 to Sep 5, 2018
 - Premiered: Summer 2018
 - Broadcast: Wednesdays at 01:00 (JST)
 - Producers: None found, add some
 - Licensors: None found, add some
 - Studios: J.C.Staff
 - Source: Manga
 - Genres: Comedy, Ecchi, Seinen
 - Duration: 24 min. per ep.
 - Rating: R - 17+ (violence & profanity)


## Links

- [[Refrain] Back Street Girls - Gokudolls - 01 VF VOSTFR [720p]](https://uptobox.com/cp2lv772ug68)
- [[Refrain] Back Street Girls - Gokudolls - 01 VF VOSTFR [1080p]](https://uptobox.com/3fqtts53nzje)
- [[Refrain] Back Street Girls - Gokudolls - 01 VOSTFR [720p]](https://uptobox.com/kia0b1fdplh7)
- [[Refrain] Back Street Girls - Gokudolls - 01 VOSTFR [1080p]](https://uptobox.com/ax9t66u0vb5x)
- [[Refrain] Back Street Girls - Gokudolls - 02 VF VOSTFR [720p]](https://uptobox.com/nmz6m0ffhnb9)
- [[Refrain] Back Street Girls - Gokudolls - 02 VF VOSTFR [1080p]](https://uptobox.com/7lf9ktyyidvh)
- [[Refrain] Back Street Girls - Gokudolls - 02 VOSTFR [720p]](https://uptobox.com/y2gmrhqp7jld)
- [[Refrain] Back Street Girls - Gokudolls - 02 VOSTFR [1080p]](https://uptobox.com/tjwipdy5owsw)
- [[Refrain] Back Street Girls - Gokudolls - 03 VF VOSTFR [720p]](https://uptobox.com/5rhiqjqlw63h)
- [[Refrain] Back Street Girls - Gokudolls - 03 VF VOSTFR [1080p]](https://uptobox.com/lznabaeyic09)
- [[Refrain] Back Street Girls - Gokudolls - 03 VOSTFR [720p]](https://uptobox.com/wv60gc7lper6)
- [[Refrain] Back Street Girls - Gokudolls - 03 VOSTFR [1080p]](https://uptobox.com/l1u64sqj7bz1)
- [[Refrain] Back Street Girls - Gokudolls - 04 VF VOSTFR [720p]](https://uptobox.com/7rz5gj6042l9)
- [[Refrain] Back Street Girls - Gokudolls - 04 VF VOSTFR [1080p]](https://uptobox.com/xbh5uplgn76m)
- [[Refrain] Back Street Girls - Gokudolls - 04 VOSTFR [720p]](https://uptobox.com/rlbnpuqxp43r)
- [[Refrain] Back Street Girls - Gokudolls - 04 VOSTFR [1080p]](https://uptobox.com/g1f4a7j3x51c)
- [[Refrain] Back Street Girls - Gokudolls - 05 VF VOSTFR [720p]](https://uptobox.com/px7hh5mc7i6k)
- [[Refrain] Back Street Girls - Gokudolls - 05 VF VOSTFR [1080p]](https://uptobox.com/kxbvu6qolxqp)
- [[Refrain] Back Street Girls - Gokudolls - 05 VOSTFR [720p]](https://uptobox.com/bzqm2ln6kkmg)
- [[Refrain] Back Street Girls - Gokudolls - 05 VOSTFR [1080p]](https://uptobox.com/yklxjc53bxda)
- [[Refrain] Back Street Girls - Gokudolls - 06 VF VOSTFR [720p]](https://uptobox.com/zxzpoxovx1g2)
- [[Refrain] Back Street Girls - Gokudolls - 06 VF VOSTFR [1080p]](https://uptobox.com/7cm0qcm425x4)
- [[Refrain] Back Street Girls - Gokudolls - 06 VOSTFR [720p]](https://uptobox.com/qz6rt1l5bqji)
- [[Refrain] Back Street Girls - Gokudolls - 06 VOSTFR [1080p]](https://uptobox.com/wpnk3kl6q1tl)
- [[Refrain] Back Street Girls - Gokudolls - 07 VF VOSTFR [720p]](https://uptobox.com/y5r1iag1c9de)
- [[Refrain] Back Street Girls - Gokudolls - 07 VF VOSTFR [1080p]](https://uptobox.com/yrlax8pqsm7t)
- [[Refrain] Back Street Girls - Gokudolls - 07 VOSTFR [720p]](https://uptobox.com/pjsa97mvlc3z)
- [[Refrain] Back Street Girls - Gokudolls - 07 VOSTFR [1080p]](https://uptobox.com/q8v66ccv9hws)
- [[Refrain] Back Street Girls - Gokudolls - 08 VF VOSTFR [720p]](https://uptobox.com/8qkdlqxo13zn)
- [[Refrain] Back Street Girls - Gokudolls - 08 VF VOSTFR [1080p]](https://uptobox.com/9prtaar0jcwt)
- [[Refrain] Back Street Girls - Gokudolls - 08 VOSTFR [720p]](https://uptobox.com/mapszr8m37jv)
- [[Refrain] Back Street Girls - Gokudolls - 08 VOSTFR [1080p]](https://uptobox.com/lmsqab423zv8)
- [[Refrain] Back Street Girls - Gokudolls - 09 VF VOSTFR [720p]](https://uptobox.com/elo0keug0pic)
- [[Refrain] Back Street Girls - Gokudolls - 09 VF VOSTFR [1080p]](https://uptobox.com/5el6411rmq3w)
- [[Refrain] Back Street Girls - Gokudolls - 09 VOSTFR [720p]](https://uptobox.com/qtzqfl0p1g6j)
- [[Refrain] Back Street Girls - Gokudolls - 09 VOSTFR [1080p]](https://uptobox.com/u4gepk32ldyq)
- [[Refrain] Back Street Girls - Gokudolls - 10 VF VOSTFR [720p]](https://uptobox.com/mmit9hemnzvk)
- [[Refrain] Back Street Girls - Gokudolls - 10 VF VOSTFR [1080p]](https://uptobox.com/ykxvdt0lcr37)
- [[Refrain] Back Street Girls - Gokudolls - 10 VOSTFR [720p]](https://uptobox.com/mzkk2mq5msvp)
- [[Refrain] Back Street Girls - Gokudolls - 10 VOSTFR [1080p]](https://uptobox.com/g3a7q1t30fem)
