# Hyakuren no Haou to Seiyaku no Valkyria

![Hyakuren no Haou to Seiyaku no Valkyria](https://cdn.myanimelist.net/images/anime/1585/95225l.jpg)

* Japanese:  百錬の覇王と聖約の戦乙女〈ヴァルキュリア〉

## Information

 - Type: TV
 - Episodes: 12
 - Status: Finished Airing
 - Aired: Jul 8, 2018 to Sep 23, 2018
 - Premiered: Summer 2018
 - Broadcast: Sundays at 01:30 (JST)
 - Producers: Crunchyroll SC Anime Fund
 - Licensors: Funimation
 - Studios: EMT²
 - Source: Light novel
 - Genres: Fantasy, Harem
 - Duration: 23 min. per ep.
 - Rating: None


## Links

- [[Refrain] Hyakuren no Haou to Seiyaku no Valkyria - 01 VOSTFR [720p]](https://uptobox.com/7d5d73pn4f0h)
- [[Refrain] Hyakuren no Haou to Seiyaku no Valkyria - 01 VOSTFR [1080p]](https://uptobox.com/xpivt6scvakz)
- [[Refrain] Hyakuren no Haou to Seiyaku no Valkyria - 02 VOSTFR [720p]](https://uptobox.com/qjbae06i95u0)
- [[Refrain] Hyakuren no Haou to Seiyaku no Valkyria - 02 VOSTFR [1080p]](https://uptobox.com/i0amvpb58wxu)
- [[Refrain] Hyakuren no Haou to Seiyaku no Valkyria - 03 VOSTFR [720p]](https://uptobox.com/u7uqgr03uuuc)
- [[Refrain] Hyakuren no Haou to Seiyaku no Valkyria - 03 VOSTFR [1080p]](https://uptobox.com/h50hy0wtr36d)
- [[Refrain] Hyakuren no Haou to Seiyaku no Valkyria - 04 VOSTFR [720p]](https://uptobox.com/3g4fo5pdplnb)
- [[Refrain] Hyakuren no Haou to Seiyaku no Valkyria - 04 VOSTFR [1080p]](https://uptobox.com/05onqhjpri0m)
- [[Refrain] Hyakuren no Haou to Seiyaku no Valkyria - 05 VOSTFR [720p]](https://uptobox.com/1xrcs5bq0dp1)
- [[Refrain] Hyakuren no Haou to Seiyaku no Valkyria - 05 VOSTFR [1080p]](https://uptobox.com/q7m1hoapaugm)
- [[Refrain] Hyakuren no Haou to Seiyaku no Valkyria - 06 VOSTFR [720p]](https://uptobox.com/3tqaq7iza0w6)
- [[Refrain] Hyakuren no Haou to Seiyaku no Valkyria - 06 VOSTFR [1080p]](https://uptobox.com/j0jbsnr1d6vb)
- [[Refrain] Hyakuren no Haou to Seiyaku no Valkyria - 07 VOSTFR [720p]](https://uptobox.com/792x4a0fvxw2)
- [[Refrain] Hyakuren no Haou to Seiyaku no Valkyria - 07 VOSTFR [1080p]](https://uptobox.com/71j7ahxrcogv)
- [[Refrain] Hyakuren no Haou to Seiyaku no Valkyria - 08 VOSTFR [720p]](https://uptobox.com/pvfuyj0penqq)
- [[Refrain] Hyakuren no Haou to Seiyaku no Valkyria - 08 VOSTFR [1080p]](https://uptobox.com/fgvklouesy9a)
- [[Refrain] Hyakuren no Haou to Seiyaku no Valkyria - 09 VOSTFR [720p]](https://uptobox.com/am0qijvvrtgd)
- [[Refrain] Hyakuren no Haou to Seiyaku no Valkyria - 09 VOSTFR [1080p]](https://uptobox.com/qg1yg1t8mdad)
- [[Refrain] Hyakuren no Haou to Seiyaku no Valkyria - 10 VOSTFR [720p]](https://uptobox.com/z1mylm3l0ji8)
- [[Refrain] Hyakuren no Haou to Seiyaku no Valkyria - 10 VOSTFR [1080p]](https://uptobox.com/tkb21xcwbaxb)
- [[Refrain] Hyakuren no Haou to Seiyaku no Valkyria - 11 VOSTFR [720p]](https://uptobox.com/r4ax44122hn3)
- [[Refrain] Hyakuren no Haou to Seiyaku no Valkyria - 11 VOSTFR [1080p]](https://uptobox.com/x8krekleckmr)
- [[Refrain] Hyakuren no Haou to Seiyaku no Valkyria - 12 VOSTFR [720p]](https://uptobox.com/9128vw6sekeo)
- [[Refrain] Hyakuren no Haou to Seiyaku no Valkyria - 12 VOSTFR [1080p]](https://uptobox.com/698mzbr769qo)
