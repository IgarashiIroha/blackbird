# Happy Sugar Life

![Happy Sugar Life](https://cdn.myanimelist.net/images/anime/1086/91313l.jpg)

* Japanese:  ハッピーシュガーライフ

## Information

 - Type: TV
 - Episodes: 12
 - Status: Finished Airing
 - Aired: Jul 14, 2018 to Sep 29, 2018
 - Premiered: Summer 2018
 - Broadcast: Saturdays at 01:55 (JST)
 - Producers: VAP, Square Enix, Genco, Mainichi Broadcasting System, Bit Promotion
 - Licensors: None found, add some
 - Studios: Ezόla
 - Source: Manga
 - Genres: Drama, Horror, Psychological, Shounen
 - Duration: 24 min. per ep.
 - Rating: R - 17+ (violence & profanity)


## Links

- [[Refrain] Happy Sugar Life - 01 VOSTFR [720p]](https://uptobox.com/ur57gxvgtdr7)
- [[Refrain] Happy Sugar Life - 01 VOSTFR [1080p]](https://uptobox.com/pik8oek58rym)
- [[Refrain] Happy Sugar Life - 02 VOSTFR [720p]](https://uptobox.com/kxqq8xk2a8lb)
- [[Refrain] Happy Sugar Life - 02 VOSTFR [1080p]](https://uptobox.com/yjyu3fi04683)
- [[Refrain] Happy Sugar Life - 03 VOSTFR [720p]](https://uptobox.com/g6qz65whkowa)
- [[Refrain] Happy Sugar Life - 03 VOSTFR [1080p]](https://uptobox.com/52qtbdora748)
- [[Refrain] Happy Sugar Life - 04 VOSTFR [720p]](https://uptobox.com/eeoq503izpng)
- [[Refrain] Happy Sugar Life - 04 VOSTFR [1080p]](https://uptobox.com/dufwhxlg0qdo)
- [[Refrain] Happy Sugar Life - 05 VOSTFR [720p]](https://uptobox.com/9vauv2ktktud)
- [[Refrain] Happy Sugar Life - 05 VOSTFR [1080p]](https://uptobox.com/vmct3r5h6549)
- [[Refrain] Happy Sugar Life - 06 VOSTFR [720p]](https://uptobox.com/vz39qporoo8f)
- [[Refrain] Happy Sugar Life - 06 VOSTFR [1080p]](https://uptobox.com/vo8n38owzwcz)
- [[Refrain] Happy Sugar Life - 07 VOSTFR [720p]](https://uptobox.com/u2yblqkxkiw9)
- [[Refrain] Happy Sugar Life - 07 VOSTFR [1080p]](https://uptobox.com/3ol3yqxzoinz)
- [[Refrain] Happy Sugar Life - 08 VOSTFR [720p]](https://uptobox.com/f0rtigf6j9jd)
- [[Refrain] Happy Sugar Life - 08 VOSTFR [1080p]](https://uptobox.com/stcjutb47fv1)
- [[Refrain] Happy Sugar Life - 09 VOSTFR [720p]](https://uptobox.com/r5brtp80lwxk)
- [[Refrain] Happy Sugar Life - 09 VOSTFR [1080p]](https://uptobox.com/v5bvg39dxef0)
- [[Refrain] Happy Sugar Life - 10 VOSTFR [720p]](https://uptobox.com/cgso9ydnqddg)
- [[Refrain] Happy Sugar Life - 10 VOSTFR [1080p]](https://uptobox.com/ujcwm3m0qchx)
- [[Refrain] Happy Sugar Life - 11 VOSTFR [720p]](https://uptobox.com/2jouo1y3a1mx)
- [[Refrain] Happy Sugar Life - 11 VOSTFR [1080p]](https://uptobox.com/zpqr89454czr)
- [[Refrain] Happy Sugar Life - 12 VOSTFR [720p]](https://uptobox.com/vh6dfbb1epze)
- [[Refrain] Happy Sugar Life - 12 VOSTFR [1080p]](https://uptobox.com/badqahcz54rq)
