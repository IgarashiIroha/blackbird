# Shingeki no Kyojin Season 3

![Shingeki no Kyojin Season 3](https://cdn.myanimelist.net/images/anime/1173/92110l.jpg)

* Japanese:  進撃の巨人 Season3

## Information

 - Type: TV
 - Episodes: 12
 - Status: Finished Airing
 - Aired: Jul 23, 2018 to Oct 15, 2018
 - Premiered: Summer 2018
 - Broadcast: Mondays at 00:35 (JST)
 - Producers: Production I.G, Dentsu, NHK, Mainichi Broadcasting System, Pony Canyon, Kodansha, Techno Sound, Pony Canyon Enterprise 
 - Licensors: Funimation 
 - Studios: Wit Studio 
 - Source: Manga
 - Genres: Action, Drama, Fantasy, Military, Mystery, Shounen, Super Power
 - Duration: 23 min. per ep.
 - Rating: R - 17+ (violence & profanity)


## Links

