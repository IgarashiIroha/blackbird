# Gintama.: Shirogane no Tamashii-hen - Kouhan-sen

![Gintama.: Shirogane no Tamashii-hen - Kouhan-sen](https://cdn.myanimelist.net/images/anime/1776/96566l.jpg)

* Japanese:  銀魂. 銀ノ魂篇 後半戦

## Information

 - Type: TV
 - Episodes: 14
 - Status: Finished Airing
 - Aired: Jul 9, 2018 to Oct 8, 2018
 - Premiered: Summer 2018
 - Broadcast: Mondays at 01:35 (JST)
 - Producers: None found, add some
 - Licensors: None found, add some
 - Studios: Bandai Namco Pictures
 - Source: Manga
 - Genres: Action, Comedy, Historical, Parody, Samurai, Sci-Fi, Shounen
 - Duration: 24 min. per ep.
 - Rating: PG-13 - Teens 13 or older


## Links

- [[Refrain] Gintama. - Shirogane no Tamashii-hen 2 - 354 VOSTFR [720p]](https://uptobox.com/pzzkye41g0pr)
- [[Refrain] Gintama. - Shirogane no Tamashii-hen 2 - 354 VOSTFR [1080p]](https://uptobox.com/5uzmybqzd2hd)
- [[Refrain] Gintama. - Shirogane no Tamashii-hen 2 - 355 VOSTFR [720p]](https://uptobox.com/n59iomu2vb0m)
- [[Refrain] Gintama. - Shirogane no Tamashii-hen 2 - 355 VOSTFR [1080p]](https://uptobox.com/69a2mb58k7r2)
- [[Refrain] Gintama. - Shirogane no Tamashii-hen 2 - 356 VOSTFR [720p]](https://uptobox.com/k23rna6ss35q)
- [[Refrain] Gintama. - Shirogane no Tamashii-hen 2 - 356 VOSTFR [1080p]](https://uptobox.com/mnanzcjiaq2k)
- [[Refrain] Gintama. - Shirogane no Tamashii-hen 2 - 357 VOSTFR [720p]](https://uptobox.com/pqn2r70jeh6h)
- [[Refrain] Gintama. - Shirogane no Tamashii-hen 2 - 357 VOSTFR [1080p]](https://uptobox.com/wrjy7bu21h5e)
- [[Refrain] Gintama. - Shirogane no Tamashii-hen 2 - 358 VOSTFR [720p]](https://uptobox.com/8xznyic9hhzx)
- [[Refrain] Gintama. - Shirogane no Tamashii-hen 2 - 358 VOSTFR [1080p]](https://uptobox.com/uvq088ysj8zb)
- [[Refrain] Gintama. - Shirogane no Tamashii-hen 2 - 359 VOSTFR [720p]](https://uptobox.com/d092z7xht407)
- [[Refrain] Gintama. - Shirogane no Tamashii-hen 2 - 359 VOSTFR [1080p]](https://uptobox.com/mv30xy97ol3f)
- [[Refrain] Gintama. - Shirogane no Tamashii-hen 2 - 360 VOSTFR [720p]](https://uptobox.com/mo4wrn3fk654)
- [[Refrain] Gintama. - Shirogane no Tamashii-hen 2 - 360 VOSTFR [1080p]](https://uptobox.com/4p4r52z4tf22)
- [[Refrain] Gintama. - Shirogane no Tamashii-hen 2 - 361 VOSTFR [720p]](https://uptobox.com/ypm2wu4kacu9)
- [[Refrain] Gintama. - Shirogane no Tamashii-hen 2 - 361 VOSTFR [1080p]](https://uptobox.com/3booc3rlntik)
- [[Refrain] Gintama. - Shirogane no Tamashii-hen 2 - 362 VOSTFR [720p]](https://uptobox.com/hliu8uuw4nfy)
- [[Refrain] Gintama. - Shirogane no Tamashii-hen 2 - 362 VOSTFR [1080p]](https://uptobox.com/2i23hrdg1yjf)
- [[Refrain] Gintama. - Shirogane no Tamashii-hen 2 - 363 VOSTFR [720p]](https://uptobox.com/7959wzay42vo)
- [[Refrain] Gintama. - Shirogane no Tamashii-hen 2 - 363 VOSTFR [1080p]](https://uptobox.com/mnxjf94rz1l5)
- [[Refrain] Gintama. - Shirogane no Tamashii-hen 2 - 364 VOSTFR [720p]](https://uptobox.com/tpiinzzq1w9s)
- [[Refrain] Gintama. - Shirogane no Tamashii-hen 2 - 364 VOSTFR [1080p]](https://uptobox.com/79jeq2nrgoo9)
- [[Refrain] Gintama. - Shirogane no Tamashii-hen 2 - 365 VOSTFR [720p]](https://uptobox.com/xaqz27cdx21x)
- [[Refrain] Gintama. - Shirogane no Tamashii-hen 2 - 365 VOSTFR [1080p]](https://uptobox.com/kq8hkybo1qry)
- [[Refrain] Gintama. - Shirogane no Tamashii-hen 2 - 366 VOSTFR [720p]](https://uptobox.com/mciwapp521wh)
- [[Refrain] Gintama. - Shirogane no Tamashii-hen 2 - 366 VOSTFR [1080p]](https://uptobox.com/310u6dr86xag)
- [[Refrain] Gintama. - Shirogane no Tamashii-hen 2 - 367 VOSTFR [720p]](https://uptobox.com/lg2jmyad3pyv)
- [[Refrain] Gintama. - Shirogane no Tamashii-hen 2 - 367 VOSTFR [1080p]](https://uptobox.com/hlc9kuh47fq1)
