# Muhyo to Rouji no Mahouritsu Soudan Jimusho

![Muhyo to Rouji no Mahouritsu Soudan Jimusho](https://cdn.myanimelist.net/images/anime/1180/93407l.jpg)

* Japanese:  ムヒョとロージーの魔法律相談事務所

## Information

 - Type: TV
 - Episodes: 12
 - Status: Finished Airing
 - Aired: Aug 3, 2018 to Oct 19, 2018
 - Premiered: Summer 2018
 - Broadcast: Fridays at 21:30 (JST)
 - Producers: Sony Pictures Entertainment, Lantis, Animax, Shueisha, JY Animation, Bandai Namco Arts, SKY PerfecTV!
 - Licensors: None found, add some
 - Studios: Studio Deen
 - Source: Manga
 - Genres: Action, Comedy, Mystery, Shounen, Supernatural
 - Duration: 23 min. per ep.
 - Rating: PG-13 - Teens 13 or older


## Links

- [[Refrain] Muhyo to Rouji no Mahouritsu Soudan Jimusho - 01 VOSTFR [720p]](https://uptobox.com/dzjn8383nzie)
- [[Refrain] Muhyo to Rouji no Mahouritsu Soudan Jimusho - 01 VOSTFR [1080p]](https://uptobox.com/kcorixxs5bjc)
- [[Refrain] Muhyo to Rouji no Mahouritsu Soudan Jimusho - 02 VOSTFR [720p]](https://uptobox.com/x6caqbyl48er)
- [[Refrain] Muhyo to Rouji no Mahouritsu Soudan Jimusho - 02 VOSTFR [1080p]](https://uptobox.com/0dad0p5hz0b7)
- [[Refrain] Muhyo to Rouji no Mahouritsu Soudan Jimusho - 03 VOSTFR [720p]](https://uptobox.com/cp3g1nu2xv3l)
- [[Refrain] Muhyo to Rouji no Mahouritsu Soudan Jimusho - 03 VOSTFR [1080p]](https://uptobox.com/77ysxwbhqmqj)
- [[Refrain] Muhyo to Rouji no Mahouritsu Soudan Jimusho - 04 VOSTFR [720p]](https://uptobox.com/lr6z3je298aw)
- [[Refrain] Muhyo to Rouji no Mahouritsu Soudan Jimusho - 04 VOSTFR [1080p]](https://uptobox.com/to4ojvper1t9)
- [[Refrain] Muhyo to Rouji no Mahouritsu Soudan Jimusho - 05 VOSTFR [720p]](https://uptobox.com/ntg7jmc4wuvs)
- [[Refrain] Muhyo to Rouji no Mahouritsu Soudan Jimusho - 05 VOSTFR [1080p]](https://uptobox.com/cio9gymph8rt)
- [[Refrain] Muhyo to Rouji no Mahouritsu Soudan Jimusho - 06 VOSTFR [720p]](https://uptobox.com/r844xfr3j1wm)
- [[Refrain] Muhyo to Rouji no Mahouritsu Soudan Jimusho - 06 VOSTFR [1080p]](https://uptobox.com/31a5smhsa2q2)
- [[Refrain] Muhyo to Rouji no Mahouritsu Soudan Jimusho - 07 VOSTFR [720p]](https://uptobox.com/khbaw4u7gqrs)
- [[Refrain] Muhyo to Rouji no Mahouritsu Soudan Jimusho - 07 VOSTFR [1080p]](https://uptobox.com/xstsa9o0xywp)
- [[Refrain] Muhyo to Rouji no Mahouritsu Soudan Jimusho - 08 VOSTFR [720p]](https://uptobox.com/tag90hs9qsqa)
- [[Refrain] Muhyo to Rouji no Mahouritsu Soudan Jimusho - 08 VOSTFR [1080p]](https://uptobox.com/1zmgvkg7ru83)
- [[Refrain] Muhyo to Rouji no Mahouritsu Soudan Jimusho - 09 VOSTFR [720p]](https://uptobox.com/zdz3hth2puym)
- [[Refrain] Muhyo to Rouji no Mahouritsu Soudan Jimusho - 09 VOSTFR [1080p]](https://uptobox.com/iq7aj7eoxnd3)
- [[Refrain] Muhyo to Rouji no Mahouritsu Soudan Jimusho - 10 VOSTFR [720p]](https://uptobox.com/w6yzf0py3dr8)
- [[Refrain] Muhyo to Rouji no Mahouritsu Soudan Jimusho - 10 VOSTFR [1080p]](https://uptobox.com/py986p196die)
- [[Refrain] Muhyo to Rouji no Mahouritsu Soudan Jimusho - 11 VOSTFR [720p]](https://uptobox.com/ct1no5g89jry)
- [[Refrain] Muhyo to Rouji no Mahouritsu Soudan Jimusho - 11 VOSTFR [1080p]](https://uptobox.com/serqlv1tg238)
- [[Refrain] Muhyo to Rouji no Mahouritsu Soudan Jimusho - 12 VOSTFR [720p]](https://uptobox.com/ayusr29aaidu)
- [[Refrain] Muhyo to Rouji no Mahouritsu Soudan Jimusho - 12 VOSTFR [1080p]](https://uptobox.com/b5yw0yzyrpgq)
