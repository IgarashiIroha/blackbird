# Free!: Dive to the Future

![Free!: Dive to the Future](https://cdn.myanimelist.net/images/anime/1243/95025l.jpg)

* Japanese:  Free!-Dive to the Future-

## Information

 - Type: TV
 - Episodes: 12
 - Status: Finished Airing
 - Aired: Jul 12, 2018 to Sep 27, 2018
 - Premiered: Summer 2018
 - Broadcast: Thursdays at 00:00 (JST)
 - Producers: None found, add some
 - Licensors: Funimation
 - Studios: Kyoto Animation, Animation Do
 - Source: Original
 - Genres: Comedy, Drama, School, Slice of Life, Sports
 - Duration: 23 min. per ep.
 - Rating: PG-13 - Teens 13 or older


## Links

- [[Refrain] Free! Dive to the Future - 01 VOSTFR [720p]](https://uptobox.com/l98fq5stx87f)
- [[Refrain] Free! Dive to the Future - 01 VOSTFR [1080p]](https://uptobox.com/2yt47dc4nju9)
- [[Refrain] Free! Dive to the Future - 02 VOSTFR [720p]](https://uptobox.com/vu8hcklo356f)
- [[Refrain] Free! Dive to the Future - 02 VOSTFR [1080p]](https://uptobox.com/0qe7v2f8yfrj)
- [[Refrain] Free! Dive to the Future - 03 VOSTFR [720p]](https://uptobox.com/a1w0qdhu0m0v)
- [[Refrain] Free! Dive to the Future - 03 VOSTFR [1080p]](https://uptobox.com/b1zg20yx2zf6)
- [[Refrain] Free! Dive to the Future - 04 VOSTFR [720p]](https://uptobox.com/wykw71uhv69a)
- [[Refrain] Free! Dive to the Future - 04 VOSTFR [1080p]](https://uptobox.com/krclnqib9dv2)
- [[Refrain] Free! Dive to the Future - 05 VOSTFR [720p]](https://uptobox.com/qa3997jgqs10)
- [[Refrain] Free! Dive to the Future - 05 VOSTFR [1080p]](https://uptobox.com/lkxof0c3nxis)
- [[Refrain] Free! Dive to the Future - 06 VOSTFR [720p]](https://uptobox.com/nu6jzgmziczk)
- [[Refrain] Free! Dive to the Future - 06 VOSTFR [1080p]](https://uptobox.com/2v714zd9egh4)
- [[Refrain] Free! Dive to the Future - 07 VOSTFR [720p]](https://uptobox.com/o7efkbww036a)
- [[Refrain] Free! Dive to the Future - 07 VOSTFR [1080p]](https://uptobox.com/bo1aiso8fbf4)
- [[Refrain] Free! Dive to the Future - 08 VOSTFR [720p]](https://uptobox.com/x4gfvvp1rvy6)
- [[Refrain] Free! Dive to the Future - 08 VOSTFR [1080p]](https://uptobox.com/pjohai9ltq7n)
- [[Refrain] Free! Dive to the Future - 09 VOSTFR [720p]](https://uptobox.com/yhqev6pn0k71)
- [[Refrain] Free! Dive to the Future - 09 VOSTFR [1080p]](https://uptobox.com/khhxh0m7gnrv)
- [[Refrain] Free! Dive to the Future - 10 VOSTFR [720p]](https://uptobox.com/0eumatu0adi5)
- [[Refrain] Free! Dive to the Future - 10 VOSTFR [1080p]](https://uptobox.com/n9jo8a40hqfq)
- [[Refrain] Free! Dive to the Future - 11 VOSTFR [720p]](https://uptobox.com/j13ggsf5x8fo)
- [[Refrain] Free! Dive to the Future - 11 VOSTFR [1080p]](https://uptobox.com/nx3ddfbqk5c4)
- [[Refrain] Free! Dive to the Future - 12 VOSTFR [720p]](https://uptobox.com/bqgfg6btg5mm)
- [[Refrain] Free! Dive to the Future - 12 VOSTFR [1080p]](https://uptobox.com/hld7pr6d0c6c)
