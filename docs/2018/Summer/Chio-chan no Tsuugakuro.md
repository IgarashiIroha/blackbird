# Chio-chan no Tsuugakuro

![Chio-chan no Tsuugakuro](https://cdn.myanimelist.net/images/anime/1073/95042l.jpg)

* Japanese:  ちおちゃんの通学路

## Information

 - Type: TV
 - Episodes: 12
 - Status: Finished Airing
 - Aired: Jul 6, 2018 to Sep 21, 2018
 - Premiered: Summer 2018
 - Broadcast: Fridays at 22:00 (JST)
 - Producers: None found, add some
 - Licensors: Funimation
 - Studios: Diomedea
 - Source: Manga
 - Genres: Comedy, Seinen
 - Duration: 23 min. per ep.
 - Rating: PG-13 - Teens 13 or older


## Links

- [[Refrain] Chio-chan no Tsuugakuro - 01 VOSTFR {ADN} [720p]](https://uptobox.com/smicjkjfdj86)
- [[Refrain] Chio-chan no Tsuugakuro - 01 VOSTFR {ADN} [1080p]](https://uptobox.com/1vntvmyvjkba)
- [[Refrain] Chio-chan no Tsuugakuro - 01 VOSTFR {CR} [720p]](https://uptobox.com/114mnhcetkjg)
- [[Refrain] Chio-chan no Tsuugakuro - 01 VOSTFR {CR} [1080p]](https://uptobox.com/w70d1bw4duju)
- [[Refrain] Chio-chan no Tsuugakuro - 02 VOSTFR {ADN} [720p]](https://uptobox.com/t011di7egglp)
- [[Refrain] Chio-chan no Tsuugakuro - 02 VOSTFR {ADN} [1080p]](https://uptobox.com/0dc3op0t7zjf)
- [[Refrain] Chio-chan no Tsuugakuro - 02 VOSTFR {CR} [720p]](https://uptobox.com/up5077z0qtlu)
- [[Refrain] Chio-chan no Tsuugakuro - 02 VOSTFR {CR} [1080p]](https://uptobox.com/c9eu6qrqdzrk)
- [[Refrain] Chio-chan no Tsuugakuro - 03 VOSTFR {ADN} [720p]](https://uptobox.com/epg1q9g0mr1v)
- [[Refrain] Chio-chan no Tsuugakuro - 03 VOSTFR {ADN} [1080p]](https://uptobox.com/0y3oxgxiirnt)
- [[Refrain] Chio-chan no Tsuugakuro - 03 VOSTFR {CR} [720p]](https://uptobox.com/pmhmxauc8ipd)
- [[Refrain] Chio-chan no Tsuugakuro - 03 VOSTFR {CR} [1080p]](https://uptobox.com/o6ke8hkl4ltx)
- [[Refrain] Chio-chan no Tsuugakuro - 04 VOSTFR {ADN} [720p]](https://uptobox.com/28rswukbrhqr)
- [[Refrain] Chio-chan no Tsuugakuro - 04 VOSTFR {ADN} [1080p]](https://uptobox.com/7aqmpdl7vff7)
- [[Refrain] Chio-chan no Tsuugakuro - 04 VOSTFR {CR} [720p]](https://uptobox.com/kvfzhmh9mwt0)
- [[Refrain] Chio-chan no Tsuugakuro - 04 VOSTFR {CR} [1080p]](https://uptobox.com/46q5py60ffao)
- [[Refrain] Chio-chan no Tsuugakuro - 05 VOSTFR {ADN} [720p]](https://uptobox.com/n1o9n97nij6i)
- [[Refrain] Chio-chan no Tsuugakuro - 05 VOSTFR {ADN} [1080p]](https://uptobox.com/dgkllhdvae5y)
- [[Refrain] Chio-chan no Tsuugakuro - 05 VOSTFR {CR} [720p]](https://uptobox.com/y2cbt8m2ye4r)
- [[Refrain] Chio-chan no Tsuugakuro - 05 VOSTFR {CR} [1080p]](https://uptobox.com/uega836qb11w)
- [[Refrain] Chio-chan no Tsuugakuro - 06 VOSTFR {ADN} [720p]](https://uptobox.com/nvp00wu7hzdy)
- [[Refrain] Chio-chan no Tsuugakuro - 06 VOSTFR {ADN} [1080p]](https://uptobox.com/chb4ltfpm7op)
- [[Refrain] Chio-chan no Tsuugakuro - 06 VOSTFR {CR} [720p]](https://uptobox.com/4lkse3cz623d)
- [[Refrain] Chio-chan no Tsuugakuro - 06 VOSTFR {CR} [1080p]](https://uptobox.com/gkk65oshuk1i)
- [[Refrain] Chio-chan no Tsuugakuro - 07 VOSTFR {ADN} [720p]](https://uptobox.com/8utjz9jv8k3q)
- [[Refrain] Chio-chan no Tsuugakuro - 07 VOSTFR {ADN} [1080p]](https://uptobox.com/emugd6yqwbox)
- [[Refrain] Chio-chan no Tsuugakuro - 07 VOSTFR {CR} [720p]](https://uptobox.com/lg0nrnadb9v5)
- [[Refrain] Chio-chan no Tsuugakuro - 07 VOSTFR {CR} [1080p]](https://uptobox.com/13ci8zb2f8qq)
- [[Refrain] Chio-chan no Tsuugakuro - 08 VOSTFR {ADN} [720p]](https://uptobox.com/njvxyxrndilb)
- [[Refrain] Chio-chan no Tsuugakuro - 08 VOSTFR {ADN} [1080p]](https://uptobox.com/qnbu3uw09zdl)
- [[Refrain] Chio-chan no Tsuugakuro - 08 VOSTFR {CR} [720p]](https://uptobox.com/izett9iskuca)
- [[Refrain] Chio-chan no Tsuugakuro - 08 VOSTFR {CR} [1080p]](https://uptobox.com/zkemdo9k9liu)
- [[Refrain] Chio-chan no Tsuugakuro - 09 VOSTFR {ADN} [720p]](https://uptobox.com/xcvccitnyd2s)
- [[Refrain] Chio-chan no Tsuugakuro - 09 VOSTFR {ADN} [1080p]](https://uptobox.com/6y0p2j933iu5)
- [[Refrain] Chio-chan no Tsuugakuro - 09 VOSTFR {CR} [720p]](https://uptobox.com/6dojfl6rlhae)
- [[Refrain] Chio-chan no Tsuugakuro - 09 VOSTFR {CR} [1080p]](https://uptobox.com/xnai0huyow0a)
- [[Refrain] Chio-chan no Tsuugakuro - 10 VOSTFR {ADN} [720p]](https://uptobox.com/v7u70nbqmd3t)
- [[Refrain] Chio-chan no Tsuugakuro - 10 VOSTFR {ADN} [1080p]](https://uptobox.com/u8rjnznt464w)
- [[Refrain] Chio-chan no Tsuugakuro - 10 VOSTFR {CR} [720p]](https://uptobox.com/gzsaws1ve2sk)
- [[Refrain] Chio-chan no Tsuugakuro - 10 VOSTFR {CR} [1080p]](https://uptobox.com/7e0k8fiocgv7)
- [[Refrain] Chio-chan no Tsuugakuro - 11 VOSTFR {ADN} [720p]](https://uptobox.com/p84m76zub0wm)
- [[Refrain] Chio-chan no Tsuugakuro - 11 VOSTFR {ADN} [1080p]](https://uptobox.com/r2mfun943unt)
- [[Refrain] Chio-chan no Tsuugakuro - 11 VOSTFR {CR} [720p]](https://uptobox.com/6siimhnyeg71)
- [[Refrain] Chio-chan no Tsuugakuro - 11 VOSTFR {CR} [1080p]](https://uptobox.com/o17sv38ozw0o)
- [[Refrain] Chio-chan no Tsuugakuro - 12 VOSTFR {ADN} [720p]](https://uptobox.com/y9budu3rg0dn)
- [[Refrain] Chio-chan no Tsuugakuro - 12 VOSTFR {ADN} [1080p]](https://uptobox.com/8lly0v06anyc)
- [[Refrain] Chio-chan no Tsuugakuro - 12 VOSTFR {CR} [720p]](https://uptobox.com/1xvva2c83ddo)
- [[Refrain] Chio-chan no Tsuugakuro - 12 VOSTFR {CR} [1080p]](https://uptobox.com/zhyfus2borbz)
