# One Room 2nd Season

![One Room 2nd Season](https://cdn.myanimelist.net/images/anime/1098/93257l.jpg)

* Japanese:  One Room 第2期

## Information

 - Type: TV
 - Episodes: 12
 - Status: Finished Airing
 - Aired: Jul 10, 2018 to Sep 24, 2018
 - Premiered: Summer 2018
 - Broadcast: Tuesdays at 01:35 (JST)
 - Producers: None found, add some 
 - Licensors: None found, add some 
 - Studios: Zero-G 
 - Source: Original
 - Genres: Slice of Life
 - Duration: 4 min. per ep.
 - Rating: PG-13 - Teens 13 or older


## Links

