# Isekai Maou to Shoukan Shoujo no Dorei Majutsu

![Isekai Maou to Shoukan Shoujo no Dorei Majutsu](https://cdn.myanimelist.net/images/anime/1649/93412l.jpg)

* Japanese:  異世界魔王と召喚少女の奴隷魔術

## Information

 - Type: TV
 - Episodes: 12
 - Status: Finished Airing
 - Aired: Jul 5, 2018 to Sep 20, 2018
 - Premiered: Summer 2018
 - Broadcast: Thursdays at 22:00 (JST)
 - Producers: None found, add some
 - Licensors: Funimation, Crossphere
 - Studios: Ajia-Do
 - Source: Light novel
 - Genres: Harem, Comedy, Magic, Ecchi, Fantasy
 - Duration: 23 min. per ep.
 - Rating: PG-13 - Teens 13 or older


## Links

- [[Refrain] Isekai Maou to Shoukan Shoujo no Dorei Majutsu - 01 VOSTFR [720p]](https://uptobox.com/fswh32cpcc4g)
- [[Refrain] Isekai Maou to Shoukan Shoujo no Dorei Majutsu - 01 VOSTFR [1080p]](https://uptobox.com/dxjpemtdb8p9)
- [[Refrain] Isekai Maou to Shoukan Shoujo no Dorei Majutsu - 02 VOSTFR [720p]](https://uptobox.com/5fewi7e2qgox)
- [[Refrain] Isekai Maou to Shoukan Shoujo no Dorei Majutsu - 02 VOSTFR [1080p]](https://uptobox.com/rpxrgh99x26h)
- [[Refrain] Isekai Maou to Shoukan Shoujo no Dorei Majutsu - 03 VOSTFR [720p]](https://uptobox.com/ij7mqwx5nvey)
- [[Refrain] Isekai Maou to Shoukan Shoujo no Dorei Majutsu - 03 VOSTFR [1080p]](https://uptobox.com/m3m9njawrsch)
- [[Refrain] Isekai Maou to Shoukan Shoujo no Dorei Majutsu - 04 VOSTFR [720p]](https://uptobox.com/a32o2xdb763j)
- [[Refrain] Isekai Maou to Shoukan Shoujo no Dorei Majutsu - 04 VOSTFR [1080p]](https://uptobox.com/uvp018ca4fw0)
- [[Refrain] Isekai Maou to Shoukan Shoujo no Dorei Majutsu - 05 VOSTFR [720p]](https://uptobox.com/hbyv7lmfb7yt)
- [[Refrain] Isekai Maou to Shoukan Shoujo no Dorei Majutsu - 05 VOSTFR [1080p]](https://uptobox.com/gaoq1ej7hloj)
- [[Refrain] Isekai Maou to Shoukan Shoujo no Dorei Majutsu - 06 VOSTFR [720p]](https://uptobox.com/eths6w0y9tno)
- [[Refrain] Isekai Maou to Shoukan Shoujo no Dorei Majutsu - 06 VOSTFR [1080p]](https://uptobox.com/tw5la5sondut)
- [[Refrain] Isekai Maou to Shoukan Shoujo no Dorei Majutsu - 07 VOSTFR [720p]](https://uptobox.com/m680dv0of7w5)
- [[Refrain] Isekai Maou to Shoukan Shoujo no Dorei Majutsu - 07 VOSTFR [1080p]](https://uptobox.com/8xgbxgqzolyj)
- [[Refrain] Isekai Maou to Shoukan Shoujo no Dorei Majutsu - 08 VOSTFR [720p]](https://uptobox.com/dhi0jvctebq7)
- [[Refrain] Isekai Maou to Shoukan Shoujo no Dorei Majutsu - 08 VOSTFR [1080p]](https://uptobox.com/rsj4pp5eg4to)
- [[Refrain] Isekai Maou to Shoukan Shoujo no Dorei Majutsu - 09 VOSTFR [720p]](https://uptobox.com/y8n80rm2uxq1)
- [[Refrain] Isekai Maou to Shoukan Shoujo no Dorei Majutsu - 09 VOSTFR [1080p]](https://uptobox.com/fz83d2bu5cwj)
- [[Refrain] Isekai Maou to Shoukan Shoujo no Dorei Majutsu - 10 VOSTFR [720p]](https://uptobox.com/j9sru59811we)
- [[Refrain] Isekai Maou to Shoukan Shoujo no Dorei Majutsu - 10 VOSTFR [1080p]](https://uptobox.com/j61elwieyeab)
- [[Refrain] Isekai Maou to Shoukan Shoujo no Dorei Majutsu - 11 VOSTFR [720p]](https://uptobox.com/93hhvvs53qk0)
- [[Refrain] Isekai Maou to Shoukan Shoujo no Dorei Majutsu - 11 VOSTFR [1080p]](https://uptobox.com/3xbaik9q1juf)
- [[Refrain] Isekai Maou to Shoukan Shoujo no Dorei Majutsu - 12 VOSTFR [720p]](https://uptobox.com/sjcur33xyjii)
- [[Refrain] Isekai Maou to Shoukan Shoujo no Dorei Majutsu - 12 VOSTFR [1080p]](https://uptobox.com/id8do1io5kt7)
