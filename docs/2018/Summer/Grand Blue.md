# Grand Blue

![Grand Blue](https://cdn.myanimelist.net/images/anime/1302/94882l.jpg)

* Japanese:  ぐらんぶる

## Information

 - Type: TV
 - Episodes: 12
 - Status: Finished Airing
 - Aired: Jul 14, 2018 to Sep 29, 2018
 - Premiered: Summer 2018
 - Broadcast: Saturdays at 02:55 (JST)
 - Producers: Mainichi Broadcasting System, Kodansha, NBCUniversal Entertainment Japan, Avex Pictures, Q-Tec, East Japan Marketing & Communications, GYAO!
 - Licensors: None found, add some
 - Studios: Zero-G
 - Source: Manga
 - Genres: Comedy, Seinen, Slice of Life
 - Duration: 24 min. per ep.
 - Rating: PG-13 - Teens 13 or older


## Links

- [[Refrain] Grand Blue - 01 VOSTFR [720p]](https://uptobox.com/cfhc8t1k7zsr)
- [[Refrain] Grand Blue - 01 VOSTFR [1080p]](https://uptobox.com/t574qtrnjhqh)
- [[Refrain] Grand Blue - 02 VOSTFR [720p]](https://uptobox.com/yyv0jlbrgdv4)
- [[Refrain] Grand Blue - 02 VOSTFR [1080p]](https://uptobox.com/7xg8cxbssi5a)
- [[Refrain] Grand Blue - 03 VOSTFR [720p]](https://uptobox.com/7iipqyg5qrid)
- [[Refrain] Grand Blue - 03 VOSTFR [1080p]](https://uptobox.com/h4is59fqdqy1)
- [[Refrain] Grand Blue - 04 VOSTFR [720p]](https://uptobox.com/t2aw7g7ji89e)
- [[Refrain] Grand Blue - 04 VOSTFR [1080p]](https://uptobox.com/wp4b9sb6kwyh)
- [[Refrain] Grand Blue - 05 VOSTFR [720p]](https://uptobox.com/c9c1b49rm1a2)
- [[Refrain] Grand Blue - 05 VOSTFR [1080p]](https://uptobox.com/vf8zutkr34gj)
- [[Refrain] Grand Blue - 06 VOSTFR [720p]](https://uptobox.com/z6b9r432ldi8)
- [[Refrain] Grand Blue - 06 VOSTFR [1080p]](https://uptobox.com/oro8ss24e7lu)
- [[Refrain] Grand Blue - 07 VOSTFR [720p]](https://uptobox.com/srl8ii09g2yu)
- [[Refrain] Grand Blue - 07 VOSTFR [1080p]](https://uptobox.com/monq672vqzb7)
- [[Refrain] Grand Blue - 08 VOSTFR [720p]](https://uptobox.com/j6e11p3pcmbt)
- [[Refrain] Grand Blue - 08 VOSTFR [1080p]](https://uptobox.com/zd77p5iogksf)
- [[Refrain] Grand Blue - 09 VOSTFR [720p]](https://uptobox.com/fyp0o5uxw6kf)
- [[Refrain] Grand Blue - 09 VOSTFR [1080p]](https://uptobox.com/4zlfpr280zbr)
- [[Refrain] Grand Blue - 10 VOSTFR [720p]](https://uptobox.com/kb0rz5iquzce)
- [[Refrain] Grand Blue - 10 VOSTFR [1080p]](https://uptobox.com/7huqa7br7z9v)
- [[Refrain] Grand Blue - 11 VOSTFR [720p]](https://uptobox.com/d35qlvyxolfm)
- [[Refrain] Grand Blue - 11 VOSTFR [1080p]](https://uptobox.com/ae3490dgu2c2)
- [[Refrain] Grand Blue - 12 VOSTFR [720p]](https://uptobox.com/e940hseyujsx)
- [[Refrain] Grand Blue - 12 VOSTFR [1080p]](https://uptobox.com/3kthsblz2oxm)
