# Sirius the Jaeger

![Sirius the Jaeger](https://cdn.myanimelist.net/images/anime/1456/94897l.jpg)

* Japanese:  天狼〈シリウス〉 Sirius the Jaeger

## Information

 - Type: TV
 - Episodes: 12
 - Status: Finished Airing
 - Aired: Jul 12, 2018 to Sep 27, 2018
 - Premiered: Summer 2018
 - Broadcast: Thursdays at 23:00 (JST)
 - Producers: Infinite
 - Licensors: None found, add some
 - Studios: P.A. Works
 - Source: Original
 - Genres: Action, Historical, Supernatural, Vampire
 - Duration: 23 min. per ep.
 - Rating: R - 17+ (violence & profanity)


## Links

- [[Refrain] Sirius The Jaeger - 01 VF VOSTFR [720p]](https://uptobox.com/l59r2cp5l7mp)
- [[Refrain] Sirius The Jaeger - 01 VF VOSTFR [1080p]](https://uptobox.com/uw2kpjgvyy5g)
- [[Refrain] Sirius The Jaeger - 02 VF VOSTFR [720p]](https://uptobox.com/32nfojta9umm)
- [[Refrain] Sirius The Jaeger - 02 VF VOSTFR [1080p]](https://uptobox.com/yx6mxhis46w4)
- [[Refrain] Sirius The Jaeger - 03 VF VOSTFR [720p]](https://uptobox.com/2krepc9lva47)
- [[Refrain] Sirius The Jaeger - 03 VF VOSTFR [1080p]](https://uptobox.com/g2al1nvjggv2)
- [[Refrain] Sirius The Jaeger - 04 VF VOSTFR [720p]](https://uptobox.com/ty1jl5x2osqa)
- [[Refrain] Sirius The Jaeger - 04 VF VOSTFR [1080p]](https://uptobox.com/s80plk6lzerh)
- [[Refrain] Sirius The Jaeger - 05 VF VOSTFR [720p]](https://uptobox.com/3jjed3optbnn)
- [[Refrain] Sirius The Jaeger - 05 VF VOSTFR [1080p]](https://uptobox.com/9alk4nqgvy8h)
- [[Refrain] Sirius The Jaeger - 06 VF VOSTFR [720p]](https://uptobox.com/2hybbbr2htet)
- [[Refrain] Sirius The Jaeger - 06 VF VOSTFR [1080p]](https://uptobox.com/f3iiezz8zmya)
- [[Refrain] Sirius The Jaeger - 07 VF VOSTFR [720p]](https://uptobox.com/5r5np6fnuuh1)
- [[Refrain] Sirius The Jaeger - 07 VF VOSTFR [1080p]](https://uptobox.com/a1po8wazd2d5)
- [[Refrain] Sirius The Jaeger - 08 VF VOSTFR [720p]](https://uptobox.com/g90o6d23h4dz)
- [[Refrain] Sirius The Jaeger - 08 VF VOSTFR [1080p]](https://uptobox.com/vyurqxinxnvk)
- [[Refrain] Sirius The Jaeger - 09 VF VOSTFR [720p]](https://uptobox.com/2s11z1oc5ynl)
- [[Refrain] Sirius The Jaeger - 09 VF VOSTFR [1080p]](https://uptobox.com/qapdftcjcsc2)
- [[Refrain] Sirius The Jaeger - 10 VF VOSTFR [720p]](https://uptobox.com/k034gm06t0iy)
- [[Refrain] Sirius The Jaeger - 10 VF VOSTFR [1080p]](https://uptobox.com/cknusi41sj6p)
- [[Refrain] Sirius The Jaeger - 11 VF VOSTFR [720p]](https://uptobox.com/ji4xu3cpli6q)
- [[Refrain] Sirius The Jaeger - 11 VF VOSTFR [1080p]](https://uptobox.com/zsrtrs78obyy)
- [[Refrain] Sirius The Jaeger - 12 VF VOSTFR [720p]](https://uptobox.com/gj35oxglnh2l)
- [[Refrain] Sirius The Jaeger - 12 VF VOSTFR [1080p]](https://uptobox.com/a4t68kxcajkm)
