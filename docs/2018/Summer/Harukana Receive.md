# Harukana Receive

![Harukana Receive](https://cdn.myanimelist.net/images/anime/1924/95212l.jpg)

* Japanese:  はるかなレシーブ

## Information

 - Type: TV
 - Episodes: 12
 - Status: Finished Airing
 - Aired: Jul 6, 2018 to Sep 21, 2018
 - Premiered: Summer 2018
 - Broadcast: Fridays at 21:00 (JST)
 - Producers: None found, add some
 - Licensors: Funimation
 - Studios: C2C
 - Source: Manga
 - Genres: Sports, Slice of Life
 - Duration: 23 min. per ep.
 - Rating: PG-13 - Teens 13 or older


## Links

- [[Refrain] Harukana Receive - 01 VOSTFR {ADN} [720p]](https://uptobox.com/hv9w5hyw35vc)
- [[Refrain] Harukana Receive - 01 VOSTFR {ADN} [1080p]](https://uptobox.com/61ssyr64k4zg)
- [[Refrain] Harukana Receive - 01 VOSTFR {CR} [720p]](https://uptobox.com/awi2jf265n3t)
- [[Refrain] Harukana Receive - 01 VOSTFR {CR} [1080p]](https://uptobox.com/9nw39y0or04l)
- [[Refrain] Harukana Receive - 02 VOSTFR {ADN} [720p]](https://uptobox.com/7hmznllcy0n9)
- [[Refrain] Harukana Receive - 02 VOSTFR {ADN} [1080p]](https://uptobox.com/f0ylxi7urpuh)
- [[Refrain] Harukana Receive - 02 VOSTFR {CR} [720p]](https://uptobox.com/4yljnz61gcsz)
- [[Refrain] Harukana Receive - 02 VOSTFR {CR} [1080p]](https://uptobox.com/elnw10rlk04z)
- [[Refrain] Harukana Receive - 03 VOSTFR {ADN} [720p]](https://uptobox.com/faq2j2hpwfbi)
- [[Refrain] Harukana Receive - 03 VOSTFR {ADN} [1080p]](https://uptobox.com/1x96griux4l2)
- [[Refrain] Harukana Receive - 03 VOSTFR {CR} [720p]](https://uptobox.com/rev51d1ke56z)
- [[Refrain] Harukana Receive - 03 VOSTFR {CR} [1080p]](https://uptobox.com/9u43mjj43dce)
- [[Refrain] Harukana Receive - 04 VOSTFR {ADN} [720p]](https://uptobox.com/0oy4y58ny3y2)
- [[Refrain] Harukana Receive - 04 VOSTFR {ADN} [1080p]](https://uptobox.com/f0k0wdjfq7zp)
- [[Refrain] Harukana Receive - 04 VOSTFR {CR} [720p]](https://uptobox.com/fta5zdtw8qbd)
- [[Refrain] Harukana Receive - 04 VOSTFR {CR} [1080p]](https://uptobox.com/v3o7jt405z28)
- [[Refrain] Harukana Receive - 05 VOSTFR {ADN} [720p]](https://uptobox.com/4pha1zh2s3w4)
- [[Refrain] Harukana Receive - 05 VOSTFR {ADN} [1080p]](https://uptobox.com/rfzky2d9lbw0)
- [[Refrain] Harukana Receive - 05 VOSTFR {CR} [720p]](https://uptobox.com/clci9vr297x4)
- [[Refrain] Harukana Receive - 05 VOSTFR {CR} [1080p]](https://uptobox.com/jkiq8pyus9bi)
- [[Refrain] Harukana Receive - 06 VOSTFR {ADN} [720p]](https://uptobox.com/45bsekd2sc34)
- [[Refrain] Harukana Receive - 06 VOSTFR {ADN} [1080p]](https://uptobox.com/sq35gukofa1f)
- [[Refrain] Harukana Receive - 06 VOSTFR {CR} [720p]](https://uptobox.com/4wj8zzwgadk8)
- [[Refrain] Harukana Receive - 06 VOSTFR {CR} [1080p]](https://uptobox.com/da4rob7xbfyb)
- [[Refrain] Harukana Receive - 07 VOSTFR {ADN} [720p]](https://uptobox.com/u0ef23i3ysb3)
- [[Refrain] Harukana Receive - 07 VOSTFR {ADN} [1080p]](https://uptobox.com/bib4el23uda7)
- [[Refrain] Harukana Receive - 07 VOSTFR {CR} [720p]](https://uptobox.com/6581kvm7md0v)
- [[Refrain] Harukana Receive - 07 VOSTFR {CR} [1080p]](https://uptobox.com/s0lngu9fzrrf)
- [[Refrain] Harukana Receive - 08 VOSTFR {ADN} [720p]](https://uptobox.com/s99dmqu00ji1)
- [[Refrain] Harukana Receive - 08 VOSTFR {ADN} [1080p]](https://uptobox.com/alx1lcc85i6s)
- [[Refrain] Harukana Receive - 08 VOSTFR {CR} [720p]](https://uptobox.com/6gka2y145003)
- [[Refrain] Harukana Receive - 08 VOSTFR {CR} [1080p]](https://uptobox.com/vn0im4j94p11)
- [[Refrain] Harukana Receive - 09 VOSTFR {ADN} [720p]](https://uptobox.com/o0tk1oyx6kdb)
- [[Refrain] Harukana Receive - 09 VOSTFR {ADN} [1080p]](https://uptobox.com/2xb663oa5fjo)
- [[Refrain] Harukana Receive - 09 VOSTFR {CR} [720p]](https://uptobox.com/s4mepqx8t5s8)
- [[Refrain] Harukana Receive - 09 VOSTFR {CR} [1080p]](https://uptobox.com/g8e0ild3rbom)
- [[Refrain] Harukana Receive - 10 VOSTFR {ADN} [720p]](https://uptobox.com/lx9rbrp41fbj)
- [[Refrain] Harukana Receive - 10 VOSTFR {ADN} [1080p]](https://uptobox.com/kyhtiff6plde)
- [[Refrain] Harukana Receive - 10 VOSTFR {CR} [720p]](https://uptobox.com/iud9ak2m4zej)
- [[Refrain] Harukana Receive - 10 VOSTFR {CR} [1080p]](https://uptobox.com/9mmaxjloyfqg)
- [[Refrain] Harukana Receive - 11 VOSTFR {ADN} [720p]](https://uptobox.com/yaeaj48z6vgl)
- [[Refrain] Harukana Receive - 11 VOSTFR {ADN} [1080p]](https://uptobox.com/4laf2zgtsmwr)
- [[Refrain] Harukana Receive - 11 VOSTFR {CR} [720p]](https://uptobox.com/ltryl8lpybgy)
- [[Refrain] Harukana Receive - 11 VOSTFR {CR} [1080p]](https://uptobox.com/0ntcxe4t85ta)
- [[Refrain] Harukana Receive - 12 VOSTFR {ADN} [720p]](https://uptobox.com/qfdgnoy0du63)
- [[Refrain] Harukana Receive - 12 VOSTFR {ADN} [1080p]](https://uptobox.com/0od5u3sm76jo)
- [[Refrain] Harukana Receive - 12 VOSTFR {CR} [720p]](https://uptobox.com/u556r1386ngi)
- [[Refrain] Harukana Receive - 12 VOSTFR {CR} [1080p]](https://uptobox.com/bgg2l7k4kl8e)
