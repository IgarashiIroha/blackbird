# Cinderella Girls Gekijou 3rd Season

![Cinderella Girls Gekijou 3rd Season](https://cdn.myanimelist.net/images/anime/1618/90871l.jpg)

* Japanese:  シンデレラガールズ劇場 第3期

## Information

 - Type: TV
 - Episodes: 13
 - Status: Finished Airing
 - Aired: Jul 3, 2018 to Sep 25, 2018
 - Premiered: Summer 2018
 - Broadcast: Tuesdays at 21:54 (JST)
 - Producers: None found, add some 
 - Licensors: Crunchyroll 
 - Studios: Gathering, Lesprit 
 - Source: 4-koma manga
 - Genres: Comedy, Slice of Life
 - Duration: 3 min. per ep.
 - Rating: PG-13 - Teens 13 or older


## Links

