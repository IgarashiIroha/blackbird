# Phantom in the Twilight

![Phantom in the Twilight](https://cdn.myanimelist.net/images/anime/1156/93567l.jpg)

* Japanese:  ファントム イン ザ トワイライト

## Information

 - Type: TV
 - Episodes: 12
 - Status: Finished Airing
 - Aired: Jul 10, 2018 to Sep 25, 2018
 - Premiered: Summer 2018
 - Broadcast: Mondays at 00:30 (JST)
 - Producers: None found, add some
 - Licensors: None found, add some
 - Studios: LIDENFILMS
 - Source: Original
 - Genres: Action, Supernatural, Vampire
 - Duration: 23 min. per ep.
 - Rating: PG-13 - Teens 13 or older


## Links

- [[Refrain] Phantom in the Twilight - 01 VOSTFR [720p]](https://uptobox.com/w4ipx1jw1ued)
- [[Refrain] Phantom in the Twilight - 01 VOSTFR [1080p]](https://uptobox.com/rerg1l9wwgay)
- [[Refrain] Phantom in the Twilight - 02 VOSTFR [720p]](https://uptobox.com/1fm89dcm0td4)
- [[Refrain] Phantom in the Twilight - 02 VOSTFR [1080p]](https://uptobox.com/aq67e8m4woiy)
- [[Refrain] Phantom in the Twilight - 03 VOSTFR [720p]](https://uptobox.com/16sxj72lt5h6)
- [[Refrain] Phantom in the Twilight - 03 VOSTFR [1080p]](https://uptobox.com/8ax9m9c4urdm)
- [[Refrain] Phantom in the Twilight - 04 VOSTFR [720p]](https://uptobox.com/22lrh4jm3s82)
- [[Refrain] Phantom in the Twilight - 04 VOSTFR [1080p]](https://uptobox.com/onbkl3ow5jls)
- [[Refrain] Phantom in the Twilight - 05 VOSTFR [720p]](https://uptobox.com/gv1f4gh6mz9e)
- [[Refrain] Phantom in the Twilight - 05 VOSTFR [1080p]](https://uptobox.com/q2c53jbstyl3)
- [[Refrain] Phantom in the Twilight - 06 VOSTFR [720p]](https://uptobox.com/n4e4czxut5cy)
- [[Refrain] Phantom in the Twilight - 06 VOSTFR [1080p]](https://uptobox.com/v427pf4484zx)
- [[Refrain] Phantom in the Twilight - 07 VOSTFR [720p]](https://uptobox.com/fokkt3abs6t2)
- [[Refrain] Phantom in the Twilight - 07 VOSTFR [1080p]](https://uptobox.com/z5jabgl4dnis)
- [[Refrain] Phantom in the Twilight - 08 VOSTFR [720p]](https://uptobox.com/jwpvg34eukmq)
- [[Refrain] Phantom in the Twilight - 08 VOSTFR [1080p]](https://uptobox.com/p76qp1hyowar)
- [[Refrain] Phantom in the Twilight - 09 VOSTFR [720p]](https://uptobox.com/x7etm5fw00dx)
- [[Refrain] Phantom in the Twilight - 09 VOSTFR [1080p]](https://uptobox.com/tovlruge7xdu)
- [[Refrain] Phantom in the Twilight - 10 VOSTFR [720p]](https://uptobox.com/1lyo3z5z5iw1)
- [[Refrain] Phantom in the Twilight - 10 VOSTFR [1080p]](https://uptobox.com/humn2mttbwzt)
- [[Refrain] Phantom in the Twilight - 11 VOSTFR [720p]](https://uptobox.com/59crr8hk7ij8)
- [[Refrain] Phantom in the Twilight - 11 VOSTFR [1080p]](https://uptobox.com/dv60zvemp01v)
- [[Refrain] Phantom in the Twilight - 12 VOSTFR [720p]](https://uptobox.com/uf2copceejch)
- [[Refrain] Phantom in the Twilight - 12 VOSTFR [1080p]](https://uptobox.com/zx4n0ye45f9a)
