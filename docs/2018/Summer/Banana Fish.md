# Banana Fish

![Banana Fish](https://cdn.myanimelist.net/images/anime/1190/93472l.jpg)

* Japanese:  BANANA FISH

## Information

 - Type: TV
 - Episodes: 24
 - Status: Finished Airing
 - Aired: Jul 6, 2018 to Dec 21, 2018
 - Premiered: Summer 2018
 - Broadcast: Fridays at 00:55 (JST)
 - Producers: Aniplex, Fuji TV, dugout
 - Licensors: None found, add some
 - Studios: MAPPA
 - Source: Manga
 - Genres: Action, Adventure, Drama, Shoujo
 - Duration: 22 min. per ep.
 - Rating: R - 17+ (violence & profanity)


## Links

- [[Refrain] Banana Fish - 01 VOSTFR [720p]](https://uptobox.com/vjzl02iq6ome)
- [[Refrain] Banana Fish - 01 VOSTFR [720p]](https://uptobox.com/wudcbk0gwd5y)
- [[Refrain] Banana Fish - 01 VOSTFR [1080p]](https://uptobox.com/zrvkb5apugdc)
- [[Refrain] Banana Fish - 01 VOSTFR [1080p]](https://uptobox.com/0ea7712g488f)
- [[Refrain] Banana Fish - 02 VOSTFR [720p]](https://uptobox.com/jihmk94xd6ma)
- [[Refrain] Banana Fish - 02 VOSTFR [720p]](https://uptobox.com/pi1gkko6c8kl)
- [[Refrain] Banana Fish - 02 VOSTFR [1080p]](https://uptobox.com/ltp8ibowgomn)
- [[Refrain] Banana Fish - 02 VOSTFR [1080p]](https://uptobox.com/rpimp9bvg3eh)
- [[Refrain] Banana Fish - 03 VOSTFR [720p]](https://uptobox.com/iursqvuhseyz)
- [[Refrain] Banana Fish - 03 VOSTFR [720p]](https://uptobox.com/rfgmqhpxtdy5)
- [[Refrain] Banana Fish - 03 VOSTFR [1080p]](https://uptobox.com/3zlnn3uemio9)
- [[Refrain] Banana Fish - 03 VOSTFR [1080p]](https://uptobox.com/0mwh6bjlogym)
- [[Refrain] Banana Fish - 04 VOSTFR [720p]](https://uptobox.com/9h3b6hfqlo0w)
- [[Refrain] Banana Fish - 04 VOSTFR [720p]](https://uptobox.com/pqz3fmdvt3kq)
- [[Refrain] Banana Fish - 04 VOSTFR [1080p]](https://uptobox.com/ebv4sjxmxw4b)
- [[Refrain] Banana Fish - 04 VOSTFR [1080p]](https://uptobox.com/1uxkxtk8vvyg)
- [[Refrain] Banana Fish - 05 VOSTFR [720p]](https://uptobox.com/tv9693ri1j9t)
- [[Refrain] Banana Fish - 05 VOSTFR [720p]](https://uptobox.com/e98r8kl6j3lk)
- [[Refrain] Banana Fish - 05 VOSTFR [1080p]](https://uptobox.com/0pmipmu4jnsj)
- [[Refrain] Banana Fish - 05 VOSTFR [1080p]](https://uptobox.com/94gnv0x2w80s)
- [[Refrain] Banana Fish - 06 VOSTFR [720p]](https://uptobox.com/26zs5mjz7t0d)
- [[Refrain] Banana Fish - 06 VOSTFR [720p]](https://uptobox.com/e7f3anjbzu7t)
- [[Refrain] Banana Fish - 06 VOSTFR [1080p]](https://uptobox.com/5lnudwwl6t8j)
- [[Refrain] Banana Fish - 06 VOSTFR [1080p]](https://uptobox.com/mujlec6v7mew)
- [[Refrain] Banana Fish - 07 VOSTFR [720p]](https://uptobox.com/xxmdq0i1algu)
- [[Refrain] Banana Fish - 07 VOSTFR [720p]](https://uptobox.com/tii9avhtpc11)
- [[Refrain] Banana Fish - 07 VOSTFR [1080p]](https://uptobox.com/gikkb1i3pek0)
- [[Refrain] Banana Fish - 07 VOSTFR [1080p]](https://uptobox.com/g5f1m1vgigdq)
- [[Refrain] Banana Fish - 08 VOSTFR [720p]](https://uptobox.com/xn2gjfzo9n4y)
- [[Refrain] Banana Fish - 08 VOSTFR [720p]](https://uptobox.com/qgxx9p108fmf)
- [[Refrain] Banana Fish - 08 VOSTFR [1080p]](https://uptobox.com/rj6zhj19h6zk)
- [[Refrain] Banana Fish - 08 VOSTFR [1080p]](https://uptobox.com/47geqivde5om)
- [[Refrain] Banana Fish - 09 VOSTFR [720p]](https://uptobox.com/6ax5gdi6jhzh)
- [[Refrain] Banana Fish - 09 VOSTFR [720p]](https://uptobox.com/585ufpvhs6rq)
- [[Refrain] Banana Fish - 09 VOSTFR [1080p]](https://uptobox.com/jwxvyr7t745k)
- [[Refrain] Banana Fish - 09 VOSTFR [1080p]](https://uptobox.com/odw37s835tlu)
- [[Refrain] Banana Fish - 10 VOSTFR [720p]](https://uptobox.com/2ccvw808qa4i)
- [[Refrain] Banana Fish - 10 VOSTFR [720p]](https://uptobox.com/zgn783wlg366)
- [[Refrain] Banana Fish - 10 VOSTFR [1080p]](https://uptobox.com/fbux00281m3t)
- [[Refrain] Banana Fish - 10 VOSTFR [1080p]](https://uptobox.com/rs7e65yhbcux)
- [[Refrain] Banana Fish - 11 VOSTFR [720p]](https://uptobox.com/60t4vmcitpe5)
- [[Refrain] Banana Fish - 11 VOSTFR [720p]](https://uptobox.com/rxoerr4xrudu)
- [[Refrain] Banana Fish - 11 VOSTFR [1080p]](https://uptobox.com/0cs5ucrz74xr)
- [[Refrain] Banana Fish - 11 VOSTFR [1080p]](https://uptobox.com/imldv3zk48xw)
- [[Refrain] Banana Fish - 12 VOSTFR [720p]](https://uptobox.com/gsefh6lmrq8z)
- [[Refrain] Banana Fish - 12 VOSTFR [720p]](https://uptobox.com/8eqxyzdht867)
- [[Refrain] Banana Fish - 12 VOSTFR [1080p]](https://uptobox.com/ixtbxk9v79t2)
- [[Refrain] Banana Fish - 12 VOSTFR [1080p]](https://uptobox.com/c3cmg0o7wozr)
- [[Refrain] Banana Fish - 13 VOSTFR [720p]](https://uptobox.com/a06q5votpn5p)
- [[Refrain] Banana Fish - 13 VOSTFR [1080p]](https://uptobox.com/dhq6zpob0n43)
- [[Refrain] Banana Fish - 14 VOSTFR [720p]](https://uptobox.com/j3jzdv6qq4il)
- [[Refrain] Banana Fish - 14 VOSTFR [1080p]](https://uptobox.com/rvh7m8j4w35a)
- [[Refrain] Banana Fish - 15 VOSTFR [720p]](https://uptobox.com/2hll95sddqmp)
- [[Refrain] Banana Fish - 15 VOSTFR [1080p]](https://uptobox.com/c9e9vjhda2ti)
- [[Refrain] Banana Fish - 16 VOSTFR [720p]](https://uptobox.com/py0yqnuc0r15)
- [[Refrain] Banana Fish - 16 VOSTFR [1080p]](https://uptobox.com/1b3goe5c8rsx)
- [[Refrain] Banana Fish - 17 VOSTFR [720p]](https://uptobox.com/m71dtwgcc29e)
- [[Refrain] Banana Fish - 17 VOSTFR [1080p]](https://uptobox.com/1t2y5b99jm29)
- [[Refrain] Banana Fish - 18 VOSTFR [720p]](https://uptobox.com/c45499y9ivu9)
- [[Refrain] Banana Fish - 18 VOSTFR [1080p]](https://uptobox.com/rujj7z2ruvmr)
- [[Refrain] Banana Fish - 19 VOSTFR [720p]](https://uptobox.com/nl0jccmspmk8)
- [[Refrain] Banana Fish - 19 VOSTFR [1080p]](https://uptobox.com/yl4m2nlcbhin)
- [[Refrain] Banana Fish - 20 VOSTFR [720p]](https://uptobox.com/q5f8nc2dyfnr)
- [[Refrain] Banana Fish - 20 VOSTFR [1080p]](https://uptobox.com/lrd4gk52ghb6)
- [[Refrain] Banana Fish - 21 VOSTFR [720p]](https://uptobox.com/5s9dlju4f4qg)
- [[Refrain] Banana Fish - 21 VOSTFR [1080p]](https://uptobox.com/0d1btw48ki28)
- [[Refrain] Banana Fish - 22 VOSTFR [720p]](https://uptobox.com/z9fvnjhbgl6c)
- [[Refrain] Banana Fish - 22 VOSTFR [1080p]](https://uptobox.com/6sg3gynfqg09)
- [[Refrain] Banana Fish - 23 VOSTFR [720p]](https://uptobox.com/3h8khiaeekwy)
- [[Refrain] Banana Fish - 23 VOSTFR [1080p]](https://uptobox.com/qmgg8pvphe9l)
- [[Refrain] Banana Fish - 24 VOSTFR [720p]](https://uptobox.com/4qop8a3boqg6)
- [[Refrain] Banana Fish - 24 VOSTFR [1080p]](https://uptobox.com/zw0yj4bcb8ym)
