# BanG Dream! Garupa☆Pico

![BanG Dream! Garupa☆Pico](https://cdn.myanimelist.net/images/anime/1341/92872l.jpg)

* Japanese:  BanG Dream! ガルパ☆ピコ

## Information

 - Type: TV
 - Episodes: 26
 - Status: Finished Airing
 - Aired: Jul 5, 2018 to Dec 27, 2018
 - Premiered: Summer 2018
 - Broadcast: Thursdays at 23:00 (JST)
 - Producers: None found, add some 
 - Licensors: None found, add some 
 - Studios: SANZIGEN, DMM.futureworks 
 - Source: Manga
 - Genres: Music, Comedy
 - Duration: 3 min. per ep.
 - Rating: PG-13 - Teens 13 or older


## Links

