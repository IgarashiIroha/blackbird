# Angolmois: Genkou Kassenki

![Angolmois: Genkou Kassenki](https://cdn.myanimelist.net/images/anime/1469/95052l.jpg)

* Japanese:  アンゴルモア 元寇合戦記

## Information

 - Type: TV
 - Episodes: 12
 - Status: Finished Airing
 - Aired: Jul 11, 2018 to Sep 26, 2018
 - Premiered: Summer 2018
 - Broadcast: Wednesdays at 12:30 (JST)
 - Producers: None found, add some
 - Licensors: None found, add some
 - Studios: NAZ
 - Source: Manga
 - Genres: Action, Drama, Historical, Military, Samurai
 - Duration: 23 min. per ep.
 - Rating: PG-13 - Teens 13 or older


## Links

- [[Refrain] Angolmois - Genkou Kassenki - 01 VOSTFR [720p]](https://uptobox.com/3b8zls6qbbq7)
- [[Refrain] Angolmois - Genkou Kassenki - 01 VOSTFR [1080p]](https://uptobox.com/tabt63eiu8jd)
- [[Refrain] Angolmois - Genkou Kassenki - 02 VOSTFR [720p]](https://uptobox.com/2n9nawxh8tqk)
- [[Refrain] Angolmois - Genkou Kassenki - 02 VOSTFR [1080p]](https://uptobox.com/3jsbfdzik1rq)
- [[Refrain] Angolmois - Genkou Kassenki - 03 VOSTFR [720p]](https://uptobox.com/ku4wi67u8oge)
- [[Refrain] Angolmois - Genkou Kassenki - 03 VOSTFR [1080p]](https://uptobox.com/ni6rv9k5qi43)
- [[Refrain] Angolmois - Genkou Kassenki - 04 VOSTFR [720p]](https://uptobox.com/u6a7wrnztrg9)
- [[Refrain] Angolmois - Genkou Kassenki - 04 VOSTFR [1080p]](https://uptobox.com/xbs5m1izmnvp)
- [[Refrain] Angolmois - Genkou Kassenki - 05 VOSTFR [720p]](https://uptobox.com/o7qwxzqrc7as)
- [[Refrain] Angolmois - Genkou Kassenki - 05 VOSTFR [1080p]](https://uptobox.com/incipby5f7jf)
- [[Refrain] Angolmois - Genkou Kassenki - 06 VOSTFR [720p]](https://uptobox.com/32dk07m2hemd)
- [[Refrain] Angolmois - Genkou Kassenki - 06 VOSTFR [1080p]](https://uptobox.com/b9znoomvu4si)
- [[Refrain] Angolmois - Genkou Kassenki - 07 VOSTFR [720p]](https://uptobox.com/id0hpfsg2653)
- [[Refrain] Angolmois - Genkou Kassenki - 07 VOSTFR [1080p]](https://uptobox.com/ghh526j8bz62)
- [[Refrain] Angolmois - Genkou Kassenki - 08 VOSTFR [720p]](https://uptobox.com/7s0lmw1gu3mf)
- [[Refrain] Angolmois - Genkou Kassenki - 08 VOSTFR [1080p]](https://uptobox.com/lhpyhnzgcz7c)
- [[Refrain] Angolmois - Genkou Kassenki - 09 VOSTFR [720p]](https://uptobox.com/l7fwo4xxaknv)
- [[Refrain] Angolmois - Genkou Kassenki - 09 VOSTFR [1080p]](https://uptobox.com/cnzz8lpec6az)
- [[Refrain] Angolmois - Genkou Kassenki - 10 VOSTFR [720p]](https://uptobox.com/r2kvhdy9po05)
- [[Refrain] Angolmois - Genkou Kassenki - 10 VOSTFR [1080p]](https://uptobox.com/rq4ojmg13uak)
- [[Refrain] Angolmois - Genkou Kassenki - 11 VOSTFR [720p]](https://uptobox.com/un5wze7mheii)
- [[Refrain] Angolmois - Genkou Kassenki - 11 VOSTFR [1080p]](https://uptobox.com/uhfiisyn06oj)
- [[Refrain] Angolmois - Genkou Kassenki - 12 VOSTFR [720p]](https://uptobox.com/cvadrosr7kmz)
- [[Refrain] Angolmois - Genkou Kassenki - 12 VOSTFR [1080p]](https://uptobox.com/ynmqpnbd2drh)
