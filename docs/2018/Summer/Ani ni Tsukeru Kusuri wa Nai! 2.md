# Ani ni Tsukeru Kusuri wa Nai! 2

![Ani ni Tsukeru Kusuri wa Nai! 2](https://cdn.myanimelist.net/images/anime/1854/93785l.jpg)

* Japanese:  兄に付ける薬はない! 2

## Information

 - Type: TV
 - Episodes: 24
 - Status: Finished Airing
 - Aired: Jul 9, 2018 to Dec 17, 2018
 - Premiered: Summer 2018
 - Broadcast: Mondays at 21:54 (JST)
 - Producers: Tencent Penguin Pictures 
 - Licensors: None found, add some 
 - Studios: Fanworks, Imagineer 
 - Source: Web manga
 - Genres: Slice of Life, Comedy, School
 - Duration: 3 min. per ep.
 - Rating: PG - Children


## Links

