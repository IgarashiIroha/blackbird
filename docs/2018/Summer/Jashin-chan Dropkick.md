# Jashin-chan Dropkick

![Jashin-chan Dropkick](https://cdn.myanimelist.net/images/anime/1892/93420l.jpg)

* Japanese:  邪神ちゃんドロップキック

## Information

 - Type: TV
 - Episodes: 11
 - Status: Finished Airing
 - Aired: Jul 10, 2018 to Sep 17, 2018
 - Premiered: Summer 2018
 - Broadcast: Tuesdays at 01:05 (JST)
 - Producers: None found, add some
 - Licensors: None found, add some
 - Studios: Nomad
 - Source: Web manga
 - Genres: Comedy, Supernatural
 - Duration: 23 min. per ep.
 - Rating: PG-13 - Teens 13 or older


## Links

- [[Refrain] Jashin-chan Dropkick - 01 VOSTFR [720p]](https://uptobox.com/h5uq1gh1wwds)
- [[Refrain] Jashin-chan Dropkick - 01 VOSTFR [1080p]](https://uptobox.com/bptaq0vd3ks5)
- [[Refrain] Jashin-chan Dropkick - 02 VOSTFR [720p]](https://uptobox.com/eydt09qh6khe)
- [[Refrain] Jashin-chan Dropkick - 02 VOSTFR [1080p]](https://uptobox.com/cut1145ow40y)
- [[Refrain] Jashin-chan Dropkick - 03 VOSTFR [720p]](https://uptobox.com/z4kmu17ui0dr)
- [[Refrain] Jashin-chan Dropkick - 03 VOSTFR [1080p]](https://uptobox.com/f6xq3za13ogy)
- [[Refrain] Jashin-chan Dropkick - 04 VOSTFR [720p]](https://uptobox.com/6jeytmsouwnn)
- [[Refrain] Jashin-chan Dropkick - 04 VOSTFR [1080p]](https://uptobox.com/ttbojqug0ktg)
- [[Refrain] Jashin-chan Dropkick - 05 VOSTFR [720p]](https://uptobox.com/z20qi3qvg26v)
- [[Refrain] Jashin-chan Dropkick - 05 VOSTFR [1080p]](https://uptobox.com/mntzk5stkjgg)
- [[Refrain] Jashin-chan Dropkick - 06 VOSTFR [720p]](https://uptobox.com/fa8m2ieywb7u)
- [[Refrain] Jashin-chan Dropkick - 06 VOSTFR [1080p]](https://uptobox.com/6a2oeckxlen2)
- [[Refrain] Jashin-chan Dropkick - 07 VOSTFR [720p]](https://uptobox.com/3auw4d8z2n3j)
- [[Refrain] Jashin-chan Dropkick - 07 VOSTFR [1080p]](https://uptobox.com/lzu6dmfn8ikb)
- [[Refrain] Jashin-chan Dropkick - 08 VOSTFR [720p]](https://uptobox.com/45eszexk3c1r)
- [[Refrain] Jashin-chan Dropkick - 08 VOSTFR [1080p]](https://uptobox.com/rv6ctg2ft4cv)
- [[Refrain] Jashin-chan Dropkick - 09 VOSTFR [720p]](https://uptobox.com/zfiykfqe9twa)
- [[Refrain] Jashin-chan Dropkick - 09 VOSTFR [1080p]](https://uptobox.com/5659aagzv71a)
- [[Refrain] Jashin-chan Dropkick - 10 VOSTFR [720p]](https://uptobox.com/f59s29qr29qu)
- [[Refrain] Jashin-chan Dropkick - 10 VOSTFR [1080p]](https://uptobox.com/438gggk8liwx)
- [[Refrain] Jashin-chan Dropkick - 11 VOSTFR [720p]](https://uptobox.com/6fjbyhpus1rl)
- [[Refrain] Jashin-chan Dropkick - 11 VOSTFR [1080p]](https://uptobox.com/rct05lld53gq)
- [[Refrain] Jashin-chan Dropkick - 12 VOSTFR [720p]](https://uptobox.com/w378rd6uix2g)
- [[Refrain] Jashin-chan Dropkick - 12 VOSTFR [1080p]](https://uptobox.com/t6ybire4ct1v)
