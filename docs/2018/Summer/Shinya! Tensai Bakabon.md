# Shinya! Tensai Bakabon

![Shinya! Tensai Bakabon](https://cdn.myanimelist.net/images/anime/1031/92911l.jpg)

* Japanese:  深夜！天才バカボン

## Information

 - Type: TV
 - Episodes: 12
 - Status: Finished Airing
 - Aired: Jul 11, 2018 to Sep 26, 2018
 - Premiered: Summer 2018
 - Broadcast: Wednesdays at 01:35 (JST)
 - Producers: None found, add some
 - Licensors: None found, add some
 - Studios: Pierrot Plus
 - Source: Manga
 - Genres: Comedy, Slice of Life
 - Duration: 24 min. per ep.
 - Rating: G - All Ages


## Links

- [[Refrain] Shinya! Tensai Bakabon - 01 VOSTFR [720p]](https://uptobox.com/jby8siwrkjxq)
- [[Refrain] Shinya! Tensai Bakabon - 01 VOSTFR [1080p]](https://uptobox.com/ayjoz1czndu8)
- [[Refrain] Shinya! Tensai Bakabon - 02 VOSTFR [720p]](https://uptobox.com/b65mqueab194)
- [[Refrain] Shinya! Tensai Bakabon - 02 VOSTFR [1080p]](https://uptobox.com/fq1xm6p6enn1)
- [[Refrain] Shinya! Tensai Bakabon - 03 VOSTFR [720p]](https://uptobox.com/lf55bu9ey5qi)
- [[Refrain] Shinya! Tensai Bakabon - 03 VOSTFR [1080p]](https://uptobox.com/z7wmorfgbfjs)
- [[Refrain] Shinya! Tensai Bakabon - 04 VOSTFR [720p]](https://uptobox.com/mm8fctsktw9f)
- [[Refrain] Shinya! Tensai Bakabon - 04 VOSTFR [1080p]](https://uptobox.com/q3eco6cxkv5m)
- [[Refrain] Shinya! Tensai Bakabon - 05 VOSTFR [720p]](https://uptobox.com/imp1laz1yi3j)
- [[Refrain] Shinya! Tensai Bakabon - 05 VOSTFR [1080p]](https://uptobox.com/w159ftnfql6z)
- [[Refrain] Shinya! Tensai Bakabon - 06 VOSTFR [720p]](https://uptobox.com/sp7yyurvvre3)
- [[Refrain] Shinya! Tensai Bakabon - 06 VOSTFR [1080p]](https://uptobox.com/fum3pcl1ad30)
- [[Refrain] Shinya! Tensai Bakabon - 07 VOSTFR [720p]](https://uptobox.com/s3iad0dik3l5)
- [[Refrain] Shinya! Tensai Bakabon - 07 VOSTFR [1080p]](https://uptobox.com/cene794zkst1)
- [[Refrain] Shinya! Tensai Bakabon - 08 VOSTFR [720p]](https://uptobox.com/e0aqshpx4od3)
- [[Refrain] Shinya! Tensai Bakabon - 08 VOSTFR [1080p]](https://uptobox.com/7omol5m1g6at)
- [[Refrain] Shinya! Tensai Bakabon - 09 VOSTFR [720p]](https://uptobox.com/8bwz3putcaw6)
- [[Refrain] Shinya! Tensai Bakabon - 09 VOSTFR [1080p]](https://uptobox.com/xz2htvhm3l2r)
- [[Refrain] Shinya! Tensai Bakabon - 10 VOSTFR [720p]](https://uptobox.com/5fkan0w5fpqs)
- [[Refrain] Shinya! Tensai Bakabon - 10 VOSTFR [1080p]](https://uptobox.com/s1qazq89j85k)
- [[Refrain] Shinya! Tensai Bakabon - 11 VOSTFR [720p]](https://uptobox.com/n6acs9pai9i1)
- [[Refrain] Shinya! Tensai Bakabon - 11 VOSTFR [1080p]](https://uptobox.com/dyzv3fec175r)
- [[Refrain] Shinya! Tensai Bakabon - 12 VOSTFR [720p]](https://uptobox.com/nb6pp8tfgx64)
- [[Refrain] Shinya! Tensai Bakabon - 12 VOSTFR [1080p]](https://uptobox.com/tfkmpcm7o1q7)
