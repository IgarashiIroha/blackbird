# Aguu: Tensai Ningyou

![Aguu: Tensai Ningyou](https://cdn.myanimelist.net/images/anime/1208/92825l.jpg)

* Japanese:  悪偶 ‐天才人形‐

## Information

 - Type: TV
 - Episodes: 12
 - Status: Finished Airing
 - Aired: Jul 10, 2018 to Sep 25, 2018
 - Premiered: Summer 2018
 - Broadcast: Tuesdays at 00:00 (JST)
 - Producers: Tencent Animation & Comics 
 - Licensors: None found, add some 
 - Studios: Studio Deen 
 - Source: Web manga
 - Genres: Action, Drama, Supernatural
 - Duration: 23 min. per ep.
 - Rating: PG-13 - Teens 13 or older


## Links

