# High Score Girl

![High Score Girl](https://cdn.myanimelist.net/images/anime/1668/91345l.jpg)

* Japanese:  ハイスコアガール

## Information

 - Type: TV
 - Episodes: 12
 - Status: Finished Airing
 - Aired: Jul 14, 2018 to Sep 28, 2018
 - Premiered: Summer 2018
 - Broadcast: Saturdays at 00:30 (JST)
 - Producers: Warner Bros. Japan
 - Licensors: None found, add some
 - Studios: J.C.Staff
 - Source: Manga
 - Genres: Comedy, Romance, School, Seinen
 - Duration: 24 min. per ep.
 - Rating: PG-13 - Teens 13 or older


## Links

- [[Refrain] Hi Score Girl - 01 VF VOSTFR [720p]](https://uptobox.com/wlk2b7rgtvr2)
- [[Refrain] Hi Score Girl - 01 VF VOSTFR [1080p]](https://uptobox.com/mxc2fvlj7sia)
- [[Refrain] Hi Score Girl - 02 VF VOSTFR [720p]](https://uptobox.com/3dbqg14my9z5)
- [[Refrain] Hi Score Girl - 02 VF VOSTFR [1080p]](https://uptobox.com/x87y99ax4v9o)
- [[Refrain] Hi Score Girl - 03 VF VOSTFR [720p]](https://uptobox.com/ncpoorfiebi6)
- [[Refrain] Hi Score Girl - 03 VF VOSTFR [1080p]](https://uptobox.com/uswhdbq5j3s1)
- [[Refrain] Hi Score Girl - 04 VF VOSTFR [720p]](https://uptobox.com/k8ve5ebcxnlw)
- [[Refrain] Hi Score Girl - 04 VF VOSTFR [1080p]](https://uptobox.com/gohinkaop9fp)
- [[Refrain] Hi Score Girl - 05 VF VOSTFR [1080p]](https://uptobox.com/rlglswf3jcnu)
- [[Refrain] Hi Score Girl - 06 VF VOSTFR [720p]](https://uptobox.com/qlmhxageed01)
- [[Refrain] Hi Score Girl - 06 VF VOSTFR [1080p]](https://uptobox.com/8e5m90hls9ee)
- [[Refrain] Hi Score Girl - 07 VF VOSTFR [720p]](https://uptobox.com/axcwkimukwid)
- [[Refrain] Hi Score Girl - 07 VF VOSTFR [1080p]](https://uptobox.com/0oaazse45qvy)
- [[Refrain] Hi Score Girl - 08 VF VOSTFR [720p]](https://uptobox.com/kf4iqu3x2buc)
- [[Refrain] Hi Score Girl - 08 VF VOSTFR [1080p]](https://uptobox.com/2bsynlmczr71)
- [[Refrain] Hi Score Girl - 09 VF VOSTFR [720p]](https://uptobox.com/9ywdlm0naic2)
- [[Refrain] Hi Score Girl - 09 VF VOSTFR [1080p]](https://uptobox.com/jeo4qi9zuhzm)
- [[Refrain] Hi Score Girl - 10 VF VOSTFR [720p]](https://uptobox.com/f2erqaw05iqb)
- [[Refrain] Hi Score Girl - 10 VF VOSTFR [1080p]](https://uptobox.com/bmn17vt3qwj4)
- [[Refrain] Hi Score Girl - 11 VF VOSTFR [720p]](https://uptobox.com/5gd52jysgzjy)
- [[Refrain] Hi Score Girl - 11 VF VOSTFR [1080p]](https://uptobox.com/vz9yr9282j41)
- [[Refrain] Hi Score Girl - 12 VF VOSTFR [720p]](https://uptobox.com/788opmbi0mbq)
- [[Refrain] Hi Score Girl - 12 VF VOSTFR [1080p]](https://uptobox.com/eo1aug7yxgiv)
- [[Refrain] High Score Girl - 13 VF VOSTFR [1080p]](https://uptobox.com/4whaojlvb4q0)
- [[Refrain] High Score Girl - 13 VF VOSTFR [720p]](https://uptobox.com/cda4m75wneaq)
- [[Refrain] High Score Girl - 14 VF VOSTFR [1080p]](https://uptobox.com/va6xybz2tten)
- [[Refrain] High Score Girl - 14 VF VOSTFR [720p]](https://uptobox.com/fhc4zcppge7s)
- [[Refrain] High Score Girl - 15 VF VOSTFR [1080p]](https://uptobox.com/fkvkp08wok9x)
- [[Refrain] High Score Girl - 15 VF VOSTFR [720p]](https://uptobox.com/ruc1zcqh72x4)
