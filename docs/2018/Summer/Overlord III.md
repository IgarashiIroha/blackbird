# Overlord III

![Overlord III](https://cdn.myanimelist.net/images/anime/1511/93473l.jpg)

* Japanese:  オーバーロードⅢ

## Information

 - Type: TV
 - Episodes: 13
 - Status: Finished Airing
 - Aired: Jul 10, 2018 to Oct 2, 2018
 - Premiered: Summer 2018
 - Broadcast: Tuesdays at 22:00 (JST)
 - Producers: None found, add some 
 - Licensors: Funimation 
 - Studios: Madhouse 
 - Source: Light novel
 - Genres: Action, Fantasy, Game, Magic, Supernatural
 - Duration: 23 min. per ep.
 - Rating: R - 17+ (violence & profanity)


## Links

