# Yume Oukoku to Nemureru 100-nin no Ouji-sama

![Yume Oukoku to Nemureru 100-nin no Ouji-sama](https://cdn.myanimelist.net/images/anime/1526/95041l.jpg)

* Japanese:  夢王国と眠れる100人の王子様

## Information

 - Type: TV
 - Episodes: 12
 - Status: Finished Airing
 - Aired: Jul 5, 2018 to Sep 20, 2018
 - Premiered: Summer 2018
 - Broadcast: Thursdays at 22:30 (JST)
 - Producers: dugout 
 - Licensors: Sentai Filmworks 
 - Studios: Project No.9 
 - Source: Game
 - Genres: Adventure, Fantasy
 - Duration: 23 min. per ep.
 - Rating: PG-13 - Teens 13 or older


## Links

