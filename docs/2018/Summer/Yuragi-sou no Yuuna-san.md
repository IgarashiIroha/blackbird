# Yuragi-sou no Yuuna-san

![Yuragi-sou no Yuuna-san](https://cdn.myanimelist.net/images/anime/1483/93141l.jpg)

* Japanese:  ゆらぎ荘の幽奈さん

## Information

 - Type: TV
 - Episodes: 12
 - Status: Finished Airing
 - Aired: Jul 14, 2018 to Sep 29, 2018
 - Premiered: Summer 2018
 - Broadcast: Saturdays at 23:30 (JST)
 - Producers: Aniplex, Shueisha
 - Licensors: Aniplex of America
 - Studios: Xebec
 - Source: Manga
 - Genres: Harem, Comedy, Supernatural, Romance, Ecchi, Shounen
 - Duration: 23 min. per ep.
 - Rating: PG-13 - Teens 13 or older


## Links

- [[Refrain] Yuragi-sou no Yuuna-san - 01 VOSTFR [720p]](https://uptobox.com/p5zz474e2z35)
- [[Refrain] Yuragi-sou no Yuuna-san - 01 VOSTFR [1080p]](https://uptobox.com/tx0hqvgjz6r1)
- [[Refrain] Yuragi-sou no Yuuna-san - 02 VOSTFR [720p]](https://uptobox.com/jblxge7uux1a)
- [[Refrain] Yuragi-sou no Yuuna-san - 02 VOSTFR [1080p]](https://uptobox.com/azu7g0ymyo6b)
- [[Refrain] Yuragi-sou no Yuuna-san - 03 VOSTFR [720p]](https://uptobox.com/kk2cavyc4pse)
- [[Refrain] Yuragi-sou no Yuuna-san - 03 VOSTFR [1080p]](https://uptobox.com/graq97ds0ucv)
- [[Refrain] Yuragi-sou no Yuuna-san - 04 VOSTFR [720p]](https://uptobox.com/ajis0rljrbf0)
- [[Refrain] Yuragi-sou no Yuuna-san - 04 VOSTFR [1080p]](https://uptobox.com/vf6gw3pre56u)
- [[Refrain] Yuragi-sou no Yuuna-san - 05 VOSTFR [720p]](https://uptobox.com/xby8cx3btith)
- [[Refrain] Yuragi-sou no Yuuna-san - 05 VOSTFR [1080p]](https://uptobox.com/1i511rkh2u6y)
- [[Refrain] Yuragi-sou no Yuuna-san - 06 VOSTFR [720p]](https://uptobox.com/dkuvzfnknjki)
- [[Refrain] Yuragi-sou no Yuuna-san - 06 VOSTFR [1080p]](https://uptobox.com/tpi9oxkbj59t)
- [[Refrain] Yuragi-sou no Yuuna-san - 07 VOSTFR [720p]](https://uptobox.com/yo0tupp2hje1)
- [[Refrain] Yuragi-sou no Yuuna-san - 07 VOSTFR [1080p]](https://uptobox.com/8b8e5r1dcfp8)
- [[Refrain] Yuragi-sou no Yuuna-san - 08 VOSTFR [720p]](https://uptobox.com/m5d8dlxfmxlr)
- [[Refrain] Yuragi-sou no Yuuna-san - 08 VOSTFR [1080p]](https://uptobox.com/g9jmnv7lx5w1)
- [[Refrain] Yuragi-sou no Yuuna-san - 09 VOSTFR [720p]](https://uptobox.com/mjmogwvz2g7a)
- [[Refrain] Yuragi-sou no Yuuna-san - 09 VOSTFR [1080p]](https://uptobox.com/hwlu7tqwe5mk)
- [[Refrain] Yuragi-sou no Yuuna-san - 10 VOSTFR [720p]](https://uptobox.com/zk5kjzzh9yde)
- [[Refrain] Yuragi-sou no Yuuna-san - 10 VOSTFR [1080p]](https://uptobox.com/yxns18vv6zr7)
- [[Refrain] Yuragi-sou no Yuuna-san - 11 VO [720p]](https://uptobox.com/sd5zucvnohvs)
- [[Refrain] Yuragi-sou no Yuuna-san - 11 VO [1080p]](https://uptobox.com/trdg9ouqgy10)
- [[Refrain] Yuragi-sou no Yuuna-san - 11 VOSTFR [720p]](https://uptobox.com/78rh7s6pm31p)
- [[Refrain] Yuragi-sou no Yuuna-san - 11 VOSTFR [1080p]](https://uptobox.com/vpr18xrqf6jx)
- [[Refrain] Yuragi-sou no Yuuna-san - 12 VO [720p]](https://uptobox.com/rtrenppwuiow)
- [[Refrain] Yuragi-sou no Yuuna-san - 12 VO [1080p]](https://uptobox.com/pi9l7burlyns)
- [[Refrain] Yuragi-sou no Yuuna-san - 12 VOSTFR [720p]](https://uptobox.com/p1rpz69ocy6h)
- [[Refrain] Yuragi-sou no Yuuna-san - 12 VOSTFR [1080p]](https://uptobox.com/u6e42t7fjj63)
