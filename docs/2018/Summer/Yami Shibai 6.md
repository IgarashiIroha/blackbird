# Yami Shibai 6

![Yami Shibai 6](https://cdn.myanimelist.net/images/anime/1911/95045l.jpg)

* Japanese:  闇芝居 6期

## Information

 - Type: TV
 - Episodes: 13
 - Status: Finished Airing
 - Aired: Jul 7, 2018 to Sep 29, 2018
 - Premiered: Summer 2018
 - Broadcast: Saturdays at 04:00 (JST)
 - Producers: TV Tokyo
 - Licensors: None found, add some
 - Studios: ILCA
 - Source: Original
 - Genres: Dementia, Horror, Supernatural
 - Duration: 4 min. per ep.
 - Rating: PG-13 - Teens 13 or older


## Links

- [[Refrain] Yami Shibai 6th Season - 01 VOSTFR [720p]](https://uptobox.com/i1w7pohvwjfb)
- [[Refrain] Yami Shibai 6th Season - 01 VOSTFR [1080p]](https://uptobox.com/23k47vbya5bf)
- [[Refrain] Yami Shibai 6th Season - 02 VOSTFR [720p]](https://uptobox.com/jwns4lk4x5nj)
- [[Refrain] Yami Shibai 6th Season - 02 VOSTFR [1080p]](https://uptobox.com/px8bzzs1z2mo)
- [[Refrain] Yami Shibai 6th Season - 03 VOSTFR [720p]](https://uptobox.com/cilh0lc9i5zc)
- [[Refrain] Yami Shibai 6th Season - 03 VOSTFR [1080p]](https://uptobox.com/3ofa8d8x0r9e)
- [[Refrain] Yami Shibai 6th Season - 04 VOSTFR [720p]](https://uptobox.com/6ok05x3h1n2l)
- [[Refrain] Yami Shibai 6th Season - 04 VOSTFR [1080p]](https://uptobox.com/a751vg4rwbvu)
- [[Refrain] Yami Shibai 6th Season - 05 VOSTFR [720p]](https://uptobox.com/dkq86oamoz7c)
- [[Refrain] Yami Shibai 6th Season - 05 VOSTFR [1080p]](https://uptobox.com/yu4kzbruivg2)
- [[Refrain] Yami Shibai 6th Season - 06 VOSTFR [720p]](https://uptobox.com/gdee467xgr2s)
- [[Refrain] Yami Shibai 6th Season - 06 VOSTFR [1080p]](https://uptobox.com/8x2kfusv1igb)
- [[Refrain] Yami Shibai 6th Season - 07 VOSTFR [720p]](https://uptobox.com/0nll0asdmlch)
- [[Refrain] Yami Shibai 6th Season - 07 VOSTFR [1080p]](https://uptobox.com/jw0d47hmagb5)
- [[Refrain] Yami Shibai 6th Season - 08 VOSTFR [720p]](https://uptobox.com/swi5tot1g0gh)
- [[Refrain] Yami Shibai 6th Season - 08 VOSTFR [1080p]](https://uptobox.com/xllis52kmize)
- [[Refrain] Yami Shibai 6th Season - 09 VOSTFR [720p]](https://uptobox.com/azki4vy18ysm)
- [[Refrain] Yami Shibai 6th Season - 09 VOSTFR [1080p]](https://uptobox.com/lgayk9uyb1tm)
- [[Refrain] Yami Shibai 6th Season - 10 VOSTFR [720p]](https://uptobox.com/dct44aj71pe2)
- [[Refrain] Yami Shibai 6th Season - 10 VOSTFR [1080p]](https://uptobox.com/sgs7xjsh2hrf)
- [[Refrain] Yami Shibai 6th Season - 11 VOSTFR [720p]](https://uptobox.com/zf9o01ebbqz2)
- [[Refrain] Yami Shibai 6th Season - 11 VOSTFR [1080p]](https://uptobox.com/pg14cnew06u5)
- [[Refrain] Yami Shibai 6th Season - 12 VOSTFR [720p]](https://uptobox.com/phzdki0ddvuf)
- [[Refrain] Yami Shibai 6th Season - 12 VOSTFR [1080p]](https://uptobox.com/t68x1lirz875)
- [[Refrain] Yami Shibai 6th Season - 13 VOSTFR [720p]](https://uptobox.com/owlazm7zukws)
- [[Refrain] Yami Shibai 6th Season - 13 VOSTFR [1080p]](https://uptobox.com/c8fzn4x4d0sp)
