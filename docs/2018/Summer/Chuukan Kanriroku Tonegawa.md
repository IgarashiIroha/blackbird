# Chuukan Kanriroku Tonegawa

![Chuukan Kanriroku Tonegawa](https://cdn.myanimelist.net/images/anime/1072/93145l.jpg)

* Japanese:  中間管理録トネガワ

## Information

 - Type: TV
 - Episodes: 24
 - Status: Finished Airing
 - Aired: Jul 4, 2018 to Dec 26, 2018
 - Premiered: Summer 2018
 - Broadcast: Wednesdays at 00:00 (JST)
 - Producers: VAP, Half H.P Studio, Nippon Television Network, Nippon Television Music
 - Licensors: Sentai Filmworks
 - Studios: Madhouse
 - Source: Manga
 - Genres: Comedy, Parody, Psychological, Drama, Seinen
 - Duration: 23 min. per ep.
 - Rating: PG-13 - Teens 13 or older


## Links

- [[Refrain] Chuukan Kanriroku Tonegawa - 01 VOSTFR [720p]](https://uptobox.com/5480o3fva33x)
- [[Refrain] Chuukan Kanriroku Tonegawa - 01 VOSTFR [1080p]](https://uptobox.com/0d8lsyyurs7g)
- [[Refrain] Chuukan Kanriroku Tonegawa - 02 VOSTFR [720p]](https://uptobox.com/k20f2xrs1xb5)
- [[Refrain] Chuukan Kanriroku Tonegawa - 02 VOSTFR [1080p]](https://uptobox.com/cbgl1hh6isdo)
- [[Refrain] Chuukan Kanriroku Tonegawa - 03 VOSTFR [720p]](https://uptobox.com/o6om20l7ytq2)
- [[Refrain] Chuukan Kanriroku Tonegawa - 03 VOSTFR [1080p]](https://uptobox.com/u2a31fowsyx5)
- [[Refrain] Chuukan Kanriroku Tonegawa - 04 VOSTFR [720p]](https://uptobox.com/yh5oej17o2oq)
- [[Refrain] Chuukan Kanriroku Tonegawa - 04 VOSTFR [1080p]](https://uptobox.com/tff4zoa7ypic)
- [[Refrain] Chuukan Kanriroku Tonegawa - 05 VOSTFR [720p]](https://uptobox.com/yc6tyeajcold)
- [[Refrain] Chuukan Kanriroku Tonegawa - 05 VOSTFR [1080p]](https://uptobox.com/jqfgcx0y3znz)
- [[Refrain] Chuukan Kanriroku Tonegawa - 06 VOSTFR [720p]](https://uptobox.com/lfajm8rofc3k)
- [[Refrain] Chuukan Kanriroku Tonegawa - 06 VOSTFR [1080p]](https://uptobox.com/rl8suljyk4mr)
- [[Refrain] Chuukan Kanriroku Tonegawa - 07 VOSTFR [720p]](https://uptobox.com/59ov5pyin5lt)
- [[Refrain] Chuukan Kanriroku Tonegawa - 07 VOSTFR [1080p]](https://uptobox.com/2tcwsydw7cdo)
- [[Refrain] Chuukan Kanriroku Tonegawa - 08 VOSTFR [720p]](https://uptobox.com/qs10z14l2f4r)
- [[Refrain] Chuukan Kanriroku Tonegawa - 08 VOSTFR [1080p]](https://uptobox.com/t5umnzlsb4to)
- [[Refrain] Chuukan Kanriroku Tonegawa - 09 VOSTFR [720p]](https://uptobox.com/3vlmirrvcpap)
- [[Refrain] Chuukan Kanriroku Tonegawa - 09 VOSTFR [1080p]](https://uptobox.com/hwgb3sybengg)
- [[Refrain] Chuukan Kanriroku Tonegawa - 10 VOSTFR [720p]](https://uptobox.com/i395ptsu7qh4)
- [[Refrain] Chuukan Kanriroku Tonegawa - 10 VOSTFR [1080p]](https://uptobox.com/jpe6p0mdgu2j)
- [[Refrain] Chuukan Kanriroku Tonegawa - 11 VOSTFR [720p]](https://uptobox.com/2eufstuyx93v)
- [[Refrain] Chuukan Kanriroku Tonegawa - 11 VOSTFR [1080p]](https://uptobox.com/v4a7uhos9k6m)
- [[Refrain] Chuukan Kanriroku Tonegawa - 12 VOSTFR [720p]](https://uptobox.com/lirlyir8og93)
- [[Refrain] Chuukan Kanriroku Tonegawa - 12 VOSTFR [1080p]](https://uptobox.com/j67p8f0gqg2m)
- [[Refrain] Chuukan Kanriroku Tonegawa - 13 VOSTFR [720p]](https://uptobox.com/1eihuo1dqi5q)
- [[Refrain] Chuukan Kanriroku Tonegawa - 13 VOSTFR [1080p]](https://uptobox.com/8dozdxks68y6)
- [[Refrain] Chuukan Kanriroku Tonegawa - 14 VOSTFR [720p]](https://uptobox.com/8gz571i9s0z9)
- [[Refrain] Chuukan Kanriroku Tonegawa - 14 VOSTFR [1080p]](https://uptobox.com/jsc8cfjm8jtc)
- [[Refrain] Chuukan Kanriroku Tonegawa - 15 VOSTFR [720p]](https://uptobox.com/bbaxrqx4gy71)
- [[Refrain] Chuukan Kanriroku Tonegawa - 15 VOSTFR [1080p]](https://uptobox.com/fbvfrzyq3s1e)
- [[Refrain] Chuukan Kanriroku Tonegawa - 16 VOSTFR [720p]](https://uptobox.com/o7is4w672g2k)
- [[Refrain] Chuukan Kanriroku Tonegawa - 16 VOSTFR [1080p]](https://uptobox.com/sem7d3slz58q)
- [[Refrain] Chuukan Kanriroku Tonegawa - 17 VOSTFR [720p]](https://uptobox.com/r5cam1srjo0i)
- [[Refrain] Chuukan Kanriroku Tonegawa - 17 VOSTFR [1080p]](https://uptobox.com/kpuypeuxsv5f)
- [[Refrain] Chuukan Kanriroku Tonegawa - 18 VOSTFR [720p]](https://uptobox.com/fuf7s1yqjl6d)
- [[Refrain] Chuukan Kanriroku Tonegawa - 18 VOSTFR [1080p]](https://uptobox.com/qu5hioh0bpk2)
- [[Refrain] Chuukan Kanriroku Tonegawa - 19 VOSTFR [720p]](https://uptobox.com/tpwlb7754x3u)
- [[Refrain] Chuukan Kanriroku Tonegawa - 19 VOSTFR [1080p]](https://uptobox.com/izoh9n87wgok)
- [[Refrain] Chuukan Kanriroku Tonegawa - 20 VOSTFR [720p]](https://uptobox.com/ret2isl45g5m)
- [[Refrain] Chuukan Kanriroku Tonegawa - 20 VOSTFR [1080p]](https://uptobox.com/y30bbqy7t6i7)
- [[Refrain] Chuukan Kanriroku Tonegawa - 21 VOSTFR [720p]](https://uptobox.com/pzjbyh6v4oec)
- [[Refrain] Chuukan Kanriroku Tonegawa - 21 VOSTFR [1080p]](https://uptobox.com/qrobj4pt0yz8)
- [[Refrain] Chuukan Kanriroku Tonegawa - 22 VOSTFR [720p]](https://uptobox.com/ri5l427e205i)
- [[Refrain] Chuukan Kanriroku Tonegawa - 22 VOSTFR [1080p]](https://uptobox.com/9djoiqul2yl5)
- [[Refrain] Chuukan Kanriroku Tonegawa - 23 VOSTFR [720p]](https://uptobox.com/iasy1jrw5npz)
- [[Refrain] Chuukan Kanriroku Tonegawa - 23 VOSTFR [1080p]](https://uptobox.com/hghabb52cd6f)
- [[Refrain] Chuukan Kanriroku Tonegawa - 24 VOSTFR [720p]](https://uptobox.com/gcicndae6ukc)
- [[Refrain] Chuukan Kanriroku Tonegawa - 24 VOSTFR [1080p]](https://uptobox.com/fkcj4l3y5sa4)
