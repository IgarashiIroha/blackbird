# Island

![Island](https://cdn.myanimelist.net/images/anime/1951/95080l.jpg)

* Japanese:  ISLAND

## Information

 - Type: TV
 - Episodes: 12
 - Status: Finished Airing
 - Aired: Jul 1, 2018 to Sep 16, 2018
 - Premiered: Summer 2018
 - Broadcast: Sundays at 22:00 (JST)
 - Producers: Crunchyroll SC Anime Fund
 - Licensors: Funimation
 - Studios: feel.
 - Source: Visual novel
 - Genres: Sci-Fi, Drama
 - Duration: 23 min. per ep.
 - Rating: PG-13 - Teens 13 or older


## Links

- [[Refrain] Island - 01 VOSTFR [720p]](https://uptobox.com/j6g41kynruuv)
- [[Refrain] Island - 01 VOSTFR [1080p]](https://uptobox.com/p9mk4kr6waq2)
- [[Refrain] Island - 02 VOSTFR [720p]](https://uptobox.com/1fa7dq2tf32g)
- [[Refrain] Island - 02 VOSTFR [1080p]](https://uptobox.com/lt8gl52de5gz)
- [[Refrain] Island - 03 VOSTFR [720p]](https://uptobox.com/d4rb1to5nr38)
- [[Refrain] Island - 03 VOSTFR [1080p]](https://uptobox.com/qwa08couhzbg)
- [[Refrain] Island - 04 VOSTFR [720p]](https://uptobox.com/bg3sqz41wwlb)
- [[Refrain] Island - 04 VOSTFR [1080p]](https://uptobox.com/7nvh29qpod1u)
- [[Refrain] Island - 05 VOSTFR [720p]](https://uptobox.com/x7nokggmvkkb)
- [[Refrain] Island - 05 VOSTFR [1080p]](https://uptobox.com/he4b0j0ahshy)
- [[Refrain] Island - 06 VOSTFR [720p]](https://uptobox.com/ebnmx33p58cb)
- [[Refrain] Island - 06 VOSTFR [1080p]](https://uptobox.com/no3qzr9x8mrf)
- [[Refrain] Island - 07 VOSTFR [720p]](https://uptobox.com/t5smu26cxe81)
- [[Refrain] Island - 07 VOSTFR [1080p]](https://uptobox.com/9em08kenzlxz)
- [[Refrain] Island - 08 VOSTFR [720p]](https://uptobox.com/9zjfh5012vzo)
- [[Refrain] Island - 08 VOSTFR [1080p]](https://uptobox.com/apd9ui1pff3p)
- [[Refrain] Island - 09 VOSTFR [720p]](https://uptobox.com/iip92van9u7l)
- [[Refrain] Island - 09 VOSTFR [1080p]](https://uptobox.com/xv1u623fr9tc)
- [[Refrain] Island - 10 VOSTFR [720p]](https://uptobox.com/prab2j8nlc4x)
- [[Refrain] Island - 10 VOSTFR [1080p]](https://uptobox.com/b6mde1jpjskt)
- [[Refrain] Island - 11 VOSTFR [720p]](https://uptobox.com/4us3sl8vmhec)
- [[Refrain] Island - 11 VOSTFR [1080p]](https://uptobox.com/ku0x0v0nxwi3)
- [[Refrain] Island - 12 VOSTFR [720p]](https://uptobox.com/nci82ythvvlw)
- [[Refrain] Island - 12 VOSTFR [1080p]](https://uptobox.com/d51ihwabubhr)
