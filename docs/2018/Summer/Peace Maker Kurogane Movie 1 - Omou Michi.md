# Peace Maker Kurogane Movie 1: Omou Michi

![Peace Maker Kurogane Movie 1: Omou Michi](https://cdn.myanimelist.net/images/anime/1379/91338l.jpg)

* Japanese:  PEACE MAKER 鐵 前編～想道〈オモウミチ〉～

## Information

 - Type: Movie
 - Episodes: 1
 - Status: Finished Airing
 - Aired: Jun 2, 2018
 - Producers: None found, add some 
 - Licensors: None found, add some 
 - Studios: White Fox 
 - Source: Manga
 - Genres: Action, Comedy, Historical, Samurai, Shounen
 - Duration: Unknown
 - Rating: R - 17+ (violence & profanity)


## Links

- [[Refrain] Peace Maker Kurogane Movie 1 - Omou Michi - 00 VOSTFR [720p]](https://uptobox.com/gztlt0sikdb9)
- [[Refrain] Peace Maker Kurogane Movie 1 - Omou Michi - 00 VOSTFR [1080p]](https://uptobox.com/va1aqcxb8z77)
- [ Friend - 00 VOSTFR [720p]](https://uptobox.com/xyaqbgwf7j7j)
