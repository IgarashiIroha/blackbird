# Yakusoku no Nanaya Matsuri

![Yakusoku no Nanaya Matsuri](https://cdn.myanimelist.net/images/anime/1473/94594l.jpg)

* Japanese:  約束の七夜祭り

## Information

 - Type: ONA
 - Episodes: 1
 - Status: Finished Airing
 - Aired: Aug 3, 2018
 - Producers: XFLAG
 - Licensors: None found, add some
 - Studios: Yokohama Animation Lab
 - Source: Original
 - Genres: Drama, Samurai, Sci-Fi
 - Duration: 1 hr. 2 min.
 - Rating: PG-13 - Teens 13 or older


## Links

- [[Refrain] Yakusoku no Nanaya Matsuri - 01 VOSTFR [720p]](https://uptobox.com/germtzqvpgkw)
- [[Refrain] Yakusoku no Nanaya Matsuri - 01 VOSTFR [1080p]](https://uptobox.com/e5pgdeuoxmjn)
