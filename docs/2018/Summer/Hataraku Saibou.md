# Hataraku Saibou

![Hataraku Saibou](https://cdn.myanimelist.net/images/anime/1270/95221l.jpg)

* Japanese:  はたらく細胞

## Information

 - Type: TV
 - Episodes: 13
 - Status: Finished Airing
 - Aired: Jul 8, 2018 to Sep 30, 2018
 - Premiered: Summer 2018
 - Broadcast: Sundays at 00:00 (JST)
 - Producers: Aniplex, Kodansha, Magic Capsule
 - Licensors: Aniplex of America
 - Studios: David Production
 - Source: Manga
 - Genres: Comedy, Shounen
 - Duration: 23 min. per ep.
 - Rating: PG-13 - Teens 13 or older


## Links

- [[Refrain] Hataraku Saibou (TV) - 01 VOSTFR [720p]](https://uptobox.com/arwn9867isnf)
- [[Refrain] Hataraku Saibou (TV) - 01 VOSTFR [1080p]](https://uptobox.com/md5rutpvkbf3)
- [[Refrain] Hataraku Saibou (TV) - 02 VOSTFR [720p]](https://uptobox.com/7dqjmdxtks6c)
- [[Refrain] Hataraku Saibou (TV) - 02 VOSTFR [1080p]](https://uptobox.com/bi3gnj6drd2t)
- [[Refrain] Hataraku Saibou (TV) - 03 VOSTFR [720p]](https://uptobox.com/mx6uosbt9x67)
- [[Refrain] Hataraku Saibou (TV) - 03 VOSTFR [1080p]](https://uptobox.com/x1bjil85qgti)
- [[Refrain] Hataraku Saibou (TV) - 04 VOSTFR [720p]](https://uptobox.com/2eo1jqkhm4vp)
- [[Refrain] Hataraku Saibou (TV) - 04 VOSTFR [1080p]](https://uptobox.com/pgrbb3wdoesl)
- [[Refrain] Hataraku Saibou (TV) - 05 VOSTFR [720p]](https://uptobox.com/n6h9he1dwxhc)
- [[Refrain] Hataraku Saibou (TV) - 05 VOSTFR [1080p]](https://uptobox.com/krziojb59cbi)
- [[Refrain] Hataraku Saibou (TV) - 06 VOSTFR [720p]](https://uptobox.com/i9eii152dfdu)
- [[Refrain] Hataraku Saibou (TV) - 06 VOSTFR [1080p]](https://uptobox.com/ybfvic7chjrr)
- [[Refrain] Hataraku Saibou (TV) - 07 VOSTFR [720p]](https://uptobox.com/cibtz6nm96v1)
- [[Refrain] Hataraku Saibou (TV) - 07 VOSTFR [1080p]](https://uptobox.com/i6i5i34h2i09)
- [[Refrain] Hataraku Saibou (TV) - 08 VOSTFR [720p]](https://uptobox.com/sa1tgx30iazl)
- [[Refrain] Hataraku Saibou (TV) - 08 VOSTFR [1080p]](https://uptobox.com/wzltrkdx1pja)
- [[Refrain] Hataraku Saibou (TV) - 09 VOSTFR [720p]](https://uptobox.com/2tgvzxwdiim2)
- [[Refrain] Hataraku Saibou (TV) - 09 VOSTFR [1080p]](https://uptobox.com/agmragwbxsgb)
- [[Refrain] Hataraku Saibou (TV) - 10 VOSTFR [720p]](https://uptobox.com/99swh1awbv6l)
- [[Refrain] Hataraku Saibou (TV) - 10 VOSTFR [1080p]](https://uptobox.com/hdwotgn3cuab)
- [[Refrain] Hataraku Saibou (TV) - 11 VOSTFR [720p]](https://uptobox.com/g3iu1miez5c3)
- [[Refrain] Hataraku Saibou (TV) - 11 VOSTFR [1080p]](https://uptobox.com/x72krc6z86s9)
- [[Refrain] Hataraku Saibou (TV) - 12 VOSTFR [720p]](https://uptobox.com/l8p4kphl65qs)
- [[Refrain] Hataraku Saibou (TV) - 12 VOSTFR [1080p]](https://uptobox.com/w9mqryx58t5y)
- [[Refrain] Hataraku Saibou (TV) - 13 VOSTFR [720p]](https://uptobox.com/6blyonz7srqz)
- [[Refrain] Hataraku Saibou (TV) - 13 VOSTFR [1080p]](https://uptobox.com/iu2lt2b0xswu)
- [[Refrain] Hataraku Saibou - Special VOSTFR [720p]](https://uptobox.com/lctmj3f1uoyk)
- [[Refrain] Hataraku Saibou - Special VOSTFR [1080p]](https://uptobox.com/uoqxtdr4owco)
