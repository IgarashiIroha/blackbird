# Tsukumogami Kashimasu

![Tsukumogami Kashimasu](https://cdn.myanimelist.net/images/anime/1715/91114l.jpg)

* Japanese:  つくもがみ貸します

## Information

 - Type: TV
 - Episodes: 12
 - Status: Finished Airing
 - Aired: Jul 23, 2018 to Oct 15, 2018
 - Premiered: Summer 2018
 - Broadcast: Mondays at 00:10 (JST)
 - Producers: TMS Entertainment, NHK
 - Licensors: None found, add some
 - Studios: Telecom Animation Film
 - Source: Novel
 - Genres: Slice of Life, Comedy, Historical, Demons, Supernatural
 - Duration: 25 min. per ep.
 - Rating: PG-13 - Teens 13 or older


## Links

- [[Refrain] Tsukumogami Kashimasu - 01 VOSTFR [720p]](https://uptobox.com/danflrv2n8g2)
- [[Refrain] Tsukumogami Kashimasu - 01 VOSTFR [1080p]](https://uptobox.com/gjjki9sy9kw2)
- [[Refrain] Tsukumogami Kashimasu - 01 VOSTFR {ADN} [720p]](https://uptobox.com/z1oxj8f5m48p)
- [[Refrain] Tsukumogami Kashimasu - 01 VOSTFR {ADN} [1080p]](https://uptobox.com/dtrp4zpq035t)
- [[Refrain] Tsukumogami Kashimasu - 02 VOSTFR [720p]](https://uptobox.com/13cav964o9g5)
- [[Refrain] Tsukumogami Kashimasu - 02 VOSTFR [1080p]](https://uptobox.com/2m19rjm1r2w2)
- [[Refrain] Tsukumogami Kashimasu - 02 VOSTFR {ADN} [720p]](https://uptobox.com/kt21yjiwdbuj)
- [[Refrain] Tsukumogami Kashimasu - 02 VOSTFR {ADN} [1080p]](https://uptobox.com/2vl10nfe4f5z)
- [[Refrain] Tsukumogami Kashimasu - 03 VOSTFR [720p]](https://uptobox.com/w5w3wsbgc3bp)
- [[Refrain] Tsukumogami Kashimasu - 03 VOSTFR [1080p]](https://uptobox.com/jnwy7tidonz6)
- [[Refrain] Tsukumogami Kashimasu - 03 VOSTFR {ADN} [720p]](https://uptobox.com/9thi5qjynrff)
- [[Refrain] Tsukumogami Kashimasu - 03 VOSTFR {ADN} [1080p]](https://uptobox.com/wrbxxvc6y2j9)
- [[Refrain] Tsukumogami Kashimasu - 04 VOSTFR [720p]](https://uptobox.com/txvarda2s8ub)
- [[Refrain] Tsukumogami Kashimasu - 04 VOSTFR [1080p]](https://uptobox.com/vy8ctwzhyvww)
- [[Refrain] Tsukumogami Kashimasu - 04 VOSTFR {ADN} [720p]](https://uptobox.com/up5zzi2e8knn)
- [[Refrain] Tsukumogami Kashimasu - 04 VOSTFR {ADN} [1080p]](https://uptobox.com/fqh9qx0kdk7r)
- [[Refrain] Tsukumogami Kashimasu - 05 VOSTFR [720p]](https://uptobox.com/wh4b7dz5c3ek)
- [[Refrain] Tsukumogami Kashimasu - 05 VOSTFR [1080p]](https://uptobox.com/o9y6z7n5uvk9)
- [[Refrain] Tsukumogami Kashimasu - 05 VOSTFR {ADN} [720p]](https://uptobox.com/va2l6eixwnes)
- [[Refrain] Tsukumogami Kashimasu - 05 VOSTFR {ADN} [1080p]](https://uptobox.com/6dzqr2d9tw5u)
- [[Refrain] Tsukumogami Kashimasu - 06 VOSTFR [720p]](https://uptobox.com/4uvhg8akfibh)
- [[Refrain] Tsukumogami Kashimasu - 06 VOSTFR [1080p]](https://uptobox.com/29cnw2nzumpe)
- [[Refrain] Tsukumogami Kashimasu - 06 VOSTFR {ADN} [720p]](https://uptobox.com/6cojh8jk0zi6)
- [[Refrain] Tsukumogami Kashimasu - 06 VOSTFR {ADN} [1080p]](https://uptobox.com/dpaz6husu0tw)
- [[Refrain] Tsukumogami Kashimasu - 07 VOSTFR [720p]](https://uptobox.com/jsy0101nqzzs)
- [[Refrain] Tsukumogami Kashimasu - 07 VOSTFR [1080p]](https://uptobox.com/0pynbxio6gos)
- [[Refrain] Tsukumogami Kashimasu - 07 VOSTFR {ADN} [720p]](https://uptobox.com/83wmpfzflojf)
- [[Refrain] Tsukumogami Kashimasu - 07 VOSTFR {ADN} [1080p]](https://uptobox.com/05r6drh9kky3)
- [[Refrain] Tsukumogami Kashimasu - 08 VOSTFR [720p]](https://uptobox.com/c3v2vp9hty3g)
- [[Refrain] Tsukumogami Kashimasu - 08 VOSTFR [1080p]](https://uptobox.com/tg1wwm6wc8ak)
- [[Refrain] Tsukumogami Kashimasu - 08 VOSTFR {ADN} [720p]](https://uptobox.com/yk27e09chm26)
- [[Refrain] Tsukumogami Kashimasu - 08 VOSTFR {ADN} [1080p]](https://uptobox.com/c3uitgvbfha6)
- [[Refrain] Tsukumogami Kashimasu - 09 VOSTFR [720p]](https://uptobox.com/ztpu471supnl)
- [[Refrain] Tsukumogami Kashimasu - 09 VOSTFR [1080p]](https://uptobox.com/rgrxvmwnaf1b)
- [[Refrain] Tsukumogami Kashimasu - 09 VOSTFR {ADN} [720p]](https://uptobox.com/j9y3l6gjryb0)
- [[Refrain] Tsukumogami Kashimasu - 09 VOSTFR {ADN} [1080p]](https://uptobox.com/y9hgu0x5ax4x)
- [[Refrain] Tsukumogami Kashimasu - 10 VOSTFR [720p]](https://uptobox.com/3t06b1bh6ixw)
- [[Refrain] Tsukumogami Kashimasu - 10 VOSTFR [1080p]](https://uptobox.com/t9h21ilxyihf)
- [[Refrain] Tsukumogami Kashimasu - 10 VOSTFR {ADN} [720p]](https://uptobox.com/8k0wtj2oumng)
- [[Refrain] Tsukumogami Kashimasu - 10 VOSTFR {ADN} [1080p]](https://uptobox.com/zcwtricn439q)
- [[Refrain] Tsukumogami Kashimasu - 11 VOSTFR [720p]](https://uptobox.com/a8y945gooeip)
- [[Refrain] Tsukumogami Kashimasu - 11 VOSTFR [1080p]](https://uptobox.com/4nidhvb3zyu4)
- [[Refrain] Tsukumogami Kashimasu - 11 VOSTFR {ADN} [720p]](https://uptobox.com/b3jmpsyngcsg)
- [[Refrain] Tsukumogami Kashimasu - 11 VOSTFR {ADN} [1080p]](https://uptobox.com/yd22w6ojzbw1)
- [[Refrain] Tsukumogami Kashimasu - 12 VOSTFR [720p]](https://uptobox.com/fsd1ct2ix744)
- [[Refrain] Tsukumogami Kashimasu - 12 VOSTFR [1080p]](https://uptobox.com/iadc4u4zedev)
- [[Refrain] Tsukumogami Kashimasu - 12 VOSTFR {ADN} [720p]](https://uptobox.com/c7xiwhxwif7k)
- [[Refrain] Tsukumogami Kashimasu - 12 VOSTFR {ADN} [1080p]](https://uptobox.com/7kr1i21odwxl)
