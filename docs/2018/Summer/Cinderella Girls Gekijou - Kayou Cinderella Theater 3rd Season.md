# Cinderella Girls Gekijou: Kayou Cinderella Theater 3rd Season

![Cinderella Girls Gekijou: Kayou Cinderella Theater 3rd Season](https://cdn.myanimelist.net/images/anime/1652/90873l.jpg)

* Japanese:  シンデレラガールズ劇場 火曜シンデレラシアター 第3期

## Information

 - Type: TV
 - Episodes: 7
 - Status: Finished Airing
 - Aired: Jul 3, 2018 to Sep 25, 2018
 - Premiered: Summer 2018
 - Broadcast: Tuesdays at 21:54 (JST)
 - Producers: None found, add some 
 - Licensors: Crunchyroll 
 - Studios: Gathering, Lesprit 
 - Source: 4-koma manga
 - Genres: Comedy, Slice of Life
 - Duration: 1 min. per ep.
 - Rating: PG-13 - Teens 13 or older


## Links

