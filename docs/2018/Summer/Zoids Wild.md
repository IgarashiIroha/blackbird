# Zoids Wild

![Zoids Wild](https://cdn.myanimelist.net/images/anime/1792/95048l.jpg)

* Japanese:  ゾイドワイルド

## Information

 - Type: TV
 - Episodes: Unknown
 - Status: Currently Airing
 - Aired: Jul 7, 2018 to ?
 - Premiered: Summer 2018
 - Broadcast: Saturdays at 06:30 (JST)
 - Producers: None found, add some 
 - Licensors: None found, add some 
 - Studios: OLM 
 - Source: Other
 - Genres: Action, Adventure, Comedy, Mecha, Sci-Fi
 - Duration: 24 min.
 - Rating: PG - Children


## Links

