# Asobi Asobase

![Asobi Asobase](https://cdn.myanimelist.net/images/anime/1139/95077l.jpg)

* Japanese:  あそびあそばせ

## Information

 - Type: TV
 - Episodes: 12
 - Status: Finished Airing
 - Aired: Jul 8, 2018 to Sep 23, 2018
 - Premiered: Summer 2018
 - Broadcast: Sundays at 21:00 (JST)
 - Producers: None found, add some
 - Licensors: None found, add some
 - Studios: Lerche
 - Source: Manga
 - Genres: Comedy, School
 - Duration: 23 min. per ep.
 - Rating: PG-13 - Teens 13 or older


## Links


- [[Refrain] Asobi Asobase - 01 VOSTFR [720p]](https://uptobox.com/3yvhinh0ddjq)
- [[Refrain] Asobi Asobase - 01 VOSTFR [1080p]](https://uptobox.com/uyugfac7jrz0)
- [[Refrain] Asobi Asobase - 02 VOSTFR [720p]](https://uptobox.com/mccvrzu7e3qe)
- [[Refrain] Asobi Asobase - 02 VOSTFR [1080p]](https://uptobox.com/wig8oj6vr0iy)
- [[Refrain] Asobi Asobase - 03 VOSTFR [720p]](https://uptobox.com/8el56kai88qc)
- [[Refrain] Asobi Asobase - 03 VOSTFR [1080p]](https://uptobox.com/hi5t2aekega7)
- [[Refrain] Asobi Asobase - 04 VOSTFR [720p]](https://uptobox.com/iph5i95ms3r8)
- [[Refrain] Asobi Asobase - 04 VOSTFR [1080p]](https://uptobox.com/6b8gqkqxxco8)
- [[Refrain] Asobi Asobase - 05 VOSTFR [720p]](https://uptobox.com/4t1ndot5gpn3)
- [[Refrain] Asobi Asobase - 05 VOSTFR [1080p]](https://uptobox.com/g4mmnqbqzm84)
- [[Refrain] Asobi Asobase - 06 VOSTFR [720p]](https://uptobox.com/cpbqc11rldtv)
- [[Refrain] Asobi Asobase - 06 VOSTFR [1080p]](https://uptobox.com/afdlvsx8duj8)
- [[Refrain] Asobi Asobase - 07 VOSTFR [720p]](https://uptobox.com/vbt0zgz8blmr)
- [[Refrain] Asobi Asobase - 07 VOSTFR [1080p]](https://uptobox.com/g3q7bva2nee6)
- [[Refrain] Asobi Asobase - 08 VOSTFR [720p]](https://uptobox.com/7m2v0nnkaot0)
- [[Refrain] Asobi Asobase - 08 VOSTFR [1080p]](https://uptobox.com/pfswda7i2n2l)
- [[Refrain] Asobi Asobase - 09 VOSTFR [720p]](https://uptobox.com/n9myykr10xml)
- [[Refrain] Asobi Asobase - 09 VOSTFR [1080p]](https://uptobox.com/40lp00w0wkm7)
- [[Refrain] Asobi Asobase - 10 VOSTFR [720p]](https://uptobox.com/21un5lr82b0y)
- [[Refrain] Asobi Asobase - 10 VOSTFR [1080p]](https://uptobox.com/hc2q8efi4j9y)
- [[Refrain] Asobi Asobase - 11 VOSTFR [720p]](https://uptobox.com/n5d3tvj7tqx7)
- [[Refrain] Asobi Asobase - 11 VOSTFR [1080p]](https://uptobox.com/wkfiok0vvrrg)
- [[Refrain] Asobi Asobase - 12 VOSTFR [720p]](https://uptobox.com/obt5nx3o2h4k)
- [[Refrain] Asobi Asobase - 12 VOSTFR [1080p]](https://uptobox.com/0eatuvefw4po)
