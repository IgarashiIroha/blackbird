# Shoujo☆Kageki Revue Starlight

![Shoujo☆Kageki Revue Starlight](https://cdn.myanimelist.net/images/anime/1165/93552l.jpg)

* Japanese:  少女☆歌劇 レヴュー・スタァライト

## Information

 - Type: TV
 - Episodes: 12
 - Status: Finished Airing
 - Aired: Jul 13, 2018 to Sep 28, 2018
 - Premiered: Summer 2018
 - Broadcast: Fridays at 01:28 (JST)
 - Producers: Pony Canyon, TBS, Bushiroad, Overlap, Nelke Planning, APDREAM 
 - Licensors: Sentai Filmworks 
 - Studios: Kinema Citrus 
 - Source: Original
 - Genres: Music, School
 - Duration: 24 min. per ep.
 - Rating: PG-13 - Teens 13 or older


## Links

