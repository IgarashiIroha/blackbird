# Hanebado!

![Hanebado!](https://cdn.myanimelist.net/images/anime/1288/93432l.jpg)

* Japanese:  はねバド！

## Information

 - Type: TV
 - Episodes: 13
 - Status: Finished Airing
 - Aired: Jul 2, 2018 to Oct 1, 2018
 - Premiered: Summer 2018
 - Broadcast: Mondays at 00:00 (JST)
 - Producers: Kodansha, TOHO animation
 - Licensors: Funimation
 - Studios: LIDENFILMS
 - Source: Manga
 - Genres: Sports, Seinen
 - Duration: 24 min. per ep.
 - Rating: PG-13 - Teens 13 or older


## Links

- [[Refrain] Hanebado! - 01 VOSTFR [720p]](https://uptobox.com/lhyeo91rtv7t)
- [[Refrain] Hanebado! - 01 VOSTFR [1080p]](https://uptobox.com/dw0r0vfpt95f)
- [[Refrain] Hanebado! - 02 VOSTFR [720p]](https://uptobox.com/82h1f6vxpb7m)
- [[Refrain] Hanebado! - 02 VOSTFR [1080p]](https://uptobox.com/btv3ltpywdwr)
- [[Refrain] Hanebado! - 03 VOSTFR [720p]](https://uptobox.com/5numzdd1ogie)
- [[Refrain] Hanebado! - 03 VOSTFR [1080p]](https://uptobox.com/sm2a18f89cpv)
- [[Refrain] Hanebado! - 04 VOSTFR [720p]](https://uptobox.com/3znaouifsf97)
- [[Refrain] Hanebado! - 04 VOSTFR [1080p]](https://uptobox.com/hfuysdqzfw4m)
- [[Refrain] Hanebado! - 05 VOSTFR [720p]](https://uptobox.com/dhmpv64qhhr1)
- [[Refrain] Hanebado! - 05 VOSTFR [1080p]](https://uptobox.com/vm95z6cqzhv5)
- [[Refrain] Hanebado! - 06 VOSTFR [720p]](https://uptobox.com/l4m8kov4nbew)
- [[Refrain] Hanebado! - 06 VOSTFR [1080p]](https://uptobox.com/050vzsdtminr)
- [[Refrain] Hanebado! - 07 VOSTFR [720p]](https://uptobox.com/p5du6msj0zl2)
- [[Refrain] Hanebado! - 07 VOSTFR [1080p]](https://uptobox.com/ggt7l3aoikpq)
- [[Refrain] Hanebado! - 08 VOSTFR [720p]](https://uptobox.com/6qc12a9cq4xd)
- [[Refrain] Hanebado! - 08 VOSTFR [1080p]](https://uptobox.com/imct162dhhnr)
- [[Refrain] Hanebado! - 09 VOSTFR [720p]](https://uptobox.com/jin9fovtex7m)
- [[Refrain] Hanebado! - 09 VOSTFR [1080p]](https://uptobox.com/s66onqlk0ej9)
- [[Refrain] Hanebado! - 10 VOSTFR [720p]](https://uptobox.com/iz14wnrejsib)
- [[Refrain] Hanebado! - 10 VOSTFR [1080p]](https://uptobox.com/zncgfxejwte1)
- [[Refrain] Hanebado! - 11 VOSTFR [720p]](https://uptobox.com/p5e3sf81gmqu)
- [[Refrain] Hanebado! - 11 VOSTFR [1080p]](https://uptobox.com/rgmvgxafi32t)
- [[Refrain] Hanebado! - 12 VOSTFR [720p]](https://uptobox.com/jl140a6v6f2q)
- [[Refrain] Hanebado! - 12 VOSTFR [1080p]](https://uptobox.com/7znryqmizapj)
- [[Refrain] Hanebado! - 13 VOSTFR [720p]](https://uptobox.com/gqzwz867j2vf)
- [[Refrain] Hanebado! - 13 VOSTFR [1080p]](https://uptobox.com/7btjdr0p9qmn)
