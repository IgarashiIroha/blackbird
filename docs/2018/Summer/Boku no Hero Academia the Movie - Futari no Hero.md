# Boku no Hero Academia the Movie: Futari no Hero

![Boku no Hero Academia the Movie: Futari no Hero](https://cdn.myanimelist.net/images/anime/1736/93138l.jpg)

* Japanese:  僕のヒーローアカデミア THE MOVIE ～2人の英雄（ヒーロー）～

## Information

 - Type: Movie
 - Episodes: 1
 - Status: Finished Airing
 - Aired: Aug 3, 2018
 - Producers: TOHO animation, Shueisha 
 - Licensors: Funimation 
 - Studios: Bones 
 - Source: Manga
 - Genres: Action, Super Power, Shounen
 - Duration: 1 hr. 36 min.
 - Rating: PG-13 - Teens 13 or older


## Links

- [[Refrain] Boku no Hero Academia the Movie - Futari no Hero - 00 VOSTFR {ADN} [720p]](https://uptobox.com/8ee8i4ff4g5u)
- [[Refrain] Boku no Hero Academia the Movie - Futari no Hero - 00 VOSTFR {ADN} [1080p]](https://uptobox.com/4m6xnj6w7vg8)
