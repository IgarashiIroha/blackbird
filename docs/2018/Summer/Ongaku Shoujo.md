# Ongaku Shoujo (TV)

![Ongaku Shoujo (TV)](https://cdn.myanimelist.net/images/anime/1811/91043l.jpg)

* Japanese:  音楽少女

## Information

 - Type: TV
 - Episodes: 12
 - Status: Finished Airing
 - Aired: Jul 6, 2018 to Sep 21, 2018
 - Premiered: Summer 2018
 - Broadcast: Fridays at 23:00 (JST)
 - Producers: King Records
 - Licensors: None found, add some
 - Studios: Studio Deen
 - Source: Original
 - Genres: Music, Slice of Life
 - Duration: 23 min. per ep.
 - Rating: PG-13 - Teens 13 or older


## Links

- [[Refrain] Ongaku Shoujo (TV) - 01 VOSTFR [720p]](https://uptobox.com/ceom28dwz0we)
- [[Refrain] Ongaku Shoujo (TV) - 01 VOSTFR [1080p]](https://uptobox.com/ynliqktavmal)
- [[Refrain] Ongaku Shoujo (TV) - 01 VOSTFR {ADN} [720p]](https://uptobox.com/q3siww5e1gqp)
- [[Refrain] Ongaku Shoujo (TV) - 01 VOSTFR {ADN} [1080p]](https://uptobox.com/6ljjqg0qt6hx)
- [[Refrain] Ongaku Shoujo (TV) - 02 VOSTFR [720p]](https://uptobox.com/9ewimj4cd2gq)
- [[Refrain] Ongaku Shoujo (TV) - 02 VOSTFR [1080p]](https://uptobox.com/g12b8w0ofse9)
- [[Refrain] Ongaku Shoujo (TV) - 02 VOSTFR {ADN} [720p]](https://uptobox.com/2nt5nasxxovd)
- [[Refrain] Ongaku Shoujo (TV) - 02 VOSTFR {ADN} [1080p]](https://uptobox.com/g402pt0q4hqz)
- [[Refrain] Ongaku Shoujo (TV) - 03 VOSTFR [720p]](https://uptobox.com/5ks73osqtci0)
- [[Refrain] Ongaku Shoujo (TV) - 03 VOSTFR [1080p]](https://uptobox.com/cuuee32p98fo)
- [[Refrain] Ongaku Shoujo (TV) - 03 VOSTFR {ADN} [720p]](https://uptobox.com/85hqa4ht95om)
- [[Refrain] Ongaku Shoujo (TV) - 03 VOSTFR {ADN} [1080p]](https://uptobox.com/5k4an3is51is)
- [[Refrain] Ongaku Shoujo (TV) - 04 VOSTFR [720p]](https://uptobox.com/a3efvq3ysmd8)
- [[Refrain] Ongaku Shoujo (TV) - 04 VOSTFR [1080p]](https://uptobox.com/uehd7kqk571g)
- [[Refrain] Ongaku Shoujo (TV) - 04 VOSTFR {ADN} [720p]](https://uptobox.com/z9rxc2yqbvjt)
- [[Refrain] Ongaku Shoujo (TV) - 04 VOSTFR {ADN} [1080p]](https://uptobox.com/9spkqhh77wh7)
- [[Refrain] Ongaku Shoujo (TV) - 05 VOSTFR [720p]](https://uptobox.com/28f3sz5220xz)
- [[Refrain] Ongaku Shoujo (TV) - 05 VOSTFR [1080p]](https://uptobox.com/ot2hvc78tt22)
- [[Refrain] Ongaku Shoujo (TV) - 05 VOSTFR {ADN} [720p]](https://uptobox.com/1vtswlf2ddog)
- [[Refrain] Ongaku Shoujo (TV) - 05 VOSTFR {ADN} [1080p]](https://uptobox.com/zo0ol3el5u5n)
- [[Refrain] Ongaku Shoujo (TV) - 06 VOSTFR [720p]](https://uptobox.com/ihdujlx6f581)
- [[Refrain] Ongaku Shoujo (TV) - 06 VOSTFR [1080p]](https://uptobox.com/iyhkz6tvdq3s)
- [[Refrain] Ongaku Shoujo (TV) - 06 VOSTFR {ADN} [720p]](https://uptobox.com/kwlcei563ctw)
- [[Refrain] Ongaku Shoujo (TV) - 06 VOSTFR {ADN} [1080p]](https://uptobox.com/cef3t19whkxk)
- [[Refrain] Ongaku Shoujo (TV) - 07 VOSTFR [720p]](https://uptobox.com/9tdy6dev9vx0)
- [[Refrain] Ongaku Shoujo (TV) - 07 VOSTFR [1080p]](https://uptobox.com/xr6roph4xlhg)
- [[Refrain] Ongaku Shoujo (TV) - 07 VOSTFR {ADN} [720p]](https://uptobox.com/wx3jwlsw21e5)
- [[Refrain] Ongaku Shoujo (TV) - 07 VOSTFR {ADN} [1080p]](https://uptobox.com/08opkb5bjv7e)
- [[Refrain] Ongaku Shoujo (TV) - 08 VOSTFR [720p]](https://uptobox.com/811m28jpuke4)
- [[Refrain] Ongaku Shoujo (TV) - 08 VOSTFR [1080p]](https://uptobox.com/fshfp1hsxhjv)
- [[Refrain] Ongaku Shoujo (TV) - 08 VOSTFR {ADN} [720p]](https://uptobox.com/33lp115mv5s8)
- [[Refrain] Ongaku Shoujo (TV) - 08 VOSTFR {ADN} [1080p]](https://uptobox.com/awbalpmfoga1)
- [[Refrain] Ongaku Shoujo (TV) - 09 VOSTFR [720p]](https://uptobox.com/outc3fyskdqo)
- [[Refrain] Ongaku Shoujo (TV) - 09 VOSTFR [1080p]](https://uptobox.com/9xon3nxl8zqy)
- [[Refrain] Ongaku Shoujo (TV) - 09 VOSTFR {ADN} [720p]](https://uptobox.com/aupa64503jxu)
- [[Refrain] Ongaku Shoujo (TV) - 09 VOSTFR {ADN} [1080p]](https://uptobox.com/3hevrxh013gf)
- [[Refrain] Ongaku Shoujo (TV) - 10 VOSTFR [720p]](https://uptobox.com/76n3ds9drhbr)
- [[Refrain] Ongaku Shoujo (TV) - 10 VOSTFR [1080p]](https://uptobox.com/6xcxa2jymg51)
- [[Refrain] Ongaku Shoujo (TV) - 10 VOSTFR {ADN} [720p]](https://uptobox.com/17gzr81xeodp)
- [[Refrain] Ongaku Shoujo (TV) - 10 VOSTFR {ADN} [1080p]](https://uptobox.com/xr3l3526pbl8)
- [[Refrain] Ongaku Shoujo (TV) - 11 VOSTFR [720p]](https://uptobox.com/76dog53aaxee)
- [[Refrain] Ongaku Shoujo (TV) - 11 VOSTFR [1080p]](https://uptobox.com/6iusao8xtopf)
- [[Refrain] Ongaku Shoujo (TV) - 11 VOSTFR {ADN} [720p]](https://uptobox.com/a6nb5fn8f8i6)
- [[Refrain] Ongaku Shoujo (TV) - 11 VOSTFR {ADN} [1080p]](https://uptobox.com/vqggu1v362ch)
- [[Refrain] Ongaku Shoujo (TV) - 12 VOSTFR [720p]](https://uptobox.com/mrr195534lzs)
- [[Refrain] Ongaku Shoujo (TV) - 12 VOSTFR [1080p]](https://uptobox.com/ibu3x9s529gz)
- [[Refrain] Ongaku Shoujo (TV) - 12 VOSTFR {ADN} [720p]](https://uptobox.com/9j2yeho7u57z)
- [[Refrain] Ongaku Shoujo (TV) - 12 VOSTFR {ADN} [1080p]](https://uptobox.com/la0j65l8y1c1)
