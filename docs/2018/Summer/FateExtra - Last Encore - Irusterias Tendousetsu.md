# Fate/Extra: Last Encore - Irusterias Tendousetsu

![Fate/Extra: Last Encore - Irusterias Tendousetsu](https://cdn.myanimelist.net/images/anime/1280/93468l.jpg)

* Japanese:  Fate/EXTRA Last Encore イルステリアス天動説

## Information

 - Type: TV
 - Episodes: 3
 - Status: Finished Airing
 - Aired: Jul 29, 2018
 - Premiered: Summer 2018
 - Broadcast: Unknown
 - Producers: None found, add some
 - Licensors: None found, add some
 - Studios: Shaft
 - Source: Game
 - Genres: Action, Magic, Fantasy
 - Duration: 32 min. per ep.
 - Rating: R - 17+ (violence & profanity)


## Links

- [[Refrain] FateEXTRA Last Encore - 11 VF VOSTFR [720p]](https://uptobox.com/m4jp4v5einwo)
- [[Refrain] FateEXTRA Last Encore - 12 VF VOSTFR [720p]](https://uptobox.com/3zuaioyy3s4h)
- [[Refrain] FateEXTRA Last Encore - 13 VF VOSTFR [720p]](https://uptobox.com/b1qkhnysmroy)
