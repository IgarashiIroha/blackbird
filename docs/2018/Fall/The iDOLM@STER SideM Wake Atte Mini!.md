# The iDOLM@STER SideM: Wake Atte Mini!

![The iDOLM@STER SideM: Wake Atte Mini!](https://cdn.myanimelist.net/images/anime/1711/93506l.jpg)

* Japanese:  アイドルマスター SideM 理由あってMini!

## Information

- Type: TV
- Episodes: 12
- Status: Finished Airing
- Aired: Oct 9, 2018 to Dec 25, 2018
- Premiered: Fall 2018
- Broadcast: Tuesdays at 21:54 (JST)
- Producers: Lantis, Bandai Namco Entertainment
- Licensors: None found, add some
- Studios: Zero-G
- Source: Manga
- Genres: Slice of Life, Comedy
- Duration: 3 min. per ep.
- Rating: PG-13 - Teens 13 or older


## Links
- [[Refrain] The iDOLM@STER SideM - Wake Atte Mini! - 01 VOSTFR [720p]](https://uptobox.com/5m7lt7x8utr3)
- [[Refrain] The iDOLM@STER SideM - Wake Atte Mini! - 01 VOSTFR [1080p]](https://uptobox.com/ftzx0b9qbyss)
- [[Refrain] The iDOLM@STER SideM - Wake Atte Mini! - 02 VOSTFR [720p]](https://uptobox.com/6gn0ytaepb26)
- [[Refrain] The iDOLM@STER SideM - Wake Atte Mini! - 02 VOSTFR [1080p]](https://uptobox.com/0cukv30hlf7s)
- [[Refrain] The iDOLM@STER SideM - Wake Atte Mini! - 03 VOSTFR [720p]](https://uptobox.com/tbpoh6x48sdg)
- [[Refrain] The iDOLM@STER SideM - Wake Atte Mini! - 03 VOSTFR [1080p]](https://uptobox.com/2fdfgwa7rs89)
- [[Refrain] The iDOLM@STER SideM - Wake Atte Mini! - 04 VOSTFR [720p]](https://uptobox.com/580qkw5pi8y7)
- [[Refrain] The iDOLM@STER SideM - Wake Atte Mini! - 04 VOSTFR [1080p]](https://uptobox.com/3537k6rdwew0)
- [[Refrain] The iDOLM@STER SideM - Wake Atte Mini! - 05 VOSTFR [720p]](https://uptobox.com/tugu1te7mnhm)
- [[Refrain] The iDOLM@STER SideM - Wake Atte Mini! - 05 VOSTFR [1080p]](https://uptobox.com/x6r2px6dobr8)
- [[Refrain] The iDOLM@STER SideM - Wake Atte Mini! - 06 VOSTFR [720p]](https://uptobox.com/1fgbl0l70x2f)
- [[Refrain] The iDOLM@STER SideM - Wake Atte Mini! - 06 VOSTFR [1080p]](https://uptobox.com/l6iesx7qyknk)
- [[Refrain] The iDOLM@STER SideM - Wake Atte Mini! - 07 VOSTFR [720p]](https://uptobox.com/f8x145pioamu)
- [[Refrain] The iDOLM@STER SideM - Wake Atte Mini! - 07 VOSTFR [1080p]](https://uptobox.com/3x95mkns2npf)
- [[Refrain] The iDOLM@STER SideM - Wake Atte Mini! - 08 VOSTFR [720p]](https://uptobox.com/dvyt0a7yk9gg)
- [[Refrain] The iDOLM@STER SideM - Wake Atte Mini! - 08 VOSTFR [1080p]](https://uptobox.com/9whwwawf4ap7)
- [[Refrain] The iDOLM@STER SideM - Wake Atte Mini! - 09 VOSTFR [720p]](https://uptobox.com/m80yk680y5l8)
- [[Refrain] The iDOLM@STER SideM - Wake Atte Mini! - 09 VOSTFR [1080p]](https://uptobox.com/q3m37v5fc597)
- [[Refrain] The iDOLM@STER SideM - Wake Atte Mini! - 10 VOSTFR [720p]](https://uptobox.com/f6gtmp229xlm)
- [[Refrain] The iDOLM@STER SideM - Wake Atte Mini! - 10 VOSTFR [1080p]](https://uptobox.com/5ono5jv4f2sx)
- [[Refrain] The iDOLM@STER SideM - Wake Atte Mini! - 11 VOSTFR [720p]](https://uptobox.com/ga31lamh36ar)
- [[Refrain] The iDOLM@STER SideM - Wake Atte Mini! - 11 VOSTFR [1080p]](https://uptobox.com/9cqn6c4ol25r)
- [[Refrain] The iDOLM@STER SideM - Wake Atte Mini! - 12 VOSTFR [720p]](https://uptobox.com/g78ogbf4hlao)
- [[Refrain] The iDOLM@STER SideM - Wake Atte Mini! - 12 VOSTFR [1080p]](https://uptobox.com/bbdcozsw5ldd)
