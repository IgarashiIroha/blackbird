# Gakuen Basara

![Gakuen Basara](https://cdn.myanimelist.net/images/anime/1278/93829l.jpg)

* Japanese:  学園BASARA

## Information

 - Type: TV
 - Episodes: 12
 - Status: Finished Airing
 - Aired: Oct 5, 2018 to Dec 21, 2018
 - Premiered: Fall 2018
 - Broadcast: Fridays at 01:28 (JST)
 - Producers: TBS, DAX Production, High Kick Entertainment 
 - Licensors: None found, add some 
 - Studios: Brain&#039;s Base 
 - Source: Game
 - Genres: Action, Comedy, School
 - Duration: 24 min. per ep.
 - Rating: PG-13 - Teens 13 or older


## Links
- [[Refrain] Gakuen Basara - 01 VOSTFR [720p]](https://uptobox.com/puankxo0cc16)
- [[Refrain] Gakuen Basara - 01 VOSTFR [1080p]](https://uptobox.com/lwdrpz8snx93)
- [[Refrain] Gakuen Basara - 01 VOSTFR {ADN} [720p]](https://uptobox.com/tc6w3h8rtae2)
- [[Refrain] Gakuen Basara - 01 VOSTFR {ADN} [1080p]](https://uptobox.com/5ucnxoovqoej)
- [[Refrain] Gakuen Basara - 02 VOSTFR [720p]](https://uptobox.com/xmbppehx0a93)
- [[Refrain] Gakuen Basara - 02 VOSTFR [1080p]](https://uptobox.com/fonsq1c392rb)
- [[Refrain] Gakuen Basara - 02 VOSTFR {ADN} [720p]](https://uptobox.com/2htcer0qm5s5)
- [[Refrain] Gakuen Basara - 02 VOSTFR {ADN} [1080p]](https://uptobox.com/vz14cjynjkvo)
- [[Refrain] Gakuen Basara - 03 VOSTFR [720p]](https://uptobox.com/qyb97w3v06r4)
- [[Refrain] Gakuen Basara - 03 VOSTFR [1080p]](https://uptobox.com/iasaxp6qsolt)
- [[Refrain] Gakuen Basara - 03 VOSTFR {ADN} [720p]](https://uptobox.com/uet1errawnko)
- [[Refrain] Gakuen Basara - 03 VOSTFR {ADN} [1080p]](https://uptobox.com/yueemn2qm5ae)
- [[Refrain] Gakuen Basara - 04 VOSTFR [720p]](https://uptobox.com/yngeth35oaqp)
- [[Refrain] Gakuen Basara - 04 VOSTFR [1080p]](https://uptobox.com/udxrqrscd2me)
- [[Refrain] Gakuen Basara - 04 VOSTFR {ADN} [720p]](https://uptobox.com/68xjk3hhl4yw)
- [[Refrain] Gakuen Basara - 04 VOSTFR {ADN} [1080p]](https://uptobox.com/ktebkhk16lq1)
- [[Refrain] Gakuen Basara - 05 VOSTFR [720p]](https://uptobox.com/trsnbpjz9c2m)
- [[Refrain] Gakuen Basara - 05 VOSTFR [1080p]](https://uptobox.com/f8iq38vffgkm)
- [[Refrain] Gakuen Basara - 05 VOSTFR {ADN} [720p]](https://uptobox.com/qaiv5t7i8xvl)
- [[Refrain] Gakuen Basara - 05 VOSTFR {ADN} [1080p]](https://uptobox.com/h57x938u9he8)
- [[Refrain] Gakuen Basara - 06 VOSTFR [720p]](https://uptobox.com/raibk5ppbsqm)
- [[Refrain] Gakuen Basara - 06 VOSTFR [1080p]](https://uptobox.com/jjk3m3wdl5f5)
- [[Refrain] Gakuen Basara - 06 VOSTFR {ADN} [720p]](https://uptobox.com/uxnn88c8nzw2)
- [[Refrain] Gakuen Basara - 06 VOSTFR {ADN} [1080p]](https://uptobox.com/k04pbynd8lvc)
- [[Refrain] Gakuen Basara - 07 VOSTFR [720p]](https://uptobox.com/crtekj1nk4h9)
- [[Refrain] Gakuen Basara - 07 VOSTFR [1080p]](https://uptobox.com/a001fffvfnl1)
- [[Refrain] Gakuen Basara - 07 VOSTFR {ADN} [720p]](https://uptobox.com/ap6tw253ans4)
- [[Refrain] Gakuen Basara - 07 VOSTFR {ADN} [1080p]](https://uptobox.com/claqzbpya2qt)
- [[Refrain] Gakuen Basara - 08 VOSTFR [720p]](https://uptobox.com/3jvlmzg0bk63)
- [[Refrain] Gakuen Basara - 08 VOSTFR [1080p]](https://uptobox.com/flo578d2vg2m)
- [[Refrain] Gakuen Basara - 08 VOSTFR {ADN} [720p]](https://uptobox.com/p37l2pvh18ju)
- [[Refrain] Gakuen Basara - 08 VOSTFR {ADN} [1080p]](https://uptobox.com/06isd1jq284m)
- [[Refrain] Gakuen Basara - 09 VOSTFR [720p]](https://uptobox.com/2m2fgxdntva9)
- [[Refrain] Gakuen Basara - 09 VOSTFR [1080p]](https://uptobox.com/s04npo5snore)
- [[Refrain] Gakuen Basara - 09 VOSTFR {ADN} [720p]](https://uptobox.com/i0dpc4pip4z2)
- [[Refrain] Gakuen Basara - 09 VOSTFR {ADN} [1080p]](https://uptobox.com/wdpyz1kzigpl)
- [[Refrain] Gakuen Basara - 10 VOSTFR [720p]](https://uptobox.com/uq8m6pstq1jy)
- [[Refrain] Gakuen Basara - 10 VOSTFR [1080p]](https://uptobox.com/web37exz4ty0)
- [[Refrain] Gakuen Basara - 10 VOSTFR {ADN} [720p]](https://uptobox.com/u511oi8cxxdg)
- [[Refrain] Gakuen Basara - 10 VOSTFR {ADN} [1080p]](https://uptobox.com/y7zx3foj3y8g)
- [[Refrain] Gakuen Basara - 11 VOSTFR [720p]](https://uptobox.com/qp0eib93u6uo)
- [[Refrain] Gakuen Basara - 11 VOSTFR [1080p]](https://uptobox.com/tlt9k930c97h)
- [[Refrain] Gakuen Basara - 11 VOSTFR {ADN} [720p]](https://uptobox.com/7n679i12gdit)
- [[Refrain] Gakuen Basara - 11 VOSTFR {ADN} [1080p]](https://uptobox.com/d39xakht0vho)
- [[Refrain] Gakuen Basara - 12 VOSTFR [720p]](https://uptobox.com/sqb1qsfgvvvh)
- [[Refrain] Gakuen Basara - 12 VOSTFR [1080p]](https://uptobox.com/f0o13r0xiu38)
- [[Refrain] Gakuen Basara - 12 VOSTFR {ADN} [720p]](https://uptobox.com/nyw7nthnjiit)
- [[Refrain] Gakuen Basara - 12 VOSTFR {ADN} [1080p]](https://uptobox.com/ns852ztwwslh)
