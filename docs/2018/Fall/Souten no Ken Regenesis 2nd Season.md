# Souten no Ken: Regenesis 2nd Season

![Souten no Ken: Regenesis 2nd Season](https://cdn.myanimelist.net/images/anime/1002/93814l.jpg)

* Japanese:  蒼天の拳 REGENESIS 第2期

## Information

- Type: ONA
- Episodes: 12
- Status: Finished Airing
- Aired: Oct 2, 2018 to Dec 18, 2018
- Producers: Souten no Ken
- Licensors: None found, add some
- Studios: Polygon Pictures
- Source: Manga
- Genres: Action, Historical, Martial Arts
- Duration: 22 min. per ep.
- Rating: R - 17+ (violence & profanity)


## Links
- [[Refrain] Souten no Ken - Regenesis 2nd Season - 13 VOSTFR {ADN} [720p]](https://uptobox.com/qk4rz7tbq2og)
- [[Refrain] Souten no Ken - Regenesis 2nd Season - 13 VOSTFR {ADN} [1080p]](https://uptobox.com/q5jrs7xmmmnr)
- [[Refrain] Souten no Ken - Regenesis 2nd Season - 14 VOSTFR {ADN} [720p]](https://uptobox.com/jaishahrm3d2)
- [[Refrain] Souten no Ken - Regenesis 2nd Season - 14 VOSTFR {ADN} [1080p]](https://uptobox.com/yq0p54kb64ut)
- [[Refrain] Souten no Ken - Regenesis 2nd Season - 15 VOSTFR {ADN} [720p]](https://uptobox.com/2mapl321aurx)
- [[Refrain] Souten no Ken - Regenesis 2nd Season - 15 VOSTFR {ADN} [1080p]](https://uptobox.com/3a7sz1mlcs3g)
- [[Refrain] Souten no Ken - Regenesis 2nd Season - 16 VOSTFR {ADN} [720p]](https://uptobox.com/3ff5n02hf3r8)
- [[Refrain] Souten no Ken - Regenesis 2nd Season - 16 VOSTFR {ADN} [1080p]](https://uptobox.com/646r9d4j8mrp)
- [[Refrain] Souten no Ken - Regenesis 2nd Season - 17 VOSTFR {ADN} [720p]](https://uptobox.com/dgq3122hrgu2)
- [[Refrain] Souten no Ken - Regenesis 2nd Season - 17 VOSTFR {ADN} [1080p]](https://uptobox.com/mg8nkgldjsdv)
- [[Refrain] Souten no Ken - Regenesis 2nd Season - 18 VOSTFR {ADN} [720p]](https://uptobox.com/t48buqjuqzty)
- [[Refrain] Souten no Ken - Regenesis 2nd Season - 18 VOSTFR {ADN} [1080p]](https://uptobox.com/3u1ph7jhvd76)
- [[Refrain] Souten no Ken - Regenesis 2nd Season - 19 VOSTFR {ADN} [720p]](https://uptobox.com/0vmc7youxl79)
- [[Refrain] Souten no Ken - Regenesis 2nd Season - 19 VOSTFR {ADN} [1080p]](https://uptobox.com/20jycoj73fi0)
- [[Refrain] Souten no Ken - Regenesis 2nd Season - 20 VOSTFR {ADN} [720p]](https://uptobox.com/t7g7f21qpbgm)
- [[Refrain] Souten no Ken - Regenesis 2nd Season - 20 VOSTFR {ADN} [1080p]](https://uptobox.com/ce0hydqkkedu)
- [[Refrain] Souten no Ken - Regenesis 2nd Season - 21 VOSTFR {ADN} [720p]](https://uptobox.com/9rsix7ct9vmx)
- [[Refrain] Souten no Ken - Regenesis 2nd Season - 21 VOSTFR {ADN} [1080p]](https://uptobox.com/piucxeyoxlbb)
- [[Refrain] Souten no Ken - Regenesis 2nd Season - 22 VOSTFR {ADN} [720p]](https://uptobox.com/6d00p4gdotfn)
- [[Refrain] Souten no Ken - Regenesis 2nd Season - 22 VOSTFR {ADN} [1080p]](https://uptobox.com/h8vprwjd4ccb)
- [[Refrain] Souten no Ken - Regenesis 2nd Season - 23 VOSTFR {ADN} [720p]](https://uptobox.com/hzblsz6vhlgq)
- [[Refrain] Souten no Ken - Regenesis 2nd Season - 23 VOSTFR {ADN} [1080p]](https://uptobox.com/qpew2wbmtfib)
- [[Refrain] Souten no Ken - Regenesis 2nd Season - 24 VOSTFR {ADN} [720p]](https://uptobox.com/6mz9qjie0b8t)
- [[Refrain] Souten no Ken - Regenesis 2nd Season - 24 VOSTFR {ADN} [1080p]](https://uptobox.com/wnw6kstvqlv0)
