# Seishun Buta Yarou wa Bunny Girl Senpai no Yume wo Minai

![Seishun Buta Yarou wa Bunny Girl Senpai no Yume wo Minai](https://cdn.myanimelist.net/images/anime/1301/93586l.jpg)

* Japanese:  青春ブタ野郎はバニーガール先輩の夢を見ない

## Information

 - Type: TV
 - Episodes: 13
 - Status: Finished Airing
 - Aired: Oct 4, 2018 to Dec 27, 2018
 - Premiered: Fall 2018
 - Broadcast: Thursdays at 02:20 (JST)
 - Producers: Aniplex, Tokyo MX, Hakuhodo DY Music & Pictures, Nagoya Broadcasting Network, BS11, ABC Animation, Kadokawa 
 - Licensors: Aniplex of America 
 - Studios: CloverWorks 
 - Source: Light novel
 - Genres: Comedy, Supernatural, Romance, School
 - Duration: 24 min. per ep.
 - Rating: PG-13 - Teens 13 or older


## Links
- [[Refrain] Seishun Buta Yarou wa Bunny Girl Senpai no Yume wo Minai - 01 VOSTFR [720p]](https://uptobox.com/a9bug0lozzon)
- [[Refrain] Seishun Buta Yarou wa Bunny Girl Senpai no Yume wo Minai - 01 VOSTFR [1080p]](https://uptobox.com/d87zonei88es)
- [[Refrain] Seishun Buta Yarou wa Bunny Girl Senpai no Yume wo Minai - 02 VOSTFR [720p]](https://uptobox.com/jf245de66zf3)
- [[Refrain] Seishun Buta Yarou wa Bunny Girl Senpai no Yume wo Minai - 02 VOSTFR [1080p]](https://uptobox.com/mc9x3d4uzxlf)
- [[Refrain] Seishun Buta Yarou wa Bunny Girl Senpai no Yume wo Minai - 03 VOSTFR [720p]](https://uptobox.com/u31ng1p37dwx)
- [[Refrain] Seishun Buta Yarou wa Bunny Girl Senpai no Yume wo Minai - 03 VOSTFR [1080p]](https://uptobox.com/7hr0tjfp5dxs)
- [[Refrain] Seishun Buta Yarou wa Bunny Girl Senpai no Yume wo Minai - 04 VOSTFR [720p]](https://uptobox.com/pc0l5dz9kdo8)
- [[Refrain] Seishun Buta Yarou wa Bunny Girl Senpai no Yume wo Minai - 04 VOSTFR [1080p]](https://uptobox.com/0prmuw2a04ef)
- [[Refrain] Seishun Buta Yarou wa Bunny Girl Senpai no Yume wo Minai - 05 VOSTFR [720p]](https://uptobox.com/wtyrxzojyu5h)
- [[Refrain] Seishun Buta Yarou wa Bunny Girl Senpai no Yume wo Minai - 05 VOSTFR [1080p]](https://uptobox.com/o8px5ikhm6uo)
- [[Refrain] Seishun Buta Yarou wa Bunny Girl Senpai no Yume wo Minai - 06 VOSTFR [720p]](https://uptobox.com/91nmwpfno0ip)
- [[Refrain] Seishun Buta Yarou wa Bunny Girl Senpai no Yume wo Minai - 06 VOSTFR [1080p]](https://uptobox.com/8qplcn82jxrh)
- [[Refrain] Seishun Buta Yarou wa Bunny Girl Senpai no Yume wo Minai - 07 VOSTFR [720p]](https://uptobox.com/gtdpvcc35g5t)
- [[Refrain] Seishun Buta Yarou wa Bunny Girl Senpai no Yume wo Minai - 07 VOSTFR [1080p]](https://uptobox.com/3kkia8x99b55)
- [[Refrain] Seishun Buta Yarou wa Bunny Girl Senpai no Yume wo Minai - 08 VOSTFR [720p]](https://uptobox.com/bwtslz90qowh)
- [[Refrain] Seishun Buta Yarou wa Bunny Girl Senpai no Yume wo Minai - 08 VOSTFR [1080p]](https://uptobox.com/b65tyokmi9fc)
- [[Refrain] Seishun Buta Yarou wa Bunny Girl Senpai no Yume wo Minai - 09 VOSTFR [720p]](https://uptobox.com/yp18ekiywdld)
- [[Refrain] Seishun Buta Yarou wa Bunny Girl Senpai no Yume wo Minai - 09 VOSTFR [1080p]](https://uptobox.com/9k6bqvpy879j)
- [[Refrain] Seishun Buta Yarou wa Bunny Girl Senpai no Yume wo Minai - 10 VOSTFR [720p]](https://uptobox.com/tt7xeb4c4zg4)
- [[Refrain] Seishun Buta Yarou wa Bunny Girl Senpai no Yume wo Minai - 10 VOSTFR [1080p]](https://uptobox.com/s0rembv6tprz)
- [[Refrain] Seishun Buta Yarou wa Bunny Girl Senpai no Yume wo Minai - 11 VOSTFR [720p]](https://uptobox.com/yscefu2uyk42)
- [[Refrain] Seishun Buta Yarou wa Bunny Girl Senpai no Yume wo Minai - 11 VOSTFR [1080p]](https://uptobox.com/i8f2k2h4umin)
- [[Refrain] Seishun Buta Yarou wa Bunny Girl Senpai no Yume wo Minai - 12 VOSTFR [720p]](https://uptobox.com/2hs8gjf217gx)
- [[Refrain] Seishun Buta Yarou wa Bunny Girl Senpai no Yume wo Minai - 12 VOSTFR [1080p]](https://uptobox.com/o8ne1kbt7f2c)
- [[Refrain] Seishun Buta Yarou wa Bunny Girl Senpai no Yume wo Minai - 13 VOSTFR [720p]](https://uptobox.com/v4cy2zb9la22)
- [[Refrain] Seishun Buta Yarou wa Bunny Girl Senpai no Yume wo Minai - 13 VOSTFR [1080p]](https://uptobox.com/21ouivp5814c)
