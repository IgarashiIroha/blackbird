# Anima Yell!

![Anima Yell!](https://cdn.myanimelist.net/images/anime/1051/93862l.jpg)

* Japanese:  アニマエール！

## Information

 - Type: TV
 - Episodes: 12
 - Status: Finished Airing
 - Aired: Oct 7, 2018 to Dec 23, 2018
 - Premiered: Fall 2018
 - Broadcast: Sundays at 23:30 (JST)
 - Producers: Jinnan Studio 
 - Licensors: None found, add some 
 - Studios: Doga Kobo 
 - Source: 4-koma manga
 - Genres: Comedy, Sports, School
 - Duration: 23 min. per ep.
 - Rating: PG-13 - Teens 13 or older


## Links
- [[Refrain] Anima Yell! - 01 VOSTFR [720p]](https://uptobox.com/p0egjieokq6v)
- [[Refrain] Anima Yell! - 01 VOSTFR [1080p]](https://uptobox.com/5mil859f59rz)
- [[Refrain] Anima Yell! - 02 VOSTFR [720p]](https://uptobox.com/k4yngd9rd3xa)
- [[Refrain] Anima Yell! - 02 VOSTFR [1080p]](https://uptobox.com/k355e3uuq00j)
- [[Refrain] Anima Yell! - 03 VOSTFR [720p]](https://uptobox.com/7n08wyyci3uc)
- [[Refrain] Anima Yell! - 03 VOSTFR [1080p]](https://uptobox.com/j0dvz5cgcre1)
- [[Refrain] Anima Yell! - 04 VOSTFR [720p]](https://uptobox.com/hj62szivus1p)
- [[Refrain] Anima Yell! - 04 VOSTFR [1080p]](https://uptobox.com/8l70kk49vf0w)
- [[Refrain] Anima Yell! - 05 VOSTFR [720p]](https://uptobox.com/eohtj74yfr2o)
- [[Refrain] Anima Yell! - 05 VOSTFR [1080p]](https://uptobox.com/tw194yzr9vv5)
- [[Refrain] Anima Yell! - 06 VOSTFR [720p]](https://uptobox.com/9tlzmu7bdhoc)
- [[Refrain] Anima Yell! - 06 VOSTFR [1080p]](https://uptobox.com/l7lbtmn3mi1r)
- [[Refrain] Anima Yell! - 07 VOSTFR [720p]](https://uptobox.com/fpl7x0mkiry2)
- [[Refrain] Anima Yell! - 07 VOSTFR [1080p]](https://uptobox.com/0ffhhh9o6p0p)
- [[Refrain] Anima Yell! - 08 VOSTFR [720p]](https://uptobox.com/fc0mh890p1c4)
- [[Refrain] Anima Yell! - 08 VOSTFR [1080p]](https://uptobox.com/c6bwp14ialy8)
- [[Refrain] Anima Yell! - 09 VOSTFR [720p]](https://uptobox.com/urjl6ct273im)
- [[Refrain] Anima Yell! - 09 VOSTFR [1080p]](https://uptobox.com/j1m9xgd8kpxl)
- [[Refrain] Anima Yell! - 10 VOSTFR [720p]](https://uptobox.com/cn4cz36net4z)
- [[Refrain] Anima Yell! - 10 VOSTFR [1080p]](https://uptobox.com/67si1fw4auhz)
- [[Refrain] Anima Yell! - 11 VOSTFR [720p]](https://uptobox.com/qssutwtijc7x)
- [[Refrain] Anima Yell! - 11 VOSTFR [1080p]](https://uptobox.com/58zqu16eykzh)
- [[Refrain] Anima Yell! - 12 VOSTFR [720p]](https://uptobox.com/w2j8mqbro8ge)
- [[Refrain] Anima Yell! - 12 VOSTFR [1080p]](https://uptobox.com/g0iw0x5aaisz)
