# Bakumatsu

![Bakumatsu](https://cdn.myanimelist.net/images/anime/1764/95110l.jpg)

* Japanese:  BAKUMATSU

## Information

 - Type: TV
 - Episodes: 12
 - Status: Finished Airing
 - Aired: Oct 5, 2018 to Dec 21, 2018
 - Premiered: Fall 2018
 - Broadcast: Fridays at 01:58 (JST)
 - Producers: TBS, MAGES., Furyu
 - Licensors: None found, add some
 - Studios: Studio Deen
 - Source: Game
 - Genres: Action, Historical, Samurai
 - Duration: 24 min. per ep.
 - Rating: PG-13 - Teens 13 or older


## Links
- [[Refrain] Bakumatsu - 01 VOSTFR [720p]](https://uptobox.com/75hm0mx44xws)
- [[Refrain] Bakumatsu - 01 VOSTFR [1080p]](https://uptobox.com/u0525whwt2cq)
- [[Refrain] Bakumatsu - 01 VOSTFR {ADN} [720p]](https://uptobox.com/m31gf9m2oc3u)
- [[Refrain] Bakumatsu - 01 VOSTFR {ADN} [1080p]](https://uptobox.com/5vs691le4djj)
- [[Refrain] Bakumatsu - 02 VOSTFR [720p]](https://uptobox.com/1pvf7rlt5qq1)
- [[Refrain] Bakumatsu - 02 VOSTFR [1080p]](https://uptobox.com/ej9w3o96gcxz)
- [[Refrain] Bakumatsu - 02 VOSTFR {ADN} [720p]](https://uptobox.com/bv7twf3cq0gz)
- [[Refrain] Bakumatsu - 02 VOSTFR {ADN} [1080p]](https://uptobox.com/08x6xwmtxcf8)
- [[Refrain] Bakumatsu - 03 VOSTFR [720p]](https://uptobox.com/wwsf2ijfo4sb)
- [[Refrain] Bakumatsu - 03 VOSTFR [1080p]](https://uptobox.com/5u88ygo6s3mn)
- [[Refrain] Bakumatsu - 03 VOSTFR {ADN} [720p]](https://uptobox.com/36bhps1ntijv)
- [[Refrain] Bakumatsu - 03 VOSTFR {ADN} [1080p]](https://uptobox.com/v8n1krmkpda1)
- [[Refrain] Bakumatsu - 04 VOSTFR [720p]](https://uptobox.com/q8olb4rwgfvf)
- [[Refrain] Bakumatsu - 04 VOSTFR [1080p]](https://uptobox.com/i41zjwiy50qr)
- [[Refrain] Bakumatsu - 04 VOSTFR {ADN} [720p]](https://uptobox.com/dqnds25a9e1k)
- [[Refrain] Bakumatsu - 04 VOSTFR {ADN} [1080p]](https://uptobox.com/wm2g948kxbmc)
- [[Refrain] Bakumatsu - 05 VOSTFR [720p]](https://uptobox.com/s6s29hn0zcwv)
- [[Refrain] Bakumatsu - 05 VOSTFR [1080p]](https://uptobox.com/85b8pimigbty)
- [[Refrain] Bakumatsu - 05 VOSTFR {ADN} [720p]](https://uptobox.com/qxnhzrus2qls)
- [[Refrain] Bakumatsu - 05 VOSTFR {ADN} [1080p]](https://uptobox.com/htxz2yo43xgh)
- [[Refrain] Bakumatsu - 06 VOSTFR [720p]](https://uptobox.com/1gvq989hr9kt)
- [[Refrain] Bakumatsu - 06 VOSTFR [1080p]](https://uptobox.com/1jfdlag9x13l)
- [[Refrain] Bakumatsu - 06 VOSTFR {ADN} [720p]](https://uptobox.com/z8uxydnl0df1)
- [[Refrain] Bakumatsu - 06 VOSTFR {ADN} [1080p]](https://uptobox.com/5mvi2648ze55)
- [[Refrain] Bakumatsu - 07 VOSTFR [720p]](https://uptobox.com/sk2jno9mjjo6)
- [[Refrain] Bakumatsu - 07 VOSTFR [1080p]](https://uptobox.com/3vedi8rbgxn0)
- [[Refrain] Bakumatsu - 07 VOSTFR {ADN} [720p]](https://uptobox.com/ak0t55fao0wy)
- [[Refrain] Bakumatsu - 07 VOSTFR {ADN} [1080p]](https://uptobox.com/asp1wnrdo0ts)
- [[Refrain] Bakumatsu - 08 VOSTFR [720p]](https://uptobox.com/mo48fx7uhd2t)
- [[Refrain] Bakumatsu - 08 VOSTFR [1080p]](https://uptobox.com/9vcs5lqjfqoj)
- [[Refrain] Bakumatsu - 08 VOSTFR {ADN} [720p]](https://uptobox.com/1xl6m03show5)
- [[Refrain] Bakumatsu - 08 VOSTFR {ADN} [1080p]](https://uptobox.com/p75q6d7tndsu)
- [[Refrain] Bakumatsu - 09 VOSTFR [720p]](https://uptobox.com/todahaybazsn)
- [[Refrain] Bakumatsu - 09 VOSTFR [1080p]](https://uptobox.com/g0rq6eefq1kg)
- [[Refrain] Bakumatsu - 09 VOSTFR {ADN} [720p]](https://uptobox.com/b142d96vhl5t)
- [[Refrain] Bakumatsu - 09 VOSTFR {ADN} [1080p]](https://uptobox.com/04dv643jg3lh)
- [[Refrain] Bakumatsu - 10 VOSTFR [720p]](https://uptobox.com/guxxh6e0m3mi)
- [[Refrain] Bakumatsu - 10 VOSTFR [1080p]](https://uptobox.com/hixpn6ykyetv)
- [[Refrain] Bakumatsu - 10 VOSTFR {ADN} [720p]](https://uptobox.com/2xhvqojpwpvu)
- [[Refrain] Bakumatsu - 10 VOSTFR {ADN} [1080p]](https://uptobox.com/wqv1r0y3kura)
- [[Refrain] Bakumatsu - 11 VOSTFR [720p]](https://uptobox.com/dfaewypy03ty)
- [[Refrain] Bakumatsu - 11 VOSTFR [1080p]](https://uptobox.com/80t9qg94cvf5)
- [[Refrain] Bakumatsu - 11 VOSTFR {ADN} [720p]](https://uptobox.com/wflnrh77b9rl)
- [[Refrain] Bakumatsu - 11 VOSTFR {ADN} [1080p]](https://uptobox.com/qhkh8r9qus3l)
- [[Refrain] Bakumatsu - 12 VOSTFR [720p]](https://uptobox.com/0zxaipxcotlo)
- [[Refrain] Bakumatsu - 12 VOSTFR [1080p]](https://uptobox.com/gd0w84gt5w9c)
- [[Refrain] Bakumatsu - 12 VOSTFR {ADN} [720p]](https://uptobox.com/56c7dxp0ucc2)
- [[Refrain] Bakumatsu - 12 VOSTFR {ADN} [1080p]](https://uptobox.com/g93c05mmhtzn)
- [[Refrain] Bakumatsu - 13 VOSTFR {ADN} [720p]](https://uptobox.com/kyf5h8cq96yd)
- [[Refrain] Bakumatsu - 13 VOSTFR {ADN} [1080p]](https://uptobox.com/wl3jmxm1alz8)
- [[Refrain] Bakumatsu - 14 VOSTFR {ADN} [720p]](https://uptobox.com/v2pb8719gwn0)
- [[Refrain] Bakumatsu - 14 VOSTFR {ADN} [1080p]](https://uptobox.com/7syoxhknu50n)
- [[Refrain] Bakumatsu - 15 VOSTFR {ADN} [720p]](https://uptobox.com/vd931w1s2qco)
- [[Refrain] Bakumatsu - 15 VOSTFR {ADN} [1080p]](https://uptobox.com/kn0li1udt31b)
- [[Refrain] Bakumatsu - 16 VOSTFR {ADN} [720p]](https://uptobox.com/a0mf7rs5strb)
- [[Refrain] Bakumatsu - 16 VOSTFR {ADN} [1080p]](https://uptobox.com/ymeqlarofqn0)
- [[Refrain] Bakumatsu - 17 VOSTFR {ADN} [720p]](https://uptobox.com/qy59zu3fsuma)
- [[Refrain] Bakumatsu - 17 VOSTFR {ADN} [1080p]](https://uptobox.com/933t6x031p20)
- [[Refrain] Bakumatsu - 18 VOSTFR {ADN} [720p]](https://uptobox.com/ru3cxnphugd9)
- [[Refrain] Bakumatsu - 18 VOSTFR {ADN} [1080p]](https://uptobox.com/275wxo87b9ve)
- [[Refrain] Bakumatsu - 19 VOSTFR {ADN} [720p]](https://uptobox.com/bptefjeyzhcg)
- [[Refrain] Bakumatsu - 19 VOSTFR {ADN} [1080p]](https://uptobox.com/t1hhkjk6a3q3)
- [[Refrain] Bakumatsu - 20 VOSTFR {ADN} [720p]](https://uptobox.com/qoem7sc0wcpd)
- [[Refrain] Bakumatsu - 20 VOSTFR {ADN} [1080p]](https://uptobox.com/1lt3t1wyonam)
- [[Refrain] Bakumatsu - 21 VOSTFR {ADN} [720p]](https://uptobox.com/zypz1yu78nyv)
- [[Refrain] Bakumatsu - 21 VOSTFR {ADN} [1080p]](https://uptobox.com/261i9jnkuk7v)
- [[Refrain] Bakumatsu - 22 VOSTFR {ADN} [720p]](https://uptobox.com/y2ldhlnm7hbm)
- [[Refrain] Bakumatsu - 22 VOSTFR {ADN} [1080p]](https://uptobox.com/gct8pz99ddy1)
