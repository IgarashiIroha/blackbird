# Gyakuten Saiban: Sono &quot;Shinjitsu&quot;, Igi Ari! Season 2

![Gyakuten Saiban: Sono &quot;Shinjitsu&quot;, Igi Ari! Season 2](https://cdn.myanimelist.net/images/anime/1861/91089l.jpg)

* Japanese:  逆転裁判 ～その「真実」、異議あり！～ Season 2

## Information

 - Type: TV
 - Episodes: Unknown
 - Status: Currently Airing
 - Aired: Oct 6, 2018 to ?
 - Premiered: Fall 2018
 - Broadcast: Saturdays at 17:30 (JST)
 - Producers: Aniplex, Yomiuri Telecasting, Capcom
 - Licensors: Funimation
 - Studios: CloverWorks
 - Source: Game
 - Genres: Comedy, Drama, Mystery, Police
 - Duration: 24 min.
 - Rating: PG-13 - Teens 13 or older


## Links

- [[Refrain] Gyakuten Saiban - Sono Shinjitsu - Igi Ari! Season 2 - 01 VOSTFR [720p]](https://uptobox.com/sf0of65fv05y)
- [[Refrain] Gyakuten Saiban - Sono Shinjitsu - Igi Ari! Season 2 - 01 VOSTFR [1080p]](https://uptobox.com/8ppojxzkcqp0)
- [[Refrain] Gyakuten Saiban - Sono Shinjitsu - Igi Ari! Season 2 - 02 VOSTFR [720p]](https://uptobox.com/nhov13y4idz7)
- [[Refrain] Gyakuten Saiban - Sono Shinjitsu - Igi Ari! Season 2 - 02 VOSTFR [1080p]](https://uptobox.com/g63a20yz9z97)
- [[Refrain] Gyakuten Saiban - Sono Shinjitsu - Igi Ari! Season 2 - 03 VOSTFR [720p]](https://uptobox.com/fooo6upfw0gy)
- [[Refrain] Gyakuten Saiban - Sono Shinjitsu - Igi Ari! Season 2 - 03 VOSTFR [1080p]](https://uptobox.com/b5fxxcg7mw3m)
- [[Refrain] Gyakuten Saiban - Sono Shinjitsu - Igi Ari! Season 2 - 04 VOSTFR [720p]](https://uptobox.com/zwo2jrb74kia)
- [[Refrain] Gyakuten Saiban - Sono Shinjitsu - Igi Ari! Season 2 - 04 VOSTFR [1080p]](https://uptobox.com/8x45m7twa1es)
- [[Refrain] Gyakuten Saiban - Sono Shinjitsu - Igi Ari! Season 2 - 05 VOSTFR [720p]](https://uptobox.com/uq2hucvogi8m)
- [[Refrain] Gyakuten Saiban - Sono Shinjitsu - Igi Ari! Season 2 - 05 VOSTFR [1080p]](https://uptobox.com/uqb5v1mo51el)
- [[Refrain] Gyakuten Saiban - Sono Shinjitsu - Igi Ari! Season 2 - 06 VOSTFR [720p]](https://uptobox.com/k3dd0awwfbvg)
- [[Refrain] Gyakuten Saiban - Sono Shinjitsu - Igi Ari! Season 2 - 06 VOSTFR [1080p]](https://uptobox.com/w8klj1gdi5jc)
- [[Refrain] Gyakuten Saiban - Sono Shinjitsu - Igi Ari! Season 2 - 07 VOSTFR [720p]](https://uptobox.com/mnhf6i7sgram)
- [[Refrain] Gyakuten Saiban - Sono Shinjitsu - Igi Ari! Season 2 - 07 VOSTFR [1080p]](https://uptobox.com/p9kvum922e6r)
- [[Refrain] Gyakuten Saiban - Sono Shinjitsu - Igi Ari! Season 2 - 08 VOSTFR [720p]](https://uptobox.com/kt2hzlsyqacj)
- [[Refrain] Gyakuten Saiban - Sono Shinjitsu - Igi Ari! Season 2 - 08 VOSTFR [1080p]](https://uptobox.com/0d5aqlidmp01)
- [[Refrain] Gyakuten Saiban - Sono Shinjitsu - Igi Ari! Season 2 - 09 VOSTFR [720p]](https://uptobox.com/29gs1xii2pd2)
- [[Refrain] Gyakuten Saiban - Sono Shinjitsu - Igi Ari! Season 2 - 09 VOSTFR [1080p]](https://uptobox.com/916inrhragm2)
- [[Refrain] Gyakuten Saiban - Sono Shinjitsu - Igi Ari! Season 2 - 10 VOSTFR [720p]](https://uptobox.com/2z5x5s0ub3wn)
- [[Refrain] Gyakuten Saiban - Sono Shinjitsu - Igi Ari! Season 2 - 10 VOSTFR [1080p]](https://uptobox.com/zigfji8svitf)
- [[Refrain] Gyakuten Saiban - Sono Shinjitsu - Igi Ari! Season 2 - 11 VOSTFR [720p]](https://uptobox.com/hyexu0iwku5b)
- [[Refrain] Gyakuten Saiban - Sono Shinjitsu - Igi Ari! Season 2 - 11 VOSTFR [1080p]](https://uptobox.com/vrxldf77o7t3)
- [[Refrain] Gyakuten Saiban - Sono Shinjitsu - Igi Ari! Season 2 - 12 VOSTFR [720p]](https://uptobox.com/4h8d8f4zjdpm)
- [[Refrain] Gyakuten Saiban - Sono Shinjitsu - Igi Ari! Season 2 - 12 VOSTFR [1080p]](https://uptobox.com/lmmgw3cq8y0s)
- [[Refrain] Gyakuten Saiban - Sono Shinjitsu - Igi Ari! Season 2 - 13 VOSTFR [720p]](https://uptobox.com/8jhh2rj3x0g7)
- [[Refrain] Gyakuten Saiban - Sono Shinjitsu - Igi Ari! Season 2 - 13 VOSTFR [1080p]](https://uptobox.com/o11cv3y0i2el)
- [[Refrain] Gyakuten Saiban - Sono Shinjitsu - Igi Ari! Season 2 - 14 VOSTFR [720p]](https://uptobox.com/anfk5aellkux)
- [[Refrain] Gyakuten Saiban - Sono Shinjitsu - Igi Ari! Season 2 - 14 VOSTFR [1080p]](https://uptobox.com/nhiea6etizy3)
- [[Refrain] Gyakuten Saiban - Sono Shinjitsu - Igi Ari! Season 2 - 15 VOSTFR [720p]](https://uptobox.com/3d5wjnfunnwc)
- [[Refrain] Gyakuten Saiban - Sono Shinjitsu - Igi Ari! Season 2 - 15 VOSTFR [1080p]](https://uptobox.com/nmt4j6a7nhft)
- [[Refrain] Gyakuten Saiban - Sono Shinjitsu - Igi Ari! Season 2 - 16 VOSTFR [720p]](https://uptobox.com/4zqid2tt2zns)
- [[Refrain] Gyakuten Saiban - Sono Shinjitsu - Igi Ari! Season 2 - 16 VOSTFR [1080p]](https://uptobox.com/cctiflz0lono)
- [[Refrain] Gyakuten Saiban - Sono Shinjitsu - Igi Ari! Season 2 - 17 VOSTFR [720p]](https://uptobox.com/mmw6ysl85iyu)
- [[Refrain] Gyakuten Saiban - Sono Shinjitsu - Igi Ari! Season 2 - 17 VOSTFR [1080p]](https://uptobox.com/p3fna3uu0qhh)
- [[Refrain] Gyakuten Saiban - Sono Shinjitsu - Igi Ari! Season 2 - 2 VOSTFR [720p]](https://uptobox.com/marrzyk43mqh)
- [[Refrain] Gyakuten Saiban - Sono Shinjitsu - Igi Ari! Season 2 - 2 VOSTFR [1080p]](https://uptobox.com/gq4q4gmfhe8p)
- [[Refrain] Gyakuten Saiban - Sono Shinjitsu - Igi Ari! Season 2 - 19 VOSTFR [720p]](https://uptobox.com/2o89avqtg0nb)
- [[Refrain] Gyakuten Saiban - Sono Shinjitsu - Igi Ari! Season 2 - 19 VOSTFR [1080p]](https://uptobox.com/qd4o5oo5ezj6)
- [[Refrain] Gyakuten Saiban - Sono Shinjitsu - Igi Ari! Season 2 - 20 VOSTFR [720p]](https://uptobox.com/8jnrkr7398tn)
- [[Refrain] Gyakuten Saiban - Sono Shinjitsu - Igi Ari! Season 2 - 20 VOSTFR [1080p]](https://uptobox.com/xtj15390dom7)
- [[Refrain] Gyakuten Saiban - Sono Shinjitsu - Igi Ari! Season 2 - 21 VOSTFR [720p]](https://uptobox.com/m9pt6wae4ru6)
- [[Refrain] Gyakuten Saiban - Sono Shinjitsu - Igi Ari! Season 2 - 21 VOSTFR [1080p]](https://uptobox.com/ghnmlvgwks8r)
- [[Refrain] Gyakuten Saiban - Sono Shinjitsu - Igi Ari! Season 2 - 22 VOSTFR [720p]](https://uptobox.com/lj021lmkwrgi)
- [[Refrain] Gyakuten Saiban - Sono Shinjitsu - Igi Ari! Season 2 - 22 VOSTFR [1080p]](https://uptobox.com/w1kt25jj3y5w)
- [[Refrain] Gyakuten Saiban - Sono Shinjitsu - Igi Ari! Season 2 - 23 VOSTFR [720p]](https://uptobox.com/ddoz2dlx9c1r)
- [[Refrain] Gyakuten Saiban - Sono Shinjitsu - Igi Ari! Season 2 - 23 VOSTFR [1080p]](https://uptobox.com/ed19u6phuhse)
