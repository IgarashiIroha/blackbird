# Akanesasu Shoujo

![Akanesasu Shoujo](https://cdn.myanimelist.net/images/anime/1210/95781l.jpg)

* Japanese:  あかねさす少女

## Information

 - Type: TV
 - Episodes: 12
 - Status: Finished Airing
 - Aired: Oct 1, 2018 to Dec 17, 2018
 - Premiered: Fall 2018
 - Broadcast: Mondays at 19:30 (JST)
 - Producers: Yomiuri Telecasting, Animax, Pony Canyon, Asmik Ace, Studio Mausu, Exit Tunes, Kadokawa, Studio Shamrock, Gamegate 
 - Licensors: Sentai Filmworks, Ponycan USA 
 - Studios: DandeLion Animation Studio, Jumonji 
 - Source: Original
 - Genres: Action, School, Sci-Fi
 - Duration: 23 min. per ep.
 - Rating: PG-13 - Teens 13 or older


## Links

