# Thunderbolt Fantasy: Touriken Yuuki 2

![Thunderbolt Fantasy: Touriken Yuuki 2](https://s3.anilist.co/media/anime/cover/large/nx99557-Y9Thcex23ypW.png)

* Japanese:  Thunderbolt Fantasy: 東離劍遊紀

## Information

 - Type: TV
 - Episodes: 13
 - Status: Finished Airing
 - Aired: Oct 1, 2018 to Dec 24, 2018
 - Premiered: Fall 2018
 - Producers: Nitroplus
 - Source: Original
 - Genres: Action, Drama, Fantasy
 - Duration: 25 min. per ep.


## Links
- [[Refrain] Thunderbolt Fantasy - Tōriken Yūki 2 - 01 VOSTFR [720p]](https://uptobox.com/n5noyufsr3p5)
- [[Refrain] Thunderbolt Fantasy - Tōriken Yūki 2 - 01 VOSTFR [1080p]](https://uptobox.com/6i00tohtf6ri)
- [[Refrain] Thunderbolt Fantasy - Tōriken Yūki 2 - 02 VOSTFR [720p]](https://uptobox.com/cnn0lqua9yy5)
- [[Refrain] Thunderbolt Fantasy - Tōriken Yūki 2 - 02 VOSTFR [1080p]](https://uptobox.com/ik3tbrb4vk36)
- [[Refrain] Thunderbolt Fantasy - Tōriken Yūki 2 - 03 VOSTFR [720p]](https://uptobox.com/nq682cw4svr3)
- [[Refrain] Thunderbolt Fantasy - Tōriken Yūki 2 - 03 VOSTFR [1080p]](https://uptobox.com/bby651clnzk3)
- [[Refrain] Thunderbolt Fantasy - Tōriken Yūki 2 - 04 VOSTFR [720p]](https://uptobox.com/49a17ixhfqz3)
- [[Refrain] Thunderbolt Fantasy - Tōriken Yūki 2 - 04 VOSTFR [1080p]](https://uptobox.com/rxdlhdzkx570)
- [[Refrain] Thunderbolt Fantasy - Tōriken Yūki 2 - 05 VOSTFR [720p]](https://uptobox.com/sgym8ir7wcyq)
- [[Refrain] Thunderbolt Fantasy - Tōriken Yūki 2 - 05 VOSTFR [1080p]](https://uptobox.com/jn7c7j2ktj2q)
- [[Refrain] Thunderbolt Fantasy - Tōriken Yūki 2 - 06 VOSTFR [720p]](https://uptobox.com/dxgsmj41qwfj)
- [[Refrain] Thunderbolt Fantasy - Tōriken Yūki 2 - 06 VOSTFR [1080p]](https://uptobox.com/o7nbhlffpkqz)
- [[Refrain] Thunderbolt Fantasy - Tōriken Yūki 2 - 07 VOSTFR [720p]](https://uptobox.com/r6e9fxbig1xd)
- [[Refrain] Thunderbolt Fantasy - Tōriken Yūki 2 - 07 VOSTFR [1080p]](https://uptobox.com/56mphhydlr6x)
- [[Refrain] Thunderbolt Fantasy - Tōriken Yūki 2 - 08 VOSTFR [720p]](https://uptobox.com/8mey5koy89y1)
- [[Refrain] Thunderbolt Fantasy - Tōriken Yūki 2 - 08 VOSTFR [1080p]](https://uptobox.com/mcfy910s3wkp)
- [[Refrain] Thunderbolt Fantasy - Tōriken Yūki 2 - 09 VOSTFR [720p]](https://uptobox.com/2veefmoctf6a)
- [[Refrain] Thunderbolt Fantasy - Tōriken Yūki 2 - 09 VOSTFR [1080p]](https://uptobox.com/o0qzlgynksm6)
- [[Refrain] Thunderbolt Fantasy - Tōriken Yūki 2 - 10 VOSTFR [720p]](https://uptobox.com/5felvonkp3a9)
- [[Refrain] Thunderbolt Fantasy - Tōriken Yūki 2 - 10 VOSTFR [1080p]](https://uptobox.com/o3u0a0fus2c4)
- [[Refrain] Thunderbolt Fantasy - Tōriken Yūki 2 - 11 VOSTFR [720p]](https://uptobox.com/34r60ln6nt1a)
- [[Refrain] Thunderbolt Fantasy - Tōriken Yūki 2 - 11 VOSTFR [1080p]](https://uptobox.com/t2hck5q753hh)
- [[Refrain] Thunderbolt Fantasy - Tōriken Yūki 2 - 12 VOSTFR [720p]](https://uptobox.com/sq244c1r3jb7)
- [[Refrain] Thunderbolt Fantasy - Tōriken Yūki 2 - 12 VOSTFR [1080p]](https://uptobox.com/u0xjxj5p6udf)
- [[Refrain] Thunderbolt Fantasy - Tōriken Yūki 2 - 13 VOSTFR [720p]](https://uptobox.com/3aogs5285uwe)
- [[Refrain] Thunderbolt Fantasy - Tōriken Yūki 2 - 13 VOSTFR [1080p]](https://uptobox.com/vbh99bygnagj)
