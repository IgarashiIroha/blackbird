# Tsurune: Kazemai Koukou Kyuudoubu

![Tsurune: Kazemai Koukou Kyuudoubu](https://cdn.myanimelist.net/images/anime/1360/93571l.jpg)

* Japanese:  ツルネ ―風舞高校弓道部―

## Information

 - Type: TV
 - Episodes: 13
 - Status: Currently Airing
 - Aired: Oct 22, 2018 to Jan 21, 2019
 - Premiered: Fall 2018
 - Broadcast: Mondays at 00:10 (JST)
 - Producers: Lantis, Pony Canyon, Rakuonsha, Bandai Namco Arts 
 - Licensors: Sentai Filmworks, Ponycan USA 
 - Studios: Kyoto Animation 
 - Source: Light novel
 - Genres: Sports, Drama, School
 - Duration: 24 min. per ep.
 - Rating: PG-13 - Teens 13 or older


## Links
- [[Refrain] Tsurune - Kazemai Koukou Kyuudoubu - 01 VOSTFR [720p]](https://uptobox.com/006gchrcg8oi)
- [[Refrain] Tsurune - Kazemai Koukou Kyuudoubu - 01 VOSTFR [1080p]](https://uptobox.com/fom21u3wc6mi)
- [[Refrain] Tsurune - Kazemai Koukou Kyuudoubu - 02 VOSTFR [720p]](https://uptobox.com/gterog75ihtj)
- [[Refrain] Tsurune - Kazemai Koukou Kyuudoubu - 02 VOSTFR [1080p]](https://uptobox.com/k7gbih2lsrkl)
- [[Refrain] Tsurune - Kazemai Koukou Kyuudoubu - 03 VOSTFR [720p]](https://uptobox.com/pulyjurbqodg)
- [[Refrain] Tsurune - Kazemai Koukou Kyuudoubu - 03 VOSTFR [1080p]](https://uptobox.com/3zq3pc7djxwk)
- [[Refrain] Tsurune - Kazemai Koukou Kyuudoubu - 04 VOSTFR [720p]](https://uptobox.com/kirjccchm3kf)
- [[Refrain] Tsurune - Kazemai Koukou Kyuudoubu - 04 VOSTFR [1080p]](https://uptobox.com/o1ot8bo5fkei)
- [[Refrain] Tsurune - Kazemai Koukou Kyuudoubu - 05 VOSTFR [720p]](https://uptobox.com/ce2uaasj43my)
- [[Refrain] Tsurune - Kazemai Koukou Kyuudoubu - 05 VOSTFR [1080p]](https://uptobox.com/rinepgiphwso)
- [[Refrain] Tsurune - Kazemai Koukou Kyuudoubu - 06 VOSTFR [720p]](https://uptobox.com/3rwznr6hhzx0)
- [[Refrain] Tsurune - Kazemai Koukou Kyuudoubu - 06 VOSTFR [1080p]](https://uptobox.com/yj8jc7mvnzzb)
- [[Refrain] Tsurune - Kazemai Koukou Kyuudoubu - 07 VOSTFR [720p]](https://uptobox.com/fl81gsn0252w)
- [[Refrain] Tsurune - Kazemai Koukou Kyuudoubu - 07 VOSTFR [1080p]](https://uptobox.com/9k1ka457s2k8)
- [[Refrain] Tsurune - Kazemai Koukou Kyuudoubu - 08 VOSTFR [720p]](https://uptobox.com/k56aaw96a6ao)
- [[Refrain] Tsurune - Kazemai Koukou Kyuudoubu - 08 VOSTFR [1080p]](https://uptobox.com/dni9nkrie3iu)
- [[Refrain] Tsurune - Kazemai Koukou Kyuudoubu - 09 VOSTFR [720p]](https://uptobox.com/1pwni4c743dp)
- [[Refrain] Tsurune - Kazemai Koukou Kyuudoubu - 09 VOSTFR [1080p]](https://uptobox.com/1h6gbiqzqw8b)
- [[Refrain] Tsurune - Kazemai Koukou Kyuudoubu - 10 VOSTFR [720p]](https://uptobox.com/ippvi85tj2q9)
- [[Refrain] Tsurune - Kazemai Koukou Kyuudoubu - 10 VOSTFR [1080p]](https://uptobox.com/qlud2a0t2p84)
- [[Refrain] Tsurune - Kazemai Koukou Kyuudoubu - 11 VOSTFR [720p]](https://uptobox.com/9ffqtqd04u3m)
- [[Refrain] Tsurune - Kazemai Koukou Kyuudoubu - 11 VOSTFR [1080p]](https://uptobox.com/hac5pe2wt6jp)
- [[Refrain] Tsurune - Kazemai Koukou Kyuudoubu - 12 VOSTFR [720p]](https://uptobox.com/5xiwdt1c9dh2)
- [[Refrain] Tsurune - Kazemai Koukou Kyuudoubu - 12 VOSTFR [1080p]](https://uptobox.com/7jutxxc2b8gd)
- [[Refrain] Tsurune - Kazemai Koukou Kyuudoubu - 13 VOSTFR [720p]](https://uptobox.com/k7152ry3v0lv)
- [[Refrain] Tsurune - Kazemai Koukou Kyuudoubu - 13 VOSTFR [1080p]](https://uptobox.com/g4d5vuaulbet)
- [[Refrain] Tsurune - Kazemai Koukou Kyuudoubu - 14 VOSTFR [720p]](https://uptobox.com/dqhl398f8o0u)
- [[Refrain] Tsurune - Kazemai Koukou Kyuudoubu - 14 VOSTFR [1080p]](https://uptobox.com/wzckzrxsts1u)
