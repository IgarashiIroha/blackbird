# Gaikotsu Shotenin Honda-san

![Gaikotsu Shotenin Honda-san](https://cdn.myanimelist.net/images/anime/1555/93865l.jpg)

* Japanese:  ガイコツ書店員本田さん

## Information

 - Type: TV
 - Episodes: 12
 - Status: Finished Airing
 - Aired: Oct 8, 2018 to Dec 24, 2018
 - Premiered: Fall 2018
 - Broadcast: Mondays at 00:30 (JST)
 - Producers: Lantis, Half H.P Studio 
 - Licensors: None found, add some 
 - Studios: DLE 
 - Source: Web manga
 - Genres: Slice of Life, Comedy
 - Duration: 11 min. per ep.
 - Rating: PG-13 - Teens 13 or older


## Links
- [[Refrain] Gaikotsu Shotenin Honda-san - 01 VOSTFR [720p]](https://uptobox.com/2kjhwr1bi1zg)
- [[Refrain] Gaikotsu Shotenin Honda-san - 01 VOSTFR [1080p]](https://uptobox.com/2y60fagtmay7)
- [[Refrain] Gaikotsu Shotenin Honda-san - 02 VOSTFR [720p]](https://uptobox.com/clgksbi8n66v)
- [[Refrain] Gaikotsu Shotenin Honda-san - 02 VOSTFR [1080p]](https://uptobox.com/uysci4zn34uh)
- [[Refrain] Gaikotsu Shotenin Honda-san - 03 VOSTFR [720p]](https://uptobox.com/bu5flxp1mkr6)
- [[Refrain] Gaikotsu Shotenin Honda-san - 03 VOSTFR [1080p]](https://uptobox.com/kpxt3vnulify)
- [[Refrain] Gaikotsu Shotenin Honda-san - 04 VOSTFR [720p]](https://uptobox.com/7emkyhkaneon)
- [[Refrain] Gaikotsu Shotenin Honda-san - 04 VOSTFR [1080p]](https://uptobox.com/k3ciyq0ivq70)
- [[Refrain] Gaikotsu Shotenin Honda-san - 05 VOSTFR [720p]](https://uptobox.com/hl3lwdi4u909)
- [[Refrain] Gaikotsu Shotenin Honda-san - 05 VOSTFR [1080p]](https://uptobox.com/w73bbvse9046)
- [[Refrain] Gaikotsu Shotenin Honda-san - 06 VOSTFR [720p]](https://uptobox.com/cffxharfx0od)
- [[Refrain] Gaikotsu Shotenin Honda-san - 06 VOSTFR [1080p]](https://uptobox.com/2l5x6lroeum3)
- [[Refrain] Gaikotsu Shotenin Honda-san - 07 VOSTFR [720p]](https://uptobox.com/yydg7mgxghwy)
- [[Refrain] Gaikotsu Shotenin Honda-san - 07 VOSTFR [1080p]](https://uptobox.com/kmrx7w77umpo)
- [[Refrain] Gaikotsu Shotenin Honda-san - 08 VOSTFR [720p]](https://uptobox.com/du2nmefmvso4)
- [[Refrain] Gaikotsu Shotenin Honda-san - 08 VOSTFR [1080p]](https://uptobox.com/oeeccsj4xbh1)
- [[Refrain] Gaikotsu Shotenin Honda-san - 09 VOSTFR [720p]](https://uptobox.com/x4klxfo5qu8r)
- [[Refrain] Gaikotsu Shotenin Honda-san - 09 VOSTFR [1080p]](https://uptobox.com/9ckdg14dwd69)
- [[Refrain] Gaikotsu Shotenin Honda-san - 10 VOSTFR [720p]](https://uptobox.com/pj8m2sz2ze86)
- [[Refrain] Gaikotsu Shotenin Honda-san - 10 VOSTFR [1080p]](https://uptobox.com/0hi28fvlo3qz)
- [[Refrain] Gaikotsu Shotenin Honda-san - 11 VOSTFR [720p]](https://uptobox.com/v3waswlqg6my)
- [[Refrain] Gaikotsu Shotenin Honda-san - 11 VOSTFR [1080p]](https://uptobox.com/poz60keq9blp)
- [[Refrain] Gaikotsu Shotenin Honda-san - 12 VOSTFR [720p]](https://uptobox.com/king3i8244ut)
- [[Refrain] Gaikotsu Shotenin Honda-san - 12 VOSTFR [1080p]](https://uptobox.com/8lb7oy03fmbl)
