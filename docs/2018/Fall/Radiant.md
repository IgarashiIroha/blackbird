# Radiant

![Radiant](https://cdn.myanimelist.net/images/anime/1318/95345l.jpg)

* Japanese:  ラディアン

## Information

- Type: TV
- Episodes: 21
- Status: Currently Airing
- Aired: Oct 6, 2018 to ?
- Premiered: Fall 2018
- Broadcast: Saturdays at 17:35 (JST)
- Producers: Studio Hibari, NHK, NHK Enterprises
- Licensors: Funimation
- Studios: Lerche
- Source: Other
- Genres: Action, Adventure, Magic, Fantasy
- Duration: 24 min. per ep.
- Rating: PG-13 - Teens 13 or older


## Links
- [[Refrain] Radiant - 01 VOSTFR [720p]](https://uptobox.com/93pj5sc2u5wf)
- [[Refrain] Radiant - 01 VOSTFR [1080p]](https://uptobox.com/qeqaohr3bf4w)
- [[Refrain] Radiant - 01 VOSTFR {ADN} [720p]](https://uptobox.com/mk38ig2cvts9)
- [[Refrain] Radiant - 01 VOSTFR {ADN} [1080p]](https://uptobox.com/1o396e0mr7rj)
- [[Refrain] Radiant - 02 VOSTFR [720p]](https://uptobox.com/78xwka2ap6eg)
- [[Refrain] Radiant - 02 VOSTFR [1080p]](https://uptobox.com/h4okzplg5qz5)
- [[Refrain] Radiant - 02 VOSTFR {ADN} [720p]](https://uptobox.com/fexygxn5t3j2)
- [[Refrain] Radiant - 02 VOSTFR {ADN} [1080p]](https://uptobox.com/g6v5hmr7hq2p)
- [[Refrain] Radiant - 03 VOSTFR [720p]](https://uptobox.com/pl2h0geqd1p4)
- [[Refrain] Radiant - 03 VOSTFR [1080p]](https://uptobox.com/y79zaay4ao28)
- [[Refrain] Radiant - 03 VOSTFR {ADN} [720p]](https://uptobox.com/loqhozvf79tw)
- [[Refrain] Radiant - 03 VOSTFR {ADN} [1080p]](https://uptobox.com/19zkfuheoaoa)
- [[Refrain] Radiant - 04 VOSTFR [720p]](https://uptobox.com/hmc5v0y99149)
- [[Refrain] Radiant - 04 VOSTFR [1080p]](https://uptobox.com/x2l2s9i0p96x)
- [[Refrain] Radiant - 04 VOSTFR {ADN} [720p]](https://uptobox.com/iyyobk1rqd40)
- [[Refrain] Radiant - 04 VOSTFR {ADN} [1080p]](https://uptobox.com/ohrkm6evvnhh)
- [[Refrain] Radiant - 05 VOSTFR [720p]](https://uptobox.com/e61yolalx1mg)
- [[Refrain] Radiant - 05 VOSTFR [1080p]](https://uptobox.com/oky37f8fvxli)
- [[Refrain] Radiant - 05 VOSTFR {ADN} [720p]](https://uptobox.com/70a01axakw5n)
- [[Refrain] Radiant - 05 VOSTFR {ADN} [1080p]](https://uptobox.com/f43g2ndx4zwj)
- [[Refrain] Radiant - 06 VOSTFR [720p]](https://uptobox.com/yd2rzp1hgke2)
- [[Refrain] Radiant - 06 VOSTFR [1080p]](https://uptobox.com/d5j7w8bgtoai)
- [[Refrain] Radiant - 06 VOSTFR {ADN} [720p]](https://uptobox.com/ommz28va8ju4)
- [[Refrain] Radiant - 06 VOSTFR {ADN} [1080p]](https://uptobox.com/onzi4k2gbfzu)
- [[Refrain] Radiant - 07 VOSTFR [720p]](https://uptobox.com/s9sn378j8qfg)
- [[Refrain] Radiant - 07 VOSTFR [1080p]](https://uptobox.com/gh5qfd1b00ho)
- [[Refrain] Radiant - 07 VOSTFR {ADN} [720p]](https://uptobox.com/lpxbe6cx7kbm)
- [[Refrain] Radiant - 07 VOSTFR {ADN} [1080p]](https://uptobox.com/1p8wtqczs92l)
- [[Refrain] Radiant - 08 VOSTFR [720p]](https://uptobox.com/8njlxf9yhfz2)
- [[Refrain] Radiant - 08 VOSTFR [1080p]](https://uptobox.com/u5iemvkrmzlj)
- [[Refrain] Radiant - 08 VOSTFR {ADN} [720p]](https://uptobox.com/yn301yjgyuay)
- [[Refrain] Radiant - 08 VOSTFR {ADN} [1080p]](https://uptobox.com/m448q9m1uv0v)
- [[Refrain] Radiant - 09 VOSTFR [720p]](https://uptobox.com/3lkki4dzgaw8)
- [[Refrain] Radiant - 09 VOSTFR [1080p]](https://uptobox.com/ujsuo8e3vm62)
- [[Refrain] Radiant - 09 VOSTFR {ADN} [720p]](https://uptobox.com/j5xu6z37m811)
- [[Refrain] Radiant - 09 VOSTFR {ADN} [1080p]](https://uptobox.com/cvx3r99xre5l)
- [[Refrain] Radiant - 10 VOSTFR [720p]](https://uptobox.com/uc2allv4i4dc)
- [[Refrain] Radiant - 10 VOSTFR [1080p]](https://uptobox.com/f5aynep7ef35)
- [[Refrain] Radiant - 10 VOSTFR {ADN} [720p]](https://uptobox.com/8u1x73nltzj1)
- [[Refrain] Radiant - 10 VOSTFR {ADN} [1080p]](https://uptobox.com/5zmzhwysb7sh)
- [[Refrain] Radiant - 11 VOSTFR [720p]](https://uptobox.com/kmrysjpmzrh1)
- [[Refrain] Radiant - 11 VOSTFR [1080p]](https://uptobox.com/o09558h2eh71)
- [[Refrain] Radiant - 11 VOSTFR {ADN} [720p]](https://uptobox.com/voti6sjhomuy)
- [[Refrain] Radiant - 11 VOSTFR {ADN} [1080p]](https://uptobox.com/i2czsy1gw8qe)
- [[Refrain] Radiant - 12 VOSTFR [720p]](https://uptobox.com/wy1rfr6r9fl3)
- [[Refrain] Radiant - 12 VOSTFR [1080p]](https://uptobox.com/efhzrdxdw8db)
- [[Refrain] Radiant - 12 VOSTFR {ADN} [720p]](https://uptobox.com/0b2wxfrjlnqr)
- [[Refrain] Radiant - 12 VOSTFR {ADN} [1080p]](https://uptobox.com/52jc9xm3fj6k)
- [[Refrain] Radiant - 13 VOSTFR [720p]](https://uptobox.com/8gznp20enzix)
- [[Refrain] Radiant - 13 VOSTFR [1080p]](https://uptobox.com/i3qi6oi2jg6y)
- [[Refrain] Radiant - 13 VOSTFR {ADN} [720p]](https://uptobox.com/sfhfnpeq3xmq)
- [[Refrain] Radiant - 13 VOSTFR {ADN} [1080p]](https://uptobox.com/yorj8phhk8od)
- [[Refrain] Radiant - 14 VOSTFR [720p]](https://uptobox.com/hupepuj8zdgb)
- [[Refrain] Radiant - 14 VOSTFR [1080p]](https://uptobox.com/7ogdyuwvfn8o)
- [[Refrain] Radiant - 14 VOSTFR {ADN} [720p]](https://uptobox.com/9z5tjw9eazj9)
- [[Refrain] Radiant - 14 VOSTFR {ADN} [1080p]](https://uptobox.com/29syhdiq0wsp)
- [[Refrain] Radiant - 15 VOSTFR [720p]](https://uptobox.com/5bfzmb663szm)
- [[Refrain] Radiant - 15 VOSTFR [1080p]](https://uptobox.com/61b9a85t2bl1)
- [[Refrain] Radiant - 15 VOSTFR {ADN} [720p]](https://uptobox.com/7vkve6cj83th)
- [[Refrain] Radiant - 15 VOSTFR {ADN} [1080p]](https://uptobox.com/xfbg0bwsqspl)
- [[Refrain] Radiant - 16 VOSTFR [720p]](https://uptobox.com/g4sk8l805b2i)
- [[Refrain] Radiant - 16 VOSTFR [1080p]](https://uptobox.com/eiijhr6325m3)
- [[Refrain] Radiant - 16 VOSTFR {ADN} [720p]](https://uptobox.com/4p3lqj4spc02)
- [[Refrain] Radiant - 16 VOSTFR {ADN} [1080p]](https://uptobox.com/ruks363oux54)
- [[Refrain] Radiant - 17 VOSTFR {ADN} [720p]](https://uptobox.com/l9kkn6o4xzwu)
- [[Refrain] Radiant - 17 VOSTFR {ADN} [1080p]](https://uptobox.com/n628kj66dtak)
- [[Refrain] Radiant - 18 VOSTFR {ADN} [720p]](https://uptobox.com/1ogqjk4rgu90)
- [[Refrain] Radiant - 18 VOSTFR {ADN} [1080p]](https://uptobox.com/eu14gmzasebh)
- [[Refrain] Radiant - 19 VOSTFR {ADN} [720p]](https://uptobox.com/rf3em4pk7k6o)
- [[Refrain] Radiant - 19 VOSTFR {ADN} [1080p]](https://uptobox.com/ixay3fvr9pxl)
- [[Refrain] Radiant - 21 VOSTFR [720p]](https://uptobox.com/gwa0tp4b40ti)
- [[Refrain] Radiant - 21 VOSTFR [1080p]](https://uptobox.com/q67y0bplwfbn)
- [[Refrain] Radiant - 20 VOSTFR [720p]](https://uptobox.com/sosm9zkbwx4g)
- [[Refrain] Radiant - 20 VOSTFR [1080p]](https://uptobox.com/7d6mygujjii2)
- [[Refrain] Radiant - 19 VOSTFR [720p]](https://uptobox.com/in730n5hswgp)
- [[Refrain] Radiant - 19 VOSTFR [1080p]](https://uptobox.com/2fjvdv08ai8v)
- [[Refrain] Radiant - 18 VOSTFR [720p]](https://uptobox.com/1o7e6s06k73h)
- [[Refrain] Radiant - 18 VOSTFR [1080p]](https://uptobox.com/gjdjb64h29zh)
- [[Refrain] Radiant - 17 VOSTFR [720p]](https://uptobox.com/p3bl94slncbr)
- [[Refrain] Radiant - 17 VOSTFR [1080p]](https://uptobox.com/cawfqvj5z8yd)
- [[Refrain] Radiant - 20 VOSTFR {ADN} [720p]](https://uptobox.com/1rnxmc2ej8tc)
- [[Refrain] Radiant - 20 VOSTFR {ADN} [1080p]](https://uptobox.com/kyhstz3njxyv)
- [[Refrain] Radiant - 21 VOSTFR {ADN} [720p]](https://uptobox.com/y2r6ry1a5b1l)
- [[Refrain] Radiant - 21 VOSTFR {ADN} [1080p]](https://uptobox.com/xamopyyycuqy)
