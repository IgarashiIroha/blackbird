# Hangyakusei Million Arthur

![Hangyakusei Million Arthur](https://cdn.myanimelist.net/images/anime/1679/96158l.jpg)

* Japanese:  叛逆性ミリオンアーサー

## Information

 - Type: TV
 - Episodes: 10
 - Status: Finished Airing
 - Aired: Oct 25, 2018 to Dec 27, 2018
 - Premiered: Fall 2018
 - Broadcast: Thursdays at 22:00 (JST)
 - Producers: Square Enix, Sotsu, Genco, Lantis, Magic Capsule, Fields, bilibili, Bandai Namco Arts, Happinet 
 - Licensors: None found, add some 
 - Studios: J.C.Staff 
 - Source: Game
 - Genres: Action, Adventure, Magic, Fantasy
 - Duration: 24 min. per ep.
 - Rating: PG-13 - Teens 13 or older


## Links

