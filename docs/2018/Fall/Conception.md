# Conception

![Conception](https://cdn.myanimelist.net/images/anime/1849/95019l.jpg)

* Japanese:  CONCEPTION（コンセプション）

## Information

 - Type: TV
 - Episodes: 12
 - Status: Finished Airing
 - Aired: Oct 10, 2018 to Dec 26, 2018
 - Premiered: Fall 2018
 - Broadcast: Wednesdays at 01:30 (JST)
 - Producers: None found, add some 
 - Licensors: Funimation 
 - Studios: Gonzo 
 - Source: Game
 - Genres: Action, Adventure, Harem, Magic, Romance, Fantasy
 - Duration: 23 min. per ep.
 - Rating: PG-13 - Teens 13 or older


## Links
- [[Refrain] Conception - 01 VOSTFR [720p]](https://uptobox.com/rfnhktyaqec3)
- [[Refrain] Conception - 01 VOSTFR [1080p]](https://uptobox.com/l9n6bz7kui84)
- [[Refrain] Conception - 02 VOSTFR [720p]](https://uptobox.com/rlccaeme3o03)
- [[Refrain] Conception - 02 VOSTFR [1080p]](https://uptobox.com/qthaszbuyigd)
- [[Refrain] Conception - 03 VOSTFR [720p]](https://uptobox.com/awe6vip2za1z)
- [[Refrain] Conception - 03 VOSTFR [1080p]](https://uptobox.com/cbr9k3tk4bw0)
- [[Refrain] Conception - 04 VOSTFR [720p]](https://uptobox.com/khxwj06z1c3y)
- [[Refrain] Conception - 04 VOSTFR [1080p]](https://uptobox.com/d5wy13xy0phe)
- [[Refrain] Conception - 05 VOSTFR [720p]](https://uptobox.com/lbl3pg9vukjj)
- [[Refrain] Conception - 05 VOSTFR [1080p]](https://uptobox.com/bpq65u97k7za)
- [[Refrain] Conception - 06 VOSTFR [720p]](https://uptobox.com/0qkldiiw1dmd)
- [[Refrain] Conception - 06 VOSTFR [1080p]](https://uptobox.com/ev9bufwxooii)
- [[Refrain] Conception - 07 VOSTFR [720p]](https://uptobox.com/nsbftto8d529)
- [[Refrain] Conception - 07 VOSTFR [1080p]](https://uptobox.com/iurh6kcx97t5)
- [[Refrain] Conception - 08 VOSTFR [720p]](https://uptobox.com/ne7jsx7ae5f4)
- [[Refrain] Conception - 08 VOSTFR [1080p]](https://uptobox.com/c6g8cqnr7nb9)
- [[Refrain] Conception - 09 VOSTFR [720p]](https://uptobox.com/s3bmfzxy5lnm)
- [[Refrain] Conception - 09 VOSTFR [1080p]](https://uptobox.com/a4s3bomjceft)
- [[Refrain] Conception - 10 VOSTFR [720p]](https://uptobox.com/i7vhibrnmm1i)
- [[Refrain] Conception - 10 VOSTFR [1080p]](https://uptobox.com/rv2ss606f77b)
- [[Refrain] Conception - 11 VOSTFR [720p]](https://uptobox.com/ucx2nubbgac9)
- [[Refrain] Conception - 11 VOSTFR [1080p]](https://uptobox.com/jod93mybxvtw)
- [[Refrain] Conception - 12 VOSTFR [720p]](https://uptobox.com/h2wopp5kzqhe)
- [[Refrain] Conception - 12 VOSTFR [1080p]](https://uptobox.com/0hvh7myt5sm2)
