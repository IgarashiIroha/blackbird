# Golden Kamuy 2nd Season

![Golden Kamuy 2nd Season](https://cdn.myanimelist.net/images/anime/1180/95018l.jpg)

* Japanese:  ゴールデンカムイ 第二期

## Information

 - Type: TV
 - Episodes: 12
 - Status: Finished Airing
 - Aired: Oct 8, 2018 to Dec 24, 2018
 - Premiered: Fall 2018
 - Broadcast: Mondays at 23:00 (JST)
 - Producers: Magic Capsule, NBCUniversal Entertainment Japan, Tokyo MX, Good Smile Company, Sammy, Shueisha, East Japan Marketing & Communications, Twin Engine 
 - Licensors: Funimation 
 - Studios: Geno Studio 
 - Source: Manga
 - Genres: Action, Adventure, Historical, Seinen
 - Duration: 23 min. per ep.
 - Rating: R - 17+ (violence & profanity)


## Links
- [[Refrain] Golden Kamuy 2nd Season - 13 VOSTFR [720p]](https://uptobox.com/0gsqpgrjqtk4)
- [[Refrain] Golden Kamuy 2nd Season - 13 VOSTFR [1080p]](https://uptobox.com/fz77nfxulwp7)
- [[Refrain] Golden Kamuy 2nd Season - 14 VOSTFR [720p]](https://uptobox.com/00tynd3jiez6)
- [[Refrain] Golden Kamuy 2nd Season - 14 VOSTFR [1080p]](https://uptobox.com/uyho55b6bhua)
- [[Refrain] Golden Kamuy 2nd Season - 15 VOSTFR [720p]](https://uptobox.com/ftave5sx4d5j)
- [[Refrain] Golden Kamuy 2nd Season - 15 VOSTFR [1080p]](https://uptobox.com/pinvb25ge02h)
- [[Refrain] Golden Kamuy 2nd Season - 16 VOSTFR [720p]](https://uptobox.com/a2dsqyrl5xgn)
- [[Refrain] Golden Kamuy 2nd Season - 16 VOSTFR [1080p]](https://uptobox.com/6gmxgyde4eoi)
- [[Refrain] Golden Kamuy 2nd Season - 17 VOSTFR [720p]](https://uptobox.com/xhijahctthp1)
- [[Refrain] Golden Kamuy 2nd Season - 17 VOSTFR [1080p]](https://uptobox.com/3ecqcliz0kqx)
- [[Refrain] Golden Kamuy 2nd Season - 18 VOSTFR [720p]](https://uptobox.com/a57qntull356)
- [[Refrain] Golden Kamuy 2nd Season - 18 VOSTFR [1080p]](https://uptobox.com/kyulvk6hl3v5)
- [[Refrain] Golden Kamuy 2nd Season - 19 VOSTFR [720p]](https://uptobox.com/rdkr2jf9lp5c)
- [[Refrain] Golden Kamuy 2nd Season - 19 VOSTFR [1080p]](https://uptobox.com/0aluus04amcv)
- [[Refrain] Golden Kamuy 2nd Season - 20 VOSTFR [720p]](https://uptobox.com/n0exjmm5kvv4)
- [[Refrain] Golden Kamuy 2nd Season - 20 VOSTFR [1080p]](https://uptobox.com/wqmidqhsea1z)
- [[Refrain] Golden Kamuy 2nd Season - 21 VOSTFR [720p]](https://uptobox.com/xthdf8a7ij2e)
- [[Refrain] Golden Kamuy 2nd Season - 21 VOSTFR [1080p]](https://uptobox.com/rjr8nikfosjr)
- [[Refrain] Golden Kamuy 2nd Season - 22 VOSTFR [720p]](https://uptobox.com/i250m23egyg9)
- [[Refrain] Golden Kamuy 2nd Season - 22 VOSTFR [1080p]](https://uptobox.com/wxsq4bn6f89h)
- [[Refrain] Golden Kamuy 2nd Season - 23 VOSTFR [720p]](https://uptobox.com/q1vdm2klt6m7)
- [[Refrain] Golden Kamuy 2nd Season - 23 VOSTFR [1080p]](https://uptobox.com/zswx7808exjf)
- [[Refrain] Golden Kamuy 2nd Season - 24 VOSTFR [720p]](https://uptobox.com/dhz84cp5ci8g)
- [[Refrain] Golden Kamuy 2nd Season - 24 VOSTFR [1080p]](https://uptobox.com/1dipwd9khbk5)
