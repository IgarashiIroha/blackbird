# Toaru Majutsu no Index III

![Toaru Majutsu no Index III](https://cdn.myanimelist.net/images/anime/1583/93857l.jpg)

* Japanese:  とある魔術の禁書目録Ⅲ

## Information

 - Type: TV
 - Episodes: 26
 - Status: Currently Airing
 - Aired: Oct 5, 2018 to ?
 - Premiered: Fall 2018
 - Broadcast: Fridays at 22:00 (JST)
 - Producers: Square Enix, Movic, AT-X, Warner Bros. Japan, NBCUniversal Entertainment Japan, Kadokawa
 - Licensors: None found, add some
 - Studios: J.C.Staff
 - Source: Light novel
 - Genres: Action, Magic, Sci-Fi, Super Power
 - Duration: 23 min. per ep.
 - Rating: PG-13 - Teens 13 or older


## Links

- [[Refrain] Toaru Majutsu no Index III - 01 VOSTFR [720p]](https://uptobox.com/ix914pzwpz0u)
- [[Refrain] Toaru Majutsu no Index III - 01 VOSTFR [1080p]](https://uptobox.com/69j1czteb9gg)
- [[Refrain] Toaru Majutsu no Index III - 02 VOSTFR [720p]](https://uptobox.com/2cu0gcf8zawp)
- [[Refrain] Toaru Majutsu no Index III - 02 VOSTFR [1080p]](https://uptobox.com/ox2jrq3c3lsp)
- [[Refrain] Toaru Majutsu no Index III - 03 VOSTFR [720p]](https://uptobox.com/urbn1bzgnkq1)
- [[Refrain] Toaru Majutsu no Index III - 03 VOSTFR [1080p]](https://uptobox.com/ufj98tikb7u5)
- [[Refrain] Toaru Majutsu no Index III - 04 VOSTFR [720p]](https://uptobox.com/vttqsqzqaxxo)
- [[Refrain] Toaru Majutsu no Index III - 04 VOSTFR [1080p]](https://uptobox.com/ntfc73gbbmmi)
- [[Refrain] Toaru Majutsu no Index III - 05 VOSTFR [720p]](https://uptobox.com/k07cudx1fec9)
- [[Refrain] Toaru Majutsu no Index III - 05 VOSTFR [1080p]](https://uptobox.com/83ta77hn5282)
- [[Refrain] Toaru Majutsu no Index III - 06 VOSTFR [720p]](https://uptobox.com/bhje2vhohoo1)
- [[Refrain] Toaru Majutsu no Index III - 06 VOSTFR [1080p]](https://uptobox.com/ut1qh3dgjs0j)
- [[Refrain] Toaru Majutsu no Index III - 07 VOSTFR [720p]](https://uptobox.com/ca2bxb24bfhd)
- [[Refrain] Toaru Majutsu no Index III - 07 VOSTFR [1080p]](https://uptobox.com/hv1gme29oayp)
- [[Refrain] Toaru Majutsu no Index III - 08 VOSTFR [720p]](https://uptobox.com/yhvi003xda7c)
- [[Refrain] Toaru Majutsu no Index III - 08 VOSTFR [1080p]](https://uptobox.com/x6bo2pzi5v9j)
- [[Refrain] Toaru Majutsu no Index III - 09 VOSTFR [720p]](https://uptobox.com/3m65orrmnjxp)
- [[Refrain] Toaru Majutsu no Index III - 09 VOSTFR [1080p]](https://uptobox.com/g6q440gd7kcb)
- [[Refrain] Toaru Majutsu no Index III - 10 VOSTFR [720p]](https://uptobox.com/fgf637r94hib)
- [[Refrain] Toaru Majutsu no Index III - 10 VOSTFR [1080p]](https://uptobox.com/dsf3426uewit)
- [[Refrain] Toaru Majutsu no Index III - 11 VOSTFR [720p]](https://uptobox.com/ftw58vtce9qe)
- [[Refrain] Toaru Majutsu no Index III - 11 VOSTFR [1080p]](https://uptobox.com/3eytenn1cz75)
- [[Refrain] Toaru Majutsu no Index III - 12 VOSTFR [720p]](https://uptobox.com/kwgeewr7aqay)
- [[Refrain] Toaru Majutsu no Index III - 12 VOSTFR [1080p]](https://uptobox.com/hkf2xxxro93g)
- [[Refrain] Toaru Majutsu no Index III - 13 VOSTFR [720p]](https://uptobox.com/i1qx6xon6hg9)
- [[Refrain] Toaru Majutsu no Index III - 13 VOSTFR [1080p]](https://uptobox.com/4tys9u7firjk)
- [[Refrain] Toaru Majutsu no Index III - 14 VOSTFR [720p]](https://uptobox.com/upbfvc9artbo)
- [[Refrain] Toaru Majutsu no Index III - 14 VOSTFR [1080p]](https://uptobox.com/v7zis1p9cdyl)
- [[Refrain] Toaru Majutsu no Index III - 15 VOSTFR [720p]](https://uptobox.com/tv90js3gc7rx)
- [[Refrain] Toaru Majutsu no Index III - 15 VOSTFR [1080p]](https://uptobox.com/yjfamwvp7dcz)
- [[Refrain] Toaru Majutsu no Index III - 16 VOSTFR [720p]](https://uptobox.com/f4haurhplt53)
- [[Refrain] Toaru Majutsu no Index III - 16 VOSTFR [1080p]](https://uptobox.com/74l97bx0gbca)
- [[Refrain] Toaru Majutsu no Index III - 17 VOSTFR [720p]](https://uptobox.com/88f6gmmi2ema)
- [[Refrain] Toaru Majutsu no Index III - 17 VOSTFR [1080p]](https://uptobox.com/g4gqfde4fyxo)
- [[Refrain] Toaru Majutsu no Index III - 18 VOSTFR [720p]](https://uptobox.com/t6er2dtw56xj)
- [[Refrain] Toaru Majutsu no Index III - 18 VOSTFR [1080p]](https://uptobox.com/lhf7vcfyj3ap)
- [[Refrain] Toaru Majutsu no Index III - 19 VOSTFR [720p]](https://uptobox.com/779sug6fu3du)
- [[Refrain] Toaru Majutsu no Index III - 19 VOSTFR [1080p]](https://uptobox.com/9qglhu3svxp0)
- [[Refrain] Toaru Majutsu no Index III - 20 VOSTFR [720p]](https://uptobox.com/dhgtovss9flx)
- [[Refrain] Toaru Majutsu no Index III - 20 VOSTFR [1080p]](https://uptobox.com/7aia010ilk8z)
- [[Refrain] Toaru Majutsu no Index III - 21 VOSTFR [720p]](https://uptobox.com/k07khosylfkr)
- [[Refrain] Toaru Majutsu no Index III - 21 VOSTFR [1080p]](https://uptobox.com/5pp1mngzrtdi)
- [[Refrain] Toaru Majutsu no Index III - 22 VOSTFR [720p]](https://uptobox.com/qz0ljpjojglu)
- [[Refrain] Toaru Majutsu no Index III - 22 VOSTFR [1080p]](https://uptobox.com/lk8qc57nvf11)
- [[Refrain] Toaru Majutsu no Index III - 23 VOSTFR [720p]](https://uptobox.com/s5uxjjw41a8p)
- [[Refrain] Toaru Majutsu no Index III - 23 VOSTFR [1080p]](https://uptobox.com/5gwyyezs68x7)
- [[Refrain] Toaru Majutsu no Index III - 24 VOSTFR [720p]](https://uptobox.com/8nttpnk04jw5)
- [[Refrain] Toaru Majutsu no Index III - 24 VOSTFR [1080p]](https://uptobox.com/d2y9p4z17798)
- [[Refrain] Toaru Majutsu no Index III - 25 VOSTFR [720p]](https://uptobox.com/7idoqlyy3n6x)
- [[Refrain] Toaru Majutsu no Index III - 25 VOSTFR [1080p]](https://uptobox.com/09n1vnv4xcau)
- [[Refrain] Toaru Majutsu no Index III - 26 VOSTFR [720p]](https://uptobox.com/hawr6yvqn2je)
- [[Refrain] Toaru Majutsu no Index III - 26 VOSTFR [1080p]](https://uptobox.com/qs4jseyr4bv1)
