# Zombieland Saga

![Zombieland Saga](https://cdn.myanimelist.net/images/anime/1094/95453l.jpg)

* Japanese:  ゾンビランドサガ

## Information

 - Type: TV
 - Episodes: 12
 - Status: Finished Airing
 - Aired: Oct 4, 2018 to Dec 20, 2018
 - Premiered: Fall 2018
 - Broadcast: Thursdays at 23:30 (JST)
 - Producers: Avex Pictures, Cygames, dugout 
 - Licensors: Funimation 
 - Studios: MAPPA 
 - Source: Original
 - Genres: Music, Comedy, Supernatural
 - Duration: 23 min. per ep.
 - Rating: R - 17+ (violence & profanity)


## Links
- [[Refrain] Zombieland Saga - 01 VOSTFR [720p]](https://uptobox.com/ctoo9966afj9)
- [[Refrain] Zombieland Saga - 01 VOSTFR [1080p]](https://uptobox.com/mll0g2tcr4ov)
- [[Refrain] Zombieland Saga - 02 VOSTFR [720p]](https://uptobox.com/q04h25ntgmvy)
- [[Refrain] Zombieland Saga - 02 VOSTFR [1080p]](https://uptobox.com/4ckq420jghsk)
- [[Refrain] Zombieland Saga - 03 VOSTFR [720p]](https://uptobox.com/mtzxudk800ov)
- [[Refrain] Zombieland Saga - 03 VOSTFR [1080p]](https://uptobox.com/0f2txteq2p4h)
- [[Refrain] Zombieland Saga - 04 VOSTFR [720p]](https://uptobox.com/cmbufy2beuwb)
- [[Refrain] Zombieland Saga - 04 VOSTFR [1080p]](https://uptobox.com/6qu85yjmuy5g)
- [[Refrain] Zombieland Saga - 05 VOSTFR [720p]](https://uptobox.com/rxzau7oe72vd)
- [[Refrain] Zombieland Saga - 05 VOSTFR [1080p]](https://uptobox.com/uyk23uok8pla)
- [[Refrain] Zombieland Saga - 06 VOSTFR [720p]](https://uptobox.com/iz9qu8clk8yf)
- [[Refrain] Zombieland Saga - 06 VOSTFR [1080p]](https://uptobox.com/0k1nwd4uo23u)
- [[Refrain] Zombieland Saga - 07 VOSTFR [720p]](https://uptobox.com/j2fpvfmlv69c)
- [[Refrain] Zombieland Saga - 07 VOSTFR [1080p]](https://uptobox.com/iwb7qo6jx8e9)
- [[Refrain] Zombieland Saga - 08 VOSTFR [720p]](https://uptobox.com/c4b5n8kuduh0)
- [[Refrain] Zombieland Saga - 08 VOSTFR [1080p]](https://uptobox.com/en67qxkdunfg)
- [[Refrain] Zombieland Saga - 09 VOSTFR [720p]](https://uptobox.com/8jqm5hzjvz23)
- [[Refrain] Zombieland Saga - 09 VOSTFR [1080p]](https://uptobox.com/fwehj09feavj)
- [[Refrain] Zombieland Saga - 10 VOSTFR [720p]](https://uptobox.com/21hwsgbbhzi8)
- [[Refrain] Zombieland Saga - 10 VOSTFR [1080p]](https://uptobox.com/rg7urc1bxlxy)
- [[Refrain] Zombieland Saga - 11 VOSTFR [720p]](https://uptobox.com/2fwfxrjre4qh)
- [[Refrain] Zombieland Saga - 11 VOSTFR [1080p]](https://uptobox.com/6uymmijo16jc)
- [[Refrain] Zombieland Saga - 12 VOSTFR [720p]](https://uptobox.com/et74s72copc3)
- [[Refrain] Zombieland Saga - 12 VOSTFR [1080p]](https://uptobox.com/zuoxny9oarnv)
