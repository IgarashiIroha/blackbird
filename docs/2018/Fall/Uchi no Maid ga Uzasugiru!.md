# Uchi no Maid ga Uzasugiru!

![Uchi no Maid ga Uzasugiru!](https://cdn.myanimelist.net/images/anime/1786/95330l.jpg)

* Japanese:  うちのメイドがウザすぎる！

## Information

 - Type: TV
 - Episodes: 12
 - Status: Finished Airing
 - Aired: Oct 5, 2018 to Dec 21, 2018
 - Premiered: Fall 2018
 - Broadcast: Fridays at 23:00 (JST)
 - Producers: AT-X, Sony Music Communications, Futabasha, Kadokawa Media House, NTT Plala, Kadokawa
 - Licensors: None found, add some
 - Studios: Doga Kobo
 - Source: Manga
 - Genres: Slice of Life, Comedy, Shoujo Ai
 - Duration: 23 min. per ep.
 - Rating: PG-13 - Teens 13 or older


## Links
- [[Refrain] Uchi no Maid ga Uzasugiru! - 01 VOSTFR [720p]](https://uptobox.com/9c4zqadejk3q)
- [[Refrain] Uchi no Maid ga Uzasugiru! - 01 VOSTFR [1080p]](https://uptobox.com/1nyp7xq2655u)
- [[Refrain] Uchi no Maid ga Uzasugiru! - 01 VOSTFR {ADN} [720p]](https://uptobox.com/uaoz8wvq7p7q)
- [[Refrain] Uchi no Maid ga Uzasugiru! - 01 VOSTFR {ADN} [1080p]](https://uptobox.com/2ziisgofvhrl)
- [[Refrain] Uchi no Maid ga Uzasugiru! - 01 VOSTFR {CR} [720p]](https://uptobox.com/63my7heqw48j)
- [[Refrain] Uchi no Maid ga Uzasugiru! - 01 VOSTFR {CR} [1080p]](https://uptobox.com/eq6t66xblw2f)
- [[Refrain] Uchi no Maid ga Uzasugiru! - 02 VOSTFR [720p]](https://uptobox.com/f0438d0p1vvu)
- [[Refrain] Uchi no Maid ga Uzasugiru! - 02 VOSTFR [1080p]](https://uptobox.com/ciafqpg391dh)
- [[Refrain] Uchi no Maid ga Uzasugiru! - 02 VOSTFR {ADN} [720p]](https://uptobox.com/t2jpr5me8cy6)
- [[Refrain] Uchi no Maid ga Uzasugiru! - 02 VOSTFR {ADN} [1080p]](https://uptobox.com/hs7gydrh7n8e)
- [[Refrain] Uchi no Maid ga Uzasugiru! - 03 VOSTFR [720p]](https://uptobox.com/qna809ygqa4p)
- [[Refrain] Uchi no Maid ga Uzasugiru! - 03 VOSTFR [1080p]](https://uptobox.com/lcf3xpp6js0o)
- [[Refrain] Uchi no Maid ga Uzasugiru! - 03 VOSTFR {ADN} [720p]](https://uptobox.com/svekcxqyua8k)
- [[Refrain] Uchi no Maid ga Uzasugiru! - 03 VOSTFR {ADN} [1080p]](https://uptobox.com/gh7ix026zfcr)
- [[Refrain] Uchi no Maid ga Uzasugiru! - 04 VOSTFR [720p]](https://uptobox.com/arex2htfyz18)
- [[Refrain] Uchi no Maid ga Uzasugiru! - 04 VOSTFR [1080p]](https://uptobox.com/deoq1fj4j7la)
- [[Refrain] Uchi no Maid ga Uzasugiru! - 04 VOSTFR {ADN} [720p]](https://uptobox.com/bqbatzmvdh2h)
- [[Refrain] Uchi no Maid ga Uzasugiru! - 04 VOSTFR {ADN} [1080p]](https://uptobox.com/5pj5uv2qih26)
- [[Refrain] Uchi no Maid ga Uzasugiru! - 05 VOSTFR [720p]](https://uptobox.com/x6wm2aqej8w4)
- [[Refrain] Uchi no Maid ga Uzasugiru! - 05 VOSTFR [1080p]](https://uptobox.com/ks2qewkl8btn)
- [[Refrain] Uchi no Maid ga Uzasugiru! - 05 VOSTFR {ADN} [720p]](https://uptobox.com/aj6txdmwh10v)
- [[Refrain] Uchi no Maid ga Uzasugiru! - 05 VOSTFR {ADN} [1080p]](https://uptobox.com/361kbpj5j3r1)
- [[Refrain] Uchi no Maid ga Uzasugiru! - 06 VOSTFR [720p]](https://uptobox.com/nzw8yc16vy64)
- [[Refrain] Uchi no Maid ga Uzasugiru! - 06 VOSTFR [1080p]](https://uptobox.com/4z7nyuiuej1v)
- [[Refrain] Uchi no Maid ga Uzasugiru! - 06 VOSTFR {ADN} [720p]](https://uptobox.com/yalnzx8lw2h9)
- [[Refrain] Uchi no Maid ga Uzasugiru! - 06 VOSTFR {ADN} [1080p]](https://uptobox.com/m56v5zpqawlo)
- [[Refrain] Uchi no Maid ga Uzasugiru! - 07 VOSTFR [720p]](https://uptobox.com/2t5ypet9bfmm)
- [[Refrain] Uchi no Maid ga Uzasugiru! - 07 VOSTFR [1080p]](https://uptobox.com/vc5jvn9r6p9m)
- [[Refrain] Uchi no Maid ga Uzasugiru! - 07 VOSTFR {ADN} [720p]](https://uptobox.com/82g97fgemxou)
- [[Refrain] Uchi no Maid ga Uzasugiru! - 07 VOSTFR {ADN} [1080p]](https://uptobox.com/h019djj2og2x)
- [[Refrain] Uchi no Maid ga Uzasugiru! - 08 VOSTFR [720p]](https://uptobox.com/v7hnoz5z0z96)
- [[Refrain] Uchi no Maid ga Uzasugiru! - 08 VOSTFR [1080p]](https://uptobox.com/qtqt6fb68qe8)
- [[Refrain] Uchi no Maid ga Uzasugiru! - 08 VOSTFR {ADN} [720p]](https://uptobox.com/v5dctxp680iz)
- [[Refrain] Uchi no Maid ga Uzasugiru! - 08 VOSTFR {ADN} [1080p]](https://uptobox.com/533axtuag3b8)
- [[Refrain] Uchi no Maid ga Uzasugiru! - 09 VOSTFR [720p]](https://uptobox.com/vvcjdv8ym65v)
- [[Refrain] Uchi no Maid ga Uzasugiru! - 09 VOSTFR [1080p]](https://uptobox.com/7mukm5e2dkda)
- [[Refrain] Uchi no Maid ga Uzasugiru! - 09 VOSTFR {ADN} [720p]](https://uptobox.com/1w9y6i2rccjt)
- [[Refrain] Uchi no Maid ga Uzasugiru! - 09 VOSTFR {ADN} [1080p]](https://uptobox.com/37epmqoeiynb)
- [[Refrain] Uchi no Maid ga Uzasugiru! - 10 VOSTFR [720p]](https://uptobox.com/wbihqa1s9xtg)
- [[Refrain] Uchi no Maid ga Uzasugiru! - 10 VOSTFR [1080p]](https://uptobox.com/3fxlqaztd6hq)
- [[Refrain] Uchi no Maid ga Uzasugiru! - 10 VOSTFR {ADN} [720p]](https://uptobox.com/i72n3pbc34kl)
- [[Refrain] Uchi no Maid ga Uzasugiru! - 10 VOSTFR {ADN} [1080p]](https://uptobox.com/vl05c59bonfr)
- [[Refrain] Uchi no Maid ga Uzasugiru! - 11 VOSTFR [720p]](https://uptobox.com/7z17mg8xkbzr)
- [[Refrain] Uchi no Maid ga Uzasugiru! - 11 VOSTFR [1080p]](https://uptobox.com/zwa2uzhgamo5)
- [[Refrain] Uchi no Maid ga Uzasugiru! - 11 VOSTFR {ADN} [720p]](https://uptobox.com/kmai5foclnez)
- [[Refrain] Uchi no Maid ga Uzasugiru! - 11 VOSTFR {ADN} [1080p]](https://uptobox.com/edmp12k3w3wl)
- [[Refrain] Uchi no Maid ga Uzasugiru! - 12 VOSTFR [720p]](https://uptobox.com/o567peysujvl)
- [[Refrain] Uchi no Maid ga Uzasugiru! - 12 VOSTFR [1080p]](https://uptobox.com/xqemx2fw0hzw)
- [[Refrain] Uchi no Maid ga Uzasugiru! - 12 VOSTFR {ADN} [720p]](https://uptobox.com/sh50oyq4uvz9)
- [[Refrain] Uchi no Maid ga Uzasugiru! - 12 VOSTFR {ADN} [1080p]](https://uptobox.com/3wc1cr99yolj)
