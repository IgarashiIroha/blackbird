# Hero Mask

![Hero Mask](https://cdn.myanimelist.net/images/anime/1934/96007l.jpg)

* Japanese:  ヒーローマスク

## Information

 - Type: ONA
 - Episodes: 15
 - Status: Finished Airing
 - Aired: Dec 3, 2018
 - Producers: None found, add some
 - Licensors: None found, add some
 - Studios: Studio Pierrot
 - Source: Original
 - Genres: Action, Mystery, Police, Sci-Fi
 - Duration: 24 min. per ep.
 - Rating: None


## Links

- [[Refrain] Hero Mask - 01 VF VOSTFR [720p]](https://uptobox.com/rud7sja8hbhr)
- [[Refrain] Hero Mask - 01 VF VOSTFR [1080p]](https://uptobox.com/ctips2alpyfi)
- [[Refrain] Hero Mask - 02 VF VOSTFR [720p]](https://uptobox.com/9ieb4k8pi4ly)
- [[Refrain] Hero Mask - 02 VF VOSTFR [1080p]](https://uptobox.com/hqso2rcmqya8)
- [[Refrain] Hero Mask - 03 VF VOSTFR [720p]](https://uptobox.com/93r7mcdisa4d)
- [[Refrain] Hero Mask - 03 VF VOSTFR [1080p]](https://uptobox.com/dym5wclh7gvx)
- [[Refrain] Hero Mask - 04 VF VOSTFR [720p]](https://uptobox.com/utwjcya0cmpr)
- [[Refrain] Hero Mask - 04 VF VOSTFR [1080p]](https://uptobox.com/smdi0n4pthi1)
- [[Refrain] Hero Mask - 05 VF VOSTFR [720p]](https://uptobox.com/n33qe3zthkbg)
- [[Refrain] Hero Mask - 05 VF VOSTFR [1080p]](https://uptobox.com/fyapsif3gl8v)
- [[Refrain] Hero Mask - 06 VF VOSTFR [720p]](https://uptobox.com/wkj7pbaxptjn)
- [[Refrain] Hero Mask - 06 VF VOSTFR [1080p]](https://uptobox.com/m1q7utbenray)
- [[Refrain] Hero Mask - 07 VF VOSTFR [720p]](https://uptobox.com/yhl1tiuf2rdg)
- [[Refrain] Hero Mask - 07 VF VOSTFR [1080p]](https://uptobox.com/1gvl96fp59we)
- [[Refrain] Hero Mask - 08 VF VOSTFR [720p]](https://uptobox.com/rjw5dfvkpqq9)
- [[Refrain] Hero Mask - 08 VF VOSTFR [1080p]](https://uptobox.com/avsq0fmjw6ah)
- [[Refrain] Hero Mask - 09 VF VOSTFR [720p]](https://uptobox.com/3i8udhrsacbn)
- [[Refrain] Hero Mask - 09 VF VOSTFR [1080p]](https://uptobox.com/egcijxiesedd)
- [[Refrain] Hero Mask - 10 VF VOSTFR [720p]](https://uptobox.com/xrrwswtsvbgd)
- [[Refrain] Hero Mask - 10 VF VOSTFR [1080p]](https://uptobox.com/ds107mhgk1z7)
- [[Refrain] Hero Mask - 11 VF VOSTFR [720p]](https://uptobox.com/3op0egkcyk5h)
- [[Refrain] Hero Mask - 11 VF VOSTFR [1080p]](https://uptobox.com/ok94pls3mv02)
- [[Refrain] Hero Mask - 12 VF VOSTFR [720p]](https://uptobox.com/5otj7jwn4npp)
- [[Refrain] Hero Mask - 12 VF VOSTFR [1080p]](https://uptobox.com/ev5dq293lb6r)
- [[Refrain] Hero Mask - 13 VF VOSTFR [720p]](https://uptobox.com/qocgk5c3138u)
- [[Refrain] Hero Mask - 13 VF VOSTFR [1080p]](https://uptobox.com/a12indg29num)
- [[Refrain] Hero Mask - 14 VF VOSTFR [720p]](https://uptobox.com/4hyziady39jr)
- [[Refrain] Hero Mask - 14 VF VOSTFR [1080p]](https://uptobox.com/rg5f76o14aqv)
- [[Refrain] Hero Mask - 15 VF VOSTFR [720p]](https://uptobox.com/ia362yquf6yn)
- [[Refrain] Hero Mask - 15 VF VOSTFR [1080p]](https://uptobox.com/lmjt82ea5gxq)
