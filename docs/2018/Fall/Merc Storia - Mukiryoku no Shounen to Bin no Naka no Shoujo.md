# Merc Storia: Mukiryoku no Shounen to Bin no Naka no Shoujo

![Merc Storia: Mukiryoku no Shounen to Bin no Naka no Shoujo](https://cdn.myanimelist.net/images/anime/1394/95704l.jpg)

* Japanese:  メルクストーリア -無気力少年と瓶の中の少女-

## Information

 - Type: TV
 - Episodes: 12
 - Status: Finished Airing
 - Aired: Oct 11, 2018 to Dec 27, 2018
 - Premiered: Fall 2018
 - Broadcast: Thursdays at 22:30 (JST)
 - Producers: Happy Elements 
 - Licensors: None found, add some 
 - Studios: Encourage Films 
 - Source: Game
 - Genres: Action, Adventure, Magic, Fantasy
 - Duration: 23 min. per ep.
 - Rating: PG-13 - Teens 13 or older


## Links
- [[Refrain] Merc Storia - Mukiryoku Shounen to Bin no Naka no Shoujo - 01 VOSTFR {ADN} [720p]](https://uptobox.com/qw97eyl9j77u)
- [[Refrain] Merc Storia - Mukiryoku Shounen to Bin no Naka no Shoujo - 01 VOSTFR {ADN} [1080p]](https://uptobox.com/cb6g8pwkd6jj)
- [[Refrain] Merc Storia - Mukiryoku Shounen to Bin no Naka no Shoujo - 02 VOSTFR {ADN} [720p]](https://uptobox.com/x9fuyf1g6qb1)
- [[Refrain] Merc Storia - Mukiryoku Shounen to Bin no Naka no Shoujo - 02 VOSTFR {ADN} [1080p]](https://uptobox.com/q6w4dvn5b559)
- [[Refrain] Merc Storia - Mukiryoku Shounen to Bin no Naka no Shoujo - 03 VOSTFR {ADN} [720p]](https://uptobox.com/qpawnk3cyzm3)
- [[Refrain] Merc Storia - Mukiryoku Shounen to Bin no Naka no Shoujo - 03 VOSTFR {ADN} [1080p]](https://uptobox.com/21ron4og08pi)
- [[Refrain] Merc Storia - Mukiryoku Shounen to Bin no Naka no Shoujo - 04 VOSTFR {ADN} [720p]](https://uptobox.com/0rpf6kzfz98w)
- [[Refrain] Merc Storia - Mukiryoku Shounen to Bin no Naka no Shoujo - 04 VOSTFR {ADN} [1080p]](https://uptobox.com/4invvv877mxr)
- [[Refrain] Merc Storia - Mukiryoku Shounen to Bin no Naka no Shoujo - 05 VOSTFR {ADN} [720p]](https://uptobox.com/w4yirzmude9f)
- [[Refrain] Merc Storia - Mukiryoku Shounen to Bin no Naka no Shoujo - 05 VOSTFR {ADN} [1080p]](https://uptobox.com/b4hiti5xup36)
- [[Refrain] Merc Storia - Mukiryoku Shounen to Bin no Naka no Shoujo - 06 VOSTFR {ADN} [720p]](https://uptobox.com/s7d2g6zj8xdv)
- [[Refrain] Merc Storia - Mukiryoku Shounen to Bin no Naka no Shoujo - 06 VOSTFR {ADN} [1080p]](https://uptobox.com/a9yjk00bkhlk)
- [[Refrain] Merc Storia - Mukiryoku Shounen to Bin no Naka no Shoujo - 07 VOSTFR {ADN} [720p]](https://uptobox.com/f5rk3bfim2kp)
- [[Refrain] Merc Storia - Mukiryoku Shounen to Bin no Naka no Shoujo - 07 VOSTFR {ADN} [1080p]](https://uptobox.com/flnzgoxmdmpw)
- [[Refrain] Merc Storia - Mukiryoku Shounen to Bin no Naka no Shoujo - 08 VOSTFR {ADN} [720p]](https://uptobox.com/f5qxq3ho6j3j)
- [[Refrain] Merc Storia - Mukiryoku Shounen to Bin no Naka no Shoujo - 08 VOSTFR {ADN} [1080p]](https://uptobox.com/kj63q7dmwsoh)
- [[Refrain] Merc Storia - Mukiryoku Shounen to Bin no Naka no Shoujo - 09 VOSTFR {ADN} [720p]](https://uptobox.com/le9ag631x2pg)
- [[Refrain] Merc Storia - Mukiryoku Shounen to Bin no Naka no Shoujo - 09 VOSTFR {ADN} [1080p]](https://uptobox.com/iu6f3wz34wl8)
- [[Refrain] Merc Storia - Mukiryoku Shounen to Bin no Naka no Shoujo - 10 VOSTFR {ADN} [720p]](https://uptobox.com/la63gcsxc3mw)
- [[Refrain] Merc Storia - Mukiryoku Shounen to Bin no Naka no Shoujo - 10 VOSTFR {ADN} [1080p]](https://uptobox.com/kcresyeqabta)
- [[Refrain] Merc Storia - Mukiryoku Shounen to Bin no Naka no Shoujo - 11 VOSTFR {ADN} [720p]](https://uptobox.com/faafrls7fbc6)
- [[Refrain] Merc Storia - Mukiryoku Shounen to Bin no Naka no Shoujo - 11 VOSTFR {ADN} [1080p]](https://uptobox.com/wzk20nckjwlc)
- [[Refrain] Merc Storia - Mukiryoku Shounen to Bin no Naka no Shoujo - 12 VOSTFR {ADN} [720p]](https://uptobox.com/35jluxyl1i4x)
- [[Refrain] Merc Storia - Mukiryoku Shounen to Bin no Naka no Shoujo - 12 VOSTFR {ADN} [1080p]](https://uptobox.com/a7268n7exmxn)
