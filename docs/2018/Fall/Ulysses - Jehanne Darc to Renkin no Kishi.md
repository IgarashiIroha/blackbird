# Ulysses: Jehanne Darc to Renkin no Kishi

![Ulysses: Jehanne Darc to Renkin no Kishi](https://cdn.myanimelist.net/images/anime/1143/96156l.jpg)

* Japanese:  ユリシーズ ジャンヌ・ダルクと錬金の騎士

## Information

 - Type: TV
 - Episodes: 12
 - Status: Finished Airing
 - Aired: Oct 7, 2018 to Dec 30, 2018
 - Premiered: Fall 2018
 - Broadcast: Sundays at 22:00 (JST)
 - Producers: Lantis, Studio Mausu, Shueisha, Yomiuri TV Enterprise, Emon, DMM pictures, Crunchyroll SC Anime Fund, Bandai Namco Arts 
 - Licensors: Funimation 
 - Studios: AXsiZ 
 - Source: Light novel
 - Genres: Action, Fantasy
 - Duration: 23 min. per ep.
 - Rating: PG-13 - Teens 13 or older


## Links
- [[Refrain] Ulysses - Jeanne d'Arc to Renkin no Kishi - 01 VOSTFR [720p]](https://uptobox.com/buj3sq880ngl)
- [[Refrain] Ulysses - Jeanne d'Arc to Renkin no Kishi - 01 VOSTFR [1080p]](https://uptobox.com/lqohcgr693wk)
- [[Refrain] Ulysses - Jeanne d'Arc to Renkin no Kishi - 02 VOSTFR [720p]](https://uptobox.com/cdvc9n0cnuhh)
- [[Refrain] Ulysses - Jeanne d'Arc to Renkin no Kishi - 02 VOSTFR [1080p]](https://uptobox.com/m7ikm5x4wsu4)
- [[Refrain] Ulysses - Jeanne d'Arc to Renkin no Kishi - 03 VOSTFR [720p]](https://uptobox.com/p6fxrdw6y58l)
- [[Refrain] Ulysses - Jeanne d'Arc to Renkin no Kishi - 03 VOSTFR [1080p]](https://uptobox.com/p61zopgr6s6x)
- [[Refrain] Ulysses - Jeanne d'Arc to Renkin no Kishi - 04 VOSTFR [720p]](https://uptobox.com/d3f5mr9nbqef)
- [[Refrain] Ulysses - Jeanne d'Arc to Renkin no Kishi - 04 VOSTFR [1080p]](https://uptobox.com/2dvq50s24utq)
- [[Refrain] Ulysses - Jeanne d'Arc to Renkin no Kishi - 05 VOSTFR [720p]](https://uptobox.com/6hywfmri90pn)
- [[Refrain] Ulysses - Jeanne d'Arc to Renkin no Kishi - 05 VOSTFR [1080p]](https://uptobox.com/0w7cld5ap1cp)
- [[Refrain] Ulysses - Jeanne d'Arc to Renkin no Kishi - 06 VOSTFR [720p]](https://uptobox.com/dlj0swx2akyo)
- [[Refrain] Ulysses - Jeanne d'Arc to Renkin no Kishi - 06 VOSTFR [1080p]](https://uptobox.com/gbsyuwmyn3zb)
- [[Refrain] Ulysses - Jeanne d'Arc to Renkin no Kishi - 07 VOSTFR [720p]](https://uptobox.com/p1hy9m4wn9je)
- [[Refrain] Ulysses - Jeanne d'Arc to Renkin no Kishi - 07 VOSTFR [1080p]](https://uptobox.com/35igs9543fod)
- [[Refrain] Ulysses - Jeanne d'Arc to Renkin no Kishi - 08 VOSTFR [720p]](https://uptobox.com/mf8a7jj6uq3a)
- [[Refrain] Ulysses - Jeanne d'Arc to Renkin no Kishi - 08 VOSTFR [1080p]](https://uptobox.com/qw0lepwwxh8s)
- [[Refrain] Ulysses - Jeanne d'Arc to Renkin no Kishi - 09 VOSTFR [720p]](https://uptobox.com/3rqbu0gslese)
- [[Refrain] Ulysses - Jeanne d'Arc to Renkin no Kishi - 09 VOSTFR [1080p]](https://uptobox.com/pqixlbh1qdkj)
- [[Refrain] Ulysses - Jeanne d'Arc to Renkin no Kishi - 10 VOSTFR [720p]](https://uptobox.com/u1acjouiep4y)
- [[Refrain] Ulysses - Jeanne d'Arc to Renkin no Kishi - 10 VOSTFR [1080p]](https://uptobox.com/5l22y7tpw3v4)
- [[Refrain] Ulysses - Jeanne d'Arc to Renkin no Kishi - 11 VOSTFR [720p]](https://uptobox.com/fu9zoto2hzrn)
- [[Refrain] Ulysses - Jeanne d'Arc to Renkin no Kishi - 11 VOSTFR [1080p]](https://uptobox.com/kmh4uo826t46)
- [[Refrain] Ulysses - Jeanne d'Arc to Renkin no Kishi - 12 VOSTFR [720p]](https://uptobox.com/ii2nx1durdlm)
- [[Refrain] Ulysses - Jeanne d'Arc to Renkin no Kishi - 12 VOSTFR [1080p]](https://uptobox.com/tkxhue0fswmk)
