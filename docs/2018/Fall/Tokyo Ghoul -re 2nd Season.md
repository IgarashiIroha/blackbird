# Tokyo Ghoul:re 2nd Season

![Tokyo Ghoul:re 2nd Season](https://cdn.myanimelist.net/images/anime/1271/95623l.jpg)

* Japanese:  京喰種トーキョーグール：re 第2期

## Information

 - Type: TV
 - Episodes: 12
 - Status: Finished Airing
 - Aired: Oct 9, 2018 to Dec 25, 2018
 - Premiered: Fall 2018
 - Broadcast: Tuesdays at 23:00 (JST)
 - Producers: Marvelous, TC Entertainment, Shueisha 
 - Licensors: Funimation 
 - Studios: Studio Pierrot, Pierrot Plus 
 - Source: Manga
 - Genres: Action, Drama, Horror, Mystery, Psychological, Seinen, Supernatural
 - Duration: 24 min. per ep.
 - Rating: R - 17+ (violence & profanity)


## Links
- [[Refrain] Tokyo Ghoul - re 2nd Season - 01 VOSTFR [720p]](https://uptobox.com/265yavsw0nge)
- [[Refrain] Tokyo Ghoul - re 2nd Season - 01 VOSTFR [1080p]](https://uptobox.com/gos9if7bs9e6)
- [[Refrain] Tokyo Ghoul - re 2nd Season - 02 VOSTFR [720p]](https://uptobox.com/xh2kqh5qxltq)
- [[Refrain] Tokyo Ghoul - re 2nd Season - 02 VOSTFR [1080p]](https://uptobox.com/kp2q64pgfg6w)
- [[Refrain] Tokyo Ghoul - re 2nd Season - 03 VOSTFR [720p]](https://uptobox.com/pc88r34jph7u)
- [[Refrain] Tokyo Ghoul - re 2nd Season - 03 VOSTFR [1080p]](https://uptobox.com/hux93mzcgqfm)
- [[Refrain] Tokyo Ghoul - re 2nd Season - 04 VOSTFR [720p]](https://uptobox.com/o1f6kfbs2j5k)
- [[Refrain] Tokyo Ghoul - re 2nd Season - 04 VOSTFR [1080p]](https://uptobox.com/n6kk0kskmstu)
- [[Refrain] Tokyo Ghoul - re 2nd Season - 05 VOSTFR [720p]](https://uptobox.com/6ktw0hj7hn5k)
- [[Refrain] Tokyo Ghoul - re 2nd Season - 05 VOSTFR [1080p]](https://uptobox.com/cfuwjpmf4wce)
- [[Refrain] Tokyo Ghoul - re 2nd Season - 06 VOSTFR [720p]](https://uptobox.com/sjysqgk7i97s)
- [[Refrain] Tokyo Ghoul - re 2nd Season - 06 VOSTFR [1080p]](https://uptobox.com/e2cxelwcqycn)
- [[Refrain] Tokyo Ghoul - re 2nd Season - 07 VOSTFR [720p]](https://uptobox.com/xti2dsl6rulj)
- [[Refrain] Tokyo Ghoul - re 2nd Season - 07 VOSTFR [1080p]](https://uptobox.com/nwbkipiq8da8)
- [[Refrain] Tokyo Ghoul - re 2nd Season - 08 VOSTFR [720p]](https://uptobox.com/fz1mhf6z4q7d)
- [[Refrain] Tokyo Ghoul - re 2nd Season - 08 VOSTFR [1080p]](https://uptobox.com/3af5sz0u8ufo)
- [[Refrain] Tokyo Ghoul - re 2nd Season - 09 VOSTFR [720p]](https://uptobox.com/0fbxnv3xr221)
- [[Refrain] Tokyo Ghoul - re 2nd Season - 09 VOSTFR [1080p]](https://uptobox.com/f7a39fel6ml8)
- [[Refrain] Tokyo Ghoul - re 2nd Season - 10 VOSTFR [720p]](https://uptobox.com/wyhtes344bqp)
- [[Refrain] Tokyo Ghoul - re 2nd Season - 10 VOSTFR [1080p]](https://uptobox.com/ifcac48t6893)
- [[Refrain] Tokyo Ghoul - re 2nd Season - 11 VOSTFR [720p]](https://uptobox.com/yo5kyf5cj3mm)
- [[Refrain] Tokyo Ghoul - re 2nd Season - 11 VOSTFR [1080p]](https://uptobox.com/x6velo21w6i7)
- [[Refrain] Tokyo Ghoul - re 2nd Season - 12 VOSTFR [720p]](https://uptobox.com/qlzp42fd7gnx)
- [[Refrain] Tokyo Ghoul - re 2nd Season - 12 VOSTFR [1080p]](https://uptobox.com/1d5cgm8qp8r2)
