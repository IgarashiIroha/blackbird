# RErideD: Tokigoe no Derrida

![RErideD: Tokigoe no Derrida](https://cdn.myanimelist.net/images/anime/1481/95784l.jpg)

* Japanese:  RErideD－刻越えのデリダ－

## Information

 - Type: TV
 - Episodes: 12
 - Status: Finished Airing
 - Aired: Sep 22, 2018 to Nov 21, 2018
 - Premiered: Fall 2018
 - Broadcast: Thursdays at 01:05 (JST)
 - Producers: Half H.P Studio, Kadokawa 
 - Licensors: Funimation 
 - Studios: GEEKTOYS 
 - Source: Original
 - Genres: Action, Sci-Fi, Drama
 - Duration: 23 min. per ep.
 - Rating: PG-13 - Teens 13 or older


## Links
- [[Refrain] RErideD - Tokigoe no Derrida - 01 VOSTFR [720p]](https://uptobox.com/1m9ikd0nyogk)
- [[Refrain] RErideD - Tokigoe no Derrida - 01 VOSTFR [1080p]](https://uptobox.com/ozm5n4tnojh9)
- [[Refrain] RErideD - Tokigoe no Derrida - 02 VOSTFR [720p]](https://uptobox.com/cz8ldwau6dv4)
- [[Refrain] RErideD - Tokigoe no Derrida - 02 VOSTFR [1080p]](https://uptobox.com/yl4kwo4znzqn)
- [[Refrain] RErideD - Tokigoe no Derrida - 03 VOSTFR [720p]](https://uptobox.com/qlac1agia8df)
- [[Refrain] RErideD - Tokigoe no Derrida - 03 VOSTFR [1080p]](https://uptobox.com/nz1n5olkfohf)
- [[Refrain] RErideD - Tokigoe no Derrida - 04 VOSTFR [720p]](https://uptobox.com/4af9sobq8vcu)
- [[Refrain] RErideD - Tokigoe no Derrida - 04 VOSTFR [1080p]](https://uptobox.com/p6cu64krwusu)
- [[Refrain] RErideD - Tokigoe no Derrida - 05 VOSTFR [720p]](https://uptobox.com/ylt78rewcvw7)
- [[Refrain] RErideD - Tokigoe no Derrida - 05 VOSTFR [1080p]](https://uptobox.com/jwrupjmd48y9)
- [[Refrain] RErideD - Tokigoe no Derrida - 06 VOSTFR [720p]](https://uptobox.com/9125twn87cgy)
- [[Refrain] RErideD - Tokigoe no Derrida - 06 VOSTFR [1080p]](https://uptobox.com/9ukmqib5uztv)
- [[Refrain] RErideD - Tokigoe no Derrida - 07 VOSTFR [720p]](https://uptobox.com/529haidc0ksk)
- [[Refrain] RErideD - Tokigoe no Derrida - 07 VOSTFR [1080p]](https://uptobox.com/rhq7if869yji)
- [[Refrain] RErideD - Tokigoe no Derrida - 08 VOSTFR [720p]](https://uptobox.com/hqwc84q2w0a7)
- [[Refrain] RErideD - Tokigoe no Derrida - 08 VOSTFR [1080p]](https://uptobox.com/v7nd56irarny)
- [[Refrain] RErideD - Tokigoe no Derrida - 09 VOSTFR [720p]](https://uptobox.com/f9zce533th1z)
- [[Refrain] RErideD - Tokigoe no Derrida - 09 VOSTFR [1080p]](https://uptobox.com/0suwew6aoqbu)
- [[Refrain] RErideD - Tokigoe no Derrida - 10 VOSTFR [720p]](https://uptobox.com/5x2i86ciw8sc)
- [[Refrain] RErideD - Tokigoe no Derrida - 10 VOSTFR [1080p]](https://uptobox.com/4wbjm71dgyfp)
- [[Refrain] RErideD - Tokigoe no Derrida - 11 VOSTFR [720p]](https://uptobox.com/15j5snjwpjws)
- [[Refrain] RErideD - Tokigoe no Derrida - 11 VOSTFR [1080p]](https://uptobox.com/xprp9ov78qdj)
- [[Refrain] RErideD - Tokigoe no Derrida - 12 VOSTFR [720p]](https://uptobox.com/1grptvyubv91)
- [[Refrain] RErideD - Tokigoe no Derrida - 12 VOSTFR [1080p]](https://uptobox.com/wpyobae0p1gt)
