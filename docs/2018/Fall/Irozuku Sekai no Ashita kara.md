# Irozuku Sekai no Ashita kara

![Irozuku Sekai no Ashita kara](https://cdn.myanimelist.net/images/anime/1424/93855l.jpg)

* Japanese:  色づく世界の明日から

## Information

 - Type: TV
 - Episodes: 13
 - Status: Finished Airing
 - Aired: Oct 6, 2018 to Dec 29, 2018
 - Premiered: Fall 2018
 - Broadcast: Saturdays at 01:55 (JST)
 - Producers: NBCUniversal Entertainment Japan, Infinite
 - Licensors: None found, add some
 - Studios: P.A. Works
 - Source: Original
 - Genres: Drama, Magic, Romance
 - Duration: 24 min. per ep.
 - Rating: PG-13 - Teens 13 or older


## Links
- [[Refrain] Irozuku Sekai no Ashita kara - 01 VOSTFR [720p]](https://uptobox.com/xe4n2qoyqhcj)
- [[Refrain] Irozuku Sekai no Ashita kara - 01 VOSTFR [1080p]](https://uptobox.com/9qh3vg2yh7n4)
- [[Refrain] Irozuku Sekai no Ashita kara - 02 VOSTFR [720p]](https://uptobox.com/g1cu0uupivvm)
- [[Refrain] Irozuku Sekai no Ashita kara - 02 VOSTFR [1080p]](https://uptobox.com/bxigvuyr1qf3)
- [[Refrain] Irozuku Sekai no Ashita kara - 03 VOSTFR [720p]](https://uptobox.com/hl2dt4yguwm2)
- [[Refrain] Irozuku Sekai no Ashita kara - 03 VOSTFR [1080p]](https://uptobox.com/gh3modmcieo6)
- [[Refrain] Irozuku Sekai no Ashita kara - 04 VOSTFR [720p]](https://uptobox.com/jnj7dhmddect)
- [[Refrain] Irozuku Sekai no Ashita kara - 04 VOSTFR [1080p]](https://uptobox.com/j1fvxlzwzc8d)
- [[Refrain] Irozuku Sekai no Ashita kara - 05 VOSTFR [720p]](https://uptobox.com/lnkbt18ck9b7)
- [[Refrain] Irozuku Sekai no Ashita kara - 05 VOSTFR [1080p]](https://uptobox.com/gh0foccwn3et)
- [[Refrain] Irozuku Sekai no Ashita kara - 06 VOSTFR [720p]](https://uptobox.com/c3f93g0f02i3)
- [[Refrain] Irozuku Sekai no Ashita kara - 06 VOSTFR [1080p]](https://uptobox.com/t9e5fof5zgf0)
- [[Refrain] Irozuku Sekai no Ashita kara - 07 VOSTFR [720p]](https://uptobox.com/17xjskxitagr)
- [[Refrain] Irozuku Sekai no Ashita kara - 07 VOSTFR [1080p]](https://uptobox.com/alwt5a0vgx27)
- [[Refrain] Irozuku Sekai no Ashita kara - 08 VOSTFR [1080p]](https://uptobox.com/6dbqlebn0jx8)
- [[Refrain] Irozuku Sekai no Ashita kara - 09 VOSTFR [720p]](https://uptobox.com/lf5e4xlq3fc8)
- [[Refrain] Irozuku Sekai no Ashita kara - 09 VOSTFR [1080p]](https://uptobox.com/wkqk946iztu2)
- [[Refrain] Irozuku Sekai no Ashita kara - 10 VOSTFR [720p]](https://uptobox.com/905gy51ffnws)
- [[Refrain] Irozuku Sekai no Ashita kara - 10 VOSTFR [1080p]](https://uptobox.com/4owt02bu09ac)
- [[Refrain] Irozuku Sekai no Ashita kara - 11 VOSTFR [720p]](https://uptobox.com/msl1ya8c1fuo)
- [[Refrain] Irozuku Sekai no Ashita kara - 11 VOSTFR [1080p]](https://uptobox.com/ina0cvqord9e)
- [[Refrain] Irozuku Sekai no Ashita kara - 12 VOSTFR [720p]](https://uptobox.com/pa8vobpbq5m5)
- [[Refrain] Irozuku Sekai no Ashita kara - 12 VOSTFR [1080p]](https://uptobox.com/h0bpc5k5ahix)
- [[Refrain] Irozuku Sekai no Ashita kara - 13 VOSTFR [720p]](https://uptobox.com/v8y618cez4dv)
- [[Refrain] Irozuku Sekai no Ashita kara - 13 VOSTFR [1080p]](https://uptobox.com/bwou3vg6b6k9)
