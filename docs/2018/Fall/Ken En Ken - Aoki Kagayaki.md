# Ken En Ken: Aoki Kagayaki

![Ken En Ken: Aoki Kagayaki](https://cdn.myanimelist.net/images/anime/1339/95573l.jpg)

* Japanese:  軒轅剣・蒼き曜

## Information

 - Type: TV
 - Episodes: 13
 - Status: Finished Airing
 - Aired: Oct 2, 2018 to Dec 25, 2018
 - Premiered: Fall 2018
 - Broadcast: Tuesdays at 02:05 (JST)
 - Producers: TV Tokyo, Ai Addiction 
 - Licensors: None found, add some 
 - Studios: Studio Deen 
 - Source: Game
 - Genres: Action, Adventure, Demons, Magic, Martial Arts, Fantasy
 - Duration: 24 min. per ep.
 - Rating: PG-13 - Teens 13 or older


## Links
- [[Refrain] Ken En Ken - Aoki Kagayaki - 01 VOSTFR [720p]](https://uptobox.com/2857uofq3w0d)
- [[Refrain] Ken En Ken - Aoki Kagayaki - 01 VOSTFR [1080p]](https://uptobox.com/09usx6qb0wvt)
- [[Refrain] Ken En Ken - Aoki Kagayaki - 02 VOSTFR [720p]](https://uptobox.com/vnix6zt2h46f)
- [[Refrain] Ken En Ken - Aoki Kagayaki - 02 VOSTFR [1080p]](https://uptobox.com/4ekozl5mcpdb)
- [[Refrain] Ken En Ken - Aoki Kagayaki - 03 VOSTFR [720p]](https://uptobox.com/gqtw23md7k57)
- [[Refrain] Ken En Ken - Aoki Kagayaki - 03 VOSTFR [1080p]](https://uptobox.com/nule1ngvtz47)
- [[Refrain] Ken En Ken - Aoki Kagayaki - 04 VOSTFR [720p]](https://uptobox.com/bc4spzubag5y)
- [[Refrain] Ken En Ken - Aoki Kagayaki - 04 VOSTFR [1080p]](https://uptobox.com/xm5tckfwcrqw)
- [[Refrain] Ken En Ken - Aoki Kagayaki - 05 VOSTFR [720p]](https://uptobox.com/4edc8zihre5t)
- [[Refrain] Ken En Ken - Aoki Kagayaki - 05 VOSTFR [1080p]](https://uptobox.com/8v3lpo680d46)
- [[Refrain] Ken En Ken - Aoki Kagayaki - 06 VOSTFR [720p]](https://uptobox.com/2z52dpj2nq50)
- [[Refrain] Ken En Ken - Aoki Kagayaki - 06 VOSTFR [1080p]](https://uptobox.com/1a040gxnp30c)
- [[Refrain] Ken En Ken - Aoki Kagayaki - 07 VOSTFR [720p]](https://uptobox.com/2vnoqkr4jaje)
- [[Refrain] Ken En Ken - Aoki Kagayaki - 07 VOSTFR [1080p]](https://uptobox.com/jvptb4rjvz6k)
- [[Refrain] Ken En Ken - Aoki Kagayaki - 08 VOSTFR [720p]](https://uptobox.com/wcwuw8049r4k)
- [[Refrain] Ken En Ken - Aoki Kagayaki - 08 VOSTFR [1080p]](https://uptobox.com/b88zlscrcmuq)
- [[Refrain] Ken En Ken - Aoki Kagayaki - 09 VOSTFR [720p]](https://uptobox.com/2s3i0kngv3qy)
- [[Refrain] Ken En Ken - Aoki Kagayaki - 09 VOSTFR [1080p]](https://uptobox.com/uyxu1ikx3o7w)
- [[Refrain] Ken En Ken - Aoki Kagayaki - 10 VOSTFR [720p]](https://uptobox.com/ahswktgqadsp)
- [[Refrain] Ken En Ken - Aoki Kagayaki - 10 VOSTFR [1080p]](https://uptobox.com/39728co0jug2)
- [[Refrain] Ken En Ken - Aoki Kagayaki - 11 VOSTFR [720p]](https://uptobox.com/dlm2gk7x77g7)
- [[Refrain] Ken En Ken - Aoki Kagayaki - 11 VOSTFR [1080p]](https://uptobox.com/oxz31pqaep7l)
- [[Refrain] Ken En Ken - Aoki Kagayaki - 12 VOSTFR [720p]](https://uptobox.com/t8ks9lcpaada)
- [[Refrain] Ken En Ken - Aoki Kagayaki - 12 VOSTFR [1080p]](https://uptobox.com/2m5z7rvkb6af)
- [[Refrain] Ken En Ken - Aoki Kagayaki - 13 VOSTFR [720p]](https://uptobox.com/l6wqlrlvhdn6)
- [[Refrain] Ken En Ken - Aoki Kagayaki - 13 VOSTFR [1080p]](https://uptobox.com/ji9hruc90y2s)
