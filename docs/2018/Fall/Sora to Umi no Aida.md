# Sora to Umi no Aida

![Sora to Umi no Aida](https://cdn.myanimelist.net/images/anime/1297/97156l.jpg)

* Japanese:  ソラとウミのアイダ

## Information

 - Type: TV
 - Episodes: 12
 - Status: Finished Airing
 - Aired: Oct 4, 2018 to Dec 20, 2018
 - Premiered: Fall 2018
 - Broadcast: Thursdays at 00:00 (JST)
 - Producers: Half H.P Studio 
 - Licensors: Sentai Filmworks 
 - Studios: TMS Entertainment 
 - Source: Game
 - Genres: Sci-Fi, Adventure, Space, Fantasy
 - Duration: 23 min. per ep.
 - Rating: PG-13 - Teens 13 or older


## Links
- [[Refrain] Sora to Umi no Aida - 01 VOSTFR [720p]](https://uptobox.com/eay4l08onwuc)
- [[Refrain] Sora to Umi no Aida - 01 VOSTFR [1080p]](https://uptobox.com/9brx9yjq0sit)
- [[Refrain] Sora to Umi no Aida - 02 VOSTFR [720p]](https://uptobox.com/m0iw3q64vo2b)
- [[Refrain] Sora to Umi no Aida - 02 VOSTFR [1080p]](https://uptobox.com/u37ck6slkjxy)
- [[Refrain] Sora to Umi no Aida - 03 VOSTFR [720p]](https://uptobox.com/ta37xwf25dfm)
- [[Refrain] Sora to Umi no Aida - 03 VOSTFR [1080p]](https://uptobox.com/rex2y8kkr5uh)
- [[Refrain] Sora to Umi no Aida - 04 VOSTFR [720p]](https://uptobox.com/0e3luh8v4dzw)
- [[Refrain] Sora to Umi no Aida - 04 VOSTFR [1080p]](https://uptobox.com/z7htham6d3t3)
- [[Refrain] Sora to Umi no Aida - 05 VOSTFR [720p]](https://uptobox.com/lmbfx5qmrx51)
- [[Refrain] Sora to Umi no Aida - 05 VOSTFR [1080p]](https://uptobox.com/uc3byyziqbdm)
- [[Refrain] Sora to Umi no Aida - 06 VOSTFR [720p]](https://uptobox.com/6q652robx0t2)
- [[Refrain] Sora to Umi no Aida - 06 VOSTFR [1080p]](https://uptobox.com/sultnmampstz)
- [[Refrain] Sora to Umi no Aida - 07 VOSTFR [720p]](https://uptobox.com/dea9xf1hforx)
- [[Refrain] Sora to Umi no Aida - 07 VOSTFR [1080p]](https://uptobox.com/zjige2tv7wmh)
- [[Refrain] Sora to Umi no Aida - 08 VOSTFR [720p]](https://uptobox.com/l7fcpfue7i21)
- [[Refrain] Sora to Umi no Aida - 08 VOSTFR [1080p]](https://uptobox.com/uuvg6a49nfy1)
- [[Refrain] Sora to Umi no Aida - 09 VOSTFR [720p]](https://uptobox.com/ow3qml1elyiv)
- [[Refrain] Sora to Umi no Aida - 09 VOSTFR [1080p]](https://uptobox.com/s00y2kgls7jg)
- [[Refrain] Sora to Umi no Aida - 10 VOSTFR [720p]](https://uptobox.com/0oa3w5ce376t)
- [[Refrain] Sora to Umi no Aida - 10 VOSTFR [1080p]](https://uptobox.com/8ad1oq10p7nj)
- [[Refrain] Sora to Umi no Aida - 11 VOSTFR [720p]](https://uptobox.com/b08rdx6lnh95)
- [[Refrain] Sora to Umi no Aida - 11 VOSTFR [1080p]](https://uptobox.com/ouh2an7tpl2p)
- [[Refrain] Sora to Umi no Aida - 12 VOSTFR [720p]](https://uptobox.com/rblw5y83p6fx)
- [[Refrain] Sora to Umi no Aida - 12 VOSTFR [1080p]](https://uptobox.com/wc6myrw3mzex)
