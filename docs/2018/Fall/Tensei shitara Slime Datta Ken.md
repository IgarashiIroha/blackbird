# Tensei shitara Slime Datta Ken

![Tensei shitara Slime Datta Ken](https://cdn.myanimelist.net/images/anime/1694/93337l.jpg)

* Japanese:  転生したらスライムだった件

## Information

 - Type: TV
 - Episodes: 24
 - Status: Currently Airing
 - Aired: Oct 2, 2018 to ?
 - Premiered: Fall 2018
 - Broadcast: Tuesdays at 00:00 (JST)
 - Producers: Lantis, BS11, Bandai Namco Arts, Micro Magazine Publishing 
 - Licensors: Funimation 
 - Studios: 8bit 
 - Source: Manga
 - Genres: Fantasy, Shounen
 - Duration: 23 min. per ep.
 - Rating: PG-13 - Teens 13 or older


## Links
- [[Refrain] Tensei Shitara Slime Datta Ken - 01 VOSTFR [720p]](https://uptobox.com/grhw42dsxf0c)
- [[Refrain] Tensei Shitara Slime Datta Ken - 01 VOSTFR [1080p]](https://uptobox.com/0ydboqa2z7ds)
- [[Refrain] Tensei Shitara Slime Datta Ken - 02 VOSTFR [720p]](https://uptobox.com/xi3081c2ynxd)
- [[Refrain] Tensei Shitara Slime Datta Ken - 02 VOSTFR [1080p]](https://uptobox.com/kcwpf1jtshsr)
- [[Refrain] Tensei Shitara Slime Datta Ken - 03 VOSTFR [720p]](https://uptobox.com/0yyoaq0zothn)
- [[Refrain] Tensei Shitara Slime Datta Ken - 03 VOSTFR [1080p]](https://uptobox.com/xo78kkb0vmu0)
- [[Refrain] Tensei Shitara Slime Datta Ken - 04 VOSTFR [720p]](https://uptobox.com/rp1l8ctwwxdg)
- [[Refrain] Tensei Shitara Slime Datta Ken - 04 VOSTFR [1080p]](https://uptobox.com/55f5g7sgtcls)
- [[Refrain] Tensei Shitara Slime Datta Ken - 05 VOSTFR [720p]](https://uptobox.com/edjm9b88fek7)
- [[Refrain] Tensei Shitara Slime Datta Ken - 05 VOSTFR [1080p]](https://uptobox.com/rl01wnb8ukgc)
- [[Refrain] Tensei Shitara Slime Datta Ken - 06 VOSTFR [720p]](https://uptobox.com/1kc5xpe6wcsq)
- [[Refrain] Tensei Shitara Slime Datta Ken - 06 VOSTFR [1080p]](https://uptobox.com/mjrwlsx03xm1)
- [[Refrain] Tensei Shitara Slime Datta Ken - 07 VOSTFR [720p]](https://uptobox.com/5ze9imq7pcjr)
- [[Refrain] Tensei Shitara Slime Datta Ken - 07 VOSTFR [1080p]](https://uptobox.com/m9a2h85e7y3v)
- [[Refrain] Tensei Shitara Slime Datta Ken - 08 VOSTFR [720p]](https://uptobox.com/j8hpew9wyocn)
- [[Refrain] Tensei Shitara Slime Datta Ken - 08 VOSTFR [1080p]](https://uptobox.com/imfwemxr5q5q)
- [[Refrain] Tensei Shitara Slime Datta Ken - 09 VOSTFR [720p]](https://uptobox.com/8p42ps5ub0h9)
- [[Refrain] Tensei Shitara Slime Datta Ken - 09 VOSTFR [1080p]](https://uptobox.com/udn9yg1dy7b9)
- [[Refrain] Tensei Shitara Slime Datta Ken - 10 VOSTFR [720p]](https://uptobox.com/j4bjvyp3y1jy)
- [[Refrain] Tensei Shitara Slime Datta Ken - 10 VOSTFR [1080p]](https://uptobox.com/9tk5x48m15yx)
- [[Refrain] Tensei Shitara Slime Datta Ken - 11 VOSTFR [720p]](https://uptobox.com/n0lmvpfl1zpm)
- [[Refrain] Tensei Shitara Slime Datta Ken - 11 VOSTFR [1080p]](https://uptobox.com/7zqtbkp5yx77)
- [[Refrain] Tensei Shitara Slime Datta Ken - 12 VOSTFR [720p]](https://uptobox.com/5puldy2nmkit)
- [[Refrain] Tensei Shitara Slime Datta Ken - 12 VOSTFR [1080p]](https://uptobox.com/p9owvg52zc1y)
- [[Refrain] Tensei Shitara Slime Datta Ken - 13 VOSTFR [720p]](https://uptobox.com/jlntzyqqqohi)
- [[Refrain] Tensei Shitara Slime Datta Ken - 13 VOSTFR [1080p]](https://uptobox.com/4lysuznhisps)
- [[Refrain] Tensei Shitara Slime Datta Ken - 14 VOSTFR [720p]](https://uptobox.com/wkt32tn1myqz)
- [[Refrain] Tensei Shitara Slime Datta Ken - 14 VOSTFR [1080p]](https://uptobox.com/murrh7h9hvdo)
