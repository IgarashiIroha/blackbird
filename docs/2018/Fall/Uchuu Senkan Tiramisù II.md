# Uchuu Senkan Tiramisù II

![Uchuu Senkan Tiramisù II](https://cdn.myanimelist.net/images/anime/1665/93614l.jpg)

* Japanese:  宇宙戦艦ティラミスⅡ（ツヴァイ）

## Information

 - Type: TV
 - Episodes: 13
 - Status: Finished Airing
 - Aired: Oct 2, 2018 to Dec 25, 2018
 - Premiered: Fall 2018
 - Broadcast: Tuesdays at 01:00 (JST)
 - Producers: Frontier Works, Nippon Columbia, Studio Mausu, Tokyo MX, Adores, Shinchosha, Furyu, Exa International, NTT Plala, JY Animation 
 - Licensors: Funimation 
 - Studios: Gonzo 
 - Source: Web manga
 - Genres: Sci-Fi, Slice of Life, Space, Comedy, Mecha
 - Duration: 7 min. per ep.
 - Rating: PG-13 - Teens 13 or older


## Links

