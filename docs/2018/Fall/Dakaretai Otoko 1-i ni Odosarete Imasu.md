# Dakaretai Otoko 1-i ni Odosarete Imasu.

![Dakaretai Otoko 1-i ni Odosarete Imasu.](https://cdn.myanimelist.net/images/anime/1402/95620l.jpg)

* Japanese:  抱かれたい男1位に脅されています。

## Information

 - Type: TV
 - Episodes: 13
 - Status: Finished Airing
 - Aired: Oct 6, 2018 to Dec 29, 2018
 - Premiered: Fall 2018
 - Broadcast: Saturdays at 00:00 (JST)
 - Producers: Aniplex, Movic, Libre, animate 
 - Licensors: Aniplex of America 
 - Studios: CloverWorks 
 - Source: Manga
 - Genres: Comedy, Drama, Romance, Shounen Ai
 - Duration: 23 min. per ep.
 - Rating: PG-13 - Teens 13 or older


## Links
- [[Refrain] Dakaretai Otoko 1-i ni Odosarete Imasu. - 01 VOSTFR [720p]](https://uptobox.com/gawt3kmc3k4j)
- [[Refrain] Dakaretai Otoko 1-i ni Odosarete Imasu. - 01 VOSTFR [1080p]](https://uptobox.com/yysccde95m08)
- [[Refrain] Dakaretai Otoko 1-i ni Odosarete Imasu. - 02 VOSTFR [720p]](https://uptobox.com/ebcys6ttroyp)
- [[Refrain] Dakaretai Otoko 1-i ni Odosarete Imasu. - 02 VOSTFR [1080p]](https://uptobox.com/w2b5g3gnkiqw)
- [[Refrain] Dakaretai Otoko 1-i ni Odosarete Imasu. - 03 VOSTFR [720p]](https://uptobox.com/ulxarakvezfj)
- [[Refrain] Dakaretai Otoko 1-i ni Odosarete Imasu. - 03 VOSTFR [1080p]](https://uptobox.com/ngt87rrult9s)
- [[Refrain] Dakaretai Otoko 1-i ni Odosarete Imasu. - 04 VOSTFR [720p]](https://uptobox.com/ypuggk8ogrjf)
- [[Refrain] Dakaretai Otoko 1-i ni Odosarete Imasu. - 04 VOSTFR [1080p]](https://uptobox.com/k5wkx9jvf8ac)
- [[Refrain] Dakaretai Otoko 1-i ni Odosarete Imasu. - 05 VOSTFR [720p]](https://uptobox.com/4vlqtlrh3d9f)
- [[Refrain] Dakaretai Otoko 1-i ni Odosarete Imasu. - 05 VOSTFR [1080p]](https://uptobox.com/a9s8vbumr8fo)
- [[Refrain] Dakaretai Otoko 1-i ni Odosarete Imasu. - 06 VOSTFR [720p]](https://uptobox.com/5s0mlcl68zys)
- [[Refrain] Dakaretai Otoko 1-i ni Odosarete Imasu. - 06 VOSTFR [1080p]](https://uptobox.com/jv2dbh2a8mm2)
- [[Refrain] Dakaretai Otoko 1-i ni Odosarete Imasu. - 07 VOSTFR [720p]](https://uptobox.com/fk9rn4anf7s3)
- [[Refrain] Dakaretai Otoko 1-i ni Odosarete Imasu. - 07 VOSTFR [1080p]](https://uptobox.com/rl9dtjuw6kup)
- [[Refrain] Dakaretai Otoko 1-i ni Odosarete Imasu. - 08 VOSTFR [720p]](https://uptobox.com/ejh6r4x9i2rb)
- [[Refrain] Dakaretai Otoko 1-i ni Odosarete Imasu. - 08 VOSTFR [1080p]](https://uptobox.com/revvfb3pkwki)
- [[Refrain] Dakaretai Otoko 1-i ni Odosarete Imasu. - 09 VOSTFR [720p]](https://uptobox.com/8u0fwtgxboxq)
- [[Refrain] Dakaretai Otoko 1-i ni Odosarete Imasu. - 09 VOSTFR [1080p]](https://uptobox.com/bzeyrx3smf1e)
- [[Refrain] Dakaretai Otoko 1-i ni Odosarete Imasu. - 10 VOSTFR [720p]](https://uptobox.com/idrdpu15hykn)
- [[Refrain] Dakaretai Otoko 1-i ni Odosarete Imasu. - 10 VOSTFR [1080p]](https://uptobox.com/9l0g2pe7haz1)
- [[Refrain] Dakaretai Otoko 1-i ni Odosarete Imasu. - 11 VOSTFR [720p]](https://uptobox.com/zvep91tewiei)
- [[Refrain] Dakaretai Otoko 1-i ni Odosarete Imasu. - 11 VOSTFR [1080p]](https://uptobox.com/dil5e3erjf0j)
- [[Refrain] Dakaretai Otoko 1-i ni Odosarete Imasu. - 12 VOSTFR [720p]](https://uptobox.com/npl5daxwhfdi)
- [[Refrain] Dakaretai Otoko 1-i ni Odosarete Imasu. - 12 VOSTFR [1080p]](https://uptobox.com/drvh979dnk4i)
- [[Refrain] Dakaretai Otoko 1-i ni Odosarete Imasu. - 13 VOSTFR [720p]](https://uptobox.com/7nf1jfiervuk)
- [[Refrain] Dakaretai Otoko 1-i ni Odosarete Imasu. - 13 VOSTFR [1080p]](https://uptobox.com/gm0whtn11dvc)
