# Sword Art Online: Alicization

![Sword Art Online: Alicization](https://cdn.myanimelist.net/images/anime/1993/93837l.jpg)

* Japanese:  ソードアート・オンライン アリシゼーション

## Information

 - Type: TV
 - Episodes: Unknown
 - Status: Currently Airing
 - Aired: Oct 7, 2018 to ?
 - Premiered: Fall 2018
 - Broadcast: Sundays at 00:00 (JST)
 - Producers: Aniplex, Genco, Bandai Namco Entertainment, Egg Firm, Straight Edge, Kadokawa, Sonilude 
 - Licensors: Aniplex of America 
 - Studios: A-1 Pictures 
 - Source: Light novel
 - Genres: Action, Game, Adventure, Romance, Fantasy
 - Duration: 24 min.
 - Rating: PG-13 - Teens 13 or older


## Links
- [[Refrain] Sword Art Online - Alicization - 01 VOSTFR [720p]](https://uptobox.com/hxdew5yclbfq)
- [[Refrain] Sword Art Online - Alicization - 01 VOSTFR [1080p]](https://uptobox.com/bmdms6kvlanf)
- [[Refrain] Sword Art Online - Alicization - 02 VOSTFR [720p]](https://uptobox.com/vo8jcjhuti4h)
- [[Refrain] Sword Art Online - Alicization - 02 VOSTFR [1080p]](https://uptobox.com/as559pep820b)
- [[Refrain] Sword Art Online - Alicization - 03 VOSTFR [720p]](https://uptobox.com/yq3hftt5646j)
- [[Refrain] Sword Art Online - Alicization - 03 VOSTFR [1080p]](https://uptobox.com/djrxmz56uo3j)
- [[Refrain] Sword Art Online - Alicization - 04 VOSTFR [720p]](https://uptobox.com/p1snxj5h8ao2)
- [[Refrain] Sword Art Online - Alicization - 04 VOSTFR [1080p]](https://uptobox.com/4ee9ugr3ee9r)
- [[Refrain] Sword Art Online - Alicization - 05 VOSTFR [720p]](https://uptobox.com/r7fh74v23fmh)
- [[Refrain] Sword Art Online - Alicization - 05 VOSTFR [1080p]](https://uptobox.com/8m6tz9exwu8y)
- [[Refrain] Sword Art Online - Alicization - 06 VOSTFR [720p]](https://uptobox.com/7ufwnt62z8qa)
- [[Refrain] Sword Art Online - Alicization - 06 VOSTFR [1080p]](https://uptobox.com/48suc58ifz5t)
- [[Refrain] Sword Art Online - Alicization - 07 VOSTFR [720p]](https://uptobox.com/trao6wcfol59)
- [[Refrain] Sword Art Online - Alicization - 07 VOSTFR [1080p]](https://uptobox.com/9eqrqsldjpv4)
- [[Refrain] Sword Art Online - Alicization - 08 VOSTFR [720p]](https://uptobox.com/u793cothbflh)
- [[Refrain] Sword Art Online - Alicization - 08 VOSTFR [1080p]](https://uptobox.com/0ksxltfh2ofa)
- [[Refrain] Sword Art Online - Alicization - 09 VOSTFR [720p]](https://uptobox.com/1flpif288gzx)
- [[Refrain] Sword Art Online - Alicization - 09 VOSTFR [1080p]](https://uptobox.com/zbftyjr2r5nc)
- [[Refrain] Sword Art Online - Alicization - 10 VOSTFR [720p]](https://uptobox.com/h6cezdbe0lhw)
- [[Refrain] Sword Art Online - Alicization - 10 VOSTFR [1080p]](https://uptobox.com/4vwlenc5obs5)
- [[Refrain] Sword Art Online - Alicization - 11 VOSTFR [720p]](https://uptobox.com/p47rdzwa5m48)
- [[Refrain] Sword Art Online - Alicization - 11 VOSTFR [1080p]](https://uptobox.com/mb7cn1gqdnr9)
- [[Refrain] Sword Art Online - Alicization - 12 VOSTFR [720p]](https://uptobox.com/adr2riprnpgh)
- [[Refrain] Sword Art Online - Alicization - 12 VOSTFR [1080p]](https://uptobox.com/fanzbgofimww)
- [[Refrain] Sword Art Online - Alicization - 13 VOSTFR [1080p]](https://uptobox.com/0dicdqxpk4xg)
- [[Refrain] Sword Art Online - Alicization - 13 VOSTFR [720p]](https://uptobox.com/d3qi0ukfetdc)
- [[Refrain] Sword Art Online - Alicization - 14 VOSTFR [1080p]](https://uptobox.com/ciw4ds0hotfl)
- [[Refrain] Sword Art Online - Alicization - 14 VOSTFR [720p]](https://uptobox.com/0mx7kzhrb5og)
- [[Refrain] Sword Art Online - Alicization - 15 VOSTFR [1080p]](https://uptobox.com/7oodv32jfuqs)
- [[Refrain] Sword Art Online - Alicization - 15 VOSTFR [720p]](https://uptobox.com/plwka9n2xjs9)
- [[Refrain] Sword Art Online - Alicization - 16 VOSTFR [1080p]](https://uptobox.com/mq3qtgzmtb8q)
- [[Refrain] Sword Art Online - Alicization - 16 VOSTFR [720p]](https://uptobox.com/tajsknosvp0h)
- [[Refrain] Sword Art Online - Alicization - 17 VOSTFR [1080p]](https://uptobox.com/lyhni0oros18)
- [[Refrain] Sword Art Online - Alicization - 17 VOSTFR [720p]](https://uptobox.com/br77gfyaunq2)
- [[Refrain] Sword Art Online - Alicization - 18.5 VOSTFR [1080p]](https://uptobox.com/shxwfzqpio1e)
- [[Refrain] Sword Art Online - Alicization - 18.5 VOSTFR [720p]](https://uptobox.com/hbxzhd213clc)
- [[Refrain] Sword Art Online - Alicization - 18 VOSTFR [1080p]](https://uptobox.com/0gy45k7wj9kw)
- [[Refrain] Sword Art Online - Alicization - 18 VOSTFR [720p]](https://uptobox.com/2ibwfozk0wio)
- [[Refrain] Sword Art Online - Alicization - 19 VOSTFR [1080p]](https://uptobox.com/euz55gskflq2)
- [[Refrain] Sword Art Online - Alicization - 19 VOSTFR [720p]](https://uptobox.com/351gi3pwqexc)
- [[Refrain] Sword Art Online - Alicization - 20 VOSTFR [1080p]](https://uptobox.com/o74osf8jat9h)
- [[Refrain] Sword Art Online - Alicization - 20 VOSTFR [720p]](https://uptobox.com/7f5g7ve28skw)
- [[Refrain] Sword Art Online - Alicization - 21 VOSTFR [1080p]](https://uptobox.com/9263ih7u8bya)
- [[Refrain] Sword Art Online - Alicization - 21 VOSTFR [720p]](https://uptobox.com/kpzq99nd9zni)
- [[Refrain] Sword Art Online - Alicization - 22 VOSTFR [1080p]](https://uptobox.com/6n95hno73j51)
- [[Refrain] Sword Art Online - Alicization - 22 VOSTFR [720p]](https://uptobox.com/hehxgxym1bgf)
- [[Refrain] Sword Art Online - Alicization - 23 VOSTFR [1080p]](https://uptobox.com/tewrwvzxmvd6)
- [[Refrain] Sword Art Online - Alicization - 23 VOSTFR [720p]](https://uptobox.com/t7oby1u0iac7)
- [[Refrain] Sword Art Online - Alicization - 24 VOSTFR [1080p]](https://uptobox.com/4uxr6s67pxiv)
- [[Refrain] Sword Art Online - Alicization - 24 VOSTFR [720p]](https://uptobox.com/gv7cemzyk2ww)
