# Tonari no Kyuuketsuki-san

![Tonari no Kyuuketsuki-san](https://cdn.myanimelist.net/images/anime/1851/96154l.jpg)

* Japanese:  となりの吸血鬼さん

## Information

 - Type: TV
 - Episodes: 12
 - Status: Finished Airing
 - Aired: Oct 5, 2018 to Dec 21, 2018
 - Premiered: Fall 2018
 - Broadcast: Fridays at 21:00 (JST)
 - Producers: Nippon Columbia 
 - Licensors: None found, add some 
 - Studios: Studio Gokumi, AXsiZ 
 - Source: 4-koma manga
 - Genres: Comedy, Shoujo Ai, Slice of Life, Supernatural, Vampire
 - Duration: 23 min. per ep.
 - Rating: PG-13 - Teens 13 or older


## Links
- [[Refrain] Tonari no Kyuuketsuki-san - 01 VOSTFR [720p]](https://uptobox.com/xjhulojjltwz)
- [[Refrain] Tonari no Kyuuketsuki-san - 01 VOSTFR [1080p]](https://uptobox.com/f7bujdf1wx70)
- [[Refrain] Tonari no Kyuuketsuki-san - 02 VOSTFR [720p]](https://uptobox.com/0z0bd433rv04)
- [[Refrain] Tonari no Kyuuketsuki-san - 02 VOSTFR [1080p]](https://uptobox.com/qbi6dg0cb9v4)
- [[Refrain] Tonari no Kyuuketsuki-san - 03 VOSTFR [720p]](https://uptobox.com/2feq51b27ci5)
- [[Refrain] Tonari no Kyuuketsuki-san - 03 VOSTFR [1080p]](https://uptobox.com/lulax5daxhix)
- [[Refrain] Tonari no Kyuuketsuki-san - 04 VOSTFR [720p]](https://uptobox.com/y42t6yz8ignt)
- [[Refrain] Tonari no Kyuuketsuki-san - 04 VOSTFR [1080p]](https://uptobox.com/aosca06d87vq)
- [[Refrain] Tonari no Kyuuketsuki-san - 05 VOSTFR [720p]](https://uptobox.com/32lu4m2fqyvr)
- [[Refrain] Tonari no Kyuuketsuki-san - 05 VOSTFR [1080p]](https://uptobox.com/wu42prvgkzf7)
- [[Refrain] Tonari no Kyuuketsuki-san - 06 VOSTFR [720p]](https://uptobox.com/eqje2xxrhd3m)
- [[Refrain] Tonari no Kyuuketsuki-san - 06 VOSTFR [1080p]](https://uptobox.com/r2uihkrt6v45)
- [[Refrain] Tonari no Kyuuketsuki-san - 07 VOSTFR [720p]](https://uptobox.com/083yksz9ai05)
- [[Refrain] Tonari no Kyuuketsuki-san - 07 VOSTFR [1080p]](https://uptobox.com/zt44lcuzft0d)
- [[Refrain] Tonari no Kyuuketsuki-san - 08 VOSTFR [720p]](https://uptobox.com/1im584wvo2la)
- [[Refrain] Tonari no Kyuuketsuki-san - 08 VOSTFR [1080p]](https://uptobox.com/92v90txae7lp)
- [[Refrain] Tonari no Kyuuketsuki-san - 09 VOSTFR [720p]](https://uptobox.com/0mmbptiiejur)
- [[Refrain] Tonari no Kyuuketsuki-san - 09 VOSTFR [1080p]](https://uptobox.com/4u31k6xnab38)
- [[Refrain] Tonari no Kyuuketsuki-san - 10 VOSTFR [720p]](https://uptobox.com/c8r7ml6pzm8p)
- [[Refrain] Tonari no Kyuuketsuki-san - 10 VOSTFR [1080p]](https://uptobox.com/l0gfsme7dbpo)
- [[Refrain] Tonari no Kyuuketsuki-san - 11 VOSTFR [720p]](https://uptobox.com/jbuzsn666nve)
- [[Refrain] Tonari no Kyuuketsuki-san - 11 VOSTFR [1080p]](https://uptobox.com/pbylqhxshxgu)
- [[Refrain] Tonari no Kyuuketsuki-san - 12 VOSTFR [720p]](https://uptobox.com/nvpjluy681ib)
- [[Refrain] Tonari no Kyuuketsuki-san - 12 VOSTFR [1080p]](https://uptobox.com/llpjnmgg83n1)
