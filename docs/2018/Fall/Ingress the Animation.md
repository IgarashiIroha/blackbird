# Ingress the Animation

![Ingress the Animation](https://cdn.myanimelist.net/images/anime/1944/93421l.jpg)

* Japanese:  INGRESS THE ANIMATION

## Information

 - Type: ONA
 - Episodes: 11
 - Status: Finished Airing
 - Aired: Oct 18, 2018
 - Producers: Fuji TV, NIANTIC
 - Licensors: None found, add some
 - Studios: Craftar Studios
 - Source: Game
 - Genres: Action, Game, Sci-Fi, Mystery
 - Duration: 23 min. per ep.
 - Rating: None


## Links

- [[Refrain] Ingress The Animation - 01 VOSTFR [720p]](https://uptobox.com/3n3m79q32mdx)
- [[Refrain] Ingress The Animation - 01 VOSTFR [1080p]](https://uptobox.com/cy8g1rmm7om8)
- [[Refrain] Ingress The Animation - 02 VOSTFR [720p]](https://uptobox.com/bd4xe4fpoctu)
- [[Refrain] Ingress The Animation - 02 VOSTFR [1080p]](https://uptobox.com/etzxcu66h0m8)
- [[Refrain] Ingress The Animation - 03 VOSTFR [720p]](https://uptobox.com/xq6hi57uhyno)
- [[Refrain] Ingress The Animation - 03 VOSTFR [1080p]](https://uptobox.com/3ev0hfbi4efx)
- [[Refrain] Ingress The Animation - 04 VOSTFR [720p]](https://uptobox.com/gaf52a5fzsug)
- [[Refrain] Ingress The Animation - 04 VOSTFR [1080p]](https://uptobox.com/eotlgfd6ov7w)
- [[Refrain] Ingress The Animation - 05 VOSTFR [720p]](https://uptobox.com/iyi7xlyoquyo)
- [[Refrain] Ingress The Animation - 05 VOSTFR [1080p]](https://uptobox.com/0sd5d10f0yqk)
- [[Refrain] Ingress The Animation - 06 VOSTFR [720p]](https://uptobox.com/rl2ho96oizis)
- [[Refrain] Ingress The Animation - 06 VOSTFR [1080p]](https://uptobox.com/tq1yqbb72g3w)
- [[Refrain] Ingress The Animation - 07 VOSTFR [720p]](https://uptobox.com/tnm3r54rojlb)
- [[Refrain] Ingress The Animation - 07 VOSTFR [1080p]](https://uptobox.com/fw0ba9z0u5eq)
- [[Refrain] Ingress The Animation - 08 VOSTFR [720p]](https://uptobox.com/0ol1h9zwd64r)
- [[Refrain] Ingress The Animation - 08 VOSTFR [1080p]](https://uptobox.com/104jzhryqkb5)
- [[Refrain] Ingress The Animation - 09 VOSTFR [720p]](https://uptobox.com/arv7ommb051b)
- [[Refrain] Ingress The Animation - 09 VOSTFR [1080p]](https://uptobox.com/0aq0sux1e89v)
- [[Refrain] Ingress The Animation - 10 VOSTFR [720p]](https://uptobox.com/xlfutyqcwt3x)
- [[Refrain] Ingress The Animation - 10 VOSTFR [1080p]](https://uptobox.com/mtifkhue4a43)
- [[Refrain] Ingress The Animation - 11 VOSTFR [720p]](https://uptobox.com/vr1c1f1bjx08)
- [[Refrain] Ingress The Animation - 11 VOSTFR [1080p]](https://uptobox.com/m224ljur8rq8)
