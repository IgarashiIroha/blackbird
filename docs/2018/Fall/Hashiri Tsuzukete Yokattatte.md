# Hashiri Tsuzukete Yokattatte.

![Hashiri Tsuzukete Yokattatte.](https://cdn.myanimelist.net/images/anime/1533/94555l.jpg)

* Japanese:  走り続けてよかったって。

## Information

 - Type: TV
 - Episodes: 4
 - Status: Finished Airing
 - Aired: Oct 9, 2018 to Oct 30, 2018
 - Premiered: Fall 2018
 - Broadcast: Tuesdays at 02:40 (JST)
 - Producers: DAX Production, Twin Planet 
 - Licensors: None found, add some 
 - Studios: Signal.MD 
 - Source: Original
 - Genres: Drama, Slice of Life
 - Duration: 15 min. per ep.
 - Rating: PG-13 - Teens 13 or older


## Links

