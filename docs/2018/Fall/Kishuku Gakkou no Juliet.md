# Kishuku Gakkou no Juliet

![Kishuku Gakkou no Juliet](https://cdn.myanimelist.net/images/anime/1908/93416l.jpg)

* Japanese:  寄宿学校のジュリエット

## Information

 - Type: TV
 - Episodes: 12
 - Status: Finished Airing
 - Aired: Oct 6, 2018 to Dec 22, 2018
 - Premiered: Fall 2018
 - Broadcast: Saturdays at 01:25 (JST)
 - Producers: NBCUniversal Entertainment Japan 
 - Licensors: None found, add some 
 - Studios: LIDENFILMS 
 - Source: Manga
 - Genres: Action, Comedy, Romance, School, Shounen
 - Duration: 24 min. per ep.
 - Rating: PG-13 - Teens 13 or older


## Links
- [[Refrain] Kishuku Gakkou no Juliet - 01 VOSTFR [720p]](https://uptobox.com/g1yf3d6cn29r)
- [[Refrain] Kishuku Gakkou no Juliet - 01 VOSTFR [1080p]](https://uptobox.com/lnp4djm4aoja)
- [[Refrain] Kishuku Gakkou no Juliet - 02 VOSTFR [720p]](https://uptobox.com/pzedvrnczg4x)
- [[Refrain] Kishuku Gakkou no Juliet - 02 VOSTFR [1080p]](https://uptobox.com/2f70ki6xazz8)
- [[Refrain] Kishuku Gakkou no Juliet - 03 VOSTFR [720p]](https://uptobox.com/vvh4co6lhbmg)
- [[Refrain] Kishuku Gakkou no Juliet - 03 VOSTFR [1080p]](https://uptobox.com/1iela4o247of)
- [[Refrain] Kishuku Gakkou no Juliet - 04 VOSTFR [720p]](https://uptobox.com/zpye08t4bzgm)
- [[Refrain] Kishuku Gakkou no Juliet - 04 VOSTFR [1080p]](https://uptobox.com/qjabih6i5t7w)
- [[Refrain] Kishuku Gakkou no Juliet - 05 VOSTFR [720p]](https://uptobox.com/7fmcl1ks95qg)
- [[Refrain] Kishuku Gakkou no Juliet - 05 VOSTFR [1080p]](https://uptobox.com/2naow3ikbxhc)
- [[Refrain] Kishuku Gakkou no Juliet - 06 VOSTFR [720p]](https://uptobox.com/83coisz6c01o)
- [[Refrain] Kishuku Gakkou no Juliet - 06 VOSTFR [1080p]](https://uptobox.com/lq30slf3m91n)
- [[Refrain] Kishuku Gakkou no Juliet - 07 VOSTFR [720p]](https://uptobox.com/kvopghoh9ybg)
- [[Refrain] Kishuku Gakkou no Juliet - 07 VOSTFR [1080p]](https://uptobox.com/6h2qkeoqcodg)
- [[Refrain] Kishuku Gakkou no Juliet - 08 VOSTFR [720p]](https://uptobox.com/fjan9ww1fy7r)
- [[Refrain] Kishuku Gakkou no Juliet - 08 VOSTFR [1080p]](https://uptobox.com/lccl2x5zsryh)
- [[Refrain] Kishuku Gakkou no Juliet - 09 VOSTFR [720p]](https://uptobox.com/zb8306zzenlj)
- [[Refrain] Kishuku Gakkou no Juliet - 09 VOSTFR [1080p]](https://uptobox.com/3579r7deq0dl)
- [[Refrain] Kishuku Gakkou no Juliet - 10 VOSTFR [720p]](https://uptobox.com/ijajqt2f4elh)
- [[Refrain] Kishuku Gakkou no Juliet - 10 VOSTFR [1080p]](https://uptobox.com/bdlxh3od5sxc)
- [[Refrain] Kishuku Gakkou no Juliet - 11 VOSTFR [720p]](https://uptobox.com/h95ey2k1xqph)
- [[Refrain] Kishuku Gakkou no Juliet - 11 VOSTFR [1080p]](https://uptobox.com/75mf8gnhvuk1)
- [[Refrain] Kishuku Gakkou no Juliet - 12 VOSTFR [720p]](https://uptobox.com/m06ysbs9hzy1)
- [[Refrain] Kishuku Gakkou no Juliet - 12 VOSTFR [1080p]](https://uptobox.com/dpufm55pa0c4)
