# Yagate Kimi ni Naru

![Yagate Kimi ni Naru](https://cdn.myanimelist.net/images/anime/1783/96153l.jpg)

* Japanese:  やがて君になる

## Information

 - Type: TV
 - Episodes: 13
 - Status: Finished Airing
 - Aired: Oct 5, 2018 to Dec 28, 2018
 - Premiered: Fall 2018
 - Broadcast: Fridays at 21:30 (JST)
 - Producers: AT-X, Sony Music Communications, Docomo Anime Store, Kadokawa Media House, Kadokawa 
 - Licensors: Sentai Filmworks 
 - Studios: TROYCA 
 - Source: Manga
 - Genres: Romance, School, Shoujo Ai
 - Duration: 24 min. per ep.
 - Rating: PG-13 - Teens 13 or older


## Links
- [[Refrain] Yagate Kimi ni Naru - 01 VOSTFR [720p]](https://uptobox.com/cympl5lw7xiv)
- [[Refrain] Yagate Kimi ni Naru - 01 VOSTFR [1080p]](https://uptobox.com/bc47lmy22jt9)
- [[Refrain] Yagate Kimi ni Naru - 01 VOSTFR {ADN} [720p]](https://uptobox.com/cvfaa6qmh3ra)
- [[Refrain] Yagate Kimi ni Naru - 01 VOSTFR {ADN} [1080p]](https://uptobox.com/y9dzgznnjgb9)
- [[Refrain] Yagate Kimi ni Naru - 02 VOSTFR [720p]](https://uptobox.com/7vl43iufjkbl)
- [[Refrain] Yagate Kimi ni Naru - 02 VOSTFR [1080p]](https://uptobox.com/x57wwzkn0znl)
- [[Refrain] Yagate Kimi ni Naru - 02 VOSTFR {ADN} [720p]](https://uptobox.com/h342w56swwmv)
- [[Refrain] Yagate Kimi ni Naru - 02 VOSTFR {ADN} [1080p]](https://uptobox.com/yur0e1c1bzei)
- [[Refrain] Yagate Kimi ni Naru - 03 VOSTFR [720p]](https://uptobox.com/y16wvybv6huj)
- [[Refrain] Yagate Kimi ni Naru - 03 VOSTFR [1080p]](https://uptobox.com/g91d1kuannaj)
- [[Refrain] Yagate Kimi ni Naru - 03 VOSTFR {ADN} [720p]](https://uptobox.com/adpgppos5st9)
- [[Refrain] Yagate Kimi ni Naru - 03 VOSTFR {ADN} [1080p]](https://uptobox.com/2grw7ywgryv0)
- [[Refrain] Yagate Kimi ni Naru - 04 VOSTFR [720p]](https://uptobox.com/indkfbt2xtw3)
- [[Refrain] Yagate Kimi ni Naru - 04 VOSTFR [1080p]](https://uptobox.com/ta60snm5bsox)
- [[Refrain] Yagate Kimi ni Naru - 04 VOSTFR {ADN} [720p]](https://uptobox.com/e88wmrw1sd4h)
- [[Refrain] Yagate Kimi ni Naru - 04 VOSTFR {ADN} [1080p]](https://uptobox.com/oqh2s1qh0pbk)
- [[Refrain] Yagate Kimi ni Naru - 05 VOSTFR [720p]](https://uptobox.com/atvfflxsekv1)
- [[Refrain] Yagate Kimi ni Naru - 05 VOSTFR [1080p]](https://uptobox.com/0k1wlrcagvab)
- [[Refrain] Yagate Kimi ni Naru - 05 VOSTFR {ADN} [720p]](https://uptobox.com/zwbnq1fyn6ng)
- [[Refrain] Yagate Kimi ni Naru - 05 VOSTFR {ADN} [1080p]](https://uptobox.com/bvfdeiima5fr)
- [[Refrain] Yagate Kimi ni Naru - 06 VOSTFR [720p]](https://uptobox.com/o954jwfuni5f)
- [[Refrain] Yagate Kimi ni Naru - 06 VOSTFR [1080p]](https://uptobox.com/naakovxadhov)
- [[Refrain] Yagate Kimi ni Naru - 06 VOSTFR {ADN} [720p]](https://uptobox.com/n7ks0ai0w607)
- [[Refrain] Yagate Kimi ni Naru - 06 VOSTFR {ADN} [1080p]](https://uptobox.com/p84amt357ge4)
- [[Refrain] Yagate Kimi ni Naru - 07 VOSTFR [720p]](https://uptobox.com/ygpt1j6o1yh5)
- [[Refrain] Yagate Kimi ni Naru - 07 VOSTFR [1080p]](https://uptobox.com/ea1hg0e5xs7q)
- [[Refrain] Yagate Kimi ni Naru - 07 VOSTFR {ADN} [720p]](https://uptobox.com/7ohs6ckbica9)
- [[Refrain] Yagate Kimi ni Naru - 07 VOSTFR {ADN} [1080p]](https://uptobox.com/905dt78fo7ey)
- [[Refrain] Yagate Kimi ni Naru - 08 VOSTFR [720p]](https://uptobox.com/g8t4tozdsjrj)
- [[Refrain] Yagate Kimi ni Naru - 08 VOSTFR [1080p]](https://uptobox.com/tn8qllpakq15)
- [[Refrain] Yagate Kimi ni Naru - 08 VOSTFR {ADN} [720p]](https://uptobox.com/i9wrx98ittho)
- [[Refrain] Yagate Kimi ni Naru - 08 VOSTFR {ADN} [1080p]](https://uptobox.com/oirrgvpqzr7c)
- [[Refrain] Yagate Kimi ni Naru - 09 VOSTFR [720p]](https://uptobox.com/aaggbahvd504)
- [[Refrain] Yagate Kimi ni Naru - 09 VOSTFR [1080p]](https://uptobox.com/znl417o0yhcb)
- [[Refrain] Yagate Kimi ni Naru - 09 VOSTFR {ADN} [720p]](https://uptobox.com/bdzprra4srur)
- [[Refrain] Yagate Kimi ni Naru - 09 VOSTFR {ADN} [1080p]](https://uptobox.com/jp3mwbwvc4sg)
- [[Refrain] Yagate Kimi ni Naru - 10 VOSTFR [720p]](https://uptobox.com/6znrkjcczhu6)
- [[Refrain] Yagate Kimi ni Naru - 10 VOSTFR [1080p]](https://uptobox.com/89xij0m5zqq5)
- [[Refrain] Yagate Kimi ni Naru - 10 VOSTFR {ADN} [720p]](https://uptobox.com/nworeplxczqp)
- [[Refrain] Yagate Kimi ni Naru - 10 VOSTFR {ADN} [1080p]](https://uptobox.com/d8ez638dtp19)
- [[Refrain] Yagate Kimi ni Naru - 11 VOSTFR [720p]](https://uptobox.com/vz3j8mldgm7t)
- [[Refrain] Yagate Kimi ni Naru - 11 VOSTFR [1080p]](https://uptobox.com/bp7a3t3zibiw)
- [[Refrain] Yagate Kimi ni Naru - 11 VOSTFR {ADN} [720p]](https://uptobox.com/cg6tbfqelb0s)
- [[Refrain] Yagate Kimi ni Naru - 11 VOSTFR {ADN} [1080p]](https://uptobox.com/gtgy8lji651k)
- [[Refrain] Yagate Kimi ni Naru - 12 VOSTFR [720p]](https://uptobox.com/ev3iq61j9pbh)
- [[Refrain] Yagate Kimi ni Naru - 12 VOSTFR [1080p]](https://uptobox.com/h2bu2ltcz3f4)
- [[Refrain] Yagate Kimi ni Naru - 12 VOSTFR {ADN} [720p]](https://uptobox.com/cw3afgu6p0i9)
- [[Refrain] Yagate Kimi ni Naru - 12 VOSTFR {ADN} [1080p]](https://uptobox.com/5fieo1gwytz5)
- [[Refrain] Yagate Kimi ni Naru - 13 VOSTFR [720p]](https://uptobox.com/vbs12shsb4nv)
- [[Refrain] Yagate Kimi ni Naru - 13 VOSTFR [1080p]](https://uptobox.com/wtfdc4tcz0lc)
- [[Refrain] Yagate Kimi ni Naru - 13 VOSTFR {ADN} [720p]](https://uptobox.com/8op6w0bysoo6)
- [[Refrain] Yagate Kimi ni Naru - 13 VOSTFR {ADN} [1080p]](https://uptobox.com/31dmqhu5yp6c)
