# Karakuri Circus

![Karakuri Circus](https://cdn.myanimelist.net/images/anime/1529/95627l.jpg)

* Japanese: からくりサーカス

## Information

- Type: TV
- Episodes: 36
- Status: Currently Airing
- Aired: Oct 11, 2018 to ?
- Premiered: Fall 2018
- Broadcast: Thursdays at 22:30 (JST)
- Producers: Fuji Pacific Music Publishing, Twin Engine
- Licensors: None found, add some
- Studios: Studio VOLN
- Source: Manga
- Genres: Adventure, Mystery, Shounen
- Duration: 24 min. per ep.
- Rating: PG-13 - Teens 13 or older


## Links

- [[Refrain] Karakuri Circus - 01 VOSTFR [720p]](https://uptobox.com/fdmfxpwcz599)
- [[Refrain] Karakuri Circus - 01 VOSTFR [1080p]](https://uptobox.com/s30yy39dd0lz)
- [[Refrain] Karakuri Circus - 02 VOSTFR [720p]](https://uptobox.com/gxzg6vq6yrgu)
- [[Refrain] Karakuri Circus - 02 VOSTFR [1080p]](https://uptobox.com/fm7z7v8wetax)
- [[Refrain] Karakuri Circus - 03 VOSTFR [720p]](https://uptobox.com/7s54nzoma65z)
- [[Refrain] Karakuri Circus - 03 VOSTFR [1080p]](https://uptobox.com/wfmbho2elblo)
- [[Refrain] Karakuri Circus - 04 VOSTFR [720p]](https://uptobox.com/cvfa1sv7soix)
- [[Refrain] Karakuri Circus - 04 VOSTFR [1080p]](https://uptobox.com/r665078548bh)
- [[Refrain] Karakuri Circus - 05 VOSTFR [720p]](https://uptobox.com/711hc533cnp3)
- [[Refrain] Karakuri Circus - 05 VOSTFR [1080p]](https://uptobox.com/822z84sgbz2x)
- [[Refrain] Karakuri Circus - 06 VOSTFR [720p]](https://uptobox.com/3f79kuevnesm)
- [[Refrain] Karakuri Circus - 06 VOSTFR [1080p]](https://uptobox.com/zdkxlwd2phyw)
- [[Refrain] Karakuri Circus - 07 VOSTFR [720p]](https://uptobox.com/sio4q4bneruc)
- [[Refrain] Karakuri Circus - 07 VOSTFR [1080p]](https://uptobox.com/22f5xv9a76j7)
- [[Refrain] Karakuri Circus - 08 VOSTFR [720p]](https://uptobox.com/yzjm5d04k7vz)
- [[Refrain] Karakuri Circus - 08 VOSTFR [1080p]](https://uptobox.com/6h8cseay7nf1)
- [[Refrain] Karakuri Circus - 09 VOSTFR [720p]](https://uptobox.com/rrcpi91dof2r)
- [[Refrain] Karakuri Circus - 09 VOSTFR [1080p]](https://uptobox.com/bi00siotv4gu)
- [[Refrain] Karakuri Circus - 10 VOSTFR [720p]](https://uptobox.com/fth70ozrmbf2)
- [[Refrain] Karakuri Circus - 10 VOSTFR [1080p]](https://uptobox.com/umtnl2tnavwo)
- [[Refrain] Karakuri Circus - 11 VOSTFR [720p]](https://uptobox.com/u40g1i3wmujx)
- [[Refrain] Karakuri Circus - 11 VOSTFR [1080p]](https://uptobox.com/6pmf8kvgdd4g)
- [[Refrain] Karakuri Circus - 12 VOSTFR [720p]](https://uptobox.com/86omxnpijm3s)
- [[Refrain] Karakuri Circus - 12 VOSTFR [1080p]](https://uptobox.com/spc8g3p536ny)
- [[Refrain] Karakuri Circus - 13 VOSTFR [1080p]](https://uptobox.com/q6pfk7lxehn7)
- [[Refrain] Karakuri Circus - 13 VOSTFR [720p]](https://uptobox.com/4g6d0d7d6rr7)
- [[Refrain] Karakuri Circus - 14 VOSTFR [1080p]](https://uptobox.com/boyz5f5gqscb)
- [[Refrain] Karakuri Circus - 14 VOSTFR [720p]](https://uptobox.com/9rkkcs4i1tgw)
- [[Refrain] Karakuri Circus - 15 VOSTFR [1080p]](https://uptobox.com/ukhvulgpf5vn)
- [[Refrain] Karakuri Circus - 15 VOSTFR [720p]](https://uptobox.com/7uz31exn56se)
