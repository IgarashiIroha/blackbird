# Release the Spyce

![Release the Spyce](https://cdn.myanimelist.net/images/anime/1416/91403l.jpg)

* Japanese:  RELEASE THE SPYCE

## Information

 - Type: TV
 - Episodes: 12
 - Status: Finished Airing
 - Aired: Oct 7, 2018 to Dec 23, 2018
 - Premiered: Fall 2018
 - Broadcast: Sundays at 02:38 (JST)
 - Producers: Mainichi Broadcasting System, Pony Canyon, Half H.P Studio, KlockWorx, Good Smile Company, Tablier Communications, Kadokawa, Legs, Hawkeye 
 - Licensors: Sentai Filmworks 
 - Studios: Lay-duce 
 - Source: Original
 - Genres: Action, School
 - Duration: 24 min. per ep.
 - Rating: PG-13 - Teens 13 or older


## Links

