# Inazuma Eleven: Orion no Kokuin

![Inazuma Eleven: Orion no Kokuin](https://cdn.myanimelist.net/images/anime/1629/93729l.jpg)

* Japanese:  イナズマイレブン オリオンの刻印

## Information

 - Type: TV
 - Episodes: Unknown
 - Status: Currently Airing
 - Aired: Oct 5, 2018 to ?
 - Premiered: Fall 2018
 - Broadcast: Fridays at 17:55 (JST)
 - Producers: TV Tokyo, Dentsu 
 - Licensors: None found, add some 
 - Studios: OLM 
 - Source: Game
 - Genres: Sports
 - Duration: 23 min.
 - Rating: G - All Ages


## Links

