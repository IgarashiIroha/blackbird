# Shuudengo, Capsule Hotel de, Joushi ni Binetsu Tsutawaru Yoru.

![Shuudengo, Capsule Hotel de, Joushi ni Binetsu Tsutawaru Yoru.](https://cdn.myanimelist.net/images/anime/1418/93509l.jpg)

* Japanese:  終電後、カプセルホテルで、上司に微熱伝わる夜。

## Information

 - Type: TV
 - Episodes: 12
 - Status: Finished Airing
 - Aired: Oct 8, 2018 to Dec 24, 2018
 - Premiered: Fall 2018
 - Broadcast: Mondays at 01:00 (JST)
 - Producers: Studio Mausu, Namu Animation 
 - Licensors: None found, add some 
 - Studios: Suiseisha 
 - Source: Manga
 - Genres: Romance, Ecchi, Josei
 - Duration: 3 min. per ep.
 - Rating: R+ - Mild Nudity


## Links

