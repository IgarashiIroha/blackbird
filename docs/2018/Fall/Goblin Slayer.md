# Goblin Slayer

![Goblin Slayer](https://cdn.myanimelist.net/images/anime/1949/93415l.jpg)

* Japanese:  ゴブリンスレイヤー

## Information

 - Type: TV
 - Episodes: 12
 - Status: Finished Airing
 - Aired: Oct 7, 2018 to Dec 30, 2018
 - Premiered: Fall 2018
 - Broadcast: Sundays at 00:30 (JST)
 - Producers: Sony Pictures Entertainment, Frontier Works, AT-X, SoftBank Creative, Bushiroad, Hakuhodo DY Music & Pictures, Good Smile Film, Artist Management Office 
 - Licensors: Funimation 
 - Studios: White Fox 
 - Source: Light novel
 - Genres: Action, Adventure, Fantasy
 - Duration: 23 min. per ep.
 - Rating: R - 17+ (violence & profanity)


## Links
- [[Refrain] Goblin Slayer - 01 VOSTFR [720p]](https://uptobox.com/gg0ptpsi4ns7)
- [[Refrain] Goblin Slayer - 01 VOSTFR [1080p]](https://uptobox.com/2ox6s2qxwh88)
- [[Refrain] Goblin Slayer - 02 VOSTFR [720p]](https://uptobox.com/te76cgvolj3n)
- [[Refrain] Goblin Slayer - 02 VOSTFR [1080p]](https://uptobox.com/ya4qsv18tmdn)
- [[Refrain] Goblin Slayer - 03 VOSTFR [720p]](https://uptobox.com/p8316t83aobt)
- [[Refrain] Goblin Slayer - 03 VOSTFR [1080p]](https://uptobox.com/h3cdxb4ew7ks)
- [[Refrain] Goblin Slayer - 04 VOSTFR [720p]](https://uptobox.com/euyqn9m43wlp)
- [[Refrain] Goblin Slayer - 04 VOSTFR [1080p]](https://uptobox.com/gy8a399azh6u)
- [[Refrain] Goblin Slayer - 05 VOSTFR [720p]](https://uptobox.com/hy2e49jhoggb)
- [[Refrain] Goblin Slayer - 05 VOSTFR [1080p]](https://uptobox.com/xoqwo7ug6b34)
- [[Refrain] Goblin Slayer - 06 VOSTFR [720p]](https://uptobox.com/flsnbp4xq8s5)
- [[Refrain] Goblin Slayer - 06 VOSTFR [1080p]](https://uptobox.com/iiqya1h2cnd4)
- [[Refrain] Goblin Slayer - 07 VOSTFR [720p]](https://uptobox.com/nk1swixu61br)
- [[Refrain] Goblin Slayer - 07 VOSTFR [1080p]](https://uptobox.com/yeunk3hs62bq)
- [[Refrain] Goblin Slayer - 08 VOSTFR [720p]](https://uptobox.com/w3k2xb3789rh)
- [[Refrain] Goblin Slayer - 08 VOSTFR [1080p]](https://uptobox.com/87966mcolsau)
- [[Refrain] Goblin Slayer - 09 VOSTFR [720p]](https://uptobox.com/b8x6w697ns68)
- [[Refrain] Goblin Slayer - 09 VOSTFR [1080p]](https://uptobox.com/1glsqvm05jle)
- [[Refrain] Goblin Slayer - 10 VOSTFR [720p]](https://uptobox.com/4fp6v1zhtlbh)
- [[Refrain] Goblin Slayer - 10 VOSTFR [1080p]](https://uptobox.com/sxjop6nw4mrh)
- [[Refrain] Goblin Slayer - 11 VOSTFR [720p]](https://uptobox.com/p47iecrf9ow0)
- [[Refrain] Goblin Slayer - 11 VOSTFR [1080p]](https://uptobox.com/1mahb8rrkbe4)
- [[Refrain] Goblin Slayer - 12 VOSTFR [720p]](https://uptobox.com/cqyzjn9zq1gn)
- [[Refrain] Goblin Slayer - 12 VOSTFR [1080p]](https://uptobox.com/feulh51s4nmm)
