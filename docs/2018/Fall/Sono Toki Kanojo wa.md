# Sono Toki, Kanojo wa.

![Sono Toki, Kanojo wa.](https://cdn.myanimelist.net/images/anime/1908/93910l.jpg)

* Japanese:  その時、カノジョは。

## Information

 - Type: TV
 - Episodes: 12
 - Status: Finished Airing
 - Aired: Oct 7, 2018 to Dec 23, 2018
 - Premiered: Fall 2018
 - Broadcast: Sundays at 00:35 (JST)
 - Producers: None found, add some 
 - Licensors: None found, add some 
 - Studios: MASTER LIGHTS 
 - Source: Original
 - Genres: Drama, Romance
 - Duration: 3 min. per ep.
 - Rating: PG-13 - Teens 13 or older


## Links

