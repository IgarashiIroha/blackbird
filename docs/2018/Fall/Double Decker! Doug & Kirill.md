# Double Decker! Doug & Kirill

![Double Decker! Doug & Kirill](https://cdn.myanimelist.net/images/anime/1299/95783l.jpg)

* Japanese:  DOUBLE DECKER! ダグ＆キリル

## Information

 - Type: TV
 - Episodes: 13
 - Status: Finished Airing
 - Aired: Sep 30, 2018 to Dec 23, 2018
 - Premiered: Fall 2018
 - Broadcast: Sundays at 22:30 (JST)
 - Producers: Lantis 
 - Licensors: Funimation 
 - Studios: Sunrise 
 - Source: Original
 - Genres: Action, Sci-Fi, Comedy, Police
 - Duration: 24 min. per ep.
 - Rating: R - 17+ (violence & profanity)


## Links
- [[Refrain] Double Decker! Doug & Kirill - 01 VOSTFR [720p]](https://uptobox.com/cgf6jovpi30y)
- [[Refrain] Double Decker! Doug & Kirill - 01 VOSTFR [1080p]](https://uptobox.com/1kr6akbograc)
- [[Refrain] Double Decker! Doug & Kirill - 02 VOSTFR [720p]](https://uptobox.com/cybeupk8y5u2)
- [[Refrain] Double Decker! Doug & Kirill - 02 VOSTFR [1080p]](https://uptobox.com/850bocrn95wr)
- [[Refrain] Double Decker! Doug & Kirill - 03 VOSTFR [720p]](https://uptobox.com/mgg95fp6fj01)
- [[Refrain] Double Decker! Doug & Kirill - 03 VOSTFR [1080p]](https://uptobox.com/e4pu7t5nfaui)
- [[Refrain] Double Decker! Doug & Kirill - 04 VOSTFR [720p]](https://uptobox.com/sx1o6ft6s1om)
- [[Refrain] Double Decker! Doug & Kirill - 04 VOSTFR [1080p]](https://uptobox.com/36nc7y8eejad)
- [[Refrain] Double Decker! Doug & Kirill - 05 VOSTFR [720p]](https://uptobox.com/vx7u5l62r3v3)
- [[Refrain] Double Decker! Doug & Kirill - 05 VOSTFR [1080p]](https://uptobox.com/6toj9rigfphb)
- [[Refrain] Double Decker! Doug & Kirill - 06 VOSTFR [720p]](https://uptobox.com/hhu4ww3iqmdx)
- [[Refrain] Double Decker! Doug & Kirill - 06 VOSTFR [1080p]](https://uptobox.com/xwwaf9s1vx7i)
- [[Refrain] Double Decker! Doug & Kirill - 07 VOSTFR [720p]](https://uptobox.com/d68txosxa8lt)
- [[Refrain] Double Decker! Doug & Kirill - 07 VOSTFR [1080p]](https://uptobox.com/kok8kk967fti)
- [[Refrain] Double Decker! Doug & Kirill - 08 VOSTFR [720p]](https://uptobox.com/7vlzp9mcqr9p)
- [[Refrain] Double Decker! Doug & Kirill - 08 VOSTFR [1080p]](https://uptobox.com/aqclxv2r79se)
- [[Refrain] Double Decker! Doug & Kirill - 09 VOSTFR [720p]](https://uptobox.com/qm8s3cg00jfm)
- [[Refrain] Double Decker! Doug & Kirill - 09 VOSTFR [1080p]](https://uptobox.com/ecntwwgyexks)
- [[Refrain] Double Decker! Doug & Kirill - 10 VOSTFR [720p]](https://uptobox.com/fbdit80zcxsn)
- [[Refrain] Double Decker! Doug & Kirill - 10 VOSTFR [1080p]](https://uptobox.com/65jreiyhz0ri)
- [[Refrain] Double Decker! Doug & Kirill - 11 VOSTFR [720p]](https://uptobox.com/gl9t4s5ss4e6)
- [[Refrain] Double Decker! Doug & Kirill - 11 VOSTFR [1080p]](https://uptobox.com/9m6h8096yedr)
- [[Refrain] Double Decker! Doug & Kirill - 12 VOSTFR [720p]](https://uptobox.com/rkpcuzuu9rp1)
- [[Refrain] Double Decker! Doug & Kirill - 12 VOSTFR [1080p]](https://uptobox.com/fr22kxqfio5c)
- [[Refrain] Double Decker! Doug & Kirill - 13 VOSTFR [720p]](https://uptobox.com/pzl30qgh49mw)
- [[Refrain] Double Decker! Doug & Kirill - 13 VOSTFR [1080p]](https://uptobox.com/2c54poo1upen)
- [[Refrain] Double Decker! Doug & Kirill - 00 VOSTFR [720p]](https://uptobox.com/7kh4jf5aye1g)
- [[Refrain] Double Decker! Doug & Kirill - EX03 VOSTFR [720p]](https://uptobox.com/w4xlb9kk5ekb)
- [[Refrain] Double Decker! Doug & Kirill - EX03 VOSTFR [1080p]](https://uptobox.com/v6d8fjdnsdzp)
