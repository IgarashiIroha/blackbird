# Kitsune no Koe

![Kitsune no Koe](https://cdn.myanimelist.net/images/anime/1087/93424l.jpg)

* Japanese:  狐狸之声

## Information

 - Type: TV
 - Episodes: 12
 - Status: Finished Airing
 - Aired: Oct 5, 2018 to Dec 21, 2018
 - Premiered: Fall 2018
 - Broadcast: Fridays at 22:30 (JST)
 - Producers: Success Corp., Tavac, iQIYI 
 - Licensors: None found, add some 
 - Studios: Yumeta Company 
 - Source: Manga
 - Genres: Music
 - Duration: 13 min. per ep.
 - Rating: PG-13 - Teens 13 or older


## Links
- [[Refrain] Kitsune no Koe - 01 VOSTFR [720p]](https://uptobox.com/f5wto0v2oxsy)
- [[Refrain] Kitsune no Koe - 01 VOSTFR [1080p]](https://uptobox.com/gu0a8xne738g)
- [[Refrain] Kitsune no Koe - 02 VOSTFR [720p]](https://uptobox.com/1yn6g649tlvp)
- [[Refrain] Kitsune no Koe - 02 VOSTFR [1080p]](https://uptobox.com/wmggh87atj0n)
- [[Refrain] Kitsune no Koe - 03 VOSTFR [720p]](https://uptobox.com/lq1et2653l51)
- [[Refrain] Kitsune no Koe - 03 VOSTFR [1080p]](https://uptobox.com/g8jxjlqkrwmj)
- [[Refrain] Kitsune no Koe - 04 VOSTFR [720p]](https://uptobox.com/raelqoq6fq3y)
- [[Refrain] Kitsune no Koe - 04 VOSTFR [1080p]](https://uptobox.com/1hue7tgvmr99)
- [[Refrain] Kitsune no Koe - 05 VOSTFR [720p]](https://uptobox.com/6sa42raz3wba)
- [[Refrain] Kitsune no Koe - 05 VOSTFR [1080p]](https://uptobox.com/s8q8fy9yjqqq)
- [[Refrain] Kitsune no Koe - 06 VOSTFR [720p]](https://uptobox.com/1ybdnuz266fe)
- [[Refrain] Kitsune no Koe - 06 VOSTFR [1080p]](https://uptobox.com/1uzdezwl195s)
- [[Refrain] Kitsune no Koe - 07 VOSTFR [720p]](https://uptobox.com/ygf3u9l8dc67)
- [[Refrain] Kitsune no Koe - 07 VOSTFR [1080p]](https://uptobox.com/3829nx1o2842)
- [[Refrain] Kitsune no Koe - 08 VOSTFR [720p]](https://uptobox.com/disuyeoi3hqa)
- [[Refrain] Kitsune no Koe - 08 VOSTFR [1080p]](https://uptobox.com/058ja0p7imaz)
- [[Refrain] Kitsune no Koe - 09 VOSTFR [720p]](https://uptobox.com/v0pzzd1jhzs4)
- [[Refrain] Kitsune no Koe - 09 VOSTFR [1080p]](https://uptobox.com/q0apzj0esu21)
- [[Refrain] Kitsune no Koe - 10 VOSTFR [720p]](https://uptobox.com/uoi9ox9ihowq)
- [[Refrain] Kitsune no Koe - 10 VOSTFR [1080p]](https://uptobox.com/2tpuqnltbfoh)
- [[Refrain] Kitsune no Koe - 11 VOSTFR [720p]](https://uptobox.com/nx3711a65yp7)
- [[Refrain] Kitsune no Koe - 11 VOSTFR [1080p]](https://uptobox.com/48p9efwvdfyx)
- [[Refrain] Kitsune no Koe - 12 VOSTFR [720p]](https://uptobox.com/33vbv0e97st9)
- [[Refrain] Kitsune no Koe - 12 VOSTFR [1080p]](https://uptobox.com/6syypixvha2i)
