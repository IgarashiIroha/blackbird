# SSSS.Gridman

![SSSS.Gridman](https://cdn.myanimelist.net/images/anime/1973/95616l.jpg)

* Japanese:  SSSS.GRIDMAN

## Information

 - Type: TV
 - Episodes: 12
 - Status: Finished Airing
 - Aired: Oct 7, 2018 to Dec 23, 2018
 - Premiered: Fall 2018
 - Broadcast: Sundays at 00:00 (JST)
 - Producers: Mainichi Broadcasting System, Pony Canyon, Tsuburaya Productions, KlockWorx, Ultra Super Pictures, Graphinica, Docomo Anime Store, Fields, BS11 
 - Licensors: Funimation 
 - Studios: Trigger 
 - Source: Original
 - Genres: Action, Sci-Fi, Mecha
 - Duration: 23 min. per ep.
 - Rating: PG-13 - Teens 13 or older


## Links
- [[Refrain] SSSS Gridman - 01 VOSTFR [720p]](https://uptobox.com/fwu8t82s28br)
- [[Refrain] SSSS Gridman - 01 VOSTFR [1080p]](https://uptobox.com/l29zvcq1oq70)
- [[Refrain] SSSS Gridman - 02 VOSTFR [720p]](https://uptobox.com/lurcdl7nana0)
- [[Refrain] SSSS Gridman - 02 VOSTFR [1080p]](https://uptobox.com/3qweh3353u9q)
- [[Refrain] SSSS Gridman - 03 VOSTFR [720p]](https://uptobox.com/pb2188azyozq)
- [[Refrain] SSSS Gridman - 03 VOSTFR [1080p]](https://uptobox.com/e1qq6r48u8x0)
- [[Refrain] SSSS Gridman - 04 VOSTFR [720p]](https://uptobox.com/rcyl1lahh8zc)
- [[Refrain] SSSS Gridman - 04 VOSTFR [1080p]](https://uptobox.com/kqoxfj7bkion)
- [[Refrain] SSSS Gridman - 05 VOSTFR [720p]](https://uptobox.com/ktrg5e690pzx)
- [[Refrain] SSSS Gridman - 05 VOSTFR [1080p]](https://uptobox.com/jhifyillxu7c)
- [[Refrain] SSSS Gridman - 06 VOSTFR [720p]](https://uptobox.com/rrdlcrhaxpl4)
- [[Refrain] SSSS Gridman - 06 VOSTFR [1080p]](https://uptobox.com/eqfwim0nuw0p)
- [[Refrain] SSSS Gridman - 07 VOSTFR [720p]](https://uptobox.com/afx1ru67cybk)
- [[Refrain] SSSS Gridman - 07 VOSTFR [1080p]](https://uptobox.com/otin69g30sne)
- [[Refrain] SSSS Gridman - 08 VOSTFR [720p]](https://uptobox.com/7bylny5xlvh0)
- [[Refrain] SSSS Gridman - 08 VOSTFR [1080p]](https://uptobox.com/hqnhjdlwwg1y)
- [[Refrain] SSSS Gridman - 09 VOSTFR [720p]](https://uptobox.com/k335329vhdaq)
- [[Refrain] SSSS Gridman - 09 VOSTFR [1080p]](https://uptobox.com/x5ukij6kjbby)
- [[Refrain] SSSS Gridman - 10 VOSTFR [720p]](https://uptobox.com/p52ol723s5z7)
- [[Refrain] SSSS Gridman - 10 VOSTFR [1080p]](https://uptobox.com/7tjjkkf3e2lj)
- [[Refrain] SSSS Gridman - 11 VOSTFR [720p]](https://uptobox.com/pfd1jqft0kyc)
- [[Refrain] SSSS Gridman - 11 VOSTFR [1080p]](https://uptobox.com/e2xmcaa6byl5)
- [[Refrain] SSSS Gridman - 12 VOSTFR [720p]](https://uptobox.com/jz2jwkjd2e00)
- [[Refrain] SSSS Gridman - 12 VOSTFR [1080p]](https://uptobox.com/qxnb69axf686)
