# Senran Kagura Shinovi Master: Tokyo Youma-hen

![Senran Kagura Shinovi Master: Tokyo Youma-hen](https://cdn.myanimelist.net/images/anime/1358/93867l.jpg)

* Japanese:  閃乱カグラ SHINOVI MASTER -東京妖魔篇-

## Information

 - Type: TV
 - Episodes: 12
 - Status: Finished Airing
 - Aired: Oct 13, 2018 to Dec 29, 2018
 - Premiered: Fall 2018
 - Broadcast: Saturdays at 00:00 (JST)
 - Producers: Lantis, AT-X, Crunchyroll SC Anime Fund 
 - Licensors: Funimation 
 - Studios: TNK 
 - Source: Game
 - Genres: Action, Comedy, Ecchi, School
 - Duration: 23 min. per ep.
 - Rating: R+ - Mild Nudity


## Links
- [[Refrain] Senran Kagura Shinovi Master - Tokyo Youma-hen NC - 01 VOSTFR [720p]](https://uptobox.com/09nkhusf9uwe)
- [[Refrain] Senran Kagura Shinovi Master - Tokyo Youma-hen NC - 01 VOSTFR [1080p]](https://uptobox.com/1nymt2j1d1o1)
- [[Refrain] Senran Kagura Shinovi Master - Tokyo Youma-hen NC - 02 VOSTFR [720p]](https://uptobox.com/mwmbpugw91j2)
- [[Refrain] Senran Kagura Shinovi Master - Tokyo Youma-hen NC - 02 VOSTFR [1080p]](https://uptobox.com/w5k4xdia5kru)
- [[Refrain] Senran Kagura Shinovi Master - Tokyo Youma-hen NC - 03 VOSTFR [720p]](https://uptobox.com/giv4esgqx5st)
- [[Refrain] Senran Kagura Shinovi Master - Tokyo Youma-hen NC - 03 VOSTFR [1080p]](https://uptobox.com/nbgtzl0cwydc)
- [[Refrain] Senran Kagura Shinovi Master - Tokyo Youma-hen NC - 04 VOSTFR [720p]](https://uptobox.com/neoozk3kggqy)
- [[Refrain] Senran Kagura Shinovi Master - Tokyo Youma-hen NC - 04 VOSTFR [1080p]](https://uptobox.com/950swkch0hbx)
- [[Refrain] Senran Kagura Shinovi Master - Tokyo Youma-hen NC - 05 VOSTFR [720p]](https://uptobox.com/dfidnkn5kawx)
- [[Refrain] Senran Kagura Shinovi Master - Tokyo Youma-hen NC - 05 VOSTFR [1080p]](https://uptobox.com/jg3uiqjk9gpu)
- [[Refrain] Senran Kagura Shinovi Master - Tokyo Youma-hen NC - 06 VOSTFR [720p]](https://uptobox.com/bsxyq00igqzh)
- [[Refrain] Senran Kagura Shinovi Master - Tokyo Youma-hen NC - 06 VOSTFR [1080p]](https://uptobox.com/6crxrat8uinb)
- [[Refrain] Senran Kagura Shinovi Master - Tokyo Youma-hen NC - 07 VOSTFR [720p]](https://uptobox.com/rxr4u23dlw4o)
- [[Refrain] Senran Kagura Shinovi Master - Tokyo Youma-hen NC - 07 VOSTFR [1080p]](https://uptobox.com/avyae7cjyavs)
- [[Refrain] Senran Kagura Shinovi Master - Tokyo Youma-hen NC - 08 VOSTFR [720p]](https://uptobox.com/i5djwrlh0kr5)
- [[Refrain] Senran Kagura Shinovi Master - Tokyo Youma-hen NC - 08 VOSTFR [1080p]](https://uptobox.com/mtezp9eyy9f2)
- [[Refrain] Senran Kagura Shinovi Master - Tokyo Youma-hen NC - 09 VOSTFR [720p]](https://uptobox.com/h6fnqhexn5x1)
- [[Refrain] Senran Kagura Shinovi Master - Tokyo Youma-hen NC - 09 VOSTFR [1080p]](https://uptobox.com/pjsfl8ygm918)
- [[Refrain] Senran Kagura Shinovi Master - Tokyo Youma-hen NC - 10 VOSTFR [720p]](https://uptobox.com/f7g1asotdkbb)
- [[Refrain] Senran Kagura Shinovi Master - Tokyo Youma-hen NC - 10 VOSTFR [1080p]](https://uptobox.com/cayz4ws5btn7)
- [[Refrain] Senran Kagura Shinovi Master - Tokyo Youma-hen NC - 11 VOSTFR [720p]](https://uptobox.com/2hvrmb1xc2hz)
- [[Refrain] Senran Kagura Shinovi Master - Tokyo Youma-hen NC - 11 VOSTFR [1080p]](https://uptobox.com/j3q3t7fu42wj)
- [[Refrain] Senran Kagura Shinovi Master - Tokyo Youma-hen NC - 12 VOSTFR [720p]](https://uptobox.com/7fg7e9e2z4hj)
- [[Refrain] Senran Kagura Shinovi Master - Tokyo Youma-hen NC - 12 VOSTFR [1080p]](https://uptobox.com/3x2hau5990m8)
