# Cardcaptor Sakura: Clear Card-hen

![Cardcaptor Sakura: Clear Card-hen](https://cdn.myanimelist.net/images/anime/1467/90338l.jpg)

* Japanese:  カードキャプターさくら クリアカード編

## Information

 - Type: TV
 - Episodes: 22
 - Status: Finished Airing
 - Aired: Jan 7, 2018 to Jun 10, 2018
 - Premiered: Winter 2018
 - Broadcast: Sundays at 07:30 (JST)
 - Producers: NHK, Kodansha, NHK Enterprises
 - Licensors: Funimation
 - Studios: Madhouse
 - Source: Manga
 - Genres: Adventure, Comedy, Fantasy, Magic, Romance, Shoujo
 - Duration: 25 min. per ep.
 - Rating: PG - Children


## Links

- [[Refrain] Cardcaptor Sakura - Clear Card-hen (2017) - 01 VOSTFR [720p]](https://uptobox.com/fg9z3skayhok)
- [[Refrain] Cardcaptor Sakura - Clear Card-hen (2017) - 01 VOSTFR [1080p]](https://uptobox.com/0xklod9ab6bj)
- [[Refrain] Cardcaptor Sakura - Clear Card-hen (2017) - 02 VOSTFR [720p]](https://uptobox.com/ybc0k2lgv8dy)
- [[Refrain] Cardcaptor Sakura - Clear Card-hen (2017) - 02 VOSTFR [1080p]](https://uptobox.com/85p7n2s4ta9u)
- [[Refrain] Cardcaptor Sakura - Clear Card-hen (2017) - 03 VOSTFR [720p]](https://uptobox.com/14kileg75vaz)
- [[Refrain] Cardcaptor Sakura - Clear Card-hen (2017) - 03 VOSTFR [1080p]](https://uptobox.com/y21h82uipd3h)
- [[Refrain] Cardcaptor Sakura - Clear Card-hen (2017) - 04 VOSTFR [720p]](https://uptobox.com/wtf0846gb2g7)
- [[Refrain] Cardcaptor Sakura - Clear Card-hen (2017) - 04 VOSTFR [1080p]](https://uptobox.com/y3oo8rrhyydh)
- [[Refrain] Cardcaptor Sakura - Clear Card-hen (2017) - 05 VOSTFR [720p]](https://uptobox.com/yqhiho183jia)
- [[Refrain] Cardcaptor Sakura - Clear Card-hen (2017) - 05 VOSTFR [1080p]](https://uptobox.com/bis9qy9slb44)
- [[Refrain] Cardcaptor Sakura - Clear Card-hen (2017) - 06 VOSTFR [720p]](https://uptobox.com/2i3jme4cews2)
- [[Refrain] Cardcaptor Sakura - Clear Card-hen (2017) - 06 VOSTFR [1080p]](https://uptobox.com/bpswb5j7h0jm)
- [[Refrain] Cardcaptor Sakura - Clear Card-hen (2017) - 07 VOSTFR [720p]](https://uptobox.com/qme8sdqy2dku)
- [[Refrain] Cardcaptor Sakura - Clear Card-hen (2017) - 07 VOSTFR [1080p]](https://uptobox.com/jw1im9zpsycp)
- [[Refrain] Cardcaptor Sakura - Clear Card-hen (2017) - 08 VOSTFR [720p]](https://uptobox.com/g2p8wdp4yjft)
- [[Refrain] Cardcaptor Sakura - Clear Card-hen (2017) - 08 VOSTFR [1080p]](https://uptobox.com/wdzqmwo7dpwu)
- [[Refrain] Cardcaptor Sakura - Clear Card-hen (2017) - 09 VOSTFR [720p]](https://uptobox.com/17guagrc0gti)
- [[Refrain] Cardcaptor Sakura - Clear Card-hen (2017) - 09 VOSTFR [1080p]](https://uptobox.com/meaf54xiauwj)
- [[Refrain] Cardcaptor Sakura - Clear Card-hen (2017) - 10 VOSTFR [720p]](https://uptobox.com/zc8uovyh4l4v)
- [[Refrain] Cardcaptor Sakura - Clear Card-hen (2017) - 10 VOSTFR [1080p]](https://uptobox.com/xndjaoznpkx6)
- [[Refrain] Cardcaptor Sakura - Clear Card-hen (2017) - 11 VOSTFR [720p]](https://uptobox.com/9ukm0beys4l5)
- [[Refrain] Cardcaptor Sakura - Clear Card-hen (2017) - 11 VOSTFR [1080p]](https://uptobox.com/nluoapa0qioc)
- [[Refrain] Cardcaptor Sakura - Clear Card-hen (2017) - 12 VOSTFR [720p]](https://uptobox.com/0ani1k0juc9a)
- [[Refrain] Cardcaptor Sakura - Clear Card-hen (2017) - 12 VOSTFR [1080p]](https://uptobox.com/ddtmlhgzqreu)
- [[Refrain] Cardcaptor Sakura - Clear Card-hen (2017) - 13 VOSTFR [720p]](https://uptobox.com/zy0cifz682hf)
- [[Refrain] Cardcaptor Sakura - Clear Card-hen (2017) - 13 VOSTFR [1080p]](https://uptobox.com/1n6aa2ucvkq3)
- [[Refrain] Cardcaptor Sakura - Clear Card-hen (2017) - 14 VOSTFR [720p]](https://uptobox.com/qvlyh81gseuw)
- [[Refrain] Cardcaptor Sakura - Clear Card-hen (2017) - 14 VOSTFR [1080p]](https://uptobox.com/fn8edobe0lom)
- [[Refrain] Cardcaptor Sakura - Clear Card-hen (2017) - 15 VOSTFR [720p]](https://uptobox.com/zpdzswy23i7i)
- [[Refrain] Cardcaptor Sakura - Clear Card-hen (2017) - 15 VOSTFR [1080p]](https://uptobox.com/1hv2rdtn25jd)
- [[Refrain] Cardcaptor Sakura - Clear Card-hen (2017) - 16 VOSTFR [720p]](https://uptobox.com/mfzu7td0qrdq)
- [[Refrain] Cardcaptor Sakura - Clear Card-hen (2017) - 16 VOSTFR [1080p]](https://uptobox.com/zrassw0cglni)
- [[Refrain] Cardcaptor Sakura - Clear Card-hen (2017) - 17 VOSTFR [720p]](https://uptobox.com/7e8h605x6sih)
- [[Refrain] Cardcaptor Sakura - Clear Card-hen (2017) - 17 VOSTFR [1080p]](https://uptobox.com/fm8c3idx8amg)
- [[Refrain] Cardcaptor Sakura - Clear Card-hen (2017) - 18 VOSTFR [720p]](https://uptobox.com/uiogdnc5bd2d)
- [[Refrain] Cardcaptor Sakura - Clear Card-hen (2017) - 18 VOSTFR [1080p]](https://uptobox.com/bfz82i7nrlyo)
- [[Refrain] Cardcaptor Sakura - Clear Card-hen (2017) - 19 VOSTFR [720p]](https://uptobox.com/sz9f7dk5wdyj)
- [[Refrain] Cardcaptor Sakura - Clear Card-hen (2017) - 19 VOSTFR [1080p]](https://uptobox.com/p9c7uxkz6vo3)
- [[Refrain] Cardcaptor Sakura - Clear Card-hen (2017) - 20 VOSTFR [720p]](https://uptobox.com/joouompqvu6c)
- [[Refrain] Cardcaptor Sakura - Clear Card-hen (2017) - 20 VOSTFR [1080p]](https://uptobox.com/lf8uwwa29w3v)
- [[Refrain] Cardcaptor Sakura - Clear Card-hen (2017) - 21 VOSTFR [720p]](https://uptobox.com/34a52tbso5ig)
- [[Refrain] Cardcaptor Sakura - Clear Card-hen (2017) - 21 VOSTFR [1080p]](https://uptobox.com/bzhz5bv50s0d)
- [[Refrain] Cardcaptor Sakura - Clear Card-hen (2017) - 22 VOSTFR [720p]](https://uptobox.com/21pwyzccpftl)
- [[Refrain] Cardcaptor Sakura - Clear Card-hen (2017) - 22 VOSTFR [1080p]](https://uptobox.com/nze6nc0jx7c1)
- [[Refrain] Cardcaptor Sakura - Clear Card-hen - 01 VOSTFR {ADN} [720p]](https://uptobox.com/n19b64pcvn6q)
- [[Refrain] Cardcaptor Sakura - Clear Card-hen - 01 VOSTFR {ADN} [1080p]](https://uptobox.com/j9j5b0418wze)
- [[Refrain] Cardcaptor Sakura - Clear Card-hen - 02 VOSTFR {ADN} [720p]](https://uptobox.com/jk94jsjha1q7)
- [[Refrain] Cardcaptor Sakura - Clear Card-hen - 02 VOSTFR {ADN} [1080p]](https://uptobox.com/h4ayc5ejowdx)
- [[Refrain] Cardcaptor Sakura - Clear Card-hen - 03 VOSTFR {ADN} [720p]](https://uptobox.com/rb274ekzsx2r)
- [[Refrain] Cardcaptor Sakura - Clear Card-hen - 03 VOSTFR {ADN} [1080p]](https://uptobox.com/mlu1ot6op77y)
- [[Refrain] Cardcaptor Sakura - Clear Card-hen - 04 VOSTFR {ADN} [720p]](https://uptobox.com/yrf8i73dvs44)
- [[Refrain] Cardcaptor Sakura - Clear Card-hen - 04 VOSTFR {ADN} [1080p]](https://uptobox.com/3roatlmvvvw7)
- [[Refrain] Cardcaptor Sakura - Clear Card-hen - 05 VOSTFR {ADN} [720p]](https://uptobox.com/p3ur4l2njbur)
- [[Refrain] Cardcaptor Sakura - Clear Card-hen - 05 VOSTFR {ADN} [1080p]](https://uptobox.com/ap4ftue54va0)
- [[Refrain] Cardcaptor Sakura - Clear Card-hen - 06 VOSTFR {ADN} [720p]](https://uptobox.com/gczw7omlrgl9)
- [[Refrain] Cardcaptor Sakura - Clear Card-hen - 06 VOSTFR {ADN} [1080p]](https://uptobox.com/tn6b20fa8m3m)
- [[Refrain] Cardcaptor Sakura - Clear Card-hen - 07 VOSTFR {ADN} [720p]](https://uptobox.com/xkvmf7wtc14g)
- [[Refrain] Cardcaptor Sakura - Clear Card-hen - 07 VOSTFR {ADN} [1080p]](https://uptobox.com/78xxt12adren)
- [[Refrain] Cardcaptor Sakura - Clear Card-hen - 08 VOSTFR {ADN} [720p]](https://uptobox.com/zwtozlk5jw2y)
- [[Refrain] Cardcaptor Sakura - Clear Card-hen - 08 VOSTFR {ADN} [1080p]](https://uptobox.com/myo3uqjovlj0)
- [[Refrain] Cardcaptor Sakura - Clear Card-hen - 09 VOSTFR {ADN} [720p]](https://uptobox.com/rfgwx7me39qq)
- [[Refrain] Cardcaptor Sakura - Clear Card-hen - 09 VOSTFR {ADN} [1080p]](https://uptobox.com/bi4g1rga11yu)
- [[Refrain] Cardcaptor Sakura - Clear Card-hen - 10 VOSTFR {ADN} [720p]](https://uptobox.com/yvanvfvld906)
- [[Refrain] Cardcaptor Sakura - Clear Card-hen - 10 VOSTFR {ADN} [1080p]](https://uptobox.com/09mqm6bhzxhg)
- [[Refrain] Cardcaptor Sakura - Clear Card-hen - 11 VOSTFR {ADN} [720p]](https://uptobox.com/5idnyfmn04rj)
- [[Refrain] Cardcaptor Sakura - Clear Card-hen - 11 VOSTFR {ADN} [1080p]](https://uptobox.com/s6mlpes4ca4t)
- [[Refrain] Cardcaptor Sakura - Clear Card-hen - 12 VOSTFR {ADN} [720p]](https://uptobox.com/zittv2ll43qo)
- [[Refrain] Cardcaptor Sakura - Clear Card-hen - 12 VOSTFR {ADN} [1080p]](https://uptobox.com/syywrqrwlede)
- [[Refrain] Cardcaptor Sakura - Clear Card-hen - 13 VOSTFR {ADN} [720p]](https://uptobox.com/h0ew4pijn4eh)
- [[Refrain] Cardcaptor Sakura - Clear Card-hen - 13 VOSTFR {ADN} [1080p]](https://uptobox.com/sfxq9mrbhowq)
- [[Refrain] Cardcaptor Sakura - Clear Card-hen - 14 VOSTFR {ADN} [720p]](https://uptobox.com/y97fpxgenxox)
- [[Refrain] Cardcaptor Sakura - Clear Card-hen - 14 VOSTFR {ADN} [1080p]](https://uptobox.com/h1qbsz2li03w)
- [[Refrain] Cardcaptor Sakura - Clear Card-hen - 15 VOSTFR {ADN} [720p]](https://uptobox.com/2lh661ewcbdr)
- [[Refrain] Cardcaptor Sakura - Clear Card-hen - 15 VOSTFR {ADN} [1080p]](https://uptobox.com/wbvvq00gt6hn)
- [[Refrain] Cardcaptor Sakura - Clear Card-hen - 16 VOSTFR {ADN} [720p]](https://uptobox.com/cxx35atz0070)
- [[Refrain] Cardcaptor Sakura - Clear Card-hen - 16 VOSTFR {ADN} [1080p]](https://uptobox.com/91y9zb89dvjw)
- [[Refrain] Cardcaptor Sakura - Clear Card-hen - 17 VOSTFR {ADN} [720p]](https://uptobox.com/yyvr56mhtx3d)
- [[Refrain] Cardcaptor Sakura - Clear Card-hen - 17 VOSTFR {ADN} [1080p]](https://uptobox.com/p0477l5vbw9s)
- [[Refrain] Cardcaptor Sakura - Clear Card-hen - 18 VOSTFR {ADN} [720p]](https://uptobox.com/5ntcqes6lv8o)
- [[Refrain] Cardcaptor Sakura - Clear Card-hen - 18 VOSTFR {ADN} [1080p]](https://uptobox.com/ej6me30dl9n3)
- [[Refrain] Cardcaptor Sakura - Clear Card-hen - 19 VOSTFR {ADN} [720p]](https://uptobox.com/fdzqvyjptkeq)
- [[Refrain] Cardcaptor Sakura - Clear Card-hen - 19 VOSTFR {ADN} [1080p]](https://uptobox.com/dydvhigrzajp)
- [[Refrain] Cardcaptor Sakura - Clear Card-hen - 20 VOSTFR {ADN} [720p]](https://uptobox.com/bjm40jfk2e0z)
- [[Refrain] Cardcaptor Sakura - Clear Card-hen - 20 VOSTFR {ADN} [1080p]](https://uptobox.com/kqprzed99w8m)
- [[Refrain] Cardcaptor Sakura - Clear Card-hen - 21 VOSTFR {ADN} [720p]](https://uptobox.com/3gfv0hxnz9y2)
- [[Refrain] Cardcaptor Sakura - Clear Card-hen - 21 VOSTFR {ADN} [1080p]](https://uptobox.com/sw2bdi5f1knc)
- [[Refrain] Cardcaptor Sakura - Clear Card-hen - 22 VOSTFR {ADN} [720p]](https://uptobox.com/y83agxkw6tj0)
- [[Refrain] Cardcaptor Sakura - Clear Card-hen - 22 VOSTFR {ADN} [1080p]](https://uptobox.com/jjev86vssxj6)
- [[Refrain] Cardcaptor Sakura - Clear Card-hen Prologue - Sakura to Futatsu no Kuma OAV VOSTFR {ADN} [720p]](https://uptobox.com/2nnhgqifzn8v)
- [[Refrain] Cardcaptor Sakura - Clear Card-hen Prologue - Sakura to Futatsu no Kuma OAV VOSTFR {ADN} [1080p]](https://uptobox.com/6yh8hu8kgqq5)
