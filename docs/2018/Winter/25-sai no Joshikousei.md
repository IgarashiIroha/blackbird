# 25-sai no Joshikousei

![25-sai no Joshikousei](https://cdn.myanimelist.net/images/anime/12/89091l.jpg)

* Japanese:  25歳の女子高生

## Information

 - Type: TV
 - Episodes: 12
 - Status: Finished Airing
 - Aired: Jan 8, 2018 to Mar 26, 2018
 - Premiered: Winter 2018
 - Broadcast: Mondays at 01:00 (JST)
 - Producers: Cosmic Ray, Picante Circus, Suiseisha, Tavac 
 - Licensors: None found, add some 
 - Studios: Lilix 
 - Source: Web manga
 - Genres: Romance, Ecchi, School
 - Duration: 6 min. per ep.
 - Rating: R+ - Mild Nudity


## Links

