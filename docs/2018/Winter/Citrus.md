# Citrus

![Citrus](https://cdn.myanimelist.net/images/anime/11/89985l.jpg)

* Japanese:  シトラス

## Information

 - Type: TV
 - Episodes: 12
 - Status: Finished Airing
 - Aired: Jan 6, 2018 to Mar 24, 2018
 - Premiered: Winter 2018
 - Broadcast: Saturdays at 23:30 (JST)
 - Producers: Sotsu, Lantis, AT-X, KlockWorx, Happinet Pictures, BS Fuji, Tokyo MX, Infinite, Children&#039;s Playground Entertainment, Ichijinsha, Crunchyroll SC Anime Fund
 - Licensors: Funimation
 - Studios: Passione
 - Source: Manga
 - Genres: Drama, Romance, School, Shoujo Ai
 - Duration: 23 min. per ep.
 - Rating: PG-13 - Teens 13 or older


## Links

- [[Refrain] Citrus - 01 VOSTFR [720p]](https://uptobox.com/i9sz8674nt2r)
- [[Refrain] Citrus - 01 VOSTFR [1080p]](https://uptobox.com/ct4c3kfrosz3)
- [[Refrain] Citrus - 02 VOSTFR [720p]](https://uptobox.com/nah8h9vqiqo6)
- [[Refrain] Citrus - 02 VOSTFR [1080p]](https://uptobox.com/0b4jw11nrvcs)
- [[Refrain] Citrus - 03 VOSTFR [720p]](https://uptobox.com/tsigf4rkl7yq)
- [[Refrain] Citrus - 03 VOSTFR [1080p]](https://uptobox.com/uoeei7aaewjw)
- [[Refrain] Citrus - 04 VOSTFR [720p]](https://uptobox.com/4160t8pqux01)
- [[Refrain] Citrus - 04 VOSTFR [1080p]](https://uptobox.com/fzemhku10efg)
- [[Refrain] Citrus - 05 VOSTFR [720p]](https://uptobox.com/ndnay0rv2ekf)
- [[Refrain] Citrus - 05 VOSTFR [1080p]](https://uptobox.com/5ipvsbbwwjow)
- [[Refrain] Citrus - 06 VOSTFR [720p]](https://uptobox.com/ia9g90beawk8)
- [[Refrain] Citrus - 06 VOSTFR [1080p]](https://uptobox.com/h1rvu4lao48d)
- [[Refrain] Citrus - 07 VOSTFR [720p]](https://uptobox.com/v1v4pbfzwoif)
- [[Refrain] Citrus - 07 VOSTFR [1080p]](https://uptobox.com/outmxe5w1coz)
- [[Refrain] Citrus - 08 VOSTFR [720p]](https://uptobox.com/3mn238bqa2il)
- [[Refrain] Citrus - 08 VOSTFR [1080p]](https://uptobox.com/ujogirknirme)
- [[Refrain] Citrus - 09 VOSTFR [720p]](https://uptobox.com/mwzlype2nhjh)
- [[Refrain] Citrus - 09 VOSTFR [1080p]](https://uptobox.com/7g842enhwl5t)
- [[Refrain] Citrus - 10 VOSTFR [720p]](https://uptobox.com/t32y5zl3zk1v)
- [[Refrain] Citrus - 10 VOSTFR [1080p]](https://uptobox.com/cimhzr88emuz)
- [[Refrain] Citrus - 11 VOSTFR [720p]](https://uptobox.com/owrrtp2n50vf)
- [[Refrain] Citrus - 11 VOSTFR [1080p]](https://uptobox.com/apr52fom4nak)
- [[Refrain] Citrus - 12 VOSTFR [720p]](https://uptobox.com/gnkae65p6nkf)
- [[Refrain] Citrus - 12 VOSTFR [1080p]](https://uptobox.com/0i8aek7g6flo)
