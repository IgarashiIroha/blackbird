# Dagashi Kashi 2

![Dagashi Kashi 2](https://cdn.myanimelist.net/images/anime/1649/90141l.jpg)

* Japanese:  だがしかし2

## Information

 - Type: TV
 - Episodes: 12
 - Status: Finished Airing
 - Aired: Jan 12, 2018 to Mar 30, 2018
 - Premiered: Winter 2018
 - Broadcast: Fridays at 02:28 (JST)
 - Producers: Pony Canyon, TBS, Shogakukan, Lawson, Pony Canyon Enterprise
 - Licensors: Funimation
 - Studios: Tezuka Productions
 - Source: Manga
 - Genres: Slice of Life, Comedy, Shounen
 - Duration: 12 min. per ep.
 - Rating: PG-13 - Teens 13 or older


## Links

- [[Refrain] Dagashi Kashi S2 - 01 VOSTFR [720p]](https://uptobox.com/it52cucej2v5)
- [[Refrain] Dagashi Kashi S2 - 01 VOSTFR [1080p]](https://uptobox.com/7skepc83bl4x)
- [[Refrain] Dagashi Kashi S2 - 02 VOSTFR [720p]](https://uptobox.com/tpag6ewaotsr)
- [[Refrain] Dagashi Kashi S2 - 02 VOSTFR [1080p]](https://uptobox.com/h09a7pk3rtr6)
- [[Refrain] Dagashi Kashi S2 - 03 VOSTFR [720p]](https://uptobox.com/y2wpj56t7jhd)
- [[Refrain] Dagashi Kashi S2 - 03 VOSTFR [1080p]](https://uptobox.com/9huts7xwhyel)
- [[Refrain] Dagashi Kashi S2 - 04 VOSTFR [720p]](https://uptobox.com/n0elt7whi7c8)
- [[Refrain] Dagashi Kashi S2 - 04 VOSTFR [1080p]](https://uptobox.com/03qluem8rzna)
- [[Refrain] Dagashi Kashi S2 - 05 VOSTFR [720p]](https://uptobox.com/kw8kaxdqr8aa)
- [[Refrain] Dagashi Kashi S2 - 05 VOSTFR [1080p]](https://uptobox.com/cgyri4c026ee)
- [[Refrain] Dagashi Kashi S2 - 06 VOSTFR [720p]](https://uptobox.com/fuzs42nceld4)
- [[Refrain] Dagashi Kashi S2 - 06 VOSTFR [1080p]](https://uptobox.com/msig8z80w9qh)
- [[Refrain] Dagashi Kashi S2 - 07 VOSTFR [720p]](https://uptobox.com/by0b5ftjuosn)
- [[Refrain] Dagashi Kashi S2 - 07 VOSTFR [1080p]](https://uptobox.com/p8fp8b0g9ieg)
- [[Refrain] Dagashi Kashi S2 - 08 VOSTFR [720p]](https://uptobox.com/tp1rjyjhlrec)
- [[Refrain] Dagashi Kashi S2 - 08 VOSTFR [1080p]](https://uptobox.com/f9old80e0i5i)
- [[Refrain] Dagashi Kashi S2 - 09 VOSTFR [720p]](https://uptobox.com/zdr21ufkrfi1)
- [[Refrain] Dagashi Kashi S2 - 09 VOSTFR [1080p]](https://uptobox.com/4rs0nyk4ksp6)
- [[Refrain] Dagashi Kashi S2 - 10 VOSTFR [720p]](https://uptobox.com/20mr6diro96z)
- [[Refrain] Dagashi Kashi S2 - 10 VOSTFR [1080p]](https://uptobox.com/hchtln7lm461)
- [[Refrain] Dagashi Kashi S2 - 11 VOSTFR [720p]](https://uptobox.com/159b0m2yr09x)
- [[Refrain] Dagashi Kashi S2 - 11 VOSTFR [1080p]](https://uptobox.com/co978cyvqgz8)
- [[Refrain] Dagashi Kashi S2 - 12 VOSTFR [720p]](https://uptobox.com/0gnknst6cmd1)
- [[Refrain] Dagashi Kashi S2 - 12 VOSTFR [1080p]](https://uptobox.com/8trpu0qopbbn)
