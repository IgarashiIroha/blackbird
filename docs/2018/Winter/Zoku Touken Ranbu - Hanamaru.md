# Zoku Touken Ranbu: Hanamaru

![Zoku Touken Ranbu: Hanamaru](https://cdn.myanimelist.net/images/anime/5/89579l.jpg)

* Japanese:  続 刀剣乱舞-花丸-

## Information

 - Type: TV
 - Episodes: 12
 - Status: Finished Airing
 - Aired: Jan 8, 2018 to Mar 26, 2018
 - Premiered: Winter 2018
 - Broadcast: Mondays at 00:00 (JST)
 - Producers: Marvelous, Nitroplus, TOHO animation, Good Smile Company, DMM.com 
 - Licensors: Funimation 
 - Studios: Doga Kobo 
 - Source: Game
 - Genres: Action, Comedy, Drama, Fantasy, Historical, Slice of Life
 - Duration: 24 min. per ep.
 - Rating: PG-13 - Teens 13 or older


## Links

