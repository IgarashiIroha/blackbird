# Nanatsu no Bitoku

![Nanatsu no Bitoku](https://cdn.myanimelist.net/images/anime/9/89129l.jpg)

* Japanese:  七つの美徳

## Information

 - Type: TV
 - Episodes: 10
 - Status: Finished Airing
 - Aired: Jan 27, 2018 to Mar 31, 2018
 - Premiered: Winter 2018
 - Broadcast: Saturdays at 00:25 (JST)
 - Producers: Hobby Japan 
 - Licensors: Sentai Filmworks 
 - Studios: Bridge 
 - Source: Other
 - Genres: Ecchi, Fantasy
 - Duration: 5 min. per ep.
 - Rating: R+ - Mild Nudity


## Links

