# Hug tto! Precure

![Hug tto! Precure](https://cdn.myanimelist.net/images/anime/1220/92883l.jpg)

* Japanese:  HuGっと！プリキュア

## Information

 - Type: TV
 - Episodes: 49
 - Status: Currently Airing
 - Aired: Feb 4, 2018 to Jan 27, 2019
 - Premiered: Winter 2018
 - Broadcast: Sundays at 08:30 (JST)
 - Producers: Asahi Broadcasting, ABC Animation 
 - Licensors: None found, add some 
 - Studios: Toei Animation 
 - Source: Original
 - Genres: Action, Magic, Fantasy, Shoujo
 - Duration: 24 min. per ep.
 - Rating: G - All Ages


## Links

