# Fate/Extra: Last Encore

![Fate/Extra: Last Encore](https://cdn.myanimelist.net/images/anime/1122/90836l.jpg)

* Japanese:  Fate/EXTRA Last Encore

## Information

 - Type: TV
 - Episodes: 10
 - Status: Finished Airing
 - Aired: Jan 28, 2018 to Apr 1, 2018
 - Premiered: Winter 2018
 - Broadcast: Sundays at 00:00 (JST)
 - Producers: Aniplex, Marvelous, Notes
 - Licensors: None found, add some
 - Studios: Shaft
 - Source: Game
 - Genres: Action, Fantasy, Magic
 - Duration: 24 min. per ep.
 - Rating: R - 17+ (violence & profanity)


## Links

- [[Refrain] FateEXTRA Last Encore - 01 VF VOSTFR [720p]](https://uptobox.com/s0e4o3qyjrw2)
- [[Refrain] FateEXTRA Last Encore - 02 VF VOSTFR [720p]](https://uptobox.com/cbzau3nm22dc)
- [[Refrain] FateEXTRA Last Encore - 03 VF VOSTFR [720p]](https://uptobox.com/rysqx6zk6n9q)
- [[Refrain] FateEXTRA Last Encore - 04 VF VOSTFR [720p]](https://uptobox.com/qf2o0h6ygk55)
- [[Refrain] FateEXTRA Last Encore - 05 VF VOSTFR [720p]](https://uptobox.com/5zbtlj2p2el2)
- [[Refrain] FateEXTRA Last Encore - 06 VF VOSTFR [720p]](https://uptobox.com/dpsstn3wt2k7)
- [[Refrain] FateEXTRA Last Encore - 07 VF VOSTFR [720p]](https://uptobox.com/mfk419qwon37)
- [[Refrain] FateEXTRA Last Encore - 08 VF VOSTFR [720p]](https://uptobox.com/hmxlefedykp5)
- [[Refrain] FateEXTRA Last Encore - 09 VF VOSTFR [720p]](https://uptobox.com/i31fiefhwqcq)
- [[Refrain] FateEXTRA Last Encore - 10 VF VOSTFR [720p]](https://uptobox.com/u2ncyro6hl4r)
- [[Refrain] FateEXTRA Last Encore - 11 VF VOSTFR [720p]](https://uptobox.com/m4jp4v5einwo)
- [[Refrain] FateEXTRA Last Encore - 12 VF VOSTFR [720p]](https://uptobox.com/3zuaioyy3s4h)
- [[Refrain] FateEXTRA Last Encore - 13 VF VOSTFR [720p]](https://uptobox.com/b1qkhnysmroy)
