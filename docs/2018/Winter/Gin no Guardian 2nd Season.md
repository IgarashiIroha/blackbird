# Gin no Guardian 2nd Season

![Gin no Guardian 2nd Season](https://cdn.myanimelist.net/images/anime/8/86476l.jpg)

* Japanese:  銀の墓守り 第2期

## Information

 - Type: TV
 - Episodes: 6
 - Status: Finished Airing
 - Aired: Jan 13, 2018 to Feb 17, 2018
 - Premiered: Winter 2018
 - Broadcast: Saturdays at 21:00 (JST)
 - Producers: Haoliners Animation League
 - Licensors: Funimation
 - Studios: Emon, Blade
 - Source: Web manga
 - Genres: Adventure, Fantasy
 - Duration: 24 min. per ep.
 - Rating: PG-13 - Teens 13 or older


## Links

- [[Refrain] Gin no Guardian S2 - 01 VOSTFR [720p]](https://uptobox.com/zmxu4d29q9rd)
- [[Refrain] Gin no Guardian S2 - 01 VOSTFR [1080p]](https://uptobox.com/ip4scnrujtxe)
- [[Refrain] Gin no Guardian S2 - 02 VOSTFR [720p]](https://uptobox.com/7jefbfnxobfq)
- [[Refrain] Gin no Guardian S2 - 02 VOSTFR [1080p]](https://uptobox.com/vjezvg7mc4gi)
- [[Refrain] Gin no Guardian S2 - 03 VOSTFR [720p]](https://uptobox.com/wxg7oi66pbso)
- [[Refrain] Gin no Guardian S2 - 03 VOSTFR [1080p]](https://uptobox.com/tetxq1er3kod)
- [[Refrain] Gin no Guardian S2 - 04 VOSTFR [720p]](https://uptobox.com/aygc5fh8g29b)
- [[Refrain] Gin no Guardian S2 - 04 VOSTFR [1080p]](https://uptobox.com/lpoeoz91578w)
- [[Refrain] Gin no Guardian S2 - 05 VOSTFR [720p]](https://uptobox.com/s5vm05ykewq6)
- [[Refrain] Gin no Guardian S2 - 05 VOSTFR [1080p]](https://uptobox.com/f67pckyig620)
- [[Refrain] Gin no Guardian S2 - 06 VOSTFR [720p]](https://uptobox.com/29x86aa6pisr)
- [[Refrain] Gin no Guardian S2 - 06 VOSTFR [1080p]](https://uptobox.com/cfhy3glq8e52)
