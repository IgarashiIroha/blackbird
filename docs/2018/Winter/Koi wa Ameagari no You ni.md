# Koi wa Ameagari no You ni

![Koi wa Ameagari no You ni](https://cdn.myanimelist.net/images/anime/1271/90136l.jpg)

* Japanese:  恋は雨上がりのように

## Information

 - Type: TV
 - Episodes: 12
 - Status: Finished Airing
 - Aired: Jan 12, 2018 to Mar 30, 2018
 - Premiered: Winter 2018
 - Broadcast: Fridays at 00:55 (JST)
 - Producers: Aniplex, Dentsu, Fuji TV, DMM pictures
 - Licensors: None found, add some
 - Studios: Wit Studio
 - Source: Manga
 - Genres: Romance, Seinen
 - Duration: 22 min. per ep.
 - Rating: PG-13 - Teens 13 or older


## Links

- [[Refrain] Koi wa Ameagari no You ni - 01 VOSTFR [720p]](https://uptobox.com/egg0fy0jjr9p)
- [[Refrain] Koi wa Ameagari no You ni - 01 VOSTFR [1080p]](https://uptobox.com/gauoajrgzlri)
- [[Refrain] Koi wa Ameagari no You ni - 02 VOSTFR [720p]](https://uptobox.com/oedxmm0x4wxc)
- [[Refrain] Koi wa Ameagari no You ni - 02 VOSTFR [1080p]](https://uptobox.com/g60df7e1vr3c)
- [[Refrain] Koi wa Ameagari no You ni - 03 VOSTFR [720p]](https://uptobox.com/usjemu87gvoj)
- [[Refrain] Koi wa Ameagari no You ni - 03 VOSTFR [1080p]](https://uptobox.com/t60o0axj6qqh)
- [[Refrain] Koi wa Ameagari no You ni - 04 VOSTFR [720p]](https://uptobox.com/qlmx64o2fgt1)
- [[Refrain] Koi wa Ameagari no You ni - 04 VOSTFR [1080p]](https://uptobox.com/0q3m8d4qyv18)
- [[Refrain] Koi wa Ameagari no You ni - 05 VOSTFR [720p]](https://uptobox.com/dumlddp5k6fm)
- [[Refrain] Koi wa Ameagari no You ni - 05 VOSTFR [1080p]](https://uptobox.com/dhfv2oqn6ren)
- [[Refrain] Koi wa Ameagari no You ni - 06 VOSTFR [720p]](https://uptobox.com/nk0pmq74xoy8)
- [[Refrain] Koi wa Ameagari no You ni - 06 VOSTFR [1080p]](https://uptobox.com/zf9zin1g0dpc)
- [[Refrain] Koi wa Ameagari no You ni - 07 VOSTFR [720p]](https://uptobox.com/rqeq8eqngucy)
- [[Refrain] Koi wa Ameagari no You ni - 07 VOSTFR [1080p]](https://uptobox.com/g44qt3s1ojmt)
- [[Refrain] Koi wa Ameagari no You ni - 08 VOSTFR [1080p]](https://uptobox.com/pd29cxx7pmyi)
- [[Refrain] Koi wa Ameagari no You ni - 08 VOSTFR [720p]](https://uptobox.com/70a6dnkfkr3z)
- [[Refrain] Koi wa Ameagari no You ni - 09 VOSTFR [1080p]](https://uptobox.com/p2cgxxjzegb8)
- [[Refrain] Koi wa Ameagari no You ni - 09 VOSTFR [720p]](https://uptobox.com/97xviiqjod2f)
- [[Refrain] Koi wa Ameagari no You ni - 10 VOSTFR [1080p]](https://uptobox.com/jqtwyxr8r93l)
- [[Refrain] Koi wa Ameagari no You ni - 10 VOSTFR [720p]](https://uptobox.com/4gk0oyps9g1l)
- [[Refrain] Koi wa Ameagari no You ni - 11 VOSTFR [1080p]](https://uptobox.com/1zk812qjlded)
- [[Refrain] Koi wa Ameagari no You ni - 11 VOSTFR [720p]](https://uptobox.com/dt8g7tpv4hz7)
- [[Refrain] Koi wa Ameagari no You ni - 12 VOSTFR [1080p]](https://uptobox.com/rz7wb5g1pqzz)
- [[Refrain] Koi wa Ameagari no You ni - 12 VOSTFR [720p]](https://uptobox.com/yj5f8e7iwv79)
