# Emiya-san Chi no Kyou no Gohan

![Emiya-san Chi no Kyou no Gohan](https://cdn.myanimelist.net/images/anime/8/89981l.jpg)

* Japanese:  衛宮さんちの今日のごはん

## Information

 - Type: ONA
 - Episodes: 13
 - Status: Finished Airing
 - Aired: Dec 31, 2017 to Jan 1, 2019
 - Premiered: Winter 2018
 - Producers: Aniplex, Kadokawa
 - Licensors: Aniplex of America
 - Studios: ufotable
 - Source: Manga
 - Genres: Slice of Life, Comedy
 - Duration: 13 min. per ep.
-  Rating: PG-13 - Teens 13 or older


## Links

- [[Refrain] Emiya-san Chi no Kyou no Gohan - 01 VOSTFR [720p]](https://uptobox.com/dm8qz9otrugj)
- [[Refrain] Emiya-san Chi no Kyou no Gohan - 01 VOSTFR [1080p]](https://uptobox.com/dkbkhl76t67v)
- [[Refrain] Emiya-san Chi no Kyou no Gohan - 02 VOSTFR [720p]](https://uptobox.com/xc040qopqhxf)
- [[Refrain] Emiya-san Chi no Kyou no Gohan - 02 VOSTFR [1080p]](https://uptobox.com/d9pbzth384hz)
- [[Refrain] Emiya-san Chi no Kyou no Gohan - 03 VOSTFR [720p]](https://uptobox.com/o4umdpy6k1b1)
- [[Refrain] Emiya-san Chi no Kyou no Gohan - 03 VOSTFR [1080p]](https://uptobox.com/qvj25uq7yzcz)
- [[Refrain] Emiya-san Chi no Kyou no Gohan - 04 VOSTFR [720p]](https://uptobox.com/hjoakirvvg31)
- [[Refrain] Emiya-san Chi no Kyou no Gohan - 04 VOSTFR [1080p]](https://uptobox.com/wrjva20i6xek)
- [[Refrain] Emiya-san Chi no Kyou no Gohan - 05 VOSTFR [720p]](https://uptobox.com/lurt1uhj0mt8)
- [[Refrain] Emiya-san Chi no Kyou no Gohan - 05 VOSTFR [1080p]](https://uptobox.com/failk5w5aqzt)
- [[Refrain] Emiya-san Chi no Kyou no Gohan - 06 VOSTFR [720p]](https://uptobox.com/no96bjawsp7s)
- [[Refrain] Emiya-san Chi no Kyou no Gohan - 06 VOSTFR [1080p]](https://uptobox.com/v293t05kfdwa)
- [[Refrain] Emiya-san Chi no Kyou no Gohan - 07 VOSTFR [720p]](https://uptobox.com/voeg04hyf786)
- [[Refrain] Emiya-san Chi no Kyou no Gohan - 07 VOSTFR [1080p]](https://uptobox.com/wdrxpz9r97qt)
- [[Refrain] Emiya-san Chi no Kyou no Gohan - 08 VOSTFR [720p]](https://uptobox.com/mov7y4nvil1i)
- [[Refrain] Emiya-san Chi no Kyou no Gohan - 08 VOSTFR [1080p]](https://uptobox.com/1j78amcnk5il)
- [[Refrain] Emiya-san Chi no Kyou no Gohan - 09 VOSTFR [720p]](https://uptobox.com/zieyjg4l5mm7)
- [[Refrain] Emiya-san Chi no Kyou no Gohan - 09 VOSTFR [1080p]](https://uptobox.com/frq33ivo4mm7)
- [[Refrain] Emiya-san Chi no Kyou no Gohan - 10 VOSTFR [720p]](https://uptobox.com/79h5dqoxo2te)
- [[Refrain] Emiya-san Chi no Kyou no Gohan - 10 VOSTFR [1080p]](https://uptobox.com/w7ajhab66dj1)
- [[Refrain] Emiya-san Chi no Kyou no Gohan - 11 VOSTFR [720p]](https://uptobox.com/tcjm4eupxjto)
- [[Refrain] Emiya-san Chi no Kyou no Gohan - 11 VOSTFR [1080p]](https://uptobox.com/natik4b062w3)
- [[Refrain] Emiya-san Chi no Kyou no Gohan - 12 VOSTFR [720p]](https://uptobox.com/s2xogdwn4g82)
- [[Refrain] Emiya-san Chi no Kyou no Gohan - 12 VOSTFR [1080p]](https://uptobox.com/jqco5r0smsor)
- [[Refrain] Emiya-san Chi no Kyou no Gohan - 13 VOSTFR [720p]](https://uptobox.com/2vy7nfupxyvz)
- [[Refrain] Emiya-san Chi no Kyou no Gohan - 13 VOSTFR [1080p]](https://uptobox.com/c6kftrb11951)
