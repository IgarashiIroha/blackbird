# Karakai Jouzu no Takagi-san

![Karakai Jouzu no Takagi-san](https://cdn.myanimelist.net/images/anime/1591/95091l.jpg)

* Japanese:  からかい上手の高木さん

## Information

 - Type: TV
 - Episodes: 12
 - Status: Finished Airing
 - Aired: Jan 8, 2018 to Mar 26, 2018
 - Premiered: Winter 2018
 - Broadcast: Mondays at 23:00 (JST)
 - Producers: TMS Entertainment, Asatsu DK, Asmik Ace, TOHO animation, East Japan Marketing & Communications
 - Licensors: Funimation
 - Studios: Shin-Ei Animation
 - Source: Manga
 - Genres: Romance, Comedy, School, Slice of Life, Shounen
 - Duration: 23 min. per ep.
 - Rating: PG-13 - Teens 13 or older


## Links

- [[Refrain] Karakai Jouzu no Takagi-san - 01 VOSTFR [720p]](https://uptobox.com/bj45oh9t0toh)
- [[Refrain] Karakai Jouzu no Takagi-san - 01 VOSTFR [1080p]](https://uptobox.com/jx00b1trfwcj)
- [[Refrain] Karakai Jouzu no Takagi-san - 02 VOSTFR [720p]](https://uptobox.com/g4w928xsw2yg)
- [[Refrain] Karakai Jouzu no Takagi-san - 02 VOSTFR [1080p]](https://uptobox.com/80b0g3u09pwo)
- [[Refrain] Karakai Jouzu no Takagi-san - 03 VOSTFR [720p]](https://uptobox.com/w8d11ccg7ud2)
- [[Refrain] Karakai Jouzu no Takagi-san - 03 VOSTFR [1080p]](https://uptobox.com/zn861i57sruu)
- [[Refrain] Karakai Jouzu no Takagi-san - 04 VOSTFR [720p]](https://uptobox.com/aqku6kb5kiq3)
- [[Refrain] Karakai Jouzu no Takagi-san - 04 VOSTFR [1080p]](https://uptobox.com/3apk58znp555)
- [[Refrain] Karakai Jouzu no Takagi-san - 05 VOSTFR [720p]](https://uptobox.com/w6a0gch3xcy7)
- [[Refrain] Karakai Jouzu no Takagi-san - 05 VOSTFR [1080p]](https://uptobox.com/7zyqeoisinso)
- [[Refrain] Karakai Jouzu no Takagi-san - 06 VOSTFR [720p]](https://uptobox.com/1e0u8gp5uevm)
- [[Refrain] Karakai Jouzu no Takagi-san - 06 VOSTFR [1080p]](https://uptobox.com/h5jcmdgzqaax)
- [[Refrain] Karakai Jouzu no Takagi-san - 07 VOSTFR [720p]](https://uptobox.com/spqudbvyox9y)
- [[Refrain] Karakai Jouzu no Takagi-san - 07 VOSTFR [1080p]](https://uptobox.com/c6yzz9oy6g5a)
- [[Refrain] Karakai Jouzu no Takagi-san - 08 VOSTFR [720p]](https://uptobox.com/x89clyboj1eb)
- [[Refrain] Karakai Jouzu no Takagi-san - 08 VOSTFR [1080p]](https://uptobox.com/tm827tsighey)
- [[Refrain] Karakai Jouzu no Takagi-san - 09 VOSTFR [720p]](https://uptobox.com/v7qdyfiaulgz)
- [[Refrain] Karakai Jouzu no Takagi-san - 09 VOSTFR [1080p]](https://uptobox.com/otkv3xs54pph)
- [[Refrain] Karakai Jouzu no Takagi-san - 10 VOSTFR [720p]](https://uptobox.com/8lhpwmx12dph)
- [[Refrain] Karakai Jouzu no Takagi-san - 10 VOSTFR [1080p]](https://uptobox.com/8jyf25z9cheq)
- [[Refrain] Karakai Jouzu no Takagi-san - 11 VOSTFR [720p]](https://uptobox.com/wa6rrio2b1ur)
- [[Refrain] Karakai Jouzu no Takagi-san - 11 VOSTFR [1080p]](https://uptobox.com/348kp8l99tki)
- [[Refrain] Karakai Jouzu no Takagi-san - 12 VOSTFR [720p]](https://uptobox.com/p5mbgywo2w9s)
- [[Refrain] Karakai Jouzu no Takagi-san - 12 VOSTFR [1080p]](https://uptobox.com/p7l9ziznh9e6)
