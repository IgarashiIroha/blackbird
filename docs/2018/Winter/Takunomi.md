# Takunomi.

![Takunomi.](https://cdn.myanimelist.net/images/anime/1869/95731l.jpg)

* Japanese:  たくのみ。

## Information

 - Type: TV
 - Episodes: 12
 - Status: Finished Airing
 - Aired: Jan 12, 2018 to Mar 30, 2018
 - Premiered: Winter 2018
 - Broadcast: Fridays at 02:43 (JST)
 - Producers: Pony Canyon, TBS, Magic Capsule, Shogakukan, Lawson, RAY, U-NEXT, Nichion 
 - Licensors: Sentai Filmworks 
 - Studios: Production IMS 
 - Source: Web manga
 - Genres: Comedy, Slice of Life
 - Duration: 12 min. per ep.
 - Rating: PG-13 - Teens 13 or older


## Links

