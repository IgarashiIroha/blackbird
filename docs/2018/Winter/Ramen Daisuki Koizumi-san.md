# Ramen Daisuki Koizumi-san

![Ramen Daisuki Koizumi-san](https://cdn.myanimelist.net/images/anime/5/89876l.jpg)

* Japanese:  ラーメン大好き小泉さん

## Information

 - Type: TV
 - Episodes: 12
 - Status: Finished Airing
 - Aired: Jan 4, 2018 to Mar 22, 2018
 - Premiered: Winter 2018
 - Broadcast: Thursdays at 20:00 (JST)
 - Producers: Sotsu, KlockWorx, flying DOG, Grooove, Cygames, Crunchyroll SC Anime Fund
 - Licensors: None found, add some
 - Studios: Studio Gokumi, AXsiZ
 - Source: Manga
 - Genres: Comedy, Slice of Life
 - Duration: 22 min. per ep.
 - Rating: PG-13 - Teens 13 or older


## Links

- [[Refrain] Ramen Daisuki Koizumi-san - 01 VOSTFR [720p]](https://uptobox.com/9nhgm0j9kqcb)
- [[Refrain] Ramen Daisuki Koizumi-san - 01 VOSTFR [1080p]](https://uptobox.com/3x9xsxintrws)
- [[Refrain] Ramen Daisuki Koizumi-san - 02 VOSTFR [720p]](https://uptobox.com/cuf2pygoqajz)
- [[Refrain] Ramen Daisuki Koizumi-san - 02 VOSTFR [1080p]](https://uptobox.com/qahw9j5eqt51)
- [[Refrain] Ramen Daisuki Koizumi-san - 03 VOSTFR [720p]](https://uptobox.com/a0je9cj0ozqf)
- [[Refrain] Ramen Daisuki Koizumi-san - 03 VOSTFR [1080p]](https://uptobox.com/bzzrte4o7tid)
- [[Refrain] Ramen Daisuki Koizumi-san - 04 VOSTFR [720p]](https://uptobox.com/ajec8f87kylb)
- [[Refrain] Ramen Daisuki Koizumi-san - 04 VOSTFR [1080p]](https://uptobox.com/7a3b9c3wj25a)
- [[Refrain] Ramen Daisuki Koizumi-san - 05 VOSTFR [720p]](https://uptobox.com/bm30810xhfta)
- [[Refrain] Ramen Daisuki Koizumi-san - 05 VOSTFR [1080p]](https://uptobox.com/q9sfws7xvcf2)
- [[Refrain] Ramen Daisuki Koizumi-san - 06 VOSTFR [720p]](https://uptobox.com/hndl39zk9g7v)
- [[Refrain] Ramen Daisuki Koizumi-san - 06 VOSTFR [1080p]](https://uptobox.com/d4hnhrciak1v)
- [[Refrain] Ramen Daisuki Koizumi-san - 07 VOSTFR [720p]](https://uptobox.com/95hhx230byl7)
- [[Refrain] Ramen Daisuki Koizumi-san - 07 VOSTFR [1080p]](https://uptobox.com/x8pfr82yrgxs)
- [[Refrain] Ramen Daisuki Koizumi-san - 08 VOSTFR [720p]](https://uptobox.com/6hq7ddbqfmxk)
- [[Refrain] Ramen Daisuki Koizumi-san - 08 VOSTFR [1080p]](https://uptobox.com/1ldrys45b0uk)
- [[Refrain] Ramen Daisuki Koizumi-san - 09 VOSTFR [720p]](https://uptobox.com/mddr9uz599bp)
- [[Refrain] Ramen Daisuki Koizumi-san - 09 VOSTFR [1080p]](https://uptobox.com/g35tmgg279ep)
- [[Refrain] Ramen Daisuki Koizumi-san - 10 VOSTFR [720p]](https://uptobox.com/4acklx4poysg)
- [[Refrain] Ramen Daisuki Koizumi-san - 10 VOSTFR [1080p]](https://uptobox.com/4w77p58r0046)
- [[Refrain] Ramen Daisuki Koizumi-san - 11 VOSTFR [720p]](https://uptobox.com/5tgsa7ztgzg6)
- [[Refrain] Ramen Daisuki Koizumi-san - 11 VOSTFR [1080p]](https://uptobox.com/p81nkdyebb3w)
- [[Refrain] Ramen Daisuki Koizumi-san - 12 VOSTFR [720p]](https://uptobox.com/1zxele0ow4uk)
- [[Refrain] Ramen Daisuki Koizumi-san - 12 VOSTFR [1080p]](https://uptobox.com/k77j59wifq6v)
