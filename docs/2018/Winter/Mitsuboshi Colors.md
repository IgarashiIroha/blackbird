# Mitsuboshi Colors

![Mitsuboshi Colors](https://cdn.myanimelist.net/images/anime/5/89984l.jpg)

* Japanese:  三ツ星カラーズ

## Information

 - Type: TV
 - Episodes: 12
 - Status: Finished Airing
 - Aired: Jan 7, 2018 to Mar 25, 2018
 - Premiered: Winter 2018
 - Broadcast: Sundays at 22:30 (JST)
 - Producers: Frontier Works, Movic, AT-X, Grooove, Hakuhodo DY Music & Pictures, Kadokawa Media House, Kadokawa 
 - Licensors: Sentai Filmworks 
 - Studios: Silver Link. 
 - Source: Manga
 - Genres: Slice of Life, Comedy, Shounen
 - Duration: 23 min. per ep.
 - Rating: PG-13 - Teens 13 or older


## Links

