# Hataraku Onii-san!

![Hataraku Onii-san!](https://cdn.myanimelist.net/images/anime/10/88930l.jpg)

* Japanese:  働くお兄さん!

## Information

 - Type: TV
 - Episodes: 12
 - Status: Finished Airing
 - Aired: Jan 5, 2018 to Mar 23, 2018
 - Premiered: Winter 2018
 - Broadcast: Fridays at 21:54 (JST)
 - Producers: Lantis, Movic, BS Fuji, Tokyo MX, Good Smile Company 
 - Licensors: None found, add some 
 - Studios: Tomovies 
 - Source: Original
 - Genres: Slice of Life, Comedy
 - Duration: 4 min. per ep.
 - Rating: G - All Ages


## Links

