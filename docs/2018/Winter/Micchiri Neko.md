# Micchiri Neko

![Micchiri Neko](https://cdn.myanimelist.net/images/anime/7/89947l.jpg)

* Japanese:  みっちりねこ

## Information

 - Type: TV
 - Episodes: 39
 - Status: Finished Airing
 - Aired: Jan 4, 2018 to Mar 26, 2018
 - Premiered: Winter 2018
 - Broadcast: Not scheduled once per week
 - Producers: Frencel 
 - Licensors: None found, add some 
 - Studios: helo.inc 
 - Source: Web manga
 - Genres: Comedy, Kids
 - Duration: 1 min. per ep.
 - Rating: G - All Ages


## Links

