# Devilman: Crybaby

![Devilman: Crybaby](https://cdn.myanimelist.net/images/anime/2/89973l.jpg)

* Japanese:  DEVILMAN crybaby

## Information

 - Type: ONA
 - Episodes: 10
 - Status: Finished Airing
 - Aired: Jan 5, 2018
 - Producers: Aniplex, Dynamic Planning 
 - Licensors: None found, add some 
 - Studios: Science SARU 
 - Source: Manga
 - Genres: Action, Dementia, Demons, Horror, Supernatural
 - Duration: 25 min. per ep.
 - Rating: R+ - Mild Nudity


## Links

- [[Refrain] Devilman - Crybaby - 01 VF VOSTFR [1080p]](https://uptobox.com/i8hpzocxtp9d)
- [[Refrain] Devilman - Crybaby - 01 VF VOSTFR [720p]](https://uptobox.com/gdhlwkaoz9kr)
- [[Refrain] Devilman - Crybaby - 02 VF VOSTFR [1080p]](https://uptobox.com/w1dpt8lwgqx0)
- [[Refrain] Devilman - Crybaby - 02 VF VOSTFR [720p]](https://uptobox.com/wk3u9v80swqk)
- [[Refrain] Devilman - Crybaby - 03 VF VOSTFR [1080p]](https://uptobox.com/b46wftsqf7ps)
- [[Refrain] Devilman - Crybaby - 03 VF VOSTFR [720p]](https://uptobox.com/cd091vs4qb6b)
- [[Refrain] Devilman - Crybaby - 04 VF VOSTFR [1080p]](https://uptobox.com/gbm0tw782qya)
- [[Refrain] Devilman - Crybaby - 04 VF VOSTFR [720p]](https://uptobox.com/tkbfugqyyd2f)
- [[Refrain] Devilman - Crybaby - 05 VF VOSTFR [1080p]](https://uptobox.com/j36l9sflfjcp)
- [[Refrain] Devilman - Crybaby - 05 VF VOSTFR [720p]](https://uptobox.com/hh1tlnj6q7g1)
- [[Refrain] Devilman - Crybaby - 06 VF VOSTFR [1080p]](https://uptobox.com/4rmclcjo5ywh)
- [[Refrain] Devilman - Crybaby - 06 VF VOSTFR [720p]](https://uptobox.com/b9fr9r0g3vwh)
- [[Refrain] Devilman - Crybaby - 07 VF VOSTFR [1080p]](https://uptobox.com/i688wb0q3tzk)
- [[Refrain] Devilman - Crybaby - 07 VF VOSTFR [720p]](https://uptobox.com/xra8iwqqxd85)
- [[Refrain] Devilman - Crybaby - 08 VF VOSTFR [1080p]](https://uptobox.com/51tkxzv0eh19)
- [[Refrain] Devilman - Crybaby - 08 VF VOSTFR [720p]](https://uptobox.com/z3s8cpilfpip)
- [[Refrain] Devilman - Crybaby - 09 VF VOSTFR [1080p]](https://uptobox.com/za2f7o98j76c)
- [[Refrain] Devilman - Crybaby - 09 VF VOSTFR [720p]](https://uptobox.com/xsmb8heu9tde)
- [[Refrain] Devilman - Crybaby - 10 VF VOSTFR [1080p]](https://uptobox.com/pa9ip2z6jgik)
- [[Refrain] Devilman - Crybaby - 10 VF VOSTFR [720p]](https://uptobox.com/2n8zds2o0vjt)
