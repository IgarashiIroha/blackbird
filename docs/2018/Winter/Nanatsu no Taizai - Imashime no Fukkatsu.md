# Nanatsu no Taizai: Imashime no Fukkatsu

![Nanatsu no Taizai: Imashime no Fukkatsu](https://cdn.myanimelist.net/images/anime/11/90089l.jpg)

* Japanese:  七つの大罪 戒めの復活

## Information

 - Type: TV
 - Episodes: 24
 - Status: Finished Airing
 - Aired: Jan 13, 2018 to Jun 30, 2018
 - Premiered: Winter 2018
 - Broadcast: Saturdays at 06:30 (JST)
 - Producers: Aniplex, Dentsu, Kodansha, Movic, Sammy
 - Licensors: None found, add some
 - Studios: A-1 Pictures
 - Source: Manga
 - Genres: Action, Adventure, Supernatural, Magic, Fantasy, Shounen
 - Duration: 24 min. per ep.
 - Rating: PG-13 - Teens 13 or older


## Links

- [[Refrain] Nanatsu no Taizai - Imashime no Fukkatsu - 01 VF VOSTFR [720p]](https://uptobox.com/j7yliylmqma3)
- [[Refrain] Nanatsu no Taizai - Imashime no Fukkatsu - 01 VF VOSTFR [1080p]](https://uptobox.com/g5j8bo9o9lkb)
- [[Refrain] Nanatsu no Taizai - Imashime no Fukkatsu - 02 VF VOSTFR [720p]](https://uptobox.com/aa7a0cvd8yb5)
- [[Refrain] Nanatsu no Taizai - Imashime no Fukkatsu - 02 VF VOSTFR [1080p]](https://uptobox.com/j1piuxacb16y)
- [[Refrain] Nanatsu no Taizai - Imashime no Fukkatsu - 03 VF VOSTFR [720p]](https://uptobox.com/9gsw0i8cu7t8)
- [[Refrain] Nanatsu no Taizai - Imashime no Fukkatsu - 03 VF VOSTFR [1080p]](https://uptobox.com/nk53fcgmt4ox)
- [[Refrain] Nanatsu no Taizai - Imashime no Fukkatsu - 04 VF VOSTFR [720p]](https://uptobox.com/wykrpu84mr3i)
- [[Refrain] Nanatsu no Taizai - Imashime no Fukkatsu - 04 VF VOSTFR [1080p]](https://uptobox.com/vm22ezuts55r)
- [[Refrain] Nanatsu no Taizai - Imashime no Fukkatsu - 05 VF VOSTFR [720p]](https://uptobox.com/hxvg9cozu6n1)
- [[Refrain] Nanatsu no Taizai - Imashime no Fukkatsu - 05 VF VOSTFR [1080p]](https://uptobox.com/3urqt2y5s6ny)
- [[Refrain] Nanatsu no Taizai - Imashime no Fukkatsu - 06 VF VOSTFR [720p]](https://uptobox.com/niji967ynpvm)
- [[Refrain] Nanatsu no Taizai - Imashime no Fukkatsu - 06 VF VOSTFR [1080p]](https://uptobox.com/k65o99z7o4xe)
- [[Refrain] Nanatsu no Taizai - Imashime no Fukkatsu - 07 VF VOSTFR [720p]](https://uptobox.com/bhxqfapguddw)
- [[Refrain] Nanatsu no Taizai - Imashime no Fukkatsu - 07 VF VOSTFR [1080p]](https://uptobox.com/m6dd4gk087zm)
- [[Refrain] Nanatsu no Taizai - Imashime no Fukkatsu - 08 VF VOSTFR [720p]](https://uptobox.com/bd1aams2th6x)
- [[Refrain] Nanatsu no Taizai - Imashime no Fukkatsu - 08 VF VOSTFR [1080p]](https://uptobox.com/5lg7nfp49l3q)
- [[Refrain] Nanatsu no Taizai - Imashime no Fukkatsu - 09 VF VOSTFR [720p]](https://uptobox.com/79r813iunzuu)
- [[Refrain] Nanatsu no Taizai - Imashime no Fukkatsu - 09 VF VOSTFR [1080p]](https://uptobox.com/mrc2wz81klzv)
- [[Refrain] Nanatsu no Taizai - Imashime no Fukkatsu - 10 VF VOSTFR [720p]](https://uptobox.com/vmneqij830tb)
- [[Refrain] Nanatsu no Taizai - Imashime no Fukkatsu - 10 VF VOSTFR [1080p]](https://uptobox.com/14onvggag0ck)
- [[Refrain] Nanatsu no Taizai - Imashime no Fukkatsu - 11 VF VOSTFR [720p]](https://uptobox.com/fuvw9eviylrw)
- [[Refrain] Nanatsu no Taizai - Imashime no Fukkatsu - 11 VF VOSTFR [1080p]](https://uptobox.com/mrlv4q46v4ow)
- [[Refrain] Nanatsu no Taizai - Imashime no Fukkatsu - 12 VF VOSTFR [720p]](https://uptobox.com/dlt15vd485pz)
- [[Refrain] Nanatsu no Taizai - Imashime no Fukkatsu - 12 VF VOSTFR [1080p]](https://uptobox.com/en7dqm8o1ecc)
- [[Refrain] Nanatsu no Taizai - Imashime no Fukkatsu - 13 VF VOSTFR [720p]](https://uptobox.com/t8nr03j7n7de)
- [[Refrain] Nanatsu no Taizai - Imashime no Fukkatsu - 13 VF VOSTFR [1080p]](https://uptobox.com/1djmubo79awu)
- [[Refrain] Nanatsu no Taizai - Imashime no Fukkatsu - 14 VF VOSTFR [720p]](https://uptobox.com/s8jk1crgj533)
- [[Refrain] Nanatsu no Taizai - Imashime no Fukkatsu - 14 VF VOSTFR [1080p]](https://uptobox.com/y8bmgj5pkqmh)
- [[Refrain] Nanatsu no Taizai - Imashime no Fukkatsu - 15 VF VOSTFR [720p]](https://uptobox.com/vdasiesazrkc)
- [[Refrain] Nanatsu no Taizai - Imashime no Fukkatsu - 15 VF VOSTFR [1080p]](https://uptobox.com/xyiudjf689i4)
- [[Refrain] Nanatsu no Taizai - Imashime no Fukkatsu - 16 VF VOSTFR [720p]](https://uptobox.com/rxpi7tw9jpje)
- [[Refrain] Nanatsu no Taizai - Imashime no Fukkatsu - 16 VF VOSTFR [1080p]](https://uptobox.com/89r6ingy99aw)
- [[Refrain] Nanatsu no Taizai - Imashime no Fukkatsu - 17 VF VOSTFR [720p]](https://uptobox.com/des3287cirl0)
- [[Refrain] Nanatsu no Taizai - Imashime no Fukkatsu - 17 VF VOSTFR [1080p]](https://uptobox.com/pltko5t62alo)
- [[Refrain] Nanatsu no Taizai - Imashime no Fukkatsu - 18 VF VOSTFR [720p]](https://uptobox.com/ynq7gew053pu)
- [[Refrain] Nanatsu no Taizai - Imashime no Fukkatsu - 18 VF VOSTFR [1080p]](https://uptobox.com/xu3jw12kx3oo)
- [[Refrain] Nanatsu no Taizai - Imashime no Fukkatsu - 19 VF VOSTFR [720p]](https://uptobox.com/kp55cjrann4b)
- [[Refrain] Nanatsu no Taizai - Imashime no Fukkatsu - 19 VF VOSTFR [1080p]](https://uptobox.com/j18ue74zk1as)
- [[Refrain] Nanatsu no Taizai - Imashime no Fukkatsu - 20 VF VOSTFR [720p]](https://uptobox.com/w7v1mr1wjjff)
- [[Refrain] Nanatsu no Taizai - Imashime no Fukkatsu - 20 VF VOSTFR [1080p]](https://uptobox.com/f5751gn4rfsi)
- [[Refrain] Nanatsu no Taizai - Imashime no Fukkatsu - 21 VF VOSTFR [720p]](https://uptobox.com/j9rt2eihnx2b)
- [[Refrain] Nanatsu no Taizai - Imashime no Fukkatsu - 21 VF VOSTFR [1080p]](https://uptobox.com/l2u8vkcznbwn)
- [[Refrain] Nanatsu no Taizai - Imashime no Fukkatsu - 22 VF VOSTFR [720p]](https://uptobox.com/ool1mavkydrs)
- [[Refrain] Nanatsu no Taizai - Imashime no Fukkatsu - 22 VF VOSTFR [1080p]](https://uptobox.com/jbhyz6089kqb)
- [[Refrain] Nanatsu no Taizai - Imashime no Fukkatsu - 23 VF VOSTFR [720p]](https://uptobox.com/t30rou5030ri)
- [[Refrain] Nanatsu no Taizai - Imashime no Fukkatsu - 23 VF VOSTFR [1080p]](https://uptobox.com/qyn5uvfwp4s0)
- [[Refrain] Nanatsu no Taizai - Imashime no Fukkatsu - 24 VF VOSTFR [720p]](https://uptobox.com/1jqomcbigif3)
- [[Refrain] Nanatsu no Taizai - Imashime no Fukkatsu - 24 VF VOSTFR [1080p]](https://uptobox.com/hvvxbg4nmsof)
