# Gintama.: Shirogane no Tamashii-hen

![Gintama.: Shirogane no Tamashii-hen](https://cdn.myanimelist.net/images/anime/12/89603l.jpg)

* Japanese:  銀魂. 銀ノ魂篇

## Information

 - Type: TV
 - Episodes: 12
 - Status: Finished Airing
 - Aired: Jan 8, 2018 to Mar 26, 2018
 - Premiered: Winter 2018
 - Broadcast: Mondays at 01:35 (JST)
 - Producers: TV Tokyo, Aniplex, Dentsu, Shueisha
 - Licensors: None found, add some
 - Studios: Bandai Namco Pictures
 - Source: Manga
 - Genres: Action, Sci-Fi, Comedy, Historical, Parody, Samurai, Shounen
 - Duration: 24 min. per ep.
 - Rating: PG-13 - Teens 13 or older


## Links

- [[Refrain] Gintama. Shirogane no Tamashii-hen (2018) - 342 VOSTFR [720p]](https://uptobox.com/nbt0irbmhoxk)
- [[Refrain] Gintama. Shirogane no Tamashii-hen (2018) - 342 VOSTFR [1080p]](https://uptobox.com/h69j1vnijzjp)
- [[Refrain] Gintama. Shirogane no Tamashii-hen (2018) - 343 VOSTFR [720p]](https://uptobox.com/19d5fa3cw8tn)
- [[Refrain] Gintama. Shirogane no Tamashii-hen (2018) - 343 VOSTFR [1080p]](https://uptobox.com/jaym1ggfh4ys)
- [[Refrain] Gintama. Shirogane no Tamashii-hen (2018) - 344 VOSTFR [720p]](https://uptobox.com/1gn3yp3l22ct)
- [[Refrain] Gintama. Shirogane no Tamashii-hen (2018) - 344 VOSTFR [1080p]](https://uptobox.com/yag2mghbsqpj)
- [[Refrain] Gintama. Shirogane no Tamashii-hen (2018) - 345 VOSTFR [720p]](https://uptobox.com/6rotqp7aqwye)
- [[Refrain] Gintama. Shirogane no Tamashii-hen (2018) - 345 VOSTFR [1080p]](https://uptobox.com/1jld8ztv30v1)
- [[Refrain] Gintama. Shirogane no Tamashii-hen (2018) - 346 VOSTFR [720p]](https://uptobox.com/zfzi88wrrwuf)
- [[Refrain] Gintama. Shirogane no Tamashii-hen (2018) - 346 VOSTFR [1080p]](https://uptobox.com/wmbcapp5t4zv)
- [[Refrain] Gintama. Shirogane no Tamashii-hen (2018) - 347 VOSTFR [720p]](https://uptobox.com/d2fucftg9r68)
- [[Refrain] Gintama. Shirogane no Tamashii-hen (2018) - 347 VOSTFR [1080p]](https://uptobox.com/p3zjo9ls2wnc)
- [[Refrain] Gintama. Shirogane no Tamashii-hen (2018) - 348 VOSTFR [720p]](https://uptobox.com/slen30ax7qbc)
- [[Refrain] Gintama. Shirogane no Tamashii-hen (2018) - 348 VOSTFR [1080p]](https://uptobox.com/7axtpcqq9h6i)
- [[Refrain] Gintama. Shirogane no Tamashii-hen (2018) - 349 VOSTFR [720p]](https://uptobox.com/bt82eobdfp4h)
- [[Refrain] Gintama. Shirogane no Tamashii-hen (2018) - 349 VOSTFR [1080p]](https://uptobox.com/z1wd5pl5t7l6)
- [[Refrain] Gintama. Shirogane no Tamashii-hen (2018) - 350 VOSTFR [720p]](https://uptobox.com/d743mdrbt2pg)
- [[Refrain] Gintama. Shirogane no Tamashii-hen (2018) - 350 VOSTFR [1080p]](https://uptobox.com/pmujx4lgbgca)
- [[Refrain] Gintama. Shirogane no Tamashii-hen (2018) - 351 VOSTFR [720p]](https://uptobox.com/ek3h9alhl2q4)
- [[Refrain] Gintama. Shirogane no Tamashii-hen (2018) - 351 VOSTFR [1080p]](https://uptobox.com/orblkoznieir)
- [[Refrain] Gintama. Shirogane no Tamashii-hen (2018) - 352 VOSTFR [720p]](https://uptobox.com/iqf1gn56gt87)
- [[Refrain] Gintama. Shirogane no Tamashii-hen (2018) - 352 VOSTFR [1080p]](https://uptobox.com/yzjci42esk4j)
- [[Refrain] Gintama. Shirogane no Tamashii-hen (2018) - 353 VOSTFR [720p]](https://uptobox.com/to2nh92h9c9z)
- [[Refrain] Gintama. Shirogane no Tamashii-hen (2018) - 353 VOSTFR [1080p]](https://uptobox.com/7ulqg6z0w83h)
