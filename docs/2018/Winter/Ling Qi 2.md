# Ling Qi 2

![Ling Qi 2](https://cdn.myanimelist.net/images/anime/1328/90485l.jpg)

* Japanese:  灵契 2

## Information

 - Type: TV
 - Episodes: 12
 - Status: Finished Airing
 - Aired: Feb 23, 2018 to May 11, 2018
 - Premiered: Winter 2018
 - Broadcast: Saturdays at 21:00 (JST)
 - Producers: Tencent Animation & Comics
 - Licensors: None found, add some
 - Studios: Haoliners Animation League
 - Source: Web manga
 - Genres: Action, Comedy, Magic, Shounen Ai, Supernatural
 - Duration: 24 min. per ep.
 - Rating: PG-13 - Teens 13 or older


## Links

- [[Refrain] Ling Qi 2 - 01 VOSTFR [1080p]](https://uptobox.com/8kmwz2a5k2m4)
- [[Refrain] Ling Qi 2 - 01 VOSTFR [720p]](https://uptobox.com/6lby7dfjfjd8)
- [[Refrain] Ling Qi 2 - 02 VOSTFR [1080p]](https://uptobox.com/sj7f7bp81ylx)
- [[Refrain] Ling Qi 2 - 02 VOSTFR [720p]](https://uptobox.com/t3uov4fkpgz6)
- [[Refrain] Ling Qi 2 - 03 VOSTFR [1080p]](https://uptobox.com/7uxinst263vf)
- [[Refrain] Ling Qi 2 - 03 VOSTFR [720p]](https://uptobox.com/lcxnre2ckye2)
- [[Refrain] Ling Qi 2 - 04 VOSTFR [1080p]](https://uptobox.com/umac67t45rbi)
- [[Refrain] Ling Qi 2 - 04 VOSTFR [720p]](https://uptobox.com/xad4sf404uqd)
- [[Refrain] Ling Qi 2 - 05 VOSTFR [1080p]](https://uptobox.com/fkyo5sp1ss7z)
- [[Refrain] Ling Qi 2 - 05 VOSTFR [720p]](https://uptobox.com/xffqwtx40w7q)
- [[Refrain] Ling Qi 2 - 06 VOSTFR [1080p]](https://uptobox.com/0tpu2env2b8b)
- [[Refrain] Ling Qi 2 - 06 VOSTFR [720p]](https://uptobox.com/ze795vpry50l)
- [[Refrain] Ling Qi 2 - 07 VOSTFR [1080p]](https://uptobox.com/kj97d9ud36x0)
- [[Refrain] Ling Qi 2 - 07 VOSTFR [720p]](https://uptobox.com/n7xvujvq2114)
- [[Refrain] Ling Qi 2 - 08 VOSTFR [1080p]](https://uptobox.com/28nve9ibdmzm)
- [[Refrain] Ling Qi 2 - 08 VOSTFR [720p]](https://uptobox.com/vl9tc7b17kfm)
- [[Refrain] Ling Qi 2 - 09 VOSTFR [1080p]](https://uptobox.com/rlahm1xvyaey)
- [[Refrain] Ling Qi 2 - 09 VOSTFR [720p]](https://uptobox.com/p2bsm1kac3cq)
- [[Refrain] Ling Qi 2 - 10 VOSTFR [1080p]](https://uptobox.com/ay1oehdwhpob)
- [[Refrain] Ling Qi 2 - 10 VOSTFR [720p]](https://uptobox.com/cmmkzr18seoq)
- [[Refrain] Ling Qi 2 - 11 VOSTFR [1080p]](https://uptobox.com/8q1jp2e45eej)
- [[Refrain] Ling Qi 2 - 11 VOSTFR [720p]](https://uptobox.com/s5zut4fvogo9)
- [[Refrain] Ling Qi 2 - 12 VOSTFR [1080p]](https://uptobox.com/tqr1l2w606wl)
- [[Refrain] Ling Qi 2 - 12 VOSTFR [720p]](https://uptobox.com/b0ay06m5qpdc)
