# Yuru Camp△

![Yuru Camp△](https://cdn.myanimelist.net/images/anime/4/89877l.jpg)

* Japanese:  ゆるキャン△

## Information

 - Type: TV
 - Episodes: 12
 - Status: Finished Airing
 - Aired: Jan 4, 2018 to Mar 22, 2018
 - Premiered: Winter 2018
 - Broadcast: Thursdays at 23:00 (JST)
 - Producers: AT-X, Sony Music Communications, MAGES., 81 Produce, BS11, Furyu, Crunchyroll SC Anime Fund
 - Licensors: None found, add some
 - Studios: C-Station
 - Source: Manga
 - Genres: Slice of Life, Comedy
 - Duration: 23 min. per ep.
 - Rating: PG-13 - Teens 13 or older


## Links

- [[Refrain] Yuru Campв–і - 01 VOSTFR [720p]](https://uptobox.com/41b7bqdkgl20)
- [[Refrain] Yuru Campв–і - 01 VOSTFR [1080p]](https://uptobox.com/4rqr4ezk4yqb)
- [[Refrain] Yuru Campв–і - 02 VOSTFR [720p]](https://uptobox.com/r4w67bialg1i)
- [[Refrain] Yuru Campв–і - 02 VOSTFR [1080p]](https://uptobox.com/199gehvwqtbg)
- [[Refrain] Yuru Campв–і - 03 VOSTFR [720p]](https://uptobox.com/q29on5azvfc7)
- [[Refrain] Yuru Campв–і - 03 VOSTFR [1080p]](https://uptobox.com/ojthmda1dbe3)
- [[Refrain] Yuru Campв–і - 04 VOSTFR [720p]](https://uptobox.com/ygufudztni83)
- [[Refrain] Yuru Campв–і - 04 VOSTFR [1080p]](https://uptobox.com/xz06f33wh5hf)
- [[Refrain] Yuru Campв–і - 05 VOSTFR [720p]](https://uptobox.com/t3wewk1rl00g)
- [[Refrain] Yuru Campв–і - 05 VOSTFR [1080p]](https://uptobox.com/yubr1fsdk888)
- [[Refrain] Yuru Campв–і - 06 VOSTFR [720p]](https://uptobox.com/wed8zemv61ki)
- [[Refrain] Yuru Campв–і - 06 VOSTFR [1080p]](https://uptobox.com/8tbcez5djgmy)
- [[Refrain] Yuru Campв–і - 07 VOSTFR [720p]](https://uptobox.com/htjolhwjabqk)
- [[Refrain] Yuru Campв–і - 07 VOSTFR [1080p]](https://uptobox.com/kzjrdjan0mdw)
- [[Refrain] Yuru Campв–і - 08 VOSTFR [720p]](https://uptobox.com/79qzi1fvimf0)
- [[Refrain] Yuru Campв–і - 08 VOSTFR [1080p]](https://uptobox.com/fnh9rodvhag2)
- [[Refrain] Yuru Campв–і - 09 VOSTFR [720p]](https://uptobox.com/ox7y1kfniskp)
- [[Refrain] Yuru Campв–і - 09 VOSTFR [1080p]](https://uptobox.com/a1dhhm8ay3cp)
- [[Refrain] Yuru Campв–і - 10 VOSTFR [720p]](https://uptobox.com/4z1ql63bdc7i)
- [[Refrain] Yuru Campв–і - 10 VOSTFR [1080p]](https://uptobox.com/lcj9ykhsu41b)
- [[Refrain] Yuru Campв–і - 11 VOSTFR [720p]](https://uptobox.com/5f96fs7lhs99)
- [[Refrain] Yuru Campв–і - 11 VOSTFR [1080p]](https://uptobox.com/i5t03clihegn)
- [[Refrain] Yuru Campв–і - 12 VOSTFR [720p]](https://uptobox.com/7mcmmj0c6ea0)
- [[Refrain] Yuru Campв–і - 12 VOSTFR [1080p]](https://uptobox.com/3s9ubgv6n02l)
