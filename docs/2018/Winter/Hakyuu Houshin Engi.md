# Hakyuu Houshin Engi

![Hakyuu Houshin Engi](https://cdn.myanimelist.net/images/anime/11/88796l.jpg)

* Japanese:  覇穹 封神演義

## Information

 - Type: TV
 - Episodes: 23
 - Status: Finished Airing
 - Aired: Jan 12, 2018 to Jun 29, 2018
 - Premiered: Winter 2018
 - Broadcast: Fridays at 22:00 (JST)
 - Producers: NBCUniversal Entertainment Japan, i0+
 - Licensors: Funimation
 - Studios: C-Station
 - Source: Manga
 - Genres: Adventure, Demons, Fantasy, Shounen, Supernatural
 - Duration: 23 min. per ep.
 - Rating: PG-13 - Teens 13 or older


## Links

- [[Refrain] Hakyuu Houshin Engi (2017) - 01 VOSTFR [720p]](https://uptobox.com/ag3pt2qlt881)
- [[Refrain] Hakyuu Houshin Engi (2017) - 01 VOSTFR [1080p]](https://uptobox.com/55y2qa6ooo0x)
- [[Refrain] Hakyuu Houshin Engi (2017) - 02 VOSTFR [720p]](https://uptobox.com/wlnnk91un5k7)
- [[Refrain] Hakyuu Houshin Engi (2017) - 02 VOSTFR [1080p]](https://uptobox.com/6oyxj4j0lptc)
- [[Refrain] Hakyuu Houshin Engi (2017) - 03 VOSTFR [720p]](https://uptobox.com/6gwy0y0x85x8)
- [[Refrain] Hakyuu Houshin Engi (2017) - 03 VOSTFR [1080p]](https://uptobox.com/jp8zcmftbo67)
- [[Refrain] Hakyuu Houshin Engi (2017) - 04 VOSTFR [720p]](https://uptobox.com/oo58dagt9pa5)
- [[Refrain] Hakyuu Houshin Engi (2017) - 04 VOSTFR [1080p]](https://uptobox.com/g79iu7pzwak6)
- [[Refrain] Hakyuu Houshin Engi (2017) - 05 VOSTFR [720p]](https://uptobox.com/2ju27zqvxwxu)
- [[Refrain] Hakyuu Houshin Engi (2017) - 05 VOSTFR [1080p]](https://uptobox.com/hyzckr6gxrlo)
- [[Refrain] Hakyuu Houshin Engi (2017) - 06 VOSTFR [720p]](https://uptobox.com/dlxqjp733b2u)
- [[Refrain] Hakyuu Houshin Engi (2017) - 06 VOSTFR [1080p]](https://uptobox.com/9luy60fe9pwm)
- [[Refrain] Hakyuu Houshin Engi (2017) - 07 VOSTFR [720p]](https://uptobox.com/wanprtchxafu)
- [[Refrain] Hakyuu Houshin Engi (2017) - 07 VOSTFR [1080p]](https://uptobox.com/gn951pvfyrbk)
- [[Refrain] Hakyuu Houshin Engi (2017) - 08 VOSTFR [720p]](https://uptobox.com/13x62pd1yfca)
- [[Refrain] Hakyuu Houshin Engi (2017) - 08 VOSTFR [1080p]](https://uptobox.com/t1tbcqrfzqps)
- [[Refrain] Hakyuu Houshin Engi (2017) - 09 VOSTFR [720p]](https://uptobox.com/gb103omsmipc)
- [[Refrain] Hakyuu Houshin Engi (2017) - 09 VOSTFR [1080p]](https://uptobox.com/6y4n92ujxhvk)
- [[Refrain] Hakyuu Houshin Engi (2017) - 10 VOSTFR [720p]](https://uptobox.com/tkkb5esrhbwm)
- [[Refrain] Hakyuu Houshin Engi (2017) - 10 VOSTFR [1080p]](https://uptobox.com/nwxf52kicj6w)
- [[Refrain] Hakyuu Houshin Engi (2017) - 11 VOSTFR [720p]](https://uptobox.com/1tyqb1undyo1)
- [[Refrain] Hakyuu Houshin Engi (2017) - 11 VOSTFR [1080p]](https://uptobox.com/24tha73izcg7)
- [[Refrain] Hakyuu Houshin Engi (2017) - 12 VOSTFR [720p]](https://uptobox.com/jpayl1hehbmp)
- [[Refrain] Hakyuu Houshin Engi (2017) - 12 VOSTFR [1080p]](https://uptobox.com/odgph7m77mk3)
- [[Refrain] Hakyuu Houshin Engi (2017) - 13 VOSTFR [720p]](https://uptobox.com/1fok86y30d4v)
- [[Refrain] Hakyuu Houshin Engi (2017) - 13 VOSTFR [1080p]](https://uptobox.com/dk5o53bas0pd)
- [[Refrain] Hakyuu Houshin Engi (2017) - 14 VOSTFR [720p]](https://uptobox.com/cch71uprkgat)
- [[Refrain] Hakyuu Houshin Engi (2017) - 14 VOSTFR [1080p]](https://uptobox.com/x5f47i4nr8di)
- [[Refrain] Hakyuu Houshin Engi (2017) - 15 VOSTFR [720p]](https://uptobox.com/ghf410trwm1a)
- [[Refrain] Hakyuu Houshin Engi (2017) - 15 VOSTFR [1080p]](https://uptobox.com/j5hqn42w4zmx)
- [[Refrain] Hakyuu Houshin Engi (2017) - 16 VOSTFR [720p]](https://uptobox.com/os72yunqw041)
- [[Refrain] Hakyuu Houshin Engi (2017) - 16 VOSTFR [1080p]](https://uptobox.com/taezh303cwua)
- [[Refrain] Hakyuu Houshin Engi (2017) - 17 VOSTFR [720p]](https://uptobox.com/z4fjtpf5pv76)
- [[Refrain] Hakyuu Houshin Engi (2017) - 17 VOSTFR [1080p]](https://uptobox.com/raqxydzv40l5)
- [[Refrain] Hakyuu Houshin Engi (2017) - 18 VOSTFR [720p]](https://uptobox.com/jpvlyu5thhzz)
- [[Refrain] Hakyuu Houshin Engi (2017) - 18 VOSTFR [1080p]](https://uptobox.com/zb5hcihi8zgv)
- [[Refrain] Hakyuu Houshin Engi (2017) - 19 VOSTFR [720p]](https://uptobox.com/xmrcn0vht8a7)
- [[Refrain] Hakyuu Houshin Engi (2017) - 19 VOSTFR [1080p]](https://uptobox.com/raob9hnvnlu9)
- [[Refrain] Hakyuu Houshin Engi (2017) - 20 VOSTFR [720p]](https://uptobox.com/8xtgmb634d7g)
- [[Refrain] Hakyuu Houshin Engi (2017) - 20 VOSTFR [1080p]](https://uptobox.com/vndpdi87234m)
- [[Refrain] Hakyuu Houshin Engi (2017) - 21 VOSTFR [720p]](https://uptobox.com/wcr5mnuxoopd)
- [[Refrain] Hakyuu Houshin Engi (2017) - 21 VOSTFR [1080p]](https://uptobox.com/c0db1tydryos)
- [[Refrain] Hakyuu Houshin Engi (2017) - 22 VOSTFR [720p]](https://uptobox.com/0g9pi0w6ojep)
- [[Refrain] Hakyuu Houshin Engi (2017) - 22 VOSTFR [1080p]](https://uptobox.com/6oa64ox77358)
- [[Refrain] Hakyuu Houshin Engi (2017) - 23 VOSTFR [720p]](https://uptobox.com/klfb91ny2u45)
- [[Refrain] Hakyuu Houshin Engi (2017) - 23 VOSTFR [1080p]](https://uptobox.com/zu7bpvegnzbi)
- [[Refrain] Hakyuu Houshin Engi (2017) - 24 VOSTFR [720p]](https://uptobox.com/j722i3hpojkw)
- [[Refrain] Hakyuu Houshin Engi (2017) - 24 VOSTFR [1080p]](https://uptobox.com/6amcuqs9bckn)
