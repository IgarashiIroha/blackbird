# Violet Evergarden

![Violet Evergarden](https://cdn.myanimelist.net/images/anime/1795/95088l.jpg)

* Japanese:  ヴァイオレット・エヴァーガーデン

## Information

 - Type: TV
 - Episodes: 13
 - Status: Finished Airing
 - Aired: Jan 11, 2018 to Apr 5, 2018
 - Premiered: Winter 2018
 - Broadcast: Thursdays at 00:00 (JST)
 - Producers: Lantis, Pony Canyon, Rakuonsha, ABC Animation
 - Licensors: None found, add some
 - Studios: Kyoto Animation
 - Source: Light novel
 - Genres: Fantasy, Drama, Slice of Life
 - Duration: 23 min. per ep.
 - Rating: PG-13 - Teens 13 or older


## Links

- [[Refrain] Violet Evergarden - 01 VF VOSTFR [720p]](https://uptobox.com/cy0vl9k2cqzl)
- [[Refrain] Violet Evergarden - 01 VF VOSTFR [1080p]](https://uptobox.com/tur4g9jim04k)
- [[Refrain] Violet Evergarden - 02 VF VOSTFR [720p]](https://uptobox.com/mqi8gu53je1t)
- [[Refrain] Violet Evergarden - 02 VF VOSTFR [1080p]](https://uptobox.com/fy15cyca7xcb)
- [[Refrain] Violet Evergarden - 03 VF VOSTFR [720p]](https://uptobox.com/tet36vjb1u3d)
- [[Refrain] Violet Evergarden - 03 VF VOSTFR [1080p]](https://uptobox.com/2ulzpyic8o0a)
- [[Refrain] Violet Evergarden - 04 VF VOSTFR [720p]](https://uptobox.com/ehuw83tes1fo)
- [[Refrain] Violet Evergarden - 04 VF VOSTFR [1080p]](https://uptobox.com/rkiebqb1ssya)
- [[Refrain] Violet Evergarden - 05 VF VOSTFR [720p]](https://uptobox.com/7yla7knk925p)
- [[Refrain] Violet Evergarden - 05 VF VOSTFR [1080p]](https://uptobox.com/mkvv7l0gqznd)
- [[Refrain] Violet Evergarden - 06 VF VOSTFR [720p]](https://uptobox.com/cokpu2pxmi1k)
- [[Refrain] Violet Evergarden - 06 VF VOSTFR [1080p]](https://uptobox.com/sswjot7q63p6)
- [[Refrain] Violet Evergarden - 07 VF VOSTFR [720p]](https://uptobox.com/ux0uhztc9gsj)
- [[Refrain] Violet Evergarden - 07 VF VOSTFR [1080p]](https://uptobox.com/dg6aapeti8gs)
- [[Refrain] Violet Evergarden - 08 VF VOSTFR [720p]](https://uptobox.com/j09nog3tue9z)
- [[Refrain] Violet Evergarden - 08 VF VOSTFR [1080p]](https://uptobox.com/uhm0ln47uzp9)
- [[Refrain] Violet Evergarden - 09 VF VOSTFR [720p]](https://uptobox.com/toty4nf47l01)
- [[Refrain] Violet Evergarden - 09 VF VOSTFR [1080p]](https://uptobox.com/v7d56emwweov)
- [[Refrain] Violet Evergarden - 10 VF VOSTFR [720p]](https://uptobox.com/4cel6vz1r96z)
- [[Refrain] Violet Evergarden - 10 VF VOSTFR [1080p]](https://uptobox.com/g2t07xktso8h)
- [[Refrain] Violet Evergarden - 11 VF VOSTFR [720p]](https://uptobox.com/z6dp4qb10awk)
- [[Refrain] Violet Evergarden - 11 VF VOSTFR [1080p]](https://uptobox.com/x26kl90ara94)
- [[Refrain] Violet Evergarden - 12 VF VOSTFR [720p]](https://uptobox.com/erhisbc4vqpq)
- [[Refrain] Violet Evergarden - 12 VF VOSTFR [1080p]](https://uptobox.com/oszqcznemh18)
- [[Refrain] Violet Evergarden - 13 VF VOSTFR [720p]](https://uptobox.com/3fvr1e9ivc76)
- [[Refrain] Violet Evergarden - 13 VF VOSTFR [1080p]](https://uptobox.com/geg3oz1hpr5a)

- [[Pray for nZk] Violet Evergarden - Kitto Koi wo Shiru Hi ga Kuru no Darou [720p] [Multiple Subtitle]](https://uptobox.com/6gkk08438yod)
- [[Pray for nZk] Violet Evergarden - Kitto Koi wo Shiru Hi ga Kuru no Darou [1080p] [Multiple Subtitle]](https://uptobox.com/meqa50q836qi)
