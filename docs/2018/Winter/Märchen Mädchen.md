# Märchen Mädchen

![Märchen Mädchen](https://cdn.myanimelist.net/images/anime/8/90088l.jpg)

* Japanese:  メルヘン・メドヘン

## Information

 - Type: TV
 - Episodes: 10
 - Status: Finished Airing
 - Aired: Jan 11, 2018 to Mar 29, 2018
 - Premiered: Winter 2018
 - Broadcast: Thursdays at 21:00 (JST)
 - Producers: Lantis, AT-X, NBCUniversal Entertainment Japan, Medicos Entertainment, Shueisha, Kanon Sound, DMM.com, NichiNare 
 - Licensors: None found, add some 
 - Studios: Hoods Entertainment 
 - Source: Light novel
 - Genres: Fantasy, Magic, School
 - Duration: 23 min. per ep.
 - Rating: PG-13 - Teens 13 or older


## Links

