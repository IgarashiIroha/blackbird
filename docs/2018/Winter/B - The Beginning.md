# B: The Beginning

![B: The Beginning](https://cdn.myanimelist.net/images/anime/1564/90469l.jpg)

* Japanese:  B: The Beginning

## Information

 - Type: ONA
 - Episodes: 12
 - Status: Finished Airing
 - Aired: Mar 2, 2018
 - Producers: None found, add some
 - Licensors: None found, add some
 - Studios: Production I.G
 - Source: Original
 - Genres: Action, Mystery, Police, Psychological, Supernatural, Thriller
 - Duration: 23 min. per ep.
 - Rating: R - 17+ (violence & profanity)


## Links

- [[Refrain] B The Beginning - 01 VF VOSTFR [720p]](https://uptobox.com/avbxkjjs7yxt)
- [[Refrain] B The Beginning - 01 VF VOSTFR [1080p]](https://uptobox.com/ui55j4rierco)
- [[Refrain] B The Beginning - 02 VF VOSTFR [720p]](https://uptobox.com/g3s92wsc829v)
- [[Refrain] B The Beginning - 02 VF VOSTFR [1080p]](https://uptobox.com/u2mfzg6mlarq)
- [[Refrain] B The Beginning - 03 VF VOSTFR [720p]](https://uptobox.com/rl6dli7oru7a)
- [[Refrain] B The Beginning - 03 VF VOSTFR [1080p]](https://uptobox.com/v4fbr1qrrlr2)
- [[Refrain] B The Beginning - 04 VF VOSTFR [720p]](https://uptobox.com/k4f05boegz4s)
- [[Refrain] B The Beginning - 04 VF VOSTFR [1080p]](https://uptobox.com/w9dpnz0q586d)
- [[Refrain] B The Beginning - 05 VF VOSTFR [720p]](https://uptobox.com/01f9iesw60i5)
- [[Refrain] B The Beginning - 05 VF VOSTFR [1080p]](https://uptobox.com/bmwwrlg6rwnh)
- [[Refrain] B The Beginning - 06 VF VOSTFR [720p]](https://uptobox.com/mlevl5vzuj2e)
- [[Refrain] B The Beginning - 06 VF VOSTFR [1080p]](https://uptobox.com/6i1vs49uotf9)
- [[Refrain] B The Beginning - 07 VF VOSTFR [720p]](https://uptobox.com/469jw2ko2i7h)
- [[Refrain] B The Beginning - 07 VF VOSTFR [1080p]](https://uptobox.com/kojdln98cwpg)
- [[Refrain] B The Beginning - 08 VF VOSTFR [720p]](https://uptobox.com/ospys7c9isn2)
- [[Refrain] B The Beginning - 08 VF VOSTFR [1080p]](https://uptobox.com/gs96dankin6m)
- [[Refrain] B The Beginning - 09 VF VOSTFR [720p]](https://uptobox.com/vq7nryx05dh0)
- [[Refrain] B The Beginning - 09 VF VOSTFR [1080p]](https://uptobox.com/gya7zvq08zl1)
- [[Refrain] B The Beginning - 10 VF VOSTFR [720p]](https://uptobox.com/k0ngvk1l98sw)
- [[Refrain] B The Beginning - 10 VF VOSTFR [1080p]](https://uptobox.com/jpi3iaz5xcix)
- [[Refrain] B The Beginning - 11 VF VOSTFR [720p]](https://uptobox.com/e48d9klzdwx8)
- [[Refrain] B The Beginning - 11 VF VOSTFR [1080p]](https://uptobox.com/rh9tobz18trb)
- [[Refrain] B The Beginning - 12 VF VOSTFR [720p]](https://uptobox.com/5roaqeepwypv)
- [[Refrain] B The Beginning - 12 VF VOSTFR [1080p]](https://uptobox.com/75hnhsiq67bt)
