# Itou Junji: Collection

![Itou Junji: Collection](https://cdn.myanimelist.net/images/anime/7/88366l.jpg)

* Japanese:  伊藤潤二「コレクション」

## Information

 - Type: TV
 - Episodes: 12
 - Status: Finished Airing
 - Aired: Jan 5, 2018 to Mar 23, 2018
 - Premiered: Winter 2018
 - Broadcast: Fridays at 22:30 (JST)
 - Producers: Nippon Columbia, Asahi Shimbun, Yomiuri TV Enterprise, Pony Canyon Enterprise, Smiral Animation, Crunchyroll SC Anime Fund, Muse Communication
 - Licensors: Funimation
 - Studios: Studio Deen
 - Source: Manga
 - Genres: Comedy, Demons, Drama, Horror, Mystery, Psychological, School, Supernatural, Thriller
 - Duration: 24 min. per ep.
 - Rating: R - 17+ (violence & profanity)


## Links

- [[Refrain] IDOLiSH7 - 01 VOSTFR [720p]](https://uptobox.com/qa1l2vhl4k3w)
- [[Refrain] IDOLiSH7 - 01 VOSTFR [1080p]](https://uptobox.com/fg4oe2pr7s34)
- [[Refrain] IDOLiSH7 - 02 VOSTFR [720p]](https://uptobox.com/py257aaeyf6i)
- [[Refrain] IDOLiSH7 - 02 VOSTFR [1080p]](https://uptobox.com/xnui5pv5tjv4)
- [[Refrain] IDOLiSH7 - 03 VOSTFR [720p]](https://uptobox.com/skk04mxdcy42)
- [[Refrain] IDOLiSH7 - 03 VOSTFR [1080p]](https://uptobox.com/f0e324ofzoah)
- [[Refrain] IDOLiSH7 - 04 VOSTFR [720p]](https://uptobox.com/ssqxd9t6pzn3)
- [[Refrain] IDOLiSH7 - 04 VOSTFR [1080p]](https://uptobox.com/s71pof7cixt2)
- [[Refrain] IDOLiSH7 - 05 VOSTFR [720p]](https://uptobox.com/coe1nd635tsc)
- [[Refrain] IDOLiSH7 - 05 VOSTFR [1080p]](https://uptobox.com/lqqcp2r7ph4l)
- [[Refrain] IDOLiSH7 - 06 VOSTFR [720p]](https://uptobox.com/9qlz9q2gsidz)
- [[Refrain] IDOLiSH7 - 06 VOSTFR [1080p]](https://uptobox.com/ui4yy4hf8t97)
- [[Refrain] IDOLiSH7 - 07 VOSTFR [720p]](https://uptobox.com/nyyqss5xuqlh)
- [[Refrain] IDOLiSH7 - 07 VOSTFR [1080p]](https://uptobox.com/dl8n09f3f8j5)
- [[Refrain] IDOLiSH7 - 08 VOSTFR [720p]](https://uptobox.com/8k9afcqrt8sy)
- [[Refrain] IDOLiSH7 - 08 VOSTFR [1080p]](https://uptobox.com/xcc83tbt6sa3)
- [[Refrain] IDOLiSH7 - 09 VOSTFR [720p]](https://uptobox.com/5zkij9zgie4l)
- [[Refrain] IDOLiSH7 - 09 VOSTFR [1080p]](https://uptobox.com/g8cjhxo9c6b1)
- [[Refrain] IDOLiSH7 - 10 VOSTFR [720p]](https://uptobox.com/nxv6b0nlp3t0)
- [[Refrain] IDOLiSH7 - 10 VOSTFR [1080p]](https://uptobox.com/vde5kapiptz9)
- [[Refrain] IDOLiSH7 - 11 VOSTFR [720p]](https://uptobox.com/b6egjgt9807e)
- [[Refrain] IDOLiSH7 - 11 VOSTFR [1080p]](https://uptobox.com/s1jknvf9dbzq)
- [[Refrain] IDOLiSH7 - 12 VOSTFR [720p]](https://uptobox.com/mp8c9ncu0kjf)
- [[Refrain] IDOLiSH7 - 12 VOSTFR [1080p]](https://uptobox.com/7jwg27xpktvx)
- [[Refrain] IDOLiSH7 - 13 VOSTFR [720p]](https://uptobox.com/gto5ffaqrnjj)
- [[Refrain] IDOLiSH7 - 13 VOSTFR [1080p]](https://uptobox.com/5h5rdiueoxli)
- [[Refrain] IDOLiSH7 - 14 VOSTFR [720p]](https://uptobox.com/l68pb7s48fxy)
- [[Refrain] IDOLiSH7 - 14 VOSTFR [1080p]](https://uptobox.com/455qpyoj2hrx)
- [[Refrain] IDOLiSH7 - 15 VOSTFR [720p]](https://uptobox.com/c07ocnl0h4rt)
- [[Refrain] IDOLiSH7 - 15 VOSTFR [1080p]](https://uptobox.com/bgfhbg7d2qof)
- [[Refrain] IDOLiSH7 - 16 VOSTFR [720p]](https://uptobox.com/g846dgyi0l2f)
- [[Refrain] IDOLiSH7 - 16 VOSTFR [1080p]](https://uptobox.com/b7e2zv8ixxpd)
- [[Refrain] IDOLiSH7 - 17 VOSTFR [720p]](https://uptobox.com/qpvfu0r0tv73)
- [[Refrain] IDOLiSH7 - 17 VOSTFR [1080p]](https://uptobox.com/l1nnk6pla5a3)
