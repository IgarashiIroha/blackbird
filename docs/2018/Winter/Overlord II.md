# Overlord II

![Overlord II](https://cdn.myanimelist.net/images/anime/12/89562l.jpg)

* Japanese:  オーバーロードⅡ

## Information

 - Type: TV
 - Episodes: 13
 - Status: Finished Airing
 - Aired: Jan 9, 2018 to Apr 3, 2018
 - Premiered: Winter 2018
 - Broadcast: Tuesdays at 22:30 (JST)
 - Producers: AT-X, Enterbrain, Grooove, Hakuhodo DY Music & Pictures, NTT Plala, Kadokawa 
 - Licensors: Funimation 
 - Studios: Madhouse 
 - Source: Light novel
 - Genres: Action, Adventure, Fantasy, Game, Magic, Supernatural
 - Duration: 23 min. per ep.
 - Rating: R - 17+ (violence & profanity)


## Links

