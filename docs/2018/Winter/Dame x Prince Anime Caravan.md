# Dame x Prince Anime Caravan

![Dame x Prince Anime Caravan](https://cdn.myanimelist.net/images/anime/9/87462l.jpg)

* Japanese:  ダメプリ ANIME CARAVAN

## Information

 - Type: TV
 - Episodes: 12
 - Status: Finished Airing
 - Aired: Jan 10, 2018 to Mar 28, 2018
 - Premiered: Winter 2018
 - Broadcast: Wednesdays at 22:30 (JST)
 - Producers: AZ Creative
 - Licensors: Sentai Filmworks
 - Studios: Studio Flad
 - Source: Visual novel
 - Genres: Adventure, Comedy, Romance
 - Duration: 23 min. per ep.
 - Rating: PG-13 - Teens 13 or older


## Links

- [[Refrain] Dame x Prince Anime Caravan - 01 VOSTFR [720p]](https://uptobox.com/k37s07vulioh)
- [[Refrain] Dame x Prince Anime Caravan - 01 VOSTFR [1080p]](https://uptobox.com/ylvy4p5vlwjn)
- [[Refrain] Dame x Prince Anime Caravan - 02 VOSTFR [720p]](https://uptobox.com/24o6jab6iig1)
- [[Refrain] Dame x Prince Anime Caravan - 02 VOSTFR [1080p]](https://uptobox.com/3c3qogvdxqwe)
- [[Refrain] Dame x Prince Anime Caravan - 03 VOSTFR [720p]](https://uptobox.com/m5isyl9f8qgj)
- [[Refrain] Dame x Prince Anime Caravan - 03 VOSTFR [1080p]](https://uptobox.com/d2kde6532isu)
- [[Refrain] Dame x Prince Anime Caravan - 04 VOSTFR [720p]](https://uptobox.com/9q3fuizkv997)
- [[Refrain] Dame x Prince Anime Caravan - 04 VOSTFR [1080p]](https://uptobox.com/3cy56nrlq72q)
- [[Refrain] Dame x Prince Anime Caravan - 05 VOSTFR [720p]](https://uptobox.com/4s9x1b93b062)
- [[Refrain] Dame x Prince Anime Caravan - 05 VOSTFR [1080p]](https://uptobox.com/0qskpa96r3ku)
- [[Refrain] Dame x Prince Anime Caravan - 06 VOSTFR [720p]](https://uptobox.com/ktab1pe37itj)
- [[Refrain] Dame x Prince Anime Caravan - 06 VOSTFR [1080p]](https://uptobox.com/z4qw4y8wtnpn)
- [[Refrain] Dame x Prince Anime Caravan - 07 VOSTFR [720p]](https://uptobox.com/ck1ylwp9dv1x)
- [[Refrain] Dame x Prince Anime Caravan - 07 VOSTFR [1080p]](https://uptobox.com/p6w0xxlxjbe2)
- [[Refrain] Dame x Prince Anime Caravan - 08 VOSTFR [720p]](https://uptobox.com/k8ecnm55ag0b)
- [[Refrain] Dame x Prince Anime Caravan - 08 VOSTFR [1080p]](https://uptobox.com/xtjsz020hir1)
- [[Refrain] Dame x Prince Anime Caravan - 09 VOSTFR [720p]](https://uptobox.com/czlz89l4yek1)
- [[Refrain] Dame x Prince Anime Caravan - 09 VOSTFR [1080p]](https://uptobox.com/gu48uff6rs8e)
- [[Refrain] Dame x Prince Anime Caravan - 10 VOSTFR [720p]](https://uptobox.com/s7h426xtptxc)
- [[Refrain] Dame x Prince Anime Caravan - 10 VOSTFR [1080p]](https://uptobox.com/qvcnofkp5zwf)
- [[Refrain] Dame x Prince Anime Caravan - 11 VOSTFR [720p]](https://uptobox.com/6qe6z6q6pfwc)
- [[Refrain] Dame x Prince Anime Caravan - 11 VOSTFR [1080p]](https://uptobox.com/o29uof6aam9p)
- [[Refrain] Dame x Prince Anime Caravan - 12 VOSTFR [720p]](https://uptobox.com/dqhij9iy56jh)
- [[Refrain] Dame x Prince Anime Caravan - 12 VOSTFR [1080p]](https://uptobox.com/b7o0fiqguyv0)
