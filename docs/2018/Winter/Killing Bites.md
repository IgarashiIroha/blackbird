# Killing Bites

![Killing Bites](https://cdn.myanimelist.net/images/anime/13/90087l.jpg)

* Japanese:  キリングバイツ

## Information

 - Type: TV
 - Episodes: 12
 - Status: Finished Airing
 - Aired: Jan 13, 2018 to Mar 31, 2018
 - Premiered: Winter 2018
 - Broadcast: Saturdays at 02:25 (JST)
 - Producers: Sotsu, Mainichi Broadcasting System, NBCUniversal Entertainment Japan, Medicos Entertainment, Fields, KLab
 - Licensors: None found, add some
 - Studios: LIDENFILMS
 - Source: Manga
 - Genres: Action, Ecchi, Sci-Fi, Seinen
 - Duration: 24 min. per ep.
 - Rating: R+ - Mild Nudity


## Links

- [[Refrain] Killing Bites - 01 VOSTFR [720p]](https://uptobox.com/lb0tvmcsg635)
- [[Refrain] Killing Bites - 01 VOSTFR [1080p]](https://uptobox.com/94woyh1ze47s)
- [[Refrain] Killing Bites - 02 VOSTFR [720p]](https://uptobox.com/u86qqvwssp94)
- [[Refrain] Killing Bites - 02 VOSTFR [1080p]](https://uptobox.com/rq4xtpdp8kj7)
- [[Refrain] Killing Bites - 03 VOSTFR [720p]](https://uptobox.com/f4mwj43c011y)
- [[Refrain] Killing Bites - 03 VOSTFR [1080p]](https://uptobox.com/v0kccw6qoaez)
- [[Refrain] Killing Bites - 04 VOSTFR [720p]](https://uptobox.com/k4g0mk5s984j)
- [[Refrain] Killing Bites - 04 VOSTFR [1080p]](https://uptobox.com/zsn6crn3rqv7)
- [[Refrain] Killing Bites - 05 VOSTFR [720p]](https://uptobox.com/4fm124oiwlma)
- [[Refrain] Killing Bites - 05 VOSTFR [1080p]](https://uptobox.com/zwzjed741dpd)
- [[Refrain] Killing Bites - 06 VOSTFR [720p]](https://uptobox.com/8tsvgg32i9sd)
- [[Refrain] Killing Bites - 06 VOSTFR [1080p]](https://uptobox.com/4i06l0c70k38)
- [[Refrain] Killing Bites - 07 VOSTFR [720p]](https://uptobox.com/irod74zkdaul)
- [[Refrain] Killing Bites - 07 VOSTFR [1080p]](https://uptobox.com/iked5yg27gjo)
- [[Refrain] Killing Bites - 08 VOSTFR [720p]](https://uptobox.com/ufufylyevnuz)
- [[Refrain] Killing Bites - 08 VOSTFR [1080p]](https://uptobox.com/uogc99jgl7k0)
- [[Refrain] Killing Bites - 09 VOSTFR [720p]](https://uptobox.com/oh7395grufod)
- [[Refrain] Killing Bites - 09 VOSTFR [1080p]](https://uptobox.com/tfnd5t6egm6p)
- [[Refrain] Killing Bites - 10 VOSTFR [720p]](https://uptobox.com/sa76e7ebzhfs)
- [[Refrain] Killing Bites - 10 VOSTFR [1080p]](https://uptobox.com/sweyq5t01p03)
- [[Refrain] Killing Bites - 11 VOSTFR [720p]](https://uptobox.com/qr110fbesrhv)
- [[Refrain] Killing Bites - 11 VOSTFR [1080p]](https://uptobox.com/99lxtyl83fpn)
- [[Refrain] Killing Bites - 12 VOSTFR [720p]](https://uptobox.com/bot02ifznh41)
- [[Refrain] Killing Bites - 12 VOSTFR [1080p]](https://uptobox.com/1kejoscfn063)
