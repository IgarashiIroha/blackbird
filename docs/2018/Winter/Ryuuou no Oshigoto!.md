# Ryuuou no Oshigoto!

![Ryuuou no Oshigoto!](https://cdn.myanimelist.net/images/anime/12/89979l.jpg)

* Japanese:  りゅうおうのおしごと！

## Information

 - Type: TV
 - Episodes: 12
 - Status: Finished Airing
 - Aired: Jan 8, 2018 to Mar 26, 2018
 - Premiered: Winter 2018
 - Broadcast: Mondays at 22:00 (JST)
 - Producers: AT-X, Magic Capsule, Nippon Columbia, KlockWorx, BS Fuji, Tokyo MX, Hakuhodo DY Music & Pictures, Radio Osaka, Aquamarine
 - Licensors: None found, add some
 - Studios: Project No.9
 - Source: Light novel
 - Genres: Slice of Life, Game, Comedy
 - Duration: 23 min. per ep.
 - Rating: PG-13 - Teens 13 or older


## Links

- [[Refrain] Ryuuou no Oshigoto! - 01 VOSTFR [720p]](https://uptobox.com/zp5il5l9j5he)
- [[Refrain] Ryuuou no Oshigoto! - 01 VOSTFR [1080p]](https://uptobox.com/q3dm2bz00xg5)
- [[Refrain] Ryuuou no Oshigoto! - 02 VOSTFR [720p]](https://uptobox.com/lvfll3pkw4n3)
- [[Refrain] Ryuuou no Oshigoto! - 02 VOSTFR [1080p]](https://uptobox.com/1izdhwvv33dg)
- [[Refrain] Ryuuou no Oshigoto! - 03 VOSTFR [720p]](https://uptobox.com/8p7z8dtme4nb)
- [[Refrain] Ryuuou no Oshigoto! - 03 VOSTFR [1080p]](https://uptobox.com/o0isnq0uwwje)
- [[Refrain] Ryuuou no Oshigoto! - 04 VOSTFR [720p]](https://uptobox.com/etgkgxjnt4iz)
- [[Refrain] Ryuuou no Oshigoto! - 04 VOSTFR [1080p]](https://uptobox.com/fg3ehc639w8p)
- [[Refrain] Ryuuou no Oshigoto! - 05 VOSTFR [720p]](https://uptobox.com/a1sa0mzbefob)
- [[Refrain] Ryuuou no Oshigoto! - 05 VOSTFR [1080p]](https://uptobox.com/t8slt3t5d0xp)
- [[Refrain] Ryuuou no Oshigoto! - 06 VOSTFR [720p]](https://uptobox.com/yz1qhea1u2v0)
- [[Refrain] Ryuuou no Oshigoto! - 06 VOSTFR [1080p]](https://uptobox.com/t0xak1h0db7v)
- [[Refrain] Ryuuou no Oshigoto! - 07 VOSTFR [720p]](https://uptobox.com/whmad61hrgge)
- [[Refrain] Ryuuou no Oshigoto! - 07 VOSTFR [1080p]](https://uptobox.com/92glg84whmv7)
- [[Refrain] Ryuuou no Oshigoto! - 08 VOSTFR [720p]](https://uptobox.com/m2f4z3g1w8t0)
- [[Refrain] Ryuuou no Oshigoto! - 08 VOSTFR [1080p]](https://uptobox.com/5u89y81dbkbo)
- [[Refrain] Ryuuou no Oshigoto! - 09 VOSTFR [720p]](https://uptobox.com/wtswls6219pe)
- [[Refrain] Ryuuou no Oshigoto! - 09 VOSTFR [1080p]](https://uptobox.com/akn1m82u1b82)
- [[Refrain] Ryuuou no Oshigoto! - 10 VOSTFR [720p]](https://uptobox.com/d9ioiefeyrhb)
- [[Refrain] Ryuuou no Oshigoto! - 10 VOSTFR [1080p]](https://uptobox.com/yq11bton8r8n)
- [[Refrain] Ryuuou no Oshigoto! - 11 VOSTFR [720p]](https://uptobox.com/tte7s2nr3fn7)
- [[Refrain] Ryuuou no Oshigoto! - 11 VOSTFR [1080p]](https://uptobox.com/qp0tmv9qwz1o)
- [[Refrain] Ryuuou no Oshigoto! - 12 VOSTFR [720p]](https://uptobox.com/up65jg5ikv5r)
- [[Refrain] Ryuuou no Oshigoto! - 12 VOSTFR [1080p]](https://uptobox.com/6y6a3pm4ta2y)
