# Darling in the FranXX

![Darling in the FranXX](https://cdn.myanimelist.net/images/anime/1614/90408l.jpg)

* Japanese:  ダーリン・イン・ザ・フランキス

## Information

 - Type: TV
 - Episodes: 24
 - Status: Finished Airing
 - Aired: Jan 13, 2018 to Jul 7, 2018
 - Premiered: Winter 2018
 - Broadcast: Saturdays at 23:30 (JST)
 - Producers: Aniplex, Khara, Movic, Ultra Super Pictures, Lucent Pictures Entertainment, Tokyo MX, Nagoya Broadcasting Network, Asahi Broadcasting, BS11, Lawson, ABC Animation
 - Licensors: Funimation
 - Studios: A-1 Pictures, Trigger, CloverWorks
 - Source: Original
 - Genres: Action, Drama, Mecha, Romance, Sci-Fi
 - Duration: 24 min. per ep.
 - Rating: PG-13 - Teens 13 or older


## Links

- [[Refrain] Darling in the FranXX - 01 VOSTFR [720p]](https://uptobox.com/cu0jr114c1b0)
- [[Refrain] Darling in the FranXX - 01 VOSTFR [720p]](https://uptobox.com/jzfqbainhe7y)
- [[Refrain] Darling in the FranXX - 01 VOSTFR [1080p]](https://uptobox.com/u76kt6img2y6)
- [[Refrain] Darling in the FranXX - 01 VOSTFR [1080p]](https://uptobox.com/t9jozwkxf9dz)
- [[Refrain] Darling in the FranXX - 02 VOSTFR [720p]](https://uptobox.com/vdosm8dqfjv1)
- [[Refrain] Darling in the FranXX - 02 VOSTFR [720p]](https://uptobox.com/pezqffyxfi7o)
- [[Refrain] Darling in the FranXX - 02 VOSTFR [1080p]](https://uptobox.com/ezwpbmc016tx)
- [[Refrain] Darling in the FranXX - 02 VOSTFR [1080p]](https://uptobox.com/yugon6d3sd01)
- [[Refrain] Darling in the FranXX - 03 VOSTFR [720p]](https://uptobox.com/05jtoahs7kv5)
- [[Refrain] Darling in the FranXX - 03 VOSTFR [720p]](https://uptobox.com/ts550szybe2e)
- [[Refrain] Darling in the FranXX - 03 VOSTFR [1080p]](https://uptobox.com/4nah1v96np8f)
- [[Refrain] Darling in the FranXX - 03 VOSTFR [1080p]](https://uptobox.com/nkxwk3fgqa8h)
- [[Refrain] Darling in the FranXX - 04 VOSTFR [720p]](https://uptobox.com/pzhfffi6p2w2)
- [[Refrain] Darling in the FranXX - 04 VOSTFR [720p]](https://uptobox.com/iajyq6rzxak1)
- [[Refrain] Darling in the FranXX - 04 VOSTFR [1080p]](https://uptobox.com/lpx9pywc8z7m)
- [[Refrain] Darling in the FranXX - 04 VOSTFR [1080p]](https://uptobox.com/j0jrmip8zjtt)
- [[Refrain] Darling in the FranXX - 05 VOSTFR [720p]](https://uptobox.com/3mm6yx86og0m)
- [[Refrain] Darling in the FranXX - 05 VOSTFR [720p]](https://uptobox.com/aseuao33imir)
- [[Refrain] Darling in the FranXX - 05 VOSTFR [1080p]](https://uptobox.com/mjlhb0yo6fg9)
- [[Refrain] Darling in the FranXX - 05 VOSTFR [1080p]](https://uptobox.com/oibd9o6gb3h5)
- [[Refrain] Darling in the FranXX - 06 VOSTFR [720p]](https://uptobox.com/pdluftugoj2j)
- [[Refrain] Darling in the FranXX - 06 VOSTFR [720p]](https://uptobox.com/nvx5mtrqo3i1)
- [[Refrain] Darling in the FranXX - 06 VOSTFR [1080p]](https://uptobox.com/4jy307knjdi7)
- [[Refrain] Darling in the FranXX - 06 VOSTFR [1080p]](https://uptobox.com/z204x0ck0e13)
- [[Refrain] Darling in the FranXX - 07 VOSTFR [720p]](https://uptobox.com/6t3fuxl9ntwe)
- [[Refrain] Darling in the FranXX - 07 VOSTFR [720p]](https://uptobox.com/rhk51btd6mpq)
- [[Refrain] Darling in the FranXX - 07 VOSTFR [1080p]](https://uptobox.com/wd0r354imwl2)
- [[Refrain] Darling in the FranXX - 07 VOSTFR [1080p]](https://uptobox.com/j8e11c6vtuxn)
- [[Refrain] Darling in the FranXX - 08 VOSTFR [720p]](https://uptobox.com/pwoq2tbi826n)
- [[Refrain] Darling in the FranXX - 08 VOSTFR [720p]](https://uptobox.com/apv5r1jevbl9)
- [[Refrain] Darling in the FranXX - 08 VOSTFR [1080p]](https://uptobox.com/k90zcyi9wece)
- [[Refrain] Darling in the FranXX - 08 VOSTFR [1080p]](https://uptobox.com/fersdrvnwmfb)
- [[Refrain] Darling in the FranXX - 09 VOSTFR [720p]](https://uptobox.com/pipo2bnneyya)
- [[Refrain] Darling in the FranXX - 09 VOSTFR [720p]](https://uptobox.com/onn3oaiiqpjc)
- [[Refrain] Darling in the FranXX - 09 VOSTFR [1080p]](https://uptobox.com/0b57xqum32fo)
- [[Refrain] Darling in the FranXX - 09 VOSTFR [1080p]](https://uptobox.com/wb4ss2kf0kpu)
- [[Refrain] Darling in the FranXX - 10 VOSTFR [720p]](https://uptobox.com/mqjkh4tmhiyl)
- [[Refrain] Darling in the FranXX - 10 VOSTFR [720p]](https://uptobox.com/aovqcdwy8wd6)
- [[Refrain] Darling in the FranXX - 10 VOSTFR [1080p]](https://uptobox.com/amumnw5e7hp7)
- [[Refrain] Darling in the FranXX - 10 VOSTFR [1080p]](https://uptobox.com/vph82pq1dsgu)
- [[Refrain] Darling in the FranXX - 11 VOSTFR [720p]](https://uptobox.com/9uloashi1suq)
- [[Refrain] Darling in the FranXX - 11 VOSTFR [720p]](https://uptobox.com/rfzow4ylgpxz)
- [[Refrain] Darling in the FranXX - 11 VOSTFR [1080p]](https://uptobox.com/rce889e8tm67)
- [[Refrain] Darling in the FranXX - 11 VOSTFR [1080p]](https://uptobox.com/42wyxf00uxlf)
- [[Refrain] Darling in the FranXX - 12 VOSTFR [720p]](https://uptobox.com/hu1zzow638j3)
- [[Refrain] Darling in the FranXX - 12 VOSTFR [720p]](https://uptobox.com/m4gbdtarnnt3)
- [[Refrain] Darling in the FranXX - 12 VOSTFR [1080p]](https://uptobox.com/cddbfppp0pbz)
- [[Refrain] Darling in the FranXX - 12 VOSTFR [1080p]](https://uptobox.com/50okyemjlq38)
- [[Refrain] Darling in the FranXX - 13 VOSTFR [720p]](https://uptobox.com/69j5wkl3j7ig)
- [[Refrain] Darling in the FranXX - 13 VOSTFR [1080p]](https://uptobox.com/zz7ubp0m5upf)
- [[Refrain] Darling in the FranXX - 14 VOSTFR [720p]](https://uptobox.com/yndd0ketst6t)
- [[Refrain] Darling in the FranXX - 14 VOSTFR [1080p]](https://uptobox.com/gb6tvpd08vfa)
- [[Refrain] Darling in the FranXX - 15 VOSTFR [720p]](https://uptobox.com/ncxsyjc8qrdb)
- [[Refrain] Darling in the FranXX - 15 VOSTFR [1080p]](https://uptobox.com/bnytbqityk5b)
- [[Refrain] Darling in the FranXX - 16 VOSTFR [720p]](https://uptobox.com/y0t5dznb946n)
- [[Refrain] Darling in the FranXX - 16 VOSTFR [1080p]](https://uptobox.com/4dv57n4dsth9)
- [[Refrain] Darling in the FranXX - 17 VOSTFR [720p]](https://uptobox.com/cuw5cjfu229w)
- [[Refrain] Darling in the FranXX - 17 VOSTFR [1080p]](https://uptobox.com/m2v47qxmswpu)
- [[Refrain] Darling in the FranXX - 18 VOSTFR [720p]](https://uptobox.com/dz4bpnwco3px)
- [[Refrain] Darling in the FranXX - 18 VOSTFR [1080p]](https://uptobox.com/cuow2sbuudqi)
- [[Refrain] Darling in the FranXX - 19 VOSTFR [720p]](https://uptobox.com/tnu03niqrhtb)
- [[Refrain] Darling in the FranXX - 19 VOSTFR [1080p]](https://uptobox.com/66rpl9zzzuol)
- [[Refrain] Darling in the FranXX - 20 VOSTFR [720p]](https://uptobox.com/3zq2kouq97if)
- [[Refrain] Darling in the FranXX - 20 VOSTFR [1080p]](https://uptobox.com/ezcv6z9txj2e)
- [[Refrain] Darling in the FranXX - 21 VOSTFR [720p]](https://uptobox.com/lzmlkmrv4wxi)
- [[Refrain] Darling in the FranXX - 21 VOSTFR [1080p]](https://uptobox.com/79b5viz0bzwz)
- [[Refrain] Darling in the FranXX - 22 VOSTFR [720p]](https://uptobox.com/vgr9c5ilq6nk)
- [[Refrain] Darling in the FranXX - 22 VOSTFR [1080p]](https://uptobox.com/m52ihci5gjzm)
- [[Refrain] Darling in the FranXX - 23 VOSTFR [720p]](https://uptobox.com/66afuut4t12h)
- [[Refrain] Darling in the FranXX - 23 VOSTFR [1080p]](https://uptobox.com/8a9jfcxkkecu)
- [[Refrain] Darling in the FranXX - 24 VOSTFR [720p]](https://uptobox.com/59puml6y4q1e)
- [[Refrain] Darling in the FranXX - 24 VOSTFR [1080p]](https://uptobox.com/ppe4vizi8azm)
