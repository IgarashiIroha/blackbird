# Grancrest Senki

![Grancrest Senki](https://cdn.myanimelist.net/images/anime/4/89883l.jpg)

* Japanese:  グランクレスト戦記

## Information

 - Type: TV
 - Episodes: 24
 - Status: Finished Airing
 - Aired: Jan 6, 2018 to Jun 23, 2018
 - Premiered: Winter 2018
 - Broadcast: Saturdays at 00:00 (JST)
 - Producers: Aniplex, Bandai Namco Entertainment, Good Smile Company, BS11, ABC Animation, Kinoshita Group Holdings, Kadokawa, Cromea, Sonilude
 - Licensors: Aniplex of America
 - Studios: A-1 Pictures
 - Source: Light novel
 - Genres: Action, Drama, Fantasy, Romance
 - Duration: 23 min. per ep.
 - Rating: R - 17+ (violence & profanity)


## Links

- [[Refrain] Grancrest Senki - 01 VOSTFR [720p]](https://uptobox.com/v43zp6phhtk0)
- [[Refrain] Grancrest Senki - 01 VOSTFR [1080p]](https://uptobox.com/4k1kzpnnze35)
- [[Refrain] Grancrest Senki - 02 VOSTFR [720p]](https://uptobox.com/hh57dwlvg72a)
- [[Refrain] Grancrest Senki - 02 VOSTFR [1080p]](https://uptobox.com/9odxjnuyod4h)
- [[Refrain] Grancrest Senki - 03 VOSTFR [720p]](https://uptobox.com/b52r1qauw5g3)
- [[Refrain] Grancrest Senki - 03 VOSTFR [1080p]](https://uptobox.com/3fv1cjjdwxpu)
- [[Refrain] Grancrest Senki - 04 VOSTFR [720p]](https://uptobox.com/uj4spmp4jm44)
- [[Refrain] Grancrest Senki - 04 VOSTFR [1080p]](https://uptobox.com/0h1b86vko90c)
- [[Refrain] Grancrest Senki - 05 VOSTFR [720p]](https://uptobox.com/1auchlw39m1q)
- [[Refrain] Grancrest Senki - 05 VOSTFR [1080p]](https://uptobox.com/lnnh2m4dgh1r)
- [[Refrain] Grancrest Senki - 06 VOSTFR [720p]](https://uptobox.com/ggxvfjmte1vl)
- [[Refrain] Grancrest Senki - 06 VOSTFR [1080p]](https://uptobox.com/kcdmmbzmbdp9)
- [[Refrain] Grancrest Senki - 07 VOSTFR [720p]](https://uptobox.com/79mrpqe2jo5r)
- [[Refrain] Grancrest Senki - 07 VOSTFR [1080p]](https://uptobox.com/eks4df7dqvvg)
- [[Refrain] Grancrest Senki - 08 VOSTFR [720p]](https://uptobox.com/szyw3hhdhfl3)
- [[Refrain] Grancrest Senki - 08 VOSTFR [1080p]](https://uptobox.com/sqgvc4e8smw5)
- [[Refrain] Grancrest Senki - 09 VOSTFR [720p]](https://uptobox.com/xc5h00p1677v)
- [[Refrain] Grancrest Senki - 09 VOSTFR [1080p]](https://uptobox.com/t6x186pqjumk)
- [[Refrain] Grancrest Senki - 10 VOSTFR [720p]](https://uptobox.com/6aoev9z75hwe)
- [[Refrain] Grancrest Senki - 10 VOSTFR [1080p]](https://uptobox.com/vc93nyetimwh)
- [[Refrain] Grancrest Senki - 11 VOSTFR [720p]](https://uptobox.com/l6807crwkl42)
- [[Refrain] Grancrest Senki - 11 VOSTFR [1080p]](https://uptobox.com/0pk1raelehwc)
- [[Refrain] Grancrest Senki - 12 VOSTFR [720p]](https://uptobox.com/m1cpbkrs5e83)
- [[Refrain] Grancrest Senki - 12 VOSTFR [1080p]](https://uptobox.com/p914tgg4cv8o)
- [[Refrain] Grancrest Senki - 13 VOSTFR [720p]](https://uptobox.com/fdnl5myuffz1)
- [[Refrain] Grancrest Senki - 13 VOSTFR [1080p]](https://uptobox.com/oon70v2zs663)
- [[Refrain] Grancrest Senki - 14 VOSTFR [720p]](https://uptobox.com/gd2rezl05uu9)
- [[Refrain] Grancrest Senki - 14 VOSTFR [1080p]](https://uptobox.com/fl3lwzpgr9rj)
- [[Refrain] Grancrest Senki - 15 VOSTFR [720p]](https://uptobox.com/tau8k786u50k)
- [[Refrain] Grancrest Senki - 15 VOSTFR [1080p]](https://uptobox.com/sc2okqq2syx9)
- [[Refrain] Grancrest Senki - 16 VOSTFR [720p]](https://uptobox.com/ehvs2adsmvcv)
- [[Refrain] Grancrest Senki - 16 VOSTFR [1080p]](https://uptobox.com/dyzepgsbufri)
- [[Refrain] Grancrest Senki - 17 VOSTFR [720p]](https://uptobox.com/sc8juw83sy5z)
- [[Refrain] Grancrest Senki - 17 VOSTFR [1080p]](https://uptobox.com/wf1gpy91bd8s)
- [[Refrain] Grancrest Senki - 18 VOSTFR [720p]](https://uptobox.com/42podht8jts2)
- [[Refrain] Grancrest Senki - 18 VOSTFR [1080p]](https://uptobox.com/zhplezyh2qbs)
- [[Refrain] Grancrest Senki - 19 VOSTFR [720p]](https://uptobox.com/6hrh59whzryq)
- [[Refrain] Grancrest Senki - 19 VOSTFR [1080p]](https://uptobox.com/7yxy9gr19g2c)
- [[Refrain] Grancrest Senki - 20 VOSTFR [720p]](https://uptobox.com/byhwnejx4oq5)
- [[Refrain] Grancrest Senki - 20 VOSTFR [1080p]](https://uptobox.com/d34sp9k3trny)
- [[Refrain] Grancrest Senki - 21 VOSTFR [720p]](https://uptobox.com/ovxc05ww547f)
- [[Refrain] Grancrest Senki - 21 VOSTFR [1080p]](https://uptobox.com/b6chta2l9vow)
- [[Refrain] Grancrest Senki - 22 VOSTFR [720p]](https://uptobox.com/229t72bhf3f3)
- [[Refrain] Grancrest Senki - 22 VOSTFR [1080p]](https://uptobox.com/0kez6di71z8v)
- [[Refrain] Grancrest Senki - 23 VOSTFR [720p]](https://uptobox.com/zohrq6njtplg)
- [[Refrain] Grancrest Senki - 23 VOSTFR [1080p]](https://uptobox.com/n344x3syb5ee)
- [[Refrain] Grancrest Senki - 24 VOSTFR [720p]](https://uptobox.com/o6yq66okw6hw)
- [[Refrain] Grancrest Senki - 24 VOSTFR [1080p]](https://uptobox.com/oktskfaweotv)
