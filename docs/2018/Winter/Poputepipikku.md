# Poputepipikku

![Poputepipikku](https://cdn.myanimelist.net/images/anime/3/88816l.jpg)

* Japanese:  ポプテピピック

## Information

 - Type: TV
 - Episodes: 12
 - Status: Finished Airing
 - Aired: Jan 7, 2018 to Mar 25, 2018
 - Premiered: Winter 2018
 - Broadcast: Sundays at 01:00 (JST)
 - Producers: King Records
 - Licensors: Funimation, Sentai Filmworks
 - Studios: Kamikaze Douga
 - Source: 4-koma manga
 - Genres: Comedy, Parody, Dementia
 - Duration: 12 min. per ep.
 - Rating: PG-13 - Teens 13 or older


## Links

- [[Refrain] Poputepipikku - 01 VOSTFR [720p]](https://uptobox.com/orb6hzxiixmb)
- [[Refrain] Poputepipikku - 01 VOSTFR [1080p]](https://uptobox.com/xk9lsyh0abh7)
- [[Refrain] Poputepipikku - 02 VOSTFR [720p]](https://uptobox.com/ow1uqwo3vrp6)
- [[Refrain] Poputepipikku - 02 VOSTFR [1080p]](https://uptobox.com/9lgm9zlamrv4)
- [[Refrain] Poputepipikku - 03 VOSTFR [720p]](https://uptobox.com/qi9bd9a4tboa)
- [[Refrain] Poputepipikku - 03 VOSTFR [1080p]](https://uptobox.com/7mpq0k4x6fo5)
- [[Refrain] Poputepipikku - 04 VOSTFR [720p]](https://uptobox.com/y5en3llolyza)
- [[Refrain] Poputepipikku - 04 VOSTFR [1080p]](https://uptobox.com/6scdedoxjjnq)
- [[Refrain] Poputepipikku - 05 VOSTFR [720p]](https://uptobox.com/hfjjzzpbwrjm)
- [[Refrain] Poputepipikku - 05 VOSTFR [1080p]](https://uptobox.com/x7vr43is4mkq)
- [[Refrain] Poputepipikku - 06 VOSTFR [720p]](https://uptobox.com/cuq905seytv4)
- [[Refrain] Poputepipikku - 06 VOSTFR [1080p]](https://uptobox.com/g2btsko920un)
- [[Refrain] Poputepipikku - 07 VOSTFR [720p]](https://uptobox.com/3orjqox2qib9)
- [[Refrain] Poputepipikku - 07 VOSTFR [1080p]](https://uptobox.com/1e7zxvd2i4mx)
- [[Refrain] Poputepipikku - 08 VOSTFR [720p]](https://uptobox.com/re85v61ms3vn)
- [[Refrain] Poputepipikku - 08 VOSTFR [1080p]](https://uptobox.com/xaa2qdf8ydd7)
- [[Refrain] Poputepipikku - 09 VOSTFR [720p]](https://uptobox.com/p28soyjjhb2t)
- [[Refrain] Poputepipikku - 09 VOSTFR [1080p]](https://uptobox.com/vhfjin2n5urn)
- [[Refrain] Poputepipikku - 10 VOSTFR [720p]](https://uptobox.com/4zhxh3de2ytb)
- [[Refrain] Poputepipikku - 10 VOSTFR [1080p]](https://uptobox.com/pu6zals3kmoa)
- [[Refrain] Poputepipikku - 11 VOSTFR [720p]](https://uptobox.com/upyc5smkfxe8)
- [[Refrain] Poputepipikku - 11 VOSTFR [1080p]](https://uptobox.com/ejwl0vj1t3n6)
- [[Refrain] Poputepipikku - 12 VOSTFR [720p]](https://uptobox.com/xq12i5as5syb)
- [[Refrain] Poputepipikku - 12 VOSTFR [1080p]](https://uptobox.com/0w85yghjj2tx)
- [[Refrain] Poputepipikku - Blue Dragon ver - 00 VOSTFR [720p]](https://uptobox.com/vpcgca8yjpxs)
- [[Refrain] Poputepipikku - Blue Dragon ver - 00 VOSTFR [1080p]](https://uptobox.com/vmwdgfrcztes)
- [[Refrain] Poputepipikku - White Tiger ver - 00 VOSTFR [720p]](https://uptobox.com/ef8kp5pktidd)
- [[Refrain] Poputepipikku - White Tiger ver - 00 VOSTFR [1080p]](https://uptobox.com/zjfs3xrdqbhf)
- [[Refrain] Poputepipikku - Vermilion Bird ver - 00 VOSTFR [720p]](https://uptobox.com/lfyomx6qpo3r)
- [[Refrain] Poputepipikku - Vermilion Bird ver - 00 VOSTFR [1080p]](https://uptobox.com/bf0pk3h18qth)
- [[Refrain] Poputepipikku - Black Tortoise ver - 00 VOSTFR [720p]](https://uptobox.com/fl1kihy30h9q)
- [[Refrain] Poputepipikku - Black Tortoise ver - 00 VOSTFR [1080p]](https://uptobox.com/w6fty3l0b4ew)
