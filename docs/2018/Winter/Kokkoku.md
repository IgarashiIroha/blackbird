# Kokkoku

![Kokkoku](https://cdn.myanimelist.net/images/anime/8/89987l.jpg)

* Japanese:  刻刻

## Information

 - Type: TV
 - Episodes: 12
 - Status: Finished Airing
 - Aired: Jan 8, 2018 to Mar 26, 2018
 - Premiered: Winter 2018
 - Broadcast: Mondays at 00:30 (JST)
 - Producers: Twin Engine
 - Licensors: None found, add some
 - Studios: Geno Studio
 - Source: Manga
 - Genres: Psychological, Supernatural, Drama, Mystery, Seinen
 - Duration: 23 min. per ep.
 - Rating: R - 17+ (violence & profanity)


## Links

- [[Refrain] Kokkoku - 01 VOSTFR [720p]](https://uptobox.com/7mdmnreknfud)
- [[Refrain] Kokkoku - 01 VOSTFR [1080p]](https://uptobox.com/t35sz6h30grr)
- [[Refrain] Kokkoku - 02 VOSTFR [720p]](https://uptobox.com/thgz1ms98qsc)
- [[Refrain] Kokkoku - 02 VOSTFR [1080p]](https://uptobox.com/1btuldayjdl1)
- [[Refrain] Kokkoku - 03 VOSTFR [720p]](https://uptobox.com/ntr2a371me0j)
- [[Refrain] Kokkoku - 03 VOSTFR [1080p]](https://uptobox.com/9wj36lvkb6zj)
- [[Refrain] Kokkoku - 04 VOSTFR [720p]](https://uptobox.com/xmph3triqb7h)
- [[Refrain] Kokkoku - 04 VOSTFR [1080p]](https://uptobox.com/1uv72wi26jr9)
- [[Refrain] Kokkoku - 05 VOSTFR [720p]](https://uptobox.com/nbdv9ope1uiu)
- [[Refrain] Kokkoku - 05 VOSTFR [1080p]](https://uptobox.com/vgp3ibcowvw8)
- [[Refrain] Kokkoku - 06 VOSTFR [720p]](https://uptobox.com/xip3in8yf5pd)
- [[Refrain] Kokkoku - 06 VOSTFR [1080p]](https://uptobox.com/zb4uld09ns8b)
- [[Refrain] Kokkoku - 07 VOSTR [720p]](https://uptobox.com/g7wswl482971)
- [[Refrain] Kokkoku - 07 VOSTR [1080p]](https://uptobox.com/fo4r6arvyj8m)
- [[Refrain] Kokkoku - 08 VOSTFR [720p]](https://uptobox.com/wjbe2fg3vjjo)
- [[Refrain] Kokkoku - 08 VOSTFR [1080p]](https://uptobox.com/mhfmf9k9q7rj)
- [[Refrain] Kokkoku - 09 VOSTFR [720p]](https://uptobox.com/f9ngjw3a64em)
- [[Refrain] Kokkoku - 09 VOSTFR [1080p]](https://uptobox.com/jnpfrwmqrwu0)
- [[Refrain] Kokkoku - 10 VOSTFR [720p]](https://uptobox.com/ue71q1dqnf8d)
- [[Refrain] Kokkoku - 10 VOSTFR [1080p]](https://uptobox.com/aoydgdmazz9i)
- [[Refrain] Kokkoku - 11 VOSTFR [720p]](https://uptobox.com/uqljaeo02szg)
- [[Refrain] Kokkoku - 11 VOSTFR [1080p]](https://uptobox.com/m77nbewyaeb2)
- [[Refrain] Kokkoku - 12 VOSTFR [720p]](https://uptobox.com/46bl98ek32y8)
- [[Refrain] Kokkoku - 12 VOSTFR [1080p]](https://uptobox.com/wdj743f8omme)
