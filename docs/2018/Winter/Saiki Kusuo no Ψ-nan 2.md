# Saiki Kusuo no Ψ-nan 2

![Saiki Kusuo no Ψ-nan 2](https://cdn.myanimelist.net/images/anime/1961/91383l.jpg)

* Japanese:  斉木楠雄のΨ難 2

## Information

 - Type: TV
 - Episodes: 24
 - Status: Finished Airing
 - Aired: Jan 17, 2018 to Jun 27, 2018
 - Premiered: Winter 2018
 - Broadcast: Wednesdays at 01:35 (JST)
 - Producers: TV Tokyo, Shogakukan Productions, Nippon Columbia, KlockWorx, Asmik Ace, Bandai Namco Entertainment, Shueisha, Toy&#039;s Factory, Dear Stage inc.
 - Licensors: None found, add some
 - Studios: J.C.Staff, Egg Firm
 - Source: Manga
 - Genres: Comedy, School, Shounen, Slice of Life, Supernatural
 - Duration: 24 min. per ep.
 - Rating: PG-13 - Teens 13 or older


## Links

- [[Refrain] Saiki Kusuo no ОЁ-nan (TV) 2 - 01 VOSTFR [720p]](https://uptobox.com/j982yk8smah3)
- [[Refrain] Saiki Kusuo no ОЁ-nan (TV) 2 - 01 VOSTFR [1080p]](https://uptobox.com/3wx1q8nlc6z7)
- [[Refrain] Saiki Kusuo no ОЁ-nan (TV) 2 - 02 VOSTFR [720p]](https://uptobox.com/vnrv5nxq3hrz)
- [[Refrain] Saiki Kusuo no ОЁ-nan (TV) 2 - 02 VOSTFR [1080p]](https://uptobox.com/kihxd2137nsk)
- [[Refrain] Saiki Kusuo no ОЁ-nan (TV) 2 - 03 VOSTFR [720p]](https://uptobox.com/6gxe9v0owd87)
- [[Refrain] Saiki Kusuo no ОЁ-nan (TV) 2 - 03 VOSTFR [1080p]](https://uptobox.com/z9t5t1jaafc4)
- [[Refrain] Saiki Kusuo no ОЁ-nan (TV) 2 - 04 VOSTFR [720p]](https://uptobox.com/egfq3p1fb10n)
- [[Refrain] Saiki Kusuo no ОЁ-nan (TV) 2 - 04 VOSTFR [1080p]](https://uptobox.com/tqe9up51brvc)
- [[Refrain] Saiki Kusuo no ОЁ-nan (TV) 2 - 05 VOSTFR [720p]](https://uptobox.com/bqfo0audx9xb)
- [[Refrain] Saiki Kusuo no ОЁ-nan (TV) 2 - 05 VOSTFR [1080p]](https://uptobox.com/jj05hgxbzc1l)
- [[Refrain] Saiki Kusuo no ОЁ-nan (TV) 2 - 06 VOSTFR [720p]](https://uptobox.com/rqz1c1tq974t)
- [[Refrain] Saiki Kusuo no ОЁ-nan (TV) 2 - 06 VOSTFR [1080p]](https://uptobox.com/sbhp8idnndvz)
- [[Refrain] Saiki Kusuo no ОЁ-nan (TV) 2 - 07 VOSTFR [720p]](https://uptobox.com/fpeuqd8r1sla)
- [[Refrain] Saiki Kusuo no ОЁ-nan (TV) 2 - 07 VOSTFR [1080p]](https://uptobox.com/cty7jb68lha1)
- [[Refrain] Saiki Kusuo no ОЁ-nan (TV) 2 - 08 VOSTFR [720p]](https://uptobox.com/aqb0bgeqaotj)
- [[Refrain] Saiki Kusuo no ОЁ-nan (TV) 2 - 08 VOSTFR [1080p]](https://uptobox.com/gwrtnn3um584)
- [[Refrain] Saiki Kusuo no ОЁ-nan (TV) 2 - 09 VOSTFR [720p]](https://uptobox.com/7zwt247u4b2b)
- [[Refrain] Saiki Kusuo no ОЁ-nan (TV) 2 - 09 VOSTFR [1080p]](https://uptobox.com/tfpfanxzpkr7)
- [[Refrain] Saiki Kusuo no ОЁ-nan (TV) 2 - 10 VOSTFR [720p]](https://uptobox.com/kub8oq5dmce9)
- [[Refrain] Saiki Kusuo no ОЁ-nan (TV) 2 - 10 VOSTFR [1080p]](https://uptobox.com/dd51kmsp66j1)
- [[Refrain] Saiki Kusuo no ОЁ-nan (TV) 2 - 11 VOSTFR [720p]](https://uptobox.com/86uxzb4v7jn6)
- [[Refrain] Saiki Kusuo no ОЁ-nan (TV) 2 - 11 VOSTFR [1080p]](https://uptobox.com/4f1dbuz8f54l)
- [[Refrain] Saiki Kusuo no ОЁ-nan (TV) 2 - 12 VOSTFR [720p]](https://uptobox.com/cvn4tfl9i44p)
- [[Refrain] Saiki Kusuo no ОЁ-nan (TV) 2 - 12 VOSTFR [1080p]](https://uptobox.com/lumbhad8oazs)
- [[Refrain] Saiki Kusuo no ОЁ-nan (TV) 2 - 13 VOSTFR [720p]](https://uptobox.com/0tmoq4ryeb0c)
- [[Refrain] Saiki Kusuo no ОЁ-nan (TV) 2 - 13 VOSTFR [1080p]](https://uptobox.com/j4two03nn9pc)
- [[Refrain] Saiki Kusuo no ОЁ-nan (TV) 2 - 14 VOSTFR [720p]](https://uptobox.com/cm8ty7i05gsa)
- [[Refrain] Saiki Kusuo no ОЁ-nan (TV) 2 - 14 VOSTFR [1080p]](https://uptobox.com/0t9a8tpggcmj)
- [[Refrain] Saiki Kusuo no ОЁ-nan (TV) 2 - 15 VOSTFR [720p]](https://uptobox.com/b460r6q9pbsr)
- [[Refrain] Saiki Kusuo no ОЁ-nan (TV) 2 - 15 VOSTFR [1080p]](https://uptobox.com/csyb7uol7mrk)
- [[Refrain] Saiki Kusuo no ОЁ-nan (TV) 2 - 16 VOSTFR [720p]](https://uptobox.com/jtdgau1epd97)
- [[Refrain] Saiki Kusuo no ОЁ-nan (TV) 2 - 16 VOSTFR [1080p]](https://uptobox.com/s4bqx4d89o8r)
- [[Refrain] Saiki Kusuo no ОЁ-nan (TV) 2 - 17 VOSTFR [720p]](https://uptobox.com/ppzfamx9nly5)
- [[Refrain] Saiki Kusuo no ОЁ-nan (TV) 2 - 17 VOSTFR [1080p]](https://uptobox.com/9qr1hoyqn3jb)
- [[Refrain] Saiki Kusuo no ОЁ-nan (TV) 2 - 18 VOSTFR [720p]](https://uptobox.com/rq207b86vm65)
- [[Refrain] Saiki Kusuo no ОЁ-nan (TV) 2 - 18 VOSTFR [1080p]](https://uptobox.com/loqmqod60nfp)
- [[Refrain] Saiki Kusuo no ОЁ-nan (TV) 2 - 19 VOSTFR [720p]](https://uptobox.com/ojh5sxlkoeep)
- [[Refrain] Saiki Kusuo no ОЁ-nan (TV) 2 - 19 VOSTFR [1080p]](https://uptobox.com/9fudoynpgllf)
- [[Refrain] Saiki Kusuo no ОЁ-nan (TV) 2 - 20 VOSTFR [720p]](https://uptobox.com/o807dkcrm0ew)
- [[Refrain] Saiki Kusuo no ОЁ-nan (TV) 2 - 20 VOSTFR [1080p]](https://uptobox.com/r33clv4w3jfu)
- [[Refrain] Saiki Kusuo no ОЁ-nan (TV) 2 - 21 VOSTFR [720p]](https://uptobox.com/5c7ka91ex0qt)
- [[Refrain] Saiki Kusuo no ОЁ-nan (TV) 2 - 21 VOSTFR [1080p]](https://uptobox.com/e7lg1xk92tla)
- [[Refrain] Saiki Kusuo no ОЁ-nan (TV) 2 - 22 VOSTFR [720p]](https://uptobox.com/7og4avubyqq3)
- [[Refrain] Saiki Kusuo no ОЁ-nan (TV) 2 - 22 VOSTFR [1080p]](https://uptobox.com/zqv7a40ybub6)
- [[Refrain] Saiki Kusuo no ОЁ-nan (TV) 2 - 23 VOSTFR [720p]](https://uptobox.com/39cif4p8dbnl)
- [[Refrain] Saiki Kusuo no ОЁ-nan (TV) 2 - 23 VOSTFR [1080p]](https://uptobox.com/bf8ibtnhd0e4)
- [[Refrain] Saiki Kusuo no ОЁ-nan (TV) 2 - 24 VOSTFR [720p]](https://uptobox.com/nasekzyifti1)
- [[Refrain] Saiki Kusuo no ОЁ-nan (TV) 2 - 24 VOSTFR [1080p]](https://uptobox.com/fehroq7tekti)
