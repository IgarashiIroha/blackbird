# Basilisk: Ouka Ninpouchou

![Basilisk: Ouka Ninpouchou](https://cdn.myanimelist.net/images/anime/2/88384l.jpg)

* Japanese:  バジリスク ～桜花忍法帖～

## Information

 - Type: TV
 - Episodes: 24
 - Status: Finished Airing
 - Aired: Jan 9, 2018 to Jun 19, 2018
 - Premiered: Winter 2018
 - Broadcast: Tuesdays at 00:00 (JST)
 - Producers: Kodansha, Glovision, King Records, Universal Entertainment
 - Licensors: Funimation
 - Studios: Seven Arcs Pictures
 - Source: Novel
 - Genres: Action, Drama, Historical, Martial Arts
 - Duration: 23 min. per ep.
 - Rating: R - 17+ (violence & profanity)


## Links

- [[Refrain] Basilisk Ouka Ninpouchou (2017) - 01 VOSTFR [720p]](https://uptobox.com/bwbmg5le7fwa)
- [[Refrain] Basilisk Ouka Ninpouchou (2017) - 01 VOSTFR [1080p]](https://uptobox.com/59m3pnoskevz)
- [[Refrain] Basilisk Ouka Ninpouchou (2017) - 02 VOSTFR [720p]](https://uptobox.com/zp89bswfdkgz)
- [[Refrain] Basilisk Ouka Ninpouchou (2017) - 02 VOSTFR [1080p]](https://uptobox.com/3f2led79y847)
- [[Refrain] Basilisk Ouka Ninpouchou (2017) - 03 VOSTFR [720p]](https://uptobox.com/m57fu3y30amm)
- [[Refrain] Basilisk Ouka Ninpouchou (2017) - 03 VOSTFR [1080p]](https://uptobox.com/l1q2wwkwd8qb)
- [[Refrain] Basilisk Ouka Ninpouchou (2017) - 04 VOSTFR [720p]](https://uptobox.com/jy1gqsz906ar)
- [[Refrain] Basilisk Ouka Ninpouchou (2017) - 04 VOSTFR [1080p]](https://uptobox.com/ijnbihtcesxv)
- [[Refrain] Basilisk Ouka Ninpouchou (2017) - 05 VOSTFR [720p]](https://uptobox.com/ggzuzeee8swx)
- [[Refrain] Basilisk Ouka Ninpouchou (2017) - 05 VOSTFR [1080p]](https://uptobox.com/2fr6sgxw00mt)
- [[Refrain] Basilisk Ouka Ninpouchou (2017) - 06 VOSTFR [720p]](https://uptobox.com/fr4rimqjtea1)
- [[Refrain] Basilisk Ouka Ninpouchou (2017) - 06 VOSTFR [1080p]](https://uptobox.com/98ixvfrf96t8)
- [[Refrain] Basilisk Ouka Ninpouchou (2017) - 07 VOSTFR [720p]](https://uptobox.com/ypn6nvocz2fr)
- [[Refrain] Basilisk Ouka Ninpouchou (2017) - 07 VOSTFR [1080p]](https://uptobox.com/mulnr5iq02fx)
- [[Refrain] Basilisk Ouka Ninpouchou (2017) - 08 VOSTFR [720p]](https://uptobox.com/05qgn3qbxbw0)
- [[Refrain] Basilisk Ouka Ninpouchou (2017) - 08 VOSTFR [1080p]](https://uptobox.com/p2iwqh31rddq)
- [[Refrain] Basilisk Ouka Ninpouchou (2017) - 09 VOSTFR [720p]](https://uptobox.com/uw8nhzoxtbks)
- [[Refrain] Basilisk Ouka Ninpouchou (2017) - 09 VOSTFR [1080p]](https://uptobox.com/6d9ijz3lj4io)
- [[Refrain] Basilisk Ouka Ninpouchou (2017) - 10 VOSTFR [720p]](https://uptobox.com/vxt46mhfw2ev)
- [[Refrain] Basilisk Ouka Ninpouchou (2017) - 10 VOSTFR [1080p]](https://uptobox.com/02vt1ri0o7z7)
- [[Refrain] Basilisk Ouka Ninpouchou (2017) - 11 VOSTFR [720p]](https://uptobox.com/pn5e05l9mbpj)
- [[Refrain] Basilisk Ouka Ninpouchou (2017) - 11 VOSTFR [1080p]](https://uptobox.com/lowlcoihnt88)
- [[Refrain] Basilisk Ouka Ninpouchou (2017) - 12 VOSTFR [720p]](https://uptobox.com/0r61qod3q3on)
- [[Refrain] Basilisk Ouka Ninpouchou (2017) - 12 VOSTFR [1080p]](https://uptobox.com/txxe5judxnva)
- [[Refrain] Basilisk Ouka Ninpouchou (2017) - 13 VOSTFR [720p]](https://uptobox.com/b6bivulo146r)
- [[Refrain] Basilisk Ouka Ninpouchou (2017) - 13 VOSTFR [1080p]](https://uptobox.com/idzeu78fr0b7)
- [[Refrain] Basilisk Ouka Ninpouchou (2017) - 14 VOSTFR [720p]](https://uptobox.com/jnfnou1eydx0)
- [[Refrain] Basilisk Ouka Ninpouchou (2017) - 14 VOSTFR [1080p]](https://uptobox.com/0aj2ukson7ma)
- [[Refrain] Basilisk Ouka Ninpouchou (2017) - 15 VOSTFR [720p]](https://uptobox.com/ib4xfkv6tul5)
- [[Refrain] Basilisk Ouka Ninpouchou (2017) - 15 VOSTFR [1080p]](https://uptobox.com/slcn4141w0al)
- [[Refrain] Basilisk Ouka Ninpouchou (2017) - 16 VOSTFR [720p]](https://uptobox.com/xsjym8120oy1)
- [[Refrain] Basilisk Ouka Ninpouchou (2017) - 16 VOSTFR [1080p]](https://uptobox.com/eqzosy2c1adg)
- [[Refrain] Basilisk Ouka Ninpouchou (2017) - 17 VOSTFR [720p]](https://uptobox.com/h4wcti0jak77)
- [[Refrain] Basilisk Ouka Ninpouchou (2017) - 17 VOSTFR [1080p]](https://uptobox.com/d39h4c6vqnbg)
- [[Refrain] Basilisk Ouka Ninpouchou (2017) - 18 VOSTFR [720p]](https://uptobox.com/hbo4gzbbc2zk)
- [[Refrain] Basilisk Ouka Ninpouchou (2017) - 18 VOSTFR [1080p]](https://uptobox.com/e0pr67efojcu)
- [[Refrain] Basilisk Ouka Ninpouchou (2017) - 19 VOSTFR [720p]](https://uptobox.com/q330jrgli5uj)
- [[Refrain] Basilisk Ouka Ninpouchou (2017) - 19 VOSTFR [1080p]](https://uptobox.com/n19ddzskkr0y)
- [[Refrain] Basilisk Ouka Ninpouchou (2017) - 20 VOSTFR [720p]](https://uptobox.com/y8dqtsexl2yz)
- [[Refrain] Basilisk Ouka Ninpouchou (2017) - 20 VOSTFR [1080p]](https://uptobox.com/10iptejzexmf)
- [[Refrain] Basilisk Ouka Ninpouchou (2017) - 21 VOSTFR [720p]](https://uptobox.com/xkrbqh4wj3ng)
- [[Refrain] Basilisk Ouka Ninpouchou (2017) - 21 VOSTFR [1080p]](https://uptobox.com/2uyftmbqvchd)
- [[Refrain] Basilisk Ouka Ninpouchou (2017) - 22 VOSTFR [720p]](https://uptobox.com/mmo6jhqu1rno)
- [[Refrain] Basilisk Ouka Ninpouchou (2017) - 22 VOSTFR [1080p]](https://uptobox.com/kz13hbxp0ylv)
- [[Refrain] Basilisk Ouka Ninpouchou (2017) - 23 VOSTFR [720p]](https://uptobox.com/v5vauyn1puv6)
- [[Refrain] Basilisk Ouka Ninpouchou (2017) - 23 VOSTFR [1080p]](https://uptobox.com/76oj4uc61rg2)
- [[Refrain] Basilisk Ouka Ninpouchou (2017) - 24 VOSTFR [720p]](https://uptobox.com/zriqet06nrkp)
- [[Refrain] Basilisk Ouka Ninpouchou (2017) - 24 VOSTFR [1080p]](https://uptobox.com/zq4pchhllq4p)
