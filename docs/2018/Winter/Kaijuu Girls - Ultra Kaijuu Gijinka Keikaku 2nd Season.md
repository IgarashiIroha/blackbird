# Kaijuu Girls: Ultra Kaijuu Gijinka Keikaku 2nd Season

![Kaijuu Girls: Ultra Kaijuu Gijinka Keikaku 2nd Season](https://cdn.myanimelist.net/images/anime/2/89222l.jpg)

* Japanese:  怪獣娘～ウルトラ怪獣擬人化計画～ 第2期

## Information

 - Type: TV
 - Episodes: 12
 - Status: Finished Airing
 - Aired: Jan 9, 2018 to Mar 27, 2018
 - Premiered: Winter 2018
 - Broadcast: Tuesdays at 21:54 (JST)
 - Producers: Pony Canyon, Tsuburaya Productions, Docomo Anime Store, Medicrie
 - Licensors: None found, add some
 - Studios: Studio PuYUKAI
 - Source: Original
 - Genres: Comedy, Fantasy, Parody
 - Duration: 5 min. per ep.
 - Rating: PG - Children


## Links

- [[Refrain] Kaijuu Girls S2 - 01 VOSTFR [720p]](https://uptobox.com/zvz33pusgb0m)
- [[Refrain] Kaijuu Girls S2 - 01 VOSTFR [1080p]](https://uptobox.com/5jmzzn8wg0h5)
- [[Refrain] Kaijuu Girls S2 - 02 VOSTFR [720p]](https://uptobox.com/hzgrvx25ipxb)
- [[Refrain] Kaijuu Girls S2 - 02 VOSTFR [1080p]](https://uptobox.com/4bi5aiuhbyj5)
- [[Refrain] Kaijuu Girls S2 - 03 VOSTFR [720p]](https://uptobox.com/2igdgk7z0yn7)
- [[Refrain] Kaijuu Girls S2 - 03 VOSTFR [1080p]](https://uptobox.com/w4ylwloy29q8)
- [[Refrain] Kaijuu Girls S2 - 04 VOSTFR [720p]](https://uptobox.com/n340eku7wpd6)
- [[Refrain] Kaijuu Girls S2 - 04 VOSTFR [1080p]](https://uptobox.com/2lc1q8pv7wf7)
- [[Refrain] Kaijuu Girls S2 - 05 VOSTFR [720p]](https://uptobox.com/mjyt2n1sw3ot)
- [[Refrain] Kaijuu Girls S2 - 05 VOSTFR [1080p]](https://uptobox.com/tbk9vgfd1zfn)
- [[Refrain] Kaijuu Girls S2 - 06 VOSTFR [720p]](https://uptobox.com/b2cbq0jwpiij)
- [[Refrain] Kaijuu Girls S2 - 06 VOSTFR [1080p]](https://uptobox.com/im7j5b6f0mzy)
- [[Refrain] Kaijuu Girls S2 - 07 VOSTFR [720p]](https://uptobox.com/q2oh8xppx7ov)
- [[Refrain] Kaijuu Girls S2 - 07 VOSTFR [1080p]](https://uptobox.com/e688ig3befe1)
- [[Refrain] Kaijuu Girls S2 - 08 VOSTFR [720p]](https://uptobox.com/agbum4hv5fut)
- [[Refrain] Kaijuu Girls S2 - 08 VOSTFR [1080p]](https://uptobox.com/1itk3t0qle2i)
- [[Refrain] Kaijuu Girls S2 - 09 VOSTFR [720p]](https://uptobox.com/a0y0k8n07unx)
- [[Refrain] Kaijuu Girls S2 - 09 VOSTFR [1080p]](https://uptobox.com/v6vnl2l29b0g)
- [[Refrain] Kaijuu Girls S2 - 10 VOSTFR [720p]](https://uptobox.com/ym5tvc4tuohy)
- [[Refrain] Kaijuu Girls S2 - 10 VOSTFR [1080p]](https://uptobox.com/3aanndjz6n99)
- [[Refrain] Kaijuu Girls S2 - 11 VOSTFR [720p]](https://uptobox.com/9somx4oso3jv)
- [[Refrain] Kaijuu Girls S2 - 11 VOSTFR [1080p]](https://uptobox.com/opywvr38u5lh)
- [[Refrain] Kaijuu Girls S2 - 12 VOSTFR [720p]](https://uptobox.com/kyn3st8c3rq7)
- [[Refrain] Kaijuu Girls S2 - 12 VOSTFR [1080p]](https://uptobox.com/eglex3m2mk4p)
