# Hakumei to Mikochi

![Hakumei to Mikochi](https://cdn.myanimelist.net/images/anime/1250/91694l.jpg)

* Japanese:  ハクメイとミコチ

## Information

 - Type: TV
 - Episodes: 12
 - Status: Finished Airing
 - Aired: Jan 12, 2018 to Mar 30, 2018
 - Premiered: Winter 2018
 - Broadcast: Fridays at 21:00 (JST)
 - Producers: Lantis, AT-X, Sony Music Communications, Kadokawa Media House, Kadokawa 
 - Licensors: Sentai Filmworks 
 - Studios: Lerche 
 - Source: Manga
 - Genres: Slice of Life, Fantasy, Seinen
 - Duration: 23 min. per ep.
 - Rating: PG-13 - Teens 13 or older


## Links

