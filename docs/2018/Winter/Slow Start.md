# Slow Start

![Slow Start](https://cdn.myanimelist.net/images/anime/4/89982l.jpg)

* Japanese:  スロウスタート

## Information

 - Type: TV
 - Episodes: 12
 - Status: Finished Airing
 - Aired: Jan 7, 2018 to Mar 25, 2018
 - Premiered: Winter 2018
 - Broadcast: Sundays at 00:30 (JST)
 - Producers: Aniplex, Houbunsha, Kansai Telecasting, Lawson, Kinoshita Group Holdings, Drecom 
 - Licensors: Aniplex of America 
 - Studios: CloverWorks 
 - Source: 4-koma manga
 - Genres: Slice of Life, Comedy, School
 - Duration: 23 min. per ep.
 - Rating: PG-13 - Teens 13 or older


## Links

