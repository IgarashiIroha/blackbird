# Sora yori mo Tooi Basho

![Sora yori mo Tooi Basho](https://cdn.myanimelist.net/images/anime/6/89879l.jpg)

* Japanese:  宇宙よりも遠い場所

## Information

 - Type: TV
 - Episodes: 13
 - Status: Finished Airing
 - Aired: Jan 2, 2018 to Mar 27, 2018
 - Premiered: Winter 2018
 - Broadcast: Tuesdays at 20:30 (JST)
 - Producers: Media Factory, Movic, AT-X, Sony Music Communications, Docomo Anime Store, Kadokawa Media House, Kadokawa, NewGin, Crunchyroll SC Anime Fund
 - Licensors: None found, add some
 - Studios: Madhouse
 - Source: Original
 - Genres: Adventure, Seinen
 - Duration: 23 min. per ep.
 - Rating: PG-13 - Teens 13 or older


## Links
- [[Refrain] Sora yori mo Tooi Basho - 01 VOSTFR [720p]](https://uptobox.com/1ow3xstqffdb)
- [[Refrain] Sora yori mo Tooi Basho - 01 VOSTFR [1080p]](https://uptobox.com/4krzn86kknpl)
- [[Refrain] Sora yori mo Tooi Basho - 02 VOSTFR [720p]](https://uptobox.com/g4kqb6h97237)
- [[Refrain] Sora yori mo Tooi Basho - 02 VOSTFR [1080p]](https://uptobox.com/v62c35drwuhl)
- [[Refrain] Sora yori mo Tooi Basho - 03 VOSTFR [720p]](https://uptobox.com/n85d4zzl6hz2)
- [[Refrain] Sora yori mo Tooi Basho - 03 VOSTFR [1080p]](https://uptobox.com/vngr9257ucob)
- [[Refrain] Sora yori mo Tooi Basho - 04 VOSTFR [720p]](https://uptobox.com/bxbthu6gvuo7)
- [[Refrain] Sora yori mo Tooi Basho - 04 VOSTFR [1080p]](https://uptobox.com/41f8vp1x85w9)
- [[Refrain] Sora yori mo Tooi Basho - 05 VOSTFR [720p]](https://uptobox.com/yojpm01q1ugj)
- [[Refrain] Sora yori mo Tooi Basho - 05 VOSTFR [1080p]](https://uptobox.com/imxmowpj8hay)
- [[Refrain] Sora yori mo Tooi Basho - 06 VOSTFR [720p]](https://uptobox.com/a8cqbrt8bzws)
- [[Refrain] Sora yori mo Tooi Basho - 06 VOSTFR [1080p]](https://uptobox.com/jrq6s104zlfk)
- [[Refrain] Sora yori mo Tooi Basho - 07 VOSTFR [720p]](https://uptobox.com/sparabh8jfno)
- [[Refrain] Sora yori mo Tooi Basho - 07 VOSTFR [1080p]](https://uptobox.com/6ed186jqr0fv)
- [[Refrain] Sora yori mo Tooi Basho - 08 VOSTFR [720p]](https://uptobox.com/tf6dazvrm40w)
- [[Refrain] Sora yori mo Tooi Basho - 08 VOSTFR [1080p]](https://uptobox.com/gvnrsn1bbr7z)
- [[Refrain] Sora yori mo Tooi Basho - 09 VOSTFR [720p]](https://uptobox.com/rqlf2fw7ixs5)
- [[Refrain] Sora yori mo Tooi Basho - 09 VOSTFR [1080p]](https://uptobox.com/0f58j3p46vgo)
- [[Refrain] Sora yori mo Tooi Basho - 10 VOSTFR [720p]](https://uptobox.com/u1ehf44n4azr)
- [[Refrain] Sora yori mo Tooi Basho - 10 VOSTFR [1080p]](https://uptobox.com/9j0ljt24b7bq)
- [[Refrain] Sora yori mo Tooi Basho - 11 VOSTFR [720p]](https://uptobox.com/rzw8slusju5j)
- [[Refrain] Sora yori mo Tooi Basho - 11 VOSTFR [1080p]](https://uptobox.com/0arhx8wg497l)
- [[Refrain] Sora yori mo Tooi Basho - 12 VOSTFR [720p]](https://uptobox.com/svewlci3e7in)
- [[Refrain] Sora yori mo Tooi Basho - 12 VOSTFR [1080p]](https://uptobox.com/ac5j44b22rms)
- [[Refrain] Sora yori mo Tooi Basho - 13 VOSTFR [720p]](https://uptobox.com/xm9n5jztehau)
- [[Refrain] Sora yori mo Tooi Basho - 13 VOSTFR [1080p]](https://uptobox.com/7xj85hiofxhx)
