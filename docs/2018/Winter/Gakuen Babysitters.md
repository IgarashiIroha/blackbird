# Gakuen Babysitters

![Gakuen Babysitters](https://cdn.myanimelist.net/images/anime/8/89978l.jpg)

* Japanese:  学園ベビーシッターズ

## Information

 - Type: TV
 - Episodes: 12
 - Status: Finished Airing
 - Aired: Jan 7, 2018 to Mar 25, 2018
 - Premiered: Winter 2018
 - Broadcast: Sundays at 23:00 (JST)
 - Producers: Bandai Visual, Lantis, Nihon Ad Systems, Hakusensha, Banpresto, Marui Group
 - Licensors: None found, add some
 - Studios: Brain&#039;s Base
 - Source: Manga
 - Genres: Comedy, School, Shoujo, Slice of Life
 - Duration: 23 min. per ep.
 - Rating: PG-13 - Teens 13 or older


## Links

- [[Refrain] Gakuen Babysitters - 01 VOSTFR [720p]](https://uptobox.com/zvx326nogd0m)
- [[Refrain] Gakuen Babysitters - 01 VOSTFR [1080p]](https://uptobox.com/865a02b2beh4)
- [[Refrain] Gakuen Babysitters - 02 VOSTFR [720p]](https://uptobox.com/bvmtvm8w8ks4)
- [[Refrain] Gakuen Babysitters - 02 VOSTFR [1080p]](https://uptobox.com/m3e2ubstrtv6)
- [[Refrain] Gakuen Babysitters - 03 VOSTFR [720p]](https://uptobox.com/7hhui33rt9tu)
- [[Refrain] Gakuen Babysitters - 03 VOSTFR [1080p]](https://uptobox.com/iaxuv193sa8z)
- [[Refrain] Gakuen Babysitters - 04 VOSTFR [720p]](https://uptobox.com/71nikzy8ansn)
- [[Refrain] Gakuen Babysitters - 04 VOSTFR [1080p]](https://uptobox.com/f0lkzy8k7ixu)
- [[Refrain] Gakuen Babysitters - 05 VOSTFR [720p]](https://uptobox.com/d3ftvdw1tz67)
- [[Refrain] Gakuen Babysitters - 05 VOSTFR [1080p]](https://uptobox.com/owmjulyu6v9x)
- [[Refrain] Gakuen Babysitters - 06 VOSTFR [720p]](https://uptobox.com/k6xqtpfg84t1)
- [[Refrain] Gakuen Babysitters - 06 VOSTFR [1080p]](https://uptobox.com/ug2zbpv8s1hc)
- [[Refrain] Gakuen Babysitters - 07 VOSTFR [720p]](https://uptobox.com/pf37uftuwv29)
- [[Refrain] Gakuen Babysitters - 07 VOSTFR [1080p]](https://uptobox.com/kyzude7v4pyg)
- [[Refrain] Gakuen Babysitters - 08 VOSTFR [720p]](https://uptobox.com/hw2cfmb9ew15)
- [[Refrain] Gakuen Babysitters - 08 VOSTFR [1080p]](https://uptobox.com/hoqsnwd7roq4)
- [[Refrain] Gakuen Babysitters - 09 VOSTFR [720p]](https://uptobox.com/9z4cb5l6welh)
- [[Refrain] Gakuen Babysitters - 09 VOSTFR [1080p]](https://uptobox.com/fnwvdm20vys0)
- [[Refrain] Gakuen Babysitters - 10 VOSTFR [720p]](https://uptobox.com/wb4fvsa0u4fh)
- [[Refrain] Gakuen Babysitters - 10 VOSTFR [1080p]](https://uptobox.com/skpatctr2v7m)
- [[Refrain] Gakuen Babysitters - 11 VOSTFR [720p]](https://uptobox.com/b6t3xfffxf28)
- [[Refrain] Gakuen Babysitters - 11 VOSTFR [1080p]](https://uptobox.com/zafo1snzqd7h)
- [[Refrain] Gakuen Babysitters - 12 VOSTFR [720p]](https://uptobox.com/fhxv662565y0)
- [[Refrain] Gakuen Babysitters - 12 VOSTFR [1080p]](https://uptobox.com/f4717f0vfsxm)
