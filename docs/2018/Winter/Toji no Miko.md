# Toji no Miko

![Toji no Miko](https://cdn.myanimelist.net/images/anime/13/89885l.jpg)

* Japanese:  刀使ノ巫女

## Information

 - Type: TV
 - Episodes: 24
 - Status: Finished Airing
 - Aired: Jan 5, 2018 to Jun 22, 2018
 - Premiered: Winter 2018
 - Broadcast: Fridays at 21:30 (JST)
 - Producers: Square Enix, Genco, Mainichi Broadcasting System, AT-X, Glovision, Kadokawa, My Theater D.D., Crunchyroll SC Anime Fund
 - Licensors: Funimation
 - Studios: Studio Gokumi
 - Source: Original
 - Genres: Action, Fantasy
 - Duration: 23 min. per ep.
 - Rating: PG-13 - Teens 13 or older


## Links

- [[Refrain] Toji no Miko - 01 VOSTFR [720p]](https://uptobox.com/dsllnw89m288)
- [[Refrain] Toji no Miko - 01 VOSTFR [1080p]](https://uptobox.com/48wmsu9w3tle)
- [[Refrain] Toji no Miko - 02 VOSTFR [720p]](https://uptobox.com/gd7030cc2exe)
- [[Refrain] Toji no Miko - 02 VOSTFR [1080p]](https://uptobox.com/kplz16t0mef3)
- [[Refrain] Toji no Miko - 03 VOSTFR [720p]](https://uptobox.com/d01e60ucagyq)
- [[Refrain] Toji no Miko - 03 VOSTFR [1080p]](https://uptobox.com/637fcmyydywe)
- [[Refrain] Toji no Miko - 04 VOSTFR [720p]](https://uptobox.com/jmtecibqcd8d)
- [[Refrain] Toji no Miko - 04 VOSTFR [1080p]](https://uptobox.com/b19nwa0uutoq)
- [[Refrain] Toji no Miko - 05 VOSTFR [720p]](https://uptobox.com/g1czvcqmy42x)
- [[Refrain] Toji no Miko - 05 VOSTFR [1080p]](https://uptobox.com/su1kj2mg6dhl)
- [[Refrain] Toji no Miko - 06 VOSTFR [720p]](https://uptobox.com/8vop4jusr7zv)
- [[Refrain] Toji no Miko - 06 VOSTFR [1080p]](https://uptobox.com/p7e892n2l3bj)
- [[Refrain] Toji no Miko - 07 VOSTFR [720p]](https://uptobox.com/8yaxhakqcvgw)
- [[Refrain] Toji no Miko - 07 VOSTFR [1080p]](https://uptobox.com/ssm3salgx3wx)
- [[Refrain] Toji no Miko - 08 VOSTFR [720p]](https://uptobox.com/f22hxeq506wu)
- [[Refrain] Toji no Miko - 08 VOSTFR [1080p]](https://uptobox.com/dpabpb89p2lk)
- [[Refrain] Toji no Miko - 09 VOSTFR [720p]](https://uptobox.com/p0mv1qgj5xec)
- [[Refrain] Toji no Miko - 09 VOSTFR [1080p]](https://uptobox.com/j9o5tw0u6s9a)
- [[Refrain] Toji no Miko - 10 VOSTFR [720p]](https://uptobox.com/6p4f9ihkiv1n)
- [[Refrain] Toji no Miko - 10 VOSTFR [1080p]](https://uptobox.com/atjuysilgo1y)
- [[Refrain] Toji no Miko - 11 VOSTFR [720p]](https://uptobox.com/qyigra801les)
- [[Refrain] Toji no Miko - 11 VOSTFR [1080p]](https://uptobox.com/7k2442jlwdwg)
- [[Refrain] Toji no Miko - 12.5 VOSTFR [720p]](https://uptobox.com/g06cyj7mvbgt)
- [[Refrain] Toji no Miko - 12.5 VOSTFR [1080p]](https://uptobox.com/y2gfxrv9ijgm)
- [[Refrain] Toji no Miko - 12 VOSTFR [720p]](https://uptobox.com/dbhbyjuwwro8)
- [[Refrain] Toji no Miko - 12 VOSTFR [1080p]](https://uptobox.com/0wltn08mpu8z)
- [[Refrain] Toji no Miko - 13 VOSTFR [720p]](https://uptobox.com/f2q9vhuqndh7)
- [[Refrain] Toji no Miko - 13 VOSTFR [1080p]](https://uptobox.com/53fpjr71zhre)
- [[Refrain] Toji no Miko - 14 VOSTFR [720p]](https://uptobox.com/onumq4y012b0)
- [[Refrain] Toji no Miko - 14 VOSTFR [1080p]](https://uptobox.com/okscuasihut0)
- [[Refrain] Toji no Miko - 15 VOSTFR [720p]](https://uptobox.com/v90pyclkfvym)
- [[Refrain] Toji no Miko - 15 VOSTFR [1080p]](https://uptobox.com/f0xnmkikcodw)
- [[Refrain] Toji no Miko - 16 VOSTFR [720p]](https://uptobox.com/znvihrqu3e2k)
- [[Refrain] Toji no Miko - 16 VOSTFR [1080p]](https://uptobox.com/u4376n7h80r9)
- [[Refrain] Toji no Miko - 17 VOSTFR [720p]](https://uptobox.com/qid8p0wldcq1)
- [[Refrain] Toji no Miko - 17 VOSTFR [1080p]](https://uptobox.com/jifg9l1zv4q1)
- [[Refrain] Toji no Miko - 18 VOSTFR [720p]](https://uptobox.com/170fcy3hwk5o)
- [[Refrain] Toji no Miko - 18 VOSTFR [1080p]](https://uptobox.com/848m47mn01xx)
- [[Refrain] Toji no Miko - 19 VOSTFR [720p]](https://uptobox.com/8d1ek61do9w6)
- [[Refrain] Toji no Miko - 19 VOSTFR [1080p]](https://uptobox.com/uxciw0xi7ilq)
- [[Refrain] Toji no Miko - 20 VOSTFR [720p]](https://uptobox.com/e115b66smisv)
- [[Refrain] Toji no Miko - 20 VOSTFR [1080p]](https://uptobox.com/fnj2q6snrdd0)
- [[Refrain] Toji no Miko - 21 VOSTFR [720p]](https://uptobox.com/ahevdblypc9h)
- [[Refrain] Toji no Miko - 21 VOSTFR [1080p]](https://uptobox.com/gnvffw2tqkkw)
- [[Refrain] Toji no Miko - 22 VOSTFR [720p]](https://uptobox.com/ns85cuec14j1)
- [[Refrain] Toji no Miko - 22 VOSTFR [1080p]](https://uptobox.com/xik8hlpcq0ds)
- [[Refrain] Toji no Miko - 23 VOSTFR [720p]](https://uptobox.com/h2r7jer3xu7x)
- [[Refrain] Toji no Miko - 23 VOSTFR [1080p]](https://uptobox.com/gbz5q9ao5hog)
- [[Refrain] Toji no Miko - 24 VOSTFR [720p]](https://uptobox.com/6yn638emeumf)
- [[Refrain] Toji no Miko - 24 VOSTFR [1080p]](https://uptobox.com/2pd90tlcjvbv)
