# Hakata Tonkotsu Ramens

![Hakata Tonkotsu Ramens](https://cdn.myanimelist.net/images/anime/1718/91097l.jpg)

* Japanese:  博多豚骨ラーメンズ

## Information

 - Type: TV
 - Episodes: 12
 - Status: Finished Airing
 - Aired: Jan 12, 2018 to Mar 30, 2018
 - Premiered: Winter 2018
 - Broadcast: Fridays at 22:30 (JST)
 - Producers: Sotsu, AT-X, Warner Bros. Japan, KlockWorx, ASCII Media Works, Tokyo MX, BS11
 - Licensors: Funimation
 - Studios: Satelight
 - Source: Novel
 - Genres: Action
 - Duration: 23 min. per ep.
 - Rating: R - 17+ (violence & profanity)


## Links

- [[Refrain] Hakata Tonkotsu Ramens - 01 VOSTFR [720p]](https://uptobox.com/4yktfna80gc2)
- [[Refrain] Hakata Tonkotsu Ramens - 01 VOSTFR [1080p]](https://uptobox.com/gf3tvd8f4xui)
- [[Refrain] Hakata Tonkotsu Ramens - 02 VOSTFR [720p]](https://uptobox.com/e2biyon16x0m)
- [[Refrain] Hakata Tonkotsu Ramens - 02 VOSTFR [1080p]](https://uptobox.com/wj933syp5qht)
- [[Refrain] Hakata Tonkotsu Ramens - 03 VOSTFR [720p]](https://uptobox.com/51lyyax3v3fn)
- [[Refrain] Hakata Tonkotsu Ramens - 03 VOSTFR [1080p]](https://uptobox.com/4ytlsz0xhcep)
- [[Refrain] Hakata Tonkotsu Ramens - 04 VOSTFR [720p]](https://uptobox.com/jw1592tutkyc)
- [[Refrain] Hakata Tonkotsu Ramens - 04 VOSTFR [1080p]](https://uptobox.com/j7olsfpeib0b)
- [[Refrain] Hakata Tonkotsu Ramens - 05 VOSTFR [720p]](https://uptobox.com/8zz28f53i2d1)
- [[Refrain] Hakata Tonkotsu Ramens - 05 VOSTFR [1080p]](https://uptobox.com/xtjt7f3tuxv7)
- [[Refrain] Hakata Tonkotsu Ramens - 06 VOSTFR [720p]](https://uptobox.com/1szgurotltf4)
- [[Refrain] Hakata Tonkotsu Ramens - 06 VOSTFR [1080p]](https://uptobox.com/9smd6sr4uvs6)
- [[Refrain] Hakata Tonkotsu Ramens - 07 VOSTFR [720p]](https://uptobox.com/rqv65pjv47k6)
- [[Refrain] Hakata Tonkotsu Ramens - 07 VOSTFR [1080p]](https://uptobox.com/d3r4tsayvs1k)
- [[Refrain] Hakata Tonkotsu Ramens - 08 VOSTFR [720p]](https://uptobox.com/c5treaw2a0pl)
- [[Refrain] Hakata Tonkotsu Ramens - 08 VOSTFR [1080p]](https://uptobox.com/q2xhqr0aan77)
- [[Refrain] Hakata Tonkotsu Ramens - 09 VOSTFR [720p]](https://uptobox.com/4f36ym1yviy5)
- [[Refrain] Hakata Tonkotsu Ramens - 09 VOSTFR [1080p]](https://uptobox.com/jixph9vxenyf)
- [[Refrain] Hakata Tonkotsu Ramens - 10 VOSTFR [720p]](https://uptobox.com/q9yy7yvarp3x)
- [[Refrain] Hakata Tonkotsu Ramens - 10 VOSTFR [1080p]](https://uptobox.com/878ysggdup8z)
- [[Refrain] Hakata Tonkotsu Ramens - 11 VOSTFR [720p]](https://uptobox.com/k3kky8zmswcp)
- [[Refrain] Hakata Tonkotsu Ramens - 11 VOSTFR [1080p]](https://uptobox.com/mouyf6un3d1d)
- [[Refrain] Hakata Tonkotsu Ramens - 12 VOSTFR [720p]](https://uptobox.com/mclar5stu4i8)
- [[Refrain] Hakata Tonkotsu Ramens - 12 VOSTFR [1080p]](https://uptobox.com/vrt4qmisri2t)
