# Death March kara Hajimaru Isekai Kyousoukyoku

![Death March kara Hajimaru Isekai Kyousoukyoku](https://cdn.myanimelist.net/images/anime/4/88911l.jpg)

* Japanese:  デスマーチからはじまる異世界狂想曲

## Information

 - Type: TV
 - Episodes: 12
 - Status: Finished Airing
 - Aired: Jan 11, 2018 to Mar 29, 2018
 - Premiered: Winter 2018
 - Broadcast: Thursdays at 23:30 (JST)
 - Producers: Sotsu, AT-X, DIVE II Entertainment, Tokyo MX, Avex Pictures, BS11, Kadokawa, KLab
 - Licensors: Funimation
 - Studios: Silver Link., Connect
 - Source: Light novel
 - Genres: Adventure, Fantasy, Harem
 - Duration: 23 min. per ep.
 - Rating: PG-13 - Teens 13 or older


## Links

- [[Refrain] Death March kara Hajimaru Isekai Kyousoukyoku - 01 VOSTFR [720p]](https://uptobox.com/jtla7bhmw54x)
- [[Refrain] Death March kara Hajimaru Isekai Kyousoukyoku - 01 VOSTFR [1080p]](https://uptobox.com/v2e4p4c3aegr)
- [[Refrain] Death March kara Hajimaru Isekai Kyousoukyoku - 02 VOSTFR [720p]](https://uptobox.com/gmxocc1giebz)
- [[Refrain] Death March kara Hajimaru Isekai Kyousoukyoku - 02 VOSTFR [1080p]](https://uptobox.com/po6vsam6jw39)
- [[Refrain] Death March kara Hajimaru Isekai Kyousoukyoku - 03 VOSTFR [720p]](https://uptobox.com/yrude69slzl4)
- [[Refrain] Death March kara Hajimaru Isekai Kyousoukyoku - 03 VOSTFR [1080p]](https://uptobox.com/4b30vtgzqws1)
- [[Refrain] Death March kara Hajimaru Isekai Kyousoukyoku - 04 VOSTFR [720p]](https://uptobox.com/dd8drx1w21or)
- [[Refrain] Death March kara Hajimaru Isekai Kyousoukyoku - 04 VOSTFR [1080p]](https://uptobox.com/64gfwtr4ghqt)
- [[Refrain] Death March kara Hajimaru Isekai Kyousoukyoku - 05 VOSTFR [720p]](https://uptobox.com/k4llk3oksuq3)
- [[Refrain] Death March kara Hajimaru Isekai Kyousoukyoku - 05 VOSTFR [1080p]](https://uptobox.com/j0w2wb849t9q)
- [[Refrain] Death March kara Hajimaru Isekai Kyousoukyoku - 06 VOSTFR [720p]](https://uptobox.com/a8b3bozvxz3s)
- [[Refrain] Death March kara Hajimaru Isekai Kyousoukyoku - 06 VOSTFR [1080p]](https://uptobox.com/vqi5zhsbk3cn)
- [[Refrain] Death March kara Hajimaru Isekai Kyousoukyoku - 07 VOSTFR [720p]](https://uptobox.com/lvf5hqm904y2)
- [[Refrain] Death March kara Hajimaru Isekai Kyousoukyoku - 07 VOSTFR [1080p]](https://uptobox.com/o71uolg3gtcm)
- [[Refrain] Death March kara Hajimaru Isekai Kyousoukyoku - 08 VOSTFR [720p]](https://uptobox.com/5uxwgn3dhjnf)
- [[Refrain] Death March kara Hajimaru Isekai Kyousoukyoku - 08 VOSTFR [1080p]](https://uptobox.com/txif0z92z01w)
- [[Refrain] Death March kara Hajimaru Isekai Kyousoukyoku - 09 VOSTFR [720p]](https://uptobox.com/13sq3u0lvkwd)
- [[Refrain] Death March kara Hajimaru Isekai Kyousoukyoku - 09 VOSTFR [1080p]](https://uptobox.com/9pkmocjujr91)
- [[Refrain] Death March kara Hajimaru Isekai Kyousoukyoku - 10 VOSTFR [720p]](https://uptobox.com/mp0qk2uzpzhb)
- [[Refrain] Death March kara Hajimaru Isekai Kyousoukyoku - 10 VOSTFR [1080p]](https://uptobox.com/su3n2ugk46ci)
- [[Refrain] Death March kara Hajimaru Isekai Kyousoukyoku - 11 VOSTFR [720p]](https://uptobox.com/t47g6xnwqdm8)
- [[Refrain] Death March kara Hajimaru Isekai Kyousoukyoku - 11 VOSTFR [1080p]](https://uptobox.com/2b7lhrfd5h8m)
- [[Refrain] Death March kara Hajimaru Isekai Kyousoukyoku - 12 VOSTFR [720p]](https://uptobox.com/fcy9z0qdto4u)
- [[Refrain] Death March kara Hajimaru Isekai Kyousoukyoku - 12 VOSTFR [1080p]](https://uptobox.com/2tdvy9xhyrow)
