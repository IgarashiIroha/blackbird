# Sanrio Danshi

![Sanrio Danshi](https://cdn.myanimelist.net/images/anime/13/89890l.jpg)

* Japanese:  サンリオ男子

## Information

 - Type: TV
 - Episodes: 12
 - Status: Finished Airing
 - Aired: Jan 6, 2018 to Mar 24, 2018
 - Premiered: Winter 2018
 - Broadcast: Saturdays at 22:00 (JST)
 - Producers: Pony Canyon, Sanrio, Pierrot Plus
 - Licensors: Ponycan USA
 - Studios: Studio Pierrot
 - Source: Original
 - Genres: Slice of Life, School
 - Duration: 24 min. per ep.
 - Rating: PG-13 - Teens 13 or older


## Links

- [[Refrain] Sanrio Danshi - 01 VOSTFR [720p]](https://uptobox.com/eqzuubb04bpl)
- [[Refrain] Sanrio Danshi - 01 VOSTFR [1080p]](https://uptobox.com/sk7vpuipgpev)
- [[Refrain] Sanrio Danshi - 02 VOSTFR [720p]](https://uptobox.com/2cgf1wm3mmd8)
- [[Refrain] Sanrio Danshi - 02 VOSTFR [1080p]](https://uptobox.com/so18on7jrvnq)
- [[Refrain] Sanrio Danshi - 03 VOSTFR [720p]](https://uptobox.com/18c0mhy7wkcy)
- [[Refrain] Sanrio Danshi - 03 VOSTFR [1080p]](https://uptobox.com/112n30t8tlrh)
- [[Refrain] Sanrio Danshi - 04 VOSTFR [720p]](https://uptobox.com/6op47maxzdue)
- [[Refrain] Sanrio Danshi - 04 VOSTFR [1080p]](https://uptobox.com/p6dlkgtwn47u)
- [[Refrain] Sanrio Danshi - 05 VOSTFR [720p]](https://uptobox.com/x05n06547dld)
- [[Refrain] Sanrio Danshi - 05 VOSTFR [1080p]](https://uptobox.com/6s572wzzpnf8)
- [[Refrain] Sanrio Danshi - 06 VOSTFR [720p]](https://uptobox.com/rc3o6t1uvvfd)
- [[Refrain] Sanrio Danshi - 06 VOSTFR [1080p]](https://uptobox.com/4t6e4qsa1a3y)
- [[Refrain] Sanrio Danshi - 07 VOSTFR [720p]](https://uptobox.com/nt89ibkdd8a8)
- [[Refrain] Sanrio Danshi - 07 VOSTFR [1080p]](https://uptobox.com/tlzwkzepsriy)
- [[Refrain] Sanrio Danshi - 08 VOSTFR [720p]](https://uptobox.com/e6x3i7d5cxgt)
- [[Refrain] Sanrio Danshi - 08 VOSTFR [1080p]](https://uptobox.com/3scbro0r3dqj)
- [[Refrain] Sanrio Danshi - 09 VOSTFR [720p]](https://uptobox.com/s4fyg0popf3t)
- [[Refrain] Sanrio Danshi - 09 VOSTFR [1080p]](https://uptobox.com/z9wagl4em0se)
- [[Refrain] Sanrio Danshi - 10 VOSTFR [720p]](https://uptobox.com/6qllg1h3e1ex)
- [[Refrain] Sanrio Danshi - 10 VOSTFR [1080p]](https://uptobox.com/n6o6cfr1tv4a)
- [[Refrain] Sanrio Danshi - 11 VOSTFR [720p]](https://uptobox.com/hfbhnsfqabij)
- [[Refrain] Sanrio Danshi - 11 VOSTFR [1080p]](https://uptobox.com/48246jgz5ah4)
- [[Refrain] Sanrio Danshi - 12 VOSTFR [720p]](https://uptobox.com/0vkm02duqnpu)
- [[Refrain] Sanrio Danshi - 12 VOSTFR [1080p]](https://uptobox.com/p1z466rr8hc1)
