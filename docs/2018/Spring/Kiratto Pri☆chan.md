# Kiratto Pri☆chan

![Kiratto Pri☆chan](https://cdn.myanimelist.net/images/anime/1628/95071l.jpg)

* Japanese:  キラッとプリ☆チャン

## Information

 - Type: TV
 - Episodes: Unknown
 - Status: Currently Airing
 - Aired: Apr 8, 2018 to Mar, 2019
 - Premiered: Spring 2018
 - Broadcast: Sundays at 10:00 (JST)
 - Producers: None found, add some 
 - Licensors: None found, add some 
 - Studios: Tatsunoko Production, Dongwoo A&E 
 - Source: Game
 - Genres: Magic, Music, Shoujo, Slice of Life
 - Duration: 24 min.
 - Rating: PG - Children


## Links

