# Sword Art Online Alternative: Gun Gale Online

![Sword Art Online Alternative: Gun Gale Online](https://cdn.myanimelist.net/images/anime/1141/93288l.jpg)

* Japanese:  ソードアート・オンライン オルタナティブ ガンゲイル・オンライン

## Information

 - Type: TV
 - Episodes: 12
 - Status: Finished Airing
 - Aired: Apr 8, 2018 to Jul 1, 2018
 - Premiered: Spring 2018
 - Broadcast: Sundays at 00:00 (JST)
 - Producers: Aniplex, Egg Firm
 - Licensors: Aniplex of America
 - Studios: Studio 3Hz
 - Source: Light novel
 - Genres: Action, Fantasy, Game, Military, Sci-Fi
 - Duration: 23 min. per ep.
 - Rating: PG-13 - Teens 13 or older


## Links

- [[Refrain] Sword Art Online Alternative - Gun Gale Online - 01 VOSTFR [720p]](https://uptobox.com/ohk4c9wpk6j7)
- [[Refrain] Sword Art Online Alternative - Gun Gale Online - 01 VOSTFR [1080p]](https://uptobox.com/fzz3plbzh7eo)
- [[Refrain] Sword Art Online Alternative - Gun Gale Online - 02 VOSTFR [720p]](https://uptobox.com/s2w2wibkyjbg)
- [[Refrain] Sword Art Online Alternative - Gun Gale Online - 02 VOSTFR [1080p]](https://uptobox.com/ea1h96nvrmpr)
- [[Refrain] Sword Art Online Alternative - Gun Gale Online - 03 VOSTFR [720p]](https://uptobox.com/z8euf888c8ql)
- [[Refrain] Sword Art Online Alternative - Gun Gale Online - 03 VOSTFR [1080p]](https://uptobox.com/w1yg1rrsy44l)
- [[Refrain] Sword Art Online Alternative - Gun Gale Online - 04 VOSTFR [720p]](https://uptobox.com/rqtqpnjg3ohk)
- [[Refrain] Sword Art Online Alternative - Gun Gale Online - 04 VOSTFR [1080p]](https://uptobox.com/qqa93oxl0rse)
- [[Refrain] Sword Art Online Alternative - Gun Gale Online - 05 VOSTFR [720p]](https://uptobox.com/iszu2egsr9nh)
- [[Refrain] Sword Art Online Alternative - Gun Gale Online - 05 VOSTFR [1080p]](https://uptobox.com/1u6qnud4ytkz)
- [[Refrain] Sword Art Online Alternative - Gun Gale Online - 06 VOSTFR [720p]](https://uptobox.com/7gjt9o0488q1)
- [[Refrain] Sword Art Online Alternative - Gun Gale Online - 06 VOSTFR [1080p]](https://uptobox.com/xt6ijcm82cr6)
- [[Refrain] Sword Art Online Alternative - Gun Gale Online - 07 VOSTFR [720p]](https://uptobox.com/nvfegym9chih)
- [[Refrain] Sword Art Online Alternative - Gun Gale Online - 07 VOSTFR [1080p]](https://uptobox.com/4qfrxno09kdp)
- [[Refrain] Sword Art Online Alternative - Gun Gale Online - 08 VOSTFR [720p]](https://uptobox.com/y26v7d2w32zj)
- [[Refrain] Sword Art Online Alternative - Gun Gale Online - 08 VOSTFR [1080p]](https://uptobox.com/pca2tqqqv0x3)
- [[Refrain] Sword Art Online Alternative - Gun Gale Online - 09 VOSTFR [720p]](https://uptobox.com/z9zcocqwez1v)
- [[Refrain] Sword Art Online Alternative - Gun Gale Online - 09 VOSTFR [1080p]](https://uptobox.com/scl4fguwbw4s)
- [[Refrain] Sword Art Online Alternative - Gun Gale Online - 10 VOSTFR [720p]](https://uptobox.com/s3qwhlwb10m7)
- [[Refrain] Sword Art Online Alternative - Gun Gale Online - 10 VOSTFR [1080p]](https://uptobox.com/zle2hsix51r3)
- [[Refrain] Sword Art Online Alternative - Gun Gale Online - 11 VOSTFR [720p]](https://uptobox.com/q7syie49uls1)
- [[Refrain] Sword Art Online Alternative - Gun Gale Online - 11 VOSTFR [1080p]](https://uptobox.com/csxhkfivw80d)
- [[Refrain] Sword Art Online Alternative - Gun Gale Online - 12 VOSTFR [720p]](https://uptobox.com/srkbhq9nwyg6)
- [[Refrain] Sword Art Online Alternative - Gun Gale Online - 12 VOSTFR [1080p]](https://uptobox.com/dqan25o6rk6h)
