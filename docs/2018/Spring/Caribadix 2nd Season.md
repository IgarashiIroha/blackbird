# Caribadix 2nd Season

![Caribadix 2nd Season](https://cdn.myanimelist.net/images/anime/1400/91641l.jpg)

* Japanese:  ディッキー&カリーナ 2ndシーズン

## Information

 - Type: TV
 - Episodes: Unknown
 - Status: Currently Airing
 - Aired: Apr 1, 2018 to ?
 - Premiered: Spring 2018
 - Broadcast: Sundays at Unknown
 - Producers: None found, add some 
 - Licensors: None found, add some 
 - Studios: None found, add some 
 - Source: Original
 - Genres: Music, Comedy
 - Duration: 45 sec.
 - Rating: PG - Children


## Links

