# Lost Song

![Lost Song](https://cdn.myanimelist.net/images/anime/1494/91963l.jpg)

* Japanese:  LOST SONG

## Information

 - Type: TV
 - Episodes: 12
 - Status: Finished Airing
 - Aired: Apr 8, 2018 to Jun 16, 2018
 - Premiered: Spring 2018
 - Broadcast: Sundays at 01:30 (JST)
 - Producers: Ultra Super Pictures, MAGES.
 - Licensors: None found, add some
 - Studios: Dwango, LIDENFILMS
 - Source: Original
 - Genres: Drama, Fantasy
 - Duration: 23 min. per ep.
 - Rating: PG-13 - Teens 13 or older


## Links

- [[Refrain] Lost Song - 01 VF VOSTFR [720p]](https://uptobox.com/rif9pxkqxjf0)
- [[Refrain] Lost Song - 01 VF VOSTFR [1080p]](https://uptobox.com/vkmxz1vmxrl0)
- [[Refrain] Lost Song - 02 VF VOSTFR [720p]](https://uptobox.com/gjslkumn642m)
- [[Refrain] Lost Song - 02 VF VOSTFR [1080p]](https://uptobox.com/aaxscfusk1qa)
- [[Refrain] Lost Song - 03 VF VOSTFR [720p]](https://uptobox.com/zbywck3n61kk)
- [[Refrain] Lost Song - 03 VF VOSTFR [1080p]](https://uptobox.com/lyqhx7x9l398)
- [[Refrain] Lost Song - 04 VF VOSTFR [720p]](https://uptobox.com/ytxpmo3m2hew)
- [[Refrain] Lost Song - 04 VF VOSTFR [1080p]](https://uptobox.com/f9xmmp5rkqnl)
- [[Refrain] Lost Song - 05 VF VOSTFR [720p]](https://uptobox.com/pmlnlinf7al0)
- [[Refrain] Lost Song - 05 VF VOSTFR [1080p]](https://uptobox.com/1i1njwq34rqg)
- [[Refrain] Lost Song - 06 VF VOSTFR [720p]](https://uptobox.com/wk4b8lki0xiu)
- [[Refrain] Lost Song - 06 VF VOSTFR [1080p]](https://uptobox.com/cyy8skrncrrs)
- [[Refrain] Lost Song - 07 VF VOSTFR [720p]](https://uptobox.com/61lhmgodw3vm)
- [[Refrain] Lost Song - 07 VF VOSTFR [1080p]](https://uptobox.com/6immz63z81vv)
- [[Refrain] Lost Song - 08 VF VOSTFR [720p]](https://uptobox.com/890n9933hsk0)
- [[Refrain] Lost Song - 08 VF VOSTFR [1080p]](https://uptobox.com/80kodio27wf8)
- [[Refrain] Lost Song - 09 VF VOSTFR [720p]](https://uptobox.com/opc9sorka0zp)
- [[Refrain] Lost Song - 09 VF VOSTFR [1080p]](https://uptobox.com/dfi3tr3mt0b6)
- [[Refrain] Lost Song - 10 VF VOSTFR [720p]](https://uptobox.com/yfbhjql2khav)
- [[Refrain] Lost Song - 10 VF VOSTFR [1080p]](https://uptobox.com/tdmz6jfi9vyw)
- [[Refrain] Lost Song - 11 VF VOSTFR [720p]](https://uptobox.com/mf5womir4zbc)
- [[Refrain] Lost Song - 11 VF VOSTFR [1080p]](https://uptobox.com/1wgsghf79xmu)
- [[Refrain] Lost Song - 12 VF VOSTFR [720p]](https://uptobox.com/3vdp18y4saup)
- [[Refrain] Lost Song - 12 VF VOSTFR [1080p]](https://uptobox.com/fv46e2apr2w2)
