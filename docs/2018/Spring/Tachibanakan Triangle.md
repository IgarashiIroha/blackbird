# Tachibanakan Triangle

![Tachibanakan Triangle](https://cdn.myanimelist.net/images/anime/1436/91859l.jpg)

* Japanese:  立花館To Lieあんぐる

## Information

 - Type: TV
 - Episodes: 12
 - Status: Finished Airing
 - Aired: Apr 4, 2018 to Jun 20, 2018
 - Premiered: Spring 2018
 - Broadcast: Wednesdays at 01:00 (JST)
 - Producers: Ichijinsha
 - Licensors: None found, add some
 - Studios: Creators in Pack, Studio Lings
 - Source: Manga
 - Genres: Comedy, Ecchi, Shoujo Ai
 - Duration: 3 min. per ep.
 - Rating: R+ - Mild Nudity


## Links

- [[Refrain] Tachibanakan Triangle - 01 VOSTFR [720p]](https://uptobox.com/mucbe7prohyw)
- [[Refrain] Tachibanakan Triangle - 01 VOSTFR [1080p]](https://uptobox.com/fw9kiuutzwwu)
- [[Refrain] Tachibanakan Triangle - 02 VOSTFR [720p]](https://uptobox.com/62c5pae7j3wu)
- [[Refrain] Tachibanakan Triangle - 02 VOSTFR [1080p]](https://uptobox.com/itr3kz0yfzbi)
- [[Refrain] Tachibanakan Triangle - 03 VOSTFR [720p]](https://uptobox.com/rk27iulxnltc)
- [[Refrain] Tachibanakan Triangle - 03 VOSTFR [1080p]](https://uptobox.com/i8c3cjbugva2)
- [[Refrain] Tachibanakan Triangle - 04 VOSTFR [720p]](https://uptobox.com/n65hu02c1tq3)
- [[Refrain] Tachibanakan Triangle - 04 VOSTFR [1080p]](https://uptobox.com/nmdf9862743h)
- [[Refrain] Tachibanakan Triangle - 05 VOSTFR [720p]](https://uptobox.com/p1lwvt5zlroe)
- [[Refrain] Tachibanakan Triangle - 05 VOSTFR [1080p]](https://uptobox.com/b9ccb25tmj7m)
- [[Refrain] Tachibanakan Triangle - 06 VOSTFR [720p]](https://uptobox.com/s8gcbg97b8xk)
- [[Refrain] Tachibanakan Triangle - 06 VOSTFR [1080p]](https://uptobox.com/lwl14z607lis)
- [[Refrain] Tachibanakan Triangle - 07 VOSTFR [720p]](https://uptobox.com/8ng4al9mbqhn)
- [[Refrain] Tachibanakan Triangle - 07 VOSTFR [1080p]](https://uptobox.com/j1ywn4lx4yqs)
- [[Refrain] Tachibanakan Triangle - 08 VOSTFR [720p]](https://uptobox.com/r01ohjhzg72l)
- [[Refrain] Tachibanakan Triangle - 08 VOSTFR [1080p]](https://uptobox.com/kcfmtf03fic9)
- [[Refrain] Tachibanakan Triangle - 09 VOSTFR [720p]](https://uptobox.com/i77no370c9fh)
- [[Refrain] Tachibanakan Triangle - 09 VOSTFR [1080p]](https://uptobox.com/9n7g1iib5uto)
- [[Refrain] Tachibanakan Triangle - 10 VOSTFR [720p]](https://uptobox.com/7lq2m9lisa4u)
- [[Refrain] Tachibanakan Triangle - 10 VOSTFR [1080p]](https://uptobox.com/uh2365wgcxwn)
- [[Refrain] Tachibanakan Triangle - 11 VOSTFR [720p]](https://uptobox.com/wuowg5g4ub6j)
- [[Refrain] Tachibanakan Triangle - 11 VOSTFR [1080p]](https://uptobox.com/jtdbetyev7i2)
- [[Refrain] Tachibanakan Triangle - 12 VOSTFR [720p]](https://uptobox.com/tzokkedg1kkz)
- [[Refrain] Tachibanakan Triangle - 12 VOSTFR [1080p]](https://uptobox.com/amxpa8snwuwj)
