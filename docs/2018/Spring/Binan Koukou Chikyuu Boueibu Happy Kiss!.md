# Binan Koukou Chikyuu Boueibu Happy Kiss!

![Binan Koukou Chikyuu Boueibu Happy Kiss!](https://cdn.myanimelist.net/images/anime/1259/91819l.jpg)

* Japanese:  美男高校地球防衛部HAPPY KISS!

## Information

 - Type: TV
 - Episodes: 12
 - Status: Finished Airing
 - Aired: Apr 9, 2018 to Jul 2, 2018
 - Premiered: Spring 2018
 - Broadcast: Mondays at 01:35 (JST)
 - Producers: None found, add some
 - Licensors: None found, add some
 - Studios: Studio Comet
 - Source: Original
 - Genres: Comedy, Magic, Parody, School, Slice of Life
 - Duration: 24 min. per ep.
 - Rating: PG-13 - Teens 13 or older


## Links

- [[Refrain] Binan Koukou Chikyuu Boueibu Happy Kiss! - 01 VOSTFR [720p]](https://uptobox.com/pa5m0aelzabc)
- [[Refrain] Binan Koukou Chikyuu Boueibu Happy Kiss! - 01 VOSTFR [1080p]](https://uptobox.com/d24y2dw29z7s)
- [[Refrain] Binan Koukou Chikyuu Boueibu Happy Kiss! - 02 VOSTFR [720p]](https://uptobox.com/4cedluim28bt)
- [[Refrain] Binan Koukou Chikyuu Boueibu Happy Kiss! - 02 VOSTFR [1080p]](https://uptobox.com/vzlvm37fsfa3)
- [[Refrain] Binan Koukou Chikyuu Boueibu Happy Kiss! - 03 VOSTFR [720p]](https://uptobox.com/670oal00gmwm)
- [[Refrain] Binan Koukou Chikyuu Boueibu Happy Kiss! - 03 VOSTFR [1080p]](https://uptobox.com/9joz7r5iq8y7)
- [[Refrain] Binan Koukou Chikyuu Boueibu Happy Kiss! - 04 VOSTFR [720p]](https://uptobox.com/nd53n5f1ldnr)
- [[Refrain] Binan Koukou Chikyuu Boueibu Happy Kiss! - 04 VOSTFR [1080p]](https://uptobox.com/mlz25kq4nclv)
- [[Refrain] Binan Koukou Chikyuu Boueibu Happy Kiss! - 05 VOSTFR [720p]](https://uptobox.com/krut5grqwmwe)
- [[Refrain] Binan Koukou Chikyuu Boueibu Happy Kiss! - 05 VOSTFR [1080p]](https://uptobox.com/170cdk3uoqs3)
- [[Refrain] Binan Koukou Chikyuu Boueibu Happy Kiss! - 06 VOSTFR [720p]](https://uptobox.com/ut53pz9kgnro)
- [[Refrain] Binan Koukou Chikyuu Boueibu Happy Kiss! - 06 VOSTFR [1080p]](https://uptobox.com/nytf0du7u4pd)
- [[Refrain] Binan Koukou Chikyuu Boueibu Happy Kiss! - 07 VOSTFR [720p]](https://uptobox.com/hnnj5wabr2lp)
- [[Refrain] Binan Koukou Chikyuu Boueibu Happy Kiss! - 07 VOSTFR [1080p]](https://uptobox.com/10ah1y3t487f)
- [[Refrain] Binan Koukou Chikyuu Boueibu Happy Kiss! - 08 VOSTFR [720p]](https://uptobox.com/fokmfwgkxhoq)
- [[Refrain] Binan Koukou Chikyuu Boueibu Happy Kiss! - 08 VOSTFR [1080p]](https://uptobox.com/uc95ar58dicl)
- [[Refrain] Binan Koukou Chikyuu Boueibu Happy Kiss! - 09 VOSTFR [720p]](https://uptobox.com/3biaew52a12b)
- [[Refrain] Binan Koukou Chikyuu Boueibu Happy Kiss! - 09 VOSTFR [1080p]](https://uptobox.com/fihs1kg3rt9k)
- [[Refrain] Binan Koukou Chikyuu Boueibu Happy Kiss! - 11 VOSTFR [720p]](https://uptobox.com/bljek75n6v7c)
- [[Refrain] Binan Koukou Chikyuu Boueibu Happy Kiss! - 11 VOSTFR [1080p]](https://uptobox.com/ej0zkpa7agy6)
- [[Refrain] Binan Koukou Chikyuu Boueibu Happy Kiss! - 12 VOSTFR [720p]](https://uptobox.com/so2glpw41ri0)
- [[Refrain] Binan Koukou Chikyuu Boueibu Happy Kiss! - 12 VOSTFR [1080p]](https://uptobox.com/hofmg5qjeirc)
