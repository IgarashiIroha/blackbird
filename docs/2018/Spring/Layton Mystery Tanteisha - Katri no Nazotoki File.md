# Layton Mystery Tanteisha: Katri no Nazotoki File

![Layton Mystery Tanteisha: Katri no Nazotoki File](https://cdn.myanimelist.net/images/anime/1115/90222l.jpg)

* Japanese:  レイトン ミステリー探偵社~カトリーのナゾトキファイル~

## Information

 - Type: TV
 - Episodes: 50
 - Status: Currently Airing
 - Aired: Apr 8, 2018 to ?
 - Premiered: Spring 2018
 - Broadcast: Sundays at 08:30 (JST)
 - Producers: Level-5 
 - Licensors: None found, add some 
 - Studios: LIDENFILMS 
 - Source: Game
 - Genres: Comedy, Kids, Mystery
 - Duration: 23 min. per ep.
 - Rating: PG - Children


## Links

