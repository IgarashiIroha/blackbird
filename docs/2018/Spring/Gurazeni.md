# Gurazeni

![Gurazeni](https://cdn.myanimelist.net/images/anime/1365/95058l.jpg)

* Japanese:  グラゼニ

## Information

 - Type: TV
 - Episodes: 12
 - Status: Finished Airing
 - Aired: Apr 6, 2018 to Jun 22, 2018
 - Premiered: Spring 2018
 - Broadcast: Fridays at 22:30 (JST)
 - Producers: Kodansha, Ai Addiction, SKY PerfecTV! 
 - Licensors: None found, add some 
 - Studios: Studio Deen 
 - Source: Manga
 - Genres: Comedy, Drama, Seinen, Sports
 - Duration: 23 min. per ep.
 - Rating: PG-13 - Teens 13 or older


## Links

