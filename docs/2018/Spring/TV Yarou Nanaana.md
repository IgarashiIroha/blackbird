# TV Yarou Nanaana

![TV Yarou Nanaana](https://cdn.myanimelist.net/images/anime/1406/95743l.jpg)

* Japanese:  テレビ野郎 ナナーナ

## Information

 - Type: TV
 - Episodes: 24
 - Status: Finished Airing
 - Aired: Apr 6, 2018 to Sep 14, 2018
 - Premiered: Spring 2018
 - Broadcast: Fridays at 21:54 (JST)
 - Producers: None found, add some 
 - Licensors: None found, add some 
 - Studios: Studio Crocodile 
 - Source: Original
 - Genres: Comedy
 - Duration: 3 min. per ep.
 - Rating: PG-13 - Teens 13 or older


## Links

