# Uchuu Senkan Tiramisù

![Uchuu Senkan Tiramisù](https://cdn.myanimelist.net/images/anime/1864/93615l.jpg)

* Japanese:  宇宙戦艦ティラミス

## Information

 - Type: TV
 - Episodes: 13
 - Status: Finished Airing
 - Aired: Apr 3, 2018 to Jun 26, 2018
 - Premiered: Spring 2018
 - Broadcast: Tuesdays at 01:00 (JST)
 - Producers: Frontier Works, Nippon Columbia, Studio Mausu, Tokyo MX, Adores, Shinchosha, Furyu, Exa International, NTT Plala, JY Animation 
 - Licensors: Funimation 
 - Studios: Gonzo 
 - Source: Web manga
 - Genres: Space, Mecha, Comedy, Sci-Fi, Slice of Life
 - Duration: 7 min. per ep.
 - Rating: PG-13 - Teens 13 or older


## Links

