# Amai Choubatsu: Watashi wa Kanshu Senyou Pet

![Amai Choubatsu: Watashi wa Kanshu Senyou Pet](https://cdn.myanimelist.net/images/anime/1374/90580l.jpg)

* Japanese:  甘い懲罰~私は看守専用ペット

## Information

 - Type: TV
 - Episodes: 13
 - Status: Finished Airing
 - Aired: Apr 2, 2018 to Jun 25, 2018
 - Premiered: Spring 2018
 - Broadcast: Mondays at 01:00 (JST)
 - Producers: Picante Circus, Suiseisha 
 - Licensors: None found, add some 
 - Studios: Magic Bus 
 - Source: Web manga
 - Genres: Ecchi
 - Duration: 3 min. per ep.
 - Rating: R+ - Mild Nudity


## Links

