# 3D Kanojo: Real Girl

![3D Kanojo: Real Girl](https://cdn.myanimelist.net/images/anime/1327/93616l.jpg)

* Japanese:  3D彼女 リアルガール

## Information

 - Type: TV
 - Episodes: 12
 - Status: Finished Airing
 - Aired: Apr 4, 2018 to Jun 20, 2018
 - Premiered: Spring 2018
 - Broadcast: Wednesdays at 01:59 (JST)
 - Producers: None found, add some 
 - Licensors: Sentai Filmworks 
 - Studios: Hoods Entertainment 
 - Source: Manga
 - Genres: Romance, School, Shoujo
 - Duration: 22 min. per ep.
 - Rating: PG-13 - Teens 13 or older


## Links

