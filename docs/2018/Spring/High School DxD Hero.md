# High School DxD Hero

![High School DxD Hero](https://cdn.myanimelist.net/images/anime/1189/93528l.jpg)

* Japanese:  ハイスクールDxD HERO

## Information

 - Type: TV
 - Episodes: 12
 - Status: Finished Airing
 - Aired: Apr 17, 2018 to Jul 3, 2018
 - Premiered: Spring 2018
 - Broadcast: Tuesdays at 23:00 (JST)
 - Producers: AT-X
 - Licensors: Funimation
 - Studios: Passione
 - Source: Light novel
 - Genres: Action, Ecchi, Romance, Comedy, Harem, School, Demons
 - Duration: 23 min. per ep.
 - Rating: R+ - Mild Nudity


## Links

- [[Refrain] High School DxD Hero 01 VOSTFR NC BD {ADN} [1080p]](https://uptobox.com/l5z8f0rr6go7)
- [[Refrain] High School DxD Hero 02 VOSTFR NC BD {ADN} [1080p]](https://uptobox.com/5isdi5zsbm9h)
- [[Refrain] High School DxD Hero 03 VOSTFR NC BD {ADN} [1080p]](https://uptobox.com/i7lyhg9xnalk)
- [[Refrain] High School DxD Hero 04 VOSTFR NC BD {ADN} [1080p]](https://uptobox.com/y3ark8zzx6ph)
- [[Refrain] High School DxD Hero 05 VOSTFR NC BD {ADN} [1080p]](https://uptobox.com/lyos5kgk7ddv)
- [[Refrain] High School DxD Hero 06 VOSTFR NC BD {ADN} [1080p]](https://uptobox.com/l4tks06gsjc1)
- [[Refrain] High School DxD Hero 07 VOSTFR NC BD {ADN} [1080p]](https://uptobox.com/wxjauzp7r0xg)
- [[Refrain] High School DxD Hero 08 VOSTFR NC BD {ADN} [1080p]](https://uptobox.com/h3e3bifrfswb)
- [[Refrain] High School DxD Hero 09 VOSTFR NC BD {ADN} [1080p]](https://uptobox.com/3bpmattpg5xw)
- [[Refrain] High School DxD Hero 10 VOSTFR NC BD {ADN} [1080p]](https://uptobox.com/a5zieuizglsc)
- [[Refrain] High School DxD Hero 11 VOSTFR NC BD {ADN} [1080p]](https://uptobox.com/0yejjak6iyyo)
- [[Refrain] High School DxD Hero 12 VOSTFR NC BD {ADN} [1080p]](https://uptobox.com/uo5e1qmadpsp)
- [[Refrain] High School DxD Hero 00 VOSTFR NC BD {ADN} [1080p]](https://uptobox.com/y7hzgaphom13)
