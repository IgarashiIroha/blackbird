# Tada-kun wa Koi wo Shinai

![Tada-kun wa Koi wo Shinai](https://cdn.myanimelist.net/images/anime/1446/91841l.jpg)

* Japanese:  多田くんは恋をしない

## Information

 - Type: TV
 - Episodes: 13
 - Status: Finished Airing
 - Aired: Apr 5, 2018 to Jun 28, 2018
 - Premiered: Spring 2018
 - Broadcast: Thursdays at 21:00 (JST)
 - Producers: Media Factory, AT-X, Docomo Anime Store, BS11
 - Licensors: Sentai Filmworks
 - Studios: Doga Kobo
 - Source: Original
 - Genres: Comedy, Romance, Slice of Life
 - Duration: 23 min. per ep.
 - Rating: PG-13 - Teens 13 or older


## Links

- [[Refrain] Tada-kun wa Koi wo Shinai - 01 VOSTFR {ADN} [720p]](https://uptobox.com/coklvscjxo0w)
- [[Refrain] Tada-kun wa Koi wo Shinai - 01 VOSTFR {ADN} [1080p]](https://uptobox.com/g0btf28ujxa0)
- [[Refrain] Tada-kun wa Koi wo Shinai - 02 VOSTFR {ADN} [720p]](https://uptobox.com/d479p1vxqo4j)
- [[Refrain] Tada-kun wa Koi wo Shinai - 02 VOSTFR {ADN} [1080p]](https://uptobox.com/4wkhhl063aoy)
- [[Refrain] Tada-kun wa Koi wo Shinai - 03 VOSTFR {ADN} [720p]](https://uptobox.com/7lavj29ktwyh)
- [[Refrain] Tada-kun wa Koi wo Shinai - 03 VOSTFR {ADN} [1080p]](https://uptobox.com/z7ng2x7jnc4m)
- [[Refrain] Tada-kun wa Koi wo Shinai - 04 VOSTFR {ADN} [720p]](https://uptobox.com/n5hvf8t8vnoo)
- [[Refrain] Tada-kun wa Koi wo Shinai - 04 VOSTFR {ADN} [1080p]](https://uptobox.com/br41dedfi82u)
- [[Refrain] Tada-kun wa Koi wo Shinai - 05 VOSTFR {ADN} [720p]](https://uptobox.com/oxfblc8hnhxv)
- [[Refrain] Tada-kun wa Koi wo Shinai - 05 VOSTFR {ADN} [1080p]](https://uptobox.com/7dvk36f5mqar)
- [[Refrain] Tada-kun wa Koi wo Shinai - 06 VOSTFR {ADN} [720p]](https://uptobox.com/9jt4mg15s7yr)
- [[Refrain] Tada-kun wa Koi wo Shinai - 06 VOSTFR {ADN} [1080p]](https://uptobox.com/a8cnlyzdrg0d)
- [[Refrain] Tada-kun wa Koi wo Shinai - 07 VOSTFR {ADN} [720p]](https://uptobox.com/l11gza194xsa)
- [[Refrain] Tada-kun wa Koi wo Shinai - 07 VOSTFR {ADN} [1080p]](https://uptobox.com/86855i1vx5rx)
- [[Refrain] Tada-kun wa Koi wo Shinai - 08 VOSTFR {ADN} [720p]](https://uptobox.com/pfjacb3zhs2t)
- [[Refrain] Tada-kun wa Koi wo Shinai - 08 VOSTFR {ADN} [1080p]](https://uptobox.com/yn155n7abnnl)
- [[Refrain] Tada-kun wa Koi wo Shinai - 09 VOSTFR {ADN} [720p]](https://uptobox.com/fr1oqf5tkdf4)
- [[Refrain] Tada-kun wa Koi wo Shinai - 09 VOSTFR {ADN} [1080p]](https://uptobox.com/o9vr10ss61bm)
- [[Refrain] Tada-kun wa Koi wo Shinai - 10 VOSTFR {ADN} [720p]](https://uptobox.com/akhfkgupv635)
- [[Refrain] Tada-kun wa Koi wo Shinai - 10 VOSTFR {ADN} [1080p]](https://uptobox.com/6i89iqt1iqp6)
- [[Refrain] Tada-kun wa Koi wo Shinai - 11 VOSTFR {ADN} [720p]](https://uptobox.com/476zu7tn288o)
- [[Refrain] Tada-kun wa Koi wo Shinai - 11 VOSTFR {ADN} [1080p]](https://uptobox.com/1yacdsc93ryx)
- [[Refrain] Tada-kun wa Koi wo Shinai - 12 VOSTFR {ADN} [720p]](https://uptobox.com/qx6uimyyp2sv)
- [[Refrain] Tada-kun wa Koi wo Shinai - 12 VOSTFR {ADN} [1080p]](https://uptobox.com/5g6tsr30a5k0)
- [[Refrain] Tada-kun wa Koi wo Shinai - 13 VOSTFR {ADN} [720p]](https://uptobox.com/3duucydbrb3f)
- [[Refrain] Tada-kun wa Koi wo Shinai - 13 VOSTFR {ADN} [1080p]](https://uptobox.com/khkkshpqlbiz)
