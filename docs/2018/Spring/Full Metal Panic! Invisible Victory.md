# Full Metal Panic! Invisible Victory

![Full Metal Panic! Invisible Victory](https://cdn.myanimelist.net/images/anime/1033/92011l.jpg)

* Japanese:  フルメタル・パニック！Invisible Victory

## Information

 - Type: TV
 - Episodes: 12
 - Status: Finished Airing
 - Aired: Apr 13, 2018 to Jul 18, 2018
 - Premiered: Spring 2018
 - Broadcast: Fridays at 20:30 (JST)
 - Producers: None found, add some 
 - Licensors: Funimation 
 - Studios: Xebec 
 - Source: Light novel
 - Genres: Action, Mecha, Military
 - Duration: 24 min. per ep.
 - Rating: R - 17+ (violence & profanity)


## Links

