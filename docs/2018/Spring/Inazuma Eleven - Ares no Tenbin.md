# Inazuma Eleven: Ares no Tenbin

![Inazuma Eleven: Ares no Tenbin](https://cdn.myanimelist.net/images/anime/1188/90223l.jpg)

* Japanese:  イナズマイレブン アレスの天秤

## Information

 - Type: TV
 - Episodes: 26
 - Status: Finished Airing
 - Aired: Apr 6, 2018 to Sep 28, 2018
 - Premiered: Spring 2018
 - Broadcast: Fridays at 17:55 (JST)
 - Producers: TV Tokyo, Dentsu
 - Licensors: None found, add some
 - Studios: OLM
 - Source: Game
 - Genres: Sports
 - Duration: 23 min. per ep.
 - Rating: G - All Ages


## Links

- [[Refrain] Isekai Izakaya - Koto Aitheria no Izakaya Nobu - 01 VOSTFR [720p]](https://uptobox.com/9hs49rtokac3)
- [[Refrain] Isekai Izakaya - Koto Aitheria no Izakaya Nobu - 01 VOSTFR [1080p]](https://uptobox.com/2fip5s7f52f5)
- [[Refrain] Isekai Izakaya - Koto Aitheria no Izakaya Nobu - 02 VOSTFR [720p]](https://uptobox.com/zrv0zzkud5mp)
- [[Refrain] Isekai Izakaya - Koto Aitheria no Izakaya Nobu - 02 VOSTFR [1080p]](https://uptobox.com/rc2phe9921rd)
- [[Refrain] Isekai Izakaya - Koto Aitheria no Izakaya Nobu - 03 VOSTFR [720p]](https://uptobox.com/znqyuyy3mvjz)
- [[Refrain] Isekai Izakaya - Koto Aitheria no Izakaya Nobu - 03 VOSTFR [1080p]](https://uptobox.com/znxksel2eyvy)
- [[Refrain] Isekai Izakaya - Koto Aitheria no Izakaya Nobu - 04 VOSTFR [720p]](https://uptobox.com/ywe3dhbq1805)
- [[Refrain] Isekai Izakaya - Koto Aitheria no Izakaya Nobu - 04 VOSTFR [1080p]](https://uptobox.com/vihxrz9u98x1)
- [[Refrain] Isekai Izakaya - Koto Aitheria no Izakaya Nobu - 05 VOSTFR [720p]](https://uptobox.com/slsxwppd9p0c)
- [[Refrain] Isekai Izakaya - Koto Aitheria no Izakaya Nobu - 05 VOSTFR [1080p]](https://uptobox.com/gr3kuegwrl6c)
- [[Refrain] Isekai Izakaya - Koto Aitheria no Izakaya Nobu - 06 VOSTFR [720p]](https://uptobox.com/q1u2x53pt2ej)
- [[Refrain] Isekai Izakaya - Koto Aitheria no Izakaya Nobu - 06 VOSTFR [1080p]](https://uptobox.com/lm6foreiklfc)
- [[Refrain] Isekai Izakaya - Koto Aitheria no Izakaya Nobu - 07 VOSTFR [720p]](https://uptobox.com/zw1cwv9zr7lo)
- [[Refrain] Isekai Izakaya - Koto Aitheria no Izakaya Nobu - 07 VOSTFR [1080p]](https://uptobox.com/uwy7sltu1x7l)
- [[Refrain] Isekai Izakaya - Koto Aitheria no Izakaya Nobu - 08 VOSTFR [720p]](https://uptobox.com/4tnmktwy3938)
- [[Refrain] Isekai Izakaya - Koto Aitheria no Izakaya Nobu - 08 VOSTFR [1080p]](https://uptobox.com/icx4c7z86vzh)
- [[Refrain] Isekai Izakaya - Koto Aitheria no Izakaya Nobu - 09 VOSTFR [720p]](https://uptobox.com/wa6xv6vdg1d5)
- [[Refrain] Isekai Izakaya - Koto Aitheria no Izakaya Nobu - 09 VOSTFR [1080p]](https://uptobox.com/ouvviceu3ob7)
- [[Refrain] Isekai Izakaya - Koto Aitheria no Izakaya Nobu - 10 VOSTFR [720p]](https://uptobox.com/s0klvrbmubkd)
- [[Refrain] Isekai Izakaya - Koto Aitheria no Izakaya Nobu - 10 VOSTFR [1080p]](https://uptobox.com/694uv1thlep1)
- [[Refrain] Isekai Izakaya - Koto Aitheria no Izakaya Nobu - 11 VOSTFR [720p]](https://uptobox.com/dk97zo67f1vx)
- [[Refrain] Isekai Izakaya - Koto Aitheria no Izakaya Nobu - 11 VOSTFR [1080p]](https://uptobox.com/ztnoefzcv35h)
- [[Refrain] Isekai Izakaya - Koto Aitheria no Izakaya Nobu - 12 VOSTFR [720p]](https://uptobox.com/tlkhzpkckkds)
- [[Refrain] Isekai Izakaya - Koto Aitheria no Izakaya Nobu - 12 VOSTFR [1080p]](https://uptobox.com/xazuszle3b79)
- [[Refrain] Isekai Izakaya - Koto Aitheria no Izakaya Nobu - 13 VOSTFR [720p]](https://uptobox.com/34ml86ilbpcs)
- [[Refrain] Isekai Izakaya - Koto Aitheria no Izakaya Nobu - 13 VOSTFR [1080p]](https://uptobox.com/h32trq81s818)
- [[Refrain] Isekai Izakaya - Koto Aitheria no Izakaya Nobu - 14 VOSTFR [720p]](https://uptobox.com/3egmzgz0jlkg)
- [[Refrain] Isekai Izakaya - Koto Aitheria no Izakaya Nobu - 14 VOSTFR [1080p]](https://uptobox.com/ckl59il53b1i)
- [[Refrain] Isekai Izakaya - Koto Aitheria no Izakaya Nobu - 15 VOSTFR [720p]](https://uptobox.com/p8jou4o2lns0)
- [[Refrain] Isekai Izakaya - Koto Aitheria no Izakaya Nobu - 15 VOSTFR [1080p]](https://uptobox.com/n1m9lp78vz01)
- [[Refrain] Isekai Izakaya - Koto Aitheria no Izakaya Nobu - 16 VOSTFR [720p]](https://uptobox.com/r8r5t8l0gw2g)
- [[Refrain] Isekai Izakaya - Koto Aitheria no Izakaya Nobu - 16 VOSTFR [1080p]](https://uptobox.com/fsb47ei5r924)
- [[Refrain] Isekai Izakaya - Koto Aitheria no Izakaya Nobu - 17 VOSTFR [720p]](https://uptobox.com/n3kdest6vag0)
- [[Refrain] Isekai Izakaya - Koto Aitheria no Izakaya Nobu - 17 VOSTFR [1080p]](https://uptobox.com/pa6tyrx55l6h)
- [[Refrain] Isekai Izakaya - Koto Aitheria no Izakaya Nobu - 18 VOSTFR [720p]](https://uptobox.com/vn97glt9xgzk)
- [[Refrain] Isekai Izakaya - Koto Aitheria no Izakaya Nobu - 18 VOSTFR [1080p]](https://uptobox.com/e0hec7550fao)
- [[Refrain] Isekai Izakaya - Koto Aitheria no Izakaya Nobu - 19 VOSTFR [720p]](https://uptobox.com/haioltzy2jxy)
- [[Refrain] Isekai Izakaya - Koto Aitheria no Izakaya Nobu - 19 VOSTFR [1080p]](https://uptobox.com/4r0i34g8bxcw)
- [[Refrain] Isekai Izakaya - Koto Aitheria no Izakaya Nobu - 20 VOSTFR [720p]](https://uptobox.com/viwkgndhzjvd)
- [[Refrain] Isekai Izakaya - Koto Aitheria no Izakaya Nobu - 20 VOSTFR [1080p]](https://uptobox.com/w86sn1ps4pu7)
- [[Refrain] Isekai Izakaya - Koto Aitheria no Izakaya Nobu - 21 VOSTFR [720p]](https://uptobox.com/dm5h2yoyu9q2)
- [[Refrain] Isekai Izakaya - Koto Aitheria no Izakaya Nobu - 21 VOSTFR [1080p]](https://uptobox.com/k88h057sklau)
- [[Refrain] Isekai Izakaya - Koto Aitheria no Izakaya Nobu - 22 VOSTFR [720p]](https://uptobox.com/aa3k1w0wk0tp)
- [[Refrain] Isekai Izakaya - Koto Aitheria no Izakaya Nobu - 22 VOSTFR [1080p]](https://uptobox.com/36cgafewffcd)
- [[Refrain] Isekai Izakaya - Koto Aitheria no Izakaya Nobu - 23 VOSTFR [720p]](https://uptobox.com/8gvl7ikctt5j)
- [[Refrain] Isekai Izakaya - Koto Aitheria no Izakaya Nobu - 23 VOSTFR [1080p]](https://uptobox.com/whfkmjhfry1z)
- [[Refrain] Isekai Izakaya - Koto Aitheria no Izakaya Nobu - 24 VOSTFR [720p]](https://uptobox.com/7z3ib0967hof)
- [[Refrain] Isekai Izakaya - Koto Aitheria no Izakaya Nobu - 24 VOSTFR [1080p]](https://uptobox.com/2n07wrcwjrqh)
