# Alice or Alice: Siscon Niisan to Futago no Imouto

![Alice or Alice: Siscon Niisan to Futago no Imouto](https://cdn.myanimelist.net/images/anime/5/89571l.jpg)

* Japanese:  ありすorありす ～シスコン兄さんと双子の妹～

## Information

 - Type: TV
 - Episodes: 12
 - Status: Finished Airing
 - Aired: Apr 4, 2018 to Jun 20, 2018
 - Premiered: Spring 2018
 - Broadcast: Wednesdays at 22:25 (JST)
 - Producers: None found, add some 
 - Licensors: Sentai Filmworks 
 - Studios: EMT² 
 - Source: 4-koma manga
 - Genres: Seinen, Slice of Life
 - Duration: 3 min. per ep.
 - Rating: PG-13 - Teens 13 or older


## Links

