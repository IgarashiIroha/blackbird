# Wakaokami wa Shougakusei!

![Wakaokami wa Shougakusei!](https://cdn.myanimelist.net/images/anime/1825/94402l.jpg)

* Japanese:  若おかみは小学生！

## Information

 - Type: TV
 - Episodes: 24
 - Status: Finished Airing
 - Aired: Apr 8, 2018 to Sep 23, 2018
 - Premiered: Spring 2018
 - Broadcast: Sundays at 07:14 (JST)
 - Producers: TV Tokyo, Nihon Ad Systems, Kodansha, AT-X, GAGA, Techno Sound, NADA Holdings, Aeon Entertainment 
 - Licensors: None found, add some 
 - Studios: Madhouse, DLE 
 - Source: Novel
 - Genres: Comedy, Slice of Life, Supernatural
 - Duration: 12 min. per ep.
 - Rating: G - All Ages


## Links

