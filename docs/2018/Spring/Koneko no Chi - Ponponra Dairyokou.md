# Koneko no Chi: Ponponra Dairyokou

![Koneko no Chi: Ponponra Dairyokou](https://cdn.myanimelist.net/images/anime/1019/90915l.jpg)

* Japanese:  こねこのチー ポンポンらー大旅行

## Information

 - Type: TV
 - Episodes: 25
 - Status: Finished Airing
 - Aired: Apr 8, 2018 to Sep 30, 2018
 - Premiered: Spring 2018
 - Broadcast: Sundays at 07:00 (JST)
 - Producers: None found, add some
 - Licensors: None found, add some
 - Studios: Marza Animation Planet
 - Source: Original
 - Genres: Slice of Life
 - Duration: 12 min. per ep.
 - Rating: G - All Ages


## Links

- [[Pray for nZk] Koneko no Chi - Ponponra Dairyokou - 01 [720p] [Multiple Subtitle]](https://uptobox.com/2mctip17fmdz)
- [[Pray for nZk] Koneko no Chi - Ponponra Dairyokou - 01 [1080p] [Multiple Subtitle]](https://uptobox.com/caf5tqw8044q)
- [[Pray for nZk] Koneko no Chi - Ponponra Dairyokou - 02 [720p] [Multiple Subtitle]](https://uptobox.com/089i35badem9)
- [[Pray for nZk] Koneko no Chi - Ponponra Dairyokou - 02 [1080p] [Multiple Subtitle]](https://uptobox.com/suqxxjpx5stf)
- [[Pray for nZk] Koneko no Chi - Ponponra Dairyokou - 03 [720p] [Multiple Subtitle]](https://uptobox.com/qqrue40137dg)
- [[Pray for nZk] Koneko no Chi - Ponponra Dairyokou - 03 [1080p] [Multiple Subtitle]](https://uptobox.com/g29nx5le7vti)
- [[Pray for nZk] Koneko no Chi - Ponponra Dairyokou - 04 [720p] [Multiple Subtitle]](https://uptobox.com/1yuf2r6c0aq9)
- [[Pray for nZk] Koneko no Chi - Ponponra Dairyokou - 04 [1080p] [Multiple Subtitle]](https://uptobox.com/qewzkl7ekwit)
- [[Pray for nZk] Koneko no Chi - Ponponra Dairyokou - 05 [720p] [Multiple Subtitle]](https://uptobox.com/3oa70oqxt6oq)
- [[Pray for nZk] Koneko no Chi - Ponponra Dairyokou - 05 [1080p] [Multiple Subtitle]](https://uptobox.com/yt8bpiddmo3b)
- [[Pray for nZk] Koneko no Chi - Ponponra Dairyokou - 06 [720p] [Multiple Subtitle]](https://uptobox.com/z2bvf6fpcb23)
- [[Pray for nZk] Koneko no Chi - Ponponra Dairyokou - 06 [1080p] [Multiple Subtitle]](https://uptobox.com/9a84qc61l816)
- [[Pray for nZk] Koneko no Chi - Ponponra Dairyokou - 07 [720p] [Multiple Subtitle]](https://uptobox.com/ayn5ttsyg94s)
- [[Pray for nZk] Koneko no Chi - Ponponra Dairyokou - 07 [1080p] [Multiple Subtitle]](https://uptobox.com/j00xk2mtcr8l)
- [[Pray for nZk] Koneko no Chi - Ponponra Dairyokou - 08 [720p] [Multiple Subtitle]](https://uptobox.com/v2dax8vm5hut)
- [[Pray for nZk] Koneko no Chi - Ponponra Dairyokou - 08 [1080p] [Multiple Subtitle]](https://uptobox.com/sdpr2dpwy572)
- [[Pray for nZk] Koneko no Chi - Ponponra Dairyokou - 09 [720p] [Multiple Subtitle]](https://uptobox.com/uounumnh9q6y)
- [[Pray for nZk] Koneko no Chi - Ponponra Dairyokou - 09 [1080p] [Multiple Subtitle]](https://uptobox.com/bbic2ajwpq6s)
- [[Pray for nZk] Koneko no Chi - Ponponra Dairyokou - 10 [720p] [Multiple Subtitle]](https://uptobox.com/5jgyxb6v6kvc)
- [[Pray for nZk] Koneko no Chi - Ponponra Dairyokou - 10 [1080p] [Multiple Subtitle]](https://uptobox.com/1ehltrdsgrl2)
- [[Pray for nZk] Koneko no Chi - Ponponra Dairyokou - 11 [720p] [Multiple Subtitle]](https://uptobox.com/2l5kuhu273f6)
- [[Pray for nZk] Koneko no Chi - Ponponra Dairyokou - 11 [1080p] [Multiple Subtitle]](https://uptobox.com/mddt3m9efmil)
- [[Pray for nZk] Koneko no Chi - Ponponra Dairyokou - 12 [720p] [Multiple Subtitle]](https://uptobox.com/ctop3oyv49nn)
- [[Pray for nZk] Koneko no Chi - Ponponra Dairyokou - 12 [1080p] [Multiple Subtitle]](https://uptobox.com/r4jidbpx8xno)
- [[Pray for nZk] Koneko no Chi - Ponponra Dairyokou - 13 [720p] [Multiple Subtitle]](https://uptobox.com/64ajqcdqykqz)
- [[Pray for nZk] Koneko no Chi - Ponponra Dairyokou - 13 [1080p] [Multiple Subtitle]](https://uptobox.com/ztpc0fjb70s8)
- [[Pray for nZk] Koneko no Chi - Ponponra Dairyokou - 14 [720p] [Multiple Subtitle]](https://uptobox.com/lun42wyysdv6)
- [[Pray for nZk] Koneko no Chi - Ponponra Dairyokou - 14 [1080p] [Multiple Subtitle]](https://uptobox.com/19lfev1jli55)
- [[Pray for nZk] Koneko no Chi - Ponponra Dairyokou - 15 [720p] [Multiple Subtitle]](https://uptobox.com/eyeaq1ri2ob7)
- [[Pray for nZk] Koneko no Chi - Ponponra Dairyokou - 15 [1080p] [Multiple Subtitle]](https://uptobox.com/nf9b6zdnv0lt)
- [[Pray for nZk] Koneko no Chi - Ponponra Dairyokou - 16 [720p] [Multiple Subtitle]](https://uptobox.com/bou8thy33wi7)
- [[Pray for nZk] Koneko no Chi - Ponponra Dairyokou - 16 [1080p] [Multiple Subtitle]](https://uptobox.com/2bxcv62hh14l)
- [[Pray for nZk] Koneko no Chi - Ponponra Dairyokou - 17 [720p] [Multiple Subtitle]](https://uptobox.com/62v5o3ydamky)
- [[Pray for nZk] Koneko no Chi - Ponponra Dairyokou - 17 [1080p] [Multiple Subtitle]](https://uptobox.com/rh3qaqebav80)
- [[Pray for nZk] Koneko no Chi - Ponponra Dairyokou - 18 [720p] [Multiple Subtitle]](https://uptobox.com/p5yjxqiuy0wg)
- [[Pray for nZk] Koneko no Chi - Ponponra Dairyokou - 18 [1080p] [Multiple Subtitle]](https://uptobox.com/g3x4zaix7yi6)
- [[Pray for nZk] Koneko no Chi - Ponponra Dairyokou - 19 [720p] [Multiple Subtitle]](https://uptobox.com/iunalec9u0eq)
- [[Pray for nZk] Koneko no Chi - Ponponra Dairyokou - 19 [1080p] [Multiple Subtitle]](https://uptobox.com/nl4no6nitrhe)
- [[Pray for nZk] Koneko no Chi - Ponponra Dairyokou - 20 [720p] [Multiple Subtitle]](https://uptobox.com/5sl6mj4nkfzm)
- [[Pray for nZk] Koneko no Chi - Ponponra Dairyokou - 20 [1080p] [Multiple Subtitle]](https://uptobox.com/4q62w2bxbzub)
- [[Pray for nZk] Koneko no Chi - Ponponra Dairyokou - 21 [720p] [Multiple Subtitle]](https://uptobox.com/ugjlggh19rtq)
- [[Pray for nZk] Koneko no Chi - Ponponra Dairyokou - 21 [1080p] [Multiple Subtitle]](https://uptobox.com/59do15abv9lt)
- [[Pray for nZk] Koneko no Chi - Ponponra Dairyokou - 22 [720p] [Multiple Subtitle]](https://uptobox.com/ogozfdad21en)
- [[Pray for nZk] Koneko no Chi - Ponponra Dairyokou - 22 [1080p] [Multiple Subtitle]](https://uptobox.com/4xy7fmn2dhew)
- [[Pray for nZk] Koneko no Chi - Ponponra Dairyokou - 23 [720p] [Multiple Subtitle]](https://uptobox.com/inle2453vi9z)
- [[Pray for nZk] Koneko no Chi - Ponponra Dairyokou - 23 [1080p] [Multiple Subtitle]](https://uptobox.com/d71xi2m2ok4q)
- [[Pray for nZk] Koneko no Chi - Ponponra Dairyokou - 24 [720p] [Multiple Subtitle]](https://uptobox.com/pr89ykx0evoh)
- [[Pray for nZk] Koneko no Chi - Ponponra Dairyokou - 24 [1080p] [Multiple Subtitle]](https://uptobox.com/vxutzffzesyw)
- [[Pray for nZk] Koneko no Chi - Ponponra Dairyokou - 25 [720p] [Multiple Subtitle]](https://uptobox.com/2h32zp1u3ggx)
- [[Pray for nZk] Koneko no Chi - Ponponra Dairyokou - 25 [1080p] [Multiple Subtitle]](https://uptobox.com/yufu8h0wan8m)
