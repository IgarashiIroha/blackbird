# Souten no Ken: Regenesis

![Souten no Ken: Regenesis](https://cdn.myanimelist.net/images/anime/1289/90752l.jpg)

* Japanese:  蒼天の拳 REGENESIS

## Information

 - Type: ONA
 - Episodes: 12
 - Status: Finished Airing
 - Aired: Apr 3, 2018 to Jun 19, 2018
 - Producers: Souten no Ken
 - Licensors: Ponycan USA
 - Studios: Polygon Pictures
 - Source: Manga
 - Genres: Action, Historical, Martial Arts
 - Duration: 22 min. per ep.
 - Rating: R - 17+ (violence & profanity)


## Links

- [[Refrain] Souten no Ken Re -Genesis - 01 VOSTFR [720p]](https://uptobox.com/sz6f3wmgzuku)
- [[Refrain] Souten no Ken Re -Genesis - 01 VOSTFR [720p]](https://uptobox.com/bfbl5ku2scbh)
- [[Refrain] Souten no Ken Re -Genesis - 01 VOSTFR [1080p]](https://uptobox.com/mfzbisyuuj6o)
- [[Refrain] Souten no Ken Re -Genesis - 01 VOSTFR [1080p]](https://uptobox.com/917rbda85bgw)
- [[Refrain] Souten no Ken Re -Genesis - 02 VOSTFR [720p]](https://uptobox.com/e5j0e86dmloa)
- [[Refrain] Souten no Ken Re -Genesis - 02 VOSTFR [720p]](https://uptobox.com/dhri3apig48l)
- [[Refrain] Souten no Ken Re -Genesis - 02 VOSTFR [1080p]](https://uptobox.com/fyjj7tkk0fyd)
- [[Refrain] Souten no Ken Re -Genesis - 02 VOSTFR [1080p]](https://uptobox.com/odzhvzyqwdg3)
- [[Refrain] Souten no Ken Re -Genesis - 03 VOSTFR [720p]](https://uptobox.com/9y28vyph1tk8)
- [[Refrain] Souten no Ken Re -Genesis - 03 VOSTFR [720p]](https://uptobox.com/nh4gz9ib3ah6)
- [[Refrain] Souten no Ken Re -Genesis - 03 VOSTFR [1080p]](https://uptobox.com/pwp8xnuxoyb0)
- [[Refrain] Souten no Ken Re -Genesis - 03 VOSTFR [1080p]](https://uptobox.com/nxe4y887ubp9)
- [[Refrain] Souten no Ken Re -Genesis - 04 VOSTFR [720p]](https://uptobox.com/gdt33kj32dni)
- [[Refrain] Souten no Ken Re -Genesis - 04 VOSTFR [720p]](https://uptobox.com/kk0veljh68dn)
- [[Refrain] Souten no Ken Re -Genesis - 04 VOSTFR [1080p]](https://uptobox.com/c5cdxw7867lh)
- [[Refrain] Souten no Ken Re -Genesis - 04 VOSTFR [1080p]](https://uptobox.com/7d7elvye1ba9)
- [[Refrain] Souten no Ken Re -Genesis - 05 VOSTFR [720p]](https://uptobox.com/mg1a9rdcfitu)
- [[Refrain] Souten no Ken Re -Genesis - 05 VOSTFR [720p]](https://uptobox.com/u8tvqt4vqope)
- [[Refrain] Souten no Ken Re -Genesis - 05 VOSTFR [1080p]](https://uptobox.com/1oajkl84rnag)
- [[Refrain] Souten no Ken Re -Genesis - 05 VOSTFR [1080p]](https://uptobox.com/gfd27fmr1vng)
- [[Refrain] Souten no Ken Re -Genesis - 06 VOSTFR [720p]](https://uptobox.com/60jm4ppf74h8)
- [[Refrain] Souten no Ken Re -Genesis - 06 VOSTFR [720p]](https://uptobox.com/kxyxke7dke4m)
- [[Refrain] Souten no Ken Re -Genesis - 06 VOSTFR [1080p]](https://uptobox.com/jn0fbg7r203e)
- [[Refrain] Souten no Ken Re -Genesis - 06 VOSTFR [1080p]](https://uptobox.com/i20pazt68pk8)
- [[Refrain] Souten no Ken Re -Genesis - 07 VOSTFR [720p]](https://uptobox.com/lwdkaliit3f5)
- [[Refrain] Souten no Ken Re -Genesis - 07 VOSTFR [720p]](https://uptobox.com/83pmul45c07n)
- [[Refrain] Souten no Ken Re -Genesis - 07 VOSTFR [1080p]](https://uptobox.com/4qk8evi84num)
- [[Refrain] Souten no Ken Re -Genesis - 07 VOSTFR [1080p]](https://uptobox.com/x7wxjsc9vg7k)
- [[Refrain] Souten no Ken Re -Genesis - 08 VOSTFR [720p]](https://uptobox.com/vocoubazf798)
- [[Refrain] Souten no Ken Re -Genesis - 08 VOSTFR [720p]](https://uptobox.com/iqrn3l4tb0pf)
- [[Refrain] Souten no Ken Re -Genesis - 08 VOSTFR [1080p]](https://uptobox.com/06q15p34oxg4)
- [[Refrain] Souten no Ken Re -Genesis - 08 VOSTFR [1080p]](https://uptobox.com/gfa7qugy97pn)
- [[Refrain] Souten no Ken Re -Genesis - 09 VOSTFR [720p]](https://uptobox.com/fwzfkklei3a3)
- [[Refrain] Souten no Ken Re -Genesis - 09 VOSTFR [720p]](https://uptobox.com/ykial2q3joiu)
- [[Refrain] Souten no Ken Re -Genesis - 09 VOSTFR [1080p]](https://uptobox.com/7959sozflrmm)
- [[Refrain] Souten no Ken Re -Genesis - 09 VOSTFR [1080p]](https://uptobox.com/8f7seysfkgd2)
- [[Refrain] Souten no Ken Re -Genesis - 10 VOSTFR [720p]](https://uptobox.com/9yn0p5dy10of)
- [[Refrain] Souten no Ken Re -Genesis - 10 VOSTFR [720p]](https://uptobox.com/3bj3pt7rxh3u)
- [[Refrain] Souten no Ken Re -Genesis - 10 VOSTFR [1080p]](https://uptobox.com/ymka7m9kjyv4)
- [[Refrain] Souten no Ken Re -Genesis - 10 VOSTFR [1080p]](https://uptobox.com/xpvfa4bkxvj8)
- [[Refrain] Souten no Ken Re -Genesis - 11 VOSTFR [720p]](https://uptobox.com/hqbb560ow30i)
- [[Refrain] Souten no Ken Re -Genesis - 11 VOSTFR [720p]](https://uptobox.com/4g7c62wowcgu)
- [[Refrain] Souten no Ken Re -Genesis - 11 VOSTFR [1080p]](https://uptobox.com/6ofmeqmbzm6r)
- [[Refrain] Souten no Ken Re -Genesis - 11 VOSTFR [1080p]](https://uptobox.com/ugj81qpplwrt)
- [[Refrain] Souten no Ken Re -Genesis - 12 VOSTFR [720p]](https://uptobox.com/48mq95pu2wqd)
- [[Refrain] Souten no Ken Re -Genesis - 12 VOSTFR [720p]](https://uptobox.com/k4f9tz45bmda)
- [[Refrain] Souten no Ken Re -Genesis - 12 VOSTFR [1080p]](https://uptobox.com/s60993vtuvdh)
- [[Refrain] Souten no Ken Re -Genesis - 12 VOSTFR [1080p]](https://uptobox.com/eb40gf3m7v4p)
