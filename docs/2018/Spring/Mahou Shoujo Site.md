# Mahou Shoujo Site

![Mahou Shoujo Site](https://cdn.myanimelist.net/images/anime/1456/90843l.jpg)

* Japanese:  魔法少女サイト

## Information

 - Type: TV
 - Episodes: 12
 - Status: Finished Airing
 - Aired: Apr 7, 2018 to Jun 23, 2018
 - Premiered: Spring 2018
 - Broadcast: Saturdays at 01:55 (JST)
 - Producers: None found, add some
 - Licensors: None found, add some
 - Studios: production doA
 - Source: Manga
 - Genres: Drama, Horror, Psychological, Supernatural
 - Duration: 24 min. per ep.
 - Rating: R - 17+ (violence & profanity)


## Links

- [[Refrain] Mahou Shoujo Site - 01 VOSTFR [720p]](https://uptobox.com/x6dedqw3x2ck)
- [[Refrain] Mahou Shoujo Site - 01 VOSTFR [1080p]](https://uptobox.com/zgsd4j16inb1)
- [[Refrain] Mahou Shoujo Site - 02 VOSTFR [720p]](https://uptobox.com/ygfzdmh0o9rk)
- [[Refrain] Mahou Shoujo Site - 02 VOSTFR [1080p]](https://uptobox.com/qtuwzf7g6ng0)
- [[Refrain] Mahou Shoujo Site - 03 VOSTFR [720p]](https://uptobox.com/38q9izyh5rur)
- [[Refrain] Mahou Shoujo Site - 03 VOSTFR [1080p]](https://uptobox.com/8rpnv683ujlu)
- [[Refrain] Mahou Shoujo Site - 04 VOSTFR [720p]](https://uptobox.com/qooksw1m7oua)
- [[Refrain] Mahou Shoujo Site - 04 VOSTFR [1080p]](https://uptobox.com/3y6al8xxpg2c)
- [[Refrain] Mahou Shoujo Site - 05 VOSTFR [720p]](https://uptobox.com/kmv1lmhlnhty)
- [[Refrain] Mahou Shoujo Site - 05 VOSTFR [1080p]](https://uptobox.com/jz2rop911nar)
- [[Refrain] Mahou Shoujo Site - 06 VOSTFR [720p]](https://uptobox.com/3eqsqdeuofo8)
- [[Refrain] Mahou Shoujo Site - 06 VOSTFR [1080p]](https://uptobox.com/864aoztqr1re)
- [[Refrain] Mahou Shoujo Site - 07 VOSTFR [720p]](https://uptobox.com/q0qe2nq5vre3)
- [[Refrain] Mahou Shoujo Site - 07 VOSTFR [1080p]](https://uptobox.com/y8li9uvco3bv)
- [[Refrain] Mahou Shoujo Site - 08 VOSTFR [720p]](https://uptobox.com/fq50hzf94l8t)
- [[Refrain] Mahou Shoujo Site - 08 VOSTFR [1080p]](https://uptobox.com/2o7fyu8ci7fw)
- [[Refrain] Mahou Shoujo Site - 09 VOSTFR [720p]](https://uptobox.com/4ysgbmjrfrnj)
- [[Refrain] Mahou Shoujo Site - 09 VOSTFR [1080p]](https://uptobox.com/ucrm83q6g7wo)
- [[Refrain] Mahou Shoujo Site - 10 VOSTFR [720p]](https://uptobox.com/j90x8u3rgdk5)
- [[Refrain] Mahou Shoujo Site - 10 VOSTFR [1080p]](https://uptobox.com/un6gfgwcxest)
- [[Refrain] Mahou Shoujo Site - 11 VOSTFR [720p]](https://uptobox.com/qrekldx4hrz0)
- [[Refrain] Mahou Shoujo Site - 11 VOSTFR [1080p]](https://uptobox.com/6c34zymtj5bt)
- [[Refrain] Mahou Shoujo Site - 12 VOSTFR [720p]](https://uptobox.com/51ftxywybtqu)
- [[Refrain] Mahou Shoujo Site - 12 VOSTFR [1080p]](https://uptobox.com/w459ato2h3ia)
