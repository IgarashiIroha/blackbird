# Steins;Gate 0

![Steins;Gate 0](https://cdn.myanimelist.net/images/anime/1375/93521l.jpg)

* Japanese:  シュタインズ・ゲート ゼロ

## Information

 - Type: TV
 - Episodes: 23
 - Status: Finished Airing
 - Aired: Apr 12, 2018 to Sep 27, 2018
 - Premiered: Spring 2018
 - Broadcast: Thursdays at 01:35 (JST)
 - Producers: Frontier Works, Media Factory, Kadokawa Shoten, Movic
 - Licensors: Funimation
 - Studios: White Fox
 - Source: Visual novel
 - Genres: Sci-Fi, Thriller
 - Duration: 23 min. per ep.
 - Rating: PG-13 - Teens 13 or older


## Links

- [[Refrain] Steins Gate 0 - Kesshou Takei no Valentine - Bittersweet Intermedio VOSTFR [720p]](https://uptobox.com/84ebztroft9b)
- [[Refrain] Steins Gate 0 - Kesshou Takei no Valentine - Bittersweet Intermedio VOSTFR [1080p]](https://uptobox.com/njsn8ehwv2x2)
- [[Refrain] Steins;Gate 0 - 01 VOSTFR [720p]](https://uptobox.com/747eazj8k7k6)
- [[Refrain] Steins;Gate 0 - 01 VOSTFR [1080p]](https://uptobox.com/0jxgx81jfylo)
- [[Refrain] Steins;Gate 0 - 02 VOSTFR [720p]](https://uptobox.com/rt4ujsv60cjm)
- [[Refrain] Steins;Gate 0 - 02 VOSTFR [1080p]](https://uptobox.com/verifus2ynb2)
- [[Refrain] Steins;Gate 0 - 03 VOSTFR [720p]](https://uptobox.com/u40bsq9dngi2)
- [[Refrain] Steins;Gate 0 - 03 VOSTFR [1080p]](https://uptobox.com/wiob16w55tby)
- [[Refrain] Steins;Gate 0 - 04 VOSTFR [720p]](https://uptobox.com/j7y5ct6535kv)
- [[Refrain] Steins;Gate 0 - 04 VOSTFR [1080p]](https://uptobox.com/nrp6mva296q7)
- [[Refrain] Steins;Gate 0 - 05 VOSTFR [720p]](https://uptobox.com/hgy569h1enjl)
- [[Refrain] Steins;Gate 0 - 05 VOSTFR [1080p]](https://uptobox.com/h7hmof0vfs20)
- [[Refrain] Steins;Gate 0 - 06 VOSTFR [720p]](https://uptobox.com/vqct5pa7hwtm)
- [[Refrain] Steins;Gate 0 - 06 VOSTFR [1080p]](https://uptobox.com/gqc7vnccpby6)
- [[Refrain] Steins;Gate 0 - 07 VOSTFR [720p]](https://uptobox.com/kmuo1of6oo7l)
- [[Refrain] Steins;Gate 0 - 07 VOSTFR [1080p]](https://uptobox.com/ihk1oglu0yxq)
- [[Refrain] Steins;Gate 0 - 08 VOSTFR [720p]](https://uptobox.com/2ghc1jix125l)
- [[Refrain] Steins;Gate 0 - 08 VOSTFR [1080p]](https://uptobox.com/o0jzheqvvp9w)
- [[Refrain] Steins;Gate 0 - 09 VOSTFR [720p]](https://uptobox.com/6ol8rpakukds)
- [[Refrain] Steins;Gate 0 - 09 VOSTFR [1080p]](https://uptobox.com/v24bvsnkkor7)
- [[Refrain] Steins;Gate 0 - 10 VOSTFR [720p]](https://uptobox.com/iv9v3zqux4wq)
- [[Refrain] Steins;Gate 0 - 10 VOSTFR [1080p]](https://uptobox.com/dyjuansb3h0j)
- [[Refrain] Steins;Gate 0 - 11 VOSTFR [720p]](https://uptobox.com/fh6g7ld352ma)
- [[Refrain] Steins;Gate 0 - 11 VOSTFR [1080p]](https://uptobox.com/gsp8yb62odz1)
- [[Refrain] Steins;Gate 0 - 12 VOSTFR [720p]](https://uptobox.com/yfgp4bfoab5l)
- [[Refrain] Steins;Gate 0 - 12 VOSTFR [1080p]](https://uptobox.com/770k0l2yk7me)
- [[Refrain] Steins;Gate 0 - 13 VOSTFR [720p]](https://uptobox.com/onk0dxr4w0xq)
- [[Refrain] Steins;Gate 0 - 13 VOSTFR [1080p]](https://uptobox.com/fyiehfldy35h)
- [[Refrain] Steins;Gate 0 - 14 VOSTFR [720p]](https://uptobox.com/9u0mwi00gqu9)
- [[Refrain] Steins;Gate 0 - 14 VOSTFR [1080p]](https://uptobox.com/nl8dys1tyzh1)
- [[Refrain] Steins;Gate 0 - 15 VOSTFR [720p]](https://uptobox.com/y9yknjyrntjz)
- [[Refrain] Steins;Gate 0 - 15 VOSTFR [1080p]](https://uptobox.com/3l7uczc73ho5)
- [[Refrain] Steins;Gate 0 - 16 VOSTFR [720p]](https://uptobox.com/gv7bf91kjxii)
- [[Refrain] Steins;Gate 0 - 16 VOSTFR [1080p]](https://uptobox.com/43gfqjro8xfo)
- [[Refrain] Steins;Gate 0 - 17 VOSTFR [720p]](https://uptobox.com/nvx6tvqitu8m)
- [[Refrain] Steins;Gate 0 - 17 VOSTFR [1080p]](https://uptobox.com/jqp30yky6qlh)
- [[Refrain] Steins;Gate 0 - 18 VOSTFR [720p]](https://uptobox.com/vl0ojb9um58w)
- [[Refrain] Steins;Gate 0 - 18 VOSTFR [1080p]](https://uptobox.com/g2212a8m9aix)
- [[Refrain] Steins;Gate 0 - 19 VOSTFR [720p]](https://uptobox.com/8lvr93sos0bz)
- [[Refrain] Steins;Gate 0 - 19 VOSTFR [1080p]](https://uptobox.com/7flm57y0gsc9)
- [[Refrain] Steins;Gate 0 - 20 VOSTFR [720p]](https://uptobox.com/b5gl88bziidr)
- [[Refrain] Steins;Gate 0 - 20 VOSTFR [1080p]](https://uptobox.com/0qk926lfsty1)
- [[Refrain] Steins;Gate 0 - 21 VOSTFR [720p]](https://uptobox.com/701ufmvarfku)
- [[Refrain] Steins;Gate 0 - 21 VOSTFR [1080p]](https://uptobox.com/95u88sxn2s8g)
- [[Refrain] Steins;Gate 0 - 22 VOSTFR [720p]](https://uptobox.com/sgc4ybtde16w)
- [[Refrain] Steins;Gate 0 - 22 VOSTFR [1080p]](https://uptobox.com/yq1eow2ln7vr)
- [[Refrain] Steins;Gate 0 - 23 VOSTFR [720p]](https://uptobox.com/9jamv97wonc1)
- [[Refrain] Steins;Gate 0 - 23 VOSTFR [1080p]](https://uptobox.com/1hhj42jnxxkr)
