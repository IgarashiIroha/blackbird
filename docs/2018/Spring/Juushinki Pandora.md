# Juushinki Pandora

![Juushinki Pandora](https://cdn.myanimelist.net/images/anime/1897/95230l.jpg)

* Japanese:  重神機パンドーラ

## Information

 - Type: TV
 - Episodes: 26
 - Status: Finished Airing
 - Aired: Mar 29, 2018 to Sep 20, 2018
 - Premiered: Spring 2018
 - Broadcast: Wednesdays at 23:30 (JST)
 - Producers: None found, add some
 - Licensors: None found, add some
 - Studios: Satelight
 - Source: Original
 - Genres: Sci-Fi, Mecha
 - Duration: 23 min. per ep.
 - Rating: PG-13 - Teens 13 or older


## Links

- [[Refrain] Juushinki Pandora - 01 VF VOSTFR [720p]](https://uptobox.com/ie664g757vie)
- [[Refrain] Juushinki Pandora - 01 VF VOSTFR [1080p]](https://uptobox.com/p6j9vtv6si30)
- [[Refrain] Juushinki Pandora - 02 VF VOSTFR [720p]](https://uptobox.com/8g4rp803qo9j)
- [[Refrain] Juushinki Pandora - 02 VF VOSTFR [1080p]](https://uptobox.com/1op7k5a7izk4)
- [[Refrain] Juushinki Pandora - 03 VF VOSTFR [720p]](https://uptobox.com/1n4v0b6ymqqh)
- [[Refrain] Juushinki Pandora - 03 VF VOSTFR [1080p]](https://uptobox.com/hmsza8bylon0)
- [[Refrain] Juushinki Pandora - 04 VF VOSTFR [720p]](https://uptobox.com/6t4vlio25n6g)
- [[Refrain] Juushinki Pandora - 04 VF VOSTFR [1080p]](https://uptobox.com/2ern881obyk3)
- [[Refrain] Juushinki Pandora - 05 VF VOSTFR [720p]](https://uptobox.com/xfddba5dqs38)
- [[Refrain] Juushinki Pandora - 05 VF VOSTFR [1080p]](https://uptobox.com/qjiceig2fnwe)
- [[Refrain] Juushinki Pandora - 06 VF VOSTFR [720p]](https://uptobox.com/pq4k37qtggby)
- [[Refrain] Juushinki Pandora - 06 VF VOSTFR [1080p]](https://uptobox.com/hvaafva7y1m8)
- [[Refrain] Juushinki Pandora - 07 VF VOSTFR [720p]](https://uptobox.com/id6twe6umx2g)
- [[Refrain] Juushinki Pandora - 07 VF VOSTFR [1080p]](https://uptobox.com/fbx9je5h0xeq)
- [[Refrain] Juushinki Pandora - 08 VF VOSTFR [720p]](https://uptobox.com/1pz0b3e2h0ut)
- [[Refrain] Juushinki Pandora - 08 VF VOSTFR [1080p]](https://uptobox.com/07hu6p3503a5)
- [[Refrain] Juushinki Pandora - 09 VF VOSTFR [720p]](https://uptobox.com/je8zheau21i4)
- [[Refrain] Juushinki Pandora - 09 VF VOSTFR [1080p]](https://uptobox.com/hot4fzd2qkmn)
- [[Refrain] Juushinki Pandora - 10 VF VOSTFR [720p]](https://uptobox.com/u9gma02rlckx)
- [[Refrain] Juushinki Pandora - 10 VF VOSTFR [1080p]](https://uptobox.com/7mvvel1pc5by)
- [[Refrain] Juushinki Pandora - 11 VF VOSTFR [720p]](https://uptobox.com/q19ed7l1l6pq)
- [[Refrain] Juushinki Pandora - 11 VF VOSTFR [1080p]](https://uptobox.com/mu992c02jsox)
- [[Refrain] Juushinki Pandora - 12 VF VOSTFR [720p]](https://uptobox.com/6h6tlvkla86e)
- [[Refrain] Juushinki Pandora - 12 VF VOSTFR [1080p]](https://uptobox.com/nqn8jslopegt)
- [[Refrain] Juushinki Pandora - 13 VF VOSTFR [720p]](https://uptobox.com/seqp55moh60a)
- [[Refrain] Juushinki Pandora - 13 VF VOSTFR [1080p]](https://uptobox.com/jclqzts9osxo)
- [[Refrain] Juushinki Pandora - 14 VF VOSTFR [720p]](https://uptobox.com/lsg5uqds62a4)
- [[Refrain] Juushinki Pandora - 14 VF VOSTFR [1080p]](https://uptobox.com/345er8l8grek)
- [[Refrain] Juushinki Pandora - 15 VF VOSTFR [720p]](https://uptobox.com/way5i85kjf8e)
- [[Refrain] Juushinki Pandora - 15 VF VOSTFR [1080p]](https://uptobox.com/ql98be75lqp5)
- [[Refrain] Juushinki Pandora - 16 VF VOSTFR [720p]](https://uptobox.com/c2d4pxbnikny)
- [[Refrain] Juushinki Pandora - 16 VF VOSTFR [1080p]](https://uptobox.com/26frr1dfrmaq)
- [[Refrain] Juushinki Pandora - 17 VF VOSTFR [720p]](https://uptobox.com/onaublotmrsj)
- [[Refrain] Juushinki Pandora - 17 VF VOSTFR [1080p]](https://uptobox.com/ue70orsxexz6)
- [[Refrain] Juushinki Pandora - 18 VF VOSTFR [720p]](https://uptobox.com/hwdxo5hc0kiw)
- [[Refrain] Juushinki Pandora - 18 VF VOSTFR [1080p]](https://uptobox.com/3gn93pw2b2ql)
- [[Refrain] Juushinki Pandora - 19 VF VOSTFR [720p]](https://uptobox.com/dcevad2qtmky)
- [[Refrain] Juushinki Pandora - 19 VF VOSTFR [1080p]](https://uptobox.com/fct5zgjzyfnx)
- [[Refrain] Juushinki Pandora - 20 VF VOSTFR [720p]](https://uptobox.com/dkceg8646bpq)
- [[Refrain] Juushinki Pandora - 20 VF VOSTFR [1080p]](https://uptobox.com/wyusv915d82s)
- [[Refrain] Juushinki Pandora - 21 VF VOSTFR [720p]](https://uptobox.com/qvufhv3w6j2s)
- [[Refrain] Juushinki Pandora - 21 VF VOSTFR [1080p]](https://uptobox.com/q6ichrtfu7yr)
- [[Refrain] Juushinki Pandora - 22 VF VOSTFR [720p]](https://uptobox.com/c3f0nwpe6afw)
- [[Refrain] Juushinki Pandora - 22 VF VOSTFR [1080p]](https://uptobox.com/6y8pkbcar8uk)
- [[Refrain] Juushinki Pandora - 23 VF VOSTFR [720p]](https://uptobox.com/5vjcjtjabmml)
- [[Refrain] Juushinki Pandora - 23 VF VOSTFR [1080p]](https://uptobox.com/ple5utdsajyj)
- [[Refrain] Juushinki Pandora - 24 VF VOSTFR [720p]](https://uptobox.com/dkj386mwmxpm)
- [[Refrain] Juushinki Pandora - 24 VF VOSTFR [1080p]](https://uptobox.com/bfkfiglk9w9o)
- [[Refrain] Juushinki Pandora - 25 VF VOSTFR [720p]](https://uptobox.com/et2npijqi8ay)
- [[Refrain] Juushinki Pandora - 25 VF VOSTFR [1080p]](https://uptobox.com/2am64budvhex)
- [[Refrain] Juushinki Pandora - 26 VF VOSTFR [720p]](https://uptobox.com/d7ck7esgnjjf)
- [[Refrain] Juushinki Pandora - 26 VF VOSTFR [1080p]](https://uptobox.com/hnudgzt4zjmh)
