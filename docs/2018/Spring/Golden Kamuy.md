# Golden Kamuy

![Golden Kamuy](https://cdn.myanimelist.net/images/anime/1145/90880l.jpg)

* Japanese:  ゴールデンカムイ

## Information

 - Type: TV
 - Episodes: 12
 - Status: Finished Airing
 - Aired: Apr 9, 2018 to Jun 25, 2018
 - Premiered: Spring 2018
 - Broadcast: Mondays at 23:00 (JST)
 - Producers: Magic Capsule, NBCUniversal Entertainment Japan, Tokyo MX, Good Smile Company, Sammy, Shueisha, East Japan Marketing & Communications, Twin Engine
 - Licensors: Funimation
 - Studios: Geno Studio
 - Source: Manga
 - Genres: Action, Adventure, Historical, Seinen
 - Duration: 23 min. per ep.
 - Rating: R - 17+ (violence & profanity)


## Links

- [[Refrain] Golden Kamuy - 01 VOSTFR [720p]](https://uptobox.com/mj36p4gql466)
- [[Refrain] Golden Kamuy - 01 VOSTFR [1080p]](https://uptobox.com/kj3aihhpmnsy)
- [[Refrain] Golden Kamuy - 02 VOSTFR [720p]](https://uptobox.com/2cnox18ghveo)
- [[Refrain] Golden Kamuy - 02 VOSTFR [1080p]](https://uptobox.com/sesxgg8ivlhc)
- [[Refrain] Golden Kamuy - 03 VOSTFR [720p]](https://uptobox.com/akwyovbtsdop)
- [[Refrain] Golden Kamuy - 03 VOSTFR [1080p]](https://uptobox.com/jvzyhief7hnc)
- [[Refrain] Golden Kamuy - 04 VOSTFR [720p]](https://uptobox.com/ttj4akflxxk7)
- [[Refrain] Golden Kamuy - 04 VOSTFR [1080p]](https://uptobox.com/blde6ijp9mck)
- [[Refrain] Golden Kamuy - 05 VOSTFR [720p]](https://uptobox.com/db6sy9mnf2rj)
- [[Refrain] Golden Kamuy - 05 VOSTFR [1080p]](https://uptobox.com/dd9vns4djd02)
- [[Refrain] Golden Kamuy - 06 VOSTFR [720p]](https://uptobox.com/a3iqcxzq3fap)
- [[Refrain] Golden Kamuy - 06 VOSTFR [1080p]](https://uptobox.com/cjicj44uwubo)
- [[Refrain] Golden Kamuy - 07 VOSTFR [720p]](https://uptobox.com/462mnke0g6me)
- [[Refrain] Golden Kamuy - 07 VOSTFR [1080p]](https://uptobox.com/y7rujs2ptwc0)
- [[Refrain] Golden Kamuy - 08 VOSTFR [720p]](https://uptobox.com/0b7pfzt1rsbs)
- [[Refrain] Golden Kamuy - 08 VOSTFR [1080p]](https://uptobox.com/djg0murn7osg)
- [[Refrain] Golden Kamuy - 09 VOSTFR [720p]](https://uptobox.com/yj0s67s35446)
- [[Refrain] Golden Kamuy - 09 VOSTFR [1080p]](https://uptobox.com/by41k5fr2824)
- [[Refrain] Golden Kamuy - 10 VOSTFR [720p]](https://uptobox.com/oezbmz93saxr)
- [[Refrain] Golden Kamuy - 10 VOSTFR [1080p]](https://uptobox.com/guuwgak8ru9h)
- [[Refrain] Golden Kamuy - 11 VOSTFR [720p]](https://uptobox.com/t8imlfejpg6m)
- [[Refrain] Golden Kamuy - 11 VOSTFR [1080p]](https://uptobox.com/6h1gre0klrg0)
- [[Refrain] Golden Kamuy - 12 VOSTFR [720p]](https://uptobox.com/vk0kb2qd950p)
- [[Refrain] Golden Kamuy - 12 VOSTFR [1080p]](https://uptobox.com/v03v5q0qczog)
