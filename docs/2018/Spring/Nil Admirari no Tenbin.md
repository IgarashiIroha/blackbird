# Nil Admirari no Tenbin

![Nil Admirari no Tenbin](https://cdn.myanimelist.net/images/anime/1669/90604l.jpg)

* Japanese:  ニル・アドミラリの天秤

## Information

 - Type: TV
 - Episodes: 12
 - Status: Finished Airing
 - Aired: Apr 8, 2018 to Jun 24, 2018
 - Premiered: Spring 2018
 - Broadcast: Sundays at 22:00 (JST)
 - Producers: Pony Canyon
 - Licensors: None found, add some
 - Studios: Zero-G
 - Source: Visual novel
 - Genres: Fantasy, Historical, Josei, Romance
 - Duration: 23 min. per ep.
 - Rating: PG-13 - Teens 13 or older


## Links

- [[Refrain] Nil Admirari no Tenbin - Teito Genwaku Kitan - 01 VOSTFR [720p]](https://uptobox.com/xsou0cpohbu5)
- [[Refrain] Nil Admirari no Tenbin - Teito Genwaku Kitan - 01 VOSTFR [1080p]](https://uptobox.com/rp7zke1na032)
- [[Refrain] Nil Admirari no Tenbin - Teito Genwaku Kitan - 02 VOSTFR [720p]](https://uptobox.com/qe3u8o8a7a6k)
- [[Refrain] Nil Admirari no Tenbin - Teito Genwaku Kitan - 02 VOSTFR [1080p]](https://uptobox.com/dvbibzv42jby)
- [[Refrain] Nil Admirari no Tenbin - Teito Genwaku Kitan - 03 VOSTFR [720p]](https://uptobox.com/ndf6vxfbiv3l)
- [[Refrain] Nil Admirari no Tenbin - Teito Genwaku Kitan - 03 VOSTFR [1080p]](https://uptobox.com/nvflt7dl7e0p)
- [[Refrain] Nil Admirari no Tenbin - Teito Genwaku Kitan - 04 VOSTFR [720p]](https://uptobox.com/308rcmpggg3h)
- [[Refrain] Nil Admirari no Tenbin - Teito Genwaku Kitan - 04 VOSTFR [1080p]](https://uptobox.com/2twq2xop67aq)
- [[Refrain] Nil Admirari no Tenbin - Teito Genwaku Kitan - 05 VOSTFR [720p]](https://uptobox.com/528qj75y7r9b)
- [[Refrain] Nil Admirari no Tenbin - Teito Genwaku Kitan - 05 VOSTFR [1080p]](https://uptobox.com/cmz724q9ynef)
- [[Refrain] Nil Admirari no Tenbin - Teito Genwaku Kitan - 06 VOSTFR [720p]](https://uptobox.com/ad734laauo4y)
- [[Refrain] Nil Admirari no Tenbin - Teito Genwaku Kitan - 06 VOSTFR [1080p]](https://uptobox.com/gjsskivaihge)
- [[Refrain] Nil Admirari no Tenbin - Teito Genwaku Kitan - 07 VOSTFR [720p]](https://uptobox.com/ugys976a8fb9)
- [[Refrain] Nil Admirari no Tenbin - Teito Genwaku Kitan - 07 VOSTFR [1080p]](https://uptobox.com/0bc11vjeyos0)
- [[Refrain] Nil Admirari no Tenbin - Teito Genwaku Kitan - 08 VOSTFR [720p]](https://uptobox.com/0tl8rjqniptu)
- [[Refrain] Nil Admirari no Tenbin - Teito Genwaku Kitan - 08 VOSTFR [1080p]](https://uptobox.com/447be4zehwt0)
- [[Refrain] Nil Admirari no Tenbin - Teito Genwaku Kitan - 09 VOSTFR [720p]](https://uptobox.com/xi554u7fsc0g)
- [[Refrain] Nil Admirari no Tenbin - Teito Genwaku Kitan - 09 VOSTFR [1080p]](https://uptobox.com/n1ywmbddxw3j)
- [[Refrain] Nil Admirari no Tenbin - Teito Genwaku Kitan - 10 VOSTFR [720p]](https://uptobox.com/4z16paigp62p)
- [[Refrain] Nil Admirari no Tenbin - Teito Genwaku Kitan - 10 VOSTFR [1080p]](https://uptobox.com/k4r0nacbbmyd)
- [[Refrain] Nil Admirari no Tenbin - Teito Genwaku Kitan - 11 VOSTFR [720p]](https://uptobox.com/d9m1q5iud2j4)
- [[Refrain] Nil Admirari no Tenbin - Teito Genwaku Kitan - 11 VOSTFR [1080p]](https://uptobox.com/p96xm64ry8cw)
- [[Refrain] Nil Admirari no Tenbin - Teito Genwaku Kitan - 12 VOSTFR [720p]](https://uptobox.com/ej7l5i2vc2wm)
- [[Refrain] Nil Admirari no Tenbin - Teito Genwaku Kitan - 12 VOSTFR [1080p]](https://uptobox.com/blwummyy8pp9)
