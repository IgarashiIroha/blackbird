# Saredo Tsumibito wa Ryuu to Odoru

![Saredo Tsumibito wa Ryuu to Odoru](https://cdn.myanimelist.net/images/anime/1313/95061l.jpg)

* Japanese:  されど罪人は竜と踊る

## Information

 - Type: TV
 - Episodes: 12
 - Status: Finished Airing
 - Aired: Apr 6, 2018 to Jun 22, 2018
 - Premiered: Spring 2018
 - Broadcast: Fridays at 01:58 (JST)
 - Producers: TBS, NBCUniversal Entertainment Japan
 - Licensors: Funimation
 - Studios: Seven Arcs Pictures
 - Source: Light novel
 - Genres: Action, Drama, Fantasy, Sci-Fi
 - Duration: 23 min. per ep.
 - Rating: R - 17+ (violence & profanity)


## Links

- [[Refrain] Saredo Tsumibito wa Ryuu to Odoru - 01 VOSTFR {ADN} [720p]](https://uptobox.com/5qtvtscconna)
- [[Refrain] Saredo Tsumibito wa Ryuu to Odoru - 01 VOSTFR {ADN} [1080p]](https://uptobox.com/j4gu1ncdhpeb)
- [[Refrain] Saredo Tsumibito wa Ryuu to Odoru - 01 VOSTFR {CR} [720p]](https://uptobox.com/w2ebjyb6dqf6)
- [[Refrain] Saredo Tsumibito wa Ryuu to Odoru - 01 VOSTFR {CR} [1080p]](https://uptobox.com/x3c8dklj7drw)
- [[Refrain] Saredo Tsumibito wa Ryuu to Odoru - 02 VOSTFR {ADN} [720p]](https://uptobox.com/xb9ypcs2acux)
- [[Refrain] Saredo Tsumibito wa Ryuu to Odoru - 02 VOSTFR {ADN} [1080p]](https://uptobox.com/uac1dtkb5ws6)
- [[Refrain] Saredo Tsumibito wa Ryuu to Odoru - 02 VOSTFR {CR} [720p]](https://uptobox.com/xjrg80jy6h0i)
- [[Refrain] Saredo Tsumibito wa Ryuu to Odoru - 02 VOSTFR {CR} [1080p]](https://uptobox.com/dvbmsagxp3hw)
- [[Refrain] Saredo Tsumibito wa Ryuu to Odoru - 03 VOSTFR {ADN} [720p]](https://uptobox.com/uikcx0cpgmc8)
- [[Refrain] Saredo Tsumibito wa Ryuu to Odoru - 03 VOSTFR {ADN} [1080p]](https://uptobox.com/5tu2ov5ccc13)
- [[Refrain] Saredo Tsumibito wa Ryuu to Odoru - 03 VOSTFR {CR} [720p]](https://uptobox.com/7zzc3d0iz2gw)
- [[Refrain] Saredo Tsumibito wa Ryuu to Odoru - 03 VOSTFR {CR} [1080p]](https://uptobox.com/sh0e76ghn81i)
- [[Refrain] Saredo Tsumibito wa Ryuu to Odoru - 04 VOSTFR {ADN} [720p]](https://uptobox.com/2zqv33yrycka)
- [[Refrain] Saredo Tsumibito wa Ryuu to Odoru - 04 VOSTFR {ADN} [1080p]](https://uptobox.com/fsng7zjwpql8)
- [[Refrain] Saredo Tsumibito wa Ryuu to Odoru - 04 VOSTFR {CR} [720p]](https://uptobox.com/ovjrbd0hpqg7)
- [[Refrain] Saredo Tsumibito wa Ryuu to Odoru - 04 VOSTFR {CR} [1080p]](https://uptobox.com/dxull5o4n5fw)
- [[Refrain] Saredo Tsumibito wa Ryuu to Odoru - 05 VOSTFR {ADN} [720p]](https://uptobox.com/t259m4aqdtfs)
- [[Refrain] Saredo Tsumibito wa Ryuu to Odoru - 05 VOSTFR {ADN} [1080p]](https://uptobox.com/746bcbw0pmol)
- [[Refrain] Saredo Tsumibito wa Ryuu to Odoru - 05 VOSTFR {CR} [720p]](https://uptobox.com/fs8jptsd9y7q)
- [[Refrain] Saredo Tsumibito wa Ryuu to Odoru - 05 VOSTFR {CR} [1080p]](https://uptobox.com/0r8gdc8sgvpd)
- [[Refrain] Saredo Tsumibito wa Ryuu to Odoru - 06 VOSTFR {ADN} [720p]](https://uptobox.com/r4lyq9z2dkix)
- [[Refrain] Saredo Tsumibito wa Ryuu to Odoru - 06 VOSTFR {ADN} [1080p]](https://uptobox.com/fl51knpwim4x)
- [[Refrain] Saredo Tsumibito wa Ryuu to Odoru - 06 VOSTFR {CR} [720p]](https://uptobox.com/md0jcz11l3lw)
- [[Refrain] Saredo Tsumibito wa Ryuu to Odoru - 06 VOSTFR {CR} [1080p]](https://uptobox.com/hnejt9dr8mo0)
- [[Refrain] Saredo Tsumibito wa Ryuu to Odoru - 07 VOSTFR {ADN} [720p]](https://uptobox.com/qxp76ji6gn95)
- [[Refrain] Saredo Tsumibito wa Ryuu to Odoru - 07 VOSTFR {ADN} [1080p]](https://uptobox.com/10oikg3cu5cs)
- [[Refrain] Saredo Tsumibito wa Ryuu to Odoru - 07 VOSTFR {CR} [720p]](https://uptobox.com/21amlsc58tw1)
- [[Refrain] Saredo Tsumibito wa Ryuu to Odoru - 07 VOSTFR {CR} [1080p]](https://uptobox.com/mfmte2hs81go)
- [[Refrain] Saredo Tsumibito wa Ryuu to Odoru - 08 VOSTFR {ADN} [720p]](https://uptobox.com/0cc9aq7pbuhn)
- [[Refrain] Saredo Tsumibito wa Ryuu to Odoru - 08 VOSTFR {ADN} [1080p]](https://uptobox.com/5yr2fhcgksjl)
- [[Refrain] Saredo Tsumibito wa Ryuu to Odoru - 08 VOSTFR {CR} [720p]](https://uptobox.com/cdmaveoa9zy1)
- [[Refrain] Saredo Tsumibito wa Ryuu to Odoru - 08 VOSTFR {CR} [1080p]](https://uptobox.com/men2kwoc6co1)
- [[Refrain] Saredo Tsumibito wa Ryuu to Odoru - 09 VOSTFR {ADN} [720p]](https://uptobox.com/zzvcl7zpygib)
- [[Refrain] Saredo Tsumibito wa Ryuu to Odoru - 09 VOSTFR {ADN} [1080p]](https://uptobox.com/p9el96lytubw)
- [[Refrain] Saredo Tsumibito wa Ryuu to Odoru - 09 VOSTFR {CR} [720p]](https://uptobox.com/eahyu8ohgn31)
- [[Refrain] Saredo Tsumibito wa Ryuu to Odoru - 09 VOSTFR {CR} [1080p]](https://uptobox.com/ure2s9oqoz0x)
- [[Refrain] Saredo Tsumibito wa Ryuu to Odoru - 10 VOSTFR {ADN} [720p]](https://uptobox.com/sx85g29ioqol)
- [[Refrain] Saredo Tsumibito wa Ryuu to Odoru - 10 VOSTFR {ADN} [1080p]](https://uptobox.com/h6vlkqxmt15s)
- [[Refrain] Saredo Tsumibito wa Ryuu to Odoru - 10 VOSTFR {CR} [720p]](https://uptobox.com/5mgvfxp0t763)
- [[Refrain] Saredo Tsumibito wa Ryuu to Odoru - 10 VOSTFR {CR} [1080p]](https://uptobox.com/z194vhazh75e)
- [[Refrain] Saredo Tsumibito wa Ryuu to Odoru - 11 VOSTFR [1080p]](https://uptobox.com/no2xwbkz93he)
- [[Refrain] Saredo Tsumibito wa Ryuu to Odoru - 11 VOSTFR {ADN} [720p]](https://uptobox.com/w7qs3g2zwivc)
- [[Refrain] Saredo Tsumibito wa Ryuu to Odoru - 11 VOSTFR {CR} [720p]](https://uptobox.com/xbbzuvs1g9c9)
- [[Refrain] Saredo Tsumibito wa Ryuu to Odoru - 11 VOSTFR {CR} [1080p]](https://uptobox.com/7mpyvtriapj6)
- [[Refrain] Saredo Tsumibito wa Ryuu to Odoru - 12 VOSTFR {ADN} [720p]](https://uptobox.com/a6e33k039ttw)
- [[Refrain] Saredo Tsumibito wa Ryuu to Odoru - 12 VOSTFR {ADN} [1080p]](https://uptobox.com/u4sf43aa62nn)
- [[Refrain] Saredo Tsumibito wa Ryuu to Odoru - 12 VOSTFR {CR} [720p]](https://uptobox.com/1x3xykxnjmbl)
- [[Refrain] Saredo Tsumibito wa Ryuu to Odoru - 12 VOSTFR {CR} [1080p]](https://uptobox.com/j838ebgiuomt)
