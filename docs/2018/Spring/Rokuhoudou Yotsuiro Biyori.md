# Rokuhoudou Yotsuiro Biyori

![Rokuhoudou Yotsuiro Biyori](https://cdn.myanimelist.net/images/anime/1015/90727l.jpg)

* Japanese:  鹿楓堂よついろ日和

## Information

 - Type: TV
 - Episodes: 12
 - Status: Finished Airing
 - Aired: Apr 11, 2018 to Jun 27, 2018
 - Premiered: Spring 2018
 - Broadcast: Wednesdays at 00:30 (JST)
 - Producers: None found, add some
 - Licensors: None found, add some
 - Studios: Zexcs
 - Source: Manga
 - Genres: Seinen, Slice of Life
 - Duration: 23 min. per ep.
 - Rating: PG-13 - Teens 13 or older


## Links

- [[Refrain] Rokuhoudou Yotsuiro Biyori - 01 VOSTFR [720p]](https://uptobox.com/s0xf9mk2qqn6)
- [[Refrain] Rokuhoudou Yotsuiro Biyori - 01 VOSTFR [1080p]](https://uptobox.com/57g85nia55qw)
- [[Refrain] Rokuhoudou Yotsuiro Biyori - 02 VOSTFR [720p]](https://uptobox.com/m0gn0zm9oic5)
- [[Refrain] Rokuhoudou Yotsuiro Biyori - 02 VOSTFR [1080p]](https://uptobox.com/lno286c1urhb)
- [[Refrain] Rokuhoudou Yotsuiro Biyori - 03 VOSTFR [720p]](https://uptobox.com/5pn1e6sw4923)
- [[Refrain] Rokuhoudou Yotsuiro Biyori - 03 VOSTFR [1080p]](https://uptobox.com/23dcq5h4u37y)
- [[Refrain] Rokuhoudou Yotsuiro Biyori - 04 VOSTFR [720p]](https://uptobox.com/k1qpep1wvbo4)
- [[Refrain] Rokuhoudou Yotsuiro Biyori - 04 VOSTFR [1080p]](https://uptobox.com/ack078q35upq)
- [[Refrain] Rokuhoudou Yotsuiro Biyori - 05 VOSTFR [720p]](https://uptobox.com/zh3ebq9tnjeq)
- [[Refrain] Rokuhoudou Yotsuiro Biyori - 05 VOSTFR [1080p]](https://uptobox.com/dtf8w3sd9pi2)
- [[Refrain] Rokuhoudou Yotsuiro Biyori - 06 VOSTFR [720p]](https://uptobox.com/xt9ox8vc53wn)
- [[Refrain] Rokuhoudou Yotsuiro Biyori - 06 VOSTFR [1080p]](https://uptobox.com/vmheb739wphx)
- [[Refrain] Rokuhoudou Yotsuiro Biyori - 07 VOSTFR [720p]](https://uptobox.com/q9prgoptdcay)
- [[Refrain] Rokuhoudou Yotsuiro Biyori - 07 VOSTFR [1080p]](https://uptobox.com/gmakxohlahab)
- [[Refrain] Rokuhoudou Yotsuiro Biyori - 08 VOSTFR [720p]](https://uptobox.com/fp9zb3pjkwq8)
- [[Refrain] Rokuhoudou Yotsuiro Biyori - 08 VOSTFR [1080p]](https://uptobox.com/j6aqkde0zh2j)
- [[Refrain] Rokuhoudou Yotsuiro Biyori - 09 VOSTFR [720p]](https://uptobox.com/o2m7sqbrvrw2)
- [[Refrain] Rokuhoudou Yotsuiro Biyori - 09 VOSTFR [1080p]](https://uptobox.com/yv5ebh4f2ds0)
- [[Refrain] Rokuhoudou Yotsuiro Biyori - 10 VOSTFR [720p]](https://uptobox.com/mwnl8mzipce6)
- [[Refrain] Rokuhoudou Yotsuiro Biyori - 10 VOSTFR [1080p]](https://uptobox.com/bc0w2pajojx1)
- [[Refrain] Rokuhoudou Yotsuiro Biyori - 11 VOSTFR [720p]](https://uptobox.com/zhwee0xwwh3u)
- [[Refrain] Rokuhoudou Yotsuiro Biyori - 11 VOSTFR [1080p]](https://uptobox.com/ellcr609p980)
- [[Refrain] Rokuhoudou Yotsuiro Biyori - 12 VOSTFR [720p]](https://uptobox.com/sb5kvtqa3a29)
- [[Refrain] Rokuhoudou Yotsuiro Biyori - 12 VOSTFR [1080p]](https://uptobox.com/kefnsls27182)
