# Pastel Life

![Pastel Life](https://cdn.myanimelist.net/images/anime/1792/92046l.jpg)

* Japanese:  ぱすてるらいふ

## Information

 - Type: TV
 - Episodes: 6
 - Status: Finished Airing
 - Aired: May 17, 2018 to Jun 21, 2018
 - Premiered: Spring 2018
 - Broadcast: Unknown
 - Producers: None found, add some 
 - Licensors: None found, add some 
 - Studios: None found, add some 
 - Source: Unknown
 - Genres: Music, Slice of Life
 - Duration: 3 min. per ep.
 - Rating: PG-13 - Teens 13 or older


## Links

