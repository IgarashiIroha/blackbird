# Washimo 6th Season

![Washimo 6th Season](https://cdn.myanimelist.net/images/anime/1487/91475l.jpg)

* Japanese:  わしも-wasimo-(第6シリーズ)

## Information

 - Type: TV
 - Episodes: Unknown
 - Status: Currently Airing
 - Aired: Apr 2, 2018 to ?
 - Premiered: Spring 2018
 - Broadcast: Unknown
 - Producers: None found, add some 
 - Licensors: None found, add some 
 - Studios: None found, add some 
 - Source: Picture book
 - Genres: Sci-Fi, Slice of Life, Comedy, Kids, School
 - Duration: 10 min.
 - Rating: G - All Ages


## Links

