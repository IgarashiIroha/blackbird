# Caligula

![Caligula](https://cdn.myanimelist.net/images/anime/1910/93404l.jpg)

* Japanese:  Caligula -カリギュラ-

## Information

 - Type: TV
 - Episodes: 12
 - Status: Finished Airing
 - Aired: Apr 8, 2018 to Jun 24, 2018
 - Premiered: Spring 2018
 - Broadcast: Sundays at 23:30 (JST)
 - Producers: Pony Canyon, Furyu
 - Licensors: Ponycan USA
 - Studios: Satelight
 - Source: Game
 - Genres: Action, Sci-Fi
 - Duration: 23 min. per ep.
 - Rating: PG-13 - Teens 13 or older


## Links

- [[Refrain] Caligula - 01 VOSTFR [720p]](https://uptobox.com/l6nwjzbpi55h)
- [[Refrain] Caligula - 01 VOSTFR [1080p]](https://uptobox.com/ksce24dkf4lx)
- [[Refrain] Caligula - 02 VOSTFR [720p]](https://uptobox.com/fq2zwwwz2g5r)
- [[Refrain] Caligula - 02 VOSTFR [1080p]](https://uptobox.com/cxnd39yzajqp)
- [[Refrain] Caligula - 03 VOSTFR [720p]](https://uptobox.com/ax91dtzxit1o)
- [[Refrain] Caligula - 03 VOSTFR [1080p]](https://uptobox.com/op8uxvy3tmiq)
- [[Refrain] Caligula - 04 VOSTFR [720p]](https://uptobox.com/7uago175wn86)
- [[Refrain] Caligula - 04 VOSTFR [1080p]](https://uptobox.com/wnmg4xzlwjdo)
- [[Refrain] Caligula - 05 VOSTFR [720p]](https://uptobox.com/6g8yfhtsmmem)
- [[Refrain] Caligula - 05 VOSTFR [1080p]](https://uptobox.com/vwtkyabxdai6)
- [[Refrain] Caligula - 06 VOSTFR [720p]](https://uptobox.com/tnvm0ndrhu9y)
- [[Refrain] Caligula - 06 VOSTFR [1080p]](https://uptobox.com/50hdso8rkv9w)
- [[Refrain] Caligula - 07 VOSTFR [720p]](https://uptobox.com/11p3prhrj35k)
- [[Refrain] Caligula - 07 VOSTFR [1080p]](https://uptobox.com/f8un6xktadsk)
- [[Refrain] Caligula - 08 VOSTFR [720p]](https://uptobox.com/71728yqz63kl)
- [[Refrain] Caligula - 08 VOSTFR [1080p]](https://uptobox.com/kyyvyzdf448f)
- [[Refrain] Caligula - 09 VOSTFR [720p]](https://uptobox.com/etxdb0msulau)
- [[Refrain] Caligula - 09 VOSTFR [1080p]](https://uptobox.com/z1vifilcxgy7)
- [[Refrain] Caligula - 10 VOSTFR [720p]](https://uptobox.com/k54ws1kkbtqk)
- [[Refrain] Caligula - 10 VOSTFR [1080p]](https://uptobox.com/dvouh57p0fih)
- [[Refrain] Caligula - 11 VOSTFR [720p]](https://uptobox.com/vy017pjwjfsr)
- [[Refrain] Caligula - 11 VOSTFR [1080p]](https://uptobox.com/bfiuaj0x0ayp)
- [[Refrain] Caligula - 12 VOSTFR [720p]](https://uptobox.com/su5n5gbhbqd9)
- [[Refrain] Caligula - 12 VOSTFR [1080p]](https://uptobox.com/cqe7qdur9t0x)
