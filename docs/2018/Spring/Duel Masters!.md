# Duel Masters!

![Duel Masters!](https://cdn.myanimelist.net/images/anime/1920/91047l.jpg)

* Japanese:  デュエル・マスターズ!

## Information

 - Type: TV
 - Episodes: Unknown
 - Status: Currently Airing
 - Aired: Apr 1, 2018 to ?
 - Premiered: Spring 2018
 - Broadcast: Sundays at 08:30 (JST)
 - Producers: None found, add some 
 - Licensors: None found, add some 
 - Studios: Ascension 
 - Source: Original
 - Genres: Action, Adventure, Comedy, Shounen
 - Duration: 24 min.
 - Rating: PG-13 - Teens 13 or older


## Links

