# Shiyan Pin Jiating

![Shiyan Pin Jiating](https://cdn.myanimelist.net/images/anime/7/88806l.jpg)

* Japanese:  実験品家族 -クリーチャーズ・ファミリー・デイズ-

## Information

 - Type: ONA
 - Episodes: 12
 - Status: Finished Airing
 - Aired: Apr 9, 2018 to Jun 19, 2018
 - Producers: Tencent Penguin Pictures, KJJ Animation, Da Huoniao Donghua
 - Licensors: None found, add some
 - Studios: None found, add some
 - Source: Web manga
 - Genres: Sci-Fi, Slice of Life
 - Duration: 15 min. per ep.
 - Rating: PG-13 - Teens 13 or older


## Links

- [[Refrain] Shiyan Pin Jiating - 01 VOSTFR [720p]](https://uptobox.com/kb8i9f318mkn)
- [[Refrain] Shiyan Pin Jiating - 01 VOSTFR [1080p]](https://uptobox.com/p58jgk1lqxzk)
- [[Refrain] Shiyan Pin Jiating - 02 VOSTFR [720p]](https://uptobox.com/qlj54ed1wac5)
- [[Refrain] Shiyan Pin Jiating - 02 VOSTFR [1080p]](https://uptobox.com/55ato0dt3fbv)
- [[Refrain] Shiyan Pin Jiating - 03 VOSTFR [720p]](https://uptobox.com/x162fl17az3o)
- [[Refrain] Shiyan Pin Jiating - 03 VOSTFR [1080p]](https://uptobox.com/t88kmqd9khsg)
- [[Refrain] Shiyan Pin Jiating - 04 VOSTFR [720p]](https://uptobox.com/7s58fmvdpqdj)
- [[Refrain] Shiyan Pin Jiating - 04 VOSTFR [1080p]](https://uptobox.com/hk4er1azqv5a)
- [[Refrain] Shiyan Pin Jiating - 05 VOSTFR [720p]](https://uptobox.com/cwjiomo4q9ww)
- [[Refrain] Shiyan Pin Jiating - 05 VOSTFR [1080p]](https://uptobox.com/r95mu3mc2dyl)
- [[Refrain] Shiyan Pin Jiating - 06 VOSTFR [720p]](https://uptobox.com/mmirq0237fok)
- [[Refrain] Shiyan Pin Jiating - 06 VOSTFR [1080p]](https://uptobox.com/m7mfayoa55r8)
- [[Refrain] Shiyan Pin Jiating - 07 VOSTFR [720p]](https://uptobox.com/jp4a6c8gjh5f)
- [[Refrain] Shiyan Pin Jiating - 07 VOSTFR [1080p]](https://uptobox.com/0rzkv4l7p9nu)
- [[Refrain] Shiyan Pin Jiating - 08 VOSTFR [720p]](https://uptobox.com/m8ay3o1g5y0j)
- [[Refrain] Shiyan Pin Jiating - 08 VOSTFR [1080p]](https://uptobox.com/54ti9012o44q)
- [[Refrain] Shiyan Pin Jiating - 09 VOSTFR [720p]](https://uptobox.com/suzmuf9o13j1)
- [[Refrain] Shiyan Pin Jiating - 09 VOSTFR [1080p]](https://uptobox.com/iaj2cwaislgw)
- [[Refrain] Shiyan Pin Jiating - 10 VOSTFR [720p]](https://uptobox.com/5avry4sv3vos)
- [[Refrain] Shiyan Pin Jiating - 10 VOSTFR [1080p]](https://uptobox.com/j8eln7w27fci)
- [[Refrain] Shiyan Pin Jiating - 11 VOSTFR [720p]](https://uptobox.com/5sfv68mmmp0s)
- [[Refrain] Shiyan Pin Jiating - 11 VOSTFR [1080p]](https://uptobox.com/f0jcuqb528cg)
- [[Refrain] Shiyan Pin Jiating - 12 VOSTFR [720p]](https://uptobox.com/5yqt45659pw2)
- [[Refrain] Shiyan Pin Jiating - 12 VOSTFR [1080p]](https://uptobox.com/3xqdu0j47y8o)
