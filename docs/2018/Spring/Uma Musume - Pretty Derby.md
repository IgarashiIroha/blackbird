# Uma Musume: Pretty Derby

![Uma Musume: Pretty Derby](https://cdn.myanimelist.net/images/anime/1683/91888l.jpg)

* Japanese:  ウマ娘 プリティーダービー

## Information

 - Type: TV
 - Episodes: 13
 - Status: Finished Airing
 - Aired: Apr 2, 2018 to Jun 18, 2018
 - Premiered: Spring 2018
 - Broadcast: Mondays at 00:00 (JST)
 - Producers: Lantis, TOHO animation, Kansai Telecasting, Cygames 
 - Licensors: None found, add some 
 - Studios: P.A. Works 
 - Source: Game
 - Genres: Comedy, Slice of Life, Sports
 - Duration: 23 min. per ep.
 - Rating: PG-13 - Teens 13 or older


## Links

