# Dorei-ku The Animation

![Dorei-ku The Animation](https://cdn.myanimelist.net/images/anime/1801/90264l.jpg)

* Japanese:  奴隷区 The Animation

## Information

 - Type: TV
 - Episodes: 12
 - Status: Finished Airing
 - Aired: Apr 13, 2018 to Jun 29, 2018
 - Premiered: Spring 2018
 - Broadcast: Fridays at 01:00 (JST)
 - Producers: None found, add some
 - Licensors: Sentai Filmworks
 - Studios: TNK, Zero-G
 - Source: Manga
 - Genres: Drama, Psychological
 - Duration: 24 min. per ep.
 - Rating: R - 17+ (violence & profanity)


## Links

- [[Refrain] Dorei-ku The Animation - 01 VOSTFR {ADN} [720p]](https://uptobox.com/ar1rkr04a4w9)
- [[Refrain] Dorei-ku The Animation - 01 VOSTFR {ADN} [1080p]](https://uptobox.com/uzyzhxhgtrsk)
- [[Refrain] Dorei-ku The Animation - 01 VOSTFR {NF} [720p]](https://uptobox.com/r9d0j7ddxx2p)
- [[Refrain] Dorei-ku The Animation - 01 VOSTFR {NF} [1080p]](https://uptobox.com/sutzg3w0673k)
- [[Refrain] Dorei-ku The Animation - 02 VOSTFR {ADN} [720p]](https://uptobox.com/0tujdlwj09yy)
- [[Refrain] Dorei-ku The Animation - 02 VOSTFR {ADN} [1080p]](https://uptobox.com/jdxar8120oup)
- [[Refrain] Dorei-ku The Animation - 02 VOSTFR {NF} [720p]](https://uptobox.com/4yy7vbsdzq31)
- [[Refrain] Dorei-ku The Animation - 02 VOSTFR {NF} [1080p]](https://uptobox.com/81swl8h1cvos)
- [[Refrain] Dorei-ku The Animation - 03 VOSTFR {ADN} [720p]](https://uptobox.com/1sv8i1haatve)
- [[Refrain] Dorei-ku The Animation - 03 VOSTFR {ADN} [1080p]](https://uptobox.com/0rxkr5lgqm4t)
- [[Refrain] Dorei-ku The Animation - 03 VOSTFR {NF} [720p]](https://uptobox.com/776mlfuqen0i)
- [[Refrain] Dorei-ku The Animation - 03 VOSTFR {NF} [1080p]](https://uptobox.com/dxagf6s500ld)
- [[Refrain] Dorei-ku The Animation - 04 VOSTFR {ADN} [720p]](https://uptobox.com/lx8c4lomo40z)
- [[Refrain] Dorei-ku The Animation - 04 VOSTFR {ADN} [1080p]](https://uptobox.com/s7a3hn803fvo)
- [[Refrain] Dorei-ku The Animation - 04 VOSTFR {NF} [720p]](https://uptobox.com/ylcf6h86eoqd)
- [[Refrain] Dorei-ku The Animation - 04 VOSTFR {NF} [1080p]](https://uptobox.com/qanyplaxlrlc)
- [[Refrain] Dorei-ku The Animation - 05 VOSTFR {ADN} [720p]](https://uptobox.com/9gumhrspvdas)
- [[Refrain] Dorei-ku The Animation - 05 VOSTFR {ADN} [1080p]](https://uptobox.com/p8o2vlniysnx)
- [[Refrain] Dorei-ku The Animation - 05 VOSTFR {NF} [720p]](https://uptobox.com/gf6f9pgagsbm)
- [[Refrain] Dorei-ku The Animation - 05 VOSTFR {NF} [1080p]](https://uptobox.com/bpdm7qi7sgo4)
- [[Refrain] Dorei-ku The Animation - 06 VOSTFR {ADN} [720p]](https://uptobox.com/sy1zw9ottjix)
- [[Refrain] Dorei-ku The Animation - 06 VOSTFR {ADN} [1080p]](https://uptobox.com/zfhfs0gpgytn)
- [[Refrain] Dorei-ku The Animation - 06 VOSTFR {NF} [720p]](https://uptobox.com/ou36xsr4tlze)
- [[Refrain] Dorei-ku The Animation - 06 VOSTFR {NF} [1080p]](https://uptobox.com/4aewyq9u3tdh)
- [[Refrain] Dorei-ku The Animation - 07 VOSTFR {ADN} [720p]](https://uptobox.com/mmm32b548kij)
- [[Refrain] Dorei-ku The Animation - 07 VOSTFR {ADN} [1080p]](https://uptobox.com/vgdsj4nyqqbd)
- [[Refrain] Dorei-ku The Animation - 07 VOSTFR {NF} [720p]](https://uptobox.com/y5jjtrx23kny)
- [[Refrain] Dorei-ku The Animation - 07 VOSTFR {NF} [1080p]](https://uptobox.com/n8ycctyacnrq)
- [[Refrain] Dorei-ku The Animation - 08 VOSTFR {ADN} [720p]](https://uptobox.com/hime5wk024rh)
- [[Refrain] Dorei-ku The Animation - 08 VOSTFR {ADN} [1080p]](https://uptobox.com/y5i6jrh62myy)
- [[Refrain] Dorei-ku The Animation - 08 VOSTFR {NF} [720p]](https://uptobox.com/6z02lgwm7ifa)
- [[Refrain] Dorei-ku The Animation - 08 VOSTFR {NF} [1080p]](https://uptobox.com/9vnb2ibx4vey)
- [[Refrain] Dorei-ku The Animation - 09 VOSTFR {ADN} [720p]](https://uptobox.com/zbvqrtb4mfbz)
- [[Refrain] Dorei-ku The Animation - 09 VOSTFR {ADN} [1080p]](https://uptobox.com/dkx3i8s4f5h1)
- [[Refrain] Dorei-ku The Animation - 09 VOSTFR {NF} [720p]](https://uptobox.com/cgk7tkph71oq)
- [[Refrain] Dorei-ku The Animation - 09 VOSTFR {NF} [1080p]](https://uptobox.com/j3q1vlxpq2xn)
- [[Refrain] Dorei-ku The Animation - 10 VOSTFR {ADN} [720p]](https://uptobox.com/jjj5755849po)
- [[Refrain] Dorei-ku The Animation - 10 VOSTFR {ADN} [1080p]](https://uptobox.com/mkvw2hckj9fa)
- [[Refrain] Dorei-ku The Animation - 10 VOSTFR {NF} [720p]](https://uptobox.com/azgvczq7fedo)
- [[Refrain] Dorei-ku The Animation - 10 VOSTFR {NF} [1080p]](https://uptobox.com/rx8eur1btlnw)
- [[Refrain] Dorei-ku The Animation - 11 VOSTFR {ADN} [720p]](https://uptobox.com/2sxosqqcfwq7)
- [[Refrain] Dorei-ku The Animation - 11 VOSTFR {ADN} [1080p]](https://uptobox.com/84zat6sk4z0i)
- [[Refrain] Dorei-ku The Animation - 11 VOSTFR {NF} [720p]](https://uptobox.com/wrmn3k0dh6j4)
- [[Refrain] Dorei-ku The Animation - 11 VOSTFR {NF} [1080p]](https://uptobox.com/vziok7iw6d1g)
- [[Refrain] Dorei-ku The Animation - 12 VOSTFR {ADN} [720p]](https://uptobox.com/s54nsgoexjf0)
- [[Refrain] Dorei-ku The Animation - 12 VOSTFR {ADN} [1080p]](https://uptobox.com/xhvipcbmmo9i)
- [[Refrain] Dorei-ku The Animation - 12 VOSTFR {NF} [720p]](https://uptobox.com/s0srcv9kai4d)
- [[Refrain] Dorei-ku The Animation - 12 VOSTFR {NF} [1080p]](https://uptobox.com/7bdum8ylvlbg)
