# Cutie Honey Universe

![Cutie Honey Universe](https://cdn.myanimelist.net/images/anime/1879/91014l.jpg)

* Japanese:  Cutie Honey Universe

## Information

 - Type: TV
 - Episodes: 12
 - Status: Finished Airing
 - Aired: Apr 8, 2018 to Jun 24, 2018
 - Premiered: Spring 2018
 - Broadcast: Sundays at 21:00 (JST)
 - Producers: Pony Canyon, AT-X, Memory-Tech, Dynamic Planning, Bandai Namco Entertainment, Pony Canyon Enterprise 
 - Licensors: Sentai Filmworks, Ponycan USA 
 - Studios: Production Reed 
 - Source: Manga
 - Genres: Action, Comedy, Magic, Romance, Sci-Fi, Shounen
 - Duration: 23 min. per ep.
 - Rating: R - 17+ (violence & profanity)


## Links

