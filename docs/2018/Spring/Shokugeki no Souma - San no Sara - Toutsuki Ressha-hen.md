# Shokugeki no Souma: San no Sara - Toutsuki Ressha-hen

![Shokugeki no Souma: San no Sara - Toutsuki Ressha-hen](https://cdn.myanimelist.net/images/anime/1604/93531l.jpg)

* Japanese:  食戟のソーマ 餐ノ皿 遠月列車篇

## Information

 - Type: TV
 - Episodes: 12
 - Status: Finished Airing
 - Aired: Apr 9, 2018 to Jun 25, 2018
 - Premiered: Spring 2018
 - Broadcast: Mondays at 00:30 (JST)
 - Producers: Shueisha
 - Licensors: None found, add some
 - Studios: J.C.Staff
 - Source: Manga
 - Genres: Ecchi, School, Shounen
 - Duration: 24 min. per ep.
 - Rating: PG-13 - Teens 13 or older


## Links

- [[Refrain] Shokugeki no Souma - San no Sara - Toutsuki Ressha-hen - 13 VOSTFR [720p]](https://uptobox.com/6x5wceogcnf0)
- [[Refrain] Shokugeki no Souma - San no Sara - Toutsuki Ressha-hen - 13 VOSTFR [1080p]](https://uptobox.com/j7trx5gp7t4i)
- [[Refrain] Shokugeki no Souma - San no Sara - Toutsuki Ressha-hen - 14 VOSTFR [720p]](https://uptobox.com/6qs4v7uwpuaj)
- [[Refrain] Shokugeki no Souma - San no Sara - Toutsuki Ressha-hen - 14 VOSTFR [1080p]](https://uptobox.com/pgco0zd71phy)
- [[Refrain] Shokugeki no Souma - San no Sara - Toutsuki Ressha-hen - 15 VOSTFR [720p]](https://uptobox.com/4k2me8uegjqm)
- [[Refrain] Shokugeki no Souma - San no Sara - Toutsuki Ressha-hen - 15 VOSTFR [1080p]](https://uptobox.com/e9wp26xp2iaj)
- [[Refrain] Shokugeki no Souma - San no Sara - Toutsuki Ressha-hen - 16 VOSTFR [720p]](https://uptobox.com/e26c62fen791)
- [[Refrain] Shokugeki no Souma - San no Sara - Toutsuki Ressha-hen - 16 VOSTFR [1080p]](https://uptobox.com/677utqaowcca)
- [[Refrain] Shokugeki no Souma - San no Sara - Toutsuki Ressha-hen - 17 VOSTFR [720p]](https://uptobox.com/1w23na0k76w6)
- [[Refrain] Shokugeki no Souma - San no Sara - Toutsuki Ressha-hen - 17 VOSTFR [1080p]](https://uptobox.com/xiyi0ahuoq1f)
- [[Refrain] Shokugeki no Souma - San no Sara - Toutsuki Ressha-hen - 18 VOSTFR [720p]](https://uptobox.com/f2gkpdw42y6n)
- [[Refrain] Shokugeki no Souma - San no Sara - Toutsuki Ressha-hen - 18 VOSTFR [1080p]](https://uptobox.com/ydn2x3nw70fk)
- [[Refrain] Shokugeki no Souma - San no Sara - Toutsuki Ressha-hen - 19 VOSTFR [720p]](https://uptobox.com/jd2m61pcl1be)
- [[Refrain] Shokugeki no Souma - San no Sara - Toutsuki Ressha-hen - 19 VOSTFR [1080p]](https://uptobox.com/nka66lpro2ct)
- [[Refrain] Shokugeki no Souma - San no Sara - Toutsuki Ressha-hen - 20 VOSTFR [720p]](https://uptobox.com/hlhtng9joau4)
- [[Refrain] Shokugeki no Souma - San no Sara - Toutsuki Ressha-hen - 20 VOSTFR [1080p]](https://uptobox.com/hj2h3n6s8c79)
- [[Refrain] Shokugeki no Souma - San no Sara - Toutsuki Ressha-hen - 21 VOSTFR [720p]](https://uptobox.com/4n1zxd0rl397)
- [[Refrain] Shokugeki no Souma - San no Sara - Toutsuki Ressha-hen - 21 VOSTFR [1080p]](https://uptobox.com/y7bz17gkuogs)
- [[Refrain] Shokugeki no Souma - San no Sara - Toutsuki Ressha-hen - 22 VOSTFR [720p]](https://uptobox.com/sws6q0yeufg9)
- [[Refrain] Shokugeki no Souma - San no Sara - Toutsuki Ressha-hen - 22 VOSTFR [1080p]](https://uptobox.com/ivoop93cjfex)
- [[Refrain] Shokugeki no Souma - San no Sara - Toutsuki Ressha-hen - 23 VOSTFR [720p]](https://uptobox.com/86v4h83sneac)
- [[Refrain] Shokugeki no Souma - San no Sara - Toutsuki Ressha-hen - 23 VOSTFR [1080p]](https://uptobox.com/dogdkkbp3f3i)
- [[Refrain] Shokugeki no Souma - San no Sara - Toutsuki Ressha-hen - 24 VOSTFR [720p]](https://uptobox.com/0rngex4m8303)
- [[Refrain] Shokugeki no Souma - San no Sara - Toutsuki Ressha-hen - 24 VOSTFR [1080p]](https://uptobox.com/icr96ou8x6r4)
