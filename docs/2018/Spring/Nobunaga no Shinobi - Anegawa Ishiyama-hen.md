# Nobunaga no Shinobi: Anegawa Ishiyama-hen

![Nobunaga no Shinobi: Anegawa Ishiyama-hen](https://cdn.myanimelist.net/images/anime/1651/91110l.jpg)

* Japanese:  信長の忍び~姉川・石山篇~

## Information

 - Type: TV
 - Episodes: 26
 - Status: Finished Airing
 - Aired: Apr 7, 2018 to Sep 29, 2018
 - Premiered: Spring 2018
 - Broadcast: Saturdays at 01:35 (JST)
 - Producers: None found, add some
 - Licensors: None found, add some
 - Studios: TMS Entertainment
 - Source: 4-koma manga
 - Genres: Comedy, Historical
 - Duration: 3 min. per ep.
 - Rating: PG - Children


## Links

- [[Refrain] Nobunaga no Shinobi - Anegawa Ishiyama-Hen - 53 VOSTFR [720p]](https://uptobox.com/c4dyxf4mkaxj)
- [[Refrain] Nobunaga no Shinobi - Anegawa Ishiyama-Hen - 53 VOSTFR [1080p]](https://uptobox.com/ck67or3ewpk2)
- [[Refrain] Nobunaga no Shinobi - Anegawa Ishiyama-Hen - 54 VOSTFR [720p]](https://uptobox.com/wu5u3kcuebpf)
- [[Refrain] Nobunaga no Shinobi - Anegawa Ishiyama-Hen - 54 VOSTFR [1080p]](https://uptobox.com/r1lleanvylss)
- [[Refrain] Nobunaga no Shinobi - Anegawa Ishiyama-Hen - 55 VOSTFR [720p]](https://uptobox.com/5wicc3s0a4f8)
- [[Refrain] Nobunaga no Shinobi - Anegawa Ishiyama-Hen - 55 VOSTFR [1080p]](https://uptobox.com/rzinu8uggkua)
- [[Refrain] Nobunaga no Shinobi - Anegawa Ishiyama-Hen - 56 VOSTFR [720p]](https://uptobox.com/w4xd10odaz2e)
- [[Refrain] Nobunaga no Shinobi - Anegawa Ishiyama-Hen - 56 VOSTFR [1080p]](https://uptobox.com/eia8k7wjyvh3)
- [[Refrain] Nobunaga no Shinobi - Anegawa Ishiyama-Hen - 57 VOSTFR [720p]](https://uptobox.com/85f8eya6nxcr)
- [[Refrain] Nobunaga no Shinobi - Anegawa Ishiyama-Hen - 57 VOSTFR [1080p]](https://uptobox.com/0dizkszv66vs)
- [[Refrain] Nobunaga no Shinobi - Anegawa Ishiyama-Hen - 58 VOSTFR [720p]](https://uptobox.com/ra8zje5f1hh6)
- [[Refrain] Nobunaga no Shinobi - Anegawa Ishiyama-Hen - 58 VOSTFR [1080p]](https://uptobox.com/pkw02o2g5796)
- [[Refrain] Nobunaga no Shinobi - Anegawa Ishiyama-Hen - 59 VOSTFR [720p]](https://uptobox.com/x7l4ke8wm521)
- [[Refrain] Nobunaga no Shinobi - Anegawa Ishiyama-Hen - 59 VOSTFR [1080p]](https://uptobox.com/6995ytosufos)
- [[Refrain] Nobunaga no Shinobi - Anegawa Ishiyama-Hen - 60 VOSTFR [720p]](https://uptobox.com/iz78czsadmn6)
- [[Refrain] Nobunaga no Shinobi - Anegawa Ishiyama-Hen - 60 VOSTFR [1080p]](https://uptobox.com/ktf8jp9ktajf)
- [[Refrain] Nobunaga no Shinobi - Anegawa Ishiyama-Hen - 61 VOSTFR [720p]](https://uptobox.com/ga72x8fb9w4j)
- [[Refrain] Nobunaga no Shinobi - Anegawa Ishiyama-Hen - 61 VOSTFR [1080p]](https://uptobox.com/kcypkm2o4zd5)
- [[Refrain] Nobunaga no Shinobi - Anegawa Ishiyama-Hen - 62 VOSTFR [720p]](https://uptobox.com/aqe9s46rm9jo)
- [[Refrain] Nobunaga no Shinobi - Anegawa Ishiyama-Hen - 62 VOSTFR [1080p]](https://uptobox.com/m8atzmmyv61j)
- [[Refrain] Nobunaga no Shinobi - Anegawa Ishiyama-Hen - 63 VOSTFR [720p]](https://uptobox.com/lv5fjoei2rda)
- [[Refrain] Nobunaga no Shinobi - Anegawa Ishiyama-Hen - 63 VOSTFR [1080p]](https://uptobox.com/i5nzsno8kjao)
- [[Refrain] Nobunaga no Shinobi - Anegawa Ishiyama-Hen - 64 VOSTFR [720p]](https://uptobox.com/ytgld3lmlhvd)
- [[Refrain] Nobunaga no Shinobi - Anegawa Ishiyama-Hen - 64 VOSTFR [1080p]](https://uptobox.com/kkruiig0wtqe)
- [[Refrain] Nobunaga no Shinobi - Anegawa Ishiyama-Hen - 65 VOSTFR [720p]](https://uptobox.com/8h8wgp4jyg1f)
- [[Refrain] Nobunaga no Shinobi - Anegawa Ishiyama-Hen - 65 VOSTFR [1080p]](https://uptobox.com/1ijlerfcp5f5)
- [[Refrain] Nobunaga no Shinobi - Anegawa Ishiyama-hen - 66 VOSTFR [720p]](https://uptobox.com/gwgr8h3ftml8)
- [[Refrain] Nobunaga no Shinobi - Anegawa Ishiyama-hen - 66 VOSTFR [1080p]](https://uptobox.com/q177bft3qb55)
- [[Refrain] Nobunaga no Shinobi - Anegawa Ishiyama-hen - 67 VOSTFR [720p]](https://uptobox.com/k2d4e30b4gqz)
- [[Refrain] Nobunaga no Shinobi - Anegawa Ishiyama-hen - 67 VOSTFR [1080p]](https://uptobox.com/wplf9otqd3vy)
- [[Refrain] Nobunaga no Shinobi - Anegawa Ishiyama-hen - 68 VOSTFR [720p]](https://uptobox.com/ylj6kazyc4uc)
- [[Refrain] Nobunaga no Shinobi - Anegawa Ishiyama-hen - 68 VOSTFR [1080p]](https://uptobox.com/1uo11b6bs5hg)
- [[Refrain] Nobunaga no Shinobi - Anegawa Ishiyama-hen - 69 VOSTFR [720p]](https://uptobox.com/adm5nsf6grfr)
- [[Refrain] Nobunaga no Shinobi - Anegawa Ishiyama-hen - 69 VOSTFR [1080p]](https://uptobox.com/vxlo82hj8cwj)
- [[Refrain] Nobunaga no Shinobi - Anegawa Ishiyama-hen - 70 VOSTFR [720p]](https://uptobox.com/20cf579l94ei)
- [[Refrain] Nobunaga no Shinobi - Anegawa Ishiyama-hen - 70 VOSTFR [1080p]](https://uptobox.com/afweiydggsad)
- [[Refrain] Nobunaga no Shinobi - Anegawa Ishiyama-hen - 71 VOSTFR [720p]](https://uptobox.com/fh4dpdiexf0i)
- [[Refrain] Nobunaga no Shinobi - Anegawa Ishiyama-hen - 71 VOSTFR [1080p]](https://uptobox.com/3cfndahd8ioi)
- [[Refrain] Nobunaga no Shinobi - Anegawa Ishiyama-hen - 72 VOSTFR [720p]](https://uptobox.com/g1i1se73yiv8)
- [[Refrain] Nobunaga no Shinobi - Anegawa Ishiyama-hen - 72 VOSTFR [1080p]](https://uptobox.com/dafhi8cxjmvb)
- [[Refrain] Nobunaga no Shinobi - Anegawa Ishiyama-hen - 73 VOSTFR [720p]](https://uptobox.com/4p9qh8f6anp9)
- [[Refrain] Nobunaga no Shinobi - Anegawa Ishiyama-hen - 73 VOSTFR [1080p]](https://uptobox.com/k2uuofg2b07b)
- [[Refrain] Nobunaga no Shinobi - Anegawa Ishiyama-hen - 74 VOSTFR [720p]](https://uptobox.com/iq9wo4lj1noj)
- [[Refrain] Nobunaga no Shinobi - Anegawa Ishiyama-hen - 74 VOSTFR [1080p]](https://uptobox.com/f3wifeclc4pe)
- [[Refrain] Nobunaga no Shinobi - Anegawa Ishiyama-hen - 75 VOSTFR [720p]](https://uptobox.com/0um014jto46p)
- [[Refrain] Nobunaga no Shinobi - Anegawa Ishiyama-hen - 75 VOSTFR [1080p]](https://uptobox.com/7ebjzu69wdcq)
- [[Refrain] Nobunaga no Shinobi - Anegawa Ishiyama-hen - 76 VOSTFR [720p]](https://uptobox.com/p3equbuipvx5)
- [[Refrain] Nobunaga no Shinobi - Anegawa Ishiyama-hen - 76 VOSTFR [1080p]](https://uptobox.com/5wop5qipbs84)
- [[Refrain] Nobunaga no Shinobi - Anegawa Ishiyama-hen - 77 VOSTFR [720p]](https://uptobox.com/llrvxkic5tln)
- [[Refrain] Nobunaga no Shinobi - Anegawa Ishiyama-hen - 77 VOSTFR [1080p]](https://uptobox.com/nsqc5rnxf2vj)
- [[Refrain] Nobunaga no Shinobi - Anegawa Ishiyama-hen - 78 VOSTFR [720p]](https://uptobox.com/390riyjnpf89)
- [[Refrain] Nobunaga no Shinobi - Anegawa Ishiyama-hen - 78 VOSTFR [1080p]](https://uptobox.com/5qfw25cgksbg)
