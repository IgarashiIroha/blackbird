# Major 2nd

![Major 2nd](https://cdn.myanimelist.net/images/anime/1066/90145l.jpg)

* Japanese:  MAJOR 2nd

## Information

 - Type: TV
 - Episodes: 25
 - Status: Finished Airing
 - Aired: Apr 7, 2018 to Sep 22, 2018
 - Premiered: Spring 2018
 - Broadcast: Saturdays at 17:35 (JST)
 - Producers: NHK, Shogakukan, NHK Enterprises
 - Licensors: None found, add some
 - Studios: OLM
 - Source: Manga
 - Genres: Comedy, Sports, Drama, Shounen
 - Duration: 24 min. per ep.
 - Rating: PG-13 - Teens 13 or older


## Links

- [[Refrain] Major 2nd (TV) - 01 VOSTFR [720p]](https://uptobox.com/gdugr53fhurw)
- [[Refrain] Major 2nd (TV) - 01 VOSTFR [1080p]](https://uptobox.com/29gpkmli568r)
- [[Refrain] Major 2nd (TV) - 02 VOSTFR [720p]](https://uptobox.com/ka0x2q03nt4v)
- [[Refrain] Major 2nd (TV) - 02 VOSTFR [1080p]](https://uptobox.com/ebqrg2spkqth)
- [[Refrain] Major 2nd (TV) - 03 VOSTFR [720p]](https://uptobox.com/0ogcqjtmxn7p)
- [[Refrain] Major 2nd (TV) - 03 VOSTFR [1080p]](https://uptobox.com/5k6rnkhmviu7)
- [[Refrain] Major 2nd (TV) - 04 VOSTFR [720p]](https://uptobox.com/twokmpeuukcv)
- [[Refrain] Major 2nd (TV) - 04 VOSTFR [1080p]](https://uptobox.com/jhk1v3i3uyl9)
- [[Refrain] Major 2nd (TV) - 05 VOSTFR [720p]](https://uptobox.com/y40qykd9b9b9)
- [[Refrain] Major 2nd (TV) - 05 VOSTFR [1080p]](https://uptobox.com/dm1vlx4g0pp7)
- [[Refrain] Major 2nd (TV) - 06 VOSTFR [720p]](https://uptobox.com/cr6cn3hwc3jz)
- [[Refrain] Major 2nd (TV) - 06 VOSTFR [1080p]](https://uptobox.com/lbws2wz4p0aa)
- [[Refrain] Major 2nd (TV) - 07 VOSTFR [720p]](https://uptobox.com/4b4fsvjpse5u)
- [[Refrain] Major 2nd (TV) - 07 VOSTFR [1080p]](https://uptobox.com/i8o2htwedfzy)
- [[Refrain] Major 2nd (TV) - 08 VOSTFR [720p]](https://uptobox.com/5ew825bcl51n)
- [[Refrain] Major 2nd (TV) - 08 VOSTFR [1080p]](https://uptobox.com/lz3vccl1s0lo)
- [[Refrain] Major 2nd (TV) - 09 VOSTFR [720p]](https://uptobox.com/dy0h49onfros)
- [[Refrain] Major 2nd (TV) - 09 VOSTFR [1080p]](https://uptobox.com/uweoksjzypkh)
- [[Refrain] Major 2nd (TV) - 10 VOSTFR [720p]](https://uptobox.com/wwoizigl890j)
- [[Refrain] Major 2nd (TV) - 10 VOSTFR [1080p]](https://uptobox.com/812bgv8mb5t8)
- [[Refrain] Major 2nd (TV) - 11 VOSTFR [720p]](https://uptobox.com/l6en3b0ekl4l)
- [[Refrain] Major 2nd (TV) - 11 VOSTFR [1080p]](https://uptobox.com/e4buyv5twnix)
- [[Refrain] Major 2nd (TV) - 12 VOSTFR [720p]](https://uptobox.com/4ij23p11x4wa)
- [[Refrain] Major 2nd (TV) - 12 VOSTFR [1080p]](https://uptobox.com/xwdafalcf617)
- [[Refrain] Major 2nd (TV) - 13 VOSTFR [720p]](https://uptobox.com/ppogjpy0395u)
- [[Refrain] Major 2nd (TV) - 13 VOSTFR [1080p]](https://uptobox.com/i4k0pqeucelx)
- [[Refrain] Major 2nd (TV) - 14 VOSTFR [720p]](https://uptobox.com/s89t1oqxvxvo)
- [[Refrain] Major 2nd (TV) - 14 VOSTFR [1080p]](https://uptobox.com/s6m8aaz79cvn)
- [[Refrain] Major 2nd (TV) - 15 VOSTFR [720p]](https://uptobox.com/2whb2f506h56)
- [[Refrain] Major 2nd (TV) - 15 VOSTFR [1080p]](https://uptobox.com/3l9tvn40ttld)
- [[Refrain] Major 2nd (TV) - 16 VOSTFR [720p]](https://uptobox.com/w3nj1g498l0k)
- [[Refrain] Major 2nd (TV) - 16 VOSTFR [1080p]](https://uptobox.com/6jty5s8v2mx7)
- [[Refrain] Major 2nd (TV) - 17 VOSTFR [720p]](https://uptobox.com/281x2slbsnev)
- [[Refrain] Major 2nd (TV) - 17 VOSTFR [1080p]](https://uptobox.com/h18j5qz48g2p)
- [[Refrain] Major 2nd (TV) - 18 VOSTFR [720p]](https://uptobox.com/jo9v6ypc6kf3)
- [[Refrain] Major 2nd (TV) - 18 VOSTFR [1080p]](https://uptobox.com/74riiebjmslz)
- [[Refrain] Major 2nd (TV) - 19 VOSTFR [720p]](https://uptobox.com/vjnk8nsu9swp)
- [[Refrain] Major 2nd (TV) - 19 VOSTFR [1080p]](https://uptobox.com/5k9misw126jp)
- [[Refrain] Major 2nd (TV) - 20 VOSTFR [720p]](https://uptobox.com/c4sdw22jiweh)
- [[Refrain] Major 2nd (TV) - 20 VOSTFR [1080p]](https://uptobox.com/f1s4ndywu8bu)
- [[Refrain] Major 2nd (TV) - 21 VOSTFR [720p]](https://uptobox.com/we92y54oq7h7)
- [[Refrain] Major 2nd (TV) - 21 VOSTFR [1080p]](https://uptobox.com/xgpi9ye7x9la)
- [[Refrain] Major 2nd (TV) - 22 VOSTFR [720p]](https://uptobox.com/tyv3zesj31tb)
- [[Refrain] Major 2nd (TV) - 22 VOSTFR [1080p]](https://uptobox.com/jjhe9ln648qy)
- [[Refrain] Major 2nd (TV) - 23 VOSTFR [720p]](https://uptobox.com/ueih9jc423ol)
- [[Refrain] Major 2nd (TV) - 23 VOSTFR [1080p]](https://uptobox.com/tf63xjla2kzs)
- [[Refrain] Major 2nd (TV) - 24 VOSTFR [720p]](https://uptobox.com/ow782fgaluc9)
- [[Refrain] Major 2nd (TV) - 24 VOSTFR [1080p]](https://uptobox.com/gjojpabd3oqh)
- [[Refrain] Major 2nd (TV) - 25 VOSTFR [720p]](https://uptobox.com/31w0hrae3kl0)
- [[Refrain] Major 2nd (TV) - 25 VOSTFR [1080p]](https://uptobox.com/bvqrketg8c4p)
