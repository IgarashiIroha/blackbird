# Kuroneko Monroe

![Kuroneko Monroe](https://cdn.myanimelist.net/images/anime/1095/90602l.jpg)

* Japanese:  黒猫モンロヲ

## Information

 - Type: TV
 - Episodes: 23
 - Status: Finished Airing
 - Aired: Apr 17, 2018 to Sep 25, 2018
 - Premiered: Spring 2018
 - Broadcast: Tuesdays at 02:46 (JST)
 - Producers: None found, add some 
 - Licensors: None found, add some 
 - Studios: None found, add some 
 - Source: Manga
 - Genres: Comedy
 - Duration: 4 min. per ep.
 - Rating: G - All Ages


## Links

