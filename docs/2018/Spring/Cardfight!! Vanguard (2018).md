# Cardfight!! Vanguard (2018)

![Cardfight!! Vanguard (2018)](https://cdn.myanimelist.net/images/anime/1408/96485l.jpg)

* Japanese:  カードファイト!! ヴァンガード (2018)

## Information

 - Type: TV
 - Episodes: Unknown
 - Status: Currently Airing
 - Aired: May 5, 2018 to ?
 - Premiered: Spring 2018
 - Broadcast: Saturdays at 22:30 (JST)
 - Producers: None found, add some 
 - Licensors: None found, add some 
 - Studios: OLM 
 - Source: Manga
 - Genres: Action, Game, Shounen
 - Duration: 23 min.
 - Rating: PG-13 - Teens 13 or older


## Links

