# Lupin III: Part 5

![Lupin III: Part 5](https://cdn.myanimelist.net/images/anime/1235/91028l.jpg)

* Japanese:  ルパン三世 PART5

## Information

 - Type: TV
 - Episodes: 24
 - Status: Finished Airing
 - Aired: Apr 4, 2018 to Sep 19, 2018
 - Premiered: Spring 2018
 - Broadcast: Wednesdays at 01:29 (JST)
 - Producers: Nippon Television Network 
 - Licensors: None found, add some 
 - Studios: Telecom Animation Film 
 - Source: Manga
 - Genres: Action, Adventure, Comedy, Mystery, Seinen
 - Duration: 22 min. per ep.
 - Rating: PG-13 - Teens 13 or older


## Links

