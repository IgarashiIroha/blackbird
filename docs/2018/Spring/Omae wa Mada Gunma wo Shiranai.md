# Omae wa Mada Gunma wo Shiranai

![Omae wa Mada Gunma wo Shiranai](https://cdn.myanimelist.net/images/anime/1433/90426l.jpg)

* Japanese:  お前はまだグンマを知らない

## Information

 - Type: TV
 - Episodes: 12
 - Status: Finished Airing
 - Aired: Apr 2, 2018 to Jun 18, 2018
 - Premiered: Spring 2018
 - Broadcast: Mondays at 20:55 (JST)
 - Producers: TOHO animation
 - Licensors: None found, add some
 - Studios: Asahi Production
 - Source: Web manga
 - Genres: Comedy, School
 - Duration: 3 min. per ep.
 - Rating: PG-13 - Teens 13 or older


## Links

- [[Refrain] Omae wa Mada Gunma wo Shiranai - 01 VOSTFR [720p]](https://uptobox.com/qbwk8yskbvz4)
- [[Refrain] Omae wa Mada Gunma wo Shiranai - 01 VOSTFR [1080p]](https://uptobox.com/xh36wcoh1m70)
- [[Refrain] Omae wa Mada Gunma wo Shiranai - 02 VOSTFR [720p]](https://uptobox.com/mu79sn8s5ur5)
- [[Refrain] Omae wa Mada Gunma wo Shiranai - 02 VOSTFR [1080p]](https://uptobox.com/i30378l1xiea)
- [[Refrain] Omae wa Mada Gunma wo Shiranai - 03 VOSTFR [720p]](https://uptobox.com/i2vfaa9rgq6s)
- [[Refrain] Omae wa Mada Gunma wo Shiranai - 03 VOSTFR [1080p]](https://uptobox.com/vypvbgkix0pk)
- [[Refrain] Omae wa Mada Gunma wo Shiranai - 04 VOSTFR [720p]](https://uptobox.com/q967b71jf7wx)
- [[Refrain] Omae wa Mada Gunma wo Shiranai - 04 VOSTFR [1080p]](https://uptobox.com/u2bpcujm5dsd)
- [[Refrain] Omae wa Mada Gunma wo Shiranai - 05 VOSTFR [720p]](https://uptobox.com/2mfbc4cygyla)
- [[Refrain] Omae wa Mada Gunma wo Shiranai - 05 VOSTFR [1080p]](https://uptobox.com/o5hmj83awsj3)
- [[Refrain] Omae wa Mada Gunma wo Shiranai - 06 VOSTFR [720p]](https://uptobox.com/kn6b0pa42o20)
- [[Refrain] Omae wa Mada Gunma wo Shiranai - 06 VOSTFR [1080p]](https://uptobox.com/djagewo725o1)
- [[Refrain] Omae wa Mada Gunma wo Shiranai - 07 VOSTFR [720p]](https://uptobox.com/6ns5o2ushvgj)
- [[Refrain] Omae wa Mada Gunma wo Shiranai - 07 VOSTFR [1080p]](https://uptobox.com/60honazaijpm)
- [[Refrain] Omae wa Mada Gunma wo Shiranai - 08 VOSTFR [720p]](https://uptobox.com/tf0v3lzeurde)
- [[Refrain] Omae wa Mada Gunma wo Shiranai - 08 VOSTFR [1080p]](https://uptobox.com/bk5pk0tc7eqe)
- [[Refrain] Omae wa Mada Gunma wo Shiranai - 09 VOSTFR [720p]](https://uptobox.com/18lohn7bwb3i)
- [[Refrain] Omae wa Mada Gunma wo Shiranai - 09 VOSTFR [1080p]](https://uptobox.com/b4kyf6cxwp5m)
- [[Refrain] Omae wa Mada Gunma wo Shiranai - 10 VOSTFR [720p]](https://uptobox.com/1kaldhuvn6ie)
- [[Refrain] Omae wa Mada Gunma wo Shiranai - 10 VOSTFR [1080p]](https://uptobox.com/b9gzs5u3okiz)
- [[Refrain] Omae wa Mada Gunma wo Shiranai - 11 VOSTFR [720p]](https://uptobox.com/z28pmca90z44)
- [[Refrain] Omae wa Mada Gunma wo Shiranai - 11 VOSTFR [1080p]](https://uptobox.com/cxv3i8gjn6dn)
- [[Refrain] Omae wa Mada Gunma wo Shiranai - 12 VOSTFR [720p]](https://uptobox.com/605iinay86fv)
- [[Refrain] Omae wa Mada Gunma wo Shiranai - 12 VOSTFR [1080p]](https://uptobox.com/vrwdea8hduem)
