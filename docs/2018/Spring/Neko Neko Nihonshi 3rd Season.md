# Neko Neko Nihonshi 3rd Season

![Neko Neko Nihonshi 3rd Season](https://cdn.myanimelist.net/images/anime/1689/90730l.jpg)

* Japanese:  ねこねこ日本史 第3期

## Information

 - Type: TV
 - Episodes: Unknown
 - Status: Currently Airing
 - Aired: Apr 4, 2018 to ?
 - Premiered: Spring 2018
 - Broadcast: Unknown
 - Producers: None found, add some 
 - Licensors: None found, add some 
 - Studios: None found, add some 
 - Source: Web manga
 - Genres: Historical, Kids, Parody
 - Duration: 9 min.
 - Rating: G - All Ages


## Links

