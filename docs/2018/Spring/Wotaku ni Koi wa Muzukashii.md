# Wotaku ni Koi wa Muzukashii

![Wotaku ni Koi wa Muzukashii](https://cdn.myanimelist.net/images/anime/1864/93518l.jpg)

* Japanese:  ヲタクに恋は難しい

## Information

 - Type: TV
 - Episodes: 11
 - Status: Finished Airing
 - Aired: Apr 13, 2018 to Jun 22, 2018
 - Premiered: Spring 2018
 - Broadcast: Fridays at 00:55 (JST)
 - Producers: Aniplex, Dentsu, Fuji TV, Half H.P Studio, Ichijinsha, Kanetsu Co., LTD.
 - Licensors: None found, add some
 - Studios: A-1 Pictures
 - Source: Web manga
 - Genres: Slice of Life, Comedy, Romance
 - Duration: 22 min. per ep.
 - Rating: PG-13 - Teens 13 or older


## Links

- [[Refrain] Wotaku ni Koi wa Muzukashii - 01 VOSTFR [720p]](https://uptobox.com/ws0lnkpxhfz3)
- [[Refrain] Wotaku ni Koi wa Muzukashii - 01 VOSTFR [1080p]](https://uptobox.com/u2l6s9jhvfct)
- [[Refrain] Wotaku ni Koi wa Muzukashii - 02 VOSTFR [720p]](https://uptobox.com/sz8hajqh4vp9)
- [[Refrain] Wotaku ni Koi wa Muzukashii - 02 VOSTFR [1080p]](https://uptobox.com/4k8qnjp0tldk)
- [[Refrain] Wotaku ni Koi wa Muzukashii - 03 VOSTFR [720p]](https://uptobox.com/x0s6wnbewuxr)
- [[Refrain] Wotaku ni Koi wa Muzukashii - 03 VOSTFR [1080p]](https://uptobox.com/giu5qrbdj8h1)
- [[Refrain] Wotaku ni Koi wa Muzukashii - 04 VOSTFR [720p]](https://uptobox.com/krpy6p9xnulr)
- [[Refrain] Wotaku ni Koi wa Muzukashii - 04 VOSTFR [1080p]](https://uptobox.com/yfh5kxzmwixb)
- [[Refrain] Wotaku ni Koi wa Muzukashii - 05 VOSTFR [720p]](https://uptobox.com/pw87at3wn0f9)
- [[Refrain] Wotaku ni Koi wa Muzukashii - 05 VOSTFR [1080p]](https://uptobox.com/lkat9c2c22a9)
- [[Refrain] Wotaku ni Koi wa Muzukashii - 06 VOSTFR [720p]](https://uptobox.com/vwwrny13b43x)
- [[Refrain] Wotaku ni Koi wa Muzukashii - 06 VOSTFR [1080p]](https://uptobox.com/2xw9iqejsof8)
- [[Refrain] Wotaku ni Koi wa Muzukashii - 07 VOSTFR [720p]](https://uptobox.com/swlhjnuq91m7)
- [[Refrain] Wotaku ni Koi wa Muzukashii - 07 VOSTFR [1080p]](https://uptobox.com/o2sy05h73x06)
- [[Refrain] Wotaku ni Koi wa Muzukashii - 08 VOSTFR [720p]](https://uptobox.com/la0z73dww2f5)
- [[Refrain] Wotaku ni Koi wa Muzukashii - 08 VOSTFR [1080p]](https://uptobox.com/nm3uy2pvr4xw)
- [[Refrain] Wotaku ni Koi wa Muzukashii - 09 VOSTFR [720p]](https://uptobox.com/0sp1fuf5q4um)
- [[Refrain] Wotaku ni Koi wa Muzukashii - 09 VOSTFR [1080p]](https://uptobox.com/n7qhfk5bs9y2)
- [[Refrain] Wotaku ni Koi wa Muzukashii - 10 VOSTFR [720p]](https://uptobox.com/r8a8t7jvuoct)
- [[Refrain] Wotaku ni Koi wa Muzukashii - 10 VOSTFR [1080p]](https://uptobox.com/cy05606ejj7y)
- [[Refrain] Wotaku ni Koi wa Muzukashii - 11 VOSTFR [720p]](https://uptobox.com/rdov1y9tlj1q)
- [[Refrain] Wotaku ni Koi wa Muzukashii - 11 VOSTFR [1080p]](https://uptobox.com/kse2onk2dlpu)
