# Megalo Box

![Megalo Box](https://cdn.myanimelist.net/images/anime/1958/93533l.jpg)

* Japanese:  メガロボクス

## Information

 - Type: TV
 - Episodes: 13
 - Status: Finished Airing
 - Aired: Apr 6, 2018 to Jun 29, 2018
 - Premiered: Spring 2018
 - Broadcast: Fridays at 01:28 (JST)
 - Producers: TBS, Kodansha, Bandai Namco Arts
 - Licensors: Viz Media
 - Studios: TMS Entertainment
 - Source: Original
 - Genres: Action, Sci-Fi, Slice of Life, Sports, Drama
 - Duration: 24 min. per ep.
 - Rating: R - 17+ (violence & profanity)


## Links

- [[Refrain] Megalo Box - 01 VOSTFR {ADN} [720p]](https://uptobox.com/l4if932vanac)
- [[Refrain] Megalo Box - 01 VOSTFR {ADN} [1080p]](https://uptobox.com/kyg1005kzf55)
- [[Refrain] Megalo Box - 01 VOSTFR {CR} [720p]](https://uptobox.com/p3lu0q6tlqjh)
- [[Refrain] Megalo Box - 01 VOSTFR {CR} [1080p]](https://uptobox.com/f89wsxg7qh00)
- [[Refrain] Megalo Box - 02 VOSTFR {ADN} [720p]](https://uptobox.com/3azdti96k2g7)
- [[Refrain] Megalo Box - 02 VOSTFR {ADN} [1080p]](https://uptobox.com/9tlqv1y8vecg)
- [[Refrain] Megalo Box - 02 VOSTFR {CR} [720p]](https://uptobox.com/w082wzyea9ed)
- [[Refrain] Megalo Box - 02 VOSTFR {CR} [1080p]](https://uptobox.com/ajtaoy8g7hvv)
- [[Refrain] Megalo Box - 03 VOSTFR {ADN} [720p]](https://uptobox.com/5wzrtqaapy39)
- [[Refrain] Megalo Box - 03 VOSTFR {ADN} [1080p]](https://uptobox.com/vckgb71vrxjh)
- [[Refrain] Megalo Box - 03 VOSTFR {CR} [720p]](https://uptobox.com/wqmcsksx0idk)
- [[Refrain] Megalo Box - 03 VOSTFR {CR} [1080p]](https://uptobox.com/o8tmr877qxcg)
- [[Refrain] Megalo Box - 04 VOSTFR {ADN} [720p]](https://uptobox.com/xjer4kxbwcy5)
- [[Refrain] Megalo Box - 04 VOSTFR {ADN} [1080p]](https://uptobox.com/97w977am48ot)
- [[Refrain] Megalo Box - 04 VOSTFR {CR} [720p]](https://uptobox.com/m0h9376wt6g7)
- [[Refrain] Megalo Box - 04 VOSTFR {CR} [1080p]](https://uptobox.com/ykaeayt9dxiw)
- [[Refrain] Megalo Box - 05 VOSTFR {ADN} [720p]](https://uptobox.com/vy8z931wxxuk)
- [[Refrain] Megalo Box - 05 VOSTFR {ADN} [1080p]](https://uptobox.com/dqn9srp8gxkg)
- [[Refrain] Megalo Box - 05 VOSTFR {CR} [720p]](https://uptobox.com/uryme9uplk5n)
- [[Refrain] Megalo Box - 05 VOSTFR {CR} [1080p]](https://uptobox.com/6i66yqgd9whn)
- [[Refrain] Megalo Box - 06 VOSTFR {ADN} [720p]](https://uptobox.com/an8eebxacncd)
- [[Refrain] Megalo Box - 06 VOSTFR {ADN} [1080p]](https://uptobox.com/aabn7cvxlcj8)
- [[Refrain] Megalo Box - 06 VOSTFR {CR} [720p]](https://uptobox.com/d66rxft5xond)
- [[Refrain] Megalo Box - 06 VOSTFR {CR} [1080p]](https://uptobox.com/lpw7uh81xgr0)
- [[Refrain] Megalo Box - 07 VOSTFR {ADN} [720p]](https://uptobox.com/ijac9o0f7h6s)
- [[Refrain] Megalo Box - 07 VOSTFR {ADN} [1080p]](https://uptobox.com/vdzucnke206d)
- [[Refrain] Megalo Box - 07 VOSTFR {CR} [720p]](https://uptobox.com/3bnwi5r3h7ox)
- [[Refrain] Megalo Box - 07 VOSTFR {CR} [1080p]](https://uptobox.com/m70b8gnbo3qf)
- [[Refrain] Megalo Box - 08 VOSTFR {ADN} [720p]](https://uptobox.com/q9mgb6jimc14)
- [[Refrain] Megalo Box - 08 VOSTFR {ADN} [1080p]](https://uptobox.com/pqrhq3kdmz2y)
- [[Refrain] Megalo Box - 08 VOSTFR {CR} [720p]](https://uptobox.com/7zh3h4dom5wl)
- [[Refrain] Megalo Box - 08 VOSTFR {CR} [1080p]](https://uptobox.com/ae2kblwu3y89)
- [[Refrain] Megalo Box - 09 VOSTFR {ADN} [720p]](https://uptobox.com/4agwtb2i5ahg)
- [[Refrain] Megalo Box - 09 VOSTFR {ADN} [1080p]](https://uptobox.com/0qmcn9u7fm0a)
- [[Refrain] Megalo Box - 09 VOSTFR {CR} [720p]](https://uptobox.com/5g1tlfmk7n1u)
- [[Refrain] Megalo Box - 09 VOSTFR {CR} [1080p]](https://uptobox.com/00as2efw9y7x)
- [[Refrain] Megalo Box - 10 VOSTFR {ADN} [720p]](https://uptobox.com/9lcwyaon8ani)
- [[Refrain] Megalo Box - 10 VOSTFR {ADN} [1080p]](https://uptobox.com/y07fdxe2drm1)
- [[Refrain] Megalo Box - 10 VOSTFR {CR} [720p]](https://uptobox.com/277jnacsd5oj)
- [[Refrain] Megalo Box - 10 VOSTFR {CR} [1080p]](https://uptobox.com/x9r7upsmsxa7)
- [[Refrain] Megalo Box - 11 VOSTFR {ADN} [720p]](https://uptobox.com/cz4neet46bl5)
- [[Refrain] Megalo Box - 11 VOSTFR {ADN} [1080p]](https://uptobox.com/0d4cs320ibvq)
- [[Refrain] Megalo Box - 11 VOSTFR {CR} [720p]](https://uptobox.com/t4aoutppkwtw)
- [[Refrain] Megalo Box - 11 VOSTFR {CR} [1080p]](https://uptobox.com/h4ifzfmuxgq0)
- [[Refrain] Megalo Box - 12 VOSTFR {ADN} [720p]](https://uptobox.com/gba8sg16r3as)
- [[Refrain] Megalo Box - 12 VOSTFR {ADN} [1080p]](https://uptobox.com/l5unxjz7teg0)
- [[Refrain] Megalo Box - 12 VOSTFR {CR} [720p]](https://uptobox.com/9znjyohry9cd)
- [[Refrain] Megalo Box - 12 VOSTFR {CR} [1080p]](https://uptobox.com/8vbtk3vdfjj5)
- [[Refrain] Megalo Box - 13 VOSTFR {ADN} [720p]](https://uptobox.com/r0ji6hjhflxw)
- [[Refrain] Megalo Box - 13 VOSTFR {ADN} [1080p]](https://uptobox.com/umft3018dtvn)
- [[Refrain] Megalo Box - 13 VOSTFR {CR} [720p]](https://uptobox.com/owvahd5jdfd4)
- [[Refrain] Megalo Box - 13 VOSTFR {CR} [1080p]](https://uptobox.com/ltz38dvnjh5w)
