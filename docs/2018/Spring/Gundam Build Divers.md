# Gundam Build Divers

![Gundam Build Divers](https://cdn.myanimelist.net/images/anime/1701/90388l.jpg)

* Japanese:  ガンダムビルドダイバーズ

## Information

 - Type: TV
 - Episodes: 25
 - Status: Finished Airing
 - Aired: Apr 3, 2018 to Sep 25, 2018
 - Premiered: Spring 2018
 - Broadcast: Tuesdays at 17:55 (JST)
 - Producers: TV Tokyo, Sotsu
 - Licensors: None found, add some
 - Studios: Sunrise
 - Source: Original
 - Genres: Action, Mecha, Sci-Fi
 - Duration: 24 min. per ep.
 - Rating: G - All Ages


## Links

- [[Refrain] Gundam Build Divers - 00 VOSTFR [720p]](https://uptobox.com/b6uipf2vglsx)
- [[Refrain] Gundam Build Divers - 00 VOSTFR [1080p]](https://uptobox.com/7rsn1eo0iirf)
- [[Refrain] Gundam Build Divers - 01 VOSTFR [720p]](https://uptobox.com/ddoqgvwrhf7s)
- [[Refrain] Gundam Build Divers - 01 VOSTFR [1080p]](https://uptobox.com/u9zz31axj2va)
- [[Refrain] Gundam Build Divers - 02 VOSTFR [720p]](https://uptobox.com/fvyhe8l4dohk)
- [[Refrain] Gundam Build Divers - 02 VOSTFR [1080p]](https://uptobox.com/ai6t4gzc8g3o)
- [[Refrain] Gundam Build Divers - 03 VOSTFR [720p]](https://uptobox.com/mal1vjeodsu7)
- [[Refrain] Gundam Build Divers - 03 VOSTFR [1080p]](https://uptobox.com/a5avatupzxln)
- [[Refrain] Gundam Build Divers - 04 VOSTFR [720p]](https://uptobox.com/9ojtpqa9yvk3)
- [[Refrain] Gundam Build Divers - 04 VOSTFR [1080p]](https://uptobox.com/u63j2qkhbjox)
- [[Refrain] Gundam Build Divers - 05 VOSTFR [720p]](https://uptobox.com/ltrxyfwzgr61)
- [[Refrain] Gundam Build Divers - 05 VOSTFR [1080p]](https://uptobox.com/389racxvd5tu)
- [[Refrain] Gundam Build Divers - 06 VOSTFR [720p]](https://uptobox.com/g0nibtoz0x1g)
- [[Refrain] Gundam Build Divers - 06 VOSTFR [1080p]](https://uptobox.com/k1kryuz0ej0u)
- [[Refrain] Gundam Build Divers - 07 VOSTFR [720p]](https://uptobox.com/69rg6ha8py4e)
- [[Refrain] Gundam Build Divers - 07 VOSTFR [1080p]](https://uptobox.com/bmg4kvh3e2wt)
- [[Refrain] Gundam Build Divers - 08 VOSTFR [720p]](https://uptobox.com/jx76c3xxklkd)
- [[Refrain] Gundam Build Divers - 08 VOSTFR [1080p]](https://uptobox.com/zw9trwl5cmqa)
- [[Refrain] Gundam Build Divers - 09 VOSTFR [720p]](https://uptobox.com/wx6be0w2iwk4)
- [[Refrain] Gundam Build Divers - 09 VOSTFR [1080p]](https://uptobox.com/k4orz35bf4nl)
- [[Refrain] Gundam Build Divers - 10 VOSTFR [720p]](https://uptobox.com/plzp0irurlbj)
- [[Refrain] Gundam Build Divers - 10 VOSTFR [1080p]](https://uptobox.com/1e0rezxvnsoj)
- [[Refrain] Gundam Build Divers - 11 VOSTFR [720p]](https://uptobox.com/muo5gp2fjggs)
- [[Refrain] Gundam Build Divers - 11 VOSTFR [1080p]](https://uptobox.com/aj7s3n2a40kf)
- [[Refrain] Gundam Build Divers - 12 VOSTFR [720p]](https://uptobox.com/5s7fwfiv9urf)
- [[Refrain] Gundam Build Divers - 12 VOSTFR [1080p]](https://uptobox.com/ihfa09l66frd)
- [[Refrain] Gundam Build Divers - 13 VOSTFR [720p]](https://uptobox.com/1v2bmaohiyxl)
- [[Refrain] Gundam Build Divers - 13 VOSTFR [1080p]](https://uptobox.com/curvpycwwm2j)
- [[Refrain] Gundam Build Divers - 14 VOSTFR [720p]](https://uptobox.com/7le1um7rtone)
- [[Refrain] Gundam Build Divers - 14 VOSTFR [1080p]](https://uptobox.com/whcx1scgaggi)
- [[Refrain] Gundam Build Divers - 15 VOSTFR [720p]](https://uptobox.com/k1sfaa5eqs7x)
- [[Refrain] Gundam Build Divers - 15 VOSTFR [1080p]](https://uptobox.com/fz5h8xh90zw6)
- [[Refrain] Gundam Build Divers - 16 VOSTFR [720p]](https://uptobox.com/4yugug3rulzk)
- [[Refrain] Gundam Build Divers - 16 VOSTFR [1080p]](https://uptobox.com/xowub4xczah2)
- [[Refrain] Gundam Build Divers - 17 VOSTFR [720p]](https://uptobox.com/2qvcglk3vkuo)
- [[Refrain] Gundam Build Divers - 17 VOSTFR [1080p]](https://uptobox.com/lykgch9uovb2)
- [[Refrain] Gundam Build Divers - 18 VOSTFR [720p]](https://uptobox.com/do7ttzwh3y1i)
- [[Refrain] Gundam Build Divers - 18 VOSTFR [1080p]](https://uptobox.com/0ieanutl4fo3)
- [[Refrain] Gundam Build Divers - 19 VOSTFR [720p]](https://uptobox.com/1p3ocw3lq2xj)
- [[Refrain] Gundam Build Divers - 19 VOSTFR [1080p]](https://uptobox.com/cvxdipbys9tv)
- [[Refrain] Gundam Build Divers - 20 VOSTFR [720p]](https://uptobox.com/89qkimofy0fd)
- [[Refrain] Gundam Build Divers - 20 VOSTFR [1080p]](https://uptobox.com/u43rty1zjbyu)
- [[Refrain] Gundam Build Divers - 21 VOSTFR [720p]](https://uptobox.com/wtvxcud8sr48)
- [[Refrain] Gundam Build Divers - 21 VOSTFR [1080p]](https://uptobox.com/srabe9wo9u9m)
- [[Refrain] Gundam Build Divers - 22 VOSTFR [720p]](https://uptobox.com/6n59oa7r5mr7)
- [[Refrain] Gundam Build Divers - 22 VOSTFR [1080p]](https://uptobox.com/i6ly6er4umba)
- [[Refrain] Gundam Build Divers - 23 VOSTFR [720p]](https://uptobox.com/og0zi3yi42v1)
- [[Refrain] Gundam Build Divers - 23 VOSTFR [1080p]](https://uptobox.com/29y9fq1e4d8z)
- [[Refrain] Gundam Build Divers - 24 VOSTFR [720p]](https://uptobox.com/brium09ru899)
- [[Refrain] Gundam Build Divers - 24 VOSTFR [1080p]](https://uptobox.com/xc42ijl82kza)
- [[Refrain] Gundam Build Divers - 25 VOSTFR [720p]](https://uptobox.com/r0iwb1o8swj8)
- [[Refrain] Gundam Build Divers - 25 VOSTFR [1080p]](https://uptobox.com/wptf1upfzpzn)
