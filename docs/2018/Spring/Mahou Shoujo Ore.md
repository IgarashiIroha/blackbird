# Mahou Shoujo Ore

![Mahou Shoujo Ore](https://cdn.myanimelist.net/images/anime/1439/91893l.jpg)

* Japanese:  魔法少女 俺

## Information

 - Type: TV
 - Episodes: 12
 - Status: Finished Airing
 - Aired: Apr 2, 2018 to Jun 18, 2018
 - Premiered: Spring 2018
 - Broadcast: Mondays at 22:00 (JST)
 - Producers: Genco, AT-X, KlockWorx, My Theater D.D., Crunchyroll SC Anime Fund
 - Licensors: None found, add some
 - Studios: Pierrot Plus
 - Source: Web manga
 - Genres: Comedy, Fantasy, Magic
 - Duration: 23 min. per ep.
 - Rating: PG-13 - Teens 13 or older


## Links

- [[Refrain] Mahou Shoujo Ore - 01 VOSTFR [720p]](https://uptobox.com/gduilkpohq20)
- [[Refrain] Mahou Shoujo Ore - 01 VOSTFR [1080p]](https://uptobox.com/xx833t4jfh6v)
- [[Refrain] Mahou Shoujo Ore - 02 VOSTFR [720p]](https://uptobox.com/uex5n43rnq0a)
- [[Refrain] Mahou Shoujo Ore - 02 VOSTFR [1080p]](https://uptobox.com/u72vu7r5vm95)
- [[Refrain] Mahou Shoujo Ore - 03 VOSTFR [720p]](https://uptobox.com/ammwittn12nw)
- [[Refrain] Mahou Shoujo Ore - 03 VOSTFR [1080p]](https://uptobox.com/qfgnegg5sxk0)
- [[Refrain] Mahou Shoujo Ore - 04 VOSTFR [720p]](https://uptobox.com/llzd2pfuftye)
- [[Refrain] Mahou Shoujo Ore - 04 VOSTFR [1080p]](https://uptobox.com/0uaegnnq48vd)
- [[Refrain] Mahou Shoujo Ore - 05 VOSTFR [720p]](https://uptobox.com/8s5wse8ptprj)
- [[Refrain] Mahou Shoujo Ore - 05 VOSTFR [1080p]](https://uptobox.com/fz84q85canyp)
- [[Refrain] Mahou Shoujo Ore - 06 VOSTFR [720p]](https://uptobox.com/d6dcwptsaf48)
- [[Refrain] Mahou Shoujo Ore - 06 VOSTFR [1080p]](https://uptobox.com/5jy0ov1vlq2d)
- [[Refrain] Mahou Shoujo Ore - 07 VOSTFR [720p]](https://uptobox.com/wcouwj1zhnwb)
- [[Refrain] Mahou Shoujo Ore - 07 VOSTFR [1080p]](https://uptobox.com/satsdkljlayj)
- [[Refrain] Mahou Shoujo Ore - 08 VOSTFR [720p]](https://uptobox.com/6q7ax2gbilu3)
- [[Refrain] Mahou Shoujo Ore - 08 VOSTFR [1080p]](https://uptobox.com/1ed6c8q7qbiu)
- [[Refrain] Mahou Shoujo Ore - 09 VOSTFR [720p]](https://uptobox.com/u0f0aqwrkwn6)
- [[Refrain] Mahou Shoujo Ore - 09 VOSTFR [1080p]](https://uptobox.com/uy0mpk0oj2nl)
- [[Refrain] Mahou Shoujo Ore - 10 VOSTFR [720p]](https://uptobox.com/auauytv80aiy)
- [[Refrain] Mahou Shoujo Ore - 10 VOSTFR [1080p]](https://uptobox.com/kkfdj2uuftg6)
- [[Refrain] Mahou Shoujo Ore - 11 VOSTFR [720p]](https://uptobox.com/w446ax0i3lbq)
- [[Refrain] Mahou Shoujo Ore - 11 VOSTFR [1080p]](https://uptobox.com/ndapgf8p1znt)
- [[Refrain] Mahou Shoujo Ore - 12 VOSTFR [720p]](https://uptobox.com/lq4bd3k23qrj)
- [[Refrain] Mahou Shoujo Ore - 12 VOSTFR [1080p]](https://uptobox.com/5eyg750jv5tu)
