# Piano no Mori (TV)

![Piano no Mori (TV)](https://cdn.myanimelist.net/images/anime/1501/91916l.jpg)

* Japanese:  ピアノの森

## Information

 - Type: TV
 - Episodes: 12
 - Status: Finished Airing
 - Aired: Apr 9, 2018 to Jul 2, 2018
 - Premiered: Spring 2018
 - Broadcast: Mondays at 00:10 (JST)
 - Producers: NHK
 - Licensors: None found, add some
 - Studios: Gaina
 - Source: Manga
 - Genres: Adventure, Music, Comedy, Drama, School, Seinen
 - Duration: 25 min. per ep.
 - Rating: PG-13 - Teens 13 or older


## Links

- [[Refrain] Piano no Mori (2018) - 01 VF VOSTFR [720p]](https://uptobox.com/nfqkh8yzgrlq)
- [[Refrain] Piano no Mori (2018) - 01 VF VOSTFR [1080p]](https://uptobox.com/4zsj2niqxyjv)
- [[Refrain] Piano no Mori (2018) - 02 VF VOSTFR [720p]](https://uptobox.com/l7e7di5vhi3u)
- [[Refrain] Piano no Mori (2018) - 02 VF VOSTFR [1080p]](https://uptobox.com/1g248pdl0rbz)
- [[Refrain] Piano no Mori (2018) - 03 VF VOSTFR [720p]](https://uptobox.com/xgu1z762bw3x)
- [[Refrain] Piano no Mori (2018) - 03 VF VOSTFR [1080p]](https://uptobox.com/8gw29ruliopo)
- [[Refrain] Piano no Mori (2018) - 04 VF VOSTFR [720p]](https://uptobox.com/o6om6iblhbrw)
- [[Refrain] Piano no Mori (2018) - 04 VF VOSTFR [1080p]](https://uptobox.com/8hg2hcm7vr8f)
- [[Refrain] Piano no Mori (2018) - 05 VF VOSTFR [720p]](https://uptobox.com/rg0g95rwecoz)
- [[Refrain] Piano no Mori (2018) - 05 VF VOSTFR [1080p]](https://uptobox.com/gcgn7dmzki4q)
- [[Refrain] Piano no Mori (2018) - 06 VF VOSTFR [720p]](https://uptobox.com/66ell26ftymz)
- [[Refrain] Piano no Mori (2018) - 06 VF VOSTFR [1080p]](https://uptobox.com/h4ou217h5zq7)
- [[Refrain] Piano no Mori (2018) - 07 VF VOSTFR [720p]](https://uptobox.com/3vrhia7ybmtu)
- [[Refrain] Piano no Mori (2018) - 07 VF VOSTFR [1080p]](https://uptobox.com/uxwqgx8p77fv)
- [[Refrain] Piano no Mori (2018) - 08 VF VOSTFR [720p]](https://uptobox.com/t539apn8sxhs)
- [[Refrain] Piano no Mori (2018) - 08 VF VOSTFR [1080p]](https://uptobox.com/njdeqckq4395)
- [[Refrain] Piano no Mori (2018) - 09 VF VOSTFR [720p]](https://uptobox.com/i4idtsml8tyi)
- [[Refrain] Piano no Mori (2018) - 09 VF VOSTFR [1080p]](https://uptobox.com/fzbsk6f5rm7j)
- [[Refrain] Piano no Mori (2018) - 10 VF VOSTFR [720p]](https://uptobox.com/2sycy7o243f5)
- [[Refrain] Piano no Mori (2018) - 10 VF VOSTFR [1080p]](https://uptobox.com/in300neuftnc)
- [[Refrain] Piano no Mori (2018) - 11 VF VOSTFR [720p]](https://uptobox.com/ulmu5t6ajb9v)
- [[Refrain] Piano no Mori (2018) - 11 VF VOSTFR [1080p]](https://uptobox.com/h92s37uepovr)
- [[Refrain] Piano no Mori (2018) - 12 VF VOSTFR [720p]](https://uptobox.com/eu6nsf7rtl7o)
- [[Refrain] Piano no Mori (2018) - 12 VF VOSTFR [1080p]](https://uptobox.com/6py8be1xhxdt)
