# Lostorage Conflated WIXOSS

![Lostorage Conflated WIXOSS](https://cdn.myanimelist.net/images/anime/1558/90899l.jpg)

* Japanese:  Lostorage conflated WIXOSS

## Information

 - Type: TV
 - Episodes: 12
 - Status: Finished Airing
 - Aired: Apr 7, 2018 to Jun 23, 2018
 - Premiered: Spring 2018
 - Broadcast: Saturdays at 00:30 (JST)
 - Producers: None found, add some
 - Licensors: None found, add some
 - Studios: J.C.Staff
 - Source: Card game
 - Genres: Game, Psychological
 - Duration: 23 min. per ep.
 - Rating: R - 17+ (violence & profanity)


## Links

- [[Refrain] Lostorage Conflated WIXOSS - 01 VOSTFR [720p]](https://uptobox.com/wcv246qi153x)
- [[Refrain] Lostorage Conflated WIXOSS - 01 VOSTFR [1080p]](https://uptobox.com/al00kbza544f)
- [[Refrain] Lostorage Conflated WIXOSS - 02 VOSTFR [720p]](https://uptobox.com/n9e074di5jo2)
- [[Refrain] Lostorage Conflated WIXOSS - 02 VOSTFR [1080p]](https://uptobox.com/pc77txl08oy9)
- [[Refrain] Lostorage Conflated WIXOSS - 03 VOSTFR [720p]](https://uptobox.com/wgq0t8nxsewe)
- [[Refrain] Lostorage Conflated WIXOSS - 03 VOSTFR [1080p]](https://uptobox.com/zlk5oppb58t7)
- [[Refrain] Lostorage Conflated WIXOSS - 04 VOSTFR [720p]](https://uptobox.com/9kjdf9gdmbtk)
- [[Refrain] Lostorage Conflated WIXOSS - 04 VOSTFR [1080p]](https://uptobox.com/kz500gz7bwvs)
- [[Refrain] Lostorage Conflated WIXOSS - 05 VOSTFR [720p]](https://uptobox.com/k7o4gmxfy2bo)
- [[Refrain] Lostorage Conflated WIXOSS - 05 VOSTFR [1080p]](https://uptobox.com/ong9idkvdr5f)
- [[Refrain] Lostorage Conflated WIXOSS - 06 VOSTFR [720p]](https://uptobox.com/73phkofyqovk)
- [[Refrain] Lostorage Conflated WIXOSS - 06 VOSTFR [1080p]](https://uptobox.com/fsm8m5alwwm0)
- [[Refrain] Lostorage Conflated WIXOSS - 07 VOSTFR [720p]](https://uptobox.com/dzj96tgtt9o0)
- [[Refrain] Lostorage Conflated WIXOSS - 07 VOSTFR [1080p]](https://uptobox.com/ymncc9vuk9y8)
- [[Refrain] Lostorage Conflated WIXOSS - 08 VOSTFR [720p]](https://uptobox.com/11sg98n7rxe6)
- [[Refrain] Lostorage Conflated WIXOSS - 08 VOSTFR [1080p]](https://uptobox.com/hxy99by8jg98)
- [[Refrain] Lostorage Conflated WIXOSS - 09 VOSTFR [720p]](https://uptobox.com/v9aed5y59ve2)
- [[Refrain] Lostorage Conflated WIXOSS - 09 VOSTFR [1080p]](https://uptobox.com/cqeycxrzeqe4)
- [[Refrain] Lostorage Conflated WIXOSS - 10 VOSTFR [720p]](https://uptobox.com/iicfydrfi1ye)
- [[Refrain] Lostorage Conflated WIXOSS - 10 VOSTFR [1080p]](https://uptobox.com/ilnw4m9qiwad)
- [[Refrain] Lostorage Conflated WIXOSS - 11 VOSTFR [720p]](https://uptobox.com/k338rumsv8tn)
- [[Refrain] Lostorage Conflated WIXOSS - 11 VOSTFR [1080p]](https://uptobox.com/iu89hmq9whvz)
- [[Refrain] Lostorage Conflated WIXOSS - 12 VOSTFR [720p]](https://uptobox.com/7enhkj9eh1zw)
- [[Refrain] Lostorage Conflated WIXOSS - 12 VOSTFR [1080p]](https://uptobox.com/jvk8oe94a04q)
