# A.I.C.O.: Incarnation

![A.I.C.O.: Incarnation](https://cdn.myanimelist.net/images/anime/1921/91085l.jpg)

* Japanese:  A.I.C.O. -Incarnation-

## Information

 - Type: ONA
 - Episodes: 12
 - Status: Finished Airing
 - Aired: Mar 9, 2018
 - Producers: Bandai Visual, Lantis, Hakuhodo DY Music & Pictures, Yamasa
 - Licensors: None found, add some
 - Studios: Bones
 - Source: Original
 - Genres: Action, Sci-Fi
 - Duration: 24 min. per ep.
 - Rating: R - 17+ (violence & profanity)


## Links

- [[Refrain] A.I.C.O Incarnation - 01 VF VOSTFR [720p]](https://uptobox.com/6wiy6zgc5j8q)
- [[Refrain] A.I.C.O Incarnation - 01 VF VOSTFR [1080p]](https://uptobox.com/m5ojxthqx7yp)
- [[Refrain] A.I.C.O Incarnation - 02 VF VOSTFR [720p]](https://uptobox.com/wf81a650fpdv)
- [[Refrain] A.I.C.O Incarnation - 02 VF VOSTFR [1080p]](https://uptobox.com/b4qpexzekcxd)
- [[Refrain] A.I.C.O Incarnation - 03 VF VOSTFR [720p]](https://uptobox.com/874elpgoyn75)
- [[Refrain] A.I.C.O Incarnation - 03 VF VOSTFR [1080p]](https://uptobox.com/7w374esx29tv)
- [[Refrain] A.I.C.O Incarnation - 04 VF VOSTFR [720p]](https://uptobox.com/1hk5py9s6m7a)
- [[Refrain] A.I.C.O Incarnation - 04 VF VOSTFR [1080p]](https://uptobox.com/gd3ogzt4uuas)
- [[Refrain] A.I.C.O Incarnation - 05 VF VOSTFR [720p]](https://uptobox.com/x042qr727oq9)
- [[Refrain] A.I.C.O Incarnation - 05 VF VOSTFR [1080p]](https://uptobox.com/vd5wg4qy5jui)
- [[Refrain] A.I.C.O Incarnation - 06 VF VOSTFR [720p]](https://uptobox.com/pfpj2ykzjdpw)
- [[Refrain] A.I.C.O Incarnation - 06 VF VOSTFR [1080p]](https://uptobox.com/ziahiqr3qsn7)
- [[Refrain] A.I.C.O Incarnation - 07 VF VOSTFR [720p]](https://uptobox.com/ayc8gh6wvah1)
- [[Refrain] A.I.C.O Incarnation - 07 VF VOSTFR [1080p]](https://uptobox.com/rajncsqjp9up)
- [[Refrain] A.I.C.O Incarnation - 08 VF VOSTFR [720p]](https://uptobox.com/x0072vudhpxh)
- [[Refrain] A.I.C.O Incarnation - 08 VF VOSTFR [1080p]](https://uptobox.com/olqybs42nxla)
- [[Refrain] A.I.C.O Incarnation - 09 VF VOSTFR [720p]](https://uptobox.com/1hzb41cffe30)
- [[Refrain] A.I.C.O Incarnation - 09 VF VOSTFR [1080p]](https://uptobox.com/aydoa74bn7vu)
- [[Refrain] A.I.C.O Incarnation - 10 VF VOSTFR [720p]](https://uptobox.com/umu9g59awndm)
- [[Refrain] A.I.C.O Incarnation - 10 VF VOSTFR [1080p]](https://uptobox.com/lrub0mmtbqni)
- [[Refrain] A.I.C.O Incarnation - 11 VF VOSTFR [720p]](https://uptobox.com/q66rqv37m3hb)
- [[Refrain] A.I.C.O Incarnation - 11 VF VOSTFR [1080p]](https://uptobox.com/04rekpvstv37)
- [[Refrain] A.I.C.O Incarnation - 12 VF VOSTFR [720p]](https://uptobox.com/3wryfleiegah)
- [[Refrain] A.I.C.O Incarnation - 12 VF VOSTFR [1080p]](https://uptobox.com/7xc4rvd20pli)
