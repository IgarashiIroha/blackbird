# Oretacha Youkai Ningen G

![Oretacha Youkai Ningen G](https://cdn.myanimelist.net/images/anime/1954/91310l.jpg)

* Japanese:  俺たちゃ妖怪人間G

## Information

 - Type: TV
 - Episodes: 26
 - Status: Finished Airing
 - Aired: Apr 5, 2018 to Sep 27, 2018
 - Premiered: Spring 2018
 - Broadcast: Thursdays at 02:05 (JST)
 - Producers: None found, add some 
 - Licensors: None found, add some 
 - Studios: DLE 
 - Source: Original
 - Genres: Demons, Horror, Parody
 - Duration: 2 min. per ep.
 - Rating: PG-13 - Teens 13 or older


## Links

