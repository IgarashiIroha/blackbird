# Devils Line

![Devils Line](https://cdn.myanimelist.net/images/anime/1598/90817l.jpg)

* Japanese:  デビルズライン

## Information

 - Type: TV
 - Episodes: 12
 - Status: Finished Airing
 - Aired: Apr 7, 2018 to Jun 23, 2018
 - Premiered: Spring 2018
 - Broadcast: Saturdays at 23:30 (JST)
 - Producers: Kodansha, King Records
 - Licensors: Sentai Filmworks
 - Studios: Platinum Vision
 - Source: Manga
 - Genres: Action, Drama, Romance, Seinen, Supernatural, Vampire
 - Duration: 24 min. per ep.
 - Rating: R - 17+ (violence & profanity)


## Links

- [[Refrain] Devils Line - 01 VOSTFR [720p]](https://uptobox.com/8c991bdoc7id)
- [[Refrain] Devils Line - 01 VOSTFR [1080p]](https://uptobox.com/f8et19mkxp1g)
- [[Refrain] Devils Line - 01 VOSTFR {ADN} [720p]](https://uptobox.com/kieqh9kchyt4)
- [[Refrain] Devils Line - 01 VOSTFR {ADN} [1080p]](https://uptobox.com/ilft4jjj3dl0)
- [[Refrain] Devils Line - 02 VOSTFR [720p]](https://uptobox.com/53r4i67zl9i1)
- [[Refrain] Devils Line - 02 VOSTFR [1080p]](https://uptobox.com/ys73rk5rocvy)
- [[Refrain] Devils Line - 02 VOSTFR {ADN} [720p]](https://uptobox.com/bhc8c9nk7zky)
- [[Refrain] Devils Line - 02 VOSTFR {ADN} [1080p]](https://uptobox.com/lag98ild68is)
- [[Refrain] Devils Line - 03 VOSTFR [720p]](https://uptobox.com/il2hxjbsu78o)
- [[Refrain] Devils Line - 03 VOSTFR [1080p]](https://uptobox.com/rgnujylqjbyn)
- [[Refrain] Devils Line - 03 VOSTFR {ADN} [720p]](https://uptobox.com/97xq0ecq0fno)
- [[Refrain] Devils Line - 03 VOSTFR {ADN} [1080p]](https://uptobox.com/ln5eq9lmfsgl)
- [[Refrain] Devils Line - 04 VOSTFR [720p]](https://uptobox.com/s9mt5hswfmo2)
- [[Refrain] Devils Line - 04 VOSTFR [1080p]](https://uptobox.com/zz0hbxgvdvxl)
- [[Refrain] Devils Line - 04 VOSTFR {ADN} [720p]](https://uptobox.com/yf8znva6ht3j)
- [[Refrain] Devils Line - 04 VOSTFR {ADN} [1080p]](https://uptobox.com/prqioian8jto)
- [[Refrain] Devils Line - 05 VOSTFR [720p]](https://uptobox.com/q57jaf4z8k9f)
- [[Refrain] Devils Line - 05 VOSTFR [1080p]](https://uptobox.com/bcuhwqmqbcyr)
- [[Refrain] Devils Line - 05 VOSTFR {ADN} [720p]](https://uptobox.com/yrnhr32ogy9k)
- [[Refrain] Devils Line - 05 VOSTFR {ADN} [1080p]](https://uptobox.com/anble9edzjaz)
- [[Refrain] Devils Line - 06 VOSTFR [720p]](https://uptobox.com/1aamefunfu1f)
- [[Refrain] Devils Line - 06 VOSTFR [1080p]](https://uptobox.com/en5zy516skkz)
- [[Refrain] Devils Line - 06 VOSTFR {ADN} [720p]](https://uptobox.com/ny8aqidpr0l6)
- [[Refrain] Devils Line - 06 VOSTFR {ADN} [1080p]](https://uptobox.com/kn16qrfxdcii)
- [[Refrain] Devils Line - 07 VOSTFR [720p]](https://uptobox.com/yzdgyy83loaj)
- [[Refrain] Devils Line - 07 VOSTFR [1080p]](https://uptobox.com/k8savfis2aqm)
- [[Refrain] Devils Line - 07 VOSTFR {ADN} [720p]](https://uptobox.com/l5wq0pvouxbf)
- [[Refrain] Devils Line - 07 VOSTFR {ADN} [1080p]](https://uptobox.com/xj2g36wggpbj)
- [[Refrain] Devils Line - 08 VOSTFR [720p]](https://uptobox.com/2h95u25tkwsh)
- [[Refrain] Devils Line - 08 VOSTFR [1080p]](https://uptobox.com/dr1l7fso4ttk)
- [[Refrain] Devils Line - 08 VOSTFR {ADN} [720p]](https://uptobox.com/dllmt9qges98)
- [[Refrain] Devils Line - 08 VOSTFR {ADN} [1080p]](https://uptobox.com/2ejz78s59rrt)
- [[Refrain] Devils Line - 09 VOSTFR [720p]](https://uptobox.com/h3xawg2wayqs)
- [[Refrain] Devils Line - 09 VOSTFR [1080p]](https://uptobox.com/t6o1ojqq9cjc)
- [[Refrain] Devils Line - 09 VOSTFR {ADN} [720p]](https://uptobox.com/2yjb2jzx5va6)
- [[Refrain] Devils Line - 09 VOSTFR {ADN} [1080p]](https://uptobox.com/1nv58gsyuvra)
- [[Refrain] Devils Line - 10 VOSTFR [720p]](https://uptobox.com/isj2zj202m10)
- [[Refrain] Devils Line - 10 VOSTFR [1080p]](https://uptobox.com/3rjc9u8ky0yr)
- [[Refrain] Devils Line - 10 VOSTFR {ADN} [720p]](https://uptobox.com/j7oqf5vyxhcm)
- [[Refrain] Devils Line - 10 VOSTFR {ADN} [1080p]](https://uptobox.com/z2kaq78war5d)
- [[Refrain] Devils Line - 11 VOSTFR [720p]](https://uptobox.com/tj7z20cxtkz9)
- [[Refrain] Devils Line - 11 VOSTFR [1080p]](https://uptobox.com/lqiwchqzflxf)
- [[Refrain] Devils Line - 11 VOSTFR {ADN} [720p]](https://uptobox.com/qvd6q25yiw1v)
- [[Refrain] Devils Line - 11 VOSTFR {ADN} [1080p]](https://uptobox.com/6j4eycz9du7u)
- [[Refrain] Devils Line - 12 VOSTFR [720p]](https://uptobox.com/5gdfd7rtqjrg)
- [[Refrain] Devils Line - 12 VOSTFR [1080p]](https://uptobox.com/w354pqk4il4f)
- [[Refrain] Devils Line - 12 VOSTFR {ADN} [720p]](https://uptobox.com/jk9l8nrjhclp)
- [[Refrain] Devils Line - 12 VOSTFR {ADN} [1080p]](https://uptobox.com/1uh288xw5bb1)
- [[Refrain] Devils Line - 13 VOSTFR {ADN} [720p]](https://uptobox.com/vc36gttiljnf)
- [[Refrain] Devils Line - 13 VOSTFR {ADN} [1080p]](https://uptobox.com/3u27cnj08apa)
