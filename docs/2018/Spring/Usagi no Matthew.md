# Usagi no Matthew

![Usagi no Matthew](https://cdn.myanimelist.net/images/anime/1003/95068l.jpg)

* Japanese:  うさぎのマシュー

## Information

 - Type: TV
 - Episodes: Unknown
 - Status: Currently Airing
 - Aired: Apr 5, 2018 to ?
 - Premiered: Spring 2018
 - Broadcast: Thursdays at 21:54 (JST)
 - Producers: None found, add some 
 - Licensors: None found, add some 
 - Studios: Kachidoki Studio 
 - Source: Manga
 - Genres: Slice of Life
 - Duration: 1 min.
 - Rating: G - All Ages


## Links

