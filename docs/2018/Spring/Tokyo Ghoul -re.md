# Tokyo Ghoul:re

![Tokyo Ghoul:re](https://cdn.myanimelist.net/images/anime/1063/95086l.jpg)

* Japanese:  東京喰種トーキョーグール：re

## Information

 - Type: TV
 - Episodes: 12
 - Status: Finished Airing
 - Aired: Apr 3, 2018 to Jun 19, 2018
 - Premiered: Spring 2018
 - Broadcast: Tuesdays at 23:00 (JST)
 - Producers: Marvelous, TC Entertainment
 - Licensors: Funimation
 - Studios: Studio Pierrot, Pierrot Plus
 - Source: Manga
 - Genres: Action, Psychological, Supernatural, Mystery, Drama, Horror, Seinen
 - Duration: 24 min. per ep.
 - Rating: R - 17+ (violence & profanity)


## Links

- [[Refrain] Tokyo Ghoul-re - 01 VOSTFR [720p]](https://uptobox.com/5an8o18z9pfz)
- [[Refrain] Tokyo Ghoul-re - 01 VOSTFR [1080p]](https://uptobox.com/yil0j0qicu5m)
- [[Refrain] Tokyo Ghoul-re - 02 VOSTFR [720p]](https://uptobox.com/yvg39odozjxz)
- [[Refrain] Tokyo Ghoul-re - 02 VOSTFR [1080p]](https://uptobox.com/wakzuisa2rir)
- [[Refrain] Tokyo Ghoul-re - 03 VOSTFR [720p]](https://uptobox.com/v6ucgwhviw4z)
- [[Refrain] Tokyo Ghoul-re - 03 VOSTFR [1080p]](https://uptobox.com/qwhx2mtreowv)
- [[Refrain] Tokyo Ghoul-re - 04 VOSTFR [720p]](https://uptobox.com/q3xbczzsqw62)
- [[Refrain] Tokyo Ghoul-re - 04 VOSTFR [1080p]](https://uptobox.com/4x45ruj2xcbh)
- [[Refrain] Tokyo Ghoul-re - 05 VOSTFR [720p]](https://uptobox.com/nkbtsnwx11r5)
- [[Refrain] Tokyo Ghoul-re - 05 VOSTFR [1080p]](https://uptobox.com/517b1dvhcmrz)
- [[Refrain] Tokyo Ghoul-re - 06 VOSTFR [720p]](https://uptobox.com/nw8ynxo3bu9t)
- [[Refrain] Tokyo Ghoul-re - 06 VOSTFR [1080p]](https://uptobox.com/uvtox63zkenu)
- [[Refrain] Tokyo Ghoul-re - 07 VOSTFR [720p]](https://uptobox.com/osqqtptwsonr)
- [[Refrain] Tokyo Ghoul-re - 07 VOSTFR [1080p]](https://uptobox.com/qej7ws0pytgv)
- [[Refrain] Tokyo Ghoul-re - 08 VOSTFR [720p]](https://uptobox.com/szh8gk468wh1)
- [[Refrain] Tokyo Ghoul-re - 08 VOSTFR [1080p]](https://uptobox.com/iurn1nqwqsws)
- [[Refrain] Tokyo Ghoul-re - 09 VOSTFR [720p]](https://uptobox.com/rqn19im47y3e)
- [[Refrain] Tokyo Ghoul-re - 09 VOSTFR [1080p]](https://uptobox.com/b3itmf3bykag)
- [[Refrain] Tokyo Ghoul-re - 10 VOSTFR [720p]](https://uptobox.com/nhp7v3gyzlwa)
- [[Refrain] Tokyo Ghoul-re - 10 VOSTFR [1080p]](https://uptobox.com/8i0tnhv5enyf)
- [[Refrain] Tokyo Ghoul-re - 11 VOSTFR [720p]](https://uptobox.com/e3cnb1sc2114)
- [[Refrain] Tokyo Ghoul-re - 11 VOSTFR [1080p]](https://uptobox.com/gvofe9p30zee)
- [[Refrain] Tokyo Ghoul-re - 12 VOSTFR [720p]](https://uptobox.com/ts7o98hwuuki)
- [[Refrain] Tokyo Ghoul-re - 12 VOSTFR [1080p]](https://uptobox.com/u34pxmqsr9kx)
