# Hoozuki no Reitetsu 2nd Season: Sono Ni

![Hoozuki no Reitetsu 2nd Season: Sono Ni](https://cdn.myanimelist.net/images/anime/1113/92465l.jpg)

* Japanese:  鬼灯の冷徹 第弐期その弐

## Information

 - Type: TV
 - Episodes: 13
 - Status: Finished Airing
 - Aired: Apr 8, 2018 to Jul 1, 2018
 - Premiered: Spring 2018
 - Broadcast: Sundays at 01:00 (JST)
 - Producers: None found, add some
 - Licensors: Sentai Filmworks
 - Studios: Studio Deen
 - Source: Manga
 - Genres: Comedy, Demons, Fantasy, Seinen, Supernatural
 - Duration: 23 min. per ep.
 - Rating: PG-13 - Teens 13 or older


## Links

- [[Refrain] Hoozuki no Reitetsu 2nd Season - 13 VOSTFR {ADN} [720p]](https://uptobox.com/5jxmvunlp838)
- [[Refrain] Hoozuki no Reitetsu 2nd Season - 13 VOSTFR {ADN} [1080p]](https://uptobox.com/s6x7ulwkxzg0)
- [[Refrain] Hoozuki no Reitetsu 2nd Season - 14 VOSTFR {ADN} [720p]](https://uptobox.com/cfyssmu052vl)
- [[Refrain] Hoozuki no Reitetsu 2nd Season - 14 VOSTFR {ADN} [1080p]](https://uptobox.com/ctk6a24qkjkv)
- [[Refrain] Hoozuki no Reitetsu 2nd Season - 15 VOSTFR {ADN} [720p]](https://uptobox.com/1qsj3o1wdra8)
- [[Refrain] Hoozuki no Reitetsu 2nd Season - 15 VOSTFR {ADN} [1080p]](https://uptobox.com/vodwgwj7f5yk)
- [[Refrain] Hoozuki no Reitetsu 2nd Season - 16 VOSTFR {ADN} [720p]](https://uptobox.com/58dey2hgr4k9)
- [[Refrain] Hoozuki no Reitetsu 2nd Season - 16 VOSTFR {ADN} [1080p]](https://uptobox.com/c4n54bpqxmsb)
- [[Refrain] Hoozuki no Reitetsu 2nd Season - 17 VOSTFR {ADN} [720p]](https://uptobox.com/dyfta3e0qrcd)
- [[Refrain] Hoozuki no Reitetsu 2nd Season - 17 VOSTFR {ADN} [1080p]](https://uptobox.com/bl2n44cjut1h)
- [[Refrain] Hoozuki no Reitetsu 2nd Season - 18 VOSTFR {ADN} [720p]](https://uptobox.com/1873ozovhjq3)
- [[Refrain] Hoozuki no Reitetsu 2nd Season - 18 VOSTFR {ADN} [1080p]](https://uptobox.com/1sxsrodglfpl)
- [[Refrain] Hoozuki no Reitetsu 2nd Season - 19 VOSTFR {ADN} [720p]](https://uptobox.com/mrjsibm2dsx2)
- [[Refrain] Hoozuki no Reitetsu 2nd Season - 19 VOSTFR {ADN} [1080p]](https://uptobox.com/pto6emwr8hr8)
- [[Refrain] Hoozuki no Reitetsu 2nd Season - 20 VOSTFR {ADN} [720p]](https://uptobox.com/4tjjh84x94v7)
- [[Refrain] Hoozuki no Reitetsu 2nd Season - 20 VOSTFR {ADN} [1080p]](https://uptobox.com/f737pj20xk3i)
- [[Refrain] Hoozuki no Reitetsu 2nd Season - 21 VOSTFR {ADN} [720p]](https://uptobox.com/1hbcwzacv22n)
- [[Refrain] Hoozuki no Reitetsu 2nd Season - 21 VOSTFR {ADN} [1080p]](https://uptobox.com/7s9nc1yo7t7f)
- [[Refrain] Hoozuki no Reitetsu 2nd Season - 22 VOSTFR {ADN} [720p]](https://uptobox.com/uk21m944jmqz)
- [[Refrain] Hoozuki no Reitetsu 2nd Season - 22 VOSTFR {ADN} [1080p]](https://uptobox.com/ore4ioahhfpe)
- [[Refrain] Hoozuki no Reitetsu 2nd Season - 23 VOSTFR {ADN} [720p]](https://uptobox.com/ld2q3b61r43u)
- [[Refrain] Hoozuki no Reitetsu 2nd Season - 23 VOSTFR {ADN} [1080p]](https://uptobox.com/l8e6jwz4uhxi)
- [[Refrain] Hoozuki no Reitetsu 2nd Season - 24 VOSTFR {ADN} [720p]](https://uptobox.com/c99c1ye02a71)
- [[Refrain] Hoozuki no Reitetsu 2nd Season - 24 VOSTFR {ADN} [1080p]](https://uptobox.com/e3ias9h3bobc)
- [[Refrain] Hoozuki no Reitetsu 2nd Season - 25 VOSTFR {ADN} [720p]](https://uptobox.com/4rzty914nceu)
- [[Refrain] Hoozuki no Reitetsu 2nd Season - 25 VOSTFR {ADN} [1080p]](https://uptobox.com/1t2tik2x9acs)
