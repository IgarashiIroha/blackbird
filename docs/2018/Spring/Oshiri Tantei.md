# Oshiri Tantei

![Oshiri Tantei](https://cdn.myanimelist.net/images/anime/1215/92909l.jpg)

* Japanese:  おしりたんてい

## Information

 - Type: TV
 - Episodes: Unknown
 - Status: Currently Airing
 - Aired: May 3, 2018 to ?
 - Premiered: Spring 2018
 - Broadcast: Unknown
 - Producers: None found, add some 
 - Licensors: None found, add some 
 - Studios: Toei Animation 
 - Source: Picture book
 - Genres: Mystery, Comedy, Kids, Fantasy
 - Duration: 19 min.
 - Rating: PG - Children


## Links

