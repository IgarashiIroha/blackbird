# Persona 5 the Animation

![Persona 5 the Animation](https://cdn.myanimelist.net/images/anime/1829/92056l.jpg)

* Japanese:  PERSONA5 the Animation

## Information

 - Type: TV
 - Episodes: 26
 - Status: Finished Airing
 - Aired: Apr 8, 2018 to Sep 30, 2018
 - Premiered: Spring 2018
 - Broadcast: Sundays at 00:30 (JST)
 - Producers: Aniplex, A-1 Pictures, Movic, Atlus, Sammy, Sonilude
 - Licensors: Aniplex of America
 - Studios: CloverWorks
 - Source: Game
 - Genres: Action, Supernatural, Fantasy
 - Duration: 24 min. per ep.
 - Rating: R - 17+ (violence & profanity)


## Links

- [[Refrain] Persona 5 the Animation - 01 VOSTFR [720p]](https://uptobox.com/ykd8xhxomvkh)
- [[Refrain] Persona 5 the Animation - 01 VOSTFR [1080p]](https://uptobox.com/h380r7britke)
- [[Refrain] Persona 5 the Animation - 02 VOSTFR [720p]](https://uptobox.com/1agtejzsgulu)
- [[Refrain] Persona 5 the Animation - 02 VOSTFR [1080p]](https://uptobox.com/6fkfb09zbkt1)
- [[Refrain] Persona 5 the Animation - 03 VOSTFR [720p]](https://uptobox.com/kuxfzbpldd1u)
- [[Refrain] Persona 5 the Animation - 03 VOSTFR [1080p]](https://uptobox.com/kzd1hl7vbqlu)
- [[Refrain] Persona 5 the Animation - 04 VOSTFR [720p]](https://uptobox.com/0oyd71x5glw8)
- [[Refrain] Persona 5 the Animation - 04 VOSTFR [1080p]](https://uptobox.com/jtdsuhfb42ns)
- [[Refrain] Persona 5 the Animation - 05 VOSTFR [720p]](https://uptobox.com/4wec44b6szib)
- [[Refrain] Persona 5 the Animation - 05 VOSTFR [1080p]](https://uptobox.com/fxlatukgm662)
- [[Refrain] Persona 5 the Animation - 06 VOSTFR [720p]](https://uptobox.com/jnxm3xpx8ngz)
- [[Refrain] Persona 5 the Animation - 06 VOSTFR [1080p]](https://uptobox.com/ybtf7kt89lbc)
- [[Refrain] Persona 5 the Animation - 07 VOSTFR [720p]](https://uptobox.com/n4iye0qkex5y)
- [[Refrain] Persona 5 the Animation - 07 VOSTFR [1080p]](https://uptobox.com/feyhq80o9b6k)
- [[Refrain] Persona 5 the Animation - 08 VOSTFR [720p]](https://uptobox.com/8zvowj2ln34b)
- [[Refrain] Persona 5 the Animation - 08 VOSTFR [1080p]](https://uptobox.com/o6akusn8851z)
- [[Refrain] Persona 5 the Animation - 09 VOSTFR [720p]](https://uptobox.com/vl89iyr8hicu)
- [[Refrain] Persona 5 the Animation - 09 VOSTFR [1080p]](https://uptobox.com/ptgoa9faj7r4)
- [[Refrain] Persona 5 the Animation - 10 VOSTFR [720p]](https://uptobox.com/uprbv0x3wcis)
- [[Refrain] Persona 5 the Animation - 10 VOSTFR [1080p]](https://uptobox.com/a0kzk85vlstx)
- [[Refrain] Persona 5 the Animation - 11 VOSTFR [720p]](https://uptobox.com/cwzkm66nx2ad)
- [[Refrain] Persona 5 the Animation - 11 VOSTFR [1080p]](https://uptobox.com/uqwmk5ixmquc)
- [[Refrain] Persona 5 the Animation - 12 VOSTFR [720p]](https://uptobox.com/xzrgw9u0led8)
- [[Refrain] Persona 5 the Animation - 12 VOSTFR [1080p]](https://uptobox.com/kbqwz6z4tr65)
- [[Refrain] Persona 5 the Animation - 13 VOSTFR [720p]](https://uptobox.com/044xy6mu79jr)
- [[Refrain] Persona 5 the Animation - 13 VOSTFR [1080p]](https://uptobox.com/cfkiwpga45ka)
- [[Refrain] Persona 5 the Animation - 14 VOSTFR [720p]](https://uptobox.com/wne5fhja3629)
- [[Refrain] Persona 5 the Animation - 14 VOSTFR [1080p]](https://uptobox.com/zt7lc7dk4pxj)
- [[Refrain] Persona 5 the Animation - 15 VOSTFR [720p]](https://uptobox.com/rpif3beotmun)
- [[Refrain] Persona 5 the Animation - 15 VOSTFR [1080p]](https://uptobox.com/0kmb3zojhvcm)
- [[Refrain] Persona 5 the Animation - 16 VOSTFR [720p]](https://uptobox.com/ceringcqfeld)
- [[Refrain] Persona 5 the Animation - 16 VOSTFR [1080p]](https://uptobox.com/sfzfaa9069vw)
- [[Refrain] Persona 5 the Animation - 17 VOSTFR [720p]](https://uptobox.com/6z35yxh8cue4)
- [[Refrain] Persona 5 the Animation - 17 VOSTFR [1080p]](https://uptobox.com/wcda7wbfkzeh)
- [[Refrain] Persona 5 the Animation - 18 VOSTFR [720p]](https://uptobox.com/pxt8bwq7k5ub)
- [[Refrain] Persona 5 the Animation - 18 VOSTFR [1080p]](https://uptobox.com/hbq8s2lyohsn)
- [[Refrain] Persona 5 the Animation - 19 VOSTFR [720p]](https://uptobox.com/jhaqq42hg9qo)
- [[Refrain] Persona 5 the Animation - 19 VOSTFR [1080p]](https://uptobox.com/ah565ujgmen7)
- [[Refrain] Persona 5 the Animation - 20 VOSTFR [720p]](https://uptobox.com/qntjks9m0mdm)
- [[Refrain] Persona 5 the Animation - 20 VOSTFR [1080p]](https://uptobox.com/miyjdnwgu7q4)
- [[Refrain] Persona 5 the Animation - 21 VOSTFR [720p]](https://uptobox.com/apqb22yd3tcw)
- [[Refrain] Persona 5 the Animation - 21 VOSTFR [1080p]](https://uptobox.com/u922bwyboc6b)
- [[Refrain] Persona 5 the Animation - 22 VOSTFR [720p]](https://uptobox.com/pgleny1r93zu)
- [[Refrain] Persona 5 the Animation - 22 VOSTFR [1080p]](https://uptobox.com/tw522ov0dno0)
- [[Refrain] Persona 5 the Animation - 23 VOSTFR [720p]](https://uptobox.com/1t4w8kl09h3j)
- [[Refrain] Persona 5 the Animation - 23 VOSTFR [1080p]](https://uptobox.com/ybltqm9yd3ib)
- [[Refrain] Persona 5 the Animation - 24 VOSTFR [720p]](https://uptobox.com/injg51pipwfx)
- [[Refrain] Persona 5 the Animation - 24 VOSTFR [1080p]](https://uptobox.com/xxa2x58ydf3p)
- [[Refrain] Persona 5 the Animation - 25 VOSTFR [720p]](https://uptobox.com/1b4h1mrzbweg)
- [[Refrain] Persona 5 the Animation - 25 VOSTFR [1080p]](https://uptobox.com/c4yihtac81qu)
- [[Refrain] Persona 5 the Animation - 26 VOSTFR [720p]](https://uptobox.com/sbma5hf5leox)
- [[Refrain] Persona 5 the Animation - 26 VOSTFR [1080p]](https://uptobox.com/clpvtf3won0z)
- [[Refrain] Persona 5 the Animation - 27 Special VOSTFR [720p]](https://uptobox.com/mpe1jr0yr9we)
- [[Refrain] Persona 5 the Animation - 27 Special VOSTFR [1080p]](https://uptobox.com/3p8r2m7vn5cm)
- [[Refrain] Persona 5 the Animation - 28 Special VOSTFR [1080p]](https://uptobox.com/n59ijsz1b5tc)
- [[Refrain] Persona 5 the Animation - 28 Special VOSTFR [720p]](https://uptobox.com/i4gqhmwayv92)
