# Aggressive Retsuko (ONA)

![Aggressive Retsuko (ONA)](https://cdn.myanimelist.net/images/anime/12/89288l.jpg)

* Japanese:  アグレッシブ烈子

## Information

 - Type: ONA
 - Episodes: 10
 - Status: Finished Airing
 - Aired: Apr 20, 2018
 - Producers: Sanrio
 - Licensors: None found, add some
 - Studios: Fanworks
 - Source: Original
 - Genres: Slice of Life, Comedy
 - Duration: 15 min. per ep.
 - Rating: PG-13 - Teens 13 or older


## Links

- [[Refrain] Aggressive Retsuko (ONA) - 01 VF VOSTFR [720p]](https://uptobox.com/1kfkhzda9zt7)
- [[Refrain] Aggressive Retsuko (ONA) - 01 VF VOSTFR [1080p]](https://uptobox.com/i6vs7juwcg0u)
- [[Refrain] Aggressive Retsuko (ONA) - 02 VF VOSTFR [720p]](https://uptobox.com/su72nat6032b)
- [[Refrain] Aggressive Retsuko (ONA) - 02 VF VOSTFR [1080p]](https://uptobox.com/mtzuzzt75uq4)
- [[Refrain] Aggressive Retsuko (ONA) - 03 VF VOSTFR [720p]](https://uptobox.com/ntog8sg9vedp)
- [[Refrain] Aggressive Retsuko (ONA) - 03 VF VOSTFR [1080p]](https://uptobox.com/sipa3a4kh2vg)
- [[Refrain] Aggressive Retsuko (ONA) - 04 VF VOSTFR [720p]](https://uptobox.com/507tqv9pu9wk)
- [[Refrain] Aggressive Retsuko (ONA) - 04 VF VOSTFR [1080p]](https://uptobox.com/pi34x0xi0yem)
- [[Refrain] Aggressive Retsuko (ONA) - 05 VF VOSTFR [720p]](https://uptobox.com/2foxyycgw58k)
- [[Refrain] Aggressive Retsuko (ONA) - 05 VF VOSTFR [1080p]](https://uptobox.com/obdjn9rk2by8)
- [[Refrain] Aggressive Retsuko (ONA) - 06 VF VOSTFR [720p]](https://uptobox.com/io8mbht56472)
- [[Refrain] Aggressive Retsuko (ONA) - 06 VF VOSTFR [1080p]](https://uptobox.com/jwkpf9i7tk70)
- [[Refrain] Aggressive Retsuko (ONA) - 07 VF VOSTFR [720p]](https://uptobox.com/ne8fo1nsxu42)
- [[Refrain] Aggressive Retsuko (ONA) - 07 VF VOSTFR [1080p]](https://uptobox.com/9r37504l8r5y)
- [[Refrain] Aggressive Retsuko (ONA) - 08 VF VOSTFR [720p]](https://uptobox.com/abigs3jsnbvp)
- [[Refrain] Aggressive Retsuko (ONA) - 08 VF VOSTFR [1080p]](https://uptobox.com/pg5grfy1tgtt)
- [[Refrain] Aggressive Retsuko (ONA) - 09 VF VOSTFR [720p]](https://uptobox.com/iltx2ltg32t9)
- [[Refrain] Aggressive Retsuko (ONA) - 09 VF VOSTFR [1080p]](https://uptobox.com/8ed97hdgwms9)
- [[Refrain] Aggressive Retsuko (ONA) - 10 VF VOSTFR [720p]](https://uptobox.com/vssd1fs698z1)
- [[Refrain] Aggressive Retsuko (ONA) - 10 VF VOSTFR [1080p]](https://uptobox.com/aq07k10hqimt)
