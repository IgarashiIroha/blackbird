# Akkun to Kanojo

![Akkun to Kanojo](https://cdn.myanimelist.net/images/anime/1024/93549l.jpg)

* Japanese:  あっくんとカノジョ

## Information

 - Type: TV
 - Episodes: 25
 - Status: Finished Airing
 - Aired: Apr 6, 2018 to Sep 21, 2018
 - Premiered: Spring 2018
 - Broadcast: Fridays at 22:30 (JST)
 - Producers: None found, add some
 - Licensors: None found, add some
 - Studios: Yumeta Company
 - Source: Manga
 - Genres: Comedy, Josei, Romance, School
 - Duration: 3 min. per ep.
 - Rating: PG-13 - Teens 13 or older


## Links

- [[Refrain] Akkun to Kanojo - 01 VOSTFR [720p]](https://uptobox.com/0wzg8w0q1lpf)
- [[Refrain] Akkun to Kanojo - 01 VOSTFR [1080p]](https://uptobox.com/m1ew4xp1zklj)
- [[Refrain] Akkun to Kanojo - 02 VOSTFR [720p]](https://uptobox.com/goewr1usoh00)
- [[Refrain] Akkun to Kanojo - 02 VOSTFR [1080p]](https://uptobox.com/1spu2wbaftpw)
- [[Refrain] Akkun to Kanojo - 03 VOSTFR [720p]](https://uptobox.com/1jwg53ikz1kv)
- [[Refrain] Akkun to Kanojo - 03 VOSTFR [1080p]](https://uptobox.com/3yxespabai14)
- [[Refrain] Akkun to Kanojo - 04 VOSTFR [720p]](https://uptobox.com/18j4d2qzh2lx)
- [[Refrain] Akkun to Kanojo - 04 VOSTFR [1080p]](https://uptobox.com/wrwkiq3c12am)
- [[Refrain] Akkun to Kanojo - 05 VOSTFR [720p]](https://uptobox.com/i7j71wfo2cy3)
- [[Refrain] Akkun to Kanojo - 05 VOSTFR [1080p]](https://uptobox.com/avpk4lv3sttg)
- [[Refrain] Akkun to Kanojo - 06 VOSTFR [720p]](https://uptobox.com/vs71quygef0a)
- [[Refrain] Akkun to Kanojo - 06 VOSTFR [1080p]](https://uptobox.com/n0ew6viyv9ob)
- [[Refrain] Akkun to Kanojo - 07 VOSTFR [720p]](https://uptobox.com/2cpkwritq8kl)
- [[Refrain] Akkun to Kanojo - 07 VOSTFR [1080p]](https://uptobox.com/3wh5av9nykd4)
- [[Refrain] Akkun to Kanojo - 08 VOSTFR [720p]](https://uptobox.com/z2b23dbxpaev)
- [[Refrain] Akkun to Kanojo - 08 VOSTFR [1080p]](https://uptobox.com/gy34rita7wtq)
- [[Refrain] Akkun to Kanojo - 09 VOSTFR [720p]](https://uptobox.com/52ggiirgmqyk)
- [[Refrain] Akkun to Kanojo - 09 VOSTFR [1080p]](https://uptobox.com/7gixvao4dcnq)
- [[Refrain] Akkun to Kanojo - 10 VOSTFR [720p]](https://uptobox.com/hi0r1uv1spuo)
- [[Refrain] Akkun to Kanojo - 10 VOSTFR [1080p]](https://uptobox.com/qpvexq6hyqz7)
- [[Refrain] Akkun to Kanojo - 11 VOSTFR [720p]](https://uptobox.com/78uehbyvtm8g)
- [[Refrain] Akkun to Kanojo - 11 VOSTFR [1080p]](https://uptobox.com/s3ky1w0kcdhl)
- [[Refrain] Akkun to Kanojo - 12 VOSTFR [720p]](https://uptobox.com/bycf40n2w6h1)
- [[Refrain] Akkun to Kanojo - 12 VOSTFR [1080p]](https://uptobox.com/y8mxynma0nyp)
- [[Refrain] Akkun to Kanojo - 13 VOSTFR [720p]](https://uptobox.com/5yq9l0gf4l8c)
- [[Refrain] Akkun to Kanojo - 13 VOSTFR [1080p]](https://uptobox.com/tq01h43kqw8u)
- [[Refrain] Akkun to Kanojo - 14 VOSTFR [720p]](https://uptobox.com/dreueiosukly)
- [[Refrain] Akkun to Kanojo - 14 VOSTFR [1080p]](https://uptobox.com/p48fytsmdqu5)
- [[Refrain] Akkun to Kanojo - 15 VOSTFR [720p]](https://uptobox.com/6e0sgfgxlorm)
- [[Refrain] Akkun to Kanojo - 15 VOSTFR [1080p]](https://uptobox.com/8g1k62m350ja)
- [[Refrain] Akkun to Kanojo - 16 VOSTFR [720p]](https://uptobox.com/ugtz0e7e43zs)
- [[Refrain] Akkun to Kanojo - 16 VOSTFR [1080p]](https://uptobox.com/sewyyovpf5rc)
- [[Refrain] Akkun to Kanojo - 17 VOSTFR [720p]](https://uptobox.com/yhewa8zj4ie5)
- [[Refrain] Akkun to Kanojo - 17 VOSTFR [1080p]](https://uptobox.com/r4xgu5d5dx2f)
- [[Refrain] Akkun to Kanojo - 18 VOSTFR [720p]](https://uptobox.com/oezatkk4t1pa)
- [[Refrain] Akkun to Kanojo - 18 VOSTFR [1080p]](https://uptobox.com/vz1pdbmdudf8)
- [[Refrain] Akkun to Kanojo - 19 VOSTFR [720p]](https://uptobox.com/e2t56o796mtu)
- [[Refrain] Akkun to Kanojo - 19 VOSTFR [1080p]](https://uptobox.com/365guaylgy34)
- [[Refrain] Akkun to Kanojo - 20 VOSTFR [720p]](https://uptobox.com/yij38405hvb7)
- [[Refrain] Akkun to Kanojo - 20 VOSTFR [1080p]](https://uptobox.com/3emhdyddjw58)
- [[Refrain] Akkun to Kanojo - 21 VOSTFR [720p]](https://uptobox.com/x6senb6n98p4)
- [[Refrain] Akkun to Kanojo - 21 VOSTFR [1080p]](https://uptobox.com/xqv7n09yjtmc)
- [[Refrain] Akkun to Kanojo - 22 VOSTFR [720p]](https://uptobox.com/r4iqadhlam0a)
- [[Refrain] Akkun to Kanojo - 22 VOSTFR [1080p]](https://uptobox.com/hjweiaq4f2uy)
- [[Refrain] Akkun to Kanojo - 23 VOSTFR [720p]](https://uptobox.com/kl8l6jiljje4)
- [[Refrain] Akkun to Kanojo - 23 VOSTFR [1080p]](https://uptobox.com/ia3kme42gr5u)
- [[Refrain] Akkun to Kanojo - 24 VOSTFR [720p]](https://uptobox.com/elap8ixxsmr4)
- [[Refrain] Akkun to Kanojo - 24 VOSTFR [1080p]](https://uptobox.com/6tu12igbs6dz)
- [[Refrain] Akkun to Kanojo - 25 VOSTFR [720p]](https://uptobox.com/cu7iqikjmtiz)
- [[Refrain] Akkun to Kanojo - 25 VOSTFR [1080p]](https://uptobox.com/q22pctrnh41i)
