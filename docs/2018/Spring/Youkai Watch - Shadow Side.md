# Youkai Watch: Shadow Side

![Youkai Watch: Shadow Side](https://cdn.myanimelist.net/images/anime/1072/90600l.jpg)

* Japanese:  妖怪ウォッチ シャドウサイド

## Information

 - Type: TV
 - Episodes: Unknown
 - Status: Currently Airing
 - Aired: Apr 13, 2018 to ?
 - Premiered: Spring 2018
 - Broadcast: Fridays at 18:25 (JST)
 - Producers: TV Tokyo 
 - Licensors: None found, add some 
 - Studios: OLM 
 - Source: Game
 - Genres: Comedy, Demons, Kids, Supernatural
 - Duration: 24 min.
 - Rating: G - All Ages


## Links

