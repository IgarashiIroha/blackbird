# Amanchu! Advance

![Amanchu! Advance](https://cdn.myanimelist.net/images/anime/1095/95063l.jpg)

* Japanese:  あまんちゅ！～あどばんす～

## Information

 - Type: TV
 - Episodes: 12
 - Status: Finished Airing
 - Aired: Apr 7, 2018 to Jun 23, 2018
 - Premiered: Spring 2018
 - Broadcast: Saturdays at 23:00 (JST)
 - Producers: None found, add some 
 - Licensors: None found, add some 
 - Studios: J.C.Staff 
 - Source: Manga
 - Genres: Comedy, School, Shounen, Slice of Life
 - Duration: 23 min. per ep.
 - Rating: PG-13 - Teens 13 or older


## Links

