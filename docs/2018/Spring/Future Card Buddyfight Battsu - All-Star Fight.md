# Future Card Buddyfight Battsu: All-Star Fight

![Future Card Buddyfight Battsu: All-Star Fight](https://cdn.myanimelist.net/images/anime/1924/95069l.jpg)

* Japanese:  フューチャーカード バディファイト バッツ オールスターファイト

## Information

 - Type: TV
 - Episodes: 8
 - Status: Finished Airing
 - Aired: Apr 7, 2018 to May 26, 2018
 - Premiered: Spring 2018
 - Broadcast: Saturdays at 08:00 (JST)
 - Producers: None found, add some 
 - Licensors: None found, add some 
 - Studios: Xebec, OLM 
 - Source: Card game
 - Genres: Game
 - Duration: 24 min. per ep.
 - Rating: PG - Children


## Links

