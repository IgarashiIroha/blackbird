# Sword Gai: The Animation

![Sword Gai: The Animation](https://cdn.myanimelist.net/images/anime/7/89759l.jpg)

* Japanese:  ソードガイ The Animation

## Information

 - Type: ONA
 - Episodes: 12
 - Status: Finished Airing
 - Aired: Mar 23, 2018
 - Producers: LandQ studios
 - Licensors: None found, add some
 - Studios: Production I.G, DLE
 - Source: Manga
 - Genres: Action, Seinen, Supernatural
 - Duration: 22 min. per ep.
 - Rating: R - 17+ (violence & profanity)


## Links

- [[Refrain] Sword Gai The Animation - 01 VF VOSTFR [720p]](https://uptobox.com/oikr4dkm4vt4)
- [[Refrain] Sword Gai The Animation - 01 VF VOSTFR [1080p]](https://uptobox.com/xu0bjcpyk0du)
- [[Refrain] Sword Gai The Animation - 02 VF VOSTFR [720p]](https://uptobox.com/5ajvro7aus6h)
- [[Refrain] Sword Gai The Animation - 02 VF VOSTFR [1080p]](https://uptobox.com/gkfycwbtecgv)
- [[Refrain] Sword Gai The Animation - 03 VF VOSTFR [720p]](https://uptobox.com/a5vpz74c7tj0)
- [[Refrain] Sword Gai The Animation - 03 VF VOSTFR [1080p]](https://uptobox.com/gemzwxnn280t)
- [[Refrain] Sword Gai The Animation - 04 VF VOSTFR [720p]](https://uptobox.com/bdf9joqilqep)
- [[Refrain] Sword Gai The Animation - 04 VF VOSTFR [1080p]](https://uptobox.com/bqp5t0ncp7y9)
- [[Refrain] Sword Gai The Animation - 05 VF VOSTFR [720p]](https://uptobox.com/tgm1v3amb20c)
- [[Refrain] Sword Gai The Animation - 05 VF VOSTFR [1080p]](https://uptobox.com/u7rqx18zwf72)
- [[Refrain] Sword Gai The Animation - 06 VF VOSTFR [720p]](https://uptobox.com/eg1e5k8xe7so)
- [[Refrain] Sword Gai The Animation - 06 VF VOSTFR [1080p]](https://uptobox.com/qmblsrhuwazx)
- [[Refrain] Sword Gai The Animation - 07 VF VOSTFR [720p]](https://uptobox.com/7wlpuny7vgd8)
- [[Refrain] Sword Gai The Animation - 07 VF VOSTFR [1080p]](https://uptobox.com/7ogij4vt29bi)
- [[Refrain] Sword Gai The Animation - 08 VF VOSTFR [720p]](https://uptobox.com/q126chgy4ym8)
- [[Refrain] Sword Gai The Animation - 08 VF VOSTFR [1080p]](https://uptobox.com/cosl2dg3rwzk)
- [[Refrain] Sword Gai The Animation - 09 VF VOSTFR [720p]](https://uptobox.com/5vdv302ydi76)
- [[Refrain] Sword Gai The Animation - 09 VF VOSTFR [1080p]](https://uptobox.com/1mky2di63jy2)
- [[Refrain] Sword Gai The Animation - 10 VF VOSTFR [720p]](https://uptobox.com/b3c4udurqqar)
- [[Refrain] Sword Gai The Animation - 10 VF VOSTFR [1080p]](https://uptobox.com/4ed83onlxit2)
- [[Refrain] Sword Gai The Animation - 11 VF VOSTFR [720p]](https://uptobox.com/v1spy4ciqrfk)
- [[Refrain] Sword Gai The Animation - 11 VF VOSTFR [1080p]](https://uptobox.com/v9r7fh13qi4s)
- [[Refrain] Sword Gai The Animation - 12 VF VOSTFR [720p]](https://uptobox.com/4n8d21p822vx)
- [[Refrain] Sword Gai The Animation - 12 VF VOSTFR [1080p]](https://uptobox.com/6x0fie3l95p5)
