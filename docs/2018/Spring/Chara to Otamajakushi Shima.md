# Chara to Otamajakushi Shima

![Chara to Otamajakushi Shima](https://cdn.myanimelist.net/images/anime/1931/90895l.jpg)

* Japanese:  キャラとおたまじゃくし島

## Information

 - Type: TV
 - Episodes: 40
 - Status: Currently Airing
 - Aired: Apr 2, 2018 to ?
 - Premiered: Spring 2018
 - Broadcast: Mondays at 10:15 (JST)
 - Producers: Dentsu, NHK 
 - Licensors: None found, add some 
 - Studios: 33 Collective 
 - Source: Original
 - Genres: Adventure, Fantasy, Kids
 - Duration: 5 min. per ep.
 - Rating: G - All Ages


## Links

