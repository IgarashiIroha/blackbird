# Puzzle & Dragon

![Puzzle & Dragon](https://cdn.myanimelist.net/images/anime/1535/95070l.jpg)

* Japanese:  パズドラ

## Information

 - Type: TV
 - Episodes: Unknown
 - Status: Currently Airing
 - Aired: Apr 2, 2018 to ?
 - Premiered: Spring 2018
 - Broadcast: Mondays at 18:25 (JST)
 - Producers: None found, add some
 - Licensors: None found, add some
 - Studios: Studio Pierrot
 - Source: Game
 - Genres: Game, Kids
 - Duration: 24 min.
 - Rating: PG - Children


## Links
