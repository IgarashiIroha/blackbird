# Tu Bian Ying Xiong Leaf

![Tu Bian Ying Xiong Leaf](https://cdn.myanimelist.net/images/anime/1552/93084l.jpg)

* Japanese:  凸变英雄 LEAF

## Information

 - Type: ONA
 - Episodes: 12
 - Status: Finished Airing
 - Aired: Apr 14, 2018 to Jul 6, 2018
 - Producers: bilibili
 - Licensors: None found, add some
 - Studios: Haoliners Animation League, Studio LAN
 - Source: Original
 - Genres: Comedy, Super Power
 - Duration: 20 min. per ep.
 - Rating: PG-13 - Teens 13 or older


## Links

- [[Refrain] To Be Heroine - 01 VOSTFR [720p]](https://uptobox.com/xvri8boufilj)
- [[Refrain] To Be Heroine - 01 VOSTFR [1080p]](https://uptobox.com/yb0f6luq8bbn)
- [[Refrain] To Be Heroine - 02 VOSTFR [720p]](https://uptobox.com/rt477yjt82en)
- [[Refrain] To Be Heroine - 02 VOSTFR [1080p]](https://uptobox.com/9muk6pxbax2t)
- [[Refrain] To Be Heroine - 03 VOSTFR [720p]](https://uptobox.com/wgcwddquedfo)
- [[Refrain] To Be Heroine - 03 VOSTFR [1080p]](https://uptobox.com/eb5s87h9ao5a)
- [[Refrain] To Be Heroine - 04.5 VOSTFR [720p]](https://uptobox.com/t5kx5555br14)
- [[Refrain] To Be Heroine - 04.5 VOSTFR [1080p]](https://uptobox.com/1vs1un4uq080)
- [[Refrain] To Be Heroine - 04 VOSTFR [720p]](https://uptobox.com/d6ucmh98rcbb)
- [[Refrain] To Be Heroine - 04 VOSTFR [1080p]](https://uptobox.com/3pxrjkkbes3v)
- [[Refrain] To Be Heroine - 05 VOSTFR [720p]](https://uptobox.com/7818q02qzdqh)
- [[Refrain] To Be Heroine - 05 VOSTFR [1080p]](https://uptobox.com/m9dhq6rjt8dw)
- [[Refrain] To Be Heroine - 06 VOSTFR [720p]](https://uptobox.com/unm1i8yligw7)
- [[Refrain] To Be Heroine - 06 VOSTFR [1080p]](https://uptobox.com/zpwo05mibxfb)
- [[Refrain] To Be Heroine - 07 VOSTFR [720p]](https://uptobox.com/ovsp2263p9ns)
- [[Refrain] To Be Heroine - 07 VOSTFR [1080p]](https://uptobox.com/xlb2geklxw6z)
