# Hisone to Maso-tan

![Hisone to Maso-tan](https://cdn.myanimelist.net/images/anime/1539/91169l.jpg)

* Japanese:  ひそねとまそたん

## Information

 - Type: TV
 - Episodes: 12
 - Status: Finished Airing
 - Aired: Apr 13, 2018 to Jun 29, 2018
 - Premiered: Spring 2018
 - Broadcast: Fridays at 00:00 (JST)
 - Producers: Warner Bros. Japan, BS Fuji, Hakuhodo DY Music & Pictures
 - Licensors: None found, add some
 - Studios: Bones
 - Source: Original
 - Genres: Comedy, Drama, Fantasy, Military
 - Duration: 24 min. per ep.
 - Rating: PG-13 - Teens 13 or older


## Links

- [[Refrain] Hisone to Maso-tan - 01 VF VOSTFR [720p]](https://uptobox.com/969qw1uzbcii)
- [[Refrain] Hisone to Maso-tan - 01 VF VOSTFR [1080p]](https://uptobox.com/mir1wxjcmfck)
- [[Refrain] Hisone to Maso-tan - 02 VF VOSTFR [720p]](https://uptobox.com/hc4m4x3lbb3l)
- [[Refrain] Hisone to Maso-tan - 02 VF VOSTFR [1080p]](https://uptobox.com/weqf3n36aec7)
- [[Refrain] Hisone to Maso-tan - 03 VF VOSTFR [720p]](https://uptobox.com/yahdik8q0c8p)
- [[Refrain] Hisone to Maso-tan - 03 VF VOSTFR [1080p]](https://uptobox.com/1m7i01vjw9ee)
- [[Refrain] Hisone to Maso-tan - 04 VF VOSTFR [720p]](https://uptobox.com/kzu3zr7qorq9)
- [[Refrain] Hisone to Maso-tan - 04 VF VOSTFR [1080p]](https://uptobox.com/jdsxzbxlup06)
- [[Refrain] Hisone to Maso-tan - 05 VF VOSTFR [720p]](https://uptobox.com/mf6fq0krmer6)
- [[Refrain] Hisone to Maso-tan - 05 VF VOSTFR [1080p]](https://uptobox.com/29ccmj76bhsp)
- [[Refrain] Hisone to Maso-tan - 06 VF VOSTFR [720p]](https://uptobox.com/r5pyhda7bx59)
- [[Refrain] Hisone to Maso-tan - 06 VF VOSTFR [1080p]](https://uptobox.com/34sopcj04kdd)
- [[Refrain] Hisone to Maso-tan - 07 VF VOSTFR [720p]](https://uptobox.com/yiwpkpzx2ugu)
- [[Refrain] Hisone to Maso-tan - 07 VF VOSTFR [1080p]](https://uptobox.com/7pgxgclmd471)
- [[Refrain] Hisone to Maso-tan - 08 VF VOSTFR [720p]](https://uptobox.com/q3zwpn1hxoi3)
- [[Refrain] Hisone to Maso-tan - 08 VF VOSTFR [1080p]](https://uptobox.com/ini86wk0861g)
- [[Refrain] Hisone to Maso-tan - 09 VF VOSTFR [720p]](https://uptobox.com/ii1dl7osl25v)
- [[Refrain] Hisone to Maso-tan - 09 VF VOSTFR [1080p]](https://uptobox.com/ug1jvf66dzo1)
- [[Refrain] Hisone to Maso-tan - 10 VF VOSTFR [720p]](https://uptobox.com/go8fwnslo567)
- [[Refrain] Hisone to Maso-tan - 10 VF VOSTFR [1080p]](https://uptobox.com/t2lu0uruubcj)
- [[Refrain] Hisone to Maso-tan - 11 VF VOSTFR [720p]](https://uptobox.com/2i8e86r47dox)
- [[Refrain] Hisone to Maso-tan - 11 VF VOSTFR [1080p]](https://uptobox.com/dst0o25s8wh4)
- [[Refrain] Hisone to Maso-tan - 12 VF VOSTFR [720p]](https://uptobox.com/03l1nnp9c8wi)
- [[Refrain] Hisone to Maso-tan - 12 VF VOSTFR [1080p]](https://uptobox.com/azz2ttx56ibv)
