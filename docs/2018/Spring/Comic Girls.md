# Comic Girls

![Comic Girls](https://cdn.myanimelist.net/images/anime/1207/91794l.jpg)

* Japanese:  こみっくがーるず

## Information

 - Type: TV
 - Episodes: 12
 - Status: Finished Airing
 - Aired: Apr 5, 2018 to Jun 21, 2018
 - Premiered: Spring 2018
 - Broadcast: Thursdays at 23:00 (JST)
 - Producers: Houbunsha, Kansai Telecasting, BS11, My Theater D.D.
 - Licensors: None found, add some
 - Studios: Nexus
 - Source: 4-koma manga
 - Genres: Comedy, Slice of Life
 - Duration: 23 min. per ep.
 - Rating: PG-13 - Teens 13 or older


## Links

- [[Refrain] Comic Girls - 01 VOSTFR [720p]](https://uptobox.com/br626ee447vn)
- [[Refrain] Comic Girls - 01 VOSTFR [1080p]](https://uptobox.com/e96mri6ix6us)
- [[Refrain] Comic Girls - 02 VOSTFR [720p]](https://uptobox.com/4a1i42fdsy8q)
- [[Refrain] Comic Girls - 02 VOSTFR [1080p]](https://uptobox.com/1zdc62lck7vl)
- [[Refrain] Comic Girls - 03 VOSTFR [720p]](https://uptobox.com/2teag4ietri3)
- [[Refrain] Comic Girls - 03 VOSTFR [1080p]](https://uptobox.com/miy1fyr5z7s1)
- [[Refrain] Comic Girls - 04 VOSTFR [720p]](https://uptobox.com/5lzrogiaea98)
- [[Refrain] Comic Girls - 04 VOSTFR [1080p]](https://uptobox.com/wr51lv16t2pv)
- [[Refrain] Comic Girls - 05 VOSTFR [720p]](https://uptobox.com/3hil26w1jiel)
- [[Refrain] Comic Girls - 05 VOSTFR [1080p]](https://uptobox.com/d1pfzrtoh5p4)
- [[Refrain] Comic Girls - 06 VOSTFR [720p]](https://uptobox.com/2kxlytutawpk)
- [[Refrain] Comic Girls - 06 VOSTFR [1080p]](https://uptobox.com/wo2ew6hdlrqv)
- [[Refrain] Comic Girls - 07 VOSTFR [720p]](https://uptobox.com/fs2x9df8hlex)
- [[Refrain] Comic Girls - 07 VOSTFR [1080p]](https://uptobox.com/1ae4pn1w8ncb)
- [[Refrain] Comic Girls - 08 VOSTFR [720p]](https://uptobox.com/b66xhg3jbjoc)
- [[Refrain] Comic Girls - 08 VOSTFR [1080p]](https://uptobox.com/v86ivxme04ts)
- [[Refrain] Comic Girls - 09 VOSTFR [720p]](https://uptobox.com/2b0vkwp9tuio)
- [[Refrain] Comic Girls - 09 VOSTFR [1080p]](https://uptobox.com/fd46slp3vf31)
- [[Refrain] Comic Girls - 10 VOSTFR [720p]](https://uptobox.com/uqky048bwtsk)
- [[Refrain] Comic Girls - 10 VOSTFR [1080p]](https://uptobox.com/gmes3vph0tx8)
- [[Refrain] Comic Girls - 11 VOSTFR [720p]](https://uptobox.com/q9c84c5w9yrd)
- [[Refrain] Comic Girls - 11 VOSTFR [1080p]](https://uptobox.com/69dvdhkwcbmw)
- [[Refrain] Comic Girls - 12 VOSTFR [720p]](https://uptobox.com/fzrkkekcxkqx)
- [[Refrain] Comic Girls - 12 VOSTFR [1080p]](https://uptobox.com/zsrf6bwi6zkk)
