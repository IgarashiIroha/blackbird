# Aikatsu Friends!

![Aikatsu Friends!](https://cdn.myanimelist.net/images/anime/1198/90430l.jpg)

* Japanese:  アイカツフレンズ！

## Information

 - Type: TV
 - Episodes: Unknown
 - Status: Currently Airing
 - Aired: Apr 5, 2018 to ?
 - Premiered: Spring 2018
 - Broadcast: Thursdays at 18:25 (JST)
 - Producers: TV Tokyo, Dentsu 
 - Licensors: None found, add some 
 - Studios: Bandai Namco Pictures 
 - Source: Game
 - Genres: Music, School, Shoujo, Slice of Life
 - Duration: 24 min.
 - Rating: PG - Children


## Links

