# Fumikiri Jikan

![Fumikiri Jikan](https://cdn.myanimelist.net/images/anime/1598/91857l.jpg)

* Japanese:  踏切時間

## Information

 - Type: TV
 - Episodes: 12
 - Status: Finished Airing
 - Aired: Apr 10, 2018 to Jun 26, 2018
 - Premiered: Spring 2018
 - Broadcast: Tuesdays at 01:15 (JST)
 - Producers: None found, add some
 - Licensors: None found, add some
 - Studios: EKACHI EPILKA
 - Source: Manga
 - Genres: Comedy, Slice of Life
 - Duration: 3 min. per ep.
 - Rating: PG-13 - Teens 13 or older


## Links

- [[Refrain] Fumikiri Jikan - 01 VOSTFR [720p]](https://uptobox.com/yjdrzmi2bls5)
- [[Refrain] Fumikiri Jikan - 01 VOSTFR [1080p]](https://uptobox.com/6mwpnf35ypda)
- [[Refrain] Fumikiri Jikan - 02 VOSTFR [720p]](https://uptobox.com/d9t6m4fj5k7x)
- [[Refrain] Fumikiri Jikan - 02 VOSTFR [1080p]](https://uptobox.com/d9h52rxw882p)
- [[Refrain] Fumikiri Jikan - 03 VOSTFR [720p]](https://uptobox.com/428lezdr76pe)
- [[Refrain] Fumikiri Jikan - 03 VOSTFR [1080p]](https://uptobox.com/4hz8yrl6hvri)
- [[Refrain] Fumikiri Jikan - 04 VOSTFR [720p]](https://uptobox.com/7yndltziculp)
- [[Refrain] Fumikiri Jikan - 04 VOSTFR [1080p]](https://uptobox.com/vo431bh1as9r)
- [[Refrain] Fumikiri Jikan - 05 VOSTFR [720p]](https://uptobox.com/3wivjlbkccu4)
- [[Refrain] Fumikiri Jikan - 05 VOSTFR [1080p]](https://uptobox.com/lcb8ovu91on6)
- [[Refrain] Fumikiri Jikan - 06 VOSTFR [720p]](https://uptobox.com/bolrgezsalfy)
- [[Refrain] Fumikiri Jikan - 06 VOSTFR [1080p]](https://uptobox.com/yfcq7zookxp2)
- [[Refrain] Fumikiri Jikan - 07 VOSTFR [720p]](https://uptobox.com/kays85pegy5j)
- [[Refrain] Fumikiri Jikan - 07 VOSTFR [1080p]](https://uptobox.com/c8cjykolp0kd)
- [[Refrain] Fumikiri Jikan - 08 VOSTFR [720p]](https://uptobox.com/0qf94j4iu3lq)
- [[Refrain] Fumikiri Jikan - 08 VOSTFR [1080p]](https://uptobox.com/1oyad45zdyr3)
- [[Refrain] Fumikiri Jikan - 09 VOSTFR [720p]](https://uptobox.com/w3syv0glcruc)
- [[Refrain] Fumikiri Jikan - 09 VOSTFR [1080p]](https://uptobox.com/5537g059ikqr)
- [[Refrain] Fumikiri Jikan - 10 VOSTFR [720p]](https://uptobox.com/iqgzzglkqeu9)
- [[Refrain] Fumikiri Jikan - 10 VOSTFR [1080p]](https://uptobox.com/ta2dlq8t9dq1)
- [[Refrain] Fumikiri Jikan - 11 VOSTFR [720p]](https://uptobox.com/x3xk6svnhlsz)
- [[Refrain] Fumikiri Jikan - 11 VOSTFR [1080p]](https://uptobox.com/jmm0p6qnej7k)
- [[Refrain] Fumikiri Jikan - 12 VOSTFR [720p]](https://uptobox.com/5hy8to46tt5i)
- [[Refrain] Fumikiri Jikan - 12 VOSTFR [1080p]](https://uptobox.com/z6s6m519asm3)
