# Last Period: Owarinaki Rasen no Monogatari

![Last Period: Owarinaki Rasen no Monogatari](https://cdn.myanimelist.net/images/anime/1710/95065l.jpg)

* Japanese:  ラストピリオド -終わりなき螺旋の物語-

## Information

 - Type: TV
 - Episodes: 12
 - Status: Finished Airing
 - Aired: Apr 12, 2018 to Jun 28, 2018
 - Premiered: Spring 2018
 - Broadcast: Thursdays at 00:00 (JST)
 - Producers: Egg Firm
 - Licensors: None found, add some
 - Studios: J.C.Staff
 - Source: Game
 - Genres: Action, Adventure, Comedy, Fantasy, Magic
 - Duration: 23 min. per ep.
 - Rating: PG-13 - Teens 13 or older


## Links

- [[Refrain] Last Period - Owarinaki Rasen no Monogatari - 01 VOSTFR [720p]](https://uptobox.com/4ioasib2cp8s)
- [[Refrain] Last Period - Owarinaki Rasen no Monogatari - 01 VOSTFR [1080p]](https://uptobox.com/9e06hj0fcsgm)
- [[Refrain] Last Period - Owarinaki Rasen no Monogatari - 02 VOSTFR [720p]](https://uptobox.com/i31o54uh7x0b)
- [[Refrain] Last Period - Owarinaki Rasen no Monogatari - 02 VOSTFR [1080p]](https://uptobox.com/52fncr78y4dr)
- [[Refrain] Last Period - Owarinaki Rasen no Monogatari - 03 VOSTFR [720p]](https://uptobox.com/kebfxavrcc0y)
- [[Refrain] Last Period - Owarinaki Rasen no Monogatari - 03 VOSTFR [1080p]](https://uptobox.com/eo4x87g3wsu0)
- [[Refrain] Last Period - Owarinaki Rasen no Monogatari - 04 VOSTFR [720p]](https://uptobox.com/17wy0ytnwhu5)
- [[Refrain] Last Period - Owarinaki Rasen no Monogatari - 04 VOSTFR [1080p]](https://uptobox.com/8ul1ibi08c76)
- [[Refrain] Last Period - Owarinaki Rasen no Monogatari - 05 VOSTFR [720p]](https://uptobox.com/rmwvvai3jazh)
- [[Refrain] Last Period - Owarinaki Rasen no Monogatari - 05 VOSTFR [1080p]](https://uptobox.com/fh76fjbqtmci)
- [[Refrain] Last Period - Owarinaki Rasen no Monogatari - 06 VOSTFR [720p]](https://uptobox.com/dkzgbmg0a09e)
- [[Refrain] Last Period - Owarinaki Rasen no Monogatari - 06 VOSTFR [1080p]](https://uptobox.com/8dpp6bnaq90r)
- [[Refrain] Last Period - Owarinaki Rasen no Monogatari - 07 VOSTFR [720p]](https://uptobox.com/1pt2hbqndrlu)
- [[Refrain] Last Period - Owarinaki Rasen no Monogatari - 07 VOSTFR [1080p]](https://uptobox.com/q5caujm4un34)
- [[Refrain] Last Period - Owarinaki Rasen no Monogatari - 08 VOSTFR [720p]](https://uptobox.com/6it1nipobv23)
- [[Refrain] Last Period - Owarinaki Rasen no Monogatari - 08 VOSTFR [1080p]](https://uptobox.com/9w8jp4cpq3ac)
- [[Refrain] Last Period - Owarinaki Rasen no Monogatari - 09 VOSTFR [720p]](https://uptobox.com/zw7ixzg997m4)
- [[Refrain] Last Period - Owarinaki Rasen no Monogatari - 09 VOSTFR [1080p]](https://uptobox.com/1a2novtehqdt)
- [[Refrain] Last Period - Owarinaki Rasen no Monogatari - 10 VOSTFR [720p]](https://uptobox.com/fff2fgyhucms)
- [[Refrain] Last Period - Owarinaki Rasen no Monogatari - 10 VOSTFR [1080p]](https://uptobox.com/5680gzav86xm)
- [[Refrain] Last Period - Owarinaki Rasen no Monogatari - 11 VOSTFR [720p]](https://uptobox.com/fvre1354ymwo)
- [[Refrain] Last Period - Owarinaki Rasen no Monogatari - 11 VOSTFR [1080p]](https://uptobox.com/ghr2st7ewj8z)
- [[Refrain] Last Period - Owarinaki Rasen no Monogatari - 12 VOSTFR [720p]](https://uptobox.com/gdc23m8g8om6)
- [[Refrain] Last Period - Owarinaki Rasen no Monogatari - 12 VOSTFR [1080p]](https://uptobox.com/hh6uu4opok18)
