# Ladyspo

![Ladyspo](https://cdn.myanimelist.net/images/anime/1911/95066l.jpg)

* Japanese:  レディスポ

## Information

 - Type: TV
 - Episodes: 12
 - Status: Finished Airing
 - Aired: Apr 10, 2018 to Jun 26, 2018
 - Premiered: Spring 2018
 - Broadcast: Tuesdays at 01:20 (JST)
 - Producers: DAX Production, Quatre Stella, RME 
 - Licensors: None found, add some 
 - Studios: Brio Animation 
 - Source: Original
 - Genres: Action, Comedy, Sci-Fi
 - Duration: 4 min. per ep.
 - Rating: None


## Links

