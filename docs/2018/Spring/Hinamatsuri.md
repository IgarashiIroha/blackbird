# Hinamatsuri

![Hinamatsuri](https://cdn.myanimelist.net/images/anime/1580/93526l.jpg)

* Japanese:  ヒナまつり

## Information

 - Type: TV
 - Episodes: 12
 - Status: Finished Airing
 - Aired: Apr 6, 2018 to Jun 22, 2018
 - Premiered: Spring 2018
 - Broadcast: Fridays at 21:00 (JST)
 - Producers: Media Factory, Magic Capsule, Nippon Columbia, Kadokawa Media House
 - Licensors: Funimation
 - Studios: feel.
 - Source: Manga
 - Genres: Comedy, Supernatural, Sci-Fi, Slice of Life, Seinen
 - Duration: 23 min. per ep.
 - Rating: PG-13 - Teens 13 or older


## Links

- [[Refrain] Hinamatsuri - 01 VOSTFR [720p]](https://uptobox.com/s5msp2dbbymj)
- [[Refrain] Hinamatsuri - 01 VOSTFR [1080p]](https://uptobox.com/skmh6hr3sonk)
- [[Refrain] Hinamatsuri - 02 VOSTFR [720p]](https://uptobox.com/clgix4eaq3v8)
- [[Refrain] Hinamatsuri - 02 VOSTFR [1080p]](https://uptobox.com/kze08ht1zd8b)
- [[Refrain] Hinamatsuri - 03 VOSTFR [720p]](https://uptobox.com/w6k8qpisnd4d)
- [[Refrain] Hinamatsuri - 03 VOSTFR [1080p]](https://uptobox.com/zvkz8knj7axc)
- [[Refrain] Hinamatsuri - 04 VOSTFR [720p]](https://uptobox.com/9mv9ie60e2zm)
- [[Refrain] Hinamatsuri - 04 VOSTFR [1080p]](https://uptobox.com/jhy1744iiawe)
- [[Refrain] Hinamatsuri - 05 VOSTFR [720p]](https://uptobox.com/2lj8qoud2rae)
- [[Refrain] Hinamatsuri - 05 VOSTFR [1080p]](https://uptobox.com/bo7m1u36521b)
- [[Refrain] Hinamatsuri - 06 VOSTFR [720p]](https://uptobox.com/95qkmd11gfv4)
- [[Refrain] Hinamatsuri - 06 VOSTFR [1080p]](https://uptobox.com/i3nr4ra9i4nt)
- [[Refrain] Hinamatsuri - 07 VOSTFR [720p]](https://uptobox.com/8ilsu033qe12)
- [[Refrain] Hinamatsuri - 07 VOSTFR [1080p]](https://uptobox.com/luog3d40d2r2)
- [[Refrain] Hinamatsuri - 08 VOSTFR [720p]](https://uptobox.com/pk3frrr8twd0)
- [[Refrain] Hinamatsuri - 08 VOSTFR [1080p]](https://uptobox.com/agef70x7vap3)
- [[Refrain] Hinamatsuri - 09 VOSTFR [720p]](https://uptobox.com/2njzt3993dgs)
- [[Refrain] Hinamatsuri - 09 VOSTFR [1080p]](https://uptobox.com/urcm2zg8sxvz)
- [[Refrain] Hinamatsuri - 10 VOSTFR [720p]](https://uptobox.com/33n3bmr8ztqj)
- [[Refrain] Hinamatsuri - 10 VOSTFR [1080p]](https://uptobox.com/lrxvouse0ixr)
- [[Refrain] Hinamatsuri - 11 VOSTFR [720p]](https://uptobox.com/5o5j5gxro618)
- [[Refrain] Hinamatsuri - 11 VOSTFR [1080p]](https://uptobox.com/fflekxcqljtq)
- [[Refrain] Hinamatsuri - 12 VOSTFR [720p]](https://uptobox.com/gqgpqxzpvnhr)
- [[Refrain] Hinamatsuri - 12 VOSTFR [1080p]](https://uptobox.com/vizuosrih5vd)
