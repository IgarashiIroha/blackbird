# Ginga Eiyuu Densetsu: Die Neue These - Kaikou

![Ginga Eiyuu Densetsu: Die Neue These - Kaikou](https://cdn.myanimelist.net/images/anime/1104/91793l.jpg)

* Japanese:  銀河英雄伝説 Die Neue These 邂逅

## Information

 - Type: TV
 - Episodes: 12
 - Status: Finished Airing
 - Aired: Apr 3, 2018 to Jun 26, 2018
 - Premiered: Spring 2018
 - Broadcast: Tuesdays at 21:00 (JST)
 - Producers: Shochiku
 - Licensors: Funimation
 - Studios: Production I.G
 - Source: Novel
 - Genres: Action, Drama, Military, Sci-Fi, Space
 - Duration: 25 min. per ep.
 - Rating: R - 17+ (violence & profanity)


## Links

- [[Refrain] Ginga Eiyuu Densetsu - Die Neue These - Kaikou - 01 VOSTFR [720p]](https://uptobox.com/e4z3jjgo2pah)
- [[Refrain] Ginga Eiyuu Densetsu - Die Neue These - Kaikou - 01 VOSTFR [1080p]](https://uptobox.com/c3amhhk0rvgz)
- [[Refrain] Ginga Eiyuu Densetsu - Die Neue These - Kaikou - 01 VOSTFR {ADN} [720p]](https://uptobox.com/z13j5n71d4ym)
- [[Refrain] Ginga Eiyuu Densetsu - Die Neue These - Kaikou - 01 VOSTFR {ADN} [1080p]](https://uptobox.com/xdk6y53ot722)
- [[Refrain] Ginga Eiyuu Densetsu - Die Neue These - Kaikou - 01 VOSTFR {CR} [720p]](https://uptobox.com/t7yc6dcyv1sm)
- [[Refrain] Ginga Eiyuu Densetsu - Die Neue These - Kaikou - 01 VOSTFR {CR} [1080p]](https://uptobox.com/hd3cuu4mrfmc)
- [[Refrain] Ginga Eiyuu Densetsu - Die Neue These - Kaikou - 02 VOSTFR [720p]](https://uptobox.com/c84s0mib64fd)
- [[Refrain] Ginga Eiyuu Densetsu - Die Neue These - Kaikou - 02 VOSTFR [1080p]](https://uptobox.com/z4scibmdh9gr)
- [[Refrain] Ginga Eiyuu Densetsu - Die Neue These - Kaikou - 02 VOSTFR {ADN} [720p]](https://uptobox.com/gy53fp4y9q29)
- [[Refrain] Ginga Eiyuu Densetsu - Die Neue These - Kaikou - 02 VOSTFR {ADN} [1080p]](https://uptobox.com/kecn3k32xg1p)
- [[Refrain] Ginga Eiyuu Densetsu - Die Neue These - Kaikou - 02 VOSTFR {CR} [720p]](https://uptobox.com/hfzku78nu4n0)
- [[Refrain] Ginga Eiyuu Densetsu - Die Neue These - Kaikou - 02 VOSTFR {CR} [1080p]](https://uptobox.com/65g2ly4m4tux)
- [[Refrain] Ginga Eiyuu Densetsu - Die Neue These - Kaikou - 03 VOSTFR [720p]](https://uptobox.com/kunu85qqhq8r)
- [[Refrain] Ginga Eiyuu Densetsu - Die Neue These - Kaikou - 03 VOSTFR [1080p]](https://uptobox.com/zx2bmo1ajos3)
- [[Refrain] Ginga Eiyuu Densetsu - Die Neue These - Kaikou - 03 VOSTFR {ADN} [720p]](https://uptobox.com/56zv4gze47tw)
- [[Refrain] Ginga Eiyuu Densetsu - Die Neue These - Kaikou - 03 VOSTFR {ADN} [1080p]](https://uptobox.com/av9uje4rykf6)
- [[Refrain] Ginga Eiyuu Densetsu - Die Neue These - Kaikou - 03 VOSTFR {CR} [720p]](https://uptobox.com/yc1juwfc1w5n)
- [[Refrain] Ginga Eiyuu Densetsu - Die Neue These - Kaikou - 03 VOSTFR {CR} [1080p]](https://uptobox.com/vxtmimpexo9d)
- [[Refrain] Ginga Eiyuu Densetsu - Die Neue These - Kaikou - 04 VOSTFR {ADN} [720p]](https://uptobox.com/nr7xqb5d6dea)
- [[Refrain] Ginga Eiyuu Densetsu - Die Neue These - Kaikou - 04 VOSTFR {ADN} [1080p]](https://uptobox.com/14wxhum0ysmb)
- [[Refrain] Ginga Eiyuu Densetsu - Die Neue These - Kaikou - 04 VOSTFR {CR} [720p]](https://uptobox.com/mggargy9i0md)
- [[Refrain] Ginga Eiyuu Densetsu - Die Neue These - Kaikou - 04 VOSTFR {CR} [1080p]](https://uptobox.com/hmz1ag843059)
- [[Refrain] Ginga Eiyuu Densetsu - Die Neue These - Kaikou - 05 VOSTFR {ADN} [720p]](https://uptobox.com/q5zjkhwlqpyg)
- [[Refrain] Ginga Eiyuu Densetsu - Die Neue These - Kaikou - 05 VOSTFR {ADN} [1080p]](https://uptobox.com/co489chr5br2)
- [[Refrain] Ginga Eiyuu Densetsu - Die Neue These - Kaikou - 05 VOSTFR {CR} [720p]](https://uptobox.com/7te48wggfjlg)
- [[Refrain] Ginga Eiyuu Densetsu - Die Neue These - Kaikou - 05 VOSTFR {CR} [1080p]](https://uptobox.com/8uy8ooqidbyv)
- [[Refrain] Ginga Eiyuu Densetsu - Die Neue These - Kaikou - 06 VOSTFR {ADN} [720p]](https://uptobox.com/bxc2iju2eb28)
- [[Refrain] Ginga Eiyuu Densetsu - Die Neue These - Kaikou - 06 VOSTFR {ADN} [1080p]](https://uptobox.com/1fps1mo5p6ey)
- [[Refrain] Ginga Eiyuu Densetsu - Die Neue These - Kaikou - 06 VOSTFR {CR} [720p]](https://uptobox.com/gh7w9m5utn4h)
- [[Refrain] Ginga Eiyuu Densetsu - Die Neue These - Kaikou - 06 VOSTFR {CR} [1080p]](https://uptobox.com/4phuhnwwgk3o)
- [[Refrain] Ginga Eiyuu Densetsu - Die Neue These - Kaikou - 07 VOSTFR {ADN} [720p]](https://uptobox.com/q27zvaqi5ncw)
- [[Refrain] Ginga Eiyuu Densetsu - Die Neue These - Kaikou - 07 VOSTFR {ADN} [1080p]](https://uptobox.com/lbzo2k4vgiw3)
- [[Refrain] Ginga Eiyuu Densetsu - Die Neue These - Kaikou - 07 VOSTFR {CR} [720p]](https://uptobox.com/fpn4bjiad6gp)
- [[Refrain] Ginga Eiyuu Densetsu - Die Neue These - Kaikou - 07 VOSTFR {CR} [1080p]](https://uptobox.com/n03lcn6b7vzt)
- [[Refrain] Ginga Eiyuu Densetsu - Die Neue These - Kaikou - 08 VOSTFR {ADN} [720p]](https://uptobox.com/smpko839unsd)
- [[Refrain] Ginga Eiyuu Densetsu - Die Neue These - Kaikou - 08 VOSTFR {ADN} [1080p]](https://uptobox.com/fsgd8477mlga)
- [[Refrain] Ginga Eiyuu Densetsu - Die Neue These - Kaikou - 08 VOSTFR {CR} [720p]](https://uptobox.com/c70oq3yghruf)
- [[Refrain] Ginga Eiyuu Densetsu - Die Neue These - Kaikou - 08 VOSTFR {CR} [1080p]](https://uptobox.com/n4saksqc9soq)
- [[Refrain] Ginga Eiyuu Densetsu - Die Neue These - Kaikou - 09 VOSTFR {ADN} [720p]](https://uptobox.com/sru91gyov9m4)
- [[Refrain] Ginga Eiyuu Densetsu - Die Neue These - Kaikou - 09 VOSTFR {ADN} [1080p]](https://uptobox.com/ettzyur5gh0r)
- [[Refrain] Ginga Eiyuu Densetsu - Die Neue These - Kaikou - 09 VOSTFR {CR} [720p]](https://uptobox.com/u4shdtyw5ife)
- [[Refrain] Ginga Eiyuu Densetsu - Die Neue These - Kaikou - 09 VOSTFR {CR} [1080p]](https://uptobox.com/ocun1poh5kw9)
- [[Refrain] Ginga Eiyuu Densetsu - Die Neue These - Kaikou - 10 VOSTFR {ADN} [720p]](https://uptobox.com/ut1k6571zvc9)
- [[Refrain] Ginga Eiyuu Densetsu - Die Neue These - Kaikou - 10 VOSTFR {ADN} [1080p]](https://uptobox.com/fu500453lf8r)
- [[Refrain] Ginga Eiyuu Densetsu - Die Neue These - Kaikou - 10 VOSTFR {CR} [720p]](https://uptobox.com/3z9r4deujyo0)
- [[Refrain] Ginga Eiyuu Densetsu - Die Neue These - Kaikou - 10 VOSTFR {CR} [1080p]](https://uptobox.com/ld3pyy8rxycv)
- [[Refrain] Ginga Eiyuu Densetsu - Die Neue These - Kaikou - 11 VOSTFR {ADN} [720p]](https://uptobox.com/lsjt2uio0j25)
- [[Refrain] Ginga Eiyuu Densetsu - Die Neue These - Kaikou - 11 VOSTFR {ADN} [1080p]](https://uptobox.com/46s8wxm5toxx)
- [[Refrain] Ginga Eiyuu Densetsu - Die Neue These - Kaikou - 11 VOSTFR {CR} [720p]](https://uptobox.com/mbkzbna3upvl)
- [[Refrain] Ginga Eiyuu Densetsu - Die Neue These - Kaikou - 11 VOSTFR {CR} [1080p]](https://uptobox.com/wxe757a8z7sz)
- [[Refrain] Ginga Eiyuu Densetsu - Die Neue These - Kaikou - 12 VOSTFR {ADN} [720p]](https://uptobox.com/l0yiakhg59cy)
- [[Refrain] Ginga Eiyuu Densetsu - Die Neue These - Kaikou - 12 VOSTFR {ADN} [1080p]](https://uptobox.com/ef58ri21xr1b)
- [[Refrain] Ginga Eiyuu Densetsu - Die Neue These - Kaikou - 12 VOSTFR {CR} [720p]](https://uptobox.com/3qkll5xqkz87)
- [[Refrain] Ginga Eiyuu Densetsu - Die Neue These - Kaikou - 12 VOSTFR {CR} [1080p]](https://uptobox.com/ew3mf4liv5s5)
