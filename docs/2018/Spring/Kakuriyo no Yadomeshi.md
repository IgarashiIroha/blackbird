# Kakuriyo no Yadomeshi

![Kakuriyo no Yadomeshi](https://cdn.myanimelist.net/images/anime/1035/95056l.jpg)

* Japanese:  かくりよの宿飯

## Information

 - Type: TV
 - Episodes: 26
 - Status: Finished Airing
 - Aired: Apr 2, 2018 to Sep 24, 2018
 - Premiered: Spring 2018
 - Broadcast: Mondays at 22:30 (JST)
 - Producers: flying DOG, Kadokawa 
 - Licensors: Funimation 
 - Studios: Gonzo 
 - Source: Novel
 - Genres: Demons, Supernatural
 - Duration: 23 min. per ep.
 - Rating: PG-13 - Teens 13 or older


## Links

