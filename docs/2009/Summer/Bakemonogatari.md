# Bakemonogatari

![Bakemonogatari](https://cdn.myanimelist.net/images/anime/11/75274l.jpg)

* Japanese:  化物語

## Information

 - Type: TV
 - Episodes: 15
 - Status: Finished Airing
 - Aired: Jul 3, 2009 to Jun 25, 2010
 - Premiered: Summer 2009
 - Broadcast: Fridays at 23:00 (JST)
 - Producers: Aniplex, Kodansha 
 - Licensors: Aniplex of America 
 - Studios: Shaft 
 - Source: Light novel
 - Genres: Romance, Supernatural, Mystery, Vampire
 - Duration: 24 min. per ep.
 - Rating: R - 17+ (violence & profanity)


## Links

- [[Refrain] Bakemonogatari - 01 VF VOSTFR [1080p]](https://uptobox.com/v2v1gzbfojrw)
- [[Refrain] Bakemonogatari - 01 VF VOSTFR [720p]](https://uptobox.com/ciaxivxmumz1)
- [[Refrain] Bakemonogatari - 02 VF VOSTFR [1080p]](https://uptobox.com/vf5afdfyeh59)
- [[Refrain] Bakemonogatari - 02 VF VOSTFR [720p]](https://uptobox.com/85s1emfpn0wy)
- [[Refrain] Bakemonogatari - 03 VF VOSTFR [1080p]](https://uptobox.com/dt5rr3lqpilz)
- [[Refrain] Bakemonogatari - 03 VF VOSTFR [720p]](https://uptobox.com/w7sbixw3eigj)
- [[Refrain] Bakemonogatari - 04 VF VOSTFR [1080p]](https://uptobox.com/efp7h3x6fn7j)
- [[Refrain] Bakemonogatari - 04 VF VOSTFR [720p]](https://uptobox.com/mswr0dpno47a)
- [[Refrain] Bakemonogatari - 05 VF VOSTFR [1080p]](https://uptobox.com/5dam61720khb)
- [[Refrain] Bakemonogatari - 05 VF VOSTFR [720p]](https://uptobox.com/4yqai6qmtjdp)
- [[Refrain] Bakemonogatari - 06 VF VOSTFR [1080p]](https://uptobox.com/1at3qyydr1gl)
- [[Refrain] Bakemonogatari - 06 VF VOSTFR [720p]](https://uptobox.com/fjhzujy1ldep)
- [[Refrain] Bakemonogatari - 07 VF VOSTFR [1080p]](https://uptobox.com/lidb2htd7w12)
- [[Refrain] Bakemonogatari - 07 VF VOSTFR [720p]](https://uptobox.com/cosgicna3x3d)
- [[Refrain] Bakemonogatari - 08 VF VOSTFR [1080p]](https://uptobox.com/9b5v1hptzbvt)
- [[Refrain] Bakemonogatari - 08 VF VOSTFR [720p]](https://uptobox.com/g5rsn3iriphx)
- [[Refrain] Bakemonogatari - 09 VF VOSTFR [1080p]](https://uptobox.com/d1shwdtxselm)
- [[Refrain] Bakemonogatari - 09 VF VOSTFR [720p]](https://uptobox.com/6kzdihvywg3c)
- [[Refrain] Bakemonogatari - 10 VF VOSTFR [1080p]](https://uptobox.com/1o3qt3dc8oqd)
- [[Refrain] Bakemonogatari - 10 VF VOSTFR [720p]](https://uptobox.com/k8kab0p0yx0d)
- [[Refrain] Bakemonogatari - 11 VF VOSTFR [1080p]](https://uptobox.com/th4qthn87kp1)
- [[Refrain] Bakemonogatari - 11 VF VOSTFR [720p]](https://uptobox.com/f3vounm15hie)
- [[Refrain] Bakemonogatari - 12 VF VOSTFR [1080p]](https://uptobox.com/knirm4b5i6z2)
- [[Refrain] Bakemonogatari - 12 VF VOSTFR [720p]](https://uptobox.com/7lbdv5k0fgpm)
- [[Refrain] Bakemonogatari - 13 VF VOSTFR [1080p]](https://uptobox.com/yqt6l89vgqct)
- [[Refrain] Bakemonogatari - 13 VF VOSTFR [720p]](https://uptobox.com/bzb3150tyvla)
- [[Refrain] Bakemonogatari - 14 VF VOSTFR [1080p]](https://uptobox.com/qifgwqyhtjoe)
- [[Refrain] Bakemonogatari - 14 VF VOSTFR [720p]](https://uptobox.com/6atltn42cqqw)
- [[Refrain] Bakemonogatari - 15 VF VOSTFR [720p]](https://uptobox.com/3s5fn5an3s9y)
