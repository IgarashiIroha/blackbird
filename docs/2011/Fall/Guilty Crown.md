# Guilty Crown

![Guilty Crown](https://cdn.myanimelist.net/images/anime/8/33713l.jpg)

* Japanese:  ギルティクラウン

## Information

 - Type: TV
 - Episodes: 22
 - Status: Finished Airing
 - Aired: Oct 14, 2011 to Mar 23, 2012
 - Premiered: Fall 2011
 - Broadcast: Fridays at 01:15 (JST)
 - Producers: Aniplex, Dentsu, Movic, Fuji TV, Fuji Pacific Music Publishing 
 - Licensors: Funimation 
 - Studios: Production I.G 
 - Source: Original
 - Genres: Action, Sci-Fi, Super Power, Drama, Romance, Mecha
 - Duration: 24 min. per ep.
 - Rating: R - 17+ (violence & profanity)


## Links

- [[Refrain] Guilty Crown - 01 VOSTFR [720p]](https://uptobox.com/eocwfsqc9vrr)
- [[Refrain] Guilty Crown - 02 VOSTFR [720p]](https://uptobox.com/8pxw8ysbi559)
- [[Refrain] Guilty Crown - 03 VOSTFR [720p]](https://uptobox.com/y8td628lfy7i)
- [[Refrain] Guilty Crown - 04 VOSTFR [720p]](https://uptobox.com/1m57vrenkrd3)
- [[Refrain] Guilty Crown - 05 VOSTFR [720p]](https://uptobox.com/c4sg1ivcl55o)
- [[Refrain] Guilty Crown - 06 VOSTFR [720p]](https://uptobox.com/53y3nyt68q9v)
- [[Refrain] Guilty Crown - 07 VOSTFR [720p]](https://uptobox.com/kk9tol9y14i3)
- [[Refrain] Guilty Crown - 08 VOSTFR [720p]](https://uptobox.com/ssaxhj4xanqx)
- [[Refrain] Guilty Crown - 09 VOSTFR [720p]](https://uptobox.com/r6ek8tl1j8fm)
- [[Refrain] Guilty Crown - 10 VOSTFR [720p]](https://uptobox.com/uxutydubejjp)
- [[Refrain] Guilty Crown - 11 VOSTFR [720p]](https://uptobox.com/q9js6vgla0ae)
- [[Refrain] Guilty Crown - 12 VOSTFR [720p]](https://uptobox.com/z6jo7eo7c7za)
- [[Refrain] Guilty Crown - 13 VOSTFR [720p]](https://uptobox.com/sqpo34wzrszp)
- [[Refrain] Guilty Crown - 14 VOSTFR [720p]](https://uptobox.com/y9exgnw3ksa7)
- [[Refrain] Guilty Crown - 15 VOSTFR [720p]](https://uptobox.com/69ji1b3c1vmb)
- [[Refrain] Guilty Crown - 16 VOSTFR [720p]](https://uptobox.com/qsyxpqb4o85e)
- [[Refrain] Guilty Crown - 17 VOSTFR [720p]](https://uptobox.com/my2yt95mgdee)
- [[Refrain] Guilty Crown - 18 VOSTFR [720p]](https://uptobox.com/lkirzh76la22)
- [[Refrain] Guilty Crown - 19 VOSTFR [720p]](https://uptobox.com/cyhsua6opa1q)
- [[Refrain] Guilty Crown - 20 VOSTFR [720p]](https://uptobox.com/aya8yu6i6c8j)
- [[Refrain] Guilty Crown - 21 VOSTFR [720p]](https://uptobox.com/76q0trfv5xhq)
- [[Refrain] Guilty Crown - 22 VOSTFR [720p]](https://uptobox.com/vn6qgj4v6kua)
