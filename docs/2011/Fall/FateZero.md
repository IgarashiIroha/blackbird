# Fate/Zero

![Fate/Zero](https://cdn.myanimelist.net/images/anime/2/73249l.jpg)

* Japanese:  フェイト/ゼロ

## Information

 - Type: TV
 - Episodes: 13
 - Status: Finished Airing
 - Aired: Oct 2, 2011 to Dec 25, 2011
 - Premiered: Fall 2011
 - Broadcast: Sundays at 00:00 (JST)
 - Producers: Aniplex, Nitroplus, Seikaisha, Notes 
 - Licensors: Aniplex of America 
 - Studios: ufotable 
 - Source: Light novel
 - Genres: Action, Supernatural, Magic, Fantasy
 - Duration: 28 min. per ep.
 - Rating: R - 17+ (violence & profanity)


## Links

- [[Refrain] FateZero - 01 VOSTFR [720p]](https://uptobox.com/6vaiwwehrpsc)
- [[Refrain] FateZero - 02 VOSTFR [720p]](https://uptobox.com/c6gxszzy26w5)
- [[Refrain] FateZero - 03 VOSTFR [720p]](https://uptobox.com/kctnlvpd2ug8)
- [[Refrain] FateZero - 04 VOSTFR [720p]](https://uptobox.com/qk4lmks2trr6)
- [[Refrain] FateZero - 05 VOSTFR [720p]](https://uptobox.com/jpctc7klr8n3)
- [[Refrain] FateZero - 06 VOSTFR [720p]](https://uptobox.com/iry5vrrwza2s)
- [[Refrain] FateZero - 07 VOSTFR [720p]](https://uptobox.com/59hf6glcidv8)
- [[Refrain] FateZero - 08 VOSTFR [720p]](https://uptobox.com/08jvitbf02hq)
- [[Refrain] FateZero - 09 VOSTFR [720p]](https://uptobox.com/orh8qswqxqzr)
- [[Refrain] FateZero - 10 VOSTFR [720p]](https://uptobox.com/jud2rn7csu9h)
- [[Refrain] FateZero - 11 VOSTFR [720p]](https://uptobox.com/8bvk08umoups)
- [[Refrain] FateZero - 12 VOSTFR [720p]](https://uptobox.com/wcdk3ei1mx90)
- [[Refrain] FateZero - 13 VOSTFR [720p]](https://uptobox.com/bt5q49nz3903)
- [[Refrain] FateZero 2nd Season - 01 VOSTFR [720p]](https://uptobox.com/07etnc2gx50u)
- [[Refrain] FateZero 2nd Season - 02 VOSTFR [720p]](https://uptobox.com/gcokt7j56gjs)
- [[Refrain] FateZero 2nd Season - 03 VOSTFR [720p]](https://uptobox.com/eto07okd913z)
- [[Refrain] FateZero 2nd Season - 04 VOSTFR [720p]](https://uptobox.com/e94bxgahm9dr)
- [[Refrain] FateZero 2nd Season - 05 VOSTFR [720p]](https://uptobox.com/mhzibq8k1o1m)
- [[Refrain] FateZero 2nd Season - 06 VOSTFR [720p]](https://uptobox.com/f5b1tj1muzj7)
- [[Refrain] FateZero 2nd Season - 07 VOSTFR [720p]](https://uptobox.com/sxpvocnjqqc7)
- [[Refrain] FateZero 2nd Season - 08 VOSTFR [720p]](https://uptobox.com/4sy1a6km66rj)
- [[Refrain] FateZero 2nd Season - 09 VOSTFR [720p]](https://uptobox.com/mo3b4yeub4zl)
- [[Refrain] FateZero 2nd Season - 10 VOSTFR [720p]](https://uptobox.com/qui8vwetr8yw)
- [[Refrain] FateZero 2nd Season - 11 VOSTFR [720p]](https://uptobox.com/c7gpkw1g8kg1)
