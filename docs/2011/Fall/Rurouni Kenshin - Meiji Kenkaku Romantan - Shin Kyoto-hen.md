# Rurouni Kenshin: Meiji Kenkaku Romantan - Shin Kyoto-hen

![Rurouni Kenshin: Meiji Kenkaku Romantan - Shin Kyoto-hen](https://cdn.myanimelist.net/images/anime/2/39663l.jpg)

* Japanese:  るろうに剣心-明治剣客浪漫譚- 新京都編

## Information

 - Type: OVA
 - Episodes: 2
 - Status: Finished Airing
 - Aired: Dec 17, 2011 to Jun 23, 2012
 - Producers: Aniplex 
 - Licensors: Sentai Filmworks 
 - Studios: Studio Deen 
 - Source: Manga
 - Genres: Action, Samurai, Historical, Drama, Shounen
 - Duration: 46 min. per ep.
 - Rating: R - 17+ (violence & profanity)


## Links

