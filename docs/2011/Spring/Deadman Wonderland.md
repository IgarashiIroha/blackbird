# Deadman Wonderland

![Deadman Wonderland](https://cdn.myanimelist.net/images/anime/9/75299l.jpg)

* Japanese:  デッドマン・ワンダーランド

## Information

 - Type: TV
 - Episodes: 12
 - Status: Finished Airing
 - Aired: Apr 17, 2011 to Jul 3, 2011
 - Premiered: Spring 2011
 - Broadcast: Sundays at 01:00 (JST)
 - Producers: Kadokawa Shoten, Kadokawa Pictures Japan, KlockWorx, AMG MUSIC 
 - Licensors: Funimation 
 - Studios: Manglobe 
 - Source: Manga
 - Genres: Action, Sci-Fi, Horror, Shounen
 - Duration: 24 min. per ep.
 - Rating: R - 17+ (violence & profanity)


## Links

- [[Refrain] Deadman Wonderland - 01 VF VOSTFR [1080p]](https://uptobox.com/4w71g81ytny2)
- [[Refrain] Deadman Wonderland - 01 VF VOSTFR [720p]](https://uptobox.com/sfa45ptie3ab)
- [[Refrain] Deadman Wonderland - 02 VF VOSTFR [1080p]](https://uptobox.com/6ir3w1tzhje4)
- [[Refrain] Deadman Wonderland - 02 VF VOSTFR [720p]](https://uptobox.com/y67fpk6b4ruu)
- [[Refrain] Deadman Wonderland - 03 VF VOSTFR [1080p]](https://uptobox.com/5zjmeaobb5gd)
- [[Refrain] Deadman Wonderland - 03 VF VOSTFR [720p]](https://uptobox.com/2gky7vqwe1qo)
- [[Refrain] Deadman Wonderland - 04 VF VOSTFR [1080p]](https://uptobox.com/c6d0nemj3by7)
- [[Refrain] Deadman Wonderland - 04 VF VOSTFR [720p]](https://uptobox.com/465uekljxgxe)
- [[Refrain] Deadman Wonderland - 05 VF VOSTFR [1080p]](https://uptobox.com/2yj18ekbd15j)
- [[Refrain] Deadman Wonderland - 05 VF VOSTFR [720p]](https://uptobox.com/vj88k3ei6den)
- [[Refrain] Deadman Wonderland - 06 VF VOSTFR [1080p]](https://uptobox.com/8yfm89ri0isq)
- [[Refrain] Deadman Wonderland - 06 VF VOSTFR [720p]](https://uptobox.com/3mqjimw7hfjp)
- [[Refrain] Deadman Wonderland - 07 VF VOSTFR [1080p]](https://uptobox.com/hqw0rull9usg)
- [[Refrain] Deadman Wonderland - 07 VF VOSTFR [720p]](https://uptobox.com/f4ws88l43w5i)
- [[Refrain] Deadman Wonderland - 08 VF VOSTFR [1080p]](https://uptobox.com/q89d9elzm5b3)
- [[Refrain] Deadman Wonderland - 08 VF VOSTFR [720p]](https://uptobox.com/qafk6ac13x9w)
- [[Refrain] Deadman Wonderland - 09 VF VOSTFR [1080p]](https://uptobox.com/37rvd0h0j0k4)
- [[Refrain] Deadman Wonderland - 09 VF VOSTFR [720p]](https://uptobox.com/eidtpj5y42zt)
- [[Refrain] Deadman Wonderland - 10 VF VOSTFR [1080p]](https://uptobox.com/pjlmltbif6cb)
- [[Refrain] Deadman Wonderland - 10 VF VOSTFR [720p]](https://uptobox.com/ad3msuepkdtl)
- [[Refrain] Deadman Wonderland - 11 VF VOSTFR [1080p]](https://uptobox.com/zk1gqtsiwefs)
- [[Refrain] Deadman Wonderland - 11 VF VOSTFR [720p]](https://uptobox.com/4vd8lw1n34tq)
- [[Refrain] Deadman Wonderland - 12 VF VOSTFR [1080p]](https://uptobox.com/p8zqxwsfn84t)
- [[Refrain] Deadman Wonderland - 12 VF VOSTFR [720p]](https://uptobox.com/0uolab22o01y)
