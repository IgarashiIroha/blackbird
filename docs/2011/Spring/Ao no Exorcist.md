# Ao no Exorcist

![Ao no Exorcist](https://cdn.myanimelist.net/images/anime/10/75195l.jpg)

* Japanese:  青の祓魔師(エクソシスト)

## Information

 - Type: TV
 - Episodes: 25
 - Status: Finished Airing
 - Aired: Apr 17, 2011 to Oct 2, 2011
 - Premiered: Spring 2011
 - Broadcast: Sundays at 17:00 (JST)
 - Producers: Aniplex, Dentsu, Mainichi Broadcasting System, Movic, Sakura Create, Shueisha 
 - Licensors: Aniplex of America 
 - Studios: A-1 Pictures 
 - Source: Manga
 - Genres: Action, Demons, Fantasy, Shounen, Supernatural
 - Duration: 24 min. per ep.
 - Rating: PG-13 - Teens 13 or older


## Links

- [[Refrain] Ao no Exorcist - 01 VF VOSTFR [1080p]](https://uptobox.com/1vka1al2c6i9)
- [[Refrain] Ao no Exorcist - 01 VF VOSTFR [720p]](https://uptobox.com/3asaeljymzy0)
- [[Refrain] Ao no Exorcist - 02 VF VOSTFR [1080p]](https://uptobox.com/oh7cs081sa8o)
- [[Refrain] Ao no Exorcist - 02 VF VOSTFR [720p]](https://uptobox.com/d9utyr1upuib)
- [[Refrain] Ao no Exorcist - 03 VF VOSTFR [1080p]](https://uptobox.com/dndzd11gsdmw)
- [[Refrain] Ao no Exorcist - 03 VF VOSTFR [720p]](https://uptobox.com/0h70czjvrq44)
- [[Refrain] Ao no Exorcist - 04 VF VOSTFR [1080p]](https://uptobox.com/zn2t2h2jqrj4)
- [[Refrain] Ao no Exorcist - 04 VF VOSTFR [720p]](https://uptobox.com/pvx17tk94g1d)
- [[Refrain] Ao no Exorcist - 05 VF VOSTFR [1080p]](https://uptobox.com/37t6djtl5k5b)
- [[Refrain] Ao no Exorcist - 05 VF VOSTFR [720p]](https://uptobox.com/wtk7le9t20qk)
- [[Refrain] Ao no Exorcist - 06 VF VOSTFR [1080p]](https://uptobox.com/p5gx1nxs1cg6)
- [[Refrain] Ao no Exorcist - 06 VF VOSTFR [720p]](https://uptobox.com/wlyr9bp3o68w)
- [[Refrain] Ao no Exorcist - 07 VF VOSTFR [1080p]](https://uptobox.com/q0hdtdlcr8su)
- [[Refrain] Ao no Exorcist - 07 VF VOSTFR [720p]](https://uptobox.com/2fx5vf6n3dhs)
- [[Refrain] Ao no Exorcist - 08 VF VOSTFR [1080p]](https://uptobox.com/uecm6kl7f9pn)
- [[Refrain] Ao no Exorcist - 08 VF VOSTFR [720p]](https://uptobox.com/jup550r92vww)
- [[Refrain] Ao no Exorcist - 09 VF VOSTFR [1080p]](https://uptobox.com/gd2kublxrz3e)
- [[Refrain] Ao no Exorcist - 09 VF VOSTFR [720p]](https://uptobox.com/8rwrfox0hyeu)
- [[Refrain] Ao no Exorcist - 10 VF VOSTFR [1080p]](https://uptobox.com/2dqs7gf3vxz9)
- [[Refrain] Ao no Exorcist - 10 VF VOSTFR [720p]](https://uptobox.com/9j49wg1qtc0s)
- [[Refrain] Ao no Exorcist - 11 VF VOSTFR [1080p]](https://uptobox.com/qh1psko9ktox)
- [[Refrain] Ao no Exorcist - 11 VF VOSTFR [720p]](https://uptobox.com/7fzu2tjda57f)
- [[Refrain] Ao no Exorcist - 12 VF VOSTFR [720p]](https://uptobox.com/o5ne0to76ejy)
- [[Refrain] Ao no Exorcist - 13 VF VOSTFR [1080p]](https://uptobox.com/g2ssklmm12zk)
- [[Refrain] Ao no Exorcist - 13 VF VOSTFR [720p]](https://uptobox.com/1uxht08bhuo4)
- [[Refrain] Ao no Exorcist - 14 VF VOSTFR [1080p]](https://uptobox.com/66rudeosxmmt)
- [[Refrain] Ao no Exorcist - 14 VF VOSTFR [720p]](https://uptobox.com/nweh5ryu8kgf)
- [[Refrain] Ao no Exorcist - 15 VF VOSTFR [1080p]](https://uptobox.com/75ozxl7kegjm)
- [[Refrain] Ao no Exorcist - 15 VF VOSTFR [720p]](https://uptobox.com/2ccfonj656n1)
- [[Refrain] Ao no Exorcist - 16 VF VOSTFR [1080p]](https://uptobox.com/tnsm44xl8snr)
- [[Refrain] Ao no Exorcist - 16 VF VOSTFR [720p]](https://uptobox.com/ysm3iheu6p4r)
- [[Refrain] Ao no Exorcist - 17 VF VOSTFR [1080p]](https://uptobox.com/sf0894r8t02x)
- [[Refrain] Ao no Exorcist - 17 VF VOSTFR [720p]](https://uptobox.com/7lpftetqtlog)
- [[Refrain] Ao no Exorcist - 18 VF VOSTFR [1080p]](https://uptobox.com/9al73oo1nr1b)
- [[Refrain] Ao no Exorcist - 18 VF VOSTFR [1080p]](https://uptobox.com/38kml5lau5c0)
- [[Refrain] Ao no Exorcist - 18 VF VOSTFR [720p]](https://uptobox.com/p5e3f2vclg0y)
- [[Refrain] Ao no Exorcist - 19 VF VOSTFR [1080p]](https://uptobox.com/meik4zrd56j8)
- [[Refrain] Ao no Exorcist - 19 VF VOSTFR [720p]](https://uptobox.com/5pfdjrg1c3w2)
- [[Refrain] Ao no Exorcist - 20 VF VOSTFR [1080p]](https://uptobox.com/dhtv6yuhzviq)
- [[Refrain] Ao no Exorcist - 20 VF VOSTFR [720p]](https://uptobox.com/rlbz1zhc5gtm)
- [[Refrain] Ao no Exorcist - 21 VF VOSTFR [1080p]](https://uptobox.com/3gwa8bdnjqpa)
- [[Refrain] Ao no Exorcist - 21 VF VOSTFR [720p]](https://uptobox.com/q9o7u2uefgw5)
- [[Refrain] Ao no Exorcist - 22 VF VOSTFR [1080p]](https://uptobox.com/8trxhrz4dxkh)
- [[Refrain] Ao no Exorcist - 22 VF VOSTFR [720p]](https://uptobox.com/v1pn7wq4a8xf)
- [[Refrain] Ao no Exorcist - 23 VF VOSTFR [1080p]](https://uptobox.com/um0lmr53gcfr)
- [[Refrain] Ao no Exorcist - 23 VF VOSTFR [720p]](https://uptobox.com/wmqcatt7yzfx)
- [[Refrain] Ao no Exorcist - 24 VF VOSTFR [1080p]](https://uptobox.com/hvo0gbnx8tq4)
- [[Refrain] Ao no Exorcist - 24 VF VOSTFR [720p]](https://uptobox.com/o5oxl4cbmvmc)
- [[Refrain] Ao no Exorcist - 25 VF VOSTFR [1080p]](https://uptobox.com/v7e76fc40mv3)
- [[Refrain] Ao no Exorcist - 25 VF VOSTFR [720p]](https://uptobox.com/ctiq2tp9c72d)
- [[Refrain] Ao no Exorcist - Kyoto Fujouou-hen - 01 VF VOSTFR [1080p]](https://uptobox.com/tzg398ixgdet)
- [[Refrain] Ao no Exorcist - Kyoto Fujouou-hen - 01 VF VOSTFR [720p]](https://uptobox.com/hio523o2cjbr)
- [[Refrain] Ao no Exorcist - Kyoto Fujouou-hen - 02 VF VOSTFR [1080p]](https://uptobox.com/o6tb96zuuz73)
- [[Refrain] Ao no Exorcist - Kyoto Fujouou-hen - 02 VF VOSTFR [720p]](https://uptobox.com/gjheuejm3ow7)
- [[Refrain] Ao no Exorcist - Kyoto Fujouou-hen - 03 VF VOSTFR [1080p]](https://uptobox.com/v6cnadlbc7uf)
- [[Refrain] Ao no Exorcist - Kyoto Fujouou-hen - 03 VF VOSTFR [720p]](https://uptobox.com/sbg4f3x0z2gg)
- [[Refrain] Ao no Exorcist - Kyoto Fujouou-hen - 04 VF VOSTFR [1080p]](https://uptobox.com/ul8uoqg9jf2d)
- [[Refrain] Ao no Exorcist - Kyoto Fujouou-hen - 04 VF VOSTFR [720p]](https://uptobox.com/jup4jdtj3och)
- [[Refrain] Ao no Exorcist - Kyoto Fujouou-hen - 05 VF VOSTFR [1080p]](https://uptobox.com/8igsvwzemfap)
- [[Refrain] Ao no Exorcist - Kyoto Fujouou-hen - 05 VF VOSTFR [720p]](https://uptobox.com/09gzzdbbwwm0)
- [[Refrain] Ao no Exorcist - Kyoto Fujouou-hen - 06 VF VOSTFR [1080p]](https://uptobox.com/7upst65tuqtq)
- [[Refrain] Ao no Exorcist - Kyoto Fujouou-hen - 06 VF VOSTFR [720p]](https://uptobox.com/lv3srogm9ywf)
- [[Refrain] Ao no Exorcist - Kyoto Fujouou-hen - 07 VF VOSTFR [1080p]](https://uptobox.com/c91w6fcjo409)
- [[Refrain] Ao no Exorcist - Kyoto Fujouou-hen - 07 VF VOSTFR [720p]](https://uptobox.com/sht9r9kq92w2)
- [[Refrain] Ao no Exorcist - Kyoto Fujouou-hen - 08 VF VOSTFR [1080p]](https://uptobox.com/43yzm7p52gp3)
- [[Refrain] Ao no Exorcist - Kyoto Fujouou-hen - 08 VF VOSTFR [720p]](https://uptobox.com/8fl2p44zh239)
- [[Refrain] Ao no Exorcist - Kyoto Fujouou-hen - 09 VF VOSTFR [1080p]](https://uptobox.com/2rg537bn6ze0)
- [[Refrain] Ao no Exorcist - Kyoto Fujouou-hen - 09 VF VOSTFR [720p]](https://uptobox.com/e9t7l7vilsum)
- [[Refrain] Ao no Exorcist - Kyoto Fujouou-hen - 10 VF VOSTFR [1080p]](https://uptobox.com/sw8qkp0w3sb3)
- [[Refrain] Ao no Exorcist - Kyoto Fujouou-hen - 10 VF VOSTFR [720p]](https://uptobox.com/cjo7g1fv2j6b)
- [[Refrain] Ao no Exorcist - Kyoto Fujouou-hen - 11 VF VOSTFR [1080p]](https://uptobox.com/f5hftdbwvfsa)
- [[Refrain] Ao no Exorcist - Kyoto Fujouou-hen - 11 VF VOSTFR [720p]](https://uptobox.com/q5c5dekuzu0n)
- [[Refrain] Ao no Exorcist Kuro no Iede - 00 VF VOSTFR [1080p]](https://uptobox.com/k9u58myurfr0)
- [[Refrain] Ao no Exorcist Kuro no Iede - 00 VF VOSTFR [720p]](https://uptobox.com/myxtldnjjtj9)
- [[Refrain] Ao no Exorcist - Kyoto Fujouou-hen - 11 VF VOSTFR [720p]](https://uptobox.com/3h2rzyuypksp)
- [[Refrain] Ao no Exorcist - Kyoto Fujouou-hen - 12 VF VOSTFR [1080p]](https://uptobox.com/hpuuug1ylch1)
- [[Refrain] Ao no Exorcist - Kyoto Fujouou-hen - 12 VF VOSTFR [720p]](https://uptobox.com/u4lfw1cfb057)
