# Orange

![Orange](https://cdn.myanimelist.net/images/anime/4/80110l.jpg)

* Japanese:  orange（オレンジ）

## Information

 - Type: TV
 - Episodes: 13
 - Status: Finished Airing
 - Aired: Jul 4, 2016 to Sep 26, 2016
 - Premiered: Summer 2016
 - Broadcast: Mondays at 00:00 (JST)
 - Producers: TMS Entertainment, DAX Production, TOHO animation, Lawson, ABC Animation 
 - Licensors: Funimation, Crunchyroll 
 - Studios: Telecom Animation Film 
 - Source: Manga
 - Genres: Sci-Fi, Drama, Romance, School, Shoujo
 - Duration: 24 min. per ep.
 - Rating: PG-13 - Teens 13 or older


## Links

- [[Refrain] Orange - 01 VF [1080p]](https://uptobox.com/lx5tagrrbrv9)
- [[Refrain] Orange - 01 VF [720p]](https://uptobox.com/85i2gwdgs16j)
- [[Refrain] Orange - 01 VOSTFR [1080p]](https://uptobox.com/xjix2sucl7rj)
- [[Refrain] Orange - 01 VOSTFR [720p]](https://uptobox.com/qraaa31raz8m)
- [[Refrain] Orange - 02 VF [1080p]](https://uptobox.com/hkywomsyvxiy)
- [[Refrain] Orange - 02 VF [720p]](https://uptobox.com/v20a24packai)
- [[Refrain] Orange - 02 VOSTFR [1080p]](https://uptobox.com/6pnkgjg3ndc2)
- [[Refrain] Orange - 02 VOSTFR [720p]](https://uptobox.com/5u1qkw285um3)
- [[Refrain] Orange - 03 VF [1080p]](https://uptobox.com/nm1ode66ogtq)
- [[Refrain] Orange - 03 VF [720p]](https://uptobox.com/uckch1d4hdol)
- [[Refrain] Orange - 03 VOSTFR [1080p]](https://uptobox.com/ht1p08lh2chk)
- [[Refrain] Orange - 03 VOSTFR [720p]](https://uptobox.com/o7375ceiup5h)
- [[Refrain] Orange - 04 VF [1080p]](https://uptobox.com/7fl6f2qs4xdg)
- [[Refrain] Orange - 04 VF [720p]](https://uptobox.com/722ijdqlwqnl)
- [[Refrain] Orange - 04 VOSTFR [1080p]](https://uptobox.com/m8ji1kjcg136)
- [[Refrain] Orange - 04 VOSTFR [720p]](https://uptobox.com/acs6us78b0k3)
- [[Refrain] Orange - 05 VF [1080p]](https://uptobox.com/d1mqjmu9m9rv)
- [[Refrain] Orange - 05 VF [720p]](https://uptobox.com/mg710qohlv72)
- [[Refrain] Orange - 05 VOSTFR [1080p]](https://uptobox.com/er7at4zujk3s)
- [[Refrain] Orange - 05 VOSTFR [720p]](https://uptobox.com/ljcvhxjhgr6k)
- [[Refrain] Orange - 06 VF [1080p]](https://uptobox.com/luy5yi0j1olh)
- [[Refrain] Orange - 06 VF [720p]](https://uptobox.com/5cjdowgrgedy)
- [[Refrain] Orange - 06 VOSTFR [1080p]](https://uptobox.com/xjuqr9detts1)
- [[Refrain] Orange - 06 VOSTFR [720p]](https://uptobox.com/v44n2v594647)
- [[Refrain] Orange - 07 VF [1080p]](https://uptobox.com/ma4hgnv8jjbe)
- [[Refrain] Orange - 07 VF [720p]](https://uptobox.com/vnc2pwcq1hvc)
- [[Refrain] Orange - 07 VOSTFR [1080p]](https://uptobox.com/g6uwg0xosih3)
- [[Refrain] Orange - 07 VOSTFR [720p]](https://uptobox.com/zfqrvfp9a6fn)
- [[Refrain] Orange - 08 VF [1080p]](https://uptobox.com/z3g3rx4qaqbv)
- [[Refrain] Orange - 08 VF [720p]](https://uptobox.com/us8vngykn7kl)
- [[Refrain] Orange - 08 VOSTFR [1080p]](https://uptobox.com/m5lvzoex8e2m)
- [[Refrain] Orange - 08 VOSTFR [720p]](https://uptobox.com/0zldnnimbecq)
- [[Refrain] Orange - 09 VF [1080p]](https://uptobox.com/fuqupz7sj0ls)
- [[Refrain] Orange - 09 VF [720p]](https://uptobox.com/n3vqzfvcm82t)
- [[Refrain] Orange - 09 VOSTFR [1080p]](https://uptobox.com/rleswb9jr454)
- [[Refrain] Orange - 09 VOSTFR [720p]](https://uptobox.com/boyv3nho2nm2)
- [[Refrain] Orange - 10 VF [1080p]](https://uptobox.com/peedyhzbwlas)
- [[Refrain] Orange - 10 VF [720p]](https://uptobox.com/rg6z7rdiywpj)
- [[Refrain] Orange - 10 VOSTFR [1080p]](https://uptobox.com/ougiqadrph34)
- [[Refrain] Orange - 10 VOSTFR [720p]](https://uptobox.com/uv7onslcfcr0)
- [[Refrain] Orange - 11 VF [1080p]](https://uptobox.com/fbqb31xqvlch)
- [[Refrain] Orange - 11 VF [720p]](https://uptobox.com/utin63rmeyj0)
- [[Refrain] Orange - 11 VOSTFR [1080p]](https://uptobox.com/8bel7ok8hqx3)
- [[Refrain] Orange - 11 VOSTFR [720p]](https://uptobox.com/sbrzkebso568)
- [[Refrain] Orange - 12 VF [1080p]](https://uptobox.com/9ejie6bao00k)
- [[Refrain] Orange - 12 VF [720p]](https://uptobox.com/gx496n4octw2)
- [[Refrain] Orange - 12 VOSTFR [1080p]](https://uptobox.com/ipg45ylj8ars)
- [[Refrain] Orange - 12 VOSTFR [720p]](https://uptobox.com/50hh24ehzywh)
- [[Refrain] Orange - 13 VF [1080p]](https://uptobox.com/hfynx6en7pvj)
- [[Refrain] Orange - 13 VF [720p]](https://uptobox.com/t9d17xb5t6sn)
- [[Refrain] Orange - 13 VOSTFR [1080p]](https://uptobox.com/5s6byn09ibxu)
- [[Refrain] Orange - 13 VOSTFR [720p]](https://uptobox.com/o1cei5vpks1u)
