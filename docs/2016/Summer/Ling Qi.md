# Ling Qi

![Ling Qi](https://cdn.myanimelist.net/images/anime/9/81045l.jpg)

* Japanese:  灵契

## Information

 - Type: ONA
 - Episodes: 20
 - Status: Finished Airing
 - Aired: Jun 21, 2016 to Nov 1, 2016
 - Producers: Tencent Animation & Comics
 - Licensors: None found, add some
 - Studios: Haoliners Animation League
 - Source: Web manga
 - Genres: Action, Comedy, Supernatural, Magic, Shounen Ai
 - Duration: 14 min. per ep.
 - Rating: PG-13 - Teens 13 or older


## Links

- [[Refrain] Ling Qi - 01 VOSTFR [720p]](https://uptobox.com/uuk2rstswe6t)
- [[Refrain] Ling Qi - 01 VOSTFR [1080p]](https://uptobox.com/f94ol4nwx520)
- [[Refrain] Ling Qi - 02 VOSTFR [720p]](https://uptobox.com/gldh8h4pb68t)
- [[Refrain] Ling Qi - 02 VOSTFR [1080p]](https://uptobox.com/k1rbvhwbpsv7)
- [[Refrain] Ling Qi - 03 VOSTFR [720p]](https://uptobox.com/yqfl192nk6b5)
- [[Refrain] Ling Qi - 03 VOSTFR [1080p]](https://uptobox.com/0vw5vdny3pq3)
- [[Refrain] Ling Qi - 04 VOSTFR [720p]](https://uptobox.com/pyogvzlt3cgi)
- [[Refrain] Ling Qi - 04 VOSTFR [1080p]](https://uptobox.com/glcssck142oo)
- [[Refrain] Ling Qi - 05 VOSTFR [720p]](https://uptobox.com/lzhmi418fuis)
- [[Refrain] Ling Qi - 05 VOSTFR [1080p]](https://uptobox.com/md50xrl4yiys)
- [[Refrain] Ling Qi - 06 VOSTFR [720p]](https://uptobox.com/5t1ge3i2d9z5)
- [[Refrain] Ling Qi - 06 VOSTFR [1080p]](https://uptobox.com/1l5x1ujtm52w)
- [[Refrain] Ling Qi - 07 VOSTFR [720p]](https://uptobox.com/fs8dtakjc5av)
- [[Refrain] Ling Qi - 07 VOSTFR [1080p]](https://uptobox.com/k4bvi11r7wi5)
- [[Refrain] Ling Qi - 08 VOSTFR [720p]](https://uptobox.com/k6art17rtmce)
- [[Refrain] Ling Qi - 08 VOSTFR [1080p]](https://uptobox.com/lfanoqehml6r)
- [[Refrain] Ling Qi - 09 VOSTFR [720p]](https://uptobox.com/6az8qfg1vcmw)
- [[Refrain] Ling Qi - 09 VOSTFR [1080p]](https://uptobox.com/m0pnr9ovb87s)
- [[Refrain] Ling Qi - 10 VOSTFR [720p]](https://uptobox.com/tujcz4xsevva)
- [[Refrain] Ling Qi - 10 VOSTFR [1080p]](https://uptobox.com/vpyy48p4j2rh)
