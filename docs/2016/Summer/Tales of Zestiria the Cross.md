# Tales of Zestiria the Cross

![Tales of Zestiria the Cross](https://cdn.myanimelist.net/images/anime/3/79271l.jpg)

* Japanese:  テイルズ オブ ゼスティリア ザ クロス

## Information

 - Type: TV
 - Episodes: 12
 - Status: Finished Airing
 - Aired: Jul 10, 2016 to Sep 25, 2016
 - Premiered: Summer 2016
 - Broadcast: Sundays at 23:00 (JST)
 - Producers: Bandai Visual, Lantis, Bandai Namco Entertainment, Anime Consortium Japan 
 - Licensors: Funimation 
 - Studios: ufotable 
 - Source: Game
 - Genres: Action, Adventure, Magic, Fantasy
 - Duration: 23 min. per ep.
 - Rating: PG-13 - Teens 13 or older


## Links

- [[Refrain] Tales of Zestiria the Cross - 01 VF VOSTFR [1080p]](https://uptobox.com/vgnk6mogwq9h)
- [[Refrain] Tales of Zestiria the Cross - 01 VF VOSTFR [720p]](https://uptobox.com/r3fgaj8rgg0z)
- [[Refrain] Tales of Zestiria the Cross - 02 VF VOSTFR [1080p]](https://uptobox.com/wbypsqfgbtxn)
- [[Refrain] Tales of Zestiria the Cross - 02 VF VOSTFR [720p]](https://uptobox.com/kaqbjd0sti7n)
- [[Refrain] Tales of Zestiria the Cross - 03 VF VOSTFR [1080p]](https://uptobox.com/l2znqlu3rs7z)
- [[Refrain] Tales of Zestiria the Cross - 03 VF VOSTFR [720p]](https://uptobox.com/f3sw2tnm0vpk)
- [[Refrain] Tales of Zestiria the Cross - 04 VF VOSTFR [1080p]](https://uptobox.com/nqhwmc4czpn1)
- [[Refrain] Tales of Zestiria the Cross - 04 VF VOSTFR [720p]](https://uptobox.com/fm89km9n7lz6)
- [[Refrain] Tales of Zestiria the Cross - 05 VF VOSTFR [1080p]](https://uptobox.com/6j7sprs3y4zr)
- [[Refrain] Tales of Zestiria the Cross - 05 VF VOSTFR [720p]](https://uptobox.com/i6905bnlm1rg)
- [[Refrain] Tales of Zestiria the Cross - 06 VF VOSTFR [1080p]](https://uptobox.com/mxropr285udz)
- [[Refrain] Tales of Zestiria the Cross - 06 VF VOSTFR [720p]](https://uptobox.com/xkjemxt8wwi6)
- [[Refrain] Tales of Zestiria the Cross - 07 VF VOSTFR [1080p]](https://uptobox.com/6t86mesjnfab)
- [[Refrain] Tales of Zestiria the Cross - 07 VF VOSTFR [720p]](https://uptobox.com/2qejf73uhb2a)
- [[Refrain] Tales of Zestiria the Cross - 08 VF VOSTFR [1080p]](https://uptobox.com/0va9xzy47adp)
- [[Refrain] Tales of Zestiria the Cross - 08 VF VOSTFR [720p]](https://uptobox.com/ecr1ue4qj7hq)
- [[Refrain] Tales of Zestiria the Cross - 09 VF VOSTFR [1080p]](https://uptobox.com/v688dl2bhwf9)
- [[Refrain] Tales of Zestiria the Cross - 09 VF VOSTFR [720p]](https://uptobox.com/s917lxtyf4sn)
- [[Refrain] Tales of Zestiria the Cross - 10 VF VOSTFR [1080p]](https://uptobox.com/mv7m5q5t6wpd)
- [[Refrain] Tales of Zestiria the Cross - 10 VF VOSTFR [720p]](https://uptobox.com/pklrp4hw1ce9)
- [[Refrain] Tales of Zestiria the Cross - 11 VF VOSTFR [1080p]](https://uptobox.com/c7hjgbrj3ak8)
- [[Refrain] Tales of Zestiria the Cross - 11 VF VOSTFR [720p]](https://uptobox.com/qbl37fbdvtac)
- [[Refrain] Tales of Zestiria the Cross - 12 VF VOSTFR [1080p]](https://uptobox.com/70eorbf099w9)
- [[Refrain] Tales of Zestiria the Cross - 12 VF VOSTFR [720p]](https://uptobox.com/iudillo7qike)
- [[Refrain] Tales of Zestiria the Cross - 13 VF VOSTFR [1080p]](https://uptobox.com/2c589cjvh1z8)
- [[Refrain] Tales of Zestiria the Cross - 13 VF VOSTFR [720p]](https://uptobox.com/jdbythqn2c5d)
- [[Refrain] Tales of Zestiria the Cross 2nd Season - 01 VF VOSTFR [1080p]](https://uptobox.com/e2g09wqousbw)
- [[Refrain] Tales of Zestiria the Cross 2nd Season - 01 VF VOSTFR [720p]](https://uptobox.com/oxduesrbv13u)
- [[Refrain] Tales of Zestiria the Cross 2nd Season - 02 VF VOSTFR [1080p]](https://uptobox.com/fpa4wv9odwbd)
- [[Refrain] Tales of Zestiria the Cross 2nd Season - 02 VF VOSTFR [720p]](https://uptobox.com/zhrem69v3alq)
- [[Refrain] Tales of Zestiria the Cross 2nd Season - 03 VF VOSTFR [1080p]](https://uptobox.com/msjequlg3176)
- [[Refrain] Tales of Zestiria the Cross 2nd Season - 03 VF VOSTFR [720p]](https://uptobox.com/krwu5ms1f6z6)
- [[Refrain] Tales of Zestiria the Cross 2nd Season - 04 VF VOSTFR [1080p]](https://uptobox.com/kmj4bbcf7kkx)
- [[Refrain] Tales of Zestiria the Cross 2nd Season - 04 VF VOSTFR [720p]](https://uptobox.com/ltcff8v4d60c)
- [[Refrain] Tales of Zestiria the Cross 2nd Season - 05 VF VOSTFR [1080p]](https://uptobox.com/sqyp9qzjbu6r)
- [[Refrain] Tales of Zestiria the Cross 2nd Season - 05 VF VOSTFR [720p]](https://uptobox.com/rmsffhdb9dmm)
- [[Refrain] Tales of Zestiria the Cross 2nd Season - 06 VF VOSTFR [1080p]](https://uptobox.com/icjgbupciri8)
- [[Refrain] Tales of Zestiria the Cross 2nd Season - 06 VF VOSTFR [720p]](https://uptobox.com/hvoz1e946qdp)
- [[Refrain] Tales of Zestiria the Cross 2nd Season - 07 VF VOSTFR [1080p]](https://uptobox.com/1fyunr55mf3l)
- [[Refrain] Tales of Zestiria the Cross 2nd Season - 07 VF VOSTFR [720p]](https://uptobox.com/67cd2ubrvsx7)
- [[Refrain] Tales of Zestiria the Cross 2nd Season - 08 VF VOSTFR [1080p]](https://uptobox.com/rtf31q8shfos)
- [[Refrain] Tales of Zestiria the Cross 2nd Season - 08 VF VOSTFR [720p]](https://uptobox.com/nssvacsl1sy2)
- [[Refrain] Tales of Zestiria the Cross 2nd Season - 09 VF VOSTFR [1080p]](https://uptobox.com/za2xtpywcwrc)
- [[Refrain] Tales of Zestiria the Cross 2nd Season - 09 VF VOSTFR [720p]](https://uptobox.com/rr2w47wveb2l)
- [[Refrain] Tales of Zestiria the Cross 2nd Season - 10 VF VOSTFR [1080p]](https://uptobox.com/hv9haizc1kvj)
- [[Refrain] Tales of Zestiria the Cross 2nd Season - 10 VF VOSTFR [720p]](https://uptobox.com/d095rkyd64q4)
- [[Refrain] Tales of Zestiria the Cross 2nd Season - 11 VF VOSTFR [1080p]](https://uptobox.com/5uj5p1xxgo0s)
- [[Refrain] Tales of Zestiria the Cross 2nd Season - 11 VF VOSTFR [720p]](https://uptobox.com/ljxbahobe3k9)
- [[Refrain] Tales of Zestiria the Cross 2nd Season - 12 VF VOSTFR [1080p]](https://uptobox.com/v2wova4cmb3o)
- [[Refrain] Tales of Zestiria the Cross 2nd Season - 12 VF VOSTFR [720p]](https://uptobox.com/13bo5lh3uwx4)
- [[Refrain] Tales of Zestiria the Cross 2nd Season - 13 VF VOSTFR [1080p]](https://uptobox.com/qjpvzqoyjlyt)
