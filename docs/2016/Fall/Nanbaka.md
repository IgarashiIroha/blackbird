# Nanbaka

![Nanbaka](https://cdn.myanimelist.net/images/anime/2/81399l.jpg)

* Japanese:  ナンバカ

## Information

 - Type: TV
 - Episodes: 13
 - Status: Finished Airing
 - Aired: Oct 5, 2016 to Dec 28, 2016
 - Premiered: Fall 2016
 - Broadcast: Wednesdays at 03:00 (JST)
 - Producers: Nihon Ad Systems, DAX Production, Sony Music Communications, comico, Tokuma Japan Communications, Nelke Planning 
 - Licensors: Funimation 
 - Studios: Satelight 
 - Source: Web manga
 - Genres: Action, Comedy, Drama
 - Duration: 23 min. per ep.
 - Rating: PG-13 - Teens 13 or older


## Links

- [[Refrain] Nanbaka - 01 VOSTFR [1080p]](https://uptobox.com/96m1bxtvnakt)
- [[Refrain] Nanbaka - 01 VOSTFR [720p]](https://uptobox.com/rg1b0ol9my1q)
- [[Refrain] Nanbaka - 02 VOSTFR [1080p]](https://uptobox.com/aok80snzxi4p)
- [[Refrain] Nanbaka - 02 VOSTFR [720p]](https://uptobox.com/jae7zhbmcmni)
- [[Refrain] Nanbaka - 03 VOSTFR [1080p]](https://uptobox.com/ctodgkw1jgcn)
- [[Refrain] Nanbaka - 03 VOSTFR [720p]](https://uptobox.com/v5qirtsmbe23)
- [[Refrain] Nanbaka - 04 VOSTFR [1080p]](https://uptobox.com/y2pf177morgc)
- [[Refrain] Nanbaka - 04 VOSTFR [720p]](https://uptobox.com/if2c2hopy7my)
- [[Refrain] Nanbaka - 05 VOSTFR [1080p]](https://uptobox.com/zouvcgxh9g7r)
- [[Refrain] Nanbaka - 05 VOSTFR [720p]](https://uptobox.com/87097awcu69f)
- [[Refrain] Nanbaka - 06 VOSTFR [1080p]](https://uptobox.com/i48fptgo76jz)
- [[Refrain] Nanbaka - 06 VOSTFR [720p]](https://uptobox.com/i0mnyvo4nc5a)
- [[Refrain] Nanbaka - 07 VOSTFR [1080p]](https://uptobox.com/gxuyon4p4969)
- [[Refrain] Nanbaka - 07 VOSTFR [720p]](https://uptobox.com/skaj8udt7a45)
- [[Refrain] Nanbaka - 08 VOSTFR [1080p]](https://uptobox.com/78y5l5mnez3k)
- [[Refrain] Nanbaka - 08 VOSTFR [720p]](https://uptobox.com/hw8q9bbkba5l)
- [[Refrain] Nanbaka - 09 VOSTFR [1080p]](https://uptobox.com/n3zsgp597p4y)
- [[Refrain] Nanbaka - 09 VOSTFR [720p]](https://uptobox.com/5sfc8xed9bu5)
- [[Refrain] Nanbaka - 10 VOSTFR [1080p]](https://uptobox.com/mamk6rpvzszg)
- [[Refrain] Nanbaka - 10 VOSTFR [720p]](https://uptobox.com/s2d08jkhm6d8)
- [[Refrain] Nanbaka - 11 VOSTFR [1080p]](https://uptobox.com/babympcbcfst)
- [[Refrain] Nanbaka - 11 VOSTFR [720p]](https://uptobox.com/rg8m2zig54vw)
- [[Refrain] Nanbaka - 12 VOSTFR [1080p]](https://uptobox.com/5o7hpdvrqtlc)
- [[Refrain] Nanbaka - 12 VOSTFR [720p]](https://uptobox.com/umrox8ywcsvm)
- [[Refrain] Nanbaka - 13 VOSTFR [1080p]](https://uptobox.com/9u8lkez25it8)
- [[Refrain] Nanbaka - 13 VOSTFR [720p]](https://uptobox.com/yr4p08bpyieu)
- [[Refrain] Nanbaka 2 - 14 VOSTFR [1080p]](https://uptobox.com/nl33wvxln3tt)
- [[Refrain] Nanbaka 2 - 14 VOSTFR [720p]](https://uptobox.com/emc3gmbzxz2n)
- [[Refrain] Nanbaka 2 - 15 VOSTFR [1080p]](https://uptobox.com/md67azslnnys)
- [[Refrain] Nanbaka 2 - 15 VOSTFR [720p]](https://uptobox.com/n39qzcf9736b)
- [[Refrain] Nanbaka 2 - 16 VOSTFR [1080p]](https://uptobox.com/6o40geawy71q)
- [[Refrain] Nanbaka 2 - 16 VOSTFR [720p]](https://uptobox.com/hpve7tf5tkzh)
- [[Refrain] Nanbaka 2 - 17 VOSTFR [1080p]](https://uptobox.com/j1yln66k6afc)
- [[Refrain] Nanbaka 2 - 17 VOSTFR [720p]](https://uptobox.com/s143ffxoq2dd)
- [[Refrain] Nanbaka 2 - 18 VOSTFR [1080p]](https://uptobox.com/nat27sy0uboy)
- [[Refrain] Nanbaka 2 - 18 VOSTFR [720p]](https://uptobox.com/i9pqo3ulm8yd)
- [[Refrain] Nanbaka 2 - 19 VOSTFR [1080p]](https://uptobox.com/qkhf9ngla96m)
- [[Refrain] Nanbaka 2 - 19 VOSTFR [720p]](https://uptobox.com/kgeai0l0ftd0)
- [[Refrain] Nanbaka 2 - 20 VOSTFR [1080p]](https://uptobox.com/nlfqob0nad6c)
- [[Refrain] Nanbaka 2 - 20 VOSTFR [720p]](https://uptobox.com/47e084fw5247)
- [[Refrain] Nanbaka 2 - 21 VOSTFR [1080p]](https://uptobox.com/bv412xo86rxd)
- [[Refrain] Nanbaka 2 - 21 VOSTFR [720p]](https://uptobox.com/p2atxcqse3sl)
- [[Refrain] Nanbaka 2 - 22 VOSTFR [1080p]](https://uptobox.com/uqia1m26pkhr)
- [[Refrain] Nanbaka 2 - 22 VOSTFR [720p]](https://uptobox.com/v2ya0rgc3xcy)
- [[Refrain] Nanbaka 2 - 23 VOSTFR [1080p]](https://uptobox.com/0moyakb8me69)
- [[Refrain] Nanbaka 2 - 23 VOSTFR [720p]](https://uptobox.com/h50ww84qiizt)
- [[Refrain] Nanbaka 2 - 24 VOSTFR [1080p]](https://uptobox.com/8e7yvclr65ld)
- [[Refrain] Nanbaka 2 - 24 VOSTFR [720p]](https://uptobox.com/sooeoaq155i0)
- [[Refrain] Nanbaka 2 - 25 VOSTFR [1080p]](https://uptobox.com/jl1hcmneegmq)
- [[Refrain] Nanbaka 2 - 25 VOSTFR [720p]](https://uptobox.com/o4hcbn8urkjb)
- [[Refrain] Nanbaka 2 - 26 VOSTFR [1080p]](https://uptobox.com/ehg01uf86962)
- [[Refrain] Nanbaka 2 - 26 VOSTFR [720p]](https://uptobox.com/5mx7z0nmbi1v)
