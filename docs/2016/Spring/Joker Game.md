# Joker Game

![Joker Game](https://cdn.myanimelist.net/images/anime/9/77523l.jpg)

* Japanese:  ジョーカー・ゲーム

## Information

 - Type: TV
 - Episodes: 12
 - Status: Finished Airing
 - Aired: Apr 5, 2016 to Jun 21, 2016
 - Premiered: Spring 2016
 - Broadcast: Tuesdays at 23:00 (JST)
 - Producers: Frontier Works, Kadokawa Shoten, Movic, AT-X, Sony Music Communications, Hakuhodo DY Music & Pictures, Sammy 
 - Licensors: Funimation, Crunchyroll 
 - Studios: Production I.G 
 - Source: Novel
 - Genres: Military, Historical, Drama
 - Duration: 23 min. per ep.
 - Rating: R - 17+ (violence & profanity)


## Links

- [[Refrain] Joker Game - 01 VF [1080p]](https://uptobox.com/ki853vb8bl36)
- [[Refrain] Joker Game - 01 VF [720p]](https://uptobox.com/xr9eqxfohcn1)
- [[Refrain] Joker Game - 02 VF [1080p]](https://uptobox.com/2l7awjh13jn7)
- [[Refrain] Joker Game - 02 VF [720p]](https://uptobox.com/jr8hg6ypct6h)
- [[Refrain] Joker Game - 03 VF [1080p]](https://uptobox.com/x5kndd4qqacf)
- [[Refrain] Joker Game - 03 VF [720p]](https://uptobox.com/0w8u5y3ssyg9)
- [[Refrain] Joker Game - 04 VF [1080p]](https://uptobox.com/4v5x4axzpcmf)
- [[Refrain] Joker Game - 04 VF [720p]](https://uptobox.com/lhyxkcugobph)
- [[Refrain] Joker Game - 05 VF [1080p]](https://uptobox.com/6ht2qwlr02su)
- [[Refrain] Joker Game - 05 VF [720p]](https://uptobox.com/kgssqph7bmq4)
- [[Refrain] Joker Game - 06 VF [1080p]](https://uptobox.com/6v6bxq9km17j)
- [[Refrain] Joker Game - 06 VF [720p]](https://uptobox.com/8xmz234mgja2)
- [[Refrain] Joker Game - 07 VF [1080p]](https://uptobox.com/nc733j74j9gq)
- [[Refrain] Joker Game - 07 VF [720p]](https://uptobox.com/qki02e6mazc1)
- [[Refrain] Joker Game - 08 VF [1080p]](https://uptobox.com/0alajezgswk4)
- [[Refrain] Joker Game - 08 VF [720p]](https://uptobox.com/af1o853xqzn1)
- [[Refrain] Joker Game - 09 VF [1080p]](https://uptobox.com/61p5fj8m29s2)
- [[Refrain] Joker Game - 09 VF [720p]](https://uptobox.com/c66udqw5gfch)
- [[Refrain] Joker Game - 10 VF [1080p]](https://uptobox.com/z63p4kcjxrmd)
- [[Refrain] Joker Game - 10 VF [720p]](https://uptobox.com/13frtz0xr5k7)
- [[Refrain] Joker Game - 11 VF [1080p]](https://uptobox.com/17mw7bea1zqz)
- [[Refrain] Joker Game - 11 VF [720p]](https://uptobox.com/828nmv64oahb)
- [[Refrain] Joker Game - 12 VF [1080p]](https://uptobox.com/wm9stp9qihvw)
- [[Refrain] Joker Game - 12 VF [720p]](https://uptobox.com/pa4u8ow2zke8)
