# Rurouni Kenshin: Meiji Kenkaku Romantan - Tsuioku-hen

![Rurouni Kenshin: Meiji Kenkaku Romantan - Tsuioku-hen](https://cdn.myanimelist.net/images/anime/1899/94562l.jpg)

* Japanese:  るろうに剣心―明治剣客浪漫譚―追憶編

## Information

 - Type: OVA
 - Episodes: 4
 - Status: Finished Airing
 - Aired: Feb 20, 1999 to Sep 22, 1999
 - Producers: Aniplex 
 - Licensors: ADV Films, Aniplex of America 
 - Studios: Studio Deen 
 - Source: Manga
 - Genres: Action, Historical, Drama, Romance, Martial Arts, Samurai, Shounen
 - Duration: 30 min. per ep.
 - Rating: R - 17+ (violence & profanity)


## Links

