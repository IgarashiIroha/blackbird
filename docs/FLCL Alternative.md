# FLCL Alternative

![FLCL Alternative](https://cdn.myanimelist.net/images/anime/1494/93701l.jpg)

* Japanese:  フリクリ オルタナ

## Information

 - Type: Movie
 - Episodes: 1
 - Status: Finished Airing
 - Aired: Sep 7, 2018
 - Producers: Toho Visual Entertainment 
 - Licensors: NYAV Post 
 - Studios: Production I.G, Nut, Revoroot 
 - Source: Original
 - Genres: Action, Comedy, Dementia, Mecha, Parody, Sci-Fi
 - Duration: 2 hr. 15 min.
 - Rating: PG-13 - Teens 13 or older


## Links

- [[Refrain] FLCL Alternative - 01 VOSTFR [1080p]](https://uptobox.com/fg36o2ub0fr3)
- [[Refrain] FLCL Alternative - 01 VOSTFR [720p]](https://uptobox.com/qeb30i7j47v9)
- [[Refrain] FLCL Alternative - 02 VOSTFR [1080p]](https://uptobox.com/uqfhj847rqdp)
- [[Refrain] FLCL Alternative - 02 VOSTFR [720p]](https://uptobox.com/1mxykt768mbq)
- [[Refrain] FLCL Alternative - 03 VOSTFR [1080p]](https://uptobox.com/3x21jwxiaz9d)
- [[Refrain] FLCL Alternative - 03 VOSTFR [720p]](https://uptobox.com/8d3o059iydc3)
- [[Refrain] FLCL Alternative - 04 VOSTFR [1080p]](https://uptobox.com/8mbobjwzw0vl)
- [[Refrain] FLCL Alternative - 04 VOSTFR [720p]](https://uptobox.com/isib25206rfm)
- [[Refrain] FLCL Alternative - 05 VOSTFR [1080p]](https://uptobox.com/7n6qmv3e4v9d)
- [[Refrain] FLCL Alternative - 05 VOSTFR [720p]](https://uptobox.com/zpr2fqwt64ei)
- [[Refrain] FLCL Alternative - 06 VOSTFR [1080p]](https://uptobox.com/wvf9ccezokvp)
- [[Refrain] FLCL Alternative - 06 VOSTFR [720p]](https://uptobox.com/4ubaconpsphk)
